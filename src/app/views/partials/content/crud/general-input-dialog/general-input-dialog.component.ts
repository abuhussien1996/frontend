import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GeneralValidationMessagesService} from '../../../../../core/services/general-validation-messages.service';

@Component({
  selector: 'kt-general-input-dialog',
  templateUrl: './general-input-dialog.component.html',
  styleUrls: ['./general-input-dialog.component.scss']
})
export class GeneralInputDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<GeneralInputDialogComponent>,
    public validationMessageService: GeneralValidationMessagesService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [this.data.name, Validators.compose([Validators.required])],
    });
  }

  get name() {
    return this.form.get('name');
  }

  getErrorMessage(control) {
    return this.validationMessageService.getMessage(control);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onYesClick(): void {
    const controls = this.form.controls;

    if (this.form.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    this.dialogRef.close(this.name.value);
  }
}
