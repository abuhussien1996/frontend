import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralActionDialogComponent } from './general-action-dialog.component';

describe('GeneralActionDialogComponent', () => {
  let component: GeneralActionDialogComponent;
  let fixture: ComponentFixture<GeneralActionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralActionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralActionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
