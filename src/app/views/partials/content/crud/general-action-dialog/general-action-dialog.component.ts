// Angular
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'kt-general-action-dialog',
  templateUrl: './general-action-dialog.component.html',
  styleUrls: ['./general-action-dialog.component.scss']
})
export class GeneralActionDialogComponent implements OnInit {
  // Public properties
  viewLoading = false;

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<ConfirmActionDialogComponent>
   * @param data: any
   */
  constructor(
    public dialogRef: MatDialogRef<GeneralActionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
  }

  /**
   * Close dialog with false result
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * Close dialog with true result
   */
  onYesClick(): void {

    this.dialogRef.close(true);

  }
}
