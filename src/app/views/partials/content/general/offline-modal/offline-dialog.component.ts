import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'kt-offline-dialog',
    templateUrl: 'offline-dialog.component.html',
  })
  export class OfflineDialogComponent {
    constructor(
      private dialogRef: MatDialogRef<OfflineDialogComponent>) {
    }

    checkConnection() {
      if(window.clientInformation.onLine) {
        this.dialogRef.close();
      }
    }
  }