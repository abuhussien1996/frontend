// Angular
import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Subscription } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../../../core/reducers';
// Services and Models
import {
    ContactActionTypes,
    ContactModel,
    ContactOnServerCreated,
    selectContactLatestSuccessfullAction
  } from '../../../../../core/lists';
import { currentUser } from 'src/app/core/auth';
import { TranslateService } from '@ngx-translate/core';
import { skip } from 'rxjs/operators';
import {DatePipe} from "@angular/common";

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-contact-add',
    templateUrl: './contact-add.dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateContactDialogComponent implements OnInit, OnDestroy {
    contactForm: FormGroup;
    hasFormErrors: boolean;
    clicked: boolean;
    companyId: number;
	  private subscriptions: Subscription[] = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private datePipe: DatePipe,
        public dialogRef: MatDialogRef<CreateContactDialogComponent>,
        private store: Store<AppState>,
        private contactFB: FormBuilder,
        private translate: TranslateService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
		const userSub = this.store.pipe(select(currentUser)).subscribe((res) => {
			this.companyId = res !== undefined ? res.companyID : -1;
    });
    this.createForm();
		this.subscriptions.push(userSub);
        const successSubscription = this.store.pipe(select(selectContactLatestSuccessfullAction), skip(1)).subscribe((res) => {
          if (res && res.action_type) {
            switch(res.action_type) {
              case ContactActionTypes.ContactCreated :
                this.dialogRef.close({ added: true });
                break;
              default:
                break;
            }
          }
        });
        this.subscriptions.push(successSubscription);
    }

    getTitle() {
        return this.translate.instant('CONTACT.NEW_CONTACT');
    }

    /**
     * On destroy
     */
    ngOnDestroy() {
		this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Save data
     */
    onSubmit() {
        this.hasFormErrors = false;
        const controls = this.contactForm.controls;
        /** check form */
        if (this.contactForm.invalid) {
            Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
            this.hasFormErrors = true;
            return;
        }
        this.clicked = true;
        this.createContact(this.prepareContact());
    }

    /**
     * Returns object for saving
     */
    prepareContact(): ContactModel {
		const controls = this.contactForm.controls;
		const _contact = new ContactModel();
		_contact.company_id = this.companyId;
		_contact.contact_id = 0;
		_contact.first_name = controls.firstname.value;
		_contact.last_name = controls.lastname.value;
		_contact.last_name2 = controls.lastname2.value;
		_contact.entity_type = controls.entity_type.value;
		_contact.entity_id = controls.entity_id.value;
		_contact.email = controls.email.value;
		_contact.cell_phone = controls.cell_phone.value !== null ? controls.cell_phone.value.replace(/\D/g, '') : controls.cell_phone.value;
		_contact.home_phone = controls.home_phone.value !== null ? controls.home_phone.value.replace(/\D/g, '') : controls.home_phone.value;
		_contact.fax = controls.fax.value !== null ? controls.fax.value.replace(/\D/g, '') : controls.fax.value;
		_contact.state = controls.state.value ? controls.state.value.toUpperCase():'';
		_contact.city = controls.city.value;
		_contact.zip = controls.zip.value;
		_contact.address_1 = controls.address1.value;
		_contact.address_2 = controls.address2.value;
		_contact.note = '';
		_contact.is_active = controls.isActive.value;
    _contact.dob = this.datePipe.transform(controls.dob.value, 'yyyy-MM-dd');
		return _contact;
    }

    /**
     * Create contact
     *
     * @param _contact: Contact
     */
    createContact(_contact: ContactModel) {
		this.store.dispatch(new ContactOnServerCreated({ contact: _contact }));
    }

    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.contactForm.controls[controlName];
        if (!control) {
            return false;
        }
        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }

    close() {
        this.dialogRef.close();
    }

    createForm() {
        this.contactForm = this.contactFB.group({
            firstname: [null, Validators.required],
            lastname: [null, Validators.required],
            lastname2: [null],
            entity_type: [null],
            entity_id: [null],
            email: [null, [Validators.email]],
            cell_phone: [null, [Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)]],
            home_phone: [null, Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)],
            fax: [null, [Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)]],
            state: [null,Validators.pattern(/^[A-Za-z]{2}$/)],
            city: [null],
            zip: [null, [ Validators.pattern(/(^\d{5}$)|(^\d{5}-\d{4}$)/)]],
            address1: [null],
            address2: [null],
            isActive: [true],
            dob: [null],
        });
    }

    getStatusString(): string {
      switch (this.contactForm.get('isActive').value) {
        case true:
          return this.translate.instant('GLOBAL.ACTIVE');
        case false:
          return this.translate.instant('GLOBAL.INACTIVE');
      }
      return '';
    }

}
