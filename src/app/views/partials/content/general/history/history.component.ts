import {Component, Input} from '@angular/core';
import * as moment from 'moment';
import {TypesUtilsService} from '../../../../../core/_base/crud';

@Component({
  selector: 'kt-history',
  styleUrls: ['./history.component.scss'],
  templateUrl: './history.component.html'
})
export class HistoryComponent {

  @Input() id;
  @Input() history;

  constructor(private typesUtilsService: TypesUtilsService) {}

  getMomentDate(date) {
    return `${moment(new Date(date.concat(' GMT'))).format('MM/DD/YY')} at ${moment(new Date(date.concat(' GMT'))).format('h:mm a')}`;
  }

  getActionText(revType) {
    switch (revType) {
      case 'ADD':
        return 'added';
      case 'MOD':
        return 'modified';
      case 'DEL':
        return 'deleted';
      default:
        return '';
    }
  }

  symbolCSS(name) {
    const color = this.typesUtilsService.getRandomColorBasedString(name)
    return {'background-color': color ? color : '#E1F0FF', color: color ? 'white' : '#8950FC'};
  }
}
