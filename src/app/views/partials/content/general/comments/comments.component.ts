import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {AuthService, currentUser} from 'src/app/core/auth';
import {Subscription} from 'rxjs';
import * as moment from 'moment';
import {LayoutUtilsService, TypesUtilsService} from '../../../../../core/_base/crud';
import {CommentsService} from 'src/app/core/services/comments.service';

@Component({
  selector: 'kt-comments',
  templateUrl: './comments.component.html'
})
export class CommentsComponent implements OnInit, OnDestroy {

  @Input() id;
  @Input() comments;
  @Input() type;
  @Input() readonly = false;
  displayComments;
  editCommentValue;
  currentUser;
  isSubmittingComment: boolean;
  isEditing: boolean;
  subscriptions: Subscription[] = [];

  constructor(
    private layoutUtilsService: LayoutUtilsService,
    private commentsService: CommentsService,
    private cdr: ChangeDetectorRef,
    private typesUtilsService: TypesUtilsService,
    private store: Store<AppState>,
    private auth: AuthService,
  ) {

  }

  ngOnInit() {
    this.auth.getUserByToken().subscribe(user => {
      this.currentUser = user
    })
    this.displayComments = this.comments.slice(0, 4);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getMomentDate(date) {
    return date ? moment(new Date(date.concat(' GMT'))).fromNow() : '';
  }

  postComment(comment) {
    if (comment.length > 0) {
      this.isSubmittingComment = true;
      this.commentsService.postComment(this.type, this.id, comment).subscribe((res) => {
        if (res && res.data) {
          setTimeout(() => {
            const newComment = res.data;
            newComment.profileUrl = this.currentUser.profileUrl
            newComment.editMode = false;
            newComment.userName = newComment.last_modified_user;
            this.comments.unshift(newComment);
            this.isSubmittingComment = false;
            this.cdr.detectChanges();
          }, 2000);
        }
      });
    }
  }

  updateComment(comment_id) {
    if (this.editCommentValue.length > 0) {
      this.isSubmittingComment = true;
      const updatedComment = this.comments.find((c) => c.id === comment_id);
      this.commentsService.updateComment(this.type, this.id, comment_id, this.editCommentValue).subscribe((res) => {
        if (res && res.data) {
          updatedComment.comment = res.data.comment;
        }
        this.isEditing = false;
        this.cdr.detectChanges();
      });
      updatedComment.editMode = false;
      this.isSubmittingComment = false;
    }
  }

  deleteComment(comment_id) {
    const _title = 'Comment Delete';
    const _description = 'Are you sure you want to permanently delete this comment?'
    const _waitDesciption = 'Comment is deleting...'

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res1 => {
      if (!res1) {
        return;
      }
      this.commentsService.deleteComment(this.type, this.id, comment_id).subscribe((res) => {
        if (res === null) {
          this.comments.splice(this.comments.findIndex((obj) => obj.id === comment_id), 1);
          this.cdr.detectChanges();
        }
      })
    });
  }

  symbolCSS(name) {
    const color = this.typesUtilsService.getRandomColorBasedString(name);
    return {'background-color': color ? color : '#E1F0FF', color: color ? 'white' : '#8950FC'};
  }

  onScroll(event) {
    if (
      event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight
      && this.displayComments.length < this.comments.length
    ) {
      this.displayComments.push(this.comments[this.displayComments.length]);
    }
  }
}
