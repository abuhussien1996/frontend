// Angular
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgApexchartsModule} from 'ng-apexcharts';
// NgBootstrap
import {NgbDropdownModule, NgbTabsetModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
// Perfect Scrollbar
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
// Core module
import {CoreModule} from '../../core/core.module';
// CRUD Partials
import {
  ActionNotificationComponent,
  AlertComponent,
  ConfirmActionDialogComponent,
  DeleteEntityDialogComponent,
  FetchEntityDialogComponent,
  GeneralInputDialogComponent,
  UpdateStatusDialogComponent
} from './content/crud';
// Layout partials
import {
  LanguageSelectorComponent,
  NotificationComponent,
  QuickActionComponent,
  QuickPanelComponent,
  QuickUserPanelComponent,
  ScrollTopComponent,
  SearchDropdownComponent,
  SearchResultComponent,
  SplashScreenComponent,
  StickyToolbarComponent,
  Subheader1Component,
  UserProfile4Component,
  UserProfileComponent
} from './layout';
// General
import {PortletModule} from './content/general/portlet/portlet.module';
// SVG inline
import {InlineSVGModule} from 'ng-inline-svg';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import {MatRadioModule} from '@angular/material/radio';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import {OfflineDialogComponent} from './content/general/offline-modal/offline-dialog.component';
import {Store, StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {MessageEffects} from 'src/app/core/timeline/_effects/message.effects';
import {messagesReducer} from 'src/app/core/timeline/_reducers/message.reducers';
import {MessageViewComponent} from 'src/app/views/pages/apps/timeline/messages/message-view/message-view.component';
import {MessageEditComponent} from '../pages/apps/timeline/messages/message-edit/message-edit.component';
import {SendEmailFaxComponent} from '../pages/apps/timeline/send-email-fax/send-email-fax.component';
import {
  AttachmentsService,
  CategoryService,
  ContactsService,
} from 'src/app/core/lists/_services';
import {CommunicationsService, MeetingService, TaskService, TwixorService} from 'src/app/core/timeline/_services';
import {UserService} from 'src/app/core/auth/_services';
import {MeetingEditComponent} from '../pages/apps/timeline/meetings/meeting-edit/meeting-edit.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {ProgressBarComponent} from './layout/progress-bar/progress-bar.component';
import {NgxPermissionsModule, NgxPermissionsRestrictStubModule} from 'ngx-permissions';
import {MatExpansionModule} from '@angular/material/expansion';
import {
  AttachmentEffects,
  attachmentsReducer,
  categoriesReducer,
  CategoryEffects, ContactEffects, contactsReducer,
} from 'src/app/core/lists';
import {ViewCommunicationComponent} from '../pages/apps/timeline/view-communication/view-communication.component';
import {MeetingViewDialogComponent} from '../pages/apps/timeline/meetings/meeting-view/meeting-view-dialog.component';
import {UploadMarketingDocsComponent} from '../pages/apps/timeline/upload-marketing-docs/upload-marketing-docs.component';
import {CreateTaskComponent} from '../pages/calendar/create-task/create-task.component';
import {GeneralAutocompleteComponent} from './layout/general-autocomplete/general-autocomplete.component';
import {TranslateModule} from '@ngx-translate/core';
import {CommentsComponent} from './content/general/comments/comments.component';
import {HistoryComponent} from './content/general/history/history.component';
import {ChooseTemplateDialogComponent} from '../pages/calendar/choose-template/choose-template.dialog.component';
import {TemplateService} from 'src/app/core/lists/_services/template.service';
import {TypesUtilsService} from '../../core/_base/crud';
import {UserPhotoComponent} from './layout/user-photo/user-photo.component';
import { GeneralActionDialogComponent } from './content/crud/general-action-dialog/general-action-dialog.component';
import {CreateContactDialogComponent} from './content/general/contact-add-dialog/contact-add.dialog.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {SharedModule} from '../../shared.module';
import {MailViewDialogComponent} from '../pages/apps/events/inbox/mail-view-dialog/mail-view-dialog.component';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {InboxEffects, inboxReducer, InboxService} from '../../core/events';
import {ReplyDialogComponent} from '../pages/apps/events/inbox/reply-dialog/reply-dialog.component';
import {SelectWorkflowDialogComponent} from '../pages/apps/events/inbox/select-workflow-dialog/select-workflow-dialog.component';
import {WorkflowDialogComponent} from '../pages/apps/events/inbox/workflow-dialog/workflow-dialog.component';
import {FormioModule} from 'angular-formio';
import {TaskTimelineComponent} from '../pages/calendar/task-timeline/task-timeline.component';

@NgModule({
  declarations: [
    ScrollTopComponent, ActionNotificationComponent,
    ConfirmActionDialogComponent,
    DeleteEntityDialogComponent,
    FetchEntityDialogComponent,
    UpdateStatusDialogComponent,
    AlertComponent,
    OfflineDialogComponent,
    UserPhotoComponent,
    QuickPanelComponent,
    QuickUserPanelComponent,
    ScrollTopComponent,
    SearchResultComponent,
    SplashScreenComponent,
    StickyToolbarComponent,
    Subheader1Component,
    LanguageSelectorComponent,
    NotificationComponent,
    QuickActionComponent,
    SearchDropdownComponent,
    UserProfileComponent,
    UserProfile4Component,
    ProgressBarComponent,
    GeneralAutocompleteComponent,
    GeneralInputDialogComponent,
    CommentsComponent,
    HistoryComponent,
    GeneralActionDialogComponent,CreateContactDialogComponent,
    MailViewDialogComponent,
    ReplyDialogComponent,
    SelectWorkflowDialogComponent,
    WorkflowDialogComponent,
    UploadMarketingDocsComponent
  ],
  exports: [
    PortletModule,

    ScrollTopComponent,
    ActionNotificationComponent,
    ConfirmActionDialogComponent,
    DeleteEntityDialogComponent,
    FetchEntityDialogComponent,
    UpdateStatusDialogComponent,
    GeneralInputDialogComponent,
    AlertComponent,
    OfflineDialogComponent,
    QuickPanelComponent,
    QuickUserPanelComponent,
    ScrollTopComponent,
    SearchResultComponent,
    SplashScreenComponent,
    StickyToolbarComponent,
    Subheader1Component,
    LanguageSelectorComponent,
    NotificationComponent,
    QuickActionComponent,
    SearchDropdownComponent,
    UserProfileComponent,
    UserProfile4Component,
    ProgressBarComponent,
    GeneralAutocompleteComponent,
    CommentsComponent,
    HistoryComponent,
    UserPhotoComponent,CreateContactDialogComponent
  ],
  imports: [
    MatExpansionModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    InlineSVGModule,
    CoreModule,
    PortletModule,
    NgApexchartsModule,
    StoreModule.forFeature('contacts', contactsReducer),
    EffectsModule.forFeature([ContactEffects]),
    StoreModule.forFeature('messages', messagesReducer),
    EffectsModule.forFeature([MessageEffects]),
    StoreModule.forFeature('attachments', attachmentsReducer),
    EffectsModule.forFeature([AttachmentEffects]),
    StoreModule.forFeature('categories', categoriesReducer),
    EffectsModule.forFeature([CategoryEffects]),
    StoreModule.forFeature('inbox', inboxReducer),
    EffectsModule.forFeature([InboxEffects]),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatChipsModule,
    MatIconModule,
    // ng-bootstrap modules
    NgbDropdownModule,
    NgbTabsetModule,
    NgbTooltipModule,
    TranslateModule.forChild(),
    MatRippleModule,
    NgxPermissionsRestrictStubModule,
    NgxPermissionsModule.forChild(),
    MatSlideToggleModule,
    SharedModule,
    NgxExtendedPdfViewerModule,
    FormioModule

  ],
  entryComponents: [
    DeleteEntityDialogComponent,
    MessageViewComponent,
    MessageEditComponent,
    SendEmailFaxComponent,
    UploadMarketingDocsComponent,
    MeetingEditComponent,
    ActionNotificationComponent,
    ViewCommunicationComponent,
    MessageViewComponent,
    MeetingViewDialogComponent,
    CreateTaskComponent,
    ConfirmActionDialogComponent,
    ChooseTemplateDialogComponent,
    GeneralActionDialogComponent,
    CreateContactDialogComponent,
    MailViewDialogComponent,
    ReplyDialogComponent,
    SelectWorkflowDialogComponent,
    WorkflowDialogComponent,
    UploadMarketingDocsComponent,
    TaskTimelineComponent
  ],
  providers: [
    TypesUtilsService,
    ContactsService,
    UserService,
    CommunicationsService,
    MeetingService,
    TaskService,
    DatePipe,
    AttachmentsService,
    TwixorService,
    Store,
    MatDialog,
    CategoryService,
    TemplateService,
    InboxService
  ]
})
export class PartialsModule {
}
