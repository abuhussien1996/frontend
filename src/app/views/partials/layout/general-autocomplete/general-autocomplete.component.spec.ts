import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralAutocompleteComponent } from './general-autocomplete.component';

describe('GeneralAutocompleteComponent', () => {
  let component: GeneralAutocompleteComponent;
  let fixture: ComponentFixture<GeneralAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
