import {AfterContentInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, finalize, switchMap} from 'rxjs/operators';
import {AbstractControl, FormControl, ValidatorFn} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
  selector: 'kt-general-autocomplete',
  templateUrl: './general-autocomplete.component.html',
  styleUrls: ['./general-autocomplete.component.scss']
})
export class GeneralAutocompleteComponent implements OnInit, OnDestroy, AfterContentInit {

  filteredItems = of([]);
  controlObject = new FormControl();
  controlObjectVal = new FormControl();
  readonly term$ = new BehaviorSubject('');
  isLoading = new BehaviorSubject(false);
  focusInBS = new BehaviorSubject(false);
  reload = new BehaviorSubject(false);
  inputCtrl = new FormControl();
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  @Input() loadItemsFunction;
  @Input() optionFormatter;
  @Input() valueFormatter;
  @Input() panelWidth = 'auto';
  @Input() required;
  @Input() control;
  private _readOnly: any;
  @Input() set readOnly(val) {
    this._readOnly = val;
    if (this.controlObject) {
      if (val) {
        this.controlObject.disable();
      } else {
        this.controlObject.enable();
      }
    }
  };

  get readOnly() {
    return this._readOnly;
  }

  @Input() change;
  @Input() placeholder;
  @Input() dataPath = null;
  @Input() hint = false;
  @Input() hintStrong = false;
  value: any;
  @Input() label: any;
  @Input() appearance = 'fill';
  @Input() chipList = false;
  @Input() formatNewObjectFn;
  @Input() objects = [];
  @ViewChild('recorderInput') recorderInputChipList: ElementRef<HTMLInputElement>;
  private sub: any;
  private sub1: any;
  private sub2: any;
  private removeAll: boolean;
  private ctrl: FormControl;
  manualTrigger = new Subject();

  ngOnInit(): void {
    if (this.required)
      this.controlObject = new FormControl('', [this.valueSelected()])
    const ctrl = this.chipList ? this.inputCtrl : this.controlObject;
    this.ctrl = ctrl;
    ctrl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(() => this.getAutocompleteSuggestions().pipe(catchError(() => {
            this.isLoading.next(false);
            return []
          }))
        ))
      .subscribe(autocompleteList => {
        if (this.dataPath && autocompleteList[this.dataPath]) {
          autocompleteList = autocompleteList[this.dataPath];
        }
        if (!autocompleteList || !autocompleteList.length) {
          autocompleteList = [];
        }
        this.filteredItems = of(autocompleteList)
        this.isLoading.next(false);
      });

    this.sub = this.focusInBS.subscribe(() => {
      this.isLoading.next(true);

      this.sub1 = this.loadItemsFunction(this.term$.value || '').pipe(finalize(() => {
        this.isLoading.next(false);
        return []
      }))
        .subscribe(autocompleteList => {
          if (this.dataPath && autocompleteList[this.dataPath]) {
            autocompleteList = autocompleteList[this.dataPath];
          }
          if (!autocompleteList || !autocompleteList.length) {
            autocompleteList = [];
          }
          this.filteredItems = of(autocompleteList)
          this.isLoading.next(false);
          this.sub.unsubscribe();
        });
    });

    this.sub2 = this.reload.subscribe((val) => {
      if (val) {
        this.filteredItems = of([])
        this.isLoading.next(true);
        this.loadItemsFunction(this.inputCtrl.value ? this.inputCtrl.value : '');
        this.sub1 = this.loadItemsFunction(this.term$.value || '').pipe(catchError(() => {
          this.isLoading.next(false);
          return []
        }))
          .subscribe(autocompleteList => {
            this.isLoading.next(false);
            if (this.dataPath && autocompleteList[this.dataPath]) {
              autocompleteList = autocompleteList[this.dataPath];
            }
            if (!autocompleteList || !autocompleteList.length) {
              autocompleteList = [];
            }
            this.filteredItems = of(autocompleteList);
          });
      }
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
    this.sub2.unsubscribe();
  }


  displayFnRecorder(val) {
    return Math.random();
  }

  selectedRecorder(setNewValue=true) {
    if (this.chipList) {
      if (!this.objects) {
        this.objects = [];
      }
      if(setNewValue)
        this.objects.push(this.formatNewObjectFn(this.inputCtrl.value));
      this.controlObject.setValue(this.objects);
    }
    this.value = this.chipList ? this.objects : this.controlObject.value;
    this.removeAll = true;
    this.change(this.value);
    this.recorderInputChipList.nativeElement.value = '';
  }

  onChange($event: any) {
    if ($event.keyCode === 8 && this.removeAll || (!this.removeAll && this.controlObject.value === '')) {
      this.controlObject.setValue('');
      this.inputCtrl.setValue('');
      this.selectedRecorder()
    } else {
      this.removeAll = false;
    }
  }

  private getAutocompleteSuggestions(): Observable<any> {
    this.filteredItems = of([])

    const ctrl = this.chipList ? this.inputCtrl : this.controlObject;
    if (ctrl.value || ctrl.value === '') {
      if (typeof ctrl.value === 'string') {
        this.isLoading.next(true);
        return this.loadItemsFunction(ctrl.value)
      }
    }
    return this.filteredItems;
  }

  valueSelected(): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      if (this.chipList) {
        if (!(c.value) || c.value.length < 1) {
          return {required: true};
        }
        return null
      } else if (typeof c.value === 'object') {
        if (!(c.value)) {
          return {required: true};
        }
        return null
      }
      return {required: true};
    };
  }

  updateValueAndValidity() {
    this.controlObject.updateValueAndValidity();
    this.controlObject.markAsTouched()
  }

  setValue(res) {
    this.removeAll = true;
    if(this.chipList){
      this.objects = res;
      this.controlObject.setValue(res);

    }else{
      this.controlObject.setValue(res);
    }
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
    if (this.chipList){
      this.filteredItems = of(res)
    }else {
      this.filteredItems = of([res])
    }
    this.selectedRecorder(false)
  }

  onClick() {
    this.focusInBS.next(!this.focusInBS.value)
  }

  remove(entity) {
    let index;
    index = this.objects.indexOf(entity);
    if (index >= 0) {
      this.objects.splice(index, 1);
      this.controlObject.setValue(this.objects);
      this.change(this.objects);

    }
  }

  triggerReload() {
    this.reload.next(true);
  }

  ngAfterContentInit(): void {
    if (this.readOnly) {
      this.controlObject.disable();
    }
  }
}
