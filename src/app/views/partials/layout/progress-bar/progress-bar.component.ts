import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {ThemePalette} from '@angular/material/core';
import {HttpEventType, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'kt-custom-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

  @Input() requests: [Observable<any>];
  color: ThemePalette = 'primary';
  mode: any = 'determinate';
  value = 0;
  totalUpload = 0;
  @Input() totalFileSize = 1;
  valueSubject = new BehaviorSubject(0);
  description = new BehaviorSubject('');
  private preFilesLoaded = [];

  @Output() callback  = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
    let numberOfRequests = 0;
    this.requests.map((request, index) => {
      this.preFilesLoaded[index] = 0;
      numberOfRequests++;
      request.subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          const deff = event.loaded - this.preFilesLoaded[index];
          this.preFilesLoaded[index] = event.loaded
          let percentDone = Math.round(100 * event.loaded / event.total);
          this.totalUpload += deff;
          percentDone = Math.round(100 * this.totalUpload / this.totalFileSize);

          this.description.next(`File is ${percentDone}% uploaded.`)
          this.valueSubject.next(percentDone);
        } else if (event instanceof HttpResponse) {
          this.description.next('File is completely uploaded!');
          numberOfRequests --;
          if(numberOfRequests ===0)
            this.callback.emit();
        }
      })
    })
  }

}
