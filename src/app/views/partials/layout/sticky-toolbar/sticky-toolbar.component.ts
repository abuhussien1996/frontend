import {Component} from '@angular/core';
import {LayoutConfigService, OffcanvasOptions} from '../../../../core/_base/layout';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ChooseTemplateDialogComponent } from 'src/app/views/pages/calendar/choose-template/choose-template.dialog.component';

@Component({
  selector: 'kt-sticky-toolbar',
  templateUrl: './sticky-toolbar.component.html',
  styleUrls: ['./sticky-toolbar.component.scss'],
})
export class StickyToolbarComponent {
  // Public properties
  demoPanelOptions: OffcanvasOptions = {
    overlay: true,
    baseClass: 'offcanvas',
    placement: 'right',
    closeBy: 'kt_demo_panel_close',
    toggleBy: 'kt_demo_panel_toggle'
  };

  baseHref: string;

  constructor(private layoutConfigService: LayoutConfigService,
              public dialog: MatDialog
  ) {
    this.baseHref = 'https://keenthemes.com/metronic/preview/angular/';
  }

  isActiveDemo(demo) {
    return demo === this.layoutConfigService.getConfig('demo');
  }

  createTask() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    this.dialog.open(ChooseTemplateDialogComponent, dialogConfig);
  }


}
