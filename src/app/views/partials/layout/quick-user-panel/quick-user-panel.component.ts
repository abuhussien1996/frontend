// Angular
import {Component, OnInit, ViewChild} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
// Layout
import { OffcanvasOptions } from '../../../../core/_base/layout';
import { AppState } from '../../../../core/reducers';
import { currentUser, Logout, User, AuthService } from '../../../../core/auth';
import {Router} from '@angular/router';

@Component({
  selector: 'kt-quick-user-panel',
  templateUrl: './quick-user-panel.component.html',
  styleUrls: ['./quick-user-panel.component.scss']
})
export class QuickUserPanelComponent implements OnInit {
  user$: Observable<User>;
  @ViewChild('offCanvas') offCanvas;
  offcanvasOptions: OffcanvasOptions = {
    overlay: true,
    baseClass: 'offcanvas',
    placement: 'right',
    closeBy: 'kt_quick_user_close',
    toggleBy: 'kt_quick_user_toggle'
  };

  constructor(private store: Store<AppState>,private auth:AuthService, private router: Router) {

  }

  /**
   * On init
   */
  ngOnInit(): void {
    this.user$ = this.store.pipe(select(currentUser));
  }

  /**
   * Log out
   */
  logout() {
    // this.store.dispatch(new Logout());
    this.auth.logout(false);
  }

  closeOffCanvas(){
    // @ts-ignore
    const element = $('#kt_quick_user_close')[0];
    if(element)
      element.click();
  }

  redirectToUserDetails(userId){
    this.closeOffCanvas();
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.navigate(['/user-management/users/edit/'+userId]);

  }
}
