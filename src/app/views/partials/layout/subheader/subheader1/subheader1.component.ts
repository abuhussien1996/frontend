// Angular
import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
// RxJS
import { Subscription } from 'rxjs';
// Layout
import { SubheaderService } from '../../../../../core/_base/layout';
import { Breadcrumb } from '../../../../../core/_base/layout/services/subheader.service';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'kt-subheader1',
  templateUrl: './subheader1.component.html',
  styleUrls: ['./subheader1.component.scss']
})
export class Subheader1Component implements OnInit, OnDestroy, AfterViewInit {
  // Public properties
  @Input() fixed = true;
  @Input() clear = false;
  @Input() width = 'fluid';
  @Input() subheaderClasses = '';
  @Input() subheaderContainerClasses = '';
  @Input() displayDesc = false;
  @Input() displayDaterangepicker = true;

  today: number = Date.now();
  title = '';
  desc = '';
  breadcrumbs: Breadcrumb[] = [];

  // Private properties
  private subscriptions: Subscription[] = [];

  /**
   * Component constructor
   *
   * @param subheaderService: SubheaderService
   * @param router
   */
  constructor(public subheaderService: SubheaderService) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
  }

  /**
   * After view init
   */
  ngAfterViewInit(): void {
    this.subscriptions.push(this.subheaderService.title$.subscribe(bt => {
      // breadcrumbs title sometimes can be undefined
      if (bt) {
          this.title = bt.title;
          this.desc = bt.desc;
      }
    }));

    this.subscriptions.push(this.subheaderService.breadcrumbs$.subscribe(bc => {
      if (bc.length>0){
        this.breadcrumbs = bc;
      }
    }));
  }

  hasAnalytics() {
    if(this.breadcrumbs && this.breadcrumbs[0]) {
      if(this.breadcrumbs[0].title.indexOf('Lists')!==-1) {
        if(
          this.breadcrumbs[1].title.indexOf('Pharmacy')!==-1
          || this.breadcrumbs[1].title.indexOf('Client')!==-1
          || this.breadcrumbs[1].title.indexOf('Doctor')!==-1
          || this.breadcrumbs[1].title.indexOf('Patient')!==-1
          || this.breadcrumbs[1].title.indexOf('Contact')!==-1
          || this.breadcrumbs[1].title.indexOf('Facility')!==-1
        ) {
          return true;
        }
      } else if(this.breadcrumbs[1] && this.breadcrumbs[1].title.indexOf('Task')!==-1) {
        return true;
      }
    }
    return false;
  }

  getPageId() {
    if(this.breadcrumbs && this.breadcrumbs[0]) {
      if(this.breadcrumbs[0].title.indexOf('Lists')!==-1) {
        if(
          this.breadcrumbs[1].title.indexOf('Pharmacy')!==-1
          || this.breadcrumbs[1].title.indexOf('Client')!==-1
          || this.breadcrumbs[1].title.indexOf('Doctor')!==-1
          || this.breadcrumbs[1].title.indexOf('Patient')!==-1
          || this.breadcrumbs[1].title.indexOf('Facility')!==-1
        ) {
          return 2;
        } else if (
          this.breadcrumbs[1].title.indexOf('Contact')!==-1
        ) {
          return 3;
        }
      } else if(this.breadcrumbs[1] && this.breadcrumbs[1].title.indexOf('Task')!==-1) {
        return 1;
      }
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }
}
