import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../core/reducers';
import {AuthService, UserLoaded} from '../../../../core/auth';
import {LayoutUtilsService, MessageType, TypesUtilsService} from '../../../../core/_base/crud';
import {CommunicationsService} from '../../../../core/timeline/_services';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../../../core/auth/_services';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'kt-user-photo',
  templateUrl: './user-photo.component.html',
  styleUrls: ['./user-photo.component.scss']
})
export class UserPhotoComponent implements OnInit {
  photoUrl = ''
  files = [];
  user
  @Input() editable: boolean;
  @Input() size: number;
  @Input() user$: any;
  @Input() type: string;
  @Input() create: string;
  @ViewChild('fileInput') fileInput: ElementRef;


  constructor(
    private cdRef: ChangeDetectorRef,
    public dialog: MatDialog,
    private router: Router,
    private store: Store<AppState>,
    private auth: AuthService,
    private typesUtilsService: TypesUtilsService,
    private communicationsService: CommunicationsService,
    private translate: TranslateService,
    private layoutUtilsService: LayoutUtilsService,
    private userService: UserService) {
  }

  ngOnInit(): void {
    if (this.type === 'observable') {
      this.user$.subscribe(res => {
        this.user = res
        if (this.user && this.user.profileUrl) {
          this.photoUrl = this.user.profileUrl
        }
      })
      return
    }
    this.user = this.user$
    this.photoUrl = this.user.profileUrl ? this.user.profileUrl : ''
  }

  symbolCSS(name) {
    const color = this.typesUtilsService.getRandomColorBasedString(name)
    if (!color) {
      return {'background-color': '#E1F0FF', color: '#3699FF'};
    }
    return {'background-color': color, color: 'white'};
  }

  onFileChange(event) {
    this.files = [];
    for (const file of event.target.files) {
      this.files.push(file);
    }
    const request = {
      id: this.user.id,
      updateProfile: true,
      email: this.user.email,
      userName: this.user.userName,
      firstName: this.user.firstName,
      lastName: this.user.lastName
    }
    this.userService.changeProfilePic(request).subscribe((res) => {
      if (res.profileUrl) {
        const uuids = res.uuid;
        const profileUrl = res.profileUrl;
        this.communicationsService.uploadProfilePic(profileUrl, this.files[0]).subscribe();
        this.sendFiles(uuids);
      }
    })
  }

  sendFiles(uuids) {
    const request = {
      id: this.user.id,
      updateProfile: false,
      profilePath: uuids,
      email: this.user.email,
      userName: this.user.userName,
      firstName: this.user.firstName,
      lastName: this.user.lastName
    }
    this.userService.changeProfilePic(request).subscribe((res) => {
      if (res.code === 200) {
        this.auth.getUserByToken().subscribe(user => {
          if (user.profileUrl) {
            this.photoUrl = user.profileUrl
            this.store.dispatch(new UserLoaded({user}));
            this.cdRef.detectChanges();
            const photo = (document.getElementById(user.userName) as HTMLImageElement)
            if (user.profileUrl && photo) {
              photo.src = this.photoUrl;
            }
            this.layoutUtilsService.showActionNotification('Profile photo updated successfully',
              MessageType.Update, 3000, false, false);
          }
        })
      } else {
        this.layoutUtilsService.showActionNotification(this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Update, 3000, false, false);
      }
    });
  }
}
