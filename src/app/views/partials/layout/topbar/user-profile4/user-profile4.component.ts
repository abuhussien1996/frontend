// Angular
import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
// RxJS
// NGRX
import {select, Store} from '@ngrx/store';
// State
import {AppState} from '../../../../../core/reducers';
import {AuthService, currentUser, Logout} from '../../../../../core/auth';
import {TypesUtilsService} from '../../../../../core/_base/crud';

@Component({
  selector: 'kt-user-profile4',
  templateUrl: './user-profile4.component.html',
})
export class UserProfile4Component implements OnInit {
  user$: any;

  @Input() avatar = true;
  @Input() greeting = true;
  @Input() badge: boolean;
  @Input() icon: boolean;

  /**
   * Component constructor
   *
   * @param store: Store<AppState>
   */
  constructor(private store: Store<AppState>,
              private typesUtilsService: TypesUtilsService,
              private auth: AuthService,
              private cdr: ChangeDetectorRef) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit(): void {
    this.user$ = this.store.pipe(select(currentUser));
  }

  symbolCSS(name) {
    const color = this.typesUtilsService.getRandomColorBasedString(name)
    if (!color) {
      return {'background-color': '#E1F0FF', 'color': '#3699FF'};
    }
    return {'background-color': color, 'color': 'white'};
  }

  /**
   * Log out
   */
  logout() {
    this.store.dispatch(new Logout());
    this.auth.logout();
    location.reload(true);
  }
}
