// Angular
import {ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MessageType, QueryParamsModel} from 'src/app/core/_base/crud';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import { DatePipe } from '@angular/common';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {TranslateService} from '@ngx-translate/core';
import {currentUser} from 'src/app/core/auth';
import {Subscription} from 'rxjs';
import {MessageViewComponent} from 'src/app/views/pages/apps/timeline/messages/message-view/message-view.component';
import {Message} from 'src/app/core/timeline/_models/message.model';
import {selectMessagesInStore, selectMessagesPageLastQuery} from 'src/app/core/timeline/_selectors/message.selectors';
import {MessagesDataSource} from 'src/app/core/timeline/_data-sources/message.datasource';
import {AddNewMessages, MessagesPageRequested, MessageStatusUpdated} from 'src/app/core/timeline/_actions/message.actions';
import {MessageService} from 'src/app/core/timeline/_services/message.service'

@Component({
  selector: 'kt-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {

  // Show dot on top of the icon
  @Input() dot: string;

  // Show pulse on icon
  @Input() pulse: boolean;

  @Input() pulseLight: boolean;

  // Set icon class name
  @Input() icon = 'flaticon2-bell-alarm-symbol';
  @Input() iconType: '' | 'success';

  // Set true to icon as SVG or false as icon class
  @Input() useSVG: boolean;

  // Set bg image path
  @Input() bgImage: string;

  // Set skin color, default to light
  @Input() skin: 'light' | 'dark' = 'light';

  @Input() type: 'brand' | 'success' = 'success';


  refreshInterval: Subscription;
  i = 1;
  // Table fields
  dataSource: MessagesDataSource;
  displayedColumns = ['select', 'to', 'text', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = '';
  filterCondition = '';
  lastQuery: QueryParamsModel;
  // Selection
  selection = new SelectionModel<Message>(true, []);
  messagesResult: Message[] = [];
  users: any[];
  // Error Handling
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  // Subscriptions
  notifications: any[]
  private subscriptions: any[] = [];
  currentCount: number;

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param activatedRoute: ActivatedRoute
   * @param router: Router
   * @param store: Store<AppState>
   * @param cdr: ChangeDetectorRef,
   * @param translate: TranslateService,
   * @param authService: AuthService
   */
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private store: Store<AppState>,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private datePipe: DatePipe,
    private messageService: MessageService
  ) {
    this.getMessages()
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  hidden = true;
  isFirst = true

  toggleBadgeVisibility() {
    this.hidden = true
  }

  ngOnInit() {
    // Init DataSource
    this.dataSource = new MessagesDataSource(this.store);
    const newMessagesSub = this.store.pipe(select(selectMessagesInStore)).subscribe(res => {
      this.messagesResult = res.items;
      this.notifications = res.items;
      if (!this.currentCount && res.items) {
        this.getDataPromise();
        this.currentCount = res.items.length;
      }
      this.cdr.detectChanges();
    });
    this.subscriptions.push({id: 4, sub: newMessagesSub});
    const lastQuerySubscription = this.store.pipe(select(selectMessagesPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push({id: 5, sub: lastQuerySubscription});
    // Read from URL itemId, for restore previous state
    // const authSubscription = this.store.pipe(select(currentUser)).subscribe((res) => {
    //   if (res) {
    //     this.loadMessagesList();
    //   }
    // });
    this.loadMessagesList();
    // this.subscriptions.push({id: 6, sub: authSubscription});
  }


  backGroundStyle(): string {
    if (!this.bgImage) {
      return 'none';
    }

    return 'url(' + this.bgImage + ')';
  }


  getDataPromise() {
    if (this.notifications.length > 0) {
      this.messageService.findNewMessages(this.notifications[0].created)
        .subscribe(res => {
          if (res.data.length > 0) {
            this.getData(res);
            if (this.currentCount > 0) {
              this.getDataPromise();
            }
          }
          this.cdr.detectChanges();
        });
    }
  }

  getData(res) {
    return new Promise((resolve) => {
      this.store.dispatch(new AddNewMessages({messages: res.data, addCount: res.data.length}));
      this.cdr.detectChanges();
      resolve(this.notifications);
      if (this.isFirst){
        this.isFirst = false
        return this.hidden = true;
      }
      return this.hidden = false;
    })
  }

  onScroll(event) {
    if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight) {
      this.getMessages()
    }
  }

  getMessages() {
    const pageNumber = this.i;
    const queryParams = new QueryParamsModel(
      '', 'asc', '', pageNumber, 10
    );
    this.messageService.findMessages(queryParams).subscribe(res => {
      this.store.dispatch(new AddNewMessages({messages: res.data, addCount: res.data.length}));
      if (res[`data`].length > 0) {
        this.i++;
        this.cdr.detectChanges();
      }
    })
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    console.log("destroy")
    this.notifications=[]
    this.subscriptions.forEach(el => {
      if (el.sub) {
        el.sub.unsubscribe();
      }
    });
  }

  /**
   * Load Messages List
   */
  loadMessagesList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      'desc',
      'created',
    );
    // Call request from server
    this.store.dispatch(new MessagesPageRequested({page: queryParams}));
    this.selection.clear();
  }


  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};

    return filter;
  }

  /**
   * Restore state
   *
   * @param queryParams: QueryParamsModel
   * @param id: number
   */
  restoreState(queryParams: QueryParamsModel, id: number) {

    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }

    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  viewMessage(message: Message) {
    console.log('eeeeeeeeeeeeee', message.id)
    const _saveMessage = this.translate.instant('DIALOG.SAVE');
    const _messageType = message.id ? MessageType.Update : MessageType.Create;
    const dialogRef = this.dialog.open(MessageViewComponent, {width: '60vw', data: {message}});
    dialogRef.afterClosed().subscribe(res => {
      if (message.is_completed !== true) {
        this.messageService.unreadMessages(message.id).subscribe(() => {
          console.log('oooooooooooooooo', message.id)
          let notification = {} as Message
          notification = this.notifications.find(note => note.id === message.id);
          this.store.dispatch(new MessageStatusUpdated({id: notification.id, is_completed: true}))
          this.notifications.splice(notification.id, 1, notification)
          this.cdr.detectChanges();
        })
      }
    });
  }

  /**
   * Redirect to edit page
   *
   * @param id: any
   */
  editMessage(id) {
    this.router.navigate(['timeline/messages/edit/' + id]);
  }

  createMessage() {
    this.router.navigateByUrl('/timeline/messages/add');
  }

  /**
   * Check all rows are selected
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.messagesResult.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection
   */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.messagesResult.forEach(row => this.selection.select(row));
    }
  }

  getItem(type: boolean): string {
    if (!type) {
      return './assets/media/svg/icons/Design/Circle.svg';

    }
    return './assets/media/svg/icons/Code/Done-circle.svg';
  }

  getDate(created: any) {
    return new Date(created  + ' GMT').toLocaleString();
  }
}
