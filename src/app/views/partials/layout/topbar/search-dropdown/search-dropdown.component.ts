// Angular
import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {entityTypes, TagService} from '../../../../../core/services/tag-service.service';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {BehaviorSubject, forkJoin, of} from 'rxjs';
import {Router} from '@angular/router';
import {MessageViewComponent} from '../../../../pages/apps/timeline/messages/message-view/message-view.component';
import {MatDialog} from '@angular/material/dialog';
import {MeetingViewDialogComponent} from '../../../../pages/apps/timeline/meetings/meeting-view/meeting-view-dialog.component';
import {ViewCommunicationComponent} from '../../../../pages/apps/timeline/view-communication/view-communication.component';
import {ContactsPageLoaded} from '../../../../../core/lists';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../../core/reducers';
import {QueryParamsModel} from '../../../../../core/_base/crud';
import {GeneralSearchService} from '../../../../../core/services/general-search.service';
import * as _ from 'lodash';

const documents = {
  title: 'Documents',
  type: 0,
  items: [
    {
      svgPath: 'assets/media/svg/files/doc.svg',
      title: 'AirPlus Requirements',
      description: 'by Grog John'
    },
    {
      svgPath: 'assets/media/svg/files/pdf.svg',
      title: 'TechNav Documentation',
      description: 'by Mary Brown'
    },
    {
      svgPath: 'assets/media/svg/files/xml.svg',
      title: 'All Framework Docs',
      description: 'by Nick Stone'
    },
    {
      svgPath: 'assets/media/svg/files/csv.svg',
      title: 'Finance & Accounting Reports',
      description: 'by Jhon Larson'
    }
  ]
};

const members = {
  title: 'Members',
  type: 1,
  items: [
    {
      imgPath: 'assets/media/users/300_20.jpg',
      title: 'Milena Gibson',
      description: 'UI Designer'
    },
    {
      imgPath: 'assets/media/users/300_15.jpg',
      title: 'Stefan JohnStefan',
      description: 'Marketing Manager'
    },
    {
      imgPath: 'assets/media/users/300_12.jpg',
      title: 'Anna Strong',
      description: 'Software Developer'
    },
    {
      imgPath: 'assets/media/users/300_16.jpg',
      title: 'Nick Bold',
      description: 'Project Coordinator'
    }
  ]
};

const files = {
  title: 'Files',
  type: 2,
  items: [
    {
      iconClasses: 'flaticon-psd text-primary',
      title: '79 PSD files generated',
      description: 'by Grog John'
    },
    {
      iconClasses: 'flaticon2-supermarket text-warning',
      title: '$2900 worth products sold',
      description: 'Total 234 items'
    },
    {
      iconClasses: 'flaticon-safe-shield-protection text-info',
      title: '4 New items submitted',
      description: 'Marketing Manager'
    },
    {
      iconClasses: 'flaticon-safe-shield-protection text-warning',
      title: '4 New items submitted',
      description: 'Marketing Manager'
    }
  ]
};

@Component({
  selector: 'kt-search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['search-dropdown.component.scss']
})
export class SearchDropdownComponent implements OnInit {
  readonly tagsListToSearch = {
    title: 'Tags Categories',
    type: 3,
    onOpen: (type, item) => {
      item.loading = new BehaviorSubject(true);
      item.dataList = new BehaviorSubject([]);
      this.loadRelatedTagItems(type, item);

    },
    onClose: (type, item) => {

    },
    recordClicked: (record, entityType) => {

      // tslint:disable-next-line:triple-equals
      if (entityType != entityTypes.COMMUNICATION) {
        let mockAction = null
        switch (entityType) {
          case entityTypes.CONTACT :
            mockAction = new ContactsPageLoaded({
              contacts: [record.data],
              totalCount: 1,
              page: new QueryParamsModel({})
            });
            break;
        }
        this.store.dispatch(mockAction)
      }
      switch (entityType) {
        case  entityTypes.CONTACT:
          this.router.navigate(['lists/contacts/view/', record.data.id], {queryParams: {viewContext: 'CONTACT'}});
          break;
        case  entityTypes.COMMUNICATION:
          switch (record.data.type) {
            case 'FAX':
              this.dialog.open(MessageViewComponent, {data: {message: record.data}});
              break;
            case 'MEETING':
              this.dialog.open(MeetingViewDialogComponent, {data: {meeting: record.data}});
              break
            case 'EMAIL':
            case 'NOTIFICATION':
              this.dialog.open(ViewCommunicationComponent, {data: record.data});
              break;
          }
      }
    },
    items: [
      {
        svgPath: 'assets/media/svg/files/doc.svg',
        title: 'Patients',
        description: 'by Grog John',
        entityType: entityTypes.PATIENT,
        loading: of(true)
      },
      {
        svgPath: 'assets/media/svg/files/doc.svg',
        title: 'Facilities',
        description: 'by Mary Brown',
        entityType: entityTypes.FACILITY,
        loading: of(true)

      },
      {
        svgPath: 'assets/media/svg/files/doc.svg',
        title: 'Doctors',
        description: 'by Nick Stone',
        entityType: entityTypes.DOCTOR,
        loading: of(true)

      },
      {
        svgPath: 'assets/media/svg/files/doc.svg',
        title: 'Contacts',
        description: 'by Jhon Larson',
        entityType: entityTypes.CONTACT,
        loading: of(true)

      },
      {
        svgPath: 'assets/media/svg/files/doc.svg',
        title: 'Communications',
        description: 'by Nick Stone',
        entityType: entityTypes.COMMUNICATION,
        loading: of(true)
      },
    ]
  };

  // Public properties
  @Input() layout = 'dropdown';
  // Set icon class name
  @Input() icon = 'flaticon2-search-1';

  // Set true to icon as SVG or false as icon class
  @Input() useSVG: boolean;

  @Input() type: 'brand' | 'success' | 'warning' = 'success';

  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  @ViewChild('dropdown', {static: true}) dropdown;

  data: any[] = null;
  loading: boolean;
  searchText = '';
  showTagsListToSearch = false;
  form: any;
  startValue = '';
  private categoryType: entityTypes;
  filteredTags;
  isLoadingResult = false

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  constructor(private cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private tagService: TagService,
              private router: Router,
              public dialog: MatDialog,
              private generalSearchService: GeneralSearchService,
              private store: Store<AppState>
  ) {
  }

  /**
   * On init
   */
  ngOnInit(): void {
    // simulate result from API
    this.form = this.fb.group(
      {
        search: new FormControl()
      }
    );
  }

  get searchController() {
    return this.form.get('search');
  }

  /**
   * Search
   * @param e: Event
   */
  search(e) {
    const value = e.target.value;
    if (value.length <= 3) {
      return
    }
    this.isLoadingResult = true

    forkJoin({
      requestOne: this.generalSearchService.searchTasks(value),
      requestTwo: this.generalSearchService.searchProjects(value),
      requestThree: this.generalSearchService.searchCommunications(value),
      requestFour: this.generalSearchService.searchInboxes(value),
      requestFive: this.generalSearchService.countTasks(value),
    })
      .subscribe(({requestOne, requestTwo,  requestThree, requestFour, requestFive}) => {
        if (requestOne.data || requestTwo.data ||
          requestThree.data || requestFour.data || requestFive.data) {
          const result = {tasks: [], projects: [], emails: [], inboxes: [],term:'',taskCount:0}
          result.tasks = requestOne.data
          result.projects = requestTwo.data
          result.term = value
          result.emails = requestThree.data
          result.inboxes = requestFour.data
          result.taskCount = requestFive.data.open + requestFive.data.completed + requestFive.data.cancelled
          this.generalSearchService.setCurrent(result);
          this.generalSearchService.term$.next(value);
          this.router.navigateByUrl('/search/result-dashboard')
          this.isLoadingResult = false
        }
        this.cdr.detectChanges()
      });
  }

  /**
   * Clear search
   *
   * @param e: Event
   */
  clear(e) {
    this.data = null;
    this.searchInput.nativeElement.value = '';
  }

  openChange() {
    setTimeout(() => this.searchInput.nativeElement.focus());
  }

  showCloseButton() {
    return this.data && this.data.length && !this.loading;
  }

  private loadRelatedTagItems(categoryType, item) {
    this.tagService.loadRelatedTagItems(this.searchController.value.substring(1), categoryType).subscribe(result => {
      if ([entityTypes.DOCTOR, entityTypes.CONTACT, entityTypes.FACILITY, entityTypes.PATIENT].indexOf(categoryType) > -1) {
        console.log(result)
        const data = result.data.map(entity => {
          return {
            name: entity.first_name + ' ' + entity.last_name,
            description: entity.email,
            data: entity

          }
        });
        item.dataList.next(data);
        item.loading.next(false);

      } else if ([entityTypes.COMMUNICATION, entityTypes.EMAIL, entityTypes.MEETING, entityTypes.FAX].indexOf(categoryType) > -1) {
        console.log(result)
        const data = result.data.map(entity => {
          return {
            name: entity.sender_name,
            description: entity.subject,
            data: entity
          }
        });
        item.dataList.next(data);
        item.loading.next(false);

      }
    })
  }

  selectTag($event: MatAutocompleteSelectedEvent) {
    this.filteredTags = of([])

  }
}
