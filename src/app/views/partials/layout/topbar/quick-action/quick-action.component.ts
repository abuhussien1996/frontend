// Angular
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { MessageEditComponent } from 'src/app/views/pages/apps/timeline/messages/message-edit/message-edit.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SendEmailFaxComponent } from 'src/app/views/pages/apps/timeline/send-email-fax/send-email-fax.component';
import { MeetingEditComponent } from 'src/app/views/pages/apps/timeline/meetings/meeting-edit/meeting-edit.component';
import { TwixorComponent } from 'src/app/views/pages/apps/timeline/twixor/twixor.component';
import { CreateTaskComponent } from 'src/app/views/pages/calendar/create-task/create-task.component';
import { ChooseTemplateDialogComponent } from 'src/app/views/pages/calendar/choose-template/choose-template.dialog.component';

@Component({
  selector: 'kt-quick-action',
  templateUrl: './quick-action.component.html',
})
export class QuickActionComponent implements OnInit, AfterViewInit {
  // Public properties

  // Set icon class name
  @Input() icon = 'flaticon2-gear';

  // Set true to icon as SVG or false as icon class
  @Input() useSVG: boolean;

  // Set bg image path
  @Input() bgImage: string;

  // Set skin color, default to light
  @Input() skin: 'light' | 'dark' = 'light';

  /**
   * Component constructor
   */
  constructor(
    public dialog: MatDialog
  ) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * After view init
   */
  ngAfterViewInit(): void {
  }

  /**
   * On init
   */
  ngOnInit(): void {
  }

  newMeeting() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    this.dialog.open(MeetingEditComponent, dialogConfig);
  }

  newEmailFax(type) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = { communicationType: type, sendDocs: false }
    this.dialog.open(SendEmailFaxComponent, dialogConfig);
  }

  newMessage() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    this.dialog.open(MessageEditComponent, dialogConfig);
  }

  newChat() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    // dialogConfig.height = '50vw';
    // dialogConfig.width = '60vw';
    this.dialog.open(TwixorComponent, dialogConfig);
  }
  sendDocuments() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = { communicationType: 'DOCUMENT', sendDocs: true }
    this.dialog.open(SendEmailFaxComponent, dialogConfig);
  }

  createTask() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    this.dialog.open(ChooseTemplateDialogComponent, dialogConfig);
  }
}
