// Angular
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {UploadMarketingDocsComponent} from 'src/app/views/pages/apps/timeline/upload-marketing-docs/upload-marketing-docs.component';
import {LayoutConfigService} from '../../../../core/_base/layout';
import {CalendarViewComponent} from 'src/app/views/pages/calendar/calendar-view/calendar-view.component';
import {DatePipe} from '@angular/common';
import {select, Store} from '@ngrx/store';
import {currentUser, User} from '../../../../core/auth';
import {Observable} from 'rxjs';
import {AppState} from '../../../../core/reducers';
import {GlobalSettingsService} from '../../../../core/services/global-settings.service';
import {of} from 'rxjs/internal/observable/of';

@Component({
  selector: 'kt-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent implements OnInit {
  searchDisplay = true;
  notificationsDisplay = true;
  quickActionsDisplay = true;
  cartDisplay = true;
  quickPanelDisplay = true;
  languagesDisplay = true;
  userDisplay = true;
  userLayout = 'offcanvas';
  userDropdownStyle = 'light';
  meetinglDisplay = true
  messageDisplay = true
  private date: Date;
  private currentDate: string;
  user$: Observable<User>;


  constructor(
    private layoutConfigService: LayoutConfigService,
    public dialog: MatDialog,
    private store: Store<AppState>,
    public datepipe: DatePipe,
    public globalSettings: GlobalSettingsService,
    private cdr: ChangeDetectorRef
  ) {
    this.searchDisplay = this.layoutConfigService.getConfig('extras.search.display');
    this.notificationsDisplay = this.layoutConfigService.getConfig('extras.notifications.display');
    this.quickActionsDisplay = this.layoutConfigService.getConfig('extras.quick-actions.display');
    this.cartDisplay = this.layoutConfigService.getConfig('extras.cart.display');
    this.quickPanelDisplay = this.layoutConfigService.getConfig('extras.quick-panel.display');
    this.languagesDisplay = this.layoutConfigService.getConfig('extras.languages.display');
    this.userDisplay = this.layoutConfigService.getConfig('extras.user.display');
    this.userLayout = this.layoutConfigService.getConfig('extras.user.layout');
    this.userDropdownStyle = this.layoutConfigService.getConfig('extras.user.dropdown.style');
    this.currentDate = Number(this.datepipe.transform(new Date(), 'dd')).toString();
  }

  ngOnInit(): void {
    this.user$ = this.store.pipe(select(currentUser));
  }

  openCalendar() {
    if (this.dialog.openDialogs.length <= 0) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.hasBackdrop = false;
      dialogConfig.closeOnNavigation = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '60vw';
      this.dialog.open(CalendarViewComponent, dialogConfig);
    }
  }

  uploadDocs() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    this.dialog.open(UploadMarketingDocsComponent, dialogConfig);
  }

}
