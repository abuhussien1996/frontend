// Angular
import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
// Layout
import {LayoutConfigService, ToggleOptions} from '../../../core/_base/layout';
import {HtmlClassService} from '../html-class.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'kt-brand',
  templateUrl: './brand.component.html',
})
export class BrandComponent implements OnInit, AfterViewInit {
  // Public properties
  headerLogo = null;
  brandClasses = '';
  minimized: boolean;
  asideSelfMinimizeToggle = true;

  @Input() viewLogoFun;

  toggleOptions: ToggleOptions = {
    target: 'kt_body',
    targetState: 'aside-minimize',
    toggleState: 'active'
  };

  /**
   * Component constructor
   *
   * @param layoutConfigService: LayoutConfigService
   * @param htmlClassService: HtmlClassService
   */
  constructor(private layoutConfigService: LayoutConfigService, public htmlClassService: HtmlClassService,private router :ActivatedRoute
  ) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit(): void {
    this.headerLogo = this.getAsideLogo();
    this.brandClasses = this.htmlClassService.getClasses('brand', true).toString();
    this.asideSelfMinimizeToggle = this.layoutConfigService.getConfig('aside.self.minimize.toggle');
  }

  /**
   * On after view init
   */
  ngAfterViewInit(): void {
  }

  getAsideLogo() {
    return `./assets/media/logos/aplosLogo.png`;
  }

  toggleAsideClick() {
    this.minimized = !this.minimized;
    document.body.classList.toggle('aside-minimize');
    this.headerLogo = this.getAsideLogo();
    if (this.minimized)
      return this.viewLogoFun(true)
    return this.viewLogoFun(false)
  }
}
