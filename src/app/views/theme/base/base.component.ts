// Angular
import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
// RxJS
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
// Object-Path
import * as objectPath from 'object-path';
// Layout
import {LayoutConfigService, MenuConfigService, PageConfigService, SplashScreenService, SubheaderService} from '../../../core/_base/layout';
import {HtmlClassService} from '../html-class.service';
import {LayoutConfig} from '../../../core/_config/layout.config';
import {MenuConfig} from '../../../core/_config/menu.config';
import {PageConfig} from '../../../core/_config/page.config';
// User permissions
import {NgxPermissionsService} from 'ngx-permissions';
import {AuthService, currentUser, currentUserPermissions, Permission} from '../../../core/auth';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../core/reducers';
import {WorkflowService} from '../../pages/workflow/_services/workflow.service';
import {filter, take} from 'rxjs/operators';
import {CommunicationService} from '../../../core/services/communication.service';

@Component({
  selector: 'kt-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BaseComponent implements OnInit, OnDestroy {
  // Public variables
  selfLayout = 'default';
  asideSelfDisplay: true;
  contentClasses = '';
  contentContainerClasses = '';
  subheaderDisplay = true;
  contentExtended: false;
  // Private properties
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
  private currentUserPermissions$: Observable<Permission[]>;
  userLoaded = new BehaviorSubject(false);


  /**
   * Component constructor
   *
   * param layoutConfigService: LayoutConfigService
   * param menuConfigService: MenuConfigService
   * param pageConfigService: PageConfigService
   * param htmlClassService: HtmlClassService
   * param store
   * param permissionsService
   */
  constructor(
    private layoutConfigService: LayoutConfigService,
    private menuConfigService: MenuConfigService,
    private pageConfigService: PageConfigService,
    private htmlClassService: HtmlClassService,
    private store: Store<AppState>,
    private workflowService: WorkflowService,
    private permissionsService: NgxPermissionsService,
    private cdr: ChangeDetectorRef,
    public subheaderService: SubheaderService,
    private authService: AuthService,
    private splashScreenService: SplashScreenService,
    private communicationService: CommunicationService
  ) {
    this.loadRolesWithPermissions();

    // register configs by demos
    this.layoutConfigService.loadConfigs(new LayoutConfig().configs);
    this.menuConfigService.loadConfigs(new MenuConfig().configs);
     this.store.pipe(select(currentUser),filter(x => x.companyID !== undefined && x.companyID !== null), take(1)).subscribe(res => {
         this.loadSubMenus();
     });
    this.pageConfigService.loadConfigs(new PageConfig().configs);

    // setup element classes
    this.htmlClassService.setConfig(this.layoutConfigService.getConfig());

    const subscription = this.layoutConfigService.onConfigUpdated$.subscribe(layoutConfig => {
      // reset body class based on global and page level layout config, refer to html-class.service.ts
      document.body.className = '';
      this.htmlClassService.setConfig(layoutConfig);
    });
    this.unsubscribe.push(subscription);

    this.authService.getUser().subscribe(value => {
      if(value && value.id){
          this.userLoaded.next(true);
          this.splashScreenService.hide();
        }
    })
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit(): void {
    const config = this.layoutConfigService.getConfig();
    // Load UI from Layout settings
    this.selfLayout = objectPath.get(config, 'self.layout');
    this.asideSelfDisplay = objectPath.get(config, 'aside.self.display');
    this.subheaderDisplay = objectPath.get(config, 'subheader.display');
    this.contentClasses = this.htmlClassService.getClasses('content', true).toString();
    this.contentContainerClasses = this.htmlClassService.getClasses('content_container', true).toString();
    this.contentExtended = objectPath.get(config, 'content.extended');

    // let the layout type change
    const subscription = this.layoutConfigService.onConfigUpdated$.subscribe(cfg => {
      setTimeout(() => {
        this.selfLayout = objectPath.get(cfg, 'self.layout');
      });
    });
    this.unsubscribe.push(subscription);
    this.unsubscribe.push(this.subheaderService.breadcrumbs$.subscribe(bc => {
      if (bc.length>0 ){
        // page: "/data-analysis/page-builder"
        const breadcrumbs = bc[0].page;
        const configUpdate = this.layoutConfigService.getConfig();
        if (breadcrumbs.indexOf('data-analysis')!==-1){
          objectPath.set(configUpdate, 'subheader.display',false);
          this.subheaderDisplay = objectPath.get(configUpdate, 'subheader.display');
          this.layoutConfigService.setConfig(configUpdate, true);
          return;
        }
        if (!this.subheaderDisplay){
          objectPath.set(configUpdate, 'subheader.display',true);
          this.subheaderDisplay = objectPath.get(configUpdate, 'subheader.display');
          this.layoutConfigService.setConfig(configUpdate, true);
        }
      }
    }));
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this.unsubscribe.forEach(sb => sb.unsubscribe());
    // https://www.npmjs.com/package/ngx-permissions
    this.permissionsService.flushPermissions();
  }

  /**
   * NGX Permissions, init roles
   */
  loadRolesWithPermissions() {
    this.currentUserPermissions$ = this.store.pipe(select(currentUserPermissions));
    this.unsubscribe.push(this.currentUserPermissions$.subscribe(res => {
      if (!res || res.length === 0) {
        return;
      }

      this.permissionsService.flushPermissions();
      res.forEach((pm: Permission) => this.permissionsService.addPermission(pm.name));
      if (res.some(
        p => p.name === 'accessToContactModule'
          || p.name === 'accessToDoctorModule'
          || p.name === 'accessToPatientModule'
          || p.name === 'accessToFacilityModule'
      )) {
        this.permissionsService.addPermission('accessToEntityModule')
      }
      if (res.some(
        p => p.name === 'accessToPharmacyModule'
          || p.name === 'accessToPharmacyGroupModule'
      )) {
        this.permissionsService.addPermission('accessToPharmacyMenu')
      }
      if (res.some(
        p => p.name === 'accessToDrugModule'
          || p.name === 'accessToDrugGroupModule'
      )) {
        this.permissionsService.addPermission('accessToDrugMenu')
      }
      if (res.some(
        p => p.name === 'accessToAffiliationModule'
          || p.name === 'accessToAffiliationTypeModule'
          || p.name === 'accessToClientModule'
          || p.name === 'accessToInsuranceModule'
          || p.name === 'accessToCategoryModule'
          || p.name === 'accessToTagsModule'
        ) || this.permissionsService.getPermission('accessToEntityModule')
        || this.permissionsService.getPermission('accessToPharmacyMenu')
        || this.permissionsService.getPermission('accessToDrugMenu')) {
        this.permissionsService.addPermission('accessToListsModule')
      }
      if (res.some(
        p => p.name === 'accessToRoleModule'
          || p.name === 'accessToUserManagementModule'
      )) {
        this.permissionsService.addPermission('accessToUserManagementMenu')
      }
      if (res.some(
        p => p.name === 'accessToOfficeModule'
          || p.name === 'accessToCompanyModule'
      ) || this.permissionsService.getPermission('accessToUserManagementMenu')) {
        this.permissionsService.addPermission('accessToManagementModule')
      }
      if (res.some(
        p => p.name === 'accessToWorkflowModule'
          || p.name === 'accessToApiModule'
          || p.name === 'accessToActionModule'
          || p.name === 'accessToEventModule'
      )) {
        this.permissionsService.addPermission('accessToEventsModule')
      }
      if (res.some(
        p => p.name === 'accessToProgramModule'
          || p.name === 'accessToProgramParticipantModule'
      )) {
        this.permissionsService.addPermission('accessToProgramMenu')
      }
      if (res.some(
        p => p.name === 'accessToDeviceModule'
          || p.name === 'accessToDeviceTypeModule'
          || p.name === 'accessToDeviceInventoryModule'
      )) {
        this.permissionsService.addPermission('accessToDeviceMenu')
      }
      if (res.some(
        p => p.name === 'canAddLink'
          || p.name === 'canEditLink'
          || p.name === 'canSoftDeleteLink'
      )) {
        this.permissionsService.addPermission('accessToLinkManagement')
      }
      if (this.permissionsService.getPermission('accessToProgramMenu')
        || this.permissionsService.getPermission('accessToDeviceMenu')) {
        this.permissionsService.addPermission('accessToRemoteMonitoringModule')
      }
      if (this.permissionsService.getPermission('accessToKnostosBuilders')
        || this.permissionsService.getPermission('accessToPrivateBuilders')) {
        this.permissionsService.addPermission('accessToAnalysisBuilders')
      }
      localStorage.setItem('currentUserPermissions', JSON.stringify(this.permissionsService.getPermissions()));
    }));
  }

  private loadSubMenus() {
    const menuConfig = new MenuConfig();
    if (
      this.permissionsService.getPermission('canViewLinks')
      || JSON.parse(localStorage.getItem('currentUserPermissions'))[`canViewLinks`]
    ) {
      // this.store.dispatch(new LinksRequested());
    }
    this.workflowService.loadAllWorkflows().subscribe((res) => {
      menuConfig.workflowMenu.submenu = [];
      res.data.map((sub) => {
        menuConfig.workflowMenu.submenu.push(
          {
            permission: '', translate: '',
            title: sub.name,
            page: '/workflows/' + sub.id
          }
        )
      })
      this.menuConfigService.loadConfigs(menuConfig.configs);
    });
    this.communicationService.loadAllInboxes().subscribe((res) => {
      menuConfig.inboxMenu.submenu = [];
      res.data.map((sub) => {
        menuConfig.inboxMenu.submenu.push(
          {
            permission: '', translate: '',
            title: sub.name,
            page: '/events/inbox/' + sub.id
          }
        )
      })
      this.menuConfigService.loadConfigs(menuConfig.configs);
    });
  }
}
