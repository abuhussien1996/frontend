import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { InboxItem, InboxService } from 'src/app/core/events';
import { CommunicationsService } from 'src/app/core/timeline/_services';
import { LayoutUtilsService, MessageType } from 'src/app/core/_base/crud';

@Component({
    selector: 'kt-reply-dialog',
    templateUrl: 'reply-dialog.component.html',
})
export class ReplyDialogComponent implements OnInit {
    index = 0;
    item: InboxItem;
    form: FormGroup;
    files = [];
    showFileLoader = new BehaviorSubject(false);
    totalFileSize = 1;
    uploadRequests: any[];
    private callback;
    @ViewChild('fileInput') fileInput: ElementRef;

    ngOnInit() {
        this.item = this.data.inboxItem;
        this.createForm();
    }

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
		public dialog: MatDialog,
        public dialogRef: MatDialogRef<ReplyDialogComponent>,
        private fb: FormBuilder,
        private inboxService: InboxService,
        private communicationsService: CommunicationsService,
		private layoutUtilsService: LayoutUtilsService,
		private translate: TranslateService
    ) { }

    createForm() {
        this.form = this.fb.group({
            attachments: [[]],
            body: [null, Validators.required]
        });
    }

    onFileChange(event) {
      this.form.get('attachments').setValue([]);
      this.files = [];
      for (const file of event.target.files) {
        this.form.get('attachments').value.push(file.name);
        this.files.push(file);
      }
    }

    resetFileInput() {
      this.form.get('attachments').setValue([]);
      this.files = [];
      this.fileInput.nativeElement.value = [];
    }

    reply(mode) {
        this.inboxService.reply(this.item, this.form.get('body').value, this.form.get('attachments').value, mode).subscribe((res) => {
            if(res.data) {
                if(res.data.pre_signed_urls.length > 0) {
                    const obs = [];
                    const uuids = res.data.pre_signed_urls.map(x => x.uuid);
                    // tslint:disable-next-line:prefer-for-of
                    for (let i = 0; i < this.files.length; i++) {
                      obs.push(this.uploadFile(res.data.pre_signed_urls[i].url, this.files[i]));
                      this.totalFileSize += this.files[i].size;
                    }
                    this.uploadRequests = obs;
                    this.callback = this.postReplyAttachments.bind(this, res.data.id, uuids, mode);
                    this.showFileLoader.next(true);
                } else {
                    this.layoutUtilsService.showActionNotification(
                        this.translate.instant('INBOX.REPLY_SENT'),
                        MessageType.Read,
                        3000,
                        true,
                        false
                    );
                    this.dialogRef.close();
                }
            }
        })
    }

    uploadFile(url, file) {
      return this.communicationsService.uploadFiles(url, file);
    }

    postReplyAttachments(replyId, uuids, mode) {
      this.inboxService.postReplyAttachments(this.item, replyId, uuids, mode).subscribe((res) => {
        if (res === null) {
            this.layoutUtilsService.showActionNotification(
                this.translate.instant('INBOX.REPLY_SENT'),
                MessageType.Read,
                3000,
                true,
                false
            );
          this.dialogRef.close();
        }
      });
    }

    callbackOnFileLoad() {
      this.callback();
    }
}