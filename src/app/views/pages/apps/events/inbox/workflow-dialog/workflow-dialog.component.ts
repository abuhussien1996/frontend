import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { skip } from 'rxjs/operators';
import { currentUser } from 'src/app/core/auth';
import {
    InboxActionTypes,
    InboxItem,
    selectInboxAttachments,
    selectInboxError,
    selectInboxLatestSuccessfullAction,
    selectInboxWorkflow,
    StartWorkflow
} from 'src/app/core/events';
import { AppState } from 'src/app/core/reducers';
import { FormioComponent } from 'angular-formio';
import { LayoutUtilsService, MessageType } from 'src/app/core/_base/crud';

@Component({
    selector: 'kt-workflow-dialog',
    templateUrl: 'workflow-dialog.component.html',
	styleUrls: ['../../events.component.scss']
  })
  export class WorkflowDialogComponent implements OnInit, OnDestroy {
    index = 0;
    item: InboxItem;
    itemWorkflow: any;
    attachments: Array<any>;
    formSchema: any;
    formData: any;
    submissionData: any;
    isSubmitting: boolean;
    subscriptions: Subscription[] = [];
    newPatient;
    currentUser: any;
    jump: boolean;

    @ViewChild('formio', { static: false }) workflowForm: FormioComponent;

    ngOnInit() {
      this.item = this.data.inboxItem;
      this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe(res => this.currentUser = res));
      this.subscriptions.push(this.store.pipe(select(selectInboxWorkflow)).subscribe(res => {
          this.itemWorkflow = res;
          this.formSchema = JSON.parse(this.itemWorkflow.form.schema);
          this.formData = this.itemWorkflow.input_data !== null ? {data: JSON.parse(this.itemWorkflow.input_data)} : {data: {}};
          if(this.itemWorkflow.workflow_id === 121) {
            this.formData.data[`dateTime`] = this.item.created;
            this.formData.data[`sender_name`] = this.item.sender_name;
            this.formData.data[`subject`] = this.item.subject;
            this.formData.data[`status`] = this.item.status;
            this.formData.data[`body`] = this.item.body;
            this.formData.data[`type`] = this.item.type;
            this.formData.data[`assigned_user`] = this.currentUser;
          }
        }));
        this.subscriptions.push(this.store.pipe(select(selectInboxAttachments)).subscribe(res =>
        this.attachments = res.filter(a => a.uuid.endsWith('.pdf'))
        ));
        this.subscriptions.push(this.store.pipe(select(selectInboxError), skip(1)).subscribe((res) => {
          if (res) {
              this.isSubmitting = false;
          }
      }));
      this.subscriptions.push(this.store.pipe(select(selectInboxLatestSuccessfullAction), skip(1)).subscribe((res) => {
          if (res && res.action_type === InboxActionTypes.StartWorkflow) {
              this.dialogRef.close();
          }
      }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<WorkflowDialogComponent>,
      private store: Store<AppState>,
      public dialog: MatDialog,
      private layoutUtilsService: LayoutUtilsService
      ) {}

    onChange(event) {
        if (event.data) {
            this.submissionData = event.data;
        }
    }

    startWorkflow(jump) {
        this.isSubmitting = true;
        if(this.workflowForm.formio.checkValidity()) {
          this.store.dispatch(new StartWorkflow({
              item: this.item,
              itemWorkflow: this.itemWorkflow,
              attachments: this.attachments,
              submissionData: JSON.stringify(this.submissionData),
              groupId: this.data.groupId,
              jump
          }));
        } else {
          this.isSubmitting = false;
          const errMsg = 'Invalid Form Submission. Please check before submitting again';
          this.layoutUtilsService.showActionNotification(errMsg, MessageType.Update, 3000, false, false);
        }
    }

    Next() {
      if(this.index < this.attachments.length) {
          this.index++;
      }
    }

    Previous() {
        if(this.index > 0) {
            this.index--;
        }
    }

    handleFormEvent(event) {
        switch (event.type) {
          // case 'addPatient':
          //   this.openPatientAddDialog();
          //   break;
          // case 'addPatientInsurance':
          //   this.openPatientInsuranceAddDialog();
          //   break;
          default:
            break;
        }
    }

    // openPatientAddDialog() {
    //     const dialogRef = this.dialog.open(
    //         CreatePatientDialogComponent,
    //         {
    //             width: '40vw',
    //             disableClose: true,
    //             hasBackdrop: false
    //         });
    //     dialogRef.afterClosed().subscribe(result => {
    //         if(result && result.added) {
    //             this.formData.data[`patient`] = this.newPatient;
    //             this.formData = {data: this.formData.data};
    //         }
    //     });
    // }

    // openPatientInsuranceAddDialog() {
    //   if(this.formData.data[`patient`] && this.formData.data[`patient`].id > 0) {
    //     const dialogConfig = new MatDialogConfig();
    //     dialogConfig.autoFocus = true;
    //     dialogConfig.width = '40vw';
    //     dialogConfig.disableClose = true;
    //     dialogConfig.hasBackdrop = false;
    //     dialogConfig.data = {
    //         isCreate: true,
    //         patientId: this.formData.data[`patient`].id,
    //         isReadOnly: false
    //     }
    //     this.dialog.open(AddInsuranceDialogComponent, dialogConfig);
    //   }
    // }
  }
