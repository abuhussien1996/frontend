import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {merge, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {LayoutUtilsService, MessageType, QueryParamsModel} from '../../../../../../core/_base/crud';
import {
  AttachmentsRequested,
  InboxActionTypes,
  InboxDataSource,
  InboxItem,
  InboxRequested,
  InboxService,
  ItemUpdated,
  selectInboxError,
  selectInboxLatestSuccessfullAction,
  selectInboxPageLastQuery,
  selectLatestCardValues
} from '../../../../../../core/events';
import {TranslateService} from '@ngx-translate/core';
import {FormControl} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {MailViewDialogComponent} from '../mail-view-dialog/mail-view-dialog.component';
import {currentUser} from '../../../../../../core/auth';
import {Update} from '@ngrx/entity';
import {ConfirmActionDialogComponent} from '../../../../../partials/content/crud';
import {CreateTaskComponent} from '../../../../calendar/create-task/create-task.component';

@Component({
  selector: 'kt-inbox-view',
  templateUrl: './inbox-view.component.html',
  styleUrls: ['./inbox-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InboxViewComponent implements OnInit, OnDestroy {
  dataSource: any = new InboxDataSource(this.store);
  displayedColumns = ['created', 'type', 'sender_name', 'status', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterType = '';
  fromDate = new FormControl();
  toDate = new FormControl();
  parent = this;
  lastQuery: QueryParamsModel;
  currentItem: InboxItem;
  inboxResult: InboxItem[] = [];
  private subscriptions: Subscription[] = [];
  user: any;
  emailFilter = null;

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private datePipe: DatePipe,
    private inboxService: InboxService,
    private router: Router
  ) {
    this.store.pipe(select(currentUser)).subscribe((res) => {
      this.user = res
    });

  }

  ngOnInit() {
    const routerEventsSubscription = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.loadInbox()
    })
    this.subscriptions.push(routerEventsSubscription)
    const routeSub = this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id && id > 0) {
        this.emailFilter = id;
      }
    });
    const routeSub2 = this.activatedRoute.data.subscribe(data => {
      if (data && data.filter) {
        this.filterType = data.filter;
      }
    });
    this.subscriptions.push(routeSub);
    this.subscriptions.push(routeSub2);
    const currentUser$ = this.store.pipe(select(currentUser)).subscribe((res) => {
      if (!res) {
        return
      }
      setTimeout(() => {
        currentUser$.unsubscribe();
      })
      this.user = res
      console.log(this.user)
      this.loadInbox();
      const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
      this.subscriptions.push(sortSubscription);
      const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
        tap(() => this.loadInbox())
      ).subscribe();
      this.subscriptions.push(paginatorSubscriptions);
      this.dataSource = new InboxDataSource(this.store);
      const entitiesSubscription = this.dataSource.entitySubject.pipe(
        skip(1),
        distinctUntilChanged()
      ).subscribe(res => {
        this.inboxResult = res;
      });
      this.subscriptions.push(entitiesSubscription);
      const lastQuerySubscription = this.store.pipe(select(selectInboxPageLastQuery)).subscribe(res => this.lastQuery = res);
      this.subscriptions.push(lastQuerySubscription);
      const errorSubscription = this.store.pipe(select(selectInboxError), skip(1)).subscribe((res) => {
        if (res) {
          this.layoutUtilsService.showActionNotification(
            this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
            MessageType.Update,
            3000,
            true,
            false
          );
        }
      });
      this.subscriptions.push(errorSubscription);
      const successSubscription = this.store.pipe(select(selectInboxLatestSuccessfullAction), skip(1)).subscribe((res) => {
        if (res && res.action_type) {
          switch (res.action_type) {
            case InboxActionTypes.StartWorkflow:
              this.layoutUtilsService.showActionNotification(
                this.translate.instant('INBOX.WORKFLOW_SUCCESS'),
                MessageType.Update,
                3000,
                true,
                false
              );
              this.loadInbox();
              break;
            case InboxActionTypes.AttachmentsLoaded:
              this.openMailViewDialog();
              break;
            default:
              break;
          }
        }
      });
      this.subscriptions.push(successSubscription);
      this.subscriptions.push(this.store.pipe(select(selectLatestCardValues), skip(1)).subscribe((res) => {
        if (res && res.workflow_queue_id !== null) {
          this.router.navigate([`/workflows/${res.group_id}`]);
        }
      }));
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loadInbox() {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction !== '' ? this.sort.direction : 'desc',
      !this.sort.active || this.sort.active === '' ? 'created' : this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    this.store.dispatch(new InboxRequested({page: queryParams}));
  }

  filterConfiguration(): any {
    const filter: any = {};
    filter.inbox_id = this.emailFilter
    filter.type = this.filterType === 'EMAIL' ? ['EMAIL', 'MANUAL'] : [this.filterType];
    filter.from = this.datePipe.transform(this.fromDate.value, 'yyyy-MM-dd');
    filter.to = this.datePipe.transform(this.toDate.value, 'yyyy-MM-dd');
    filter.domain = this.searchInput.nativeElement.value;
    return filter;
  }

  viewMail(inboxItem: InboxItem) {
    this.currentItem = inboxItem;
    this.inboxService.getLatestInboxUpdate(inboxItem).subscribe(res => {
      // tslint:disable-next-line:triple-equals
      inboxItem = res.data;
      if (inboxItem.locked && this.user.userName !== inboxItem.locked_by) {
        const partialItem: Update<InboxItem> = {
          id: inboxItem.id,
          changes: {locked: true, locked_by: inboxItem.locked_by}
        };
        this.store.dispatch(
          new ItemUpdated({
            item: this.currentItem,
            partialItem
          })
        );
        return;
      }
      // tslint:disable-next-line:triple-equals
      if (this.user.userName != inboxItem.locked_by)
        this.inboxService.lockInbox(inboxItem, true).subscribe();
      this.requestAttachments();
    })
  }

  requestAttachments() {
    this.store.dispatch(new AttachmentsRequested({
        itemId: this.currentItem.id,
        contactId: this.currentItem.sender_id
      })
    );
  }

  openMailViewDialog() {
    const dialogRef = this.dialog.open(MailViewDialogComponent, {
      data: {inboxItem: this.currentItem},
      width: '60vw'
    });

    dialogRef.afterClosed().subscribe(res => {
      this.inboxService.lockInbox(this.currentItem, false).subscribe();
      if (res && res.reloadInbox) {
        this.loadInbox();
      }
    })
  }

  startFilter(d: Date | null): boolean {
    const date = (d || new Date());
    if (this.toDate && this.toDate.value) {
      return date.getTime() < this.toDate.value.getTime();
    }
    return true;
  }

  toFilter(d: Date | null): boolean {
    const date = (d || new Date());
    if (this.fromDate && this.fromDate.value) {
      return date.getTime() > this.fromDate.value.getTime();
    }
    return true;
  }

  getItemCssClassByStatus(status): string {
    switch (status) {
      case 'PENDING':
        return 'warning';
      case 'IN_PROGRESS':
        return 'success';
      case 'DONE':
        return 'primary';
    }
  }

  getItemTextByStatus(status): string {
    switch (status) {
      case 'PENDING':
        return 'Pending';
      case 'IN_PROGRESS':
        return 'In Progress ';
      case 'DONE':
        return 'Done';
    }
  }

  unlockInbox(inboxItem) {
    const dialogRef = this.dialog.open(ConfirmActionDialogComponent, {
      data: {title: 'Unlock Mail', description: 'Are you sure to force unlock the mail?', waitDesciption: 'Unlocking ...'},
      width: '440px'
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.inboxService.lockInbox(inboxItem, false).subscribe(() => {
        const partialItem: Update<InboxItem> = {
          id: inboxItem.id,
          changes: {locked: false, locked_by: null}
        };
        this.store.dispatch(
          new ItemUpdated({
            item: inboxItem,
            partialItem
          })
        );
        setTimeout(() => {
          const newInbox = {...inboxItem}
          newInbox.locked_by = null;
          this.viewMail(newInbox);
        }, 30)
      });
    })
  }

  createTask(selectedNote) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = {
      mode: selectedNote.task ? 'update' : 'create',
      task: selectedNote.task ? selectedNote.task : null,
      selectedNote,
      category: null,
      entityId: selectedNote.sender_id,
      entityType: '',
      start_time: selectedNote.created
    }
    const dialogRef = this.dialog.open(CreateTaskComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.task && result.task[0]){
        this.inboxService.updateInbox(selectedNote,result.task[0].id).subscribe(() =>{
          this.loadInbox()
        })
      }
    })
  }

}
