import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {ContactsService} from 'src/app/core/lists/_services';
import {TranslateService} from '@ngx-translate/core';
import {CommunicationsService} from 'src/app/core/timeline/_services';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud/utils/layout-utils.service';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {currentUser} from 'src/app/core/auth';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'kt-upload-manual',
  templateUrl: './upload-manual.component.html',
})
export class UploadManualComponent implements OnInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  recipientCtrl = new FormControl();
  topicCtrl = new FormControl();
  filteredRecipients: Observable<any[]>;
  filteredTopics: Observable<any[]>;
  recipients = [];
  topics = [];
  communicationType;
  files = [];
  failed: string[] = [];
  attachments: string[] = [];
  senderName: string;
  hasFormErrors: boolean;
  emailFaxForm: FormGroup;
  sendDocs: boolean;

  @ViewChild('recipientInput') recipientInput: ElementRef<HTMLInputElement>;
  @ViewChild('topicInput') topicInput: ElementRef<HTMLInputElement>;
  @ViewChild('fileInput') fileInput: ElementRef;
  showFileLoader = new BehaviorSubject(false);
  totalFileSize = 1;
  uploadRequests: any[];
  clicked = false
  callback;


  ngOnInit() {
    this.communicationType = this.data.communicationType;
    this.sendDocs = this.data.sendDocs;
    this.createForm();
  }

  constructor(
    public dialogRef: MatDialogRef<UploadManualComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private emailFaxFB: FormBuilder,
    private contactsService: ContactsService,
    private communicationsService: CommunicationsService,
    private translate: TranslateService,
    private layoutUtilsService: LayoutUtilsService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    public dialog: MatDialog
  ) {
    this.recipientCtrl.valueChanges
      .pipe(
        switchMap(
          value => value && value.length >= 3 ? this.contactsService.searchContacts(value) : of([])
        )
      )
      .subscribe(result => {
        this.filteredRecipients = result.data ? this.filter(result.data) : of([]);
        this.cdr.detectChanges();
      });
    this.topicCtrl.valueChanges
      .pipe(
        switchMap(
          value => value ? this.communicationsService.getDocs(value) : of([])
        )
      )
      .subscribe(result => {
        this.filteredTopics = result.data ? of(result.data) : of([]);
        this.cdr.detectChanges();
      });
    this.store.pipe(select(currentUser)).subscribe(res => this.senderName = res && res.fullName ? res.fullName : '');
  }




  filter(arr) {
    return of(arr.filter((objA) => {
      return !this.recipients.find((objB) => {
        return objA.id === objB.id
      })
    }));
  }

  onFileChange(event) {
    this.attachments = [];
    this.files = [];
    for (const file of event.target.files) {
      const split = file.name.split('.')
      const fileType = split.pop()
      if (fileType === 'pdf') {
        this.attachments.push(file.name);
        this.files.push(file);
      }
      else {
        this.resetFileInput()
        this.layoutUtilsService.showActionNotification('Please upload only pdf files', MessageType.Update, 4000, true, false);
      }
    }
  }

  sendA() {
    const controls = this.emailFaxForm.controls;
    const request = {
      body: {
        subject: controls.subject.value,
        body: controls.body.value,
        attachment_names: this.attachments,
        type: 'MANUAL'
      }
    }
    this.communicationsService.uploadManual(request).subscribe((res) => {
      if (res.data) {
        if (this.files.length === 0) {
          // tslint:disable-next-line:max-line-length
          this.layoutUtilsService.showActionNotification(this.translate.instant('EMAIL_FAX.SUCCESS'), MessageType.Update, 3000, false, false);
          this.dialogRef.close();
        }
        const obs = [];
        const uuids = res.data.pre_signed_urls.map(x => x.uuid);
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.files.length; i++) {
          obs.push(this.communicationsService.uploadFiles(res.data.pre_signed_urls[i].url, this.files[i]));
          this.totalFileSize += this.files[i].size;
        }
        this.uploadRequests = obs;
        this.callback = this.sendFiles.bind(this, res.data.id, uuids);
        this.showFileLoader.next(true);
      }
    })
  }


  upload(url, file) {
    return this.communicationsService.uploadFiles(url, file);
  }

  sendFiles(id, uuids) {
    this.communicationsService.received_communication(id, uuids).subscribe((res) => {
      if (res === null) {
        this.layoutUtilsService.showActionNotification(this.translate.instant('EMAIL_FAX.SUCCESS'), MessageType.Update, 3000, false, false);
        this.dialogRef.close({
          done: true,
        });
      } else {
        // tslint:disable-next-line:max-line-length
        this.layoutUtilsService.showActionNotification(this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'), MessageType.Update, 3000, false, false);
      }
    });
  }

  getTitle(): string {
    return this.translate.instant('TABLE.UPLOAD_MANUAL')
  }

  close() {
    this.dialogRef.close();
  }

  resetFileInput() {
    this.attachments = [];
    this.files = [];
    this.fileInput.nativeElement.value = [];
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.emailFaxForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  createForm() {
    this.emailFaxForm = this.emailFaxFB.group({
      subject: ['', Validators.required],
      body: ['']
    });
  }

  submit() {
    this.hasFormErrors = false;
    const controls = this.emailFaxForm.controls;
    /** check form */
    if (this.emailFaxForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    this.clicked = true;
    this.sendA();
  }

  callbackOnFileLoad() {
    this.callback();
  }


}
