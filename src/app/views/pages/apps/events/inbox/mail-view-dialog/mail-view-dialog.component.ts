import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {select, Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {skip} from 'rxjs/operators';
import {InboxActionTypes, InboxItem, selectInboxAttachments, selectInboxLatestSuccessfullAction} from 'src/app/core/events';
import {AppState} from 'src/app/core/reducers';
import {ReplyDialogComponent} from '../reply-dialog/reply-dialog.component';

@Component({
  selector: 'kt-mail-view-dialog',
  templateUrl: 'mail-view-dialog.component.html',
  styleUrls: ['../../events.component.scss']
})
export class MailViewDialogComponent implements OnInit, OnDestroy {
  index = 0;
  item: InboxItem;
  form: FormGroup;
  attachments: Array<any>;
  hideAttachments: boolean;
  selectedGroupId: number;
  subscriptions: Subscription[] = [];
  typeOfInbox

  ngOnInit() {
    this.item = this.data.inboxItem;
    this.typeOfInbox = this.item.type
    const attachmentsSub = this.store.pipe(select(selectInboxAttachments)).subscribe(res => {
        this.attachments = res.filter(a => a.uuid.endsWith('.pdf'));
        this.createForm();
      }
    );
    this.subscriptions.push(attachmentsSub);
    const successSubscription = this.store.pipe(select(selectInboxLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        switch (res.action_type) {
          case InboxActionTypes.ItemWorkflowLoaded:
            this.hideAttachments = true;
            this.dialogRef.close();
            break;
          default:
            break;
        }
      }
    });
    this.subscriptions.push(successSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<MailViewDialogComponent>,
    private store: Store<AppState>,
    private fb: FormBuilder
  ) {
  }

  Next() {
    if (this.index < this.attachments.length) {
      this.index++;
    }
  }

  Previous() {
    if (this.index > 0) {
      this.index--;
    }
  }

  close() {
    this.dialogRef.close({reloadInbox: true});
  }

  createForm() {
    this.form = this.fb.group({
      sender_name: [this.item.sender_name],
      created: [this.item.created + 'UTC'],
      body: [this.item.body]
    });
  }


  reply() {
    this.hideAttachments = true;
    this.dialog.open(ReplyDialogComponent, {data: {inboxItem: this.item}}).afterClosed().subscribe(() => {
      this.hideAttachments = false;
    });
  }
}
