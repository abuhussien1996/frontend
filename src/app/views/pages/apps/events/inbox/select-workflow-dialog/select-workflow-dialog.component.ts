import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { InboxService } from 'src/app/core/events';

@Component({
  selector: 'kt-select-workflow-dialog',
  templateUrl: 'select-workflow-dialog.component.html',
})
export class SelectWorkflowDialogComponent implements OnInit {
  workflowInput: FormControl;
  workflowList = [];
  workflowGroupList = [];
  workflowGroupInput: FormControl;

  ngOnInit() {
    this.workflowInput = new FormControl();
    this.workflowGroupInput = new FormControl();
    this.inboxService.getWorkflowGroups().subscribe(result => {
      this.workflowGroupList = result.data;
      this.cdr.detectChanges();
    });
  }

  constructor(
    public dialogRef: MatDialogRef<SelectWorkflowDialogComponent>,
    private inboxService: InboxService,
    private cdr: ChangeDetectorRef
  ) { }

  workflowGroupSelected() {
    this.inboxService.getWorkflows(this.workflowGroupInput.value.id).subscribe(result => {
      this.workflowList = result.data;
      this.cdr.detectChanges();
    });
    this.workflowInput.setValue(null);
  }

  displayFn(obj: any): string {
    return obj && obj.name ? obj.name : '';
  }
}