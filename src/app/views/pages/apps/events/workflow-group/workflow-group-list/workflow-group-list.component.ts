import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {distinctUntilChanged, skip} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {LayoutUtilsService, MessageType} from '../../../../../../core/_base/crud';
import {EventActionActionTypes} from '../../../../../../core/events';
import {TranslateService} from '@ngx-translate/core';
import {CompanyModel} from '../../../../../../core/management';
import {
  selectWorkflowGroupError,
  selectWorkflowGroupLatestSuccessfulAction
} from '../../../../../../core/events/_selectors/workflow-group.selectors';
import {EventWorkflowGroup} from '../../../../../../core/events/_models/workflow-group.model';
import {WorkflowGroupDatasource} from '../../../../../../core/lists/_data-sources/workflow-group.datasource';
import {WorkflowGroupOnServerDeleted, WorkflowGroupPageRequested} from '../../../../../../core/events/_actions/workflow-group.action';
import {WorkflowGroupEditComponent} from '../workflow-group-edit/workflow-group-edit.component';

@Component({
  selector: 'kt-workflow-group-list',
  templateUrl: './workflow-group-list.component.html',
  styleUrls: ['./workflow-group-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class WorkflowGroupListComponent implements OnInit {
  dataSource: WorkflowGroupDatasource;
  displayedColumns = ['name', 'description', 'actions'];
  users: any[];
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  private subscriptions: any[] = [];
  filterCompany = '';
  companies: CompanyModel[];
  private companyId: any;
  workflowGroupResult: EventWorkflowGroup[] = [];

  constructor(public dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private layoutUtilsService: LayoutUtilsService,
              private store: Store<AppState>,
              private translate: TranslateService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.store.dispatch(new WorkflowGroupPageRequested());

    const errorSubscription = this.store.pipe(select(selectWorkflowGroupError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
        );
      }
    });
    this.subscriptions.push(errorSubscription)
    const successSubscription = this.store.pipe(select(selectWorkflowGroupLatestSuccessfulAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch (res.action_type) {
          case EventActionActionTypes.EventActionCreated :
            successMsg = this.translate.instant('WORKFLOW_GROUP.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case EventActionActionTypes.EventActionUpdated :
            successMsg = this.translate.instant('WORKFLOW_GROUP.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
        }
        this.layoutUtilsService.showActionNotification(
          successMsg,
          msgType,
          3000,
          true,
          false
        );
      }
    });
    this.subscriptions.push(successSubscription)

    this.dataSource = new WorkflowGroupDatasource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.workflowGroupResult = res;
    });

    this.subscriptions.push(entitiesSubscription);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  addWorkflowGroup() {
    const workflowGroupObj: EventWorkflowGroup = new EventWorkflowGroup();
    this.dialog.open(WorkflowGroupEditComponent, {data: {workflowGroup: workflowGroupObj, companies: this.companies}});
  }

  updateWorkflowGroup(workflowGroup: EventWorkflowGroup) {
    this.dialog.open(WorkflowGroupEditComponent, {data: {workflowGroup, companies: this.companies}});
  }

  deleteWorkflowGroup(workflowGroup: any) {
    const _title: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.TITLE');
    const _description: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.WAIT_DESCRIPTION');

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.store.dispatch(new WorkflowGroupOnServerDeleted({workflowGroupId: workflowGroup.id}));
    });
  }
}
