import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {TranslateService} from '@ngx-translate/core';
import {EventWorkflowGroup} from '../../../../../../core/events/_models/workflow-group.model';
import {CompaniesService, CompanyModel} from '../../../../../../core/management';
import {Status} from '../../../../../../core/events/_models/status.models';
import {Subscription} from 'rxjs';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {WorkflowGroupService} from '../../../../../../core/events/_services/workflow-group.service';
import {WorkflowGroupCreate, WorkflowGroupUpdate} from '../../../../../../core/events/_actions/workflow-group.action';
import {Update} from '@ngrx/entity';
import {LayoutUtilsService, MessageType} from '../../../../../../core/_base/crud';
import {jsonValidator} from "../../apis/api-edit/api-edit.component";

@Component({
  selector: 'kt-workflow-group-edit',
  templateUrl: './workflow-group-edit.component.html',
})
export class WorkflowGroupEditComponent implements OnInit {
  workflowGroup: EventWorkflowGroup;
  workflowGroupForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  subscriptions: Subscription[] = [];
  companies: CompanyModel[];
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  states: Status[] = [];
  edit: boolean

  constructor(public dialogRef: MatDialogRef<WorkflowGroupEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fb: FormBuilder,
              private store: Store<AppState>,
              private translate: TranslateService,
              private workflowGroupService: WorkflowGroupService,
              private companiesService: CompaniesService,
              private layoutUtilsService: LayoutUtilsService,
  ) {
    this.edit = this.data.workflowGroup.id ? true : false
  }

  ngOnInit() {
    this.workflowGroup = Object.assign({}, this.data.workflowGroup);
    this.states = Object.assign([], this.data.workflowGroup.status);
    this.companiesService.getAllCompanies().subscribe(res => {
      this.companies = res;
    });
    this.createForm();
    if (this.workflowGroup.company_id) {
      this.workflowGroupForm.get('companyId').disable()
    }
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.states.push({
        status: value.trim(),
        workflow_group_id: this.workflowGroup.id,
        number: (this.states.length + 1).toString(),
        hidden:false
      } as Status);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.workflowGroupForm.get('states').setValue(this.states)

  }
  checkVisible(event: any,index,state){
    const index1 = this.states.indexOf(state);
    this.states[index1].hidden = event.target.checked
  }
  remove(state): void {
    const index = this.states.indexOf(state);
    if (index >= 0) {
      this.states.splice(index, 1);
    }
    for (let i = 0; i < this.states.length; i++) {
      this.states[i].number = (i + 1).toString()
    }
    this.workflowGroupForm.get('states').setValue(this.states)

  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    this.workflowGroupForm = this.fb.group({
      name: [this.workflowGroup.name, Validators.required],
      description: [this.workflowGroup.description],
      companyId: [this.workflowGroup.company_id, Validators.required],
      states: [this.states, Validators.required],
      schema: [this.workflowGroup.schema, {validators: [Validators.required, jsonValidator]}],
    });
    if(this.workflowGroup.id > 0){
      this.workflowGroupForm.controls.states.disable()
    }
  }

  getTitle() {
    let result = this.translate.instant('WORKFLOW_GROUP.NEW_WORKFLOW_GROUP');
    if (!this.workflowGroup || !this.workflowGroup.id) {
      return result;
    }
    result = this.translate.instant('WORKFLOW_GROUP.EDIT_WORKFLOW_GROUP');
    return result;
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.workflowGroupForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.workflowGroupForm.controls;
    /** check form */
    if (this.workflowGroupForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    if (this.workflowGroup.id) {
      return this.updateWorkflowGroup();
    }
    this.createWorkflowGroup();
  }


  private updateWorkflowGroup() {
    this.newDate()
    const updateWorkflowGroup: Update<EventWorkflowGroup> = {
      id: this.workflowGroup.id,
      changes: this.workflowGroup
    };
    const _updateMessage = this.translate.instant('WORKFLOW_GROUP.UPDATED');
    this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update, 3000, false, false);

    this.store.dispatch(new WorkflowGroupUpdate({
      partialEventAction: updateWorkflowGroup,
      workflowGroup: this.workflowGroup
    }));
    this.dialogRef.close();
  }

  private createWorkflowGroup() {
    this.newDate()
    const _createMessage = this.translate.instant('WORKFLOW_GROUP.NEW_WORKFLOW');
    this.layoutUtilsService.showActionNotification(_createMessage, MessageType.Update, 3000, false, false);

    this.store.dispatch(new WorkflowGroupCreate({workflowGroup: this.workflowGroup}));
    this.dialogRef.close();
  }

  newDate() {
    this.workflowGroup.status = this.states
    this.workflowGroup.name = this.workflowGroupForm.get('name').value
    this.workflowGroup.description = this.workflowGroupForm.get('description').value
    this.workflowGroup.company_id = this.workflowGroupForm.get('companyId').value
    this.workflowGroup.schema = this.workflowGroupForm.get('schema').value
  }

  onAlertClose() {
    this.hasFormErrors = false;
  }


}
