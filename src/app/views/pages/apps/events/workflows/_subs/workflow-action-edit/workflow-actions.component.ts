import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AppState} from '../../../../../../../core/reducers';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {Update} from '@ngrx/entity';
import {skip, take} from 'rxjs/operators';
import {QueryResultsModel} from 'src/app/core/_base/crud';
import {
  ApiService,
  EventActionService,
  selectWorkflowActionLatestSuccessfullAction,
  selectWorkflowActionsActionLoading,
  selectWorkflowActionsInStore,
  WorkflowAction,
  WorkflowActionOnServerCreated,
  WorkflowActionOnServerUpdated,
  WorkflowService,
} from 'src/app/core/events';
import {GeneralAutocompleteComponent} from '../../../../../../partials/layout/general-autocomplete/general-autocomplete.component';

@Component({
  selector: 'kt-workflow-actions',
  templateUrl: './workflow-actions.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})

export class WorkflowActionsComponent implements OnInit, OnDestroy {
  workflowAction: WorkflowAction;
  workflow: number;
  workflowActionsForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  company_id: number;
  worflowActionList: WorkflowAction[];
  parent_id: number;
  filteredStatus = [];
  subscriptions: Subscription[] = [];
  status = [];
  AllStatus = [];
  private workflowGroupId: any;

  apiAutocompleteCall;
  @ViewChild('apiAutocomplete') apiAutocomplete: GeneralAutocompleteComponent;

  formAutocompleteCall;
  @ViewChild('formAutocomplete') formAutocomplete: GeneralAutocompleteComponent;

  actionAutocompleteCall;
  @ViewChild('actionAutocomplete') actionAutocomplete: GeneralAutocompleteComponent;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<WorkflowActionsComponent>,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translate: TranslateService,
    private apiService: ApiService,
    private actionsService: EventActionService,
    private workflowService: WorkflowService,
    private cdr: ChangeDetectorRef,

  ) {
  }


  ngOnInit() {
    this.workflowAction = this.data.workflowAction;
    this.workflow = this.data.workflow;
    this.company_id = this.data.company;
    this.workflowGroupId = this.data.workflowGroupId;
    this.apiAutocompleteCall = this.apiService.searchApis.bind(this.apiService, this.company_id);
    this.formAutocompleteCall = this.workflowService.searchForms.bind(this.workflowService, this.company_id);
    this.actionAutocompleteCall = this.actionsService.searchEventActions.bind(this.actionsService, this.company_id);
    this.createForm();
    this.cdr.detectChanges()
    let currentStatus;
    this.workflowService.getStatusByGroupANDCompany(this.workflowGroupId, this.company_id,).subscribe(result => {
      this.status = result.data
      this.AllStatus = result.data
      this.filteredStatus = this.status
      if(this.workflowAction && this.workflowAction.status){
        currentStatus = this.AllStatus.find(e => e.status === this.workflowAction.status)
        this.cdr.detectChanges()
      }
    })
    const listSubscription = this.store.pipe(select(selectWorkflowActionsInStore), take(1)).subscribe((response: QueryResultsModel) => {
      if (response.totalCount <= 0) {
        this.workflowService.getAllWorkflowActions(this.workflow, this.company_id).subscribe(res => {
          this.worflowActionList = res.data;
          if (
            this.worflowActionList[this.worflowActionList.length - 1] !== null
            && this.worflowActionList[this.worflowActionList.length - 1] !== undefined
          ) {
            this.parent_id = this.worflowActionList[this.worflowActionList.length - 1].id;
            const st = this.worflowActionList[this.worflowActionList.length - 1].status;
            let num = 0
            if (this.status.find(e => e.status === st)) {
              num = this.status.find(e => e.status === st).number
            }
            this.filteredStatus = this.status.splice(num - 1, this.status.length);
            if(this.workflowAction && this.workflowAction.status)
            this.filteredStatus.push(currentStatus)

          }
        })
      } else {
        this.worflowActionList = response.items;
        this.parent_id = this.worflowActionList[this.worflowActionList.length - 1].id;
      }
    });
    this.subscriptions.push(listSubscription);
    const loadingSubscription = this.store.pipe(select(selectWorkflowActionsActionLoading)).subscribe(res => this.viewLoading = res);
    this.subscriptions.push(loadingSubscription);
    const successSubscription = this.store.pipe(select(selectWorkflowActionLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(successSubscription);
    if(this.workflowAction)
    this.displayData()
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    this.workflowActionsForm = this.fb.group({
      name: [this.workflowAction.name, Validators.required],
      action: [],
      description: [this.workflowAction.description],
      apis: [],
      form: [, Validators.required],
      status: [this.workflowAction.status, Validators.required],
    });
  }

  displayData(){
    this.workflowActionsForm.get('form').setValue(this.workflowAction.form);
    if(this.formAutocomplete)
      this.formAutocomplete.setValue(this.workflowAction.form)
    this.workflowActionsForm.get('action').setValue(this.workflowAction.action);
    if(this.actionAutocomplete)
      this.actionAutocomplete.setValue(this.workflowAction.action)

    if(this.workflowAction.apis)
    this.workflowActionsForm.get('apis').setValue(this.workflowAction.apis.map(api => {
      return {
        id: api.id,
        api_name: api.api_name,
      }
    }));
  }
  getTitle() {
    let result = this.translate.instant('WORKFLOW.NEW_WORKFLOW_ACTION');
    if (!this.workflowAction.id) {
      return result;
    }
    result = this.translate.instant('WORKFLOW.EDIT_WORKFLOW_ACTION') + `-` + `${this.workflowAction.name}`;
    return result;
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.workflowActionsForm.controls[controlName];
    return control.invalid && control.touched;
  }

  prepareWorkflowAction(): WorkflowAction {
    const controls = this.workflowActionsForm.controls;
    const _workflowAction = new WorkflowAction();
    _workflowAction.clear();
    _workflowAction.id = this.workflowAction.id;
    _workflowAction.name = controls.name.value;
    _workflowAction.action = controls.action.value;
    _workflowAction.apis = controls.apis.value;
    _workflowAction.form = controls.form.value;
    _workflowAction.description = controls.description.value;
    _workflowAction.status = controls.status.value;
    _workflowAction.parent_id = this.parent_id;
    _workflowAction.mapping_template = this.workflowAction.mapping_template;
    return _workflowAction;
  }

  onSubmit() {
    this.apiAutocomplete.updateValueAndValidity();
    this.formAutocomplete.updateValueAndValidity();
    this.actionAutocomplete.updateValueAndValidity();

    this.hasFormErrors = false;
    const controls = this.workflowActionsForm.controls;
    /** check form */
    if (this.workflowActionsForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const editedWorkflowAction = this.prepareWorkflowAction();
    if (editedWorkflowAction.id > 0) {
      this.updateWorkflowAction(editedWorkflowAction);
    } else {
      this.createWorkflowAction(editedWorkflowAction);
    }
  }

  updateWorkflowAction(_workflowAction: WorkflowAction) {
    const updateWorkflowAction: Update<WorkflowAction> = {
      id: _workflowAction.id,
      changes: _workflowAction
    };
    this.store.dispatch(new WorkflowActionOnServerUpdated({
      partialWorkflowAction: updateWorkflowAction,
      workflowAction: _workflowAction, workflow: this.workflow, company_id: this.company_id
    }));
  }

  createWorkflowAction(_workflowAction: WorkflowAction) {
    this.store.dispatch(new WorkflowActionOnServerCreated(
      {
        workflowAction: _workflowAction,
        workflow: this.workflow,
        company_id: this.company_id
      }
    ));
  }

  onAlertClose() {
    this.hasFormErrors = false;
  }

  getStatusString(): string {
    switch (this.workflowActionsForm.get('is_active').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }

  displayApi(api: any): string {
    let res = ''
    if (api && api.api_name) {
      res = api.api_name
    }
    return res
  }

  onApiChange(val) {
    const newapi = val.map(value => {
      return {
        id: value.id,
        api_name: value.api_name
      }
    });
    this.workflowActionsForm.get('apis').setValue(newapi);
  }

  formatNewApiFn(api: any) {
    return api;
  }

  displayForm(form: any): string {
    let res = ''
    if (form && form.form_name) {
      res = form.form_name
    }
    return res
  }

  onFormChange(val) {
    this.workflowActionsForm.get('form').setValue(val);
  }

  displayAction(action: any): string {
    let res = ''
    if (action && action.action_name) {
      res = action.action_name
    }
    return res
  }

  onActionChange(val) {
    this.workflowActionsForm.get('action').setValue(val);
  }
}

