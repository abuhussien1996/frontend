import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {LayoutUtilsService, MessageType, QueryParamsModel} from 'src/app/core/_base/crud';
import {CompanyModel} from 'src/app/core/management/_models/company.model';
import {AppState} from 'src/app/core/reducers';
import {WorkflowActionsComponent} from '../workflow-action-edit/workflow-actions.component';
import {ViewMapFormioComponent} from '../view-map-formio/view-map-formio.component';
import {
  Api,
  DeletedStatusUpdated,
  EventAction,
  Form,
  ParentUpdated,
  selectWorkflowActionError,
  selectWorkflowActionLatestSuccessfullAction,
  selectWorkflowActionsInStore,
  selectWorkflowActionsPageLastQuery,
  Workflow,
  WorkflowAction,
  WorkflowActionActionTypes,
  WorkflowActionOnServerDeleted,
  WorkflowActionsDataSource,
  WorkflowActionsPageRequested
} from 'src/app/core/events';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-workflowAction-actions-list',
  templateUrl: './workflow-actions-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkflowActionsListComponent implements OnInit, OnDestroy {
  dataSource: WorkflowActionsDataSource;
  displayedColumns = ['id','name','api_id', 'action_id', 'form_id', 'description', 'status', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = '';
  filterCondition = '';
  lastQuery: QueryParamsModel;
  selection = new SelectionModel<WorkflowAction>(true, []);
  workflowActionsResult: WorkflowAction[] = [];
  users: any[];
  companies: CompanyModel[]
  filterCompany = '';
  workflow: Workflow;
  idWorkflow: number
  idCompany: number
  noItem = false
  private subscriptions: Subscription[] = [];
  private workflowGroupId: any;

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    this.store.pipe(select(selectWorkflowActionsInStore)).subscribe((res) => {
      this.workflowActionsResult = res.items;
    })
    const routeSub = this.activatedRoute.params.subscribe(params => {
      this.idWorkflow = params.id;
      this.idCompany = params.company_id
      this.workflowGroupId = params.workflow_group_id
      if (this.idWorkflow && this.idWorkflow > 0) {
        this.loadWorkflowActionsList()
      }
    });
    this.subscriptions.push(routeSub);
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);
    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadWorkflowActionsList())
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);
    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadWorkflowActionsList();
      })
    ).subscribe();
    this.subscriptions.push(searchSubscription);
    // Init DataSource
    this.dataSource = new WorkflowActionsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.workflowActionsResult = res;
      if (res.length === 0) {
        this.noItem = true
      } else {
        this.noItem = false
      }
    });
    this.subscriptions.push(entitiesSubscription);
    const lastQuerySubscription = this.store.pipe(select(selectWorkflowActionsPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from storezzzzzzzz
    this.subscriptions.push(lastQuerySubscription);
    // Read from URL itemId, for restore previous state
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery);
      }
      this.loadWorkflowActionsList();
    });
    this.subscriptions.push(routeSubscription);
    const errorSubscription = this.store.pipe(select(selectWorkflowActionError), skip(1)).subscribe((res) => {
      if (res) {
        if (res === 'DELETE_ERROR') {
          this.loadWorkflowActionsList();
        }
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
        );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectWorkflowActionLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        let showMessage = true
        switch (res.action_type) {
          case WorkflowActionActionTypes.WorkflowActionCreated:
            successMsg = this.translate.instant('WORKFLOW.EDIT_ACTION.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case WorkflowActionActionTypes.WorkflowActionUpdated:
            successMsg = this.translate.instant('WORKFLOW.EDIT_ACTION.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case WorkflowActionActionTypes.WorkflowActionDeleted:
            successMsg = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_ACTION_SIMPLE.MESSAGE');
            msgType = MessageType.Delete;
            break;
          default :
            showMessage = false
            break;
        }
        if (showMessage) {
          this.layoutUtilsService.showActionNotification(
            successMsg,
            msgType,
            3000,
            true,
            false
          );
        }
      }
    });
    this.subscriptions.push(successSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  mapDialog(_item: WorkflowAction, index) {
    const formSchema1 = this.workflowActionsResult[index].form.schema;
    const formSchema2 = this.workflowActionsResult[index - 1].form.schema;
    const template1 = this.workflowActionsResult[index].mapping_template;

    const dialogMapRef = this.dialog.open(ViewMapFormioComponent, {
      width: '1000px',
      height: 'auto',
      data: {
        firstFormSchema: formSchema2,
        secondFormSchema: formSchema1,
        mapping_template: template1,
        workflowAction: _item,
        workflowId: this.idWorkflow,
        companyId: this.idCompany
      }
    });
    dialogMapRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  loadWorkflowActionsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new WorkflowActionsPageRequested({page: queryParams, workflow: this.idWorkflow, company: this.idCompany}));
    this.selection.clear();
  }

  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.is_active = this.filterStatus;
    filter.searchData = searchText
    filter.name = searchText;
    filter.email = searchText;
    filter.company = this.filterCompany
    return filter;
  }

  restoreState(queryParams: QueryParamsModel) {

    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }
    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  deleteWorkflowAction(_item: WorkflowAction) {
    const _title: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_ACTION_SIMPLE.TITLE');
    const _description: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_ACTION_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_ACTION_SIMPLE.WAIT_DESCRIPTION');
    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      let nextAction: WorkflowAction
      for (const action of this.workflowActionsResult) {
        if (action.parent_id === _item.id) {
          nextAction = action;
          break;
        }
      }
      if (nextAction !== null && nextAction !== undefined) {
        this.store.dispatch(new ParentUpdated({id: nextAction.id, parent_id: _item.parent_id}));
      }
      this.store.dispatch(new DeletedStatusUpdated({id: _item.id, deleted: true}));
      this.store.dispatch(new WorkflowActionOnServerDeleted({
          id: _item.id,
          workflowActions: this.workflowActionsResult,
          workflow_id: this.idWorkflow,
          company_id: this.idCompany
        }
      ));
    });
  }

  getItemCssClassByStatus(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return 'danger';
      case true:
        return 'success';
    }
  }

  getItemStatusString(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  addWorkflowAction() {
    const newWorkflowAction = new WorkflowAction();
    newWorkflowAction.clear(); // Set all defaults fields
    this.updateWorkflowAction(newWorkflowAction);
  }

  updateWorkflowAction(workflowAction: WorkflowAction) {
    this.dialog.open(WorkflowActionsComponent, {
      data: {
        workflowAction,
        workflowGroupId: this.workflowGroupId,
        workflow: this.idWorkflow,
        company: this.idCompany
      }
    });
  }

  getCompanyName(id: number): string {
    if (this.companies !== null && this.companies !== undefined) {
      if (this.companies.filter(item => item.id === id)[0] !== null && this.companies.filter(item => item.id === id)[0] !== undefined) {
        return this.companies.filter(item => item.id === id)[0].name;
      }
    }
  }

  getEventName(event: Event) {
    if (event !== null && event !== undefined) {
      return event;
    }
    return '-';
  }


  getApiName(api: Api) {
    if (api !== null && api !== undefined) {
      return api.api_name;
    }
    return '-';
  }


  getFormName(form) {
    if (form !== null && form !== undefined) {
      return form.form_name;
    }
    return '-';
  }

  getActionName(action: EventAction) {
    if (action !== null && action !== undefined) {
      return action.action_name;
    }
    return '-';
  }
}
