import { Component, ElementRef, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../../../../../core/reducers';
import {
  WorkflowActionMapServerUpdated,
  selectWorkflowActionLatestSuccessfullAction
 } from '../../../../../../../core/events';
import { skip } from 'rxjs/operators';
import { Subscription } from 'rxjs';

export interface DialogData {
  companyId: any;
  workflowId: any;
  firstFormSchema: string;
  secondFormSchema: string;
  mapping_template: any;
  workflowAction: any;
}

export interface Element {
  source: any;
  destination: any;
}

@Component({
  selector: 'kt-view-map-formio',
  templateUrl: './view-map-formio.component.html',
  styleUrls: ['./view-map-formio.component.scss']
})
export class ViewMapFormioComponent implements OnDestroy {

  @ViewChild('wizard', { static: true }) el: ElementRef;

  isDisabled = false;
  isEdit = false;
  readOnlyField1: any;
  readOnlyField2: any;
  hasFormErrors1 = false;
  hasFormErrors2 = false;
  errors1 = '';
  errors2 = '';
  viewLoading = false;
  title = 'TABLE.MAPPING';
  sourceList: any;
  destinationList: any;
  element = [];
  mapDataSource = new MatTableDataSource<Element>(this.element);
  displayedColumns: string[] = ['source', 'destination', 'action'];
  mapGroup: any;
  mapFormGroup: any;
  public elements: any;
  private mapId = null;
  private workflowId: any;
  private companyId: any;
  private successSubscription: Subscription;
  private workflowActionObj = {
    mapping_template: undefined
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<ViewMapFormioComponent>,
    private translate: TranslateService,
    private store: Store<AppState>
  ) {
    try {
      if (data.mapping_template === null) {
        this.isEdit = true;
      }
      this.companyId = this.data.companyId;
      this.workflowId = this.data.workflowId;
      const copy = JSON.parse(JSON.stringify(this.data.workflowAction))
      this.workflowActionObj = copy;
      this.prepareReadOnlyData();
      this.createForm();
    } catch (e) {
      this.onNoClick();
    }
    this.prepareReadOnlyData();
    this.createForm();
  }
  ngOnDestroy(): void {
    this.successSubscription.unsubscribe();
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit(): void {
    this.successSubscription = this.store.pipe(select(selectWorkflowActionLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        this.dialogRef.close();
      }
    });
    // Initialize form wizard
    const wizard = new KTWizard(this.el.nativeElement, {
      startStep: 1
    });

    wizard.on('beforeNext', (wizardObj) => {
      this.checkRequired();
      // wizardObj.stop();
    });

    // Change event
    wizard.on('change', () => {
      setTimeout(() => {
        KTUtil.scrollTop();
      }, 500);
    });
  }

  createForm() {
    let mapData = { template: [], id: null, is_active: false, name: null, description: null }
    if (this.data.mapping_template) {
      try {
        const _template = JSON.parse(this.data.mapping_template.template);
        this.element = _template;
        mapData = this.data.mapping_template;
      } catch (e) {
        console.log(e);
      }
    }
    this.mapId = mapData.id;
    this.mapDataSource.data = this.element;
    this.mapGroup = new FormGroup({
      source: new FormControl('', Validators.required),
      destination: new FormControl('', Validators.required),
    });
    this.mapFormGroup = new FormGroup({
      isActive: new FormControl(mapData.is_active),
      name: new FormControl(mapData.name, Validators.required),
      description: new FormControl(mapData.description)
    });
  }

  private prepareReadOnlyData() {
    try {
      const jsonObject1 = JSON.parse(this.data.firstFormSchema);
      this.readOnlyField1 = jsonObject1;
      this.sourceList = this.getObject(this.readOnlyField1);
      console.log(this.sourceList)
    } catch (objError) {
      this.hasFormErrors1 = true;
      if (objError instanceof SyntaxError) {
        this.errors1 = objError.name;
      } else {
        this.errors1 = objError.message;
      }
    }
    try {
      const jsonObject2 = JSON.parse(this.data.secondFormSchema);
      this.readOnlyField2 = jsonObject2;
      this.destinationList = this.getObject(this.readOnlyField2);
    } catch (objError) {
      this.hasFormErrors2 = true;
      if (objError instanceof SyntaxError) {
        this.errors2 = objError.name;
      } else {
        this.errors2 = objError.message;
      }
    }

  }

  getObject(obj = {}, property = 'key') {
    const items = [];

    function traverse(o) {
      // tslint:disable-next-line:forin
      for (const i in o) {
        if (i === property && o[i] !== null && o[i] !== '') items.push(o[i]);
        if (!!o[i] && typeof (o[i]) === 'object') {
          traverse(o[i]);
        }
      }
    }

    traverse(obj);
    return new Set(items);

  }

  getTitle(): string {
    return this.translate.instant(this.title);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onAlertClose($event) {
    this.hasFormErrors1 = false;
    this.hasFormErrors2 = false;
  }

  onMapClick() {
    const indexAdd = this.element.findIndex(e => e.source === this.mapGroup.get('source').value &&
      e.destination === this.mapGroup.get('destination').value);
    if (indexAdd === -1) {
      this.element.push({ source: this.mapGroup.get('source').value, destination: this.mapGroup.get('destination').value });
      // tslint:disable-next-line:no-unused-expression
      this.mapDataSource.data = this.element;
    }
  }

  deleteMap(element: any) {
    const index = this.element.findIndex(e => e.source === element.source && e.destination === element.destination);
    if (index !== -1) {
      this.element.splice(index, 1);
      this.mapDataSource.data = this.element;
    }
  }

  onSubmit() {
    this.checkRequired();
    if (this.mapFormGroup.invalid) {
      return;
    }
    this.isDisabled = true;
    const mappingTemplate = {
      description: this.mapFormGroup.get('description').value,
      id: this.mapId,
      is_active: this.mapFormGroup.get('isActive').value,
      name: this.mapFormGroup.get('name').value,
      template: JSON.stringify(this.element)
    }

    this.workflowActionObj.mapping_template = mappingTemplate;

    this.store.dispatch(new WorkflowActionMapServerUpdated({
      id: this.workflowId,
      companyId: this.companyId,
      updatedObject: this.workflowActionObj
    }))

  }

  checkRequired() {
    const controls = this.mapFormGroup.controls;
    /** check form */
    if (this.mapFormGroup.inmapping_templatevalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }
  }

  getStatusString(): string {
    switch (this.mapFormGroup.get('isActive').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }
}
