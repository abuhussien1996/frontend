import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {LayoutUtilsService, MessageType, QueryParamsModel, QueryResultsModel} from '../../../../../../core/_base/crud';
import {TranslateService} from '@ngx-translate/core';
import {
  Event,
  Workflow,
  WorkflowOnServerDeleted,
  WorkflowsPageRequested,
  selectWorkflowError,
  selectWorkflowsPageLastQuery,
  WorkflowsDataSource,
  selectWorkflowLatestSuccessfullAction,
  WorkflowActionTypes
} from 'src/app/core/events';
import {WorkflowEditComponent} from '../workflow-edit/workflow-edit.component';
import {CompaniesService, CompanyModel, selectCompaniesInStore} from '../../../../../../core/management';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-workflows-list',
  templateUrl: './workflows-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkflowsListComponent implements OnInit, OnDestroy {
  dataSource: WorkflowsDataSource;
  displayedColumns = ['name','company_id', 'event_id', 'description', 'is_active', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = '';
  lastQuery: QueryParamsModel;
  selection = new SelectionModel<Workflow>(true, []);
  companies: CompanyModel[]
  filterCompany = '';
  private subscriptions: Subscription[] = [];

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private companiesService: CompaniesService
  ) {}

  ngOnInit() {
    const companiesSubscription = this.store.pipe(
      select(selectCompaniesInStore),
    ).subscribe((response: QueryResultsModel) => {
      if (response.totalCount <= 0) {
        this.companiesService.getAllCompanies().subscribe(res => {
          this.companies = res;
        })
      } else {
        this.companies = response.items
      }
    });
    this.subscriptions.push(companiesSubscription);
    const errorSubscription = this.store.pipe(select(selectWorkflowError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectWorkflowLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch(res.action_type) {
          case WorkflowActionTypes.WorkflowCreated :
            successMsg = this.translate.instant('WORKFLOW.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case WorkflowActionTypes.WorkflowUpdated :
            successMsg = this.translate.instant('WORKFLOW.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case WorkflowActionTypes.WorkflowDeleted :
            successMsg = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.MESSAGE');
            msgType = MessageType.Delete;
            break;
        }
        this.layoutUtilsService.showActionNotification(
          successMsg,
          msgType,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(successSubscription);
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadWorkflowsList())
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadWorkflowsList();
      })
    )
      .subscribe();
    this.subscriptions.push(searchSubscription);
    // Init DataSource
    this.dataSource = new WorkflowsDataSource(this.store);
    const lastQuerySubscription = this.store.pipe(select(selectWorkflowsPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push(lastQuerySubscription);
    // Read from URL itemId, for restore previous state
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery);
      }
      this.loadWorkflowsList();
    });
    this.subscriptions.push(routeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loadWorkflowsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new WorkflowsPageRequested({page: queryParams}));
    this.selection.clear();
  }

  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.is_active = this.filterStatus;
    filter.searchData = searchText
    filter.name = searchText;
    filter.email = searchText;
    filter.company = this.filterCompany
    return filter;
  }

  restoreState(queryParams: QueryParamsModel) {
    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }

    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  deleteWorkflow(_item: Workflow) {
    const _title: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.TITLE');
    const _description: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('WORKFLOW.DELETE_WORKFLOW_SIMPLE.WAIT_DESCRIPTION');

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.store.dispatch(new WorkflowOnServerDeleted({id: _item.id}));
    });
  }

  getItemCssClassByStatus(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return 'danger';
      case true:
        return 'success';
    }
  }

  getItemStatusString(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  addWorkflow() {
    const newWorkflow = new Workflow();
    newWorkflow.clear(); // Set all defaults fields
    this.updateWorkflow(newWorkflow);
  }

  updateWorkflow(workflow: Workflow) {
    this.dialog.open(WorkflowEditComponent, {data: {workflow, companies: this.companies}});
  }

  getCompanyName(id: number): string {
    if (this.companies !== null && this.companies !== undefined) {
      if (this.companies.filter(item => item.id === id)[0] !== null && this.companies.filter(item => item.id === id)[0] !== undefined) {
        return this.companies.filter(item => item.id === id)[0].name;
      }
    }
  }

  getEventName(event: Event) {
    if (event !== null && event !== undefined) {
        return event.event_name;
    }
  }

  goToActions(idw ,idc,idg) {
    this.router.navigate(['/events/event-workflows/actions/' + idw + '/' + idc+ '/' + idg]);
  }
}
