import {ChangeDetectionStrategy, Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable, Subscription} from 'rxjs';
import {Update} from '@ngrx/entity';
import {select, Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {AppState} from 'src/app/core/reducers';
import {CompanyModel} from '../../../../../../core/management';
import {QueryResultsModel} from '../../../../../../core/_base/crud';
import {of} from 'rxjs/internal/observable/of';
import {map, skip} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete/autocomplete';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {
  Event,
  EventService,
  selectEventsInStore,
  selectWorkflowLatestSuccessfullAction,
  selectWorkflowsActionLoading,
  Workflow,
  WorkflowOnServerCreated,
  WorkflowOnServerUpdated
} from 'src/app/core/events';
import {WorkflowGroupService} from '../../../../../../core/events/_services/workflow-group.service';
import {GeneralAutocompleteComponent} from '../../../../../partials/layout/general-autocomplete/general-autocomplete.component';
import {CategoryService} from '../../../../../../core/lists/_services';
import {jsonValidator} from '../../apis/api-edit/api-edit.component';

@Component({
  selector: 'kt-workflow-edit',
  templateUrl: './workflow-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})

export class WorkflowEditComponent implements OnInit, OnDestroy {
  workflow: Workflow;
  workflowForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  eventList: any = [];
  subscriptions: Subscription[] = [];
  filteredEvents: Observable<any>;
  @ViewChild('eventInput') entityInput: ElementRef<HTMLInputElement>;
  workflowGroups = [];
  workflowGroupsAuto = []
  private companyId = null;
  @ViewChild('categoryAutocomplete') categoryAutocomplete: GeneralAutocompleteComponent;
  categoryAutocompleteCall = this.categoryService.getCategoriesByWorkflow.bind(this.categoryService, 0);
  @ViewChild('eventAutocomplete') eventAutocomplete: GeneralAutocompleteComponent;
  eventAutocompleteCall = this.eventsService.searchEventsForWorkflow.bind(this.categoryService, 0);
  isReadOnly = true
  categories = []

  constructor(
    public dialogRef: MatDialogRef<WorkflowEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translate: TranslateService,
    private eventsService: EventService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private categoryService: CategoryService,
    private workflowGroupService: WorkflowGroupService
  ) {
    this.matIconRegistry.addSvgIcon(
      `plus`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/media/svg/icons/Navigation/Plus.svg')
    );
  }

  ngOnInit() {
    this.workflow = this.data.workflow;
    const loadingSubscription = this.store.pipe(select(selectWorkflowsActionLoading)).subscribe(res => this.viewLoading = res);
    this.subscriptions.push(loadingSubscription);
    const successSubscription = this.store.pipe(select(selectWorkflowLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res.action_type) {
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(successSubscription);
    this.createForm();
    this.getWorkflowGroup()

  }

  getWorkflowGroup() {
    const getWorkflowGroup = this.workflowGroupService.getAllWorkflowGroup().subscribe(res => {
      this.workflowGroups = res.data
      if (this.workflow.workflow_group_id && this.workflowGroups) {
        const workflowGroupSelect = this.workflowGroups.find(e =>
          e.id === this.workflow.workflow_group_id
        )
        if (workflowGroupSelect) {
          this.workflowForm.get('workflowGroup').setValue(workflowGroupSelect)
          const workflowGroup = this.workflowForm.get('workflowGroup');
          workflowGroup.disable()
        }
      }
    })
    this.subscriptions.push(getWorkflowGroup);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    this.workflowForm = this.fb.group({
      name: [this.workflow.name, Validators.required],
      event_id: [],
      description: [this.workflow.description],
      is_active: [this.workflow.is_active],
      workflowGroup: [null, Validators.required],
      category: [],
      schema: [this.workflow.schema, {validators: [Validators.required, jsonValidator]}],
    });
    if (this.workflow.event) {
      this.isReadOnly = false
      this.eventAutocompleteCall = this.eventsService.searchEventsForWorkflow.bind(this.categoryService, this.workflow.company_id)
      const eventSelect = {
        company_id: 0,
        description: '0',
        due_date: '0',
        event_name: this.workflow.event.event_name,
        id: this.workflow.event.id,
        responsible: '',
        visible_from_app: false
      }
      this.eventsService.getAllEvents().subscribe(res => {
        this.workflowForm.get('event_id').setValue(eventSelect)
        this.eventAutocomplete.setValue(eventSelect)
      })
    }
    if (this.workflow.category_id) {
      this.isReadOnly = false
      this.categoryAutocompleteCall = this.categoryService.getCategoriesByWorkflow.bind(this.categoryService, this.workflow.company_id)
      this.categoryService.getCategoryForWorkflowById(this.workflow.category_id, this.workflow.company_id).subscribe(res => {
        this.categories = res[`data`];
        this.workflowForm.get('category').setValue(this.categories)
        this.categoryAutocomplete.setValue(this.categories)
      })
    }
  }

  getTitle() {
    let result = this.translate.instant('WORKFLOW.NEW_WORKFLOW');
    if (!this.workflow.id) {
      return result;
    }
    result = this.translate.instant('WORKFLOW.EDIT_WORKFLOW') + `-` + `${this.workflow.name}`;
    return result;
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.workflowForm.controls[controlName];
    return control.invalid && control.touched;
  }

  prepareWorkflow(): Workflow {
    const controls = this.workflowForm.controls;
    const _workflow = new Workflow();
    _workflow.clear();
    _workflow.id = this.workflow.id;
    _workflow.name = controls.name.value;
    _workflow.company_id = this.companyId;
    _workflow.event = controls.event_id.value;
    _workflow.category_id = controls.category.value ? controls.category.value.id : null;
    _workflow.workflow_group_id = controls.workflowGroup.value.id;
    _workflow.is_active = controls.is_active.value;
    _workflow.description = controls.description.value;
    _workflow.schema = controls.schema.value;
    return _workflow;
  }

  onSubmit() {
    this.resetRubbishData('workflowGroup')
    this.eventAutocomplete.updateValueAndValidity();
    this.categoryAutocomplete.updateValueAndValidity();
    this.hasFormErrors = false;
    const controls = this.workflowForm.controls;
    /** check form */
    if (this.workflowForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const editedWorkflow = this.prepareWorkflow();
    if (editedWorkflow.id > 0) {
      this.updateWorkflow(editedWorkflow);
    } else {
      this.createWorkflow(editedWorkflow);
    }
  }

  updateWorkflow(_workflow: Workflow) {
    _workflow.is_active = _workflow.is_active.toString() === 'true';
    const updateWorkflow: Update<Workflow> = {
      id: _workflow.id,
      changes: _workflow
    };
    this.store.dispatch(new WorkflowOnServerUpdated({
      partialWorkflow: updateWorkflow,
      workflow: _workflow
    }));
  }

  createWorkflow(_workflow: Workflow) {
    this.store.dispatch(new WorkflowOnServerCreated({workflow: _workflow}));
  }

  onAlertClose() {
    this.hasFormErrors = false;
  }

  getStatusString(): string {
    switch (this.workflowForm.get('is_active').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.eventList.push(event.option.value);
    this.entityInput.nativeElement.value = event.option.value.event_name;
  }

  eventInputChanged(value) {
    if (value) {
      if (typeof value === 'string')
        this.filteredEvents = this.eventsService.searchEvents(value, this.companyId).pipe(map(res => res.data.filter((r) => {
            return !this.eventList.find((e: any) => e.id === r.id);
          })
          )
        )
    } else {
      this.filteredEvents = of([])
    }
  }


  displayWorkflowGroup(workflowGroup: Workflow): string {
    return workflowGroup && workflowGroup.name ? workflowGroup.name : '';
  }

  workflowGroupInputChanged(value) {
    this.workflowGroupsAuto = []
    if (this.workflowGroups && typeof value === 'string') {
      this.workflowGroupsAuto = this.workflowGroups.filter(el => ((el.name).toLowerCase()).includes(value.toLowerCase()));
    }
    this.selectWorkflow()
  }

  selectedWorkflow(workflowGroup: MatAutocompleteSelectedEvent) {
    this.workflowForm.get('workflowGroup').setValue(workflowGroup.option.value)
    this.selectWorkflow()
  }

  selectWorkflow() {
    const val = this.workflowForm.get('workflowGroup').value
    if (typeof val === 'object') {
      this.companyId = val.company_id
      this.categoryAutocompleteCall = this.categoryService.getCategoriesByWorkflow.bind(this.categoryService, this.companyId)
      this.eventAutocompleteCall = this.eventsService.searchEventsForWorkflow.bind(this.categoryService, this.companyId)
      this.isReadOnly = false
      return
    }
    this.isReadOnly = true

  }

  resetRubbishData(formConName) {
    const val = this.workflowForm.get(formConName).value
    if (typeof val !== 'object') {
      this.workflowForm.get(formConName).setValue(null)
    }
  }

  filterCategoryFn(event: any): string {
    return event ? event.name : '';
  }

  onCategoryChange(val) {
    this.workflowForm.controls.category.setValue(val)
    this.workflowForm.controls.category.updateValueAndValidity();
  }

  filterEventFn(event: any): string {
    return event ? event.event_name : '';
  }

  onEventChange(val) {
    this.workflowForm.controls.event_id.setValue(val)
    this.workflowForm.controls.event_id.updateValueAndValidity();
  }
}
