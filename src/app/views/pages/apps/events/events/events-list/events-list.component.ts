import {Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';
import {debounceTime, distinctUntilChanged, tap, skip} from 'rxjs/operators';
import {fromEvent, merge, Subscription} from 'rxjs';
import {Store, select} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {LayoutUtilsService, MessageType, QueryParamsModel, QueryResultsModel} from '../../../../../../core/_base/crud';
import {TranslateService} from '@ngx-translate/core';
import {EventEditComponent} from '../event-edit/event-edit.component';
import {AllCompaniesRequested, CompaniesService, CompanyModel, selectCompaniesInStore} from '../../../../../../core/management';
import {
  Event,
  EventActionTypes,
  EventOnServerDeleted,
  EventsDataSource,
  EventsPageRequested,
  selectEventError,
  selectEventLatestSuccessfullAction,
  selectEventsPageLastQuery
} from 'src/app/core/events';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-events-list',
  templateUrl: './events-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsListComponent implements OnInit, OnDestroy {
  dataSource: EventsDataSource;
  displayedColumns = ['event_name', 'company_id', 'responsible','due_date', 'description', 'visible_from_app', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = '';
  filterCondition = '';
  lastQuery: QueryParamsModel;
  selection = new SelectionModel<Event>(true, []);
  eventsResult: Event[] = [];
  users: any[];
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  companies: CompanyModel[];
  private subscriptions: Subscription[] = [];
  filterCompany = '';

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private companiesService: CompaniesService,
  ) {}

  ngOnInit() {
    this.store.dispatch(new AllCompaniesRequested());
    const companiesSubscription = this.store.pipe(
      select(selectCompaniesInStore),
    ).subscribe((response: QueryResultsModel) => {
      if (response.totalCount <= 0) {
        this.companiesService.getAllCompanies().subscribe(res => {
          this.companies = res;
        })
      } else {
        this.companies = response.items
      }
    });
    this.subscriptions.push(companiesSubscription);
    const errorSubscription = this.store.pipe(select(selectEventError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectEventLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch(res.action_type) {
          case EventActionTypes.EventCreated :
            successMsg = this.translate.instant('EVENT.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case EventActionTypes.EventUpdated :
            successMsg = this.translate.instant('EVENT.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case EventActionTypes.EventDeleted :
            successMsg = this.translate.instant('EVENT.DELETE_EVENT_SIMPLE.MESSAGE');
            msgType = MessageType.Delete;
            break;
        }
        this.layoutUtilsService.showActionNotification(
          successMsg,
          msgType,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(successSubscription);
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadEventsList())
    )
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadEventsList();
      })
    ).subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new EventsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.eventsResult = res;
    });
    this.subscriptions.push(entitiesSubscription);
    const lastQuerySubscription = this.store.pipe(select(selectEventsPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push(lastQuerySubscription);

    // Read from URL itemId, for restore previous state
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery);
      }
      this.loadEventsList();
    });
    this.subscriptions.push(routeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loadEventsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new EventsPageRequested({page: queryParams}));
    this.selection.clear();
  }

  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.visible_from_app = this.filterStatus;
    filter.searchData = searchText
    filter.name = searchText;
    filter.email = searchText;
    filter.company = this.filterCompany
    return filter;
  }

  restoreState(queryParams: QueryParamsModel) {

    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }

    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  deleteEvent(_item: Event) {
    const _title: string = this.translate.instant('EVENT.DELETE_EVENT_SIMPLE.TITLE');
    const _description: string = this.translate.instant('EVENT.DELETE_EVENT_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('EVENT.DELETE_EVENT_SIMPLE.WAIT_DESCRIPTION');
    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.store.dispatch(new EventOnServerDeleted({id: _item.id}));
    });
  }


  getItemCssClassByStatus(visible_from_app: boolean = false): string {
    switch (visible_from_app) {
      case false:
        return 'danger';
      case true:
        return 'success';
    }
  }

  getItemStatusString(visible_from_app: boolean = false): string {
    switch (visible_from_app) {
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  addEvent() {
    const newEvent = new Event();
    newEvent.clear(); // Set all defaults fields
    this.updateEvent(newEvent);
  }

  updateEvent(event: Event) {
    this.dialog.open(EventEditComponent, {data: {event, companies: this.companies}});
  }

  getCompanyName(id: number): string {
    if (this.companies !== null && this.companies !== undefined) {
      if (this.companies.filter(item => item.id === id)[0] !== null && this.companies.filter(item => item.id === id)[0] !== undefined) {
        return this.companies.filter(item => item.id === id)[0].name;
      }
    }
  }
}
