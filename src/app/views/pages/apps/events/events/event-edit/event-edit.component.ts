import {
  Component,
  OnInit,
  Inject,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  OnDestroy
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Update } from '@ngrx/entity';
import { Store, select } from '@ngrx/store';
import { DatePipe } from '@angular/common';
import { User } from 'src/app/core/auth';
import { TranslateService } from '@ngx-translate/core';
import { AppState } from 'src/app/core/reducers';
import { CompanyModel } from '../../../../../../core/management';
import {
  selectEventLatestSuccessfullAction,
  selectEventsActionLoading,
  Event, EventOnServerUpdated, EventOnServerCreated
} from 'src/app/core/events';
import { skip } from 'rxjs/operators';

@Component({
  selector: 'kt-event-edit',
  templateUrl: './event-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class EventEditComponent implements OnInit, OnDestroy {
  event: Event;
  eventForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  currentUser: User;
  companies: CompanyModel[];
  subscriptions: Subscription[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<EventEditComponent>,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translate: TranslateService,
    public datepipe: DatePipe
    ) {}

  ngOnInit() {
    this.event = this.data.event;
    this.companies = this.data.companies;
    const loadingSub =this.store.pipe(select(selectEventsActionLoading)).subscribe(res => this.viewLoading = res);
    this.subscriptions.push(loadingSub);
    const successSubscription = this.store.pipe(select(selectEventLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(successSubscription);
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    this.eventForm = this.fb.group({
      event_name: [this.event.event_name, Validators.required],
      responsible: [this.event.responsible, Validators.required],
      description: [this.event.description, Validators.required],
      visible_from_app: [this.event.visible_from_app],
      company_id: [this.event.company_id, Validators.required],
      due_date: [this.event.due_date, Validators.required]
    });
  }


  getTitle() {
    let result = this.translate.instant('EVENT.NEW_EVENT');
    if (!this.event || !this.event.id) {
      return result;
    }
    result = this.translate.instant('EVENT.EDIT_EVENT') + `-` + `${this.event.event_name}`;
    return result;
  }

  /**
   * Check control is invalid
   * @param controlName: string
   */
  isControlInvalid(controlName: string): boolean {
    const control = this.eventForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  /** ACTIONS */

  /**
   * Returns prepared event
   */
  prepareEvent(): Event {
    const controls = this.eventForm.controls;
    const _event = new Event();
    _event.clear();
    _event.id = this.event.id;
    _event.event_name = controls.event_name.value;
    _event.company_id = controls.company_id.value;
    _event.responsible = controls.responsible.value;
    _event.visible_from_app = controls.visible_from_app.value;
    _event.due_date = controls.due_date.value;
    _event.description = controls.description.value;

    return _event;
  }

  /**
   * On Submit
   */
  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.eventForm.controls;
    /** check form */
    if (this.eventForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const editedEvent = this.prepareEvent();
    if (editedEvent.id > 0) {
      this.updateEvent(editedEvent);
    } else {
      this.createEvent(editedEvent);
    }
  }

  /**
   * Update event
   *
   * @param _event: Event
   */
  updateEvent(_event: Event) {
    _event.visible_from_app = _event.visible_from_app.toString() === 'true' ? true : false;
    const updateEvent: Update<Event> = {
      id: _event.id,
      changes: _event
    };
    this.store.dispatch(new EventOnServerUpdated({
      partialEvent: updateEvent,
      event: _event
    }));
  }

  /**
   * Create event
   *
   * @param _event: Event
   */
  createEvent(_event: Event) {
    this.store.dispatch(new EventOnServerCreated({ event: _event }));
  }

  /** Alect Close event */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }

  /**
   * Rerurns status string
   */
  getStatusString(): string {
    switch (this.eventForm.get('visible_from_app').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }
}
