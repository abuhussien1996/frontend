import { Component, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {
  selectApiById,
  Api
} from '../../../../../../core/events';

@Component({
  selector: 'kt-api-edit',
  templateUrl: './api-view.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class ApiViewComponent implements OnInit {
  api: Api;
  api$: Observable<Api>;
  viewLoading = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ApiViewComponent>,
    private store: Store<AppState>
    ) {
  }

  ngOnInit() {
    if (this.data.apiId) {
      this.api$ = this.store.pipe(select(selectApiById(this.data.apiId)));
    } else {
      const newApi = new Api();
      newApi.clear();
      this.api$ = of(newApi);
    }
    this.api$.subscribe(res => {
      if (!res) {
        return;
      }
      this.api = new Api();
      this.api.id = res.id;
      this.api.payload = res.payload;
    });
  }

  prepareApi(): Api {
    const _api = new Api();
    _api.id = this.api.id;
    _api.payload = this.api.payload;
    return _api;
  }
}
