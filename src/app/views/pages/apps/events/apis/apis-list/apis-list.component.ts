import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {LayoutUtilsService, MessageType, QueryParamsModel, QueryResultsModel} from '../../../../../../core/_base/crud';
import {TranslateService} from '@ngx-translate/core';
import {ApiEditComponent} from '../api-edit/api-edit.component';
import { ApiViewComponent } from '../api-view/api-view.component';
import {AllCompaniesRequested, CompaniesService, CompanyModel, selectCompaniesInStore} from '../../../../../../core/management';
import {
  Api,
  ApiActionTypes,
  ApisDataSource,
  ApisPageRequested,
  selectApiError,
  selectApiLatestSuccessfullAction,
  selectApisPageLastQuery
} from 'src/app/core/events';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-apis-list',
  templateUrl: './apis-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApisListComponent implements OnInit, OnDestroy {
  dataSource: ApisDataSource;
  displayedColumns = ['api_name', 'company_id' ,'http_method', 'url','description', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = '';
  filterCondition = '';
  lastQuery: QueryParamsModel;
  selection = new SelectionModel<Api>(true, []);
  apisResult: Api[] = [];
  users: any[];
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  private subscriptions: any[] = [];
  companies: CompanyModel[]
  filterCompany = '';

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private companiesService: CompaniesService
  ) {}

  ngOnInit() {
    this.store.dispatch(new AllCompaniesRequested());
    const companiesSubscription = this.store.pipe(
      select(selectCompaniesInStore),
    ).subscribe((response: QueryResultsModel) => {
      if (response.totalCount <= 0) {
        this.companiesService.getAllCompanies().subscribe(res => {
          this.companies = res;
        });
      } else {
        this.companies = response.items
      }
    });
    this.subscriptions.push(companiesSubscription);
    const errorSubscription = this.store.pipe(select(selectApiError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectApiLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch(res.action_type) {
          case ApiActionTypes.ApiCreated :
            successMsg = this.translate.instant('API.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case ApiActionTypes.ApiUpdated :
            successMsg = this.translate.instant('API.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
        }
        this.layoutUtilsService.showActionNotification(
          successMsg,
          msgType,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(successSubscription);
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadApisList())
    )
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadApisList();
      })
    ).subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new ApisDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.apisResult = res;
    });
    this.subscriptions.push(entitiesSubscription);
    const lastQuerySubscription = this.store.pipe(select(selectApisPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push(lastQuerySubscription);

    // Read from URL itemId, for restore previous state
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery);
      }
      this.loadApisList();
    });
    this.subscriptions.push(routeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loadApisList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new ApisPageRequested({page: queryParams}));
    this.selection.clear();
  }

  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.is_active = this.filterStatus;
    filter.searchData = searchText
    filter.name = searchText;
    filter.email = searchText;
    filter.company = this.filterCompany
    return filter;
  }

  restoreState(queryParams: QueryParamsModel) {

    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }

    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  getItemCssClassByStatus(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return 'danger';
      case true:
        return 'success';
    }
  }

  getItemStatusString(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  addApi() {
    const newApi = new Api();
    newApi.clear(); // Set all defaults fields
    this.updateApi(newApi);
  }

  updateApi(api: Api) {
    this.dialog.open(ApiEditComponent, {data: {api, companies: this.companies}});
  }

  getCompanyName(id: number): string {
    if (this.companies !== null && this.companies !== undefined) {
      if (this.companies.filter(item => item.id === id)[0] !== null && this.companies.filter(item => item.id === id)[0] !== undefined) {
        return this.companies.filter(item => item.id === id)[0].name;
      }
    }
  }

  viewApi(api: Api) {
    this.dialog.open(ApiViewComponent, { data: { apiId: api.id } });
  }
}
