import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {Update} from '@ngrx/entity';
import {select, Store} from '@ngrx/store';
import {User} from 'src/app/core/auth';
import {TranslateService} from '@ngx-translate/core';
import {AppState} from 'src/app/core/reducers';
import {ApiOnServerCreated, ApiOnServerUpdated} from 'src/app/core/events/_actions/api.actions';
import {CompanyModel} from '../../../../../../core/management';
import {QueryResultsModel} from '../../../../../../core/_base/crud';
import {
  Api,
  EventService,
  selectApiLatestSuccessfullAction,
  selectApisActionLoading,
  selectEventsInStore
} from '../../../../../../core/events';
import {skip} from 'rxjs/operators';
import {environment} from '../../../../../../../environments/environment';

@Component({
  selector: 'kt-api-edit',
  templateUrl: './api-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class ApiEditComponent implements OnInit, OnDestroy {
  api: Api;
  apiForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  currentUser: User;
  companies: CompanyModel[];
  events: Event[];
  subscriptions: Subscription[] = [];
  URLs = []
  environment: any;
  environmentKey = 'environment.'
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ApiEditComponent>,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translate: TranslateService,
    private eventsService: EventService
  ) {
    this.api = this.data.api;
    this.environment = environment
    Object.keys(environment).forEach((key) => {
      const val = '' + this.environment[key]
      if (!val.includes('http')) {
        return
      }
      this.URLs.push({key, val})
    });
  }


  ngOnInit() {
    this.companies = this.data.companies
    const eventsSubscription = this.store.pipe(
      select(selectEventsInStore),
    ).subscribe((response: QueryResultsModel) => {
      if (response.totalCount <= 0) {
        this.eventsService.getAllEvents().subscribe(res => {
          this.events = res[`data`];
        })
      } else {
        this.events = response.items
      }
    });
    this.subscriptions.push(eventsSubscription);
    const loadingSub = this.store.pipe(select(selectApisActionLoading)).subscribe(res => this.viewLoading = res);
    this.subscriptions.push(loadingSub);
    const successSubscription = this.store.pipe(select(selectApiLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(successSubscription);
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    const url = this.URLs.find(element => this.api.url.includes(element.key));
    if (url) {
      this.api = Object.assign({}, this.api, {url: this.api.url.replace(this.environmentKey+url.key, '')})
    }
    this.apiForm = this.fb.group({
      api_name: [this.api.api_name, Validators.required],
      description: [this.api.description, Validators.required],
      payload: [this.api.payload, {validators:[Validators.required,jsonValidator]}],
      company_id: [this.api.company_id, Validators.required],
      url: [this.api.url, Validators.required],
      http_method: [this.api.http_method, Validators.required],
      executionType: [this.api.execution_type],
      baseURL: [url],
      condition:[this.api.condition],
    });
  }

  getTitle() {
    let result = this.translate.instant('API.NEW_API');
    if (!this.api.id) {
      return result;
    }
    result = this.translate.instant('API.EDIT_API') + `-` + `${this.api.api_name}`;
    return result;
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.apiForm.controls[controlName];
    return control.invalid && control.touched;
  }

  prepareApi(): Api {
    const controls = this.apiForm.controls;
    const _api = new Api();
    _api.clear();
    _api.id = this.api.id;
    _api.api_name = controls.api_name.value;
    _api.description = controls.description.value;
    _api.payload = controls.payload.value;
    _api.company_id = controls.company_id.value;
    _api.url = (controls.baseURL.value?this.environmentKey+controls.baseURL.value.key:'') + controls.url.value;
    _api.http_method = controls.http_method.value;
    _api.execution_type = controls.executionType.value;
    _api.condition = controls.condition.value;
    return _api;
  }

  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.apiForm.controls;
    /** check form */
    if (this.apiForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const editedApi = this.prepareApi();
    if (editedApi.id > 0) {
      this.updateApi(editedApi);
    } else {
      this.createApi(editedApi);
    }
  }

  updateApi(_api: Api) {
    _api.is_active = _api.is_active.toString() === 'true';
    const updateApi: Update<Api> = {
      id: _api.id,
      changes: _api
    };
    this.store.dispatch(new ApiOnServerUpdated({
      partialApi: updateApi,
      api: _api
    }));
  }

  createApi(_api: Api) {
    this.store.dispatch(new ApiOnServerCreated({api: _api}));
  }

  onAlertClose() {
    this.hasFormErrors = false;
  }

  getStatusString(): string {
    switch (this.apiForm.get('is_active').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }
}
export function jsonValidator(control: AbstractControl): ValidationErrors | null {
  try {
    JSON.parse(control.value);
  } catch (e) {
    return { jsonInvalid: true };
  }

  return null;
};
