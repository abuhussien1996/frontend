import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {Update} from '@ngrx/entity';
import {select, Store} from '@ngrx/store';
import {User} from 'src/app/core/auth';
import {TranslateService} from '@ngx-translate/core';
import {
  EventAction,
  EventActionOnServerCreated,
  selectEventActionsActionLoading,
  EventActionOnServerUpdated,
  selectEventActionLatestSuccessfullAction
} from 'src/app/core/events';
import {AppState} from 'src/app/core/reducers';
import {CompanyModel} from '../../../../../../core/management';
import { skip } from 'rxjs/operators';

@Component({
  templateUrl: './action-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class EventActionEditComponent implements OnInit, OnDestroy {
  eventAction: EventAction;
  eventActionForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  currentUser: User;
  subscriptions: Subscription[] = [];
  companies: CompanyModel[];

  constructor(
    public dialogRef: MatDialogRef<EventActionEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translate: TranslateService
   ) {}

  ngOnInit() {
    this.eventAction = this.data.eventAction;
    this.companies = this.data.companies;
    const loadingSub = this.store.pipe(select(selectEventActionsActionLoading)).subscribe(res => this.viewLoading = res);
    this.subscriptions.push(loadingSub);
    const successSubscription = this.store.pipe(select(selectEventActionLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(successSubscription);
    this.createForm();
  }

  ngOnDestroy() {
      this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    this.eventActionForm = this.fb.group({
      action_name: [this.eventAction.action_name, Validators.required],
      description: [this.eventAction.description, Validators.required],
      is_active: [this.eventAction.is_active],
      company_id: [this.eventAction.company_id, Validators.required]
    });
  }

  getTitle() {
    let result = this.translate.instant('EVENTACTION.NEW_EVENTACTION');
    if ( !this.eventAction || !this.eventAction.id) {
      return result;
    }
    result = this.translate.instant('EVENTACTION.EDIT_EVENTACTION') + `-` + `${this.eventAction.action_name}`;
    return result;
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.eventActionForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  prepareEventAction(): EventAction {
    const controls = this.eventActionForm.controls;
    const _eventAction = new EventAction();
    _eventAction.clear();
    _eventAction.id = this.eventAction.id;
    _eventAction.action_name = controls.action_name.value;
    _eventAction.company_id = controls.company_id.value;
    _eventAction.is_active = controls.is_active.value;
    _eventAction.description = controls.description.value;
    return _eventAction;
  }

  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.eventActionForm.controls;
    /** check form */
    if (this.eventActionForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    const editedEventAction = this.prepareEventAction();
    if (editedEventAction.id > 0) {
      this.updateEventAction(editedEventAction);
    } else {
      this.createEventAction(editedEventAction);
    }
  }

  updateEventAction(_eventAction: EventAction) {
    _eventAction.is_active = _eventAction.is_active.toString() === 'true' ? true : false;
    const updateEventAction: Update<EventAction> = {
      id: _eventAction.id,
      changes: _eventAction
    };
    this.store.dispatch(new EventActionOnServerUpdated({
      partialEventAction: updateEventAction,
      eventAction: _eventAction
    }));
  }

  createEventAction(_eventAction: EventAction) {
    this.store.dispatch(new EventActionOnServerCreated({eventAction: _eventAction}));
  }

  onAlertClose() {
    this.hasFormErrors = false;
  }

  getStatusString(): string {
    switch (this.eventActionForm.get('is_active').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }
}
