import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
import {LayoutUtilsService, MessageType, QueryParamsModel, QueryResultsModel} from '../../../../../../core/_base/crud';
import {
  EventAction,
  EventActionsDataSource,
  EventActionsPageRequested,
  selectEventActionsPageLastQuery,
  selectEventActionError,
  selectEventActionLatestSuccessfullAction,
  EventActionActionTypes
} from '../../../../../../core/events';
import {TranslateService} from '@ngx-translate/core';
import {EventActionEditComponent} from '../action-edit/action-edit.component';
import {AllCompaniesRequested, CompaniesService, CompanyModel, selectCompaniesInStore} from '../../../../../../core/management';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-actions-list',
  templateUrl: './actions-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventActionsListComponent implements OnInit, OnDestroy {
  dataSource: EventActionsDataSource;
  displayedColumns = [ 'action_name','company_id', 'description', 'is_active', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = '';
  filterCondition = '';
  lastQuery: QueryParamsModel;
  selection = new SelectionModel<EventAction>(true, []);
  eventActionsResult: EventAction[] = [];
  users: any[];
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  private subscriptions: any[] = [];
  filterCompany = '';
  companies: CompanyModel[];

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private companiesService: CompaniesService
  ) {}

  ngOnInit() {
    this.store.dispatch(new AllCompaniesRequested());
    const companiesSubscription = this.store.pipe(
      select(selectCompaniesInStore),
    ).subscribe((response: QueryResultsModel) => {
      if (response.totalCount <= 0) {
        this.companiesService.getAllCompanies().subscribe(res => {
          this.companies = res;
        });
      } else {
        this.companies = response.items;
      }
    });
    this.subscriptions.push(companiesSubscription);
    const errorSubscription = this.store.pipe(select(selectEventActionError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectEventActionLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch(res.action_type) {
          case EventActionActionTypes.EventActionCreated :
            successMsg = this.translate.instant('EVENTACTION.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case EventActionActionTypes.EventActionUpdated :
            successMsg = this.translate.instant('EVENTACTION.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
        }
        this.layoutUtilsService.showActionNotification(
          successMsg,
          msgType,
          3000,
          true,
          false
          );
      }
    });
    this.subscriptions.push(successSubscription);
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadEventActionsList())
    )
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadEventActionsList();
      })
    ).subscribe();
    this.subscriptions.push(searchSubscription);
    // Init DataSource
    this.dataSource = new EventActionsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.eventActionsResult = res;
    });
    this.subscriptions.push(entitiesSubscription);
    const lastQuerySubscription = this.store.pipe(select(selectEventActionsPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push(lastQuerySubscription);

    // Read from URL itemId, for restore previous state
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery);
      }
      this.loadEventActionsList();
    });
    this.subscriptions.push(routeSubscription);
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Load EventActions List
   */
  loadEventActionsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new EventActionsPageRequested({page: queryParams}));
    this.selection.clear();
  }

  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.searchData = searchText
    filter.company = this.filterCompany
    return filter;
  }

  restoreState(queryParams: QueryParamsModel) {
    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }

    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  getItemCssClassByStatus(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return 'danger';
      case true:
        return 'success';
    }
  }

  getItemStatusString(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  addEventAction() {
    const newEventAction = new EventAction();
    newEventAction.clear(); // Set all defaults fields
    this.updateEventAction(newEventAction);
  }

  updateEventAction(eventAction: EventAction) {
    this.dialog.open(EventActionEditComponent, {data: {eventAction, companies: this.companies}});
  }

  getCompanyName(id: number): string {
    if (this.companies !== null && this.companies !== undefined) {
      if (this.companies.filter(item => item.id === id)[0] !== null && this.companies.filter(item => item.id === id)[0] !== undefined) {
        return this.companies.filter(item => item.id === id)[0].name;
      }
    }
  }

}
