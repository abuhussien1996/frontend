import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {TranslateModule} from '@ngx-translate/core';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {PartialsModule} from '../../../partials/partials.module';
import {ModuleGuard} from '../../../../core/auth';
import {HttpUtilsService, InterceptService, LayoutUtilsService, TypesUtilsService} from '../../../../core/_base/crud';
import {
  ActionNotificationComponent,
  ConfirmActionDialogComponent,
  DeleteEntityDialogComponent,
  FetchEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../../../partials/content/crud';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import {environment} from '../../../../../environments/environment';
import {NgbModule, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPermissionsModule} from 'ngx-permissions';
import {MatChipsModule} from '@angular/material/chips';
import {InlineSVGModule} from 'ng-inline-svg/lib_commonjs/inline-svg.module';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

import {EventsComponent} from './events.component';
import {EventActionsListComponent} from './actions/actions-list/actions-list.component';
import {EventActionEffects} from 'src/app/core/events/_effects/action.effects';
import {eventActionsReducer} from 'src/app/core/events/_reducers/action.reducers';
import {EventActionEditComponent} from './actions/action-edit/action-edit.component';
import {WorkflowEditComponent} from './workflows/workflow-edit/workflow-edit.component';
import {WorkflowsListComponent} from './workflows/workflows-list/workflows-list.component';
import {workflowsReducer} from 'src/app/core/events/_reducers/workflow.reducers';
import {WorkflowEffects} from 'src/app/core/events/_effects/workflow.effects';
import {WorkflowService} from 'src/app/core/events/_services/workflow.service';
import {CompaniesService} from '../../../../core/management/_services';
import {EventEditComponent} from './events/event-edit/event-edit.component';
import {EventsListComponent} from './events/events-list/events-list.component';
import {eventsReducer} from 'src/app/core/events/_reducers/event.reducers';
import {EventEffects} from 'src/app/core/events/_effects/event.effects';
import {EventService} from 'src/app/core/events/_services/event.service';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {companiesReducer, CompanyEffects} from '../../../../core/management';
import {WorkflowActionsComponent} from './workflows/_subs/workflow-action-edit/workflow-actions.component';
import {ApisListComponent} from './apis/apis-list/apis-list.component';
import {ApiEditComponent} from './apis/api-edit/api-edit.component';
import {ApiViewComponent} from './apis/api-view/api-view.component';
import {WorkflowActionsListComponent} from './workflows/_subs/workflow-actions-list/workflow-actions-list.component';
import {ViewMapFormioComponent} from './workflows/_subs/view-map-formio/view-map-formio.component';
import {MatFormioModule} from 'angular-material-formio';
import {AuthService} from 'src/app/core/auth/_services';
import {
  ApiEffects,
  ApiService,
  apisReducer,
  EventActionService,
  InboxEffects,
  inboxReducer,
  InboxService,
  WorkflowActionEffects,
  workflowActionsReducer
} from 'src/app/core/events';
import {WorkflowGroupListComponent} from './workflow-group/workflow-group-list/workflow-group-list.component';
import {WorkflowGroupEditComponent} from './workflow-group/workflow-group-edit/workflow-group-edit.component';
import {workflowGroupReducer} from '../../../../core/events/_reducers/workflow-group.reducers';
import {workflowGroupEffects} from '../../../../core/events/_effects/workflow-group.effects';
import {WorkflowGroupService} from '../../../../core/events/_services/workflow-group.service';
import {FormioModule} from 'angular-formio';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import { InboxViewComponent } from './inbox/inbox-view/inbox-view.component';
import { WorkflowDialogComponent } from './inbox/workflow-dialog/workflow-dialog.component';
import { SelectWorkflowDialogComponent } from './inbox/select-workflow-dialog/select-workflow-dialog.component';
import { MailViewDialogComponent } from './inbox/mail-view-dialog/mail-view-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReplyDialogComponent } from './inbox/reply-dialog/reply-dialog.component';
import {UploadManualComponent} from './inbox/upload-manual/upload-manual.component';


const routes: Routes = [
  {
    path: '',
    component: EventsComponent,
    children: [
      {
        path: '',
        redirectTo: 'event-actions',
        pathMatch: 'full'
      }
      , {
        path: 'event-actions',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToActionModule'},
        component: EventActionsListComponent
      }, {
        path: 'event-workflows',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToWorkflowModule'},
        component: WorkflowsListComponent
      }, {
        path: 'event-workflows/actions/:id/:company_id/:workflow_group_id',
        component: WorkflowActionsListComponent
      }, {
        path: 'event-events',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToEventModule'},
        component: EventsListComponent
      }, {
        path: 'apis',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToApiModule'},
        component: ApisListComponent
      },
      {
        path: 'workflow-group',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToWorkflowGroupModule'},
        component: WorkflowGroupListComponent
      },
      {
        path: 'inbox',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToInboxModule'},
        component: InboxViewComponent
      },
      {
        path: 'fax',
        // canActivate: [ModuleGuard],
        data: {filter: 'FAX'},
        component: InboxViewComponent
      },
      {
        path: 'upload',
        // canActivate: [ModuleGuard],
        data: {filter: 'MANUAL'},
        component: InboxViewComponent
      },
      {
        path: 'inbox/:id',
        // canActivate: [ModuleGuard],
        data: {permissionName: 'accessToInboxModule'},
        component: InboxViewComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    MatFormioModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatButtonToggleModule,
    NgbModule,
    MatChipsModule,
    PerfectScrollbarModule,
    MatDialogModule,
    InlineSVGModule,
    CommonModule,
    HttpClientModule,
    PartialsModule,
    NgxPermissionsModule.forChild(),
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatDividerModule,
    NgbProgressbarModule,
    FormioModule,
    NgxExtendedPdfViewerModule,
    DragDropModule,
    StoreModule.forFeature('eventActions', eventActionsReducer),
    EffectsModule.forFeature([EventActionEffects]),
    StoreModule.forFeature('workflows', workflowsReducer),
    EffectsModule.forFeature([WorkflowEffects]),
    StoreModule.forFeature('events', eventsReducer),
    EffectsModule.forFeature([EventEffects]),
    StoreModule.forFeature('events', eventsReducer),
    EffectsModule.forFeature([EventEffects]),
    StoreModule.forFeature('companies', companiesReducer),
    EffectsModule.forFeature([CompanyEffects]),
    StoreModule.forFeature('workflowActions', workflowActionsReducer),
    EffectsModule.forFeature([WorkflowActionEffects]),
    StoreModule.forFeature('apis', apisReducer),
    EffectsModule.forFeature([ApiEffects]),
    StoreModule.forFeature('workflowGroup', workflowGroupReducer),
    EffectsModule.forFeature([workflowGroupEffects]),
    StoreModule.forFeature('inbox', inboxReducer),
    EffectsModule.forFeature([InboxEffects])
  ],
  providers: [
    ModuleGuard,
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },

    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService,
    HttpUtilsService,
    TypesUtilsService,
    LayoutUtilsService,
    CompaniesService,
    DatePipe,
    EventActionService,
    WorkflowService,
    EventService,
    ApiService,
    AuthService,
    WorkflowGroupService,
    InboxService,
  ],
  entryComponents: [
    ActionNotificationComponent,
    ConfirmActionDialogComponent,
    DeleteEntityDialogComponent,
    FetchEntityDialogComponent,
    UpdateStatusDialogComponent,
    EventActionEditComponent,
    WorkflowGroupEditComponent,
    WorkflowEditComponent,
    EventEditComponent,
    WorkflowActionsComponent,
    ApiViewComponent,
    ApiEditComponent,
    ViewMapFormioComponent,
    WorkflowDialogComponent,
    SelectWorkflowDialogComponent,
    MailViewDialogComponent,

    ReplyDialogComponent,
    UploadManualComponent
  ],
  declarations: [
    EventsComponent,
    EventActionsListComponent,
    EventActionEditComponent,
    WorkflowsListComponent,
    WorkflowActionsListComponent,
    WorkflowEditComponent,
    EventsListComponent,
    EventEditComponent,
    WorkflowActionsComponent,
    ApisListComponent,
    ApiEditComponent,
    ApiViewComponent,
    ViewMapFormioComponent,
    WorkflowGroupListComponent,
    WorkflowGroupEditComponent,
    InboxViewComponent,
    UploadManualComponent
  ],
  exports: []
})
export class EventsModule {
}
