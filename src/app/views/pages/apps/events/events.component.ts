import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	templateUrl: './events.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsComponent implements OnInit {
 
	constructor() { }

	ngOnInit() { }
}
