// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip } from 'rxjs/operators';
import { fromEvent, merge, Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../../core/reducers';
// UI
// CRUD
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../../../core/_base/crud';
// Services and Models

import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/core/auth/_services';
import { Update } from '@ngrx/entity';
import { MessagesDataSource } from 'src/app/core/timeline/_data-sources/message.datasource';
import {  selectMessageErrorState } from 'src/app/core/timeline/_selectors/message.selectors';
import { selectMessagesPageLastQuery, selectMessagesActionLoading } from 'src/app/core/timeline/_selectors/message.selectors';
import { MessagesPageRequested, MessageOnServerDeleted, ManyMessagesOnServerDeleted } from 'src/app/core/timeline/_actions/message.actions';
import { MessageViewComponent } from '../message-view/message-view.component';
import { Message } from 'src/app/core/timeline/_models/message.model';


@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-messages-list',
	templateUrl: './messages-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessagesListComponent implements OnInit, OnDestroy {
	// Table fields
	dataSource: MessagesDataSource;
	displayedColumns = ['select', 'to', 'text','actions'];
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild('sort1', {static: true}) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', { static: true }) searchInput: ElementRef;
	filterStatus = '';
	filterCondition = '';
	lastQuery: QueryParamsModel;
	// Selection
	selection = new SelectionModel<Message>(true, []);
	messagesResult: Message[] = [];
	users :any[];
	// Error Handling
	submissionErrorObj: any;
	hasSubmissionErrors: boolean;
	// Subscriptions

	private subscriptions: any[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialog: MatDialog
	 * @param activatedRoute: ActivatedRoute
	 * @param router: Router
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param store: Store<AppState>
	 * @param cdr: ChangeDetectorRef,
	 * @param translate: TranslateService,
	 * @param authService: AuthService
	 */
	constructor(public dialog: MatDialog,
		           private activatedRoute: ActivatedRoute,
		           private router: Router,
		           private layoutUtilsService: LayoutUtilsService,
				   private store: Store<AppState>,
				   private cdr: ChangeDetectorRef,
				   private translate: TranslateService,
				   private authService: AuthService
				   ) {

					   this.authService.getAllUsers().subscribe( res => {
this.users = res

					   })
				    }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		// If the user changes the sort order, reset back to the first page.
		const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
		this.subscriptions.push({id: 1, sub: sortSubscription});

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
			tap(() => this.loadMessagesList())
		)
		.subscribe();
		this.subscriptions.push({id: 2, sub: paginatorSubscriptions});

		// Filtration, bind to searchInput
		const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
			debounceTime(150),
			distinctUntilChanged(),
			tap(() => {
				this.paginator.pageIndex = 0;
				this.loadMessagesList();
			})
		)
		.subscribe();
		this.subscriptions.push({id: 3, sub: searchSubscription});

		// Init DataSource
		this.dataSource = new MessagesDataSource(this.store);
		const entitiesSubscription = this.dataSource.entitySubject.pipe(
			skip(1),
			distinctUntilChanged()
		).subscribe(res => {
			this.messagesResult = res;
		});
		this.subscriptions.push({id: 4, sub: entitiesSubscription});
		const lastQuerySubscription = this.store.pipe(select(selectMessagesPageLastQuery)).subscribe(res => this.lastQuery = res);
		// Load last query from store
		this.subscriptions.push({id: 5, sub: lastQuerySubscription});

		// Read from URL itemId, for restore previous state
		const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
			if (params.id) {
				this.restoreState(this.lastQuery, +params.id);
			}
			this.loadMessagesList();
		});
		this.subscriptions.push({id: 6, sub: routeSubscription});
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => {
			if(el.sub) {
				el.sub.unsubscribe();
			}
		});
	}

	/**
	 * Load Messages List
	 */
	loadMessagesList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		// Call request from server
		// this.store.dispatch(new MessagesPageRequested({ page: queryParams }));
		this.selection.clear();
	}



	/**
	 * Returns object for filter
	 */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;


		filter.is_active = this.filterStatus;
		filter.searchData = searchText
		filter.name = searchText;
		filter.email = searchText;
		filter.company = searchText;

		return filter;
	}

	/**
	 * Restore state
	 *
	 * @param queryParams: QueryParamsModel
	 * @param id: number
	 */
	restoreState(queryParams: QueryParamsModel, id: number) {

		if (!queryParams.filter) {
			return;
		}
		if ('status' in queryParams.filter) {
			this.filterStatus = queryParams.filter.status.toString();
		}

		if (queryParams.filter.model) {
			this.searchInput.nativeElement.value = queryParams.filter.model;
		}
	}

	/** ACTIONS */
	/**
	 * Delete message
	 *
	 * @param _item: Message
	 */
	deleteMessage(_item: Message) {
		const _title = this.translate.instant('DIALOG.DELETE_MESSAGE_SINGLE');
		const _description = this.translate.instant('DIALOG.ARE_YOU_SURE');
		const _waitDesciption = this.translate.instant('DIALOG.DELETING');
		const _deleteMessage = this.translate.instant('DIALOG.HAVE_BEEN_DELETED');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		const dialogRefSubscription = dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.store.dispatch(new MessageOnServerDeleted({ id: _item.id }));
			const actionLoadingSubscription = this.store.pipe(select(selectMessagesActionLoading)
			).subscribe(response => {
				if (response !== true) {
					const errorStateSubscription = this.store.pipe(
						select(selectMessageErrorState)
					).subscribe(error => {
						if(error) {
							if(error.error && error.error.errors) {
								this.submissionErrorObj = error.error.errors;
							} else {
								this.submissionErrorObj = [{error:'GLOBAL.UNSPECIFIED_ERROR'}]
							}
							this.hasSubmissionErrors = true;
						} else {
							this.submissionErrorObj = null;
							this.hasSubmissionErrors = false;
						}
					})
					if (this.subscriptions.find(sub => sub.id === 9) === undefined) {
						this.subscriptions.push({id: 9, sub: errorStateSubscription});
					}
					if(this.submissionErrorObj !== null) {
						const errArray = [];
						this.submissionErrorObj.forEach(errObj => {
							const message = this.translate.instant(errObj.error);
							errArray.push(message);
						});
						const errMsg = errArray.join('\n');
						this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
					} else {
						this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete, 3000, false, false);
						this.loadMessagesList();
					}
				}
				this.cdr.detectChanges();
			});
			if (this.subscriptions.find(sub => sub.id === 8) === undefined) {
				this.subscriptions.push({id: 8, sub: actionLoadingSubscription});
			}
		});
		if (this.subscriptions.find(sub => sub.id === 7) === undefined) {
			this.subscriptions.push({id: 7, sub: dialogRefSubscription});
		}
	}

	/**
	 * Delete selected messages
	 */
	deleteMessages() {
		const _title: string = this.translate.instant('MESSAGE.DELETE_MESSAGE_MULTI.TITLE');
		const _description: string = this.translate.instant('MESSAGE.DELETE_MESSAGE_MULTI.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('MESSAGE.DELETE_MESSAGE_MULTI.WAIT_DESCRIPTION');
		const _deleteMessage = this.translate.instant('MESSAGE.DELETE_MESSAGE_MULTI.MESSAGE');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		const dialogRefSubscription = dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			const idsForDeletion: number[] = [];
			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < this.selection.selected.length; i++) {
				idsForDeletion.push(this.selection.selected[i].id);
			}
			this.store.dispatch(new ManyMessagesOnServerDeleted({ ids: idsForDeletion }));
			const actionLoadingSubscription = this.store.pipe(select(selectMessagesActionLoading)
			).subscribe(response => {
				if (response !== true) {
					const errorStateSubscription = this.store.pipe(
						select(selectMessageErrorState)
					).subscribe(error => {
						if(error) {
							if(error.error && error.error.errors) {
								this.submissionErrorObj = error.error.errors;
							} else {
								this.submissionErrorObj = [{error:'GLOBAL.UNSPECIFIED_ERROR'}]
							}
							this.hasSubmissionErrors = true;
						} else {
							this.submissionErrorObj = null;
							this.hasSubmissionErrors = false;
						}
						this.cdr.detectChanges();
					})
					if (this.subscriptions.find(sub => sub.id === 12) === undefined) {
						this.subscriptions.push({id: 12, sub: errorStateSubscription});
					}
					if(this.submissionErrorObj !== null) {
						const errArray = [];
						this.submissionErrorObj.forEach(errObj => {
							const message = this.translate.instant(errObj.error);
							errArray.push(message);
						});
						const errMsg = errArray.join('\n');
						this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
					} else {
						this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete, 3000, false, false);
						this.loadMessagesList();
					}
					this.selection.clear();
				}
				this.cdr.detectChanges();
			});
			if (this.subscriptions.find(sub => sub.id === 11) === undefined) {
				this.subscriptions.push({id: 11, sub: actionLoadingSubscription});
			}
		});
		if (this.subscriptions.find(sub => sub.id === 10) === undefined) {
			this.subscriptions.push({id: 10, sub: dialogRefSubscription});
		}
	}



	viewMessage(message: Message) {

		const _saveMessage = this.translate.instant('DIALOG.SAVE');
		const _messageType = message.id ? MessageType.Update : MessageType.Create;
		const dialogRef = this.dialog.open(MessageViewComponent, { data: { message } });
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 5000, false, false);
			this.loadMessagesList();
		});
	}

	/**
	 * Redirect to edit page
	 *
	 * @param id: any
	 */
	editMessage(id) {
		this.router.navigate(['timeline/messages/edit/'+ id]);
	}

	createMessage() {
		this.router.navigateByUrl('/timeline/messages/add');
	}

	/**
	 * Check all rows are selected
	 */
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.messagesResult.length;
		return numSelected === numRows;
	}

	/**
	 * Selects all rows if they are not all selected; otherwise clear selection
	 */
	masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.messagesResult.forEach(row => this.selection.select(row));
		}
	}



}
