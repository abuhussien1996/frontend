// Angular
import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
// RxJS
import {Observable, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
// State
import { AppState } from '../../../../../../core/reducers';
import { Permission, AuthService } from 'src/app/core/auth';
import { Message, selectMessageById } from 'src/app/core/timeline';
import {CommunicationsService} from '../../../../../../core/timeline/_services';
import {SharedHttpRequestService} from '../../../../../../core/lists/_services';



@Component({
  selector: 'kt-message-edit',
  templateUrl: './message-view.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class MessageViewComponent implements OnInit, OnDestroy {
  // Public properties
  message: Message;
  message$: Observable<Message>;
  hasFormErrors = false;
  viewLoading = false;
  loadingAfterSubmit = false;
  allPermissions$: Observable<Permission[]>;
  messagePermissions: Permission[] = [];
  attachmentData = {communication_id: 0, type: ''};

    private componentSubscriptions: Subscription;

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<MessageViewDialogComponent>
   * @param data: any
   * @param store: Store<AppState>
   */
  constructor(public dialogRef: MatDialogRef<MessageViewComponent>,
              private auth: AuthService,
              private communicationsService: CommunicationsService,
              private sharedHttpRequestService: SharedHttpRequestService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>) {
  }


  ngOnInit() {
    this.message = this.data.message;
    this.attachmentData = {communication_id: this.data.message.id, type: 'NOTIFICATION'};

  }

  ngOnDestroy() {
    if (this.componentSubscriptions) {
      this.componentSubscriptions.unsubscribe();
    }
  }

  prepareMessage(): Message {
    const _message = new Message();
    _message.id = this.message.id;
    _message.body = this.message.body
    _message.receiver = this.message.receiver
    _message.file = this.message.file

    return _message;
  }

}
