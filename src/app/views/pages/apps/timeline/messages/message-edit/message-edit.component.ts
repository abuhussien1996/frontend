// Angular
import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
// Material
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
// RxJS
import { Observable, BehaviorSubject, forkJoin, of } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../../core/reducers';
// CRUD
import { LayoutUtilsService, MessageType } from '../../../../../../core/_base/crud';
// Services and Models

import { currentUser } from 'src/app/core/auth';
import { TranslateService } from '@ngx-translate/core';
import { MessageService } from 'src/app/core/timeline/_services/message.service';
import { MatChipInputEvent } from '@angular/material/chips/chip-input';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Message } from 'src/app/core/timeline/_models/message.model';
import { startWith, map, tap, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/core/auth/_services';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-message-edit',
	templateUrl: './message-edit.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageEditComponent implements OnInit, OnDestroy {

  showFileLoader = new BehaviorSubject(false);
  totalFileSize = 1;
  uploadRequests: any[];
  clicked=false;
	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 * @param router: Router
	 * @param messageFB: FormBuilder
	 * @param messageService: MessagesService
	 * @param translate: TranslateService,
	 * @param cdr: ChangeDetectorRef
	 * @param dialog: MatDialog
	 */
	constructor(
		public dialogRef: MatDialogRef<MessageEditComponent>,
		private store: Store<AppState>,
		private router: Router,
		private messageFB: FormBuilder,
		private layoutUtilsService: LayoutUtilsService,
		private messageService: MessageService,
		private translate: TranslateService,
		private cdr: ChangeDetectorRef,
		public dialog: MatDialog,
		private userService: UserService) {
		this.store.pipe(select(currentUser)).subscribe(res => this.senderName = res && res.fullName ? res.fullName : '');
	}
	get f() {
		return this.messageForm.controls;
	}
	// Public properties
	message: Message;
	messageId$: Observable<number>;
	companyId: number;
	oldMessage: Message;
	selectedTab = 0;
	loadingSubject = new BehaviorSubject<boolean>(true);
	loading$: Observable<boolean>;
	messageForm: FormGroup;
	hasFormErrors = false;
	availableYears: number[] = [];
	submissionErrorObj: any;
	hasSubmissionErrors: boolean;
	private subscriptions: any[] = [];
	visible = true;
	selectable = true;
	removable = true;
	addOnBlur = true;
	readonly separatorKeysCodes: number[] = [ENTER, COMMA];
	resPersons: any[] = [];
	withPerson: any[] = [];
	deals: any[] = [];
	files: any[] = [];
	myControl = new FormControl();
	options = [];
  callback;
	filteredOptions: Observable<any[]>;
	@ViewChild('receiverInput') receiverInput: ElementRef<HTMLInputElement>;
	@ViewChild('receiverAutocomplete') receiverAutocomplete;
	senderName: string;

	attachments: string[] = [];

  searchUsers = this.userService.searchUsers.bind(this.userService);
  formatNewReceiverFn=((value)=>{
    return value;
  }).bind(this);
  displayFn(val?: any): string | undefined {
    if (!val)
      return '';
    let res = ''
    if (val.fullName) {
      res = val.fullName;
    }
    if (val.name) {
      res += ` <small class="text-muted">${val.name}</small>`
    }
    return res	}

	ngOnInit() {
    this.myControl.valueChanges
        .pipe(
            switchMap(
                value => value && value.length >= 3 ? this.userService.searchUsers(value) : of([])
            )
        )
        .subscribe(result => {
            this.filteredOptions = result.data ? of(result.data) : of([]);
            this.cdr.detectChanges();
        });

		const userSub = this.store.pipe(select(currentUser)).subscribe((res) => {
			if (!res) {
				this.router.navigateByUrl('/timeline/messages');
			}
			this.companyId = res !== undefined ? res.companyID : 1;
		});
		this.subscriptions.push({ id: 1, sub: userSub });
		this.loading$ = this.loadingSubject.asObservable();
		this.message = new Message();
		this.createForm();
		this.loadingSubject.next(false);
	}
	getTitle() {
		let result = this.translate.instant('MESSAGE.NEW_MESSAGE');
		if (!this.message || !this.message.id) {
			return result;
		}

		result = this.translate.instant('MESSAGE.EDIT_MESSAGE') + ` - ${this.message.id}`;
		return result;
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		this.cdr.detach();
		this.subscriptions.forEach(el => {
			if (el.sub) {
				el.sub.unsubscribe();
			}
		});
	}

	/**
	 * Create form
	 */
	createForm() {
		this.messageForm = this.messageFB.group({

			receiver: [[], this.chipListValidator()],
			body: [this.message.body, Validators.required],
			file: [''],
		});
	}

  selectedReceiver(value) {
    // tslint:disable-next-line:no-shadowed-variable
		const obj = value.map(value => {return{
			id: value.id,
			email: value.email,
			type: 'USER',
			name: value.fullName
		}});
		this.options = (obj);
		this.messageForm.get('receiver').setValue(this.options);
		this.myControl.setValue(null);
	}

	/**
	 * Save data
	 *
	 * @param withBack: boolean
	 */
	onSubmit(withBack: boolean = false) {
		this.hasFormErrors = false;
		const controls = this.messageForm.controls;
		/** check form */
    this.receiverAutocomplete.updateValueAndValidity();
		if (this.messageForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			this.selectedTab = 0;
			return;
		}
    this.clicked = true;
		this.addMessage(this.prepareMessage());
	}

	/**
	 * Returns object for saving
	 */
	prepareMessage(): Message {
		const controls = this.messageForm.controls;
		const _message = new Message();
		_message.id = this.message.id;
		_message.receiver = controls.receiver.value;
		_message.body = controls.body.value;
    _message.file = [controls.file.value.replace('C:\\fakepath\\', '')];
		return _message;
	}

	/**
	 * Add message
	 *
	 * @param _message: Message
	 * @param withBack: boolean
	 */
	addMessage(_message: Message) {



		const request = {
			body: {
				attachment_names: this.attachments,
				sender_name: this.senderName,
				subject: 'message'
			}
		}
		this.messageService.createMessage(request, _message).subscribe((res) => {

			console.log(res.body);

			if (res.body) {

				if (this.files.length === 0) {
					// tslint:disable-next-line:max-line-length
					this.layoutUtilsService.showActionNotification(this.translate.instant('MESSAGE.ADDED'), MessageType.Update, 3000, false, false);
					this.dialogRef.close();
				}

				const obs = [];
				const failed = [];
				const uuids = res.body.data.pre_signed_urls.map(x => x.uuid);
				// tslint:disable-next-line:prefer-for-of
				for (let i = 0; i < this.files.length; i++) {
					obs.push(this.messageService.uploadFiles(res.body.data.pre_signed_urls[i].url, this.files[i]));
          this.totalFileSize += this.files[i].size;
				}
				this.uploadRequests = obs;
        this.callback = this.sendFiles.bind(this, res.body.data.id, uuids);
        this.showFileLoader.next(true);

        // forkJoin(...obs).subscribe(results => {
				// 	results.forEach((obj, index) => {
				// 		if (obj == null) {
				// 			uuids.push(res.body.data.pre_signed_urls[index].uuid);
				// 		}
				// 	});
				// 	this.sendFiles(res.body.data.id, uuids);
				// });
			}
		})



	}
	sendFiles(id, uuids) {
		this.messageService.sendFiles(id, uuids).subscribe((res) => {
			console.log(res);
			if (res === null) {
				// tslint:disable-next-line:max-line-length
				this.layoutUtilsService.showActionNotification(this.translate.instant('MESSAGE.ADDED'), MessageType.Update, 3000, false, false);
				this.dialogRef.close();
			}
		});
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.messageForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	add(event: MatChipInputEvent, list: any[]): void {
		const input = event.input;
		const value = event.value;
		if ((value || '').trim()) {
			list.push({ name: value.trim() });
		}
		if (input) {
			input.value = '';
		}
		console.log(list)
	}

	onFileChange(event) {
		for (const file of event.target.files) {
			this.attachments.push(file.name);
			this.files.push(file);
		}
	}

	close() {
		this.dialogRef.close();
	}

    remove(entity: string): void {
        const index = this.options.indexOf(entity);
		if (index >= 0) {
			this.options.splice(index, 1);
			this.messageForm.get('receiver').setValue(this.options);
		}
    }

    chipListValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            return !control.value || control.value.length === 0 ? { chipListRequiredError: true } : null;
        };
    }

  callbackOnFileLoad() {
    this.callback();
  }
}
