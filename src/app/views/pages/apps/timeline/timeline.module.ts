// Angular
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
// Fake API Angular-in-memory
// Translate Module
import {TranslateModule} from '@ngx-translate/core';
// NGRX
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
// UI
import {PartialsModule} from '../../../partials/partials.module';
// Auth
import {ModuleGuard} from '../../../../core/auth';
// Core => Services
// Core => Utils
import {HttpUtilsService, InterceptService, LayoutUtilsService, TypesUtilsService} from '../../../../core/_base/crud';
// Shared
import {
  ActionNotificationComponent,
  ConfirmActionDialogComponent,
  DeleteEntityDialogComponent,
  FetchEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../../../partials/content/crud';


// Material
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {NgbModule, NgbProgressbarModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPermissionsModule} from 'ngx-permissions';
import {TimelineComponent} from './timeline.component';
import {MatChipsModule} from '@angular/material/chips';
import {MessagesListComponent} from './messages/messages-list/messages-list.component';
import {MeetingEditComponent} from './meetings/meeting-edit/meeting-edit.component';
import {MessageEditComponent} from './messages/message-edit/message-edit.component';
import {MeetingViewDialogComponent} from './meetings/meeting-view/meeting-view-dialog.component';
import {MessageViewComponent} from './messages/message-view/message-view.component';
import {MeetingService} from 'src/app/core/timeline/_services/meeting.service';
import {meetingsReducer} from 'src/app/core/timeline/_reducers/meeting.reducers';
import {MeetingEffects} from 'src/app/core/timeline/_effects/meeting.effects';
import {MessageEffects} from 'src/app/core/timeline/_effects/message.effects';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {MessageService, messagesReducer} from 'src/app/core/timeline';
import {SendEmailFaxComponent} from './send-email-fax/send-email-fax.component';
import {ContactsService} from 'src/app/core/lists/_services';
import {CommunicationsService} from 'src/app/core/timeline/_services';
import {UserService} from 'src/app/core/auth/_services';
import {AttachmentsListComponent} from './attachments/attachments-list/attachments-list.component';
import {ViewCommunicationComponent} from './view-communication/view-communication.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {TwixorComponent} from './twixor/twixor.component';
import {InlineSVGModule} from 'ng-inline-svg';
import {CreateContactDialogComponent} from '../../../partials/content/general/contact-add-dialog/contact-add.dialog.component';
import {ContactEffects, contactsReducer} from '../../../../core/lists';


// tslint:disable-next-line:class-name
const routes: Routes = [
  {
    path: '',
    component: TimelineComponent,
    // canActivate: [ModuleGuard],
    // data: { moduleName: 'ecommerce' },
    children: [
      {
        path: '',
        redirectTo: 'meetings',
        pathMatch: 'full'
      },
      {
        path: 'messages/add',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canSendMessage'},
        component: MessageEditComponent
      },
      // {
      // 	path: 'messages/add:id',
      // 	component: MessageEditComponent
      // },
      {
        path: 'messages/edit',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canSendMessage'},
        component: MessageEditComponent
      },
      {
        path: 'messages/edit/:id',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canSendMessage'},
        component: MessageEditComponent
      },
      {
        path: 'send/email',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canSendEmail'},
        component: SendEmailFaxComponent
      },
      {
        path: 'send/fax',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canSendFax'},
        component: SendEmailFaxComponent
      },
      {
        path: 'chat',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToChatModule'},
        component: TwixorComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    NgbModule,
    MatDialogModule,
    CommonModule,
    MatChipsModule,
    HttpClientModule,
    PerfectScrollbarModule,
    PartialsModule,
    NgxPermissionsModule.forChild(),
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatButtonToggleModule,
    NgbProgressbarModule,
    NgbTooltipModule,
    StoreModule.forFeature('contacts', contactsReducer),
    EffectsModule.forFeature([ContactEffects]),
    StoreModule.forFeature('meetings', meetingsReducer),
    EffectsModule.forFeature([MeetingEffects]),
    StoreModule.forFeature('messages', messagesReducer),
    EffectsModule.forFeature([MessageEffects]),
    InlineSVGModule
  ],
  providers: [
    DatePipe,
    ModuleGuard,
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService,
    HttpUtilsService,
    MeetingService,
    MessageService,
    TypesUtilsService,
    LayoutUtilsService,
    DatePipe,
    ContactsService,
    UserService,
    CommunicationsService,

  ],
  entryComponents: [
    ActionNotificationComponent,
    ConfirmActionDialogComponent,
    DeleteEntityDialogComponent,
    FetchEntityDialogComponent,
    UpdateStatusDialogComponent,
    MeetingViewDialogComponent,
    MessageViewComponent,
    ViewCommunicationComponent,
    CreateContactDialogComponent
  ],
  declarations: [
    AttachmentsListComponent,
    TimelineComponent,
    MeetingEditComponent,
    MeetingViewDialogComponent,
    MessageViewComponent,
    MessageEditComponent,
    MessagesListComponent,
    SendEmailFaxComponent,
    ViewCommunicationComponent,
    TwixorComponent,
  ],
  exports: [
    AttachmentsListComponent,
    MeetingViewDialogComponent,
    ViewCommunicationComponent
  ]
})
export class TimelineModule {
}
