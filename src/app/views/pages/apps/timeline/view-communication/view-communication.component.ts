import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {
  SharedHttpRequestService
} from '../../../../../core/lists/_services/shared-http-request.service';
import {TaskService} from '../../../../../core/timeline/_services';
import {ContactsService} from '../../../../../core/lists/_services';
import {forkJoin, of} from 'rxjs';
import * as _ from 'lodash';
import {Task} from '../../../../../core/timeline';
import {CreateTaskComponent} from '../../../calendar/create-task/create-task.component';
import {CreateWorkflowDialogComponent} from '../../../workflow/create-workflow-dialog/create-workflow-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'kt-view-communication',
  templateUrl: './view-communication.component.html',
  styleUrls: ['./view-communication.component.scss']
})
export class ViewCommunicationComponent implements OnInit {
  attachmentData = {};
  session
  entitiesObj = []
  contactsObj = []
  drugsObj = []
  entitiesGroupObj: {} = []
  private returnUrl: string;

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<ViewDialogComponent>
   * @param data: any
   * @param store: Store<AppState>
   */

  constructor(public dialogRef: MatDialogRef<ViewCommunicationComponent>,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private sharedHttpRequestService: SharedHttpRequestService,
              private taskService: TaskService,
              private cdr: ChangeDetectorRef,
              private contactsService: ContactsService,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.returnUrl = params.returnUrl || '/';
    });
    if (data && data.type === 'session') {
      this.session = data.session
    }
  }

  ngOnInit() {
    if (this.data && this.data.session) {
      if (this.session.logs) {
        this.getAllExistEntities()
      }
      return;
    }
    if (this.data && (this.data.received_id || this.data.communicationType === 'REPLY' || this.data.communicationType === 'RECEIVED')) {
      this.attachmentData = {
        received_id: this.data.id,
        type: this.data.received_id ? 'RECEIVED' : this.data.communicationType,
        contact_id: this.data.contactId
      };
    } else if (this.data) {
      this.attachmentData = {communication_id: this.data.id, type: this.data.communicationType};
    }
    return
  }

  getTitle() {
    if (this.session)
      return 'Session Information'
    return 'Communication Information'
  }

  getAllExistEntities() {
    let relatedTo = []
    const _entityIds = []
    const _contactIds = []
    const _drugIds = []
    this.session.logs.forEach((log) => {
      relatedTo = relatedTo.concat(log.related_to)
    });
    if (relatedTo.length > 0) {
      relatedTo.forEach((related) => {
        if (related.type === 'ENTITY') {
          _entityIds.push(related.related_id)
        }
        if (related.type === 'CONTACT') {
          _contactIds.push(related.related_id)
        }
        if (related.type === 'DRUG') {
          _drugIds.push(related.related_id)
        }
      });
    }
    forkJoin({
      requestOne: _entityIds.length > 0 ? this.taskService.getEntities(_entityIds) : of([]),
      requestTwo: _contactIds.length > 0 ? this.contactsService.getContactsByIds(_contactIds) : of([]),
    })
      .subscribe(({requestOne, requestTwo}) => {
        if (requestOne.data) {
          this.entitiesObj = []
          this.entitiesGroupObj = []
          this.entitiesObj = requestOne.data
          this.entitiesGroupObj = _.mapValues(_.groupBy(this.entitiesObj, 'entity_type'),
            list => list.map(type => _.omit(type, 'entity_type')));
        }
        if (requestTwo.data)
          requestTwo.data.forEach(element => {
            this.contactsObj.push(element)
            this.entitiesGroupObj['CONTACT'] = this.contactsObj
          });
        this.cdr.detectChanges()
      });
  }

  mapArray(entities, type) {
    const res = [];
    if (!entities) {
      return res;
    } else {
      entities.forEach(item => {
        if (!this.entitiesGroupObj.hasOwnProperty(type))
          return
        const entity = this.entitiesGroupObj[type].find(e => e.id === item.related_id)
        if (entity)
          res.push(entity)
      })
      return res;
    }
  }

  getTip(last, first) {
    let res = ''
    if (last) {
      res = res + last
    }
    if (first) {
      res = res + ' ' + first
    }
    return res
  }

  openSession(type, obj) {
    const _id = obj.id
    this.dialogRef.close();
    switch (type) {
      case 'TASK':
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.closeOnNavigation = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '40vw';
        const task = new Task()
        dialogConfig.data = {mode: 'update', task}
        const dialogRef = this.dialog.open(CreateTaskComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(res => {
          return;
        })
        return;
      case 'WORKFLOW':
        const dialogWorkflow = this.dialog.open(CreateWorkflowDialogComponent, {
          data: {
            workflow: undefined,
            isNew: false,
            header: undefined,
            statuses: undefined,
          },
          width: '100vw',
          maxWidth: 'unset !important',
          disableClose: true,
          hasBackdrop: true,
          closeOnNavigation: true,
          autoFocus: true
        });

        dialogWorkflow.afterClosed().subscribe(result => {
          if (result && (result.reloadWorkflow || result.cancelledWorkflowId)) {
            return;
          }
        });
        return;
      case 'CONTACT':
        this.router.navigate(['lists/contacts/edit/' + obj.entity_id], {
          queryParams: {viewContext: 'CONTACT'}})
        return;
    }
  }

  openRelatedTo(type, obj) {
    const id = obj.id
    this.dialogRef.close();
    switch (type) {
      case 'DRUG':
        this.router.navigate(['lists/drugs/edit/' + id], {relativeTo: this.activatedRoute});
        return;
      case 'PHARMACY_GROUP':
        this.router.navigate(['lists/pharmacy-groups/edit/' + id], {relativeTo: this.activatedRoute});
        return;
      case 'PHARMACY':
        this.router.navigate(['lists/pharmacies/edit/' + id], {relativeTo: this.activatedRoute});
        return;
      case 'CLIENT':
        this.router.navigate(['lists/clients/edit/' + id], {
          queryParams: {relativeTo: this.activatedRoute}
        });
        return;
      case 'FACILITY':
        this.router.navigate(['lists/facilities/edit', id], {
          queryParams: {relativeTo: this.activatedRoute}
        });
        return;
      case 'DOCTOR':
        this.router.navigate(['lists/doctors/edit/' + id], {
          queryParams: {relativeTo: this.activatedRoute}
        });
        return;
      case 'CONTACT':
        this.router.navigate(['lists/contacts/edit/' + id], {
          queryParams: {viewContext: 'CONTACT'}})
        return;
    }
  }
}
