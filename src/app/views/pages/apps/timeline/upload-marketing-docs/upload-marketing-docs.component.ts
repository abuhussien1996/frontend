import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BehaviorSubject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {CommunicationsService} from 'src/app/core/timeline/_services';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud/utils/layout-utils.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'kt-upload-marketing-docs',
  templateUrl: './upload-marketing-docs.component.html'
})
export class UploadMarketingDocsComponent implements OnInit {
  file: File;
  failed: string[] = [];
  attachment: string;
  hasFormErrors: boolean;
  uploadDocsForm: FormGroup;

  @ViewChild('fileInput') fileInput: ElementRef;
  showFileLoader = new BehaviorSubject(false);
  totalFileSize = 1;
  uploadRequests: any[];
  clicked = false
  callback;

  ngOnInit() {
    this.createForm();
  }

  constructor(
    public dialogRef: MatDialogRef<UploadMarketingDocsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private uploadDocsFB: FormBuilder,
    private communicationsService: CommunicationsService,
    private translate: TranslateService,
    private layoutUtilsService: LayoutUtilsService
  ) {
  }

  onFileChange(event) {
    this.attachment = event.target.files[0].name;
    this.file = event.target.files[0];
  }

  save() {
    const controls = this.uploadDocsForm.controls;
    const request = {
      body: {
        topic: controls.topic.value,
        description: controls.description.value,
        attachment_name: this.attachment
      }
    }
    this.communicationsService.uploadDocs(request).subscribe((res) => {
      if (res.data) {
        const obs = [];
        const id = res.data.id;
        const uuid = res.data.pre_signed_url.uuid;
        obs.push(this.communicationsService.uploadFiles(res.data.pre_signed_url.url, this.file));
        this.totalFileSize += this.file.size;
        this.uploadRequests = obs;
        this.callback = this.sendFiles.bind(this, id, uuid);
        this.showFileLoader.next(true);
      }
    })

  }

  upload(url, file) {
    return this.communicationsService.uploadFiles(url, file);
  }

  sendFiles(id, uuid) {
    this.communicationsService.updateDoc(id, uuid).subscribe((result) => {
      if (result === null) {
        this.dialogRef.close(true);
      } else {
        // tslint:disable-next-line:max-line-length
        this.layoutUtilsService.showActionNotification(this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'), MessageType.Update, 3000, false, false);
      }
    });
  }

  getTitle(): string {
    return this.translate.instant('UPLOAD_MARKETING_DOCS.UPLOAD_MARKETING_DOCS');
  }

  close() {
    this.dialogRef.close();
  }

  resetFileInput() {
    this.attachment = null;
    this.file = null;
    this.fileInput.nativeElement.value = [];
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.uploadDocsForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  createForm() {
    this.uploadDocsForm = this.uploadDocsFB.group({
      topic: ['', Validators.required],
      description: ['', Validators.required],
      file: [null, Validators.required]
    });
  }

  submit() {
    this.hasFormErrors = false;
    const controls = this.uploadDocsForm.controls;
    /** check form */
    if (this.uploadDocsForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    this.clicked = true;
    this.save();
  }

  callbackOnFileLoad() {
    this.callback();
  }
};
