import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {ContactsService} from 'src/app/core/lists/_services';
import {TranslateService} from '@ngx-translate/core';
import {CommunicationsService, TaskService} from 'src/app/core/timeline/_services';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud/utils/layout-utils.service';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {currentUser} from 'src/app/core/auth';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import * as _ from 'lodash';
import {CommunicationService} from '../../../../../core/services/communication.service';
import {GeneralAutocompleteComponent} from '../../../../partials/layout/general-autocomplete/general-autocomplete.component';

@Component({
  selector: 'kt-send-email-fax',
  templateUrl: './send-email-fax.component.html',
  styleUrls: ['./send-email-fax.component.scss']
})
export class SendEmailFaxComponent implements OnInit,AfterViewInit {
  separatorKeysCodes: number[] = [ENTER, COMMA];
  recipientCtrl = new FormControl();
  topicCtrl = new FormControl();
  filteredRecipients: Observable<any[]>;
  filteredTopics: Observable<any[]>;
  recipients = [];
  topics = [];
  communicationType;
  files = [];
  failed: string[] = [];
  attachments: string[] = [];
  senderName: string;
  hasFormErrors: boolean;
  emailFaxForm: FormGroup;
  sendDocs: boolean;
  formatNewRecipientFn = ((value) => {
    return value;
  }).bind(this);
  formatNewTopicFn = ((value) => {
    return value;
  }).bind(this);
  filterRecipientFn = this.autocompleteFormatter.bind(this);
  filterTopicFn = ((value) => {
      if (!value)
        return '';
      return (value.description || '').substring(0, 50);
    }
  )
  getDocs = this.communicationsService.getDocs.bind(this.communicationsService)
  searchContacts = this.contactsService.searchContacts.bind(this.contactsService)
  inboxAutocompleteCall = this.communicationService.loadAllInboxesWithFax.bind(this.communicationService);
  taskAutocompleteCall = this.taskService.autocomplateTask.bind(this.taskService);
  @ViewChild('inboxAutocomplete') inboxAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('recipientInput') recipientInput: ElementRef<HTMLInputElement>;
  @ViewChild('topicInput') topicInput: ElementRef<HTMLInputElement>;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('recipientAutocomplete') recipientAutocomplete;
  @ViewChild('topicAutocomplete') topicAutocomplete;
  @ViewChild('taskAutocomplete') taskAutocomplete;
  showFileLoader = new BehaviorSubject(false);
  totalFileSize = 1;
  uploadRequests: any[];
  clicked = false
  callback;
  pp =[]

  ngOnInit() {
    this.communicationType = this.data.communicationType;
    this.sendDocs = this.data.sendDocs;
    this.createForm();
  }

  ngAfterViewInit() : void{
  }
  constructor(
    private taskService: TaskService,
    public dialogRef: MatDialogRef<SendEmailFaxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private emailFaxFB: FormBuilder,
    private contactsService: ContactsService,
    private communicationsService: CommunicationsService,
    private translate: TranslateService,
    private layoutUtilsService: LayoutUtilsService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    public dialog: MatDialog,
    private communicationService: CommunicationService
  ) {
    this.store.pipe(select(currentUser)).subscribe(res => this.senderName = res && res.fullName ? res.fullName : '');
  }

  removeRecipient(recipient: string): void {
    const index = this.recipients.indexOf(recipient);
    if (index >= 0) {
      this.recipients.splice(index, 1);
      this.emailFaxForm.get('recipients').setValue(this.recipients);
    }
  }

  selectedRecipient(value): void {
    this.pp = value
    let newRecipient;
    switch (this.communicationType) {
      case 'EMAIL':
        // tslint:disable-next-line:no-shadowed-variable
        newRecipient = value.map(value => {
          return {
            id: value.id,
            email: value.email,
            type: 'CONTACT',
            name: value.first_name + ' ' + value.last_name
          }
        });
        break;
      case 'FAX':
        // tslint:disable-next-line:no-shadowed-variable
        newRecipient = value.map(value => {
          return {
            id: value.id,
            email: value.fax,
            type: 'CONTACT',
            name: value.first_name + ' ' + value.last_name
          }
        });
        break;
    }
    if (this.sendDocs) {
      // tslint:disable-next-line:no-shadowed-variable
      newRecipient = value.map(value => {
        return {
          id: value.id,
          email: value.email,
          type: 'CONTACT',
          name: value.first_name + ' ' + value.last_name
        }
      });
    }
    this.recipients = newRecipient;
    this.emailFaxForm.get('recipients').setValue(this.recipients);
    this.recipientCtrl.setValue(null);
  }

  removeTopic(topic): void {
    const index = this.topics.indexOf(topic);
    if (index >= 0) {
      this.topics.splice(index, 1);
    }
  }

  selectedTopic(value): void {
    this.topics = (value);
    this.topicCtrl.setValue(null);
  }

  filter(arr) {
    return of(arr.filter((objA) => {
      return !this.recipients.find((objB) => {
        return objA.id === objB.id
      })
    }));
  }

  onFileChange(event) {
    this.attachments = [];
    this.files = [];
    for (const file of event.target.files) {
      this.attachments.push(file.name);
      this.files.push(file);
    }
  }
  sendA() {
    const controls = this.emailFaxForm.controls;
    const inbox =  controls.inbox.value ? { id :controls.inbox.value.id }:null
    const emailRequest = {
      communication_type: this.communicationType,
      body: {
        subject: controls.subject.value,
        body: controls.body.value,
        attachment_names: this.attachments,
        receivers: controls.recipients.value,
        sender_name: this.senderName,
        task:controls.task.value
      }
    }
    let faxRequest
    if(this.communicationType === 'FAX'){
      faxRequest = {
        communication_type: this.communicationType,
        body: {
          subject: controls.subject.value,
          body: controls.body.value,
          attachment_names: this.attachments,
          receivers: controls.recipients.value,
          sender_name: this.senderName,
          task:controls.task.value,
          inbox
        }
      }
    }
    const request = this.communicationType === 'FAX' ? faxRequest:emailRequest
    this.communicationsService.sendEmailFax(request).subscribe((res) => {
      if (res.data) {
        if (this.files.length === 0) {
          // tslint:disable-next-line:max-line-length
          this.layoutUtilsService.showActionNotification(this.translate.instant('EMAIL_FAX.SUCCESS'), MessageType.Update, 3000, false, false);
          this.dialogRef.close();
        }
        const obs = [];
        const uuids = res.data.pre_signed_urls.map(x => x.uuid);
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.files.length; i++) {
          obs.push(this.communicationsService.uploadFiles(res.data.pre_signed_urls[i].url, this.files[i]));
          this.totalFileSize += this.files[i].size;
        }
        this.uploadRequests = obs;
        this.callback = this.sendFiles.bind(this, res.data.id, uuids);
        this.showFileLoader.next(true);
      }
    })
  }

  sendB() {
    const controls = this.emailFaxForm.controls;
    const request = {
      communication_type: this.communicationType,
      body: {
        subject: controls.subject.value,
        body: controls.body.value,
        uuids: this.topics.map(topic => topic.uuid),
        receivers: controls.recipients.value,
        sender_name: this.senderName,
        task:controls.task.value
      }
    }
    this.communicationsService.sendDocuments(request).subscribe((res) => {
      if (res.data) {
        this.sendFiles(res.data.id, request.body.uuids);
      } else {
        // tslint:disable-next-line:max-line-length
        this.layoutUtilsService.showActionNotification(this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'), MessageType.Update, 3000, false, false);
      }
    });
  }

  upload(url, file) {
    return this.communicationsService.uploadFiles(url, file);
  }

  sendFiles(id, uuids) {
    this.communicationsService.sendFiles(id, uuids).subscribe((res) => {
      if (res === null) {
        this.layoutUtilsService.showActionNotification(this.translate.instant('EMAIL_FAX.SUCCESS'), MessageType.Update, 3000, false, false);
        this.dialogRef.close();
      } else {
        // tslint:disable-next-line:max-line-length
        this.layoutUtilsService.showActionNotification(this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'), MessageType.Update, 3000, false, false);
      }
    });
  }

  getTitle(): string {
    return this.translate.instant('EMAIL_FAX.COMPOSE') + ' ' + this.translate.instant('EMAIL_FAX.' + this.communicationType);
  }

  close() {
    this.dialogRef.close();
  }

  resetFileInput() {
    this.attachments = [];
    this.files = [];
    this.fileInput.nativeElement.value = [];
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.emailFaxForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  chipListValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return !control.value || control.value.length === 0 ? {chipListRequiredError: true} : null;
    };
  }

  createForm() {
    this.emailFaxForm = this.emailFaxFB.group({
      recipients: [[], this.chipListValidator()],
      subject: ['', Validators.required],
      body: [''],
      inbox:[],
      task:['', Validators.required]
    });
  }

  submit() {
    this.hasFormErrors = false;
    const controls = this.emailFaxForm.controls;
    if(this.communicationType === 'FAX')
    this.inboxAutocomplete.updateValueAndValidity();
    this.recipientAutocomplete.updateValueAndValidity();
    this.taskAutocomplete.updateValueAndValidity();

    if (this.sendDocs) {
      this.topicAutocomplete.updateValueAndValidity();
    }
    if (this.emailFaxForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    this.clicked = true;
    if (this.sendDocs) {
      this.sendB();
    } else {
      this.sendA();
    }
  }

  callbackOnFileLoad() {
    this.callback();
  }

  resetAttachments() {
    this.attachments = [];
    this.files = [];
    this.topics = [];
    this.fileInput.nativeElement.value = [];
    this.topicInput.nativeElement.value = null;
    this.topicCtrl.setValue(null);
  }

  autocompleteFormatter(participant) {
    if (!participant)
      return '';
    let name = '';
    let subTitle = '';
    if (participant.first_name) {
      name += participant.first_name;
    }
    if (participant.last_name) {
      name += ' ' + participant.last_name;
    }
    if (this.data.communicationType.localeCompare('EMAIL') === 0 || this.data.communicationType.localeCompare('DOCUMENT') === 0) {
      subTitle = participant.email
    }
    if (this.data.communicationType.localeCompare('FAX') === 0) {
      subTitle = participant.fax
    }
    return `<div>${name} <small class="text-muted">${subTitle}</small></div>`

  }
  filterInboxFn(event: any): string {
    return event ? event.title : '';
  }
  onInboxChange(val) {
    this.emailFaxForm.controls.inbox.setValue(val)
    this.emailFaxForm.controls.inbox.updateValueAndValidity();
  }

  onTaskChange(val) {
    this.emailFaxForm.controls.task.setValue(val)
    this.emailFaxForm.controls.task.updateValueAndValidity();
  }
};
