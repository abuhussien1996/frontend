// Angular
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
// Material
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
// RxJS
import {BehaviorSubject, Observable, of} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
// Services and Models
import {currentUser} from 'src/app/core/auth';
import {TranslateService} from '@ngx-translate/core';
import { Meeting} from 'src/app/core/timeline';
import {MeetingService} from 'src/app/core/timeline/_services/meeting.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {DatePipe} from '@angular/common';
import {switchMap} from 'rxjs/operators';
import {ContactsService} from 'src/app/core/lists/_services';
import {UserService} from 'src/app/core/auth/_services';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud';
import * as moment from 'formiojs/node_modules/moment';
import {CreateContactDialogComponent} from '../../../../../partials/content/general/contact-add-dialog/contact-add.dialog.component';
import {GeneralAutocompleteComponent} from '../../../../../partials/layout/general-autocomplete/general-autocomplete.component';
import * as _ from 'lodash';
import {selectLastCreatedContact} from '../../../../../../core/lists';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-meeting-edit',
  templateUrl: './meeting-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeetingEditComponent implements OnInit, OnDestroy {

  showFileLoader = new BehaviorSubject(false);
  totalFileSize = 1;
  uploadRequests: any[];
  private callback;
  participantTypeCtrl = new FormControl();

  searchParticipantsFn = this.searchParticipants.bind(this);
  searchAffiliatesFn = this.searchAffiliates.bind(this);
  searchAssigneeFn = this.userService.searchUsers.bind(this.userService);
  affiliateTypeCtrl = new FormControl();
  filterEntityFn = this.autocompleteFormatter.bind(this, this.participantTypeCtrl)
  filterAffiliationFn = this.autocompleteFormatter.bind(this, this.affiliateTypeCtrl)

  filterAssigneeFn = ((val) => {
    if (!val)
      return '';
    let res = ''
    if (val.fullName) {
      res = val.fullName;
    }
    if (val.name) {
      res += ` <small class="text-muted">${val.name}</small>`
    }
    return res
  })

  formatNewEntityFn = ((value) => {
    return value;
  }).bind(this)
  formatAssigneeEntityFn = ((value) => {
    return value;
  }).bind(this)

  formatAffiliatesEntityFn = ((value) => {
    return value;
  }).bind(this)

  meeting: Meeting;
  meetingId$: Observable<number>;
  companyId: number;
  oldMeeting: Meeting;
  selectedTab = 0;
  loadingSubject = new BehaviorSubject<boolean>(true);
  loading$: Observable<boolean>;
  meetingForm: FormGroup;
  hasFormErrors = false;
  availableYears: number[] = [];
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  private subscriptions: any[] = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  resPersons: any[] = [];
  withPerson: any[] = [];
  relatedList: any[] = [];
  relatedToList: any[] = [];
  files: any[] = [];
  relates: any[] = []
  is_completedChecked = false;
  is_importantChecked = false;
  sender_name: string;
  participantCtrl = new FormControl();
  filteredParticipants: Observable<any[]>;
  participants = [];
  assigneeCtrl = new FormControl();
  filteredAssignees: Observable<any[]>;
  assignees = [];
  affiliateCtrl = new FormControl();
  filteredAffiliates: Observable<any[]>;
  affiliates = [];
  clicked = false
  newContact
  groups = [{
    type: 'Contacts',
    filteredParticipants: of([])
  }, {
    type: 'Users',
    filteredParticipants: of([])
  }];
  @ViewChild('participantInput') participantInput: ElementRef<HTMLInputElement>;
  @ViewChild('assigneeInput') assigneeInput: ElementRef<HTMLInputElement>;
  @ViewChild('affiliateInput') affiliateInput: ElementRef<HTMLInputElement>;
  @ViewChild('entityAutocomplete') entityAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('affiliatesAutocomplete') affiliatesAutocomplete;
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  attachments: string[] = [];
  private pp = [];

  /**
   * Component constructor
   *
   * @param contactsService: ContactsService
   * @param userService: UserService
   * @param doctorService: DoctorService
   * @param facilitiesService: FacilitiesService
   * @param patientsService: PatientsService
   * @param store: Store<AppState>
   * @param router: Router
   * @param meetingFB: FormBuilder
   * @param meetingService: MeetingService
   * @param translate: TranslateService
   * @param cdr: ChangeDetectorRef
   * @param dialog: MatDialog
   * @param datepipe: DatePipe
   * @param layoutUtilsService: LayoutUtilsService
   */
  constructor(
    public dialogRef: MatDialogRef<MeetingEditComponent>,
    private contactsService: ContactsService,
    private userService: UserService,
    private store: Store<AppState>,
    private router: Router,
    private meetingFB: FormBuilder,
    private meetingService: MeetingService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    public datepipe: DatePipe,
    private layoutUtilsService: LayoutUtilsService
  ) {
    this.participantTypeCtrl.setValue('CONTACT');
    this.affiliateTypeCtrl.setValue('DOCTOR');
    this.participantCtrl.valueChanges
      .subscribe(() => {
        this.searchParticipants();
      });
    this.assigneeCtrl.valueChanges
      .pipe(
        switchMap(
          value => value && value.length >= 3 ? this.userService.searchUsers(value) : of([])
        )
      )
      .subscribe(result => {
        this.filteredAssignees = result.data ? this.filter(result.data, 2) : of([]);
        this.cdr.detectChanges();
      });
    this.affiliateTypeCtrl.valueChanges
      .subscribe(() => {
        this.searchAffiliates();
      });
  }
  ngOnInit() {
    const userSub = this.store.pipe(select(currentUser)).subscribe((res) => {
      if (!res) {
        this.router.navigateByUrl('/timeline/meetings');
      }
      this.companyId = res !== undefined ? res.companyID : 1;
      this.sender_name = res && res.fullName ? res.fullName : ''
    });
    this.subscriptions.push({id: 1, sub: userSub});
    const contactSub = this.store.pipe(select(selectLastCreatedContact)).subscribe(res => this.newContact = res);
    this.subscriptions.push({id: 2, sub: contactSub});
    this.loading$ = this.loadingSubject.asObservable();
    this.loadingSubject.next(true);
    const newMeeting = new Meeting();
    newMeeting.clear();
    this.createForm();
    this.loadingSubject.next(false);
    this.participantTypeCtrl.valueChanges.subscribe(value => {
      // this.entityAutocomplete.setValue([]);
      this.entityAutocomplete.triggerReload();
    })
    this.affiliateTypeCtrl.valueChanges.subscribe(value => {
      // this.affiliatesAutocomplete.setValue([]);
      this.affiliatesAutocomplete.triggerReload();
    })
  }
  get f() {
    return this.meetingForm.controls;
  }

  fileProgress(fileInput: any) {
    this.fileData = (fileInput.target.files[0] as File);
    this.preview();
  }

  preview() {
    // Show preview
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }

  getTitle() {
    return this.translate.instant('MEETING.NEW_MEETING');
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this.cdr.detach();
    this.subscriptions.forEach(el => {
      if (el.sub) {
        el.sub.unsubscribe();
      }
    });
  }

  /**
   * Create form
   */
  createForm() {

    this.meetingForm = this.meetingFB.group({
      from_time: [new Date()],
      to_time: [],
      subject: ['', Validators.required],
      with: [[], this.chipListValidator()],
      responsible: [[]],
      body: [],
      related_to_list: [[]],
      location: [],
      files: [],
      is_completed: [],
      is_important: []
    });
  }

  /**
   * Save data
   *
   * @param withBack: boolean
   */
  onSubmit(withBack: boolean = false) {
    this.hasFormErrors = false;
    const controls = this.meetingForm.controls;
    this.entityAutocomplete.updateValueAndValidity();
    /** check form */
    if (this.meetingForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      this.selectedTab = 0;
      return;
    }
    this.clicked = true;
    this.addMeeting(this.prepareMeeting());
  }

  /**
   * Returns object for saving
   */
  prepareMeeting(): Meeting {
    const controls = this.meetingForm.controls;
    const _meeting = new Meeting();
    _meeting.clear();
    _meeting.id = 0;
    _meeting.from_time = this.datepipe.transform(controls.from_time.value, 'yyyy-MM-dd hh:mm:ss' , 'utc');
    _meeting.to_time = this.datepipe.transform(controls.to_time.value, 'yyyy-MM-dd hh:mm:ss' , 'utc');
    _meeting.subject = controls.subject.value;
    _meeting.location = controls.location.value;
    _meeting.with = controls.with.value;
    _meeting.body = controls.body.value;
    _meeting.responsible = controls.responsible.value;
    _meeting.receivers = controls.with.value.concat(...controls.responsible.value);
    _meeting.is_important = controls.is_important.value;
    _meeting.is_completed = controls.is_completed.value;
    _meeting.related_to_list = controls.related_to_list.value;
    return _meeting;
  }

  /**
   * Add meeting
   *
   * @param _meeting: Meeting
   * @param withBack: boolean
   */
  addMeeting(_meeting: Meeting) {
    const request = {
      body: {
        sender_name: this.sender_name,
        attachment_names: this.attachments,
      }
    }
    this.meetingService.createMeeting(request, _meeting).subscribe((res) => {
      if (res.body) {
        if (this.files.length === 0) {
          // tslint:disable-next-line:max-line-length
          this.layoutUtilsService.showActionNotification(this.translate.instant('MEETING.ADDED'), MessageType.Update, 3000, false, false);
          this.dialogRef.close();
        }
        const obs = [];
        const failed = [];
        const uuids = res.body.data.pre_signed_urls.map(x => x.uuid);
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.files.length; i++) {
          obs.push(this.meetingService.uploadFiles(res.body.data.pre_signed_urls[i].url, this.files[i]));
          this.totalFileSize += this.files[i].size;
        }
        this.uploadRequests = obs;
        this.callback = this.sendFiles.bind(this, res.body.data.id, uuids);
        this.showFileLoader.next(true);

        // forkJoin(...obs).subscribe(results => {
        //   results.forEach((obj, index) => {
        //     if (obj == null) {
        //       uuids.push(res.body.data.pre_signed_urls[index].uuid);
        //     }
        //   });
        //   this.sendFiles(res.body.data.id, uuids);
        // });
      }
    })
  }

  upload(url, file) {
    return this.meetingService.uploadFiles(url, file);
  }

  sendFiles(id, uuids) {
    this.meetingService.sendFiles(id, uuids).subscribe((res) => {
      console.log(res);
      if (res === null) {
        // tslint:disable-next-line:max-line-length
        this.layoutUtilsService.showActionNotification(this.translate.instant('MEETING.ADDED'), MessageType.Update, 3000, false, false);
        this.dialogRef.close();
      }
    });
  }

  onFileChange(event) {
    for (const file of event.target.files) {
      this.attachments.push(file.name);
      this.files.push(file);
    }
  }

  /**
   * Checking control validation
   *
   * @param controlName: string => Equals to formControlName
   * @param validationType: string => Equals to valitors name
   */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.meetingForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }


  selected(type: number, val): void {
    this.pp = val;
    switch (type) {
      case 1:
        const newParticipant = val.map(value => {
          const type = value.type || (this.participantTypeCtrl.value === 'DOCTOR' || this.participantTypeCtrl.value === 'FACILITY' ?
            'ENTITY' : this.participantTypeCtrl.value)

          value.type = type;
          return {
            id: value.id,
            email: value.email,
            type: type,
            name: type === 'USER' ?
              value.fullName
              : type === 'ENTITY' ?
                value.name ? value.name : value.first_name + ' ' + value.last_name :
                value.first_name + ' ' + value.last_name
          }
        });
        this.participants = newParticipant;
        // const newPar = this.meetingForm.get('with').value.concat(this.participants)
        // this.meetingForm.get('with').setValue(_.uniqWith(newPar, _.isEqual))
        this.meetingForm.get('with').setValue(this.participants);
        this.participantCtrl.setValue(null);
        break;
      case 2:
        const newAssignee = val.map(value => {
          return {
            id: value.id,
            email: value.email,
            type: 'RESPONSIBLE_PERSON',
            name: value.fullName
          }
        });
        this.assignees = (newAssignee);
        this.meetingForm.get('responsible').setValue(this.assignees);
        this.assigneeCtrl.setValue(null);
        break;
      case 3:
        const newAffiliate = val.map(value => {
          const type = value.type ||
            (this.affiliateTypeCtrl.value === 'DOCTOR'? 'DOCTOR': this.affiliateTypeCtrl.value === 'FACILITY' ?
            'FACILITY' : this.affiliateTypeCtrl.value)

          value.type = type;
          return {
            id: value.id,
            email: value.email,
            type: type,
            name: type !== 'FACILITY' ?
              value.first_name + ' ' + value.last_name
              : value.name
          }
        });
        this.affiliates= (newAffiliate);
        this.meetingForm.get('related_to_list').setValue(this.affiliates);
        this.affiliateCtrl.setValue(null);
        break;
    }
  }

  filter(arr, type) {
    switch (type) {
      case 1:
        return of(arr.filter((objA) => {
          return !this.participants.find((objB) => {
            return objA.id === objB.id
          })
        }));
      case 2:
        return of(arr.filter((objA) => {
          return !this.assignees.find((objB) => {
            return objA.id === objB.id
          })
        }));
      case 3:
        return of(arr.filter((objA) => {
          return !this.affiliates.find((objB) => {
            return objA.id === objB.id
          })
        }));
    }
    return of([]);
  }

  chipListValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return !control.value || control.value.length === 0 ? {chipListRequiredError: true} : null;
    };
  }


  searchParticipants(term = '') {
    const type = this.participantTypeCtrl.value;
    const value = term;
    // if (value && value.length >= 3) {
    switch (type) {
      case 'USER':
        return this.userService.searchUsers(value);
      case 'CONTACT':
        return this.contactsService.searchContacts(value);

    }

    return of([]);
  }


  searchAffiliates(term = '') {
    const type = this.affiliateTypeCtrl.value;
    const value = term;
    // switch (type) {
    //   case 'DOCTOR':
    //     return this.doctorService.searchDoctors(value)
    //   case 'FACILITY':
    //     return this.facilitiesService.searchFacilities(value)
    //   case 'PATIENT':
    //     return this.patientsService.searchPatients(value)
    // }
    return of([]);
  }

  close() {
    this.dialogRef.close();
  }

  callbackOnFileLoad() {
    this.callback();
  }

  autocompleteFormatter(participantType, participant) {
    if (!participant)
      return '';
    let name = '';
    let subTitle = '';
    if (participant.first_name || participant.last_name) {
      name = participant.first_name + ' ' + participant.last_name;
    } else if (participant.fullName) {
      name = participant.fullName;
    } else {
      name = participant.name;
    }
    if (participant.dob) {
      subTitle = moment(participant.dob).format('l');
    }
    if (participant.name && (participant.first_name || participant.last_name || participant.fullName)) {
      subTitle = participant.name;
    }
    return `<div>${name} <small class="text-muted">${subTitle}</small></div>`

  }
  openContactDialog() {
    const dialogRef = this.dialog.open(
      CreateContactDialogComponent,
      {
        width: '40vw',
        disableClose: true,
        hasBackdrop: false
      });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.added) {
        const newParticipant = [this.newContact].map(value => {
          return {
            id: value.id,
            email: value.email,
            type: 'CONTACT',
            name: value.first_name + ' ' + value.last_name
          }
        });
        console.log(this.meetingForm.get('with').value);
        const newPar = this.meetingForm.get('with').value;
        newPar.push(newParticipant[0])
        this.participants.push(newParticipant[0]);
        const cloneContact = _.clone(this.newContact)
        cloneContact.type='CONTACT'
        this.pp.push(cloneContact)
        this.meetingForm.get('with').setValue(_.uniqWith(newPar, _.isEqual))
        this.entityAutocomplete.setValue(this.pp);
        this.entityAutocomplete.updateValueAndValidity()
        // this.participantCtrl.setValue(null);
        // this.entityAutocomplete.setValue(this.newContact);
        this.cdr.detectChanges();
      }
    });
  }
}
