// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Observable, of, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
// State
import { AppState } from '../../../../../../core/reducers';
import { Permission, AuthService } from 'src/app/core/auth';
import { Meeting, selectMeetingById } from 'src/app/core/timeline';
import {CommunicationsService} from '../../../../../../core/timeline/_services';
import {SharedHttpRequestService} from '../../../../../../core/lists/_services';



@Component({
    selector: 'kt-meeting-edit-dialog',
    templateUrl: './meeting-view-dialog.component.html',
    changeDetection: ChangeDetectionStrategy.Default,
})
export class MeetingViewDialogComponent implements OnInit, OnDestroy {
    // Public properties
    meeting: Meeting;
    meeting$: Observable<Meeting>;
    hasFormErrors = false;
    viewLoading = false;
    loadingAfterSubmit = false;
    allPermissions$: Observable<Permission[]>;
    meetingPermissions: Permission[] = [];

    private componentSubscriptions: Subscription;
    attachmentData = { communication_id: 0, type: '' };

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<ViewDialogComponent>
	 * @param data: any
	 * @param store: Store<AppState>
	 */
  constructor(public dialogRef: MatDialogRef<MeetingViewDialogComponent>,
              private auth: AuthService,
              private communicationsService: CommunicationsService,
              private sharedHttpRequestService: SharedHttpRequestService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>) {
  }


    ngOnInit() {
        this.meeting = this.data.meeting
        this.attachmentData = { communication_id: this.meeting.id, type: 'MEETING' };
    }


    ngOnDestroy() {
        if (this.componentSubscriptions) {
            this.componentSubscriptions.unsubscribe();
        }
    }

    prepareMeeting(): Meeting {
        const _meeting = new Meeting();
        _meeting.id = this.meeting.id;
        _meeting.from_time = this.meeting.from_time
        _meeting.to_time = this.meeting.to_time
        _meeting.subject = this.meeting.subject
        _meeting.location = this.meeting.location
        _meeting.receivers = this.meeting.receivers
        _meeting.body = this.meeting.body
        _meeting.files = this.meeting.files
        _meeting.responsible = this.meeting.responsible

        return _meeting;
    }

    getReceivers(type: number) {
        switch(type) {
            case 1:
                return this.meeting.receivers.filter(r => r.type !== 'RESPONSIBLE_PERSON').map(r => r.name);
            default:
                return this.meeting.receivers.filter(r => r.type === 'RESPONSIBLE_PERSON').map(r => r.name);
        }
    }
  // tslint:disable-next-line:variable-name
  getDate(from_time: string) {
    return new Date(from_time  + ' GMT').toLocaleString();

  }
}
