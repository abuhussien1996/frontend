import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	templateUrl: './timeline.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineComponent implements OnInit {
    /**
     * Component constructor
     */
	constructor() { }

    /*
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
    */
    /**
     * On init
     */
	ngOnInit() { }
}
