import { Component, Input, OnInit } from '@angular/core';
import { AttachmentsDataSource } from '../../../../../../core/lists/_data-sources/attachments.datasource';
// NGRX
import { Store } from '@ngrx/store';
import { AppState } from '../../../../../../core/reducers';
// RXJS
import { distinctUntilChanged, skip } from 'rxjs/operators';
import {AttachmentsPageRequested, ClearAttachment} from '../../../../../../core/lists/_actions/attachment.actions';

@Component({
  selector: 'kt-attachments-list',
  templateUrl: './attachments-list.component.html',
  styleUrls: ['./attachments-list.component.scss']
})
export class AttachmentsListComponent implements OnInit {
  @Input() data: any;
  dataSource: AttachmentsDataSource;
  documents = {
    title: 'Documents',
    type: 0,
    items: []
  };
  private subscriptions: any[] = [];

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    // Init DataSource
    this.dataSource = new AttachmentsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.documents.items = res.map(attachment => ({
        svgPath: attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'pdf' ? 'assets/media/svg/files/pdf.svg'
          : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'png' ? 'assets/media/svg/files/png.svg'
            : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'jpg' ? 'assets/media/svg/files/jpg.svg'
              : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'jpeg' ? 'assets/media/svg/files/jpg.svg'
                : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'csv' ? 'assets/media/svg/files/csv.svg'
                  : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'txt' ? 'assets/media/svg/files/txt.svg'
                    : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'doc' ? 'assets/media/svg/files/doc.svg'
                      : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'docx' ? 'assets/media/svg/files/doc.svg'
                        : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'ppt' ? 'assets/media/svg/files/ppt.svg'
                          : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'html' ? 'assets/media/svg/files/html.svg'
                            : attachment.uuid.substring(attachment.uuid.lastIndexOf('.') + 1).toLowerCase() === 'xls' ? 'assets/media/svg/files/xls.svg'
                              : 'assets/media/svg/files/document.svg',
        title: attachment.uuid.substring(attachment.uuid.lastIndexOf('/') + 1),
        url: attachment.url
      }));
    });
    this.subscriptions.push({ id: 1, sub: entitiesSubscription });
    // First load
    this.loadAttachmentsList();
  }

  /**
 * On Destroy
 */
  ngOnDestroy() {
    this.subscriptions.forEach(el => el.sub.unsubscribe());
    this.store.dispatch(new ClearAttachment())
  }

  /**
 * Load Patients List from service through data-source
 */
  loadAttachmentsList() {
    // Call request from server
    this.store.dispatch(new AttachmentsPageRequested(this.data));
  }

}
