import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TwixorService} from 'src/app/core/timeline/_services';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {SubheaderService} from '../../../../../core/_base/layout';

@Component({
  selector: 'kt-twixor',
  templateUrl: './twixor.component.html'
})
export class TwixorComponent implements OnInit {
  authenticationToken: string;
  display: boolean;
  url: any;

  ngOnInit() {
    this.subheaderService.setBreadcrumbs([{title:'Chat',page:'/timeline/chat'}])
    this.setup();
  }

  setup() {
    const email = 'admin@appiyo.com';
    const password = 'test@123';
    this.twixorService.login(email, password).subscribe((res) => {
      if (res.status) {
        this.authenticationToken = encodeURIComponent(res.response.token);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(`http://18.217.199.255/agent/integration/chat?authToken=${this.authenticationToken}`);
        this.display = true;
      }
      this.cdr.detectChanges();
    });
  }

  constructor(
    public subheaderService: SubheaderService,
    private twixorService: TwixorService,
    private cdr: ChangeDetectorRef,
    private sanitizer: DomSanitizer,
    private translate: TranslateService
  ) {
  }
};
