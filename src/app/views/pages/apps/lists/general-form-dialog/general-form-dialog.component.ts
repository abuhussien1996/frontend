import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {JsonEditorComponent, JsonEditorOptions} from 'ang-jsoneditor';
import {EntityService} from '../../../management/services/entity.service';

@Component({
  selector: 'kt-general-form-dialog',
  templateUrl: './general-form-dialog.component.html',
  styleUrls: ['./general-form-dialog.component.scss']
})
export class GeneralFormDialogComponent implements OnInit {

  private isCreate = true;
  entityForm: FormGroup;
  viewLoading = false;
  public editorOptions: JsonEditorOptions;
  public data: any;
  @ViewChild(JsonEditorComponent, {static: false}) editor: JsonEditorComponent;

  constructor(@Inject(MAT_DIALOG_DATA) public ddata: any,
              private fb: FormBuilder, private entityService: EntityService,
              private dialogRef: MatDialogRef<GeneralFormDialogComponent>) {
    this.editorOptions = new JsonEditorOptions()
    this.editorOptions.mode = 'code'; // set all allowed modes
    this.editorOptions.statusBar = false; // set all allowed modes
    this.entityForm = this.fb.group({
      name: ['', Validators.required],
      schema: [this.data, [Validators.required]]
    });
  }

  ngOnInit(): void {

  }

  getTitle() {
    let title = ''
    title = this.isCreate ? 'Create ' : 'Update ';
    title = title + (this.ddata.project ? 'Project' : 'Entity');
    return title;
  }

  onSubmit() {
    if (!this.entityForm.valid || !this.editor.isValidJson()) {
      this.entityForm.markAsTouched();
      this.entityForm.markAllAsTouched();
      return;
    }
    if (this.isCreate) {
      const body = this.getEntityBody();
      this.entityService.createEntity(body).subscribe(res => {
        this.dialogRef.close({reload: true});
      });
    }
  }

  private getEntityBody() {
    const body = this.entityForm.value;
    body.schema = JSON.stringify(body.schema);
    return body;
  }
}
