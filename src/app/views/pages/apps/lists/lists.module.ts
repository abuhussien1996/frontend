import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {TranslateModule} from '@ngx-translate/core';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {PartialsModule} from '../../../partials/partials.module';
import {ModuleGuard} from '../../../../core/auth';
import {
  AttachmentsService,
  ContactEffects,
  contactsReducer,
  ContactsService,
  TemplateEffects,
  templatesReducer
} from '../../../../core/lists';
import {HttpUtilsService, InterceptService, LayoutUtilsService, TypesUtilsService} from '../../../../core/_base/crud';
import {
  ActionNotificationComponent,
  ConfirmActionDialogComponent,
  DeleteEntityDialogComponent,
  FetchEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../../../partials/content/crud';
import {ListsComponent} from './lists.component';
import {ContactsListComponent} from './contacts/contacts-list/contacts-list.component';
import {ContactEditComponent} from './contacts/contact-edit/contact-edit.component';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {NgbModule, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPermissionsModule} from 'ngx-permissions';
import {MatChipsModule} from '@angular/material/chips';
import {ContactTimelineComponent} from './contacts/_subs/timeline/contact-timeline.component';
import {TimelineService} from 'src/app/core/timeline/_services/timeline.service';
import {InlineSVGModule} from 'ng-inline-svg/lib_commonjs/inline-svg.module';
import {timelinesReducer} from 'src/app/core/timeline/_reducers/timeline.reducers';
import {TimelineEffects} from 'src/app/core/timeline/_effects/timeline.effects';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {TimelineModule} from '../timeline/timeline.module';
import {SharedModule} from 'src/app/shared.module';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime/date-time';
import {MatFormioModule} from 'angular-material-formio';
import {FormService} from 'src/app/core/events';
import {OfficesService} from '../../../../core/management/_services';
import {CreateContactDialogComponent} from '../../../partials/content/general/contact-add-dialog/contact-add.dialog.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CategoryAddDialogComponent} from './categories/category-add/category-add.dialog.component';
import {CategoriesListComponent} from './categories/categories-list/categories-list.component';
import {TemplateEditComponent} from './template/template-edit/template-edit.component';
import {TemplatesListComponent} from './template/template-list/templates-list.component';
import {TemplateService} from '../../../../core/lists/_services/template.service';
import {MergeContactsComponent} from './contacts/_subs/merge-contacts/merge-contacts.component';
import {GeneralValidationMessagesService} from '../../../../core/services/general-validation-messages.service';
import {AuthInterceptor} from '../../auth/auth.interceptor';
import { EntityListComponent } from './entity-list/entity-list.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { GeneralFormDialogComponent } from './general-form-dialog/general-form-dialog.component';
import {FormioModule} from 'angular-formio';
import {AgGridModule} from "ag-grid-angular";
import { AddProjectDialogComponent } from './project-list/add-project-dialog/add-project-dialog.component';
import { SponsorBeneficiaryDialogComponent } from './project-list/add-project-dialog/sponsor-beneficiary-dialog/sponsor-beneficiary-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: ListsComponent,
    children: [
      {
        path: '',
        redirectTo: 'contacts',
        pathMatch: 'full'
      },
      {
        path: 'contacts',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToContactModule'},
        component: ContactsListComponent
      },
      {
        path: 'contacts/add',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canAddContact'},
        component: ContactEditComponent
      },
      {
        path: 'contacts/edit',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToContactModule'},
        component: ContactEditComponent
      },
      {
        path: 'contacts/edit/:id',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToContactModule'},
        component: ContactEditComponent
      },
      {
        path: 'contacts/view/:id',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canViewContact'},
        component: ContactEditComponent
      },
      {
        path: 'category',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToCategoryModule'},
        component: CategoriesListComponent
      },
      {
        path: 'team',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToTeamModule'},
        component: CategoriesListComponent
      },
      {
        path: 'template',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToTemplateModule'},
        component: TemplatesListComponent
      },
      {
        path: 'entity',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToContactModule'},
        component: EntityListComponent
      }
      ,
      {
        path: 'project',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToContactModule'},
        component: ProjectListComponent
      }
    ]
  }
];

@NgModule({
    imports: [
        FormioModule,
        TimelineModule,
        MatButtonToggleModule,
        NgbModule,
        MatChipsModule,
        PerfectScrollbarModule,
        MatDialogModule,
        InlineSVGModule,
        CommonModule,
        HttpClientModule,
        NgxMaterialTimepickerModule,
        PartialsModule,
        NgxPermissionsModule.forChild(),
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        MatButtonModule,
        MatMenuModule,
        MatSelectModule,
        MatInputModule,
        MatTableModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatIconModule,
        MatNativeDateModule,
        MatProgressBarModule,
        MatDatepickerModule,
        MatCardModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatTabsModule,
        MatTooltipModule,
        MatSlideToggleModule,
        MatDividerModule,
        MatListModule,
        NgbProgressbarModule,
        SharedModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatFormioModule,
        DragDropModule,
        StoreModule.forFeature('contacts', contactsReducer),
        EffectsModule.forFeature([ContactEffects]),
        StoreModule.forFeature('timelines', timelinesReducer),
        EffectsModule.forFeature([TimelineEffects]),
        StoreModule.forFeature('templates', templatesReducer),
        EffectsModule.forFeature([TemplateEffects]),
        AgGridModule
    ],
  providers: [
    GeneralValidationMessagesService,
    ModuleGuard,
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'mat-dialog-container-wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    TypesUtilsService,
    LayoutUtilsService,
    HttpUtilsService,
    ContactsService,
    TimelineService,
    TypesUtilsService,
    LayoutUtilsService,
    DatePipe,
    AttachmentsService,
    TemplateService,
    FormService,
    OfficesService
  ],
  entryComponents: [
    MergeContactsComponent,
    ActionNotificationComponent,
    ConfirmActionDialogComponent,
    DeleteEntityDialogComponent,
    FetchEntityDialogComponent,
    UpdateStatusDialogComponent,
    TemplateEditComponent,
    CreateContactDialogComponent,
    CategoryAddDialogComponent,
    GeneralFormDialogComponent,
    AddProjectDialogComponent,
    SponsorBeneficiaryDialogComponent
  ],
  declarations: [
    ListsComponent,
    ContactsListComponent,
    ContactTimelineComponent,
    ContactEditComponent,
    TemplatesListComponent,
    TemplateEditComponent,
    CategoriesListComponent,
    CategoryAddDialogComponent,
    MergeContactsComponent,
    EntityListComponent,
    ProjectListComponent,
    GeneralFormDialogComponent,
    AddProjectDialogComponent,
    SponsorBeneficiaryDialogComponent
  ],
  exports: [
    ContactsListComponent,
    ContactTimelineComponent
  ]
})
export class ListsModule {
}
