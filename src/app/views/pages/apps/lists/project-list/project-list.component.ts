import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BaseDataSource, LayoutUtilsService, MessageType, QueryParamsModel} from '../../../../../core/_base/crud';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {merge, Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ProjectService} from '../../../management/services/project.service';
import {tap} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';
import {GeneralFormDialogComponent} from '../general-form-dialog/general-form-dialog.component';
import {EntityService} from '../../../management/services/entity.service';
import {AddProjectDialogComponent} from './add-project-dialog/add-project-dialog.component';
import {FormioComponent} from 'angular-formio';
import {ConfirmActionDialogComponent} from '../../../../partials/content/crud';

@Component({
  selector: 'kt-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  dataSource = new BaseDataSource();
  displayedColumns = ['id', 'name', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort', {static: true}) sort: MatSort;
  private subscriptions: Subscription[] = [];
  private schema = {
    _id: '5f6f000ed86c2dda318e6f1f',
    type: 'form',
    owner: '5f6a5a0496b5f7f96cfcb7cd',
    components: [{
      label: 'Day',
      hideInputLabels: false,
      inputsLabelPosition: 'top',
      useLocaleSettings: false,
      tableView: false,
      fields: {day: {hide: false}, month: {hide: false}, year: {hide: false}},
      key: 'day',
      type: 'day',
      input: true,
      defaultValue: '00/00/0000'
    }, {
      label: 'Type',
      widget: 'choicesjs',
      tableView: true,
      data: {values: [{label: 'Expedited', value: 'Expedited'}, {label: 'Regular', value: 'Regular'}]},
      selectThreshold: 0.3,
      key: 'type',
      type: 'select',
      indexeddb: {filter: {}},
      input: true,
      defaultValue: 'Expedited'
    }, {
      label: 'Use',
      widget: 'choicesjs',
      tableView: true,
      data: {values: [{label: 'Human', value: 'Human'}, {label: 'Veterinary', value: 'Veterinary'}]},
      selectThreshold: 0.3,
      key: 'use',
      type: 'select',
      indexeddb: {filter: {}},
      input: true,
      defaultValue: 'Veterinary'
    }, {
      label: 'Dosage Form',
      widget: 'choicesjs',
      tableView: true,
      data: {
        values: [{label: 'Capsule', value: 'capsule'}, {
          label: 'TableCapsulet',
          value: 'tableCapsulet'
        }, {label: 'Cream', value: 'Cream'}, {label: 'Solution', value: 'Solution'}, {
          label: 'Otic Drops',
          value: 'oticDrops'
        }, {label: 'Gel', value: 'gel'}, {label: 'Ointment', value: 'ointment'}, {label: 'Other', value: 'other'}]
      },
      selectThreshold: 0.3,
      key: 'dosageForm',
      type: 'select',
      indexeddb: {filter: {}},
      input: true
    }, {
      label: 'Other Comments',
      autoExpand: false,
      tableView: true,
      key: 'otherComments',
      type: 'textarea',
      input: true
    }, {
      label: 'Estimated Monthly Volume',
      tableView: true,
      key: 'estimatedMonthlyVolume',
      type: 'textfield',
      input: true
    }, {
      label: 'Price Target',
      suffix: '$',
      mask: false,
      spellcheck: true,
      tableView: false,
      delimiter: false,
      requireDecimal: false,
      inputFormat: 'plain',
      key: 'priceTarget',
      type: 'number',
      input: true
    }, {
      label: 'Current Supplier',
      widget: 'choicesjs',
      tableView: true,
      data: {
        values: [{
          label: 'National Compounding Pharmacy',
          value: 'nationalCompoundingPharmacy'
        }, {label: 'Out of State Pharmacy', value: 'outOfStatePharmacy'}, {
          label: 'Local Pharmacy',
          value: 'localPharmacy'
        }]
      },
      selectThreshold: 0.3,
      key: 'currentSupplier',
      type: 'select',
      indexeddb: {filter: {}},
      input: true
    }, {label: 'Current Supplier Name', tableView: true, key: 'currentSupplierName', type: 'textfield', input: true}],
    revisions: '',
    _vid: 0,
    title: 'Aplos',
    display: 'form',
    access: [{
      roles: ['5f6c561531bdc90c356b7149', '5f6c561531bdc9621e6b714a', '5f6c561531bdc96ede6b714b'],
      type: 'read_all'
    }],
    submissionAccess: [],
    controller: '',
    properties: {},
    settings: {},
    name: 'aplos',
    path: 'aplos',
    project: '5f6c561531bdc91f286b7148',
    created: '2020-09-26T08:47:10.532Z',
    modified: '2020-09-29T18:33:19.425Z',
    machineName: 'bdflegisogzbcxq:aplos'
  };

  constructor(private dialog: MatDialog,
              private projectService: ProjectService,
              private layoutUtilsService: LayoutUtilsService) {
  }

  ngOnInit(): void {
    this.loadProjects()
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => {
        this.loadProjects();
      })
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);

  }

  addProject() {
    const schema = this.schema;
    this.dialog.open(AddProjectDialogComponent, {data: {schema, project: true,title:'Create Project'},
      width: '800px',
    }).afterClosed().subscribe(res => {
      this.loadProjects();
    })
  }
  updateProject(project){
    const schema = this.schema;
    this.dialog.open(AddProjectDialogComponent, {data: {schema, project: true,title:'Update Project',projectModel:project},
      width: '800px',
    }).afterClosed().subscribe(res => {
      this.loadProjects();
    })
  }
  deleteProject(project){
    const dialogRef = this.dialog.open(ConfirmActionDialogComponent, {
      data: {title: 'Delete Project', description: 'Are you sure to delete this project?', waitDesciption: 'Deleting ...'},
      width: '440px'
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.projectService.deleteProject(project).subscribe(() => {
        const Msg = 'Project deleted successfully';
        this.layoutUtilsService.showActionNotification(Msg, MessageType.Delete, 3000, false, false);
        this.loadProjects()
      });
    });
  }
  private loadProjects() {
    const queryParams = new QueryParamsModel(
      {},
      this.sort.direction !== '' ? this.sort.direction : 'desc',
      !this.sort.active || this.sort.active === '' ? 'created' : this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    this.dataSource.loading$ = of(true);
    this.projectService.getProjectforAdmin(queryParams).subscribe(res => {
      this.dataSource.loading$ = of(false);
      this.dataSource.isPreloadTextViewed$ = of(false);
      this.dataSource.entitySubject.next(res.data);
      this.dataSource.paginatorTotalSubject.next(res.count);
      this.dataSource.hasItems = res.count > 0;
    })

  }


  ngOnDestroy(): void {
    this.subscriptions.forEach(el => el.unsubscribe());
  }
}
