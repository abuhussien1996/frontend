import {ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {SponsorBeneficiaryDialogComponent} from './sponsor-beneficiary-dialog/sponsor-beneficiary-dialog.component';
import {ProjectService} from '../../../../management/services/project.service';
import {FormioComponent} from 'angular-formio';
import {LayoutUtilsService, MessageType} from '../../../../../../core/_base/crud';

@Component({
  selector: 'kt-add-project-dialog',
  templateUrl: './add-project-dialog.component.html',
  styleUrls: ['./add-project-dialog.component.scss'],
})
export class AddProjectDialogComponent implements OnInit {
  selectedStatusForUpdate = new FormControl('');
  viewLoading = false;
  @ViewChild('projectFormio', {static: false}) projectFormio: FormioComponent;
  loadingAfterSubmit = false;
  selectedTab = 0;
  hasFormErrors: boolean;
  infoSchema = {
    components: [
      {
        label: 'Name',
        labelPosition: 'left-left',
        tableView: true,
        key: 'name',
        validate: {'required': true},
        type: 'textfield',
        input: true
      },
      {
        label: 'Type',
        labelPosition: 'left-left',
        widget: 'choicesjs',
        tableView: true,
        data: {
          values: [
            {
              label: '',
              value: ''
            }
          ]
        },
        selectThreshold: 0.3,
        key: 'type',
        type: 'select',
        indexeddb: {
          filter: {}
        },
        input: true
      },
      {
        label: 'Description',
        labelPosition: 'left-left',
        tableView: true,
        key: 'description',
        type: 'textfield',
        input: true
      },
      {
        label: 'Columns',
        columns: [
          {
            components: [
              {
                label: 'Start Date',
                labelPosition: 'left-left',
                tableView: false,
                enableMinDateInput: false,
                datePicker: {
                  disableWeekends: false,
                  disableWeekdays: false
                },
                enableMaxDateInput: false,
                key: 'startDate',
                type: 'datetime',
                input: true,
                widget: {
                  type: 'calendar',
                  displayInTimezone: 'viewer',
                  locale: 'en',
                  useLocaleSettings: false,
                  allowInput: true,
                  mode: 'single',
                  enableTime: true,
                  noCalendar: false,
                  format: 'yyyy-MM-dd hh:mm a',
                  hourIncrement: 1,
                  minuteIncrement: 1,
                  time_24hr: false,
                  disableWeekends: false,
                  disableWeekdays: false
                },
                hideOnChildrenHidden: false
              },
              {
                label: 'Estimated Cost',
                labelPosition: 'left-left',
                tableView: true,
                key: 'estimatedCost',
                type: 'textfield',
                input: true,
                hideOnChildrenHidden: false
              }
            ],
            width: 6,
            offset: 0,
            push: 0,
            pull: 0,
            size: 'md'
          },
          {
            components: [
              {
                label: 'End Date',
                labelPosition: 'left-left',
                tableView: false,
                enableMinDateInput: false,
                datePicker: {
                  disableWeekends: false,
                  disableWeekdays: false
                },
                enableMaxDateInput: false,
                key: 'endDate',
                type: 'datetime',
                input: true,
                widget: {
                  type: 'calendar',
                  displayInTimezone: 'viewer',
                  locale: 'en',
                  useLocaleSettings: false,
                  allowInput: true,
                  mode: 'single',
                  enableTime: true,
                  noCalendar: false,
                  format: 'yyyy-MM-dd hh:mm a',
                  hourIncrement: 1,
                  minuteIncrement: 1,
                  time_24hr: false,
                  disableWeekends: false,
                  disableWeekdays: false
                },
                hideOnChildrenHidden: false
              },
              {
                label: 'Current Cost',
                labelPosition: 'left-left',
                tableView: true,
                key: 'currentCost',
                type: 'textfield',
                input: true,
                hideOnChildrenHidden: false
              }
            ],
            width: 6,
            offset: 0,
            push: 0,
            pull: 0,
            size: 'md'
          }
        ],
        key: 'columns',
        type: 'columns',
        input: false,
        tableView: false
      },
    ]
  }
  infoSchemaInputData
  refresh = true
  projectModel
  benfSchema = {
    components: [
      {
        label: 'Sponsor',
        disableAddingRemovingRows: true,
        reorder: false,
        addAnotherPosition: 'bottom',
        defaultOpen: true,
        layoutFixed: false,
        enableRowGroups: false,
        initEmpty: true,
        hideLabel: true,
        tableView: false,
        defaultValue: [
          {
            name: '',
            relative_address: '',
            relative_contact: '',
            percent: false
          }
        ],
        clearOnHide: false,
        key: 'sponsor',
        type: 'datagrid',
        input: true,
        components: [
          {
            label: 'Name',
            disabled: true,
            tableView: true,
            key: 'name',
            type: 'textfield',
            input: true
          },
          {
            label: 'Relative Address',
            disabled: true,
            tableView: true,
            key: 'relative_address',
            type: 'textfield',
            input: true
          },
          {
            label: 'Relative Contact',
            disabled: true,
            tableView: true,
            key: 'relative_contact',
            type: 'textfield',
            input: true
          },
          {
            label: 'Percent %',
            disabled: true,
            tableView: true,
            key: 'percent',
            type: 'textfield',
            input: true
          }
        ]
      }
    ]
  }
  sponSchema = {
    components: [
      {
        label: 'Sponsor',
        disableAddingRemovingRows: true,
        reorder: false,
        addAnotherPosition: 'bottom',
        defaultOpen: true,
        layoutFixed: false,
        enableRowGroups: false,
        initEmpty: true,
        hideLabel: true,
        tableView: false,
        defaultValue: [
          {
            name: '',
            relative_address: '',
            relative_contact: '',
            percent: ''
          }
        ],
        clearOnHide: false,
        key: 'sponsor',
        type: 'datagrid',
        input: true,
        components: [
          {
            label: 'Name',
            disabled: true,
            tableView: true,
            key: 'name',
            type: 'textfield',
            input: true
          },
          {
            label: 'Relative Address',
            disabled: true,
            tableView: true,
            key: 'relative_address',
            type: 'textfield',
            input: true
          },
          {
            label: 'Relative Contact',
            disabled: true,
            tableView: true,
            key: 'relative_contact',
            type: 'textfield',
            input: true
          },
          {
            label: 'Percent %',
            disabled: true,
            tableView: true,
            key: 'percent',
            type: 'textfield',
            input: true
          }
        ]
      }
    ]
  }
  sponsorSchemaInputData = {
    data: {
      sponsor: []
    }
  }
  benfSchemaInputData = {
    data: {
      sponsor: []
    }
  }
  entities = []

  constructor(
    private projectService: ProjectService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private layoutUtilsService: LayoutUtilsService,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    if (this.data.projectModel) {
      this.projectModel = {data: JSON.parse(this.data.projectModel.data)}
      if (this.data.projectModel.entities && this.data.projectModel.entities.length > 0) {
        this.entities = this.data.projectModel.entities
        this.entities.forEach(element => {
          const relativeContacts = []
          element.contacts.forEach(ele => {
            relativeContacts.push(ele.name)
          })
          const data = JSON.parse(element.data)
          const relativeAddress = []
          data.locationList.forEach(ele =>{
            relativeAddress.push((ele.city + ' ' + ele.state + ' ' +ele.street + ' ' + ele.zipCode))
          })
          const newD = {
            name: element.name,
            relative_address: relativeAddress,
            relative_contact: relativeContacts,
            percent: data.percent
          }
          if (element.type.toLowerCase() === 'sponsor') {
            this.sponsorSchemaInputData.data.sponsor.push(newD);
          } else {
            this.benfSchemaInputData.data.sponsor.push(newD);
          }
          this.cdr.detectChanges()
        })
      }
      this.cdr.detectChanges()
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.projectFormio && !this.projectFormio.formio.checkValidity()) {
      const errMsg = 'Invalid Form Submission. Please check before submitting again';
      this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
      return
    }
    const body = {id: undefined, name: '', data: '', entities: []}
    body.name = this.infoSchemaInputData.name
    body.data = JSON.stringify(this.infoSchemaInputData)
    body.entities = this.entities
    if (this.data.projectModel) {
      body.id = this.data.projectModel.id
      this.projectService.updateProject(body).subscribe(res => {
        this.onNoClick()
      })
      return
    }
    this.projectService.createProject(body).subscribe(res => {
      this.onNoClick()
    })
  }

  onNewClick(type: string) {
    this.refresh = false
    const x =
      {
        'components': [{
          'label': 'Name',
          'tableView': true,
          'validate': {'required': true},
          'key': 'name',
          'type': 'textfield',
          'input': true
        }, {
          'label': 'Description',
          'autoExpand': false,
          'tableView': true,
          'validate': {'required': true},
          'key': 'description',
          'type': 'textarea',
          'input': true
        }, {
          'label': 'Percent',
          'mask': false,
          'spellcheck': true,
          'tableView': false,
          'delimiter': false,
          'requireDecimal': false,
          'inputFormat': 'plain',
          'validate': {'min': 0, 'max': 100},
          'key': 'percent',
          'type': 'number',
          'input': true
        }, {
          'label': 'contacts',
          'reorder': false,
          'addAnotherPosition': 'bottom',
          'defaultOpen': false,
          'layoutFixed': false,
          'enableRowGroups': false,
          'initEmpty': false,
          'tableView': false,
          'defaultValue': [{}],
          'key': 'contacts',
          'type': 'datagrid',
          'input': true,
          'components': [{
            'label': 'Name',
            'tableView': true,
            'validate': {'required': true},
            'key': 'name',
            'type': 'textfield',
            'input': true
          }, {
            'label': 'Email',
            'tableView': true,
            'validate': {'required': true},
            'key': 'email',
            'type': 'email',
            'input': true
          }, {
            'label': 'Mobile',
            'tableView': true,
            'validate': {'required': true},
            'key': 'mobile',
            'type': 'phoneNumber',
            'input': true
          }, {
            'label': 'Telephone',
            'tableView': true,
            'validate': {'required': true},
            'key': 'telephone',
            'type': 'phoneNumber',
            'input': true
          }]
        }, {
          'label': 'location list',
          'reorder': false,
          'addAnotherPosition': 'bottom',
          'defaultOpen': false,
          'layoutFixed': false,
          'enableRowGroups': false,
          'initEmpty': false,
          'tableView': false,
          'defaultValue': [{}],
          'key': 'locationList',
          'type': 'datagrid',
          'input': true,
          'components': [{'label': 'State', 'tableView': true, 'key': 'state', 'type': 'textfield', 'input': true}, {
            'label': 'City',
            'tableView': true,
            'key': 'city',
            'type': 'textfield',
            'input': true
          }, {'label': 'Street', 'tableView': true, 'key': 'street', 'type': 'textfield', 'input': true}, {
            'label': 'Zip code',
            'mask': false,
            'spellcheck': true,
            'tableView': false,
            'delimiter': false,
            'requireDecimal': false,
            'inputFormat': 'plain',
            'validate': {'min': 0, 'max': 99999},
            'key': 'zipCode',
            'type': 'number',
            'input': true
          }]
        }]
      }
    this.dialog.open(SponsorBeneficiaryDialogComponent, {
      data: {schema: x, title: 'New ' + type, type},
      width: '800px',
    }).afterClosed().subscribe(res => {
      if (res) {
        const relativeContacts = []
        res.inputDataForSave.contacts.forEach(ele => {
          relativeContacts.push(ele.name)
        })
        const relativeAddress = []
        res.inputDataForSave.locationList.forEach(ele => {
          relativeAddress.push((ele.city + ' ' + ele.state + ' ' + ele.street + ' ' + ele.zipCode))
        })
        const newD = {
          name: res.inputDataForSave.name,
          relative_address: relativeAddress,
          relative_contact: relativeContacts,
          percent: res.inputDataForSave.percent
        }
        if (type === 'sponsor') {
          this.sponsorSchemaInputData.data.sponsor.push(newD);
        } else {
          this.benfSchemaInputData.data.sponsor.push(newD);
        }
        this.entities.push(res.body)
        this.cdr.detectChanges()
      }
      this.refresh = true
      this.cdr.detectChanges()
    })

  }

  onChangeInfoData(event) {
    if (event.data)
      this.infoSchemaInputData = event.data
  }
}
