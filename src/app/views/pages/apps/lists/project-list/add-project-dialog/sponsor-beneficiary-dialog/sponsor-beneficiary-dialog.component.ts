import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'kt-sponsor-beneficiary-dialog',
  templateUrl: './sponsor-beneficiary-dialog.component.html',
  styleUrls: ['./sponsor-beneficiary-dialog.component.scss']
})
export class SponsorBeneficiaryDialogComponent implements OnInit {
  private inputDataForSave: any;
  hasFormErrors: boolean;

  constructor(public dialogRef: MatDialogRef<any>,
               @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    const body = {name:'',type:'',data:'',contacts:[]}
    body.name = this.inputDataForSave.name
    body.type = this.data.type.toUpperCase()
    body.data = JSON.stringify(this.inputDataForSave)
    body.contacts = this.inputDataForSave.contacts
    this.dialogRef.close({inputDataForSave:this.inputDataForSave,body});
  }

  onChangeLeftSide(event) {
    if (event.data)
      this.inputDataForSave = event.data
  }
}
