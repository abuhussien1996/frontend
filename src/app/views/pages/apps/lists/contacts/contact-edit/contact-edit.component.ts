// Angular
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
// Material
import {MatDialog} from '@angular/material/dialog';
// RxJS
import {BehaviorSubject, Observable, of} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {AppState} from '../../../../../../core/reducers';
// Layout
// CRUD
import {LayoutUtilsService, MessageType} from '../../../../../../core/_base/crud';
// Services and Models
import {
  ContactModel,
  ContactOnServerCreated,
  ContactOnServerUpdated,
  ContactsService,
  selectContactById,
  selectContactErrorState,
  selectContactsActionLoading,
  SharedHttpRequestService
} from '../../../../../../core/lists';
import {currentUser, currentUserPermissions} from 'src/app/core/auth';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {MergeContactsComponent} from '../_subs/merge-contacts/merge-contacts.component';
import {GeneralValidationMessagesService} from '../../../../../../core/services/general-validation-messages.service';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-contact-edit',
  templateUrl: './contact-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactEditComponent implements OnInit, OnDestroy {
  // Public properties
  contact: ContactModel;
  contactId$: Observable<number>;
  companyId: number;
  oldContact: ContactModel;
  selectedTab = 0;
  maxDate: any;
  loadingSubject = new BehaviorSubject<boolean>(true);
  loading$: Observable<boolean>;
  contactForm: FormGroup;
  hasFormErrors = false;
  isViewMode: boolean;
  isViewMode$: Observable<boolean>;
  private headerMargin: number;
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  private subscriptions: any[] = [];
  disabledTypeEntity = false

  constructor(
    private datePipe: DatePipe,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private contactFB: FormBuilder,
    private layoutUtilsService: LayoutUtilsService,
    private contactService: ContactsService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private sharedHttpRequestService: SharedHttpRequestService,
    public dialog: MatDialog,
    private globalService: GeneralValidationMessagesService
  ) {
    this.maxDate = new Date();
  }


  ngOnInit() {
    const userSub = this.store.pipe(select(currentUser)).subscribe((res) => {
      if (!res) {
        this.router.navigateByUrl('/lists/contacts');
      }
      this.companyId = res.companyID;
    });
    this.subscriptions.push({id: 1, sub: userSub});
    this.loading$ = this.loadingSubject.asObservable();
    this.loadingSubject.next(true);
    const routeSub = this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id && id > 0) {

        const contactSub = this.store.pipe(
          select(selectContactById(id))
        ).subscribe(result => {
          if (result) {
            this.loadContact(result);
            return;
          }
          const contactById = this.globalService.getEntitiesById('contacts', id).subscribe(res => {
            if (res) {
              this.loadContact(res.data);
              this.cdr.detectChanges()
              return
            }
          })
          this.subscriptions.push({sub: contactById});
        });
        this.subscriptions.push({id: 2, sub: contactSub});
      } else {
        const newContact = new ContactModel();
        newContact.clear();
        this.loadContact(newContact);
      }

      this.cdr.detectChanges();

    });
    this.subscriptions.push({id: 3, sub: routeSub});

    // sticky portlet header
    window.onload = () => {
      const style = getComputedStyle(document.getElementById('kt_header'));
      this.headerMargin = parseInt(style.height, 0);
    };
  }

  loadContact(_contact, fromService: boolean = false) {
    if (!_contact) {
      this.goBack();
      return;
    }
    this.contact = _contact;
    this.contactId$ = of(_contact.id);
    this.oldContact = Object.assign({}, _contact);
    this.initContact();
    if (fromService) {
      this.cdr.detectChanges();
    }
  }

  // If contact didn't find in store
  loadContactFromService(contactId) {
    this.contactService.getContactById(contactId).subscribe(res => {
      this.loadContact(res, true);
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this.cdr.detach();
    this.subscriptions.forEach(el => {
      if (el.sub) {
        el.sub.unsubscribe();
      }
    });
  }

  /**
   * Init contact
   */
  initContact() {
    this.createForm();
    this.loadingSubject.next(false);
    if (!this.contact.id) {
      this.isViewMode = false;
    } else if (this.activatedRoute.url[`_value`][1].path === 'view') {
      this.isViewMode = true;
      this.contactForm.disable();
    } else {
      this.subscriptions.push(this.store.pipe(select(currentUserPermissions)).subscribe((res) => {
        if (!(res.some(p => p.name === 'canEditContact'))) {
          this.contactForm.disable();
        }
      }));
      this.isViewMode = false;
    }
    this.isViewMode$ = of(this.isViewMode);
  }

  /**
   * Create form
   */
  createForm() {
    this.contactForm = this.contactFB.group({
      urbanization: [this.contact.urbanization],
      firstname: [this.contact.first_name, Validators.required],
      lastname: [this.contact.last_name, Validators.required],
      lastname2: [this.contact.last_name2],
      entity_type: [this.contact.entity_type],
      entity_id: [this.contact.entity_id],
      email: [this.contact.email, [Validators.email]],
      cell_phone: [
        this.contact.cell_phone !== '' && this.contact.cell_phone !== null ?
          '(' + this.contact.cell_phone.substring(0, 3) + ') '
          + this.contact.cell_phone.substring(3, 6)
          + '-' + this.contact.cell_phone.substring(6, 10) : '',
        [Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)]],
      home_phone: [
        this.contact.home_phone !== '' && this.contact.home_phone !== null ?
          '(' + this.contact.home_phone.substring(0, 3) + ') '
          + this.contact.home_phone.substring(3, 6)
          + '-' + this.contact.home_phone.substring(6, 10) : '',
        Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)],
      fax: [
        this.contact.fax !== '' && this.contact.fax !== null ?
          '(' + this.contact.fax.substring(0, 3) + ') '
          + this.contact.fax.substring(3, 6)
          + '-' + this.contact.fax.substring(6, 10) : '',
        [Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)]],
      state: [this.contact.state, Validators.pattern(/^[A-Za-z]{2}$/)],
      pin_number: [this.contact.pin_number,
        Validators.pattern('^[0-9]*$')],
      city: [this.contact.city],
      zip: [this.contact.zip, [Validators.pattern(/(^\d{5}$)|(^\d{5}-\d{4}$)/)]],
      address1: [this.contact.address_1],
      address2: [this.contact.address_2],
      isActive: [this.contact.is_active],
      note: [this.contact.note],
      dob: [this.contact.dob !== null && this.contact.dob !== '' ? new Date(`${this.contact.dob}T00:00:00`) : ''],
    });
    if (this.contact.entity_id) {
      this.contactForm.controls.entity_type.disable();
    }
  }

  goBack() {
    this.loadingSubject.next(false);
    let url = `/lists/contacts`;

    if (this.activatedRoute.snapshot.queryParams.entityType) {
      switch(this.activatedRoute.snapshot.queryParams.entityType){
        case 'FACILITY':
          this.router.navigate(['lists/facilities/edit/'+this.activatedRoute.snapshot.queryParams.id]);
          return;
        case 'DOCTOR':
          this.router.navigate(['lists/doctors/edit/'+ this.activatedRoute.snapshot.queryParams.id]);
          return;
        case 'PATIENT':
          this.router.navigate(['lists/patients/edit/'+ this.activatedRoute.snapshot.queryParams.id]);
          return;
      }
    }
    this.router.navigateByUrl(url);
  }

  /**
   * Reset
   */
  reset() {
    this.contact = Object.assign({}, this.oldContact);
    this.createForm();
    this.contactForm.controls[`entity_type`].enable();
    this.hasFormErrors = false;
    this.contactForm.markAsPristine();
    this.contactForm.markAsUntouched();
    this.contactForm.updateValueAndValidity();
  }

  /**
   * Save data
   *
   * @param withBack: boolean
   */
  onSubmit(withBack: boolean = false) {
    this.hasFormErrors = false;
    const controls = this.contactForm.controls;
    /** check form */
    if (this.contactForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );

      this.hasFormErrors = true;
      this.selectedTab = 0;
      return;
    }

    // tslint:disable-next-line:prefer-const
    let editedContact = this.prepareContact();

    if (editedContact.id > 0) {
      this.updateContact(editedContact, withBack);
      return;
    }

    this.addContact(editedContact, withBack);
  }

  /**
   * Returns object for saving
   */
  prepareContact(): ContactModel {
    const controls = this.contactForm.controls;
    const _contact = new ContactModel();
    _contact.id = this.contact.id;
    _contact.company_id = this.companyId;
    _contact.contact_id = this.contact.contact_id;
    _contact.first_name = controls.firstname.value;
    _contact.last_name = controls.lastname.value;
    _contact.last_name2 = controls.lastname2.value;
    _contact.entity_type = controls.entity_type.value;
    _contact.entity_id = controls.entity_id.value;
    _contact.email = controls.email.value;
    _contact.cell_phone = controls.cell_phone.value === '' ? null : controls.cell_phone.value.replace(/\D/g, '')
    _contact.home_phone = controls.home_phone.value !== null ? controls.home_phone.value.replace(/\D/g, '') : controls.home_phone.value;
    _contact.fax = controls.fax.value !== null ? controls.fax.value.replace(/\D/g, '') : controls.fax.value;
    _contact.state = controls.state.value ? controls.state.value.toUpperCase() : '';
    _contact.pin_number = controls.pin_number.value;
    _contact.city = controls.city.value;
    _contact.zip = controls.zip.value;
    _contact.address_1 = controls.address1.value;
    _contact.address_2 = controls.address2.value;
    _contact.note = controls.note.value;
    _contact.is_active = controls.isActive.value;
    _contact.urbanization = controls.urbanization.value;
    _contact.dob = this.datePipe.transform(controls.dob.value, 'yyyy-MM-dd');

    return _contact;
  }

  /**
   * Add contact
   *
   * @param _contact: ContactModel
   * @param withBack: boolean
   */
  addContact(_contact: ContactModel, withBack: boolean = false) {
    this.loadingSubject.next(true);
    this.store.dispatch(new ContactOnServerCreated({contact: _contact}));
    if (this.subscriptions.find(sub => sub.id === 4) === undefined) {
      const actionLoadingSubscription = this.store.pipe(
        select(selectContactsActionLoading)
      ).subscribe(res => {
        if (res !== true) {
          this.loadingSubject.next(false);
          if (this.subscriptions.find(sub => sub.id === 5) === undefined) {
            const errorStateSubscription = this.store.pipe(
              select(selectContactErrorState)
            ).subscribe(error => {
              if (error) {
                if (error && error.errors && error.errors[0]) {
                  this.submissionErrorObj = [{error: error.errors[0].details}];
                } else {
                  this.submissionErrorObj = [{error: 'GLOBAL.UNSPECIFIED_ERROR'}]
                }
                this.hasSubmissionErrors = true;
              } else if (this.router.routerState.snapshot.url !== '/lists/contacts') {
                this.submissionErrorObj = null;
                this.hasSubmissionErrors = false;
                const message = this.translate.instant('CONTACT.EDIT.ADD_MESSAGE');
                this.layoutUtilsService.showActionNotification(message, MessageType.Create, 3000, false, false);
                this.goBack();
              }
            });
            this.subscriptions.push({id: 5, sub: errorStateSubscription});
          }
        }
        this.cdr.detectChanges();
      });
      this.subscriptions.push({id: 4, sub: actionLoadingSubscription});
    }
  }

  /**
   * Update contact
   *
   * @param _contact: ContactModel
   * @param withBack: boolean
   */
  updateContact(_contact: ContactModel, withBack: boolean = false) {
    this.loadingSubject.next(true);

    const updateContact: Update<ContactModel> = {
      id: _contact.id,
      changes: _contact
    };

    this.store.dispatch(new ContactOnServerUpdated({
      partialContact: updateContact,
      contact: _contact
    }));
    if (this.subscriptions.find(sub => sub.id === 6) === undefined) {
      const actionLoadingSubscription = this.store.pipe(
        select(selectContactsActionLoading)
      ).subscribe(res => {
        if (res !== true) {
          this.loadingSubject.next(false);
          if (this.subscriptions.find(sub => sub.id === 7) === undefined) {
            const errorStateSubscription = this.store.pipe(
              select(selectContactErrorState)
            ).subscribe(error => {
              if (error) {
                if (error.error && error.error.errors && error.error.errors[0]) {
                  this.submissionErrorObj = [{error: error.error.errors[0].details}];
                } else {
                  this.submissionErrorObj = [{error: 'GLOBAL.UNSPECIFIED_ERROR'}]
                }
                this.hasSubmissionErrors = true;
              } else if (this.router.routerState.snapshot.url !== '/lists/contacts') {
                this.goBack();
                const message = this.translate.instant('CONTACT.EDIT.UPDATE_MESSAGE');
                this.layoutUtilsService.showActionNotification(message, MessageType.Update, 3000, false, false);
              }
            });
            this.subscriptions.push({id: 7, sub: errorStateSubscription});
          }
        }
        this.cdr.detectChanges();
      });
      this.subscriptions.push({id: 6, sub: actionLoadingSubscription});
    }
  }

  /**
   * Checking control validation
   *
   * @param controlName: string => Equals to formControlName
   * @param validationType: string => Equals to valitors name
   */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.contactForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  /**
   * Returns component title
   */

  getComponentTitle() {
    let result = this.translate.instant('CONTACT.NEW_CONTACT');
    if (!this.contact || !this.contact.id) {
      return result;
    }
    result = `<table>
      <tr>
      <td>
      <h3>` + `${this.contact.first_name}` + ' ' + `${this.contact.last_name}` + ` </h3>
       <h6>
          <span>
              <i class="fa flaticon2-phone" aria-hidden="true"></i>
          </span>&nbsp;
          <span class="text-muted font-weight-bold">Phone: <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">` + this.contact.cell_phone + `</a></span>
      </h6>
      <h6>
          <span>
              <i class="fa flaticon-squares" aria-hidden="true"></i>
          </span>&nbsp;
          <span class="text-muted font-weight-bold">PIN: ` + this.contact.pin_number + `</span>
      </h6>
      <h6>
        <span>
          <i class="fa flaticon2-new-email" aria-hidden="true"></i>
          </span>&nbsp;
          <span class="text-muted font-weight-bold">Email: ` + this.contact.email + `</span>
      </h6>
    </td>
    </tr>
    </table>`;
    return result;
  }

  /**
   * Close alert
   *
   * @param $event: any
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }

  /**
   * Rerurns status string
   */
  getStatusString(): string {
    switch (this.contactForm.get('isActive').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }

  editContact(id) {
    this.router.navigate(['lists/contacts/edit/' + id], {queryParams: {viewContext: 'CONTACT'}});
  }

  setMaxDate($event: MatDatepickerInputEvent<any>) {
    this.maxDate = $event.value;
  }

  mergeContact() {
    const dialogRef = this.dialog.open(MergeContactsComponent, {
      data: {id: this.contact.id},
      width: '440px',
      height: 'auto'
    });
    const dialogRefSubscription = dialogRef.afterClosed().subscribe(res => {
        if (res && res.contacts) {
          const contacts = res.contacts.map(a => a.id);
          this.contactService.putContactMerge(contacts, this.contact.id).subscribe(ret => {
            let message = 'We are working in the background';
            if (ret.status !== 200)
              message = ret.statusText
            this.layoutUtilsService.showActionNotification(message, MessageType.Read, 6000, true, false);
          })
        }
      }
    );
  }
}
