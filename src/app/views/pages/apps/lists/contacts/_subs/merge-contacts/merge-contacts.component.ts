import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GeneralAutocompleteComponent} from '../../../../../../partials/layout/general-autocomplete/general-autocomplete.component';
import {ContactsService} from '../../../../../../../core/lists/_services';
import {of} from 'rxjs';
import {LayoutUtilsService} from '../../../../../../../core/_base/crud';

@Component({
  selector: 'kt-merge-contacts',
  templateUrl: './merge-contacts.component.html',
  styleUrls: ['./merge-contacts.component.scss']
})
export class MergeContactsComponent implements OnInit, AfterViewInit {
  mergeForm: FormGroup;
  contactAutocompleteCall = this.contactsService.searchContacts.bind(this.contactsService);
  @ViewChild('contactAutocomplete') contactAutocomplete: GeneralAutocompleteComponent;
  filterRecipientFn = this.autocompleteFormatter.bind(this);
  formatFn = ((value) => {
    return value;
  }).bind(this);

  constructor(private contactsService: ContactsService,
              private fb: FormBuilder,
              public dialogRef: MatDialogRef<MergeContactsComponent>,
              private layoutUtilsService: LayoutUtilsService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.mergeForm = this.fb.group({
      contacts: [null, Validators.required],
    });
  }

  ngAfterViewInit() {
    this.contactAutocomplete.isLoading.subscribe(res => {
      if(!res) {
        this.contactAutocomplete.filteredItems.subscribe(result => {
          this.contactAutocomplete.filteredItems = of(result.filter(item => item.id !== this.data.id));
        })
      }
    });
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onMergeClick(): void {
    const _title = 'Confirm Merge';
    const _description = 'You are about to merge the selected contacts into the current record.\nThis process cannot be undone.\nWould you like to proceed?';
    const _waitDesciption = 'Merging...';

    const dialogR = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
    dialogR.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      const controls = this.mergeForm.controls;
      this.contactAutocomplete.updateValueAndValidity();
      if (this.mergeForm.invalid) {
        Object.keys(controls).forEach(controlName =>
          controls[controlName].markAsTouched()
        );
        return;
      }
      this.dialogRef.close({
        contacts: this.mergeForm.get('contacts').value
      });
    });
  }

  onContactChange(val) {
    this.mergeForm.controls.contacts.setValue(val)
  }

  autocompleteFormatter(participant) {
    if (!participant)
      return '';
    let name = '';
    let subTitle = '';
    if (participant.first_name) {
      name += participant.first_name;
    }
    if (participant.last_name) {
      name += ' ' + participant.last_name;
    }
    if (participant.email) {
      subTitle = ' ' + participant.email
    }
    if (participant.fax) {
      subTitle += ' ' + participant.fax
    }
    return `<div>${name} <small class="text-muted">${subTitle}</small></div>`
  }
}
