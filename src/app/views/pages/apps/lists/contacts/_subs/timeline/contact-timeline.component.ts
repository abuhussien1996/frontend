// Angular
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Material
import {MatDialog} from '@angular/material/dialog';
// NGRX
import {Store} from '@ngrx/store';
// CRUD
// Services and Models
import {TranslateService} from '@ngx-translate/core';
import {AppState} from 'src/app/core/reducers';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud';
import {ContactModel} from 'src/app/core/lists';
import {TimelineService} from 'src/app/core/timeline/_services/timeline.service'
import {FormControl, FormGroup} from '@angular/forms';
import {MeetingViewDialogComponent} from '../../../../timeline/meetings/meeting-view/meeting-view-dialog.component';
import {ViewCommunicationComponent} from '../../../../timeline/view-communication/view-communication.component';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'kt-contact-timeline',
  templateUrl: './contact-timeline.component.html',
  styleUrls: ['./contact-timeline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactTimelineComponent implements OnInit {
  // Subscriptions
  timelines: any[] = []
  contactId: number;
  private subscriptions: any[] = [];
  contact: ContactModel;
  sentFilter = '';
  search = new FormGroup({
    searchInput: new FormControl(null),
  });
  spinnerStateSent = true;
  private fromDate: string;
  private toDate: string;

  constructor(public dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private layoutUtilsService: LayoutUtilsService,
              private store: Store<AppState>,
              private cdr: ChangeDetectorRef,
              private translate: TranslateService,
              private timeline: TimelineService,
              public datepipe: DatePipe) {
  }

  ngOnInit() {
    this.refreshDate()
    this.getContactId()
  }
  getContactId(){
    const activeSubscription = this.activatedRoute.params.subscribe(params => {
      this.contactId = params.id
      if (this.contactId) {
        this.loadTimelines()
      }
    });
    this.subscriptions.push(activeSubscription);
  }
  loadTimelines(type = '') {
    this.spinnerStateSent = true
    this.sentFilter = type
    const term = this.search.get('searchInput').value
    this.timeline.findSentTimelines(term, type, this.contactId, this.fromDate, this.toDate).subscribe(res => {
      this.timelines = this.timelines.concat(res.data);
      this.spinnerStateSent = false
      this.cdr.detectChanges()
    });
  }

  resetSent() {
    this.refreshDate()
    this.timelines = [];
  }

  searchAction() {
    this.resetSent()
    this.loadTimelines(this.sentFilter)
  }

  getItem(type: string): string {
    switch (type) {
      case 'FAX':
        return './assets/media/svg/icons/Communication/Incoming-box.svg';
      case 'MEETING':
        return './assets/media/svg/icons/Code/Time-schedule.svg';
      case 'EMAIL':
        return './assets/media/svg/icons/Communication/Incoming-mail.svg';
      case 'NOTE':
        return './assets/media/svg/icons/General/Notifications1.svg';
      case 'FILTERED_VIEW_SESSION':
        return './assets/media/svg/icons/General/filter.svg';
    }
  }

  getItemClass(item): string {
    if (!item.received_id) {
      return 'timeline-item timeline-item-left ';
    }
    return 'timeline-item timeline-item-right '
  }

  readMore(item, type) {
    item.contactId = this.contactId;
    // tslint:disable-next-line:prefer-const
    let id;
    if (item.communication_id){
      id = item.communication_id
      item.id = item.communication_id;
    }else {
      id = item.received_id
      item.id = item.received_id;
    }
    switch (type) {
      case 'FAX':
        const faxDialogRef = this.dialog.open(ViewCommunicationComponent, {data: item});
        faxDialogRef.afterClosed().subscribe(res => {
          if (!res) {
            return;
          }
        });
        return;
      case 'MEETING':
        const _saveMessage = this.translate.instant('DIALOG.SAVE');
        const _messageType = id ? MessageType.Update : MessageType.Create;
        const dialogRef = this.dialog.open(MeetingViewDialogComponent, {data: {meeting: item}});
        dialogRef.afterClosed().subscribe(res => {
          if (!res) {
            return;
          }
          this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 5000, false, false);
        });
        return;
      case 'EMAIL':
        const emailDialogRef = this.dialog.open(ViewCommunicationComponent, {data: item});
        emailDialogRef.afterClosed().subscribe(res => {
          if (!res) {
            return;
          }
        });
        return;
    }
  }

  onScrollSent(event) {
    // if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight && this.sentPage < this.sentTotalPage - 1) {
    //   ++this.sentPage
    //   this.loadTimelines(this.sentFilter)
    // }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.subscriptions.forEach(el => {
      if (el.sub) {
        el.sub.unsubscribe();
      }
    });
  }

  getItemColor(type: any) {
    const main = 'svg-icon svg-icon-';
    switch (type) {
      case 'FAX':
        return main + 'success';
      case 'MEETING':
        return main + 'danger';
      case 'EMAIL':
        return main + 'primary';
      case 'NOTE':
        return main + 'warning';
      case  'FILTERED_VIEW_SESSION':
        return main + 'secondary';
    }
  }

  refreshDate() {
    const date = new Date();
    // tslint:disable-next-line:variable-name
    this.fromDate = this.datepipe.transform(date, 'yyyy-MM-dd');
    date.setDate(date.getDate() - 360)
    this.toDate = this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  getDate(created: any) {
    return new Date(created  + ' GMT').toLocaleString();
  }
}
