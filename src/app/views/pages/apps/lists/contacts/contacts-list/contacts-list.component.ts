// Angular
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Material
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {SelectionModel} from '@angular/cdk/collections';
// RXJS
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
// CRUD
import {LayoutUtilsService, MessageType, QueryParamsModel} from '../../../../../../core/_base/crud';
// Services and Models
import {
  ContactModel,
  ContactOnServerUpdated,
  ContactsDataSource,
  ContactsPageRequested,
  ManyContactsOnServerDeleted,
  OneContactOnServerDeleted,
  selectContactErrorState,
  selectContactsActionLoading,
  selectContactsPageLastQuery
} from '../../../../../../core/lists';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from 'src/app/core/auth/_services';
import {Update} from '@ngrx/entity';

// Table with EDIT item in new page
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-contacts-list',
  templateUrl: './contacts-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactsListComponent implements OnInit, OnDestroy {
  // Table fields
  dataSource: ContactsDataSource;
  displayedColumns = ['first_name', 'email', 'phone', 'entity_type', 'is_active', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  filterStatus = 'true';
  filterCondition = '';
  lastQuery: QueryParamsModel;
  // Selection
  selection = new SelectionModel<ContactModel>(true, []);
  contactsResult: ContactModel[] = [];
  users: any[];
  // Error Handling
  submissionErrorObj: any;
  hasSubmissionErrors: boolean;
  // Subscriptions
  private subscriptions: any[] = [];
  @Input() filterEntities
  @Output() totalCount = new EventEmitter<any>();

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param activatedRoute: ActivatedRoute
   * @param router: Router
   * @param layoutUtilsService: LayoutUtilsService
   * @param store: Store<AppState>
   * @param cdr: ChangeDetectorRef,
   * @param translate: TranslateService,
   * @param authService: AuthService
   */
  constructor(public dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private layoutUtilsService: LayoutUtilsService,
              private store: Store<AppState>,
              private cdr: ChangeDetectorRef,
              private translate: TranslateService,
              private authService: AuthService
  ) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push({id: 1, sub: sortSubscription});

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadContactsList())
    )
      .subscribe();
    this.subscriptions.push({id: 2, sub: paginatorSubscriptions});

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadContactsList();
      })
    )
      .subscribe();
    this.subscriptions.push({id: 3, sub: searchSubscription});

    // Init DataSource
    this.dataSource = new ContactsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.contactsResult = res;
    });
    this.subscriptions.push({id: 4, sub: entitiesSubscription});
    const lastQuerySubscription = this.store.pipe(select(selectContactsPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push({id: 5, sub: lastQuerySubscription});

    // Read from URL itemId, for restore previous state
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery, +params.id);
      }
      this.loadContactsList();
    });
    this.subscriptions.push({id: 6, sub: routeSubscription});
    this.dataSource.paginatorTotal$.subscribe(res => {
      this.totalCount.emit(res)
    })
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(el => {
      if (el.sub) {
        el.sub.unsubscribe();
      }
    });
  }

  /**
   * Load Contacts List
   */
  loadContactsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction !== '' ? this.sort.direction : 'asc',
      !this.sort.active || this.sort.active === '' ? 'firstName' : this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new ContactsPageRequested({page: queryParams}));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.status = this.filterStatus;
    filter.text = searchText ? searchText : '';
    if (this.filterEntities) {
      filter.related = this.filterEntities
    }
    if (!searchText) {
      return filter;
    }
    filter.name = searchText;
    filter.contact = searchText;
    return filter;
  }

  /**
   * Restore state
   *
   * @param queryParams: QueryParamsModel
   * @param id: number
   */
  restoreState(queryParams: QueryParamsModel, id: number) {

    if (!queryParams.filter) {
      return;
    }
    if ('status' in queryParams.filter) {
      this.filterStatus = queryParams.filter.status.toString();
    }

    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  /** ACTIONS */
  /**
   * Delete contact
   *
   * @param _item: ContactModel
   */
  deleteContact(_item: ContactModel) {
    const _title: string = this.translate.instant('CONTACT.DELETE_CONTACT_SIMPLE.TITLE');
    const _description: string = this.translate.instant('CONTACT.DELETE_CONTACT_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('CONTACT.DELETE_CONTACT_SIMPLE.WAIT_DESCRIPTION');
    const _deleteMessage = this.translate.instant('CONTACT.DELETE_CONTACT_SIMPLE.MESSAGE');

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    const dialogRefSubscription = dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }

      this.store.dispatch(new OneContactOnServerDeleted({id: _item.id}));
      const actionLoadingSubscription = this.store.pipe(select(selectContactsActionLoading)
      ).subscribe(response => {
        if (response !== true) {
          const errorStateSubscription = this.store.pipe(
            select(selectContactErrorState)
          ).subscribe(error => {
            if (error) {
              if (error.error && error.error.errors) {
                this.submissionErrorObj = error.error.errors;
              } else {
                this.submissionErrorObj = [{error: 'GLOBAL.UNSPECIFIED_ERROR'}]
              }
              this.hasSubmissionErrors = true;
            } else {
              this.submissionErrorObj = null;
              this.hasSubmissionErrors = false;
            }
          })
          if (this.subscriptions.find(sub => sub.id === 9) === undefined) {
            this.subscriptions.push({id: 9, sub: errorStateSubscription});
          }
          if (this.submissionErrorObj !== null) {
            const errArray = [];
            this.submissionErrorObj.forEach(errObj => {
              const message = this.translate.instant(errObj.error);
              errArray.push(message);
            });
            const errMsg = errArray.join('\n');
            this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
          } else {
            this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete, 3000, false, false);
            this.loadContactsList();
          }
        }
        this.cdr.detectChanges();
      });
      if (this.subscriptions.find(sub => sub.id === 8) === undefined) {
        this.subscriptions.push({id: 8, sub: actionLoadingSubscription});
      }
    });
    if (this.subscriptions.find(sub => sub.id === 7) === undefined) {
      this.subscriptions.push({id: 7, sub: dialogRefSubscription});
    }
  }

  /**
   * Delete selected contacts
   */
  deleteContacts() {
    const _title: string = this.translate.instant('CONTACT.DELETE_CONTACT_MULTI.TITLE');
    const _description: string = this.translate.instant('CONTACT.DELETE_CONTACT_MULTI.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('CONTACT.DELETE_CONTACT_MULTI.WAIT_DESCRIPTION');
    const _deleteMessage = this.translate.instant('CONTACT.DELETE_CONTACT_MULTI.MESSAGE');

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    const dialogRefSubscription = dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }

      const idsForDeletion: number[] = [];
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.selection.selected.length; i++) {
        idsForDeletion.push(this.selection.selected[i].id);
      }
      this.store.dispatch(new ManyContactsOnServerDeleted({ids: idsForDeletion}));
      const actionLoadingSubscription = this.store.pipe(select(selectContactsActionLoading)
      ).subscribe(response => {
        if (response !== true) {
          const errorStateSubscription = this.store.pipe(
            select(selectContactErrorState)
          ).subscribe(error => {
            if (error) {
              if (error.error && error.error.errors) {
                this.submissionErrorObj = error.error.errors;
              } else {
                this.submissionErrorObj = [{error: 'GLOBAL.UNSPECIFIED_ERROR'}]
              }
              this.hasSubmissionErrors = true;
            } else {
              this.submissionErrorObj = null;
              this.hasSubmissionErrors = false;
            }
            this.cdr.detectChanges();
          })
          if (this.subscriptions.find(sub => sub.id === 12) === undefined) {
            this.subscriptions.push({id: 12, sub: errorStateSubscription});
          }
          if (this.submissionErrorObj !== null) {
            const errArray = [];
            this.submissionErrorObj.forEach(errObj => {
              const message = this.translate.instant(errObj.error);
              errArray.push(message);
            });
            const errMsg = errArray.join('\n');
            this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
          } else {
            this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete, 3000, false, false);
            this.loadContactsList();
          }
          this.selection.clear();
        }
        this.cdr.detectChanges();
      });
      if (this.subscriptions.find(sub => sub.id === 11) === undefined) {
        this.subscriptions.push({id: 11, sub: actionLoadingSubscription});
      }
    });
    if (this.subscriptions.find(sub => sub.id === 10) === undefined) {
      this.subscriptions.push({id: 10, sub: dialogRefSubscription});
    }
  }

  fetchContacts() {
    const messages = [];
    this.selection.selected.forEach(elem => {
      messages.push({
        text: `${elem.first_name + '' + elem.last_name}`,
        id: elem.id.toString(),
        status: elem.is_active
      });
    });
    this.layoutUtilsService.fetchElements(messages);
  }

  /**
   * Update Status for single contact
   */
  updateStatusForContact(contact: ContactModel) {
    const _title: string = this.translate.instant('CONTACT.UPDATE_STATUS_SIMPLE.TITLE');
    const _description: string = this.translate.instant('CONTACT.UPDATE_STATUS_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('CONTACT.UPDATE_STATUS_SIMPLE.WAIT_DESCRIPTION');
    const _updateStatusSingleMessage = this.translate.instant('CONTACT.UPDATE_STATUS_SIMPLE.MESSAGE');

    const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
    const dialogRefSubscription = dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      const newContact: ContactModel = Object.assign(new ContactModel(), contact);
      newContact.changeActiveStatus();
      const updateContact: Update<ContactModel> = {
        id: newContact.id,
        changes: newContact
      };
      this.store.dispatch(new ContactOnServerUpdated({
        partialContact: updateContact,
        contact: newContact
      }));
      const actionLoadingSubscription = this.store.pipe(select(selectContactsActionLoading)
      ).subscribe(response => {
        if (response !== true) {
          const errorStateSubscription = this.store.pipe(
            select(selectContactErrorState)
          ).subscribe(error => {
            if (error) {
              if (error.error && error.error.errors) {
                this.submissionErrorObj = error.error.errors;
              } else {
                this.submissionErrorObj = [{error: 'GLOBAL.UNSPECIFIED_ERROR'}]
              }
              this.hasSubmissionErrors = true;
            } else {
              this.submissionErrorObj = null;
              this.hasSubmissionErrors = false;
            }
          })
          if (this.subscriptions.find(sub => sub.id === 15) === undefined) {
            this.subscriptions.push({id: 15, sub: errorStateSubscription});
          }
          if (this.submissionErrorObj !== null) {
            const errArray = [];
            this.submissionErrorObj.forEach(errObj => {
              const message = this.translate.instant(errObj.error);
              errArray.push(message);
            });
            const errMsg = errArray.join('\n');
            this.layoutUtilsService.showActionNotification(errMsg, MessageType.Update, 3000, false, false);
          } else {
            this.layoutUtilsService.showActionNotification(_updateStatusSingleMessage, MessageType.Update, 3000, false, false);
          }
        }
      });
      if (this.subscriptions.find(sub => sub.id === 14) === undefined) {
        this.subscriptions.push({id: 14, sub: actionLoadingSubscription});
      }
    });
    if (this.subscriptions.find(sub => sub.id === 13) === undefined) {
      this.subscriptions.push({id: 13, sub: dialogRefSubscription});
    }
  }

  /**
   * Redirect to view page
   *
   * @param id: any
   */
  viewContact(id) {
    this.router.navigate(['lists/contacts/view/' + id], {queryParams: {viewContext: 'CONTACT'}});
  }

  /**
   * Redirect to edit page
   *
   * @param id: any
   */
  editContact(id) {
    this.router.navigate(['lists/contacts/edit/' + id], {
      queryParams: {
        viewContext: 'CONTACT',
      }
    });
  }

  createContact() {
    this.router.navigateByUrl('/lists/contacts/add');
  }

  /**
   * Check all rows are selected
   */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.contactsResult.length;
    return numSelected === numRows;
  }

  /**
   * Selects all rows if they are not all selected; otherwise clear selection
   */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.contactsResult.forEach(row => this.selection.select(row));
    }
  }

  /** UI */
  /**
   * Retursn CSS Class Name by status
   *
   * @param is_active: boolean
   */
  getItemCssClassByStatus(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return 'danger';
      case true:
        return 'success';
    }
  }

  /**
   * Returns Item Status in string
   * @param is_active: boolean
   */
  getItemStatusString(is_active: boolean = false): string {
    switch (is_active) {
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  /**
   * Returns Item Status in string
   * @param adminID: number
   */
  getFullName(contact: ContactModel): string {
    return contact.first_name + ' ' + contact.last_name;
  }

  formatPhone(phone) {
    if ((phone) != null && (phone) !== '0' && (phone) !== '') {
      return '(' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + '-' + phone.substring(6, 10);
    } else {
      return '';
    }
  }

  getClassColor(type) {
    switch (type) {
      case 'PATIENT' :
        return 'warning'
      case 'FACILITY' :
        return 'success'
      case 'DOCTOR' :
        return 'primary'
      default:
        return 'danger'
    }
  }

  getTextEntity(contact: ContactModel) {
    switch (contact.entity_types) {
      case null :
        return 'Undefined'
      case 'PATIENT' :
        return 'Patient'
      case 'FACILITY' :
        return 'Facility'
      case 'DOCTOR' :
        return 'Doctor'
      default:
        return contact.entity_types
    }
  }

  contactHasMergeTypes(contact) {
    return contact.merge_types !== undefined && contact.merge_types !== null && contact.merge_types.length > 0;
  }
}
