// Angular
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
// Material
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
// RXJS
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge, Subscription} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../../../core/reducers';
// CRUD
import {LayoutUtilsService, MessageType, QueryParamsModel} from '../../../../../../core/_base/crud';
// Services and Models
import {
  CategoriesDataSource,
  CategoriesPageRequested,
  Category,
  CategoryActionTypes,
  CategoryOnServerDeleted,
  selectCategoryError,
  selectCategoryLatestSuccessfullAction
} from '../../../../../../core/lists';
import {TranslateService} from '@ngx-translate/core';
import {CategoryAddDialogComponent} from '../category-add/category-add.dialog.component';
import {Router} from '@angular/router';
import {SubheaderService} from '../../../../../../core/_base/layout';
import {AuthService, UserService} from '../../../../../../core/auth/_services';
import {Update} from '@ngrx/entity';
import {CategoryUpdated} from '../../../../../../core/lists/_actions/category.actions';
import {AuthServer} from '../../../../../../core/enums/enums';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],

  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesListComponent implements OnInit, OnDestroy {
  // Table fields
  dataSource: CategoriesDataSource;
  displayedColumns = ['id', 'name', 'role', 'connectWithGoogle', 'connectWithFax', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  categoriesResult: Category[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];
  public title: string;
  disableBtn: boolean;
  disableBtnFax: boolean;
  listFlag = null;
  AuthServer = AuthServer;

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param layoutUtilsService: LayoutUtilsService
   * @param store: Store<AppState>
   * @param translate: TranslateService
   */
  constructor(
    public dialog: MatDialog,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private userService: UserService,
    public subheaderService: SubheaderService,
    private auth: AuthService,
    private cdr: ChangeDetectorRef,
    private  router: Router
  ) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    this.subheaderService.breadcrumbs$.subscribe(bc => {
      if (bc.length > 0) {
        this.title = bc[bc.length - 1].title;
      }
    })
    this.loadCategoriesList();
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadCategoriesList())
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadCategoriesList();
      })
    ).subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new CategoriesDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.categoriesResult = res;
    });
    this.subscriptions.push(entitiesSubscription);
    const errorSubscription = this.store.pipe(select(selectCategoryError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Update,
          3000,
          true,
          false
        );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectCategoryLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch (res.action_type) {
          case CategoryActionTypes.CategoryCreated:
            successMsg = this.translate.instant('CATEGORY.ADD_SUCCESS');
            msgType = MessageType.Update;
            break;
          case CategoryActionTypes.CategoryUpdated:
            successMsg = this.translate.instant('CATEGORY.UPDATE_SUCCESS');
            msgType = MessageType.Update;
            break;
          case CategoryActionTypes.CategoryDeleted:
            successMsg = this.translate.instant('CATEGORY.DELETE_SUCCESS');
            msgType = MessageType.Delete;
            break;
        }
        if (successMsg) {
          this.layoutUtilsService.showActionNotification(
            successMsg,
            msgType,
            3000,
            true,
            false
          );
        }
        this.loadCategoriesList();
      }
    });
    this.subscriptions.push(successSubscription);
    this.currentUserRoleType = this.auth.currentUserValue.typeRole
    // tslint:disable-next-line:triple-equals
    if (this.currentUserRoleType != 'Admin') {
      this.displayedColumns.splice(this.displayedColumns.indexOf('connectWithGoogle'), 1);
    }
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Load Categories List
   */
  loadCategoriesList() {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction !== '' ? this.sort.direction : 'desc',
      !this.sort.active || this.sort.active === '' ? 'created' : this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new CategoriesPageRequested({page: queryParams}));
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.searchData = searchText ? searchText : '';
    return filter;
  }

  /** ACTIONS */
  currentUserRoleType: string;

  /**
   * Delete category
   *
   * @param _item: Category
   */
  deleteCategory(_item: Category) {
    const _title: string = this.translate.instant('CATEGORY.DELETE_CATEGORY_SIMPLE.TITLE');
    const _description: string = this.translate.instant('CATEGORY.DELETE_CATEGORY_SIMPLE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('CATEGORY.DELETE_CATEGORY_SIMPLE.WAIT_DESCRIPTION');

    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.store.dispatch(new CategoryOnServerDeleted({id: _item.id}));
    });
  }

  editCategory(category) {
    this.dialog.open(CategoryAddDialogComponent, {data: {category}, width: '450px'});
  }

  createCategory() {
    this.dialog.open(CategoryAddDialogComponent, {width: '450px'});
  }

  authAccount(category, authServer: AuthServer) {
    this.disableBtn = true;
    console.log(this.router.url);
    if (this.router.url === '/lists/category') {
      this.listFlag = '/lists/category'
    }
    if (this.router.url === '/lists/team') {
      this.listFlag = '/lists/team'
    }
    this.userService.getOAuthURL(authServer, this.listFlag, category.id).subscribe(res => {
      console.log(res);
      window.location.href = res.auth_uri;
      this.disableBtn = false;
    });
  }

  disconnectAccount(category) {
    this.disableBtn = true;
    this.userService.disconnectFromAuthServer(category.id).subscribe(res => {
      this.disableBtn = false;
      // @ts-ignore
      const updateCategory: Update<Category> = {
        id: category.id,
        changes: {inbox_activated: false}
      };
      // category.inbox_activated = false;
      this.store.dispatch(new CategoryUpdated(updateCategory));
      this.cdr.detectChanges();
    })
  }

  connectionFunction(connection: boolean, category,authServer: AuthServer) {
    if (!connection) {
      this.authAccount(category, authServer);
    } else {
      this.disconnectAccount(category)
    }
  }

  faxConnectionFunction(connection: boolean, category) {
    if (!connection) {
      this.connectWithFax(category)
    } else {
      this.disconnectFromFax(category)
    }
  }

  connectWithFax(category) {
    this.disableBtnFax = true;
    this.userService.connectFax(category.id).subscribe(res => {
      this.disableBtnFax = false;
      const updateCategory: Update<Category> = {
        id: category.id,
        changes: {fax_activated: true}
      };
      // category.inbox_activated = false;
      this.store.dispatch(new CategoryUpdated(updateCategory));
      this.cdr.detectChanges();
    })
  }

  disconnectFromFax(category) {
    this.disableBtnFax = true;
    this.userService.disconnectFax(category.id).subscribe(res => {
      this.disableBtnFax = false;
      const updateCategory: Update<Category> = {
        id: category.id,
        changes: {fax_activated: false}
      };
      this.store.dispatch(new CategoryUpdated(updateCategory));
      this.cdr.detectChanges();
    })
  }

  getText(textType: boolean, type: string) {
    if (type === 'EMAIL') {
      if (textType === false) {
        // return 'Connect with Google'
        return ''
      }
      return 'Disconnect from Account'
    }
    if (textType === false) {
      return 'Connect with Fax'
    }
    return 'Disconnect from Fax'
  }

  isConnected(state: boolean) {
    if (state) {
      return false;
    }
    return true;
  }
}
