// Angular
import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Subscription } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../../../../core/reducers';
// Services and Models
import {
    Category,
    CategoryOnServerCreated,
    CategoryOnServerUpdated,
    selectCategoryLatestSuccessfullAction
  } from '../../../../../../core/lists';
import {AuthService, currentUser} from 'src/app/core/auth';
import { TranslateService } from '@ngx-translate/core';
import { skip } from 'rxjs/operators';
import { Update } from '@ngrx/entity';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-category-add',
    templateUrl: './category-add.dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryAddDialogComponent implements OnInit, OnDestroy {
    category: Category;
    categoryForm: FormGroup;
    hasFormErrors: boolean;
    clicked: boolean;
    companyId: number;
	  private subscriptions: Subscription[] = [];
    searchRolesFn = this.authService.searchRoles.bind(this.authService);
    roleCtrl = new FormControl();
    @ViewChild('roleAutocomplete') roleAutocomplete;
    formatNewRoleFn = ((value) => {
      return value;
    }).bind(this)
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<CategoryAddDialogComponent>,
        private store: Store<AppState>,
        private categoryFB: FormBuilder,
        private translate: TranslateService,
        public dialog: MatDialog,
        private authService: AuthService
    ) { }

    ngOnInit() {
    this.category = this.data && this.data.category ? this.data.category : new Category();
		const userSub = this.store.pipe(select(currentUser)).subscribe((res) => {
			this.companyId = res !== undefined ? res.companyID : -1;
    });
    this.createForm();
		this.subscriptions.push(userSub);
        const successSubscription = this.store.pipe(select(selectCategoryLatestSuccessfullAction), skip(1)).subscribe((res) => {
          if (res && res.action_type) {
            this.dialogRef.close();
          }
        });
        this.subscriptions.push(successSubscription);
    }

    getTitle() {
        return this.category.id > 0 ? this.translate.instant('CATEGORY.EDIT_CATEGORY') :  this.translate.instant('CATEGORY.ADD_CATEGORY');
    }

    /**
     * On destroy
     */
    ngOnDestroy() {
		  this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Save data
     */
    onSubmit() {
        this.hasFormErrors = false;
        const controls = this.categoryForm.controls;
      this.roleAutocomplete.updateValueAndValidity();
      /** check form */
        if (this.categoryForm.invalid) {
            Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
            this.hasFormErrors = true;
            return;
        }
        this.clicked = true;
        if(this.category.id > 0) {
          this.editCategory();
        } else {
          this.createCategory();
        }
    }

    createCategory() {
      const body:any = {
        name: this.categoryForm.get('name').value,
        roles: this.categoryForm.get('roles').value
      }
		  this.store.dispatch(new CategoryOnServerCreated(body));
    }

    editCategory() {
      const controls = this.categoryForm.controls;
      const _category = new Category();
      _category.company_id = this.companyId;
      _category.id = this.category.id;
      _category.name = controls.name.value;
      _category.roles = controls.roles.value;
      const updateCategory: Update<Category> = {
        id: _category.id,
        changes: _category
      };
      this.store.dispatch(new CategoryOnServerUpdated({
        partialCategory: updateCategory,
        category: _category
      }));
    }

    isControlHasError(controlName: string, validationType: string): boolean {
        const control = this.categoryForm.controls[controlName];
        if (!control) {
            return false;
        }
        const result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    }

    close() {
        this.dialogRef.close();
    }

    createForm() {
        this.categoryForm = this.categoryFB.group({
          name: [this.category.name, Validators.required],
          roles: [],
        });
        if(this.category && this.category.id){
          const newParticipant =this.category.roles.map(value => {
            return {
              id: value.id,
              name: value.name
            }
          });
          this.categoryForm.get('roles').setValue(newParticipant);
          this.roleCtrl.setValue(newParticipant);

        }
    }

  selected(val): void {
        const newParticipant = val.map(value => {
          return {
            id: value.id,
            name: value.name
          }
        });
        this.categoryForm.get('roles').setValue(newParticipant);
  }

  chipListValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return !control.value || control.value.length === 0 ? {chipListRequiredError: true} : null;
    };
  }
  filterRoleFn(event: any): string {
    return event ? event.name : '';
  }
}
