import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {Update} from '@ngrx/entity';
import {select, Store} from '@ngrx/store';
import {DatePipe} from '@angular/common';
import {User} from 'src/app/core/auth';
import {TranslateService} from '@ngx-translate/core';
import {AppState} from 'src/app/core/reducers';
import {
  CategoryService,
  selectTemplateLatestSuccessfullAction,
  selectTemplatesActionLoading,
  TemplateOnServerCreated,
  TemplateOnServerUpdated
} from 'src/app/core/lists';
import {skip} from 'rxjs/operators';
import {Template} from '../../../../../../core/lists/_models/template.model';
import {GeneralAutocompleteComponent} from '../../../../../partials/layout/general-autocomplete/general-autocomplete.component';
import * as _ from 'lodash';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {UserService} from '../../../../../../core/auth/_services';

@Component({
  selector: 'kt-template-edit',
  templateUrl: './template-edit.component.html',
  styleUrls: ['./template-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class TemplateEditComponent implements AfterViewInit, OnInit, OnDestroy {
  template: Template;
  templateForm: FormGroup;
  hasFormErrors = false;
  viewLoading = false;
  currentUser: User;
  subscriptions: Subscription[] = [];
  isReadOnly = false
  categoryAutocompleteCall = this.categoryService.getAllCategories.bind(this.categoryService);
  @ViewChild('categoryAutocomplete') categoryAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('followerAutocomplete') followerAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('assigneeAutocomplete') assigneeAutocomplete: GeneralAutocompleteComponent;
  showSubTaskVar = false
  body: any;
  subTasksJson = [];
  @ViewChild('wizard', {static: true}) el: ElementRef;
  assigneeAutocompleteCall = this.userService.searchUsers.bind(this.userService);
  priorityOptions = ['HIGH', 'MEDIUM', 'LOW'];

  ngAfterViewInit(): void {
    // const editedTemplate = this.prepareTemplate();
    if (this.template && this.template.id > 0) {
      const temp = JSON.parse(this.template.template)
      if (temp.task_followers) {
        // this.templateForm.controls.task_followers.setValue(temp.task_followers)
        // this.templateForm.controls.task_followers.updateValueAndValidity();
        this.followerAutocomplete.setValue(temp.task_followers);
        this.followerAutocomplete.updateValueAndValidity();
      }
      if (temp.task_responsible_users) {
        this.assigneeAutocomplete.setValue(temp.task_responsible_users);
        this.assigneeAutocomplete.updateValueAndValidity();
      }
      this.categoryService.getCategoryById(this.template.category_id).subscribe(res => {
        // this.templateForm.controls.category.setValue(res.data)
        // this.templateForm.controls.category.updateValueAndValidity();
        this.categoryAutocomplete.setValue(res.data);
        this.categoryAutocomplete.updateValueAndValidity();
      })
    }
    // Initialize form wizard
    const wizard = new KTWizard(this.el.nativeElement, {
      startStep: 1
    });

    wizard.on('beforeNext', (wizardObj) => {
      // this.checkRequired();
      // wizardObj.stop();
    });

    // Change event
    wizard.on('change', () => {
      setTimeout(() => {
        KTUtil.scrollTop();
      }, 500);
    });
  }

  filterCategoryFn(event: any): string {
    return event ? event.name : '';
  }

  onCategoryChange(val) {
    this.templateForm.controls.category.setValue(val)
    this.templateForm.controls.category.updateValueAndValidity();
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<TemplateEditComponent>,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private translate: TranslateService,
    private categoryService: CategoryService,
    public datepipe: DatePipe,
    private cdr: ChangeDetectorRef,
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    this.isReadOnly = this.data.isReadOnly
    this.template = this.data.template ? this.data.template : new Template();
    const loadingSub = this.store.pipe(
      select(selectTemplatesActionLoading)
    ).subscribe(res =>
      this.viewLoading = res
    );
    this.subscriptions.push(loadingSub);
    const successSubscription = this.store.pipe(select(selectTemplateLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        this.dialogRef.close({added: true});
      }
    });
    this.subscriptions.push(successSubscription);
    this.createForm();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {

    this.templateForm = this.fb.group({
      name: [this.template.name, Validators.required],
      category: [null],
      title: [null, Validators.required],
      description: [],
      subTitle: [],
      subDescription: [],
      isActive: [this.template.all_roles ? this.template.all_roles : false],
      priority: [null],
      task_responsible_users: [null],
      task_followers: [null],
    });
    if (this.isReadOnly) {
      this.templateForm.disable()
    }
    if (this.template && this.template.id > 0) {
      const temp = JSON.parse(this.template.template)
      this.templateForm.controls.title.setValue(temp.task.title)
      this.templateForm.controls.priority.setValue(temp.priority)
      this.templateForm.controls.description.setValue(temp.task.description)
      this.subTasksJson = temp.subTasks
      this.showSubTaskVar = true
    }


  }

  getTitle() {
    let result = this.translate.instant('TEMPLATE.NEW_TEMPLATE');
    if (!this.template || !this.template.id) {
      return result;
    }
    if (this.isReadOnly) {
      result = this.translate.instant('TEMPLATE.VIEW_TEMPLATE') + `-` + `${this.template.name}`;
      return result;
    }
    result = this.translate.instant('TEMPLATE.EDIT_TEMPLATE') + `-` + `${this.template.name}`;
    return result;
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.templateForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  prepareTemplate(): Template {
    const model = _.clone(this.templateForm.value);
    const _template = new Template();
    _template.id = this.template.id
    _template.category_id = model.category ? model.category.id : null
    _template.name = model.name
    _template.template = {
      task: {
        title: model.title,
        description: model.description,
      },
      subTasks: this.subTasksJson,
      priority: model.priority,
      task_followers: model.task_followers,
      task_responsible_users: model.task_responsible_users
    }
    _template.all_roles = model.isActive
    _template.template = JSON.stringify(_template.template)
    return _template;
  }

  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.templateForm.controls;
    this.categoryAutocomplete.updateValueAndValidity();
    this.assigneeAutocomplete.updateValueAndValidity();
    this.followerAutocomplete.updateValueAndValidity();
    if (this.templateForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }

    const editedTemplate = this.prepareTemplate();
    if (editedTemplate.id > 0) {
      this.updateTemplate(editedTemplate);
    } else {
      this.createTemplate(editedTemplate);
    }
  }

  updateTemplate(_template: Template) {
    const updateTemplate: Update<Template> = {
      id: _template.id,
      changes: _template
    };
    this.store.dispatch(new TemplateOnServerUpdated({
      partialTemplate: updateTemplate,
      template: _template,
    }));
  }

  createTemplate(_template: Template) {
    this.store.dispatch(new TemplateOnServerCreated({template: _template}));
  }

  onAlertClose() {
    this.hasFormErrors = false;
  }

  displayDrug(drug): string {
    return drug ? drug.drug : '';
  }

  addToSubTaskJson() {
    const title = this.templateForm.get('subTitle').value
    const description = this.templateForm.get('subDescription').value
    if (!title || !description) {
      return;
    }
    this.subTasksJson.push({
      title,
      description,
      inherit: true,
      task_responsible_users: this.templateForm.controls.task_responsible_users.value,
      task_followers: this.templateForm.controls.task_followers.value,
      priority: this.templateForm.controls.priority.value,
    })
    this.templateForm.get('subTitle').reset();
    this.templateForm.get('subDescription').reset();
  }

  removeSubTask(obj) {
    this.subTasksJson = this.subTasksJson.filter(item => item !== obj);
  }

  editSubTask(obj) {
    this.templateForm.get('subTitle').setValue(obj.title);
    this.templateForm.get('subDescription').setValue(obj.description);
  }

  showSubTask() {
    this.showSubTaskVar = true
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.templateForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getStatusString(val): string {
    switch (val) {
      case true:
        return 'Yes';
      case false:
        return 'No';
    }
    return '';
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.subTasksJson, event.previousIndex, event.currentIndex);
  }

  filterAssigneeFn(assignee: any): string {
    let res = ''
    if (assignee) {
      res += assignee.fullName
    }
    if (assignee && assignee.name) {
      res += '  ' + `<small class="text-muted">` + assignee.name + `</small>`
    }
    return res
  }

  formatNewAssigneeFn(assignee: any) {
    return assignee;
  }

  formatNewFollowerFn(follower: any) {
    return follower
  }

  onAssigneeChange(val) {
    const users = val.map(item => {
      if (item && !item.responsible_id) {
        const user = {
          responsible_id: item.id,
          responsible_type: 'USER',
          fullName: item.fullName,
          name: item.name
        };
        return user
      }
      return item
    })
    this.templateForm.controls.task_responsible_users.setValue(users);
    this.templateForm.controls.task_responsible_users.updateValueAndValidity();
  }


  onFollowerChange(val) {
    const users = val.map(item => {
      if (item && !item.follower_id) {
        const user = {
          follower_id: item.id,
          type: 'USER',
          fullName: item.fullName,
          name: item.name
        };
        return user
      }
      return item
    })
    this.templateForm.controls.task_followers.setValue(users);
    this.templateForm.controls.task_followers.updateValueAndValidity();
  }

  getPriority(priority) {
    if (priority === 'HIGH')
      return 'svg-icon svg-icon-danger svg-icon-2x'
    if (priority === 'MEDIUM')
      return 'svg-icon svg-icon-warning svg-icon-2x'
    return 'svg-icon svg-icon-2x'
  }

  getPriorityText(priority) {
    switch (priority) {
      case 'HIGH':
        return this.translate.instant('TASK.HIGH');
      case 'MEDIUM':
        return this.translate.instant('TASK.MEDIUM');
      case 'LOW':
        return this.translate.instant('TASK.LOW');
      default:
        return '';
    }
  }

  getInheritString(val): string {
    if (!val) {
      return 'Inherit'
    }
    return 'Reject!';
  }

  editSubTaskInherit(obj) {
    obj.inherit = !obj.inherit;
    if (obj.inherit) {
      obj.task_responsible_users = this.templateForm.controls.task_responsible_users.value;
      obj.task_followers = this.templateForm.controls.task_followers.value;
      obj.priority = this.templateForm.controls.priority.value;
      return;
    }
    delete obj.task_responsible_users;
    delete obj.task_followers;
    delete obj.priority;
  }
}
