import {ChangeDetectionStrategy, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {debounceTime, distinctUntilChanged, skip, tap} from 'rxjs/operators';
import {fromEvent, merge, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {TemplateEditComponent} from '../template-edit/template-edit.component';
import {ActivatedRoute} from '@angular/router';
import {Template} from '../../../../../../core/lists/_models/template.model';
import {
  CategoryService,
  ClearTemplateState,
  selectTemplateError,
  selectTemplateLatestSuccessfullAction,
  selectTemplatesPageLastQuery,
  TemplateActionTypes,
  TemplateOnServerDeleted,
  TemplatesDataSource,
  TemplatesPageRequested
} from '../../../../../../core/lists';
import {LayoutUtilsService, MessageType, QueryParamsModel} from '../../../../../../core/_base/crud';
import {AppState} from '../../../../../../core/reducers';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-templates-list',
  templateUrl: './templates-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplatesListComponent implements OnInit, OnDestroy {
  @Input() isViewMode: boolean;
  dataSource: TemplatesDataSource;
  displayedColumns = ['name','category_id', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  lastQuery: QueryParamsModel;
  templatesResult: Template[] = [];
  private subscriptions: Subscription[] = [];
  public isReadOnly = false;
  categories = []
  categoryId: null;
  categoryList = []

  constructor(
    public dialog: MatDialog,
    private layoutUtilsService: LayoutUtilsService,
    private store: Store<AppState>,
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private categoryService: CategoryService
  ) {
    this.categoryService.getCategories().subscribe(res => {
      if(res)
      this.categories = res.data
    })
  }

  ngOnInit() {
    // Init DataSource
    this.dataSource = new TemplatesDataSource(this.store);
    const dataSourceSub = this.dataSource.entitySubject.subscribe(res => this.templatesResult = res);
    this.subscriptions.push(dataSourceSub);
    const errorSubscription = this.store.pipe(select(selectTemplateError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Delete,
          3000,
          true,
          false
        );
      }
    });
    this.subscriptions.push(errorSubscription);
    const successSubscription = this.store.pipe(select(selectTemplateLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch (res.action_type) {
          case TemplateActionTypes.TemplateCreated :
            successMsg = this.translate.instant('TEMPLATE.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case TemplateActionTypes.TemplateUpdated :
            successMsg = this.translate.instant('TEMPLATE.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case TemplateActionTypes.TemplateDeleted :
            successMsg = this.translate.instant('TEMPLATE.DELETE_MESSAGE');
            msgType = MessageType.Delete;
            break;
        }
        this.layoutUtilsService.showActionNotification(
          successMsg,
          msgType,
          3000,
          true,
          false
        );
      }
    });
    this.subscriptions.push(successSubscription);
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);

    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadTemplatesList())
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadTemplatesList();
      })
    ).subscribe();
    this.subscriptions.push(searchSubscription);
    const routeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.id) {
        this.restoreState(this.lastQuery);
      }
      this.loadTemplatesList();
    });
    this.subscriptions.push(routeSubscription);

    const lastQuerySubscription = this.store.pipe(select(selectTemplatesPageLastQuery)).subscribe(res => this.lastQuery = res);
    // Load last query from store
    this.subscriptions.push(lastQuerySubscription);
    // if (this.activatedRoute.url[`_value`][1].path === 'view') {
    //   this.isReadOnly = true
    //   this.isReadOnly ? this.displayedColumns = ['name', 'category_id']
    //     : this.displayedColumns = ['name', 'category_id', 'actions'];
    // }
    const categorySubscription = this.categoryService.getCategories().subscribe(res => {
      if (res)
        this.categoryList = res.data
    })
    this.subscriptions.push(categorySubscription)

  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.store.dispatch(new ClearTemplateState())
  }

  loadTemplatesList(categoryId?) {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction !== '' ? this.sort.direction : 'desc',
      !this.sort.active || this.sort.active === '' ? 'created' : this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    this.store.dispatch(new TemplatesPageRequested({page: queryParams}));
  }

  filterConfiguration(): any {
    const filter: any = {};
    filter.searchData = this.searchInput.nativeElement.value;
    filter.categoryId = this.categoryId;
    return filter;
  }

  restoreState(queryParams: QueryParamsModel) {
    if (!queryParams.filter) {
      return;
    }
    if (queryParams.filter.model) {
      this.searchInput.nativeElement.value = queryParams.filter.model;
    }
  }

  deleteTemplate(_item: Template) {
    const _title: string = this.translate.instant('TEMPLATE.DELETE.TITLE');
    const _description: string = this.translate.instant('TEMPLATE.DELETE.DESCRIPTION');
    const _waitDesciption: string = this.translate.instant('TEMPLATE.DELETE.WAIT_DESCRIPTION');
    const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.store.dispatch(new TemplateOnServerDeleted({id: _item.id}));
    });
  }

  addTemplate() {
    const newTemplate = new Template();
    this.updateTemplate(newTemplate);
  }

  updateTemplate(template: Template) {
    this.dialog.open(TemplateEditComponent, {width: '40vw', data: {template, isReadOnly: false}});
  }

  viewTemplate(template: Template) {
    this.dialog.open(TemplateEditComponent, {width: '40vw', data: {template, isReadOnly: true}});
  }
  getCategory(id){
    const obj = this.categories.filter(x => x.id === id)[0]
    return obj ? obj.name:'';
  }
}
