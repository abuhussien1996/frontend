import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
	templateUrl: './lists.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListsComponent implements OnInit {

	constructor() { }

	ngOnInit() { }
}
