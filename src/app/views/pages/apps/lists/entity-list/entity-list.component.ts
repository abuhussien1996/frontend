import {Component, OnInit, ViewChild} from '@angular/core';
import {BaseDataSource, QueryParamsModel} from '../../../../../core/_base/crud';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {merge, Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {EntityService} from '../../../management/services/entity.service';
import {of} from 'rxjs/internal/observable/of';
import {tap} from 'rxjs/operators';
import {GeneralFormDialogComponent} from '../general-form-dialog/general-form-dialog.component';
import {ColDef} from 'ag-grid-community';

@Component({
  selector: 'kt-entity-list',
  templateUrl: './entity-list.component.html',
  styleUrls: ['./entity-list.component.scss']
})
export class EntityListComponent implements OnInit {

  dataSource = new BaseDataSource();
  displayedColumns = [];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort', {static: true}) sort: MatSort;
  private subscriptions: Subscription[] = [];
  // tslint:disable-next-line:max-line-length
  schema : any;
  columnDefs: ColDef[] = [
    {field: 'make'},
    {field: 'model'},
    {field: 'price'}
  ];

  rowData = [
    {make: 'Toyota', model: 'Celica', price: 35000},
    {make: 'Ford', model: 'Mondeo', price: 32000},
    {make: 'Porsche', model: 'Boxter', price: 72000}
  ];

  constructor(private dialog: MatDialog, private entityService: EntityService) {
    this.dataSource.isPreloadTextViewed$ = of(true);
  }

  ngOnInit(): void {
    this.loadEntities();
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => {
        this.loadEntities();
      })
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);
    this.entityService.getEntitiesBY().subscribe(res => {
      this.schema = JSON.parse(res.data.entity_schema.schema);
      // @ts-ignore
      if (this.schema && this.schema.components) {
        // @ts-ignore
        this.schema.components.forEach(c => {
          this.displayedColumns.push(c.key)
        })
      }
    })
  }

  addEntity() {
    const schema = this.schema;
    this.dialog.open(GeneralFormDialogComponent, {data: {schema}}).afterClosed().subscribe(res => {
      if (res && res.reload) {
        this.loadEntities();
      }
    })

  }

  private loadEntities() {
    this.dataSource.loading$ = of(true);
    this.entityService.getEntities(new QueryParamsModel({}, undefined, undefined,
      this.paginator.pageIndex, this.paginator.pageSize)).subscribe(res => {
      this.dataSource.loading$ = of(false);
      this.dataSource.isPreloadTextViewed$ = of(false);
      this.dataSource.entitySubject.next(res.data);
      this.dataSource.paginatorTotalSubject.next(res.count);
      this.dataSource.hasItems = res.count > 0;
    })

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(el => el.unsubscribe());
  }

  getValue(label: any, entity: any) {
    this.schema = JSON.parse(entity.schema);
    // @ts-ignore
    if (this.schema && this.schema.components) {
      // @ts-ignore
      this.schema.components.forEach(c => {
        if (c.key === label) {
          return c.data;
        }
        return c.defaultValue;
      })
    }
  }
}
