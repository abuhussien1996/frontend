// Angular
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { EventLegaciesService } from './event-legacies.service';
import { Formio } from 'angular-formio';
import Webform from 'formiojs/Webform';
import { DatePipe } from '@angular/common';
import { Client, Contact, Medication, Patient, Pharmacy, PharmacyLocation } from './models';
import { DomSanitizer } from '@angular/platform-browser';
import { EntitySearchDialogComponent } from './entity-search.dialog.component';
import { AddMedicationDialogComponent } from './add-medication.dialog.component';
import { AddPatientDialogComponent } from './add-patient.dialog.component';
import { buffer } from 'rxjs/operators';
import { LayoutUtilsService, MessageType } from 'src/app/core/_base/crud';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Subscription } from 'rxjs';
import { currentUser } from 'src/app/core/auth';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-legacies-event',
    templateUrl: './event.dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventDialogComponent implements OnInit {
    faxData: any = '';
    filename = '';
    contentType = '';
    k: number;
    i: number;
    pharmacyEmail: any[] = [];
    pdfSizeMega: boolean;
    pdfSizelessMega: boolean;
    clientEmail: any[] = [];
    transaction_id = 0;
    inputForm: JSON;
    inputData: JSON;
    collectedForm: JSON;
    collectedData: JSON;
    extraData: JSON;
    tran_id: number;
    event_id: number;
    date_time2: any;
    option: string;
    bodyClient: string;
    webform: Webform;
    referralInfo: string;
    emailType: any;
    patients: Patient[] = [];
    clients: Client[] = [];
    contacts: Contact[] = [];
    pharmacies: Pharmacy[] = [];
    pharmacies_location: PharmacyLocation[] = [];
    type: string;
    srcImg: any;
    imgDisplay: boolean;
    pdf: any;
    walaa: number;
    pdfDisplay: boolean;
    eventStatus: string;
    medications: Medication[] = [];
    id: any;
    currentUser: any;
    private subscriptions: Subscription[] = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<EventDialogComponent>,
        public dialog: MatDialog,
        private eventLegaciesService: EventLegaciesService,
        private cdr: ChangeDetectorRef,
        public datepipe: DatePipe,
        private sanitizer: DomSanitizer,
        private layoutUtilsService: LayoutUtilsService,
        private store: Store<AppState>
    ) { }

    ngOnInit() {
        this.pdfSizeMega = false;
        this.pdfSizelessMega = false;
        this.i = 0;
        this.k = 0;
        this.srcImg = '';
        this.pdf = '';
        this.imgDisplay = false;
        this.pdfDisplay = false;
        this.transaction_id = this.data.transaction_id;
        this.tran_id = this.data.tran_id;
        this.event_id = this.data.event_id;
        this.eventStatus = this.data.status;
        this.date_time2 = this.data.date_time;
        this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe((res) => {
            if (res) {
                this.currentUser = res.userName;
            }
        }));
        this.getEventData();
    }

    getEventData() {
        this.filename = '';
        this.contentType = '';
        this.faxData = '';
        this.pharmacyEmail = [];
        this.clientEmail = [];
        this.k = 0;
        this.i = 0;
        this.eventLegaciesService.getOrderEntryData(this.transaction_id).subscribe((res) => {
            this.collectedForm = JSON.parse(res[`collectedForm`]);
            this.collectedData = JSON.parse('{ "data":' + res[`collectedData`] + '}');
            this.collectedData[`data`].patient = res[`patient`];
            this.collectedData[`data`].client = res[`client`];
            this.collectedData[`data`].pharmacy = res[`pharmacy`];
            this.collectedData[`data`].medication = res[`medication`];
            this.collectedData[`data`].qty = res[`qty`];
            this.collectedData[`data`].refills = res[`refills`];
            this.collectedData[`data`].jcode = res[`jcode`];
            this.collectedData[`data`].gcn = res[`gcn`];
            this.collectedData[`data`].ndc = res[`ndc`];
            this.collectedData[`data`].diagnosis = res[`diagnosis`];
            this.collectedData[`data`].drugNote = res[`drugNote`];
            this.collectedData[`data`].sig = res[`sig`];
            this.collectedData[`data`].contactPerson = res[`contactPerson`];
            this.extraData = JSON.parse(res[`extraData`]);
            this.extraData[`pharmacyFax`] = res[`pharmacyFaxes`];
            this.extraData[`clientFax`] = res[`clientFax`];
            this.extraData[`tran_id`] = this.tran_id;
            this.walaa = JSON.parse(res[`extraData`]).pharmacy_id;
            if (this.event_id === 29 || this.event_id === 32) {
                const tempDate = new Date(this.date_time2);
                this.collectedData[`data`].receivedDate = this.datepipe.transform(tempDate, 'MM/dd/yy hh:mm a');
                const tempDate2 = new Date(res[`last_Edit`]);
                this.collectedData[`data`].lastUpdated = this.datepipe.transform(tempDate2, 'MM/dd/yy hh:mm a');
                this.collectedData[`data`].tracking = this.extraData[`trackingNumber`];
                this.collectedData[`data`].fillDate = this.extraData[`pharmacy_fill_date`];
                this.collectedData[`data`].shipDate = this.extraData[`pharmacy_ship_date`];
                this.collectedData[`data`].deliveredDate = this.extraData[`prescription_delivered_date`];
                this.collectedData[`data`].courier = this.extraData[`courier_id`];
                this.collectedData[`data`].status = this.extraData[`pharmacy_order_status`];
                this.collectedData[`data`].Pharmacy_location = this.extraData[`pharmacy_location_name`];
            }
            if (this.event_id === 34 || this.event_id === 33) {
                if (this.event_id === 33) {
                    this.option = 'New'
                } else if (this.event_id === 34) {
                    this.option = 'Refill'
                }
                const data = {
                    drug_id: this.extraData[`drug_id`],
                    patient_id: this.extraData[`patient_id`],
                    pharmacy_id: this.extraData[`pharmacy_id`],
                    medido_id: 19
                }
                this.eventLegaciesService.sendDataForClient(data).subscribe(res1 => {
                    this.bodyClient = res1.results3.pharmacyNote + '\n' + '\n' +
                        'Hello,' + '\n ' + res1.results4.description + ' has facilitated a ' + this.option +
                        ' order for ' + res1.results0.drugName + '\n' + ' for Patient ' +
                        res1.results1.namePatient + '  ' + res1.results1.DOB + '.' + '\n' + 'The facilitating pharmacy is ' +
                        res1.results3.namePharmacy + '.' + '\n' + '\n' + res1.results3.namePharmacy + ' has arranged and delivered the order for ' +
                        res1.results0.drugName + ';' + ' the tracking information is' + '\n' + '\n' +
                        'Tracking number : ' + this.extraData[`trackingNumber`] + '\n' + '\n' +
                        res1.results3.namePharmacy + ' was provided with the following  shipping requirements.' + '\n' + '\n' +
                        'Shipping Address :' + '\n'
                        + '    ' + this.extraData[`shippingAddress1`] + '\n' + '    ' + this.extraData[`shippingAddress2`] + '\n' + '\n' +
                        'Delivery Instructions :' + '\n' + this.extraData[`deliveryInstructions`] + '\n' + '\n' +
                        'Please respond to ' + 'fulfill@ascellahealth.com' + ' if you have any questions or concerns about this order.' + '\n'
                        + '\n' + '\n' +
                        'Thanks.'
                    this.collectedData[`data`].body = this.bodyClient;
                    this.extraData[`subjectEmail`] = 'Order for ' + res1.results0.drugName;
                    if (this.webform) {
                        this.webform.redraw();
                    }
                    this.cdr.detectChanges();
                });
            }
            if (this.event_id === 28 || this.event_id === 31) {
                if (this.event_id === 28) {
                    this.option = 'New'
                } else if (this.event_id === 31) {
                    this.option = 'Refill'
                }
                const data = {
                    drug_id: this.extraData[`drug_id`],
                    patient_id: this.extraData[`patient_id`],
                    pharmacy_id: this.extraData[`pharmacy_id`],
                    medido_id: 19
                }
                this.eventLegaciesService.sendDataForClient(data).subscribe(res1 => {
                    this.bodyClient = res1.results3.pharmacyNote + '\n' + '\n' + 'Hello,' + '\n' +
                        res1.results4.description + ' has facilitated a ' + this.option + ' order for ' + res1.results0.drugName + '\n' + ' for Patient ' +
                        res1.results1.namePatient + '  ' + res1.results1.DOB + '.' + '\n' + 'The facilitating pharmacy is ' +
                        res1.results3.namePharmacy + '.' + '\n' + '\n' +
                        res1.results3.namePharmacy + ' has been provided with the following  shipping requirements.' + '\n' + '\n' +
                        'Shipping Address :' + '\n' +
                        '    ' + this.extraData[`shippingAddress1`] + '\n' + '    ' + this.extraData[`shippingAddress2`] + '\n' + '\n' +
                        'Delivery Instructions :' + '\n' + this.extraData[`deliveryInstructions`] + '\n' + '\n' +
                        'Please respond to ' + 'fulfill@ascellahealth.com' + ' if you have any questions or concerns about this order.' +
                        '\n' + res1.results4.description + ' will send an additional notification once a tracking number is available.'
                        + '\n' + '\n' +
                        'Thanks.'
                    this.collectedData[`data`].body = this.bodyClient;
                    this.extraData[`subjectEmail`] = 'Order for ' + res1.results0.drugName;
                    if (this.webform) {
                        this.webform.redraw();
                    }
                    this.cdr.detectChanges();
                });
            }
            if (this.event_id === 27 || this.event_id === 30) {
                if (this.event_id === 27) {
                    this.option = 'New'
                } else if (this.event_id === 30) {
                    this.option = 'Refill'
                }
                const data = {
                    drug_id: this.extraData[`drug_id`],
                    patient_id: this.extraData[`patient_id`],
                    pharmacy_id: this.extraData[`pharmacy_id`],
                    medido_id: 19
                }
                this.eventLegaciesService.sendDataForClient(data).subscribe(res1 => {
                    this.bodyClient = res1.results3.pharmacyNote + '\n' + '\n' + 'Hello,' + '\n' +
                        'We receive a request for a ' + this.option + ' order for ' + res1.results0.drugName + '\n' + ' for Patient ' +
                        res1.results1.namePatient + '  ' + res1.results1.DOB + '\n' + '\n';
                    if (this.referralInfo !== 'null' && this.referralInfo !== '' && this.referralInfo !== 'undefined') {
                        this.bodyClient = this.bodyClient + 'Referral Info :' + this.referralInfo + '\n' + '\n';
                    }
                    this.bodyClient = this.bodyClient + 'Use the following billing information:' + '\n' + 'PBM :' +
                        res1.results4.description + '\n' + 'ID: ' + res1.results5.id + '\n' + 'Group No: ' + res1.results5.group_no + '\n' + 'BIN: ' + res1.results5.bin + '\n' + 'PCN:' + res1.results5.pcn + '\n' + '\n' +
                        'To ensure you receive a paid claim note the following Prior authorization information.' + '\n' +
                        'Prior Auth number is not required to be entered for this fill.' + '\n' + '\n' +
                        'Shipping Address :' + '\n' +
                        '    ' + this.extraData[`shippingAddress1`] + '\n' + '    ' + this.extraData[`shippingAddress2`] + '\n' + '\n' +
                        'Delivery Instructions :' + '\n' + this.extraData[`deliveryInstructions`] + '\n' + '\n' +
                        'Please respond to ' + 'fulfill@ascellahealth.com' + ' to either confirm dispensing or to relay any issue which could prevent delivery as requested.' + '\n' + 'Once available,Please provide us with tracking number of the delivery for our records.' + '\n' + '\n' +
                        'Thanks.'
                    this.collectedData[`data`].body = this.bodyClient;
                    this.extraData[`subjectEmail`] = 'Order for ' + res1.results0.drugName;
                    if (this.webform) {
                        this.webform.redraw();
                    }
                    this.cdr.detectChanges();
                });
            }
            this.inputForm = JSON.parse(res[`inputForm`]);
            this.inputData = JSON.parse('{ "data":' + res[`inputData`] + '}');
            this.emailType = this.inputData[`data`].type;
            if (this.event_id === 27 || this.event_id === 30) {
                this.referralInfo = res[`referralInfo`];
                if (res[`pharmacyEmail`].length > 0) {
                    this.collectedData[`data`].multipleEmail = [];
                    for (const email of res[`pharmacyEmail`]) {
                        this.collectedData[`data`].multipleEmail.push({ email });
                    }
                }
                this.collectedData[`data`].daysSupply = res[`daysSupply`];
            } if (this.event_id === 28 || this.event_id === 31 || this.event_id === 33 || this.event_id === 34) {
                if (res[`clientEmail`].length > 0) {
                    this.collectedData[`data`].multipleEmail = [];
                    for (const email of res[`clientEmail`]) {
                        this.collectedData[`data`].multipleEmail.push({ email });
                    }
                }
                this.collectedData[`data`].daysSupply = res[`daysSupply`];
            }
            this.createOrderEntryForm();
            this.eventLegaciesService.getPatients().subscribe((patients) => {
                this.patients = patients.map(patient => new Patient(patient.id, patient.name,
                    patient.dob ? patient.dob.length >= 10 ? patient.dob.substring(0, 10) : patient.dob : patient.dob,
                    patient.sex === 'F' ? 'Female' : patient.sex === 'M' ? 'Male' : patient.sex, patient.address));
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.getClients().subscribe((clients) => {
                this.clients = clients.map(client => new Client(client.id, client.name, client.phone, client.address));
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.getContacts().subscribe((contacts) => {
                this.contacts = contacts.map(contact => new Contact(contact.id, contact.name, contact.phone, contact.address));
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.getPharmacies().subscribe((pharmacies) => {
                this.pharmacies = pharmacies.map(pharmacy => new Pharmacy(
                    pharmacy.value,
                    pharmacy.name,
                    pharmacy.Office_Phone,
                    pharmacy.Address1
                ));
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.getPharmaciesLocation(this.extraData[`pharmacy_id`]).subscribe((pharmacies_location) => {
                this.pharmacies_location = pharmacies_location.map(pharmacy_location =>
                    new PharmacyLocation(pharmacy_location.pharmacy_location_id, pharmacy_location.name,
                        pharmacy_location.npi, pharmacy_location.address));
                this.cdr.detectChanges();
            });
            if (this.extraData[`attachments`].length > 0) {
                this.eventLegaciesService.loadAttachments(this.extraData[`attachments`][0], this.emailType).subscribe((res2) => {
                    if (res2[`type`] === 'jpg' || res2[`type`] === 'PNG' || res2[`type`] === 'jpeg' || res2[`type`] === 'png') {
                        this.type = res2[`type`];
                        const u8 = this.toArrayBuffer(res2[`body`].data);
                        this.faxData = btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data)));
                        this.filename = res2[`name`];
                        this.contentType = res2[`type`];
                        this.srcImg = this.sanitize('data:image/' + this.type + ';base64, ' + btoa(this.Uint8ToString(u8)));
                        this.imgDisplay = true;
                    } else
                        if (res2[`type`] === 'pdf' || res2[`type`] === 'PDF') {
                            this.filename = res2[`name`];
                            this.pdf = this.sanitize('data:application/pdf;base64,'
                            + btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data))));
                            this.pdfDisplay = true;
                            this.contentType = res2[`type`];
                            this.faxData = res2[`body`];
                            this.pdfDisplay = true;
                        }
                    this.cdr.detectChanges();
                });
            }
            this.cdr.detectChanges();
        });
    }

    createOrderEntryForm() {
        Formio.createForm(document.getElementById('orderEntryForm'), this.collectedForm, {
            readOnly: this.eventStatus === 'confirmed'
        }).then((form) => {
            form.submission = this.collectedData;
            form.url = '';
            form.on('searchPatient', () => {
                this.eventLegaciesService.getPatients().subscribe((patients) => {
                    this.patients = patients.map(patient => new Patient(patient.id, patient.name,
                        patient.dob ? patient.dob.length >= 10 ? patient.dob.substring(0, 10) : patient.dob : patient.dob,
                        patient.sex === 'F' ? 'Female' : patient.sex === 'M' ? 'Male' : patient.sex, patient.address));
                    this.cdr.detectChanges();
                });
                this.searchEntities('patient');
                this.cdr.detectChanges();
            });
            form.on('searchClient', () => {
                this.searchEntities('client');
                this.cdr.detectChanges();
            });
            form.on('searchContact', () => {
                this.searchEntities('contact');
                this.cdr.detectChanges();
            });
            form.on('searchPharmacy', () => {
                this.searchEntities('pharmacy');
                this.cdr.detectChanges();
            });
            form.on('searchPharmacyLocation', () => {
                this.searchEntities('pharmacy_location');
                this.cdr.detectChanges();
            });
            form.on('medicationHistory', () => {
                this.eventLegaciesService.getMedications(this.extraData[`patient_id`])
                    .subscribe((medications) => {
                        this.medications = medications.map(medication => new Medication(medication.id, medication.doctor,
                            medication.doctorData, medication.drug, medication.drug_id, medication.prescribe_date,
                            medication.doctor_id, medication.refills, medication.sig, medication.qty, medication.ndc,
                            medication.diagnosis, medication.jcode, medication.drug_note, medication.pharmacy,
                            medication.pharmacyData, medication.cfi, medication.daysSupply));
                        this.searchEntities('medication');
                        this.cdr.detectChanges();
                    });
                this.cdr.detectChanges();
            });
            form.on('addMedication', () => {
                this.addMedication();
                this.cdr.detectChanges();
            });
            form.on('addPatient', () => {
                this.addPatient();
                this.cdr.detectChanges();
            });
            form.on('submit', () => {
                this.saveandnextOrder();
                this.cdr.detectChanges();
            });
            form.on('save', () => {
                this.saveOrder();
                this.cdr.detectChanges();
            });
            form.on('cancel', () => {
                this.close();
            });
            this.webform = form;
            this.cdr.detectChanges();
        });
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    // tslint:disable-next-line:no-shadowed-variable
    toArrayBuffer(buffer) {
        const ab = new ArrayBuffer(buffer.length);
        const view = new Uint8Array(buffer.length);
        for (let i = 0; i < buffer.length; ++i) {
            view[i] = buffer[i];
        }
        return view;
    }

    Uint8ToString(u8a) {
        const CHUNK_SZ = 0x8000;
        const c = [];
        for (let i = 0; i < u8a.length; i += CHUNK_SZ) {
            c.push(String.fromCharCode.apply(null, u8a.subarray(i, i + CHUNK_SZ)));
        }
        return c.join('');
    }

    searchEntities(type) {
        let dataList;
        switch (type) {
            case 'medication':
                dataList = this.medications;
                break;
            case 'patient':
                dataList = this.patients;
                break;
            case 'contact':
                dataList = this.contacts;
                break;
            case 'client':
                dataList = this.clients;
                break;
            case 'pharmacy':
                dataList = this.pharmacies;
                break;
            case 'pharmacy_location':
                dataList = this.pharmacies_location;
                break;
        }
        this.dialog.open(EntitySearchDialogComponent, { data: { dataList, type }, width: '30vw' }).afterClosed().subscribe((res) => {
            if (res) {
                switch (type) {
                    case 'medication':
                        this.handleMedication(res);
                        break;
                    case 'patient':
                        this.handlePatient(res);
                        break;
                    case 'contact':
                        this.handleContact(res);
                        break;
                    case 'client':
                        this.handleClient(res);
                        break;
                    case 'pharmacy':
                        this.handlePharmacy(res);
                        break;
                    case 'pharmacy_location':
                        this.handlePharmacyLocation(res);
                        break;
                }
            }
        })
    }

    handleMedication(input) {
        if (input.drug !== undefined) {
            this.collectedData[`data`].medication = input.drug;
            this.collectedData[`data`].qty = input.qty;
            this.collectedData[`data`].refills = input.refills;
            this.collectedData[`data`].ndc = input.ndc;
            this.collectedData[`data`].gcn = input.cfi;
            this.collectedData[`data`].jcode = input.jcode;
            this.collectedData[`data`].diagnosis = input.diagnosis;
            this.collectedData[`data`].drugNote = input.note;
            this.collectedData[`data`].sig = input.sig;
            this.collectedData[`data`].daysSupply = input.daysSupply;
            this.collectedData[`data`].prescriber = input.doctor + ' ' +
                input.doctorData;
            this.extraData[`drug_id`] = input.drug_id;
            this.extraData[`doctor_id`] = input.doctor_id;
            this.extraData[`prescription_id`] = input.id;
            this.collectedData[`data`].lastPharmacy = input.pharmacy + ' ' +
                input.pharmacyData;
            this.extraData[`pharmacy_id`] = input.pharmacy_id;
            this.extraData[`days_supply`] = input.daysSupply;
            this.webform.redraw();
        }
    }

    handlePatient(input) {
        if (input.name !== undefined) {
            this.collectedData[`data`].patient = input.name + `  `
                + input.dob + `  ` +
                (input.sex === null ? '' : input.sex)
                + `  ` + input.address;
            this.extraData[`patient_id`] = input.id;
            this.webform.redraw();
        }
    }

    handleContact(input) {
        if (input.name !== undefined) {
            this.collectedData[`data`].contactPerson = input.name + `  `
                + input.phone + `  ` +
                input.address;
            this.extraData[`contact_id`] = input.id;
            this.collectedData[`data`].contactPerson = input.name;
            this.webform.redraw();
        }
    }

    handleClient(input) {
        if (input.name !== undefined) {
            this.collectedData[`data`].client = input.name;
            this.extraData[`client_id`] = input.id;
            this.webform.redraw();
        }
    }

    handlePharmacy(input) {
        if (input.name !== undefined) {
            this.collectedData[`data`].pharmacy = input.name + `  `
                + input.phone + `  ` +
                input.address;
            this.walaa = input.id;
            this.extraData[`pharmacy_id`] = input.id;
            this.webform.redraw();
        }

    }
    handlePharmacyLocation(input) {
        if (input.name !== undefined) {
            this.collectedData[`data`].pharmacy_location = input.name + `  `
                + input.npi + `  ` +
                input.address;
            this.extraData[`pharmacy_location_id`] = input.pharmacy_location_id;
            this.extraData[`pharmacy_location_name`] = input.name;
            this.webform.redraw();
            this.cdr.detectChanges();
        }
    }

    addMedication() {
        this.dialog.open(
            AddMedicationDialogComponent,
            {
                data: {
                    emailType: this.emailType,
                    collectedData: this.collectedData,
                    extraData: this.extraData
                },
                width: '90vw'
            }
        ).afterClosed().subscribe((res) => {
            if (res && res.collectedData) {
                this.collectedData = res.collectedData;
                this.extraData = res.extraData;
                this.webform.redraw();
                this.cdr.detectChanges();
            }
        });
    }

    addPatient() {
        this.dialog.open(
            AddPatientDialogComponent,
            {
                data: {
                    emailType: this.emailType,
                    collectedData: this.collectedData,
                    extraData: this.extraData
                },
                width: '90vw'
            }
        ).afterClosed().subscribe((res) => {
            if (res && res.collectedData) {
                this.collectedData = res.collectedData;
                this.extraData = res.extraData;
                this.webform.redraw();
                this.cdr.detectChanges();
            }
        });
    }


    saveandnextOrder() {
        this.i = 0;
        this.k = 0;
        const submissionModel = {};
        submissionModel[`collected_Data`] = JSON.stringify(this.collectedData[`data`]);
        submissionModel[`transaction_id`] = this.transaction_id;
        submissionModel[`drug_id`] = this.extraData[`drug_id`];
        submissionModel[`doctor_id`] = this.extraData[`doctor_id`];
        submissionModel[`patient_id`] = this.extraData[`patient_id`];
        submissionModel[`input_data`] = JSON.stringify(this.inputData[`data`]);
        submissionModel[`event_id`] = this.event_id;
        submissionModel[`medido_id`] = 19;
        submissionModel[`user_id`] = this.currentUser;
        if (this.event_id === 25 || this.event_id === 26) {
            this.extraData[`shippingAddress1`] = this.collectedData[`data`].shipping;
            this.extraData[`shippingAddress2`] = this.collectedData[`data`].address + ', '
                + this.collectedData[`data`].state + ' ' + this.collectedData[`data`].zip;
            this.extraData[`deliveryInstructions`] = this.collectedData[`data`].deliveryInstructions;
            this.extraData[`city`] = this.collectedData[`data`].address;
            this.extraData[`state`] = this.collectedData[`data`].state;
            this.extraData[`zip`] = this.collectedData[`data`].zip;
            this.extraData[`drug_qty`] = this.collectedData[`data`].qty;
            this.extraData[`pa_number`] = this.collectedData[`data`].pa;
            this.extraData[`pa_until_date`] = this.collectedData[`data`].paTo;
            this.extraData[`Note`] = this.collectedData[`data`].otherNote;
            this.extraData[`pharmacy_id`] = this.walaa;
            this.extraData[`tran_id`] = this.tran_id;
            if (this.event_id === 25) {
                this.extraData[`option`] = 'New'

            }
            else if (this.event_id === 26) {
                this.extraData[`option`] = 'Refill'
            }
        }
        if (this.event_id === 29 || this.event_id === 32) {
            this.extraData[`trackingNumber`] = this.collectedData[`data`].tracking;
            this.extraData[`pharmacy_fill_date`] = this.collectedData[`data`].fillDate;
            this.extraData[`pharmacy_ship_date`] = this.collectedData[`data`].shipDate;
            this.extraData[`prescription_delivered_date`] = this.collectedData[`data`].deliveredDate;
            this.extraData[`courier_id`] = this.collectedData[`data`].courier;
            this.extraData[`pharmacy_order_status`] = this.collectedData[`data`].status;
            this.extraData[`pharmacy_order_statud_date`] = new Date(this.date_time2);
        }
        submissionModel[`extra_Data`] = JSON.stringify(this.extraData);
        this.eventLegaciesService.saveAndNext(submissionModel).subscribe(res => {
            this.layoutUtilsService.showActionNotification(
                'Successfully Updated!',
                MessageType.Read,
                3000,
                false,
                false
            );
            if (res.transaction_id) {
                this.transaction_id = res.transaction_id;
                this.event_id = res.event_id;
                this.getEventData();
            }
            this.cdr.detectChanges();
            this.dialogRef.close(true);
        });
    }

    base64ToArrayBuffer(base64) {
        const binary_string = window.atob(base64);
        const len = binary_string.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    sendFax22() {
        let contentType = '';
        let faxSubject = this.extraData[`subjectEmail`]
        if (faxSubject.length >= 50) {
            faxSubject = 'Order for Drug';
        }
        if (this.contentType === 'pdf' || this.contentType === 'PDF') {
            contentType = 'application/pdf';
        } else if (this.contentType === 'jpg' || this.contentType === 'jpeg'
            || this.contentType === 'JPG' || this.contentType === 'JPEG') {
            contentType = 'image/jpeg';
        } else if (this.contentType === 'png' || this.contentType === 'PNG') {
            contentType = 'image/png';
        }
        let fax;
        const body = (JSON.stringify(this.collectedData[`data`].body)).replace(new RegExp('\\\\n', 'g'), '<br />');
        const filename = this.filename;
        fax = this.extraData[`pharmacyFax`];
        if (this.event_id[`event_id`] === 28 || this.event_id[`event_id`] === 31 || this.event_id[`event_id`] === 33 || this.event_id[`event_id`] === 34) {
            fax = this.extraData[`clientFax`];
        }
        const headers = {
            Authorization: 'Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2ZTQyYmQ4MS00MTRmLTQwMjktYWYxZi03NWFkYzgyYjYyNTkiLCJpYXQiOjE1ODczNjgxMjd9.PhVIT8DmHnqUrMQoMaKej6WjZjDHB281WkW3Phbpv7s',
            'content-type': 'multipart/form-data;  boundary=---WebKitFormBoundary'
        }
        const c = new Blob(this.faxData)
        const formData: FormData = new FormData();
        formData.append('faxNumber', '+1 833-545-4160');
        formData.append('coverPage', 'true');
        formData.append('recipientName', 'Omar');
        formData.append('subject', faxSubject);
        formData.append('notes', body);
        formData.append('attachments', ({
            value: buffer(this.faxData),
            options: {
                filename,
                contentType
            }
        }).value.toString());
        this.eventLegaciesService.postData(formData).subscribe(() => { });
    }

    sendFax() {
        const submissionModel = {};
        const submissionModelFax = {};
        let checkPharmacyFax = true;
        let checkClientFax = true;
        submissionModelFax[`data`] = this.faxData;
        submissionModel[`collected_Data`] = JSON.stringify(this.collectedData[`data`]).replace(new RegExp('\\\\n', 'g'), ' ');
        submissionModelFax[`filename`] = this.filename;
        submissionModelFax[`contentType`] = this.contentType;
        submissionModelFax[`body`] = JSON.stringify(this.collectedData[`data`].body);
        submissionModelFax[`subject`] = this.extraData[`subjectEmail`];
        submissionModel[`transaction_id`] = this.transaction_id;
        submissionModel[`drug_id`] = this.extraData[`drug_id`];
        submissionModel[`doctor_id`] = this.extraData[`doctor_id`];
        submissionModel[`patient_id`] = this.extraData[`patient_id`];
        submissionModel[`input_data`] = JSON.stringify(this.inputData[`data`]);
        submissionModel[`event_id`] = this.event_id;
        submissionModel[`user_id`] = this.currentUser;
        submissionModel[`medido_id`] = 19;
        submissionModelFax[`medido_id`] = 19;
        if (submissionModel[`event_id`] === 27 || submissionModel[`event_id`] === 30) {
            this.pharmacyEmail = this.collectedData[`data`].multipleEmail;
            submissionModel[`emails`] = this.pharmacyEmail;
            submissionModelFax[`Fax`] = this.extraData[`pharmacyFax`];
            if (this.extraData[`pharmacyFax`][0].length > 2) {
                checkPharmacyFax = true;
            } else {
                checkPharmacyFax = false;
            }
            this.extraData[`sent_to_pharmacy_date`] = new Date();
            this.extraData[`sent_to_pharmacy_method`] = 'email';
        }
        if (
            submissionModel[`event_id`] === 28
            || submissionModel[`event_id`] === 31
            || submissionModel[`event_id`] === 33
            || submissionModel[`event_id`] === 34
        ) {
            this.clientEmail = this.collectedData[`data`].multipleEmail;
            if (this.extraData[`clientFax`][0].length > 2) {
                checkClientFax = true;
            } else {
                checkClientFax = false;
            }
            submissionModelFax[`Fax`] = this.extraData[`clientFax`];
            submissionModel[`emails`] = this.clientEmail;
            this.extraData[`notify_client_date`] = new Date();
        }
        submissionModel[`extra_Data`] = JSON.stringify(this.extraData);
        if (checkPharmacyFax === false) {
            this.layoutUtilsService.showActionNotification(
                'Please add a pharmacy fax to be able to  send fax to this pharmacy',
                MessageType.Read,
                3000,
                false,
                false
            );
        }
        else if (checkClientFax === false) {
            this.layoutUtilsService.showActionNotification(
                'Please add a client fax to be able to send fax to this client',
                MessageType.Read,
                3000,
                false,
                false
            );
        } else {
            this.eventLegaciesService.saveAndNext(submissionModel).subscribe(res => {
                this.layoutUtilsService.showActionNotification(
                    'Fax Queued to be sent',
                    MessageType.Read,
                    3000,
                    false,
                    false
                );
                if (res.transaction_id) {
                    this.transaction_id = res.transaction_id;
                    this.event_id = res.event_id;
                    this.getEventData();
                } else {
                    this.close();
                }
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.sendFax(submissionModelFax).subscribe(res => {
                this.cdr.detectChanges();
                this.dialogRef.close(true);
            });

        }

    }
    sendEmail() {
        let checkPharmacyEmail = true;
        let checkClientEmail = true;
        const submissionModel = {};
        submissionModel[`collected_Data`] = JSON.stringify(this.collectedData[`data`]).replace(new RegExp('\\\\n', 'g'), ' ');
        submissionModel[`transaction_id`] = this.transaction_id;
        submissionModel[`drug_id`] = this.extraData[`drug_id`];
        submissionModel[`doctor_id`] = this.extraData[`doctor_id`];
        submissionModel[`patient_id`] = this.extraData[`patient_id`];
        submissionModel[`input_data`] = JSON.stringify(this.inputData[`data`]);
        submissionModel[`event_id`] = this.event_id;
        submissionModel[`user_id`] = this.currentUser;
        submissionModel[`medido_id`] = 19;
        submissionModel[`token`] = 'Gw0ssWhvIrUjMFKvkUS76eZ4JYLOoG';
        submissionModel[`body`] = JSON.stringify(this.collectedData[`data`].body);
        submissionModel[`subject`] = this.extraData[`subjectEmail`];
        submissionModel[`attachments`] = this.extraData[`attachments`];
        if (submissionModel[`event_id`] === 27 || submissionModel[`event_id`] === 30) {
            this.pharmacyEmail = this.collectedData[`data`].multipleEmail;
            submissionModel[`emails`] = this.pharmacyEmail;
            if (this.pharmacyEmail[0].email.length > 0) {
                checkPharmacyEmail = true;
            } else {
                checkPharmacyEmail = false;
            }
            this.extraData[`sent_to_pharmacy_date`] = new Date();
            this.extraData[`sent_to_pharmacy_method`] = 'email';
        }
        if (
            submissionModel[`event_id`] === 28
            || submissionModel[`event_id`] === 31
            || submissionModel[`event_id`] === 33
            || submissionModel[`event_id`] === 34
        ) {
            this.clientEmail = this.collectedData[`data`].multipleEmail;
            submissionModel[`emails`] = this.clientEmail;
            if (this.clientEmail[0].email.length > 0) {
                checkClientEmail = true;
            } else {
                checkClientEmail = false;
            }
            this.extraData[`notify_client_date`] = new Date();
        }
        submissionModel[`extra_Data`] = JSON.stringify(this.extraData);
        if (checkPharmacyEmail === false) {
            this.layoutUtilsService.showActionNotification(
                'Please add a pharmacy email to be able to send email to this pharmacy',
                MessageType.Read,
                3000,
                false,
                false
            );
        }
        else if (checkClientEmail === false) {
            this.layoutUtilsService.showActionNotification(
                'Please add a client email to be able to send email to this client',
                MessageType.Read,
                3000,
                false,
                false
            );
        } else {
            this.eventLegaciesService.saveAndNext(submissionModel).subscribe(res => {
                this.layoutUtilsService.showActionNotification(
                    'Email Queued to be sent',
                    MessageType.Read,
                    3000,
                    false,
                    false
                );
                if (res.transaction_id) {
                    this.transaction_id = res.transaction_id;
                    this.event_id = res.event_id;
                    this.getEventData();
                } else {
                    this.close();
                }
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.sendEmail(submissionModel).subscribe(() => {
                this.dialogRef.close(true);
            });
        }
    }

    saveOrder() {
        const submissionModel = {};
        // If the regexp has flag g, then it returns an array of all matches as strings, without capturing groups and other details.
        submissionModel[`collected_Data`] = JSON.stringify(this.collectedData[`data`]).replace(new RegExp('\\\\n', 'g'), '*^*');
        submissionModel[`transaction_id`] = this.transaction_id;
        submissionModel[`drug_id`] = this.extraData[`drug_id`];
        submissionModel[`event_id`] = this.event_id;
        submissionModel[`doctor_id`] = this.extraData[`doctor_id`];
        submissionModel[`patient_id`] = this.extraData[`patient_id`];
        submissionModel[`user_id`] = this.currentUser;
        submissionModel[`medido_id`] = 19;
        if (this.event_id === 25 || this.event_id === 26) {
            this.extraData[`shippingAddress1`] = this.collectedData[`data`].shipping;
            this.extraData[`shippingAddress2`] = this.collectedData[`data`].address + ', '
                + this.collectedData[`data`].state + ' ' + this.collectedData[`data`].zip;
            this.extraData[`deliveryInstructions`] = this.collectedData[`data`].deliveryInstructions;
            this.extraData[`city`] = this.collectedData[`data`].address;
            this.extraData[`state`] = this.collectedData[`data`].state;
            this.extraData[`zip`] = this.collectedData[`data`].zip;
            this.extraData[`drug_qty`] = this.collectedData[`data`].qty;
            this.extraData[`pa_number`] = this.collectedData[`data`].pa;
            this.extraData[`pa_until_date`] = this.collectedData[`data`].paUntil;
            this.extraData[`Note`] = this.collectedData[`data`].otherNote;
            this.extraData[`pharmacy_id`] = this.walaa;
        }
        if (this.event_id === 29 || this.event_id === 32) {
            this.extraData[`trackingNumber`] = this.collectedData[`data`].tracking;
            this.extraData[`pharmacy_fill_date`] = this.collectedData[`data`].fillDate;
            this.extraData[`pharmacy_ship_date`] = this.collectedData[`data`].shipDate;
            this.extraData[`prescription_delivered_date`] = this.collectedData[`data`].deliveredDate;
            this.extraData[`courier_id`] = this.collectedData[`data`].courier;
            this.extraData[`pharmacy_order_status`] = this.collectedData[`data`].status;
            this.extraData[`pharmacy_order_statud_date`] = new Date(this.date_time2);
        }
        if (submissionModel[`event_id`] === 27 || submissionModel[`event_id`] === 30) {
            submissionModel[`emails`] = this.pharmacyEmail;
            this.extraData[`sent_to_pharmacy_date`] = new Date();
            this.extraData[`sent_to_pharmacy_method`] = 'Email';
        }
        if (
            submissionModel[`event_id`] === 28
            || submissionModel[`event_id`] === 31
            || submissionModel[`event_id`] === 33
            || submissionModel[`event_id`] === 34
        ) {
            submissionModel[`emails`] = this.clientEmail;
            this.extraData[`notify_client_date`] = new Date();
        }
        submissionModel[`extra_Data`] = JSON.stringify(this.extraData);
        this.eventLegaciesService.updateOrder(submissionModel).subscribe(() => {
            this.layoutUtilsService.showActionNotification(
                'Successfully Updated!',
                MessageType.Read,
                3000,
                false,
                false
            );
            this.cdr.detectChanges();
            this.dialogRef.close(true);
        });
    }

    savePharmacyUpdateEvent() {
        if (this.event_id === 29 || this.event_id === 32)
            if (this.collectedData[`data`].deliveredDate.length > 0) {
                this.saveandnextOrder();
            } else {
                this.saveOrder();
            }
    }

    Previous() {
        if (this.i > 0) {
            this.i = --this.i;
            this.id = this.extraData[`attachments`][this.i];
            this.downloads();
        }
    }

    Next() {
        if (this.i < this.extraData[`attachments`].length - 1) {
            this.i = ++this.i;
            this.id = this.extraData[`attachments`][this.i];
            this.downloads();
        }
    }

    downloads() {
        this.imgDisplay = false;
        this.pdfDisplay = false;
        this.eventLegaciesService.loadAttachments(this.id, this.emailType).subscribe((res2) => {
            if (res2[`type`] === 'jpg' || res2[`type`] === 'PNG' || res2[`type`] === 'jpeg' || res2[`type`] === 'png') {
                this.type = res2[`type`];
                const u8 = this.toArrayBuffer(res2[`body`].data);
                this.srcImg = this.sanitize('data:image/' + this.type + ';base64, ' + btoa(this.Uint8ToString(u8)));
                this.imgDisplay = true;
            } else if (res2[`type`] === 'pdf' || res2[`type`] === 'PDF') {
              this.pdf = this.sanitize('data:application/pdf;base64,' + btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data))));
              this.pdfDisplay = true;
            }
            this.cdr.detectChanges();
        });
        this.cdr.detectChanges();
    }

    onSubmit() {
    }

    close() {
        this.dialogRef.close();
    }

}
