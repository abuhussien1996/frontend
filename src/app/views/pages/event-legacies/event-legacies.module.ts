import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FormioModule} from 'angular-formio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {EventLegaciesService} from './event-legacies.service';
import {InterceptService} from 'src/app/core/_base/crud';
import {EventLegaciesComponent} from './event-legacies.component';
import {MatSelectModule} from '@angular/material/select';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTabsModule} from '@angular/material/tabs';
import {AddDoctorDialogComponent} from './add-doctor.dialog.component';
import {AddDrugDialogComponent} from './add-drug.dialog.component';
import {AddMedicationDialogComponent} from './add-medication.dialog.component';
import {AddPatientDialogComponent} from './add-patient.dialog.component';
import {EntitySearchDialogComponent} from './entity-search.dialog.component';
import {EventDialogComponent} from './event.dialog.component';
import { EventLegaciesRoutingModule } from './event-legacies-routing.module';
import {MatSortModule} from '@angular/material/sort';
import { PartialsModule } from '../../partials/partials.module';
import {InlineSVGModule} from "ng-inline-svg";

@NgModule({
  entryComponents: [
    AddDoctorDialogComponent,
    AddDrugDialogComponent,
    AddMedicationDialogComponent,
    AddPatientDialogComponent,
    EntitySearchDialogComponent,
    EventDialogComponent
  ],
  declarations: [
    EventLegaciesComponent,
    AddDoctorDialogComponent,
    AddDrugDialogComponent,
    AddMedicationDialogComponent,
    AddPatientDialogComponent,
    EntitySearchDialogComponent,
    EventDialogComponent
  ],
    imports: [
        MatInputModule,
        MatButtonModule,
        CommonModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        FormsModule,
        HttpClientModule,
        FormioModule,
        NgxExtendedPdfViewerModule,
        NgbDropdownModule,
        DragDropModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        NgbModule,
        MatCheckboxModule,
        MatTabsModule,
        EventLegaciesRoutingModule,
        MatSortModule,
        PartialsModule,
        InlineSVGModule
    ],
  providers: [
    EventLegaciesService,
    InterceptService,
      {
        provide: HTTP_INTERCEPTORS,
            useClass: InterceptService,
        multi: true
      }
  ],
  exports: []

})
export class EventLegaciesModule {
}
