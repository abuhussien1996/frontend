// Angular
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { Formio } from 'angular-formio';
import { EventLegaciesService } from './event-legacies.service';
import { Doctor, Drug } from './models';
import Webform from 'formiojs/Webform';
import { EntitySearchDialogComponent } from './entity-search.dialog.component';
import { AddDoctorDialogComponent } from './add-doctor.dialog.component';
import { LayoutUtilsService, MessageType } from 'src/app/core/_base/crud';
import { AddDrugDialogComponent } from './add-drug.dialog.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-legacies-add-medication',
    templateUrl: './add-medication.dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddMedicationDialogComponent implements OnInit {
    tabIndex: number;
    k: number;
    pdfSizeMega: boolean;
    pdfSizelessMega: boolean;
    pdfMedication: any;
    pdfDisplay: boolean;
    pdfDisplayMedication: boolean;
    imgDisplay: boolean;
    id: any;
    medicationForm: JSON;
    medicationFormData: JSON;
    type: string;
    srcImg: any;
    drugs: Drug[] = [];
    doctors: Doctor[] = [];
    medicationWebform: Webform;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<AddMedicationDialogComponent>,
        public dialog: MatDialog,
        private eventLegaciesService: EventLegaciesService,
        private cdr: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private layoutUtilsService: LayoutUtilsService
    ) { }

    ngOnInit() {
        this.k = 0;
        this.eventLegaciesService.getPrescriptionForm().subscribe(res => {
            this.medicationForm = JSON.parse(res.form);
            this.medicationFormData = JSON.parse(`{"data":` + res.form_data + `}`);
            if (this.data.extraData[`attachments`].length > 0) {
                this.pdfDisplayMedication = true;
                this.eventLegaciesService.loadAttachments(this.data.extraData[`attachments`][0], this.data.emailType).subscribe((res2) => {
                    if (res2[`type`] === 'jpg' || res2[`type`] === 'PNG' || res2[`type`] === 'jpeg' || res2[`type`] === 'png') {
                        this.type = res2[`type`];
                        const u8 = this.toArrayBuffer(res2[`body`].data);
                        this.srcImg = this.sanitize('data:image/' + this.type + ';base64, ' + btoa(this.Uint8ToString(u8)));
                        this.imgDisplay = true;
                    } else if (res2[`type`] === 'pdf' || res2[`type`] === 'PDF') {
                        this.pdfMedication = this.sanitize('data:application/pdf;base64,' + btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data))));
                    }
                    this.cdr.detectChanges();
                });
            }
            this.createMedicationForm();
            this.eventLegaciesService.getDrugs().subscribe((drugs) => {
                this.drugs = drugs.map(drug => new Drug(drug.id, drug.name, drug.drug_group, drug.ndc,
                drug.jcode, drug.cfi, drug.note, drug.copay_assistance, drug.copay_assistance_url)
                );
                this.cdr.detectChanges();
            });
            this.eventLegaciesService.getDoctors().subscribe((doctors) => {
                this.doctors = doctors.map(doctor => new Doctor(doctor.id, doctor.name, doctor.phone));
                this.cdr.detectChanges();
            });
            this.cdr.detectChanges();
        });
    }

    createMedicationForm() {
        Formio.createForm(document.getElementById('prescriptionForm'), this.medicationForm, {}).then((form) => {
            form.submission = this.medicationFormData;
            form.url = '';
            this.medicationFormData[`data`].patient = this.data.collectedData[`data`].patient;
            this.medicationFormData[`data`].patient_id = this.data.extraData[`patient_id`];
            form.on('searchDoctor', () => {
                this.searchEntities('doctor');
                this.cdr.detectChanges();
            });
            form.on('searchDrug', () => {
                this.searchEntities('drug');
                this.cdr.detectChanges();
            });
            form.on('addDoctor', () => {
                this.addDoctor();
                this.cdr.detectChanges();
            });
            form.on('addDrug', () => {
                this.addDrug();
                this.cdr.detectChanges();
            });
            form.on('submit', () => {
                this.saveMedication();
                this.cdr.detectChanges();
            });
            form.on('cancel', () => {
                this.pdfDisplayMedication = false;
                this.pdfDisplay = true;
                this.cdr.detectChanges();
            });
            this.medicationWebform = form;
            this.cdr.detectChanges();
        });
    }

    Previous() {
        if (this.k > 0) {
            this.k = --this.k;
            this.id = this.data.extraData[`attachments`][this.k];
            this.downloads();
        }
    }

    Next() {
        if (this.k < this.data.extraData[`attachments`].length - 1) {
            this.k = ++this.k;
            this.id = this.data.extraData[`attachments`][this.k];
            this.downloads();
        }
    }


    downloads() {
        this.imgDisplay = false;
        this.pdfDisplay = false;
        this.pdfSizeMega = false;
        this.pdfSizelessMega = false;
        this.eventLegaciesService.loadAttachments(this.id, this.data.emailType).subscribe((res2) => {
            if (res2[`type`] === 'pdf' || res2[`type`] === 'PDF') {
                this.pdfMedication = this.sanitize('data:application/pdf;base64,'
                + btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data))));
            }
            this.cdr.detectChanges();
        });
        this.cdr.detectChanges();
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    // tslint:disable-next-line:no-shadowed-variable
    toArrayBuffer(buffer) {
        const ab = new ArrayBuffer(buffer.length);
        const view = new Uint8Array(buffer.length);
        for (let i = 0; i < buffer.length; ++i) {
            view[i] = buffer[i];
        }
        return view;
    }

    Uint8ToString(u8a) {
        const CHUNK_SZ = 0x8000;
        const c = [];
        for (let i = 0; i < u8a.length; i += CHUNK_SZ) {
            c.push(String.fromCharCode.apply(null, u8a.subarray(i, i + CHUNK_SZ)));
        }
        return c.join('');
    }

    searchEntities(type) {
        let dataList;
        switch(type) {
            case 'drug':
                dataList = this.drugs;
                break;
            case 'doctor':
                dataList = this.doctors;
                break;
        }
        this.dialog.open(EntitySearchDialogComponent, {data : {dataList, type}, width: '30vw'}).afterClosed().subscribe((res) => {
            if(res) {
                switch(type) {
                    case 'doctor':
                        this.handleDoctor(res);
                        break;
                    case 'drug':
                        this.handleDrug(res);
                        break;
                    }
            }
        })
    }

    addDoctor() {
        this.dialog.open(AddDoctorDialogComponent).afterClosed().subscribe((res) => {
            if(res && res.insertId) {
                this.medicationFormData[`data`].doctor = res.doctorFormData[`data`].firstName + `  `
                + res.doctorFormData[`data`].lastName + `  ` + res.doctorFormData[`data`].email
                + `  ` + res.doctorFormData[`data`].npi;
            this.medicationFormData[`data`].doctor_id = res.insertId;
            this.layoutUtilsService.showActionNotification(
                'Successfully Added!',
                MessageType.Read,
                3000,
                false,
                false
            );
            this.medicationWebform.redraw();
            this.cdr.detectChanges();
            }
        });
    }

    handleDoctor(input) {
        if (input.name !== undefined) {
            this.medicationFormData[`data`].doctor = input.name;
            this.medicationFormData[`data`].doctor_id = input.id;
            this.medicationWebform.redraw();
        }
    }

    addDrug() {
        this.dialog.open(AddDrugDialogComponent).afterClosed().subscribe((res) => {
            if (res && res.insertId) {
                this.medicationFormData[`data`].ndc = res.drugFormData1[`data`].ndc;
                this.medicationFormData[`data`].jcode = res.drugFormData1[`data`].jcode;
                this.medicationFormData[`data`].gcn = res.drugFormData1[`data`].cfi;
                this.medicationFormData[`data`].drugNote = res.drugFormData1[`data`].note;
                for (const drugGroup of res.drugFormData1[`data`].drugGroupData) {
                    if (drugGroup.value === res.drugFormData1[`data`].drug_group_id.value) {
                        this.medicationFormData[`data`].copayAssistance = drugGroup.copay_assistance;
                        this.medicationFormData[`data`].copayUrl = drugGroup.copay_assistance_url;
                        break;
                    }
                }
                this.medicationFormData[`data`].drug = res.drugFormData1[`data`].drug + `  `
                    + res.drugFormData1[`data`].strength_uom + ` ` + res.drugFormData1[`data`].strength_unit
                    + `  ` + res.drugFormData1[`data`].ndc;
                this.medicationFormData[`data`].drug_id = res.insertId;
                this.layoutUtilsService.showActionNotification(
                    'Successfully Added!',
                    MessageType.Read,
                    3000,
                    false,
                    false
                );
                this.medicationWebform.redraw();
                this.cdr.detectChanges();
            }
        });
    }

    handleDrug(input) {
        if (input.name !== undefined) {
            this.medicationFormData[`data`].drug = input.name;
            this.medicationFormData[`data`].drug_id = input.id;
            this.medicationFormData[`data`].ndc = input.ndc;
            this.medicationFormData[`data`].jcode = input.jcode;
            this.medicationFormData[`data`].drugNote = input.drugNote;
            this.medicationFormData[`data`].gcn = input.cfi;
            this.medicationFormData[`data`].copayAssistance = input.copay_assistance;
            this.medicationFormData[`data`].copayUrl = input.copay_assistance_url;
            this.medicationWebform.redraw();
        }
    }

    saveMedication() {
        this.data.collectedData[`data`].prescriber = this.medicationFormData[`data`].doctor;
        this.data.extraData[`doctor_id`] = this.medicationFormData[`data`].doctor_id;
        this.data.collectedData[`data`].medication = this.medicationFormData[`data`].drug;
        this.data.extraData[`drug_id`] = this.medicationFormData[`data`].drug_id;
        this.data.collectedData[`data`].ndc = this.medicationFormData[`data`].ndc;
        this.data.collectedData[`data`].jcode = this.medicationFormData[`data`].jcode;
        this.data.collectedData[`data`].drugNote = this.medicationFormData[`data`].note;
        this.data.collectedData[`data`].sig = this.medicationFormData[`data`].sig;
        this.data.collectedData[`data`].copayAssistance = this.medicationFormData[`data`].copayAssistance;
        this.data.collectedData[`data`].copayUrl = this.medicationFormData[`data`].copayUrl;
        this.data.collectedData[`data`].diagnosis = this.medicationFormData[`data`].diagnosis;
        this.data.collectedData[`data`].qty = this.medicationFormData[`data`].quantity;
        this.data.collectedData[`data`].refills = this.medicationFormData[`data`].refills;
        this.data.collectedData[`data`].gcn = this.medicationFormData[`data`].gcn;
        this.data.collectedData[`data`].daysSupply = this.medicationFormData[`data`].daysSupply;
        this.data.extraData[`days_supply`] = this.medicationFormData[`data`].daysSupply;
        this.medicationFormData[`data`].medido_id = 19;
        this.eventLegaciesService.insertPrescription(this.medicationFormData[`data`]).subscribe(res => {
            this.data.extraData[`prescription_id`] = res.id;
            this.layoutUtilsService.showActionNotification(
                'Successfully Added!',
                MessageType.Read,
                3000,
                false,
                false
            );
            this.cdr.detectChanges();
            this.dialogRef.close({collectedData: this.data.collectedData, extraData: this.data.extraData});
        });

    }

    close() {
        this.dialogRef.close();
    }

}
