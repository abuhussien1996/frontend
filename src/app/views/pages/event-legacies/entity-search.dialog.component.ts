// Angular
import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { of } from 'rxjs';
// RxJS
import { switchMap } from 'rxjs/operators';
import { Client, Contact, Doctor, Drug, Facility, Insurance, Medication, Patient, Pharmacy, PharmacyLocation } from './models';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-legacies-entity-search',
    templateUrl: './entity-search.dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntitySearchDialogComponent implements OnInit {
    entityInput: FormControl;
    filteredEntities: any[];
    test = 'testing';

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<EntitySearchDialogComponent>,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
        this.entityInput = new FormControl();
        this.entityInput
            .valueChanges
            .pipe(
              switchMap(
                // tslint:disable-next-line:max-line-length
                value => value.length >= 1 ? of(this.searchEntities({name: value})) : of([])
              )
            )
            .subscribe(result => {
              this.filteredEntities = result ? result : [];
            });
    }

    displayEntity =
        entity => {
            switch (this.data.type) {
                case 'medication':
                    return entity ? entity.drug : '';
                default:
                    return entity ? entity.name : '';
            }
        }


    searchEntities(filter): [] {
        switch (this.data.type) {
            case 'medication':
                return this.data.dataList
                    .map(medication => new Medication(medication.id, medication.doctor,
                        medication.doctorData, medication.drug, medication.drug_id, medication.prescribe_date,
                        medication.doctor_id, medication.refills, medication.sig, medication.qty, medication.ndc,
                        medication.diagnosis, medication.jcode, medication.drug_note, medication.pharmacy,
                        medication.pharmacyData, medication.cfi, medication.daysSupply))
                    .filter(medication => medication.drug.toLowerCase().includes(filter.name.toLowerCase()));
            case 'patient':
                return this.data.dataList
                    .map(patient => new Patient(patient.id, patient.name, patient.dob, patient.sex, patient.address))
                    .filter(patient => patient.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'contact':
                return this.data.dataList
                    .map(contact => new Contact(contact.id, contact.name, contact.phone, contact.address))
                    .filter(contact => contact.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'client':
                return this.data.dataList
                    .map(client => new Client(client.id, client.name, client.phone, client.address))
                    .filter(client => client.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'pharmacy':
                return this.data.dataList
                    .map(pharmacy => new Pharmacy(pharmacy.id, pharmacy.name, pharmacy.phone, pharmacy.address))
                    .filter(pharmacy => pharmacy.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'pharmacy_location':
                return this.data.dataList
                    .map(pharmacy_location => new PharmacyLocation(pharmacy_location.pharmacy_location_id,
                        pharmacy_location.name, pharmacy_location.npi, pharmacy_location.address))
                    .filter(pharmacy_location => pharmacy_location.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'drug':
                return this.data.dataList
                    .map(drug => new Drug(
                        drug.id,
                        drug.name,
                        drug.drug_group,
                        drug.ndc,
                        drug.jcode,
                        drug.cfi,
                        drug.note,
                        drug.copay_assistance,
                        drug.copay_assistance_url
                    )
                    )
                    .filter(drug => (
                        drug.name.toLowerCase().includes(filter.name.toLowerCase())
                        || drug.ndc.toLowerCase().includes(filter.name.toLowerCase())
                        || drug.cfi.toString().toLowerCase().includes(filter.name.toLowerCase())
                        || drug.drug_group.toLowerCase().includes(filter.name.toLowerCase())
                    ));
            case 'doctor':
                return this.data.dataList
                    .map(doctor => new Doctor(doctor.id, doctor.name, doctor.phone))
                    .filter(doctor => doctor.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'facility':
                return this.data.dataList
                    .map(facility => new Facility(facility.id, facility.name, facility.phone))
                    .filter(facility => facility.name.toLowerCase().includes(filter.name.toLowerCase()));
            case 'insurance':
                return this.data.dataList
                    .map(insurance => new Insurance(insurance.id, insurance.name, insurance.bin, insurance.pcn))
                    .filter(insurance => insurance.name.toLowerCase().includes(filter.name.toLowerCase()));
        }
    }

    close() {
        this.dialogRef.close(this.entityInput.value);
    }

}
