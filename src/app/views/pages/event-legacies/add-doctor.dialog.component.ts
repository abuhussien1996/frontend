// Angular
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { EventLegaciesService } from './event-legacies.service';
import { Formio } from 'angular-formio';
import Webform from 'formiojs/Webform';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-legacies-add-doctor',
    templateUrl: './add-doctor.dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddDoctorDialogComponent implements OnInit {
    doctorForm: JSON;
    doctorFormData: JSON;
    doctorWebform: Webform;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<AddDoctorDialogComponent>,
        public dialog: MatDialog,
        private eventLegaciesService: EventLegaciesService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.eventLegaciesService.getDoctorForm().subscribe(res => {
            this.doctorForm = JSON.parse(res.form);
            this.doctorFormData = JSON.parse(`{"data":` + res.form_data + `}`);
            this.createDoctorForm();
            this.cdr.detectChanges();
        });
    }

    createDoctorForm() {
        Formio.createForm(document.getElementById('doctorForm'), this.doctorForm, {}).then((form) => {
            form.submission = this.doctorFormData;
            form.url = '';
            this.doctorWebform = form;
            this.cdr.detectChanges();
        })
    }

    onSubmit() {
        this.doctorFormData[`data`].medido_id = 19;
        this.eventLegaciesService.insertDoctor(this.doctorFormData[`data`]).subscribe(res => {
            if(res.insertId > 0) {
                this.dialogRef.close({insertId: res.insertId, doctorFormData: this.doctorFormData});
            } else {
                this.close();
            }
        })
    }

    close() {
        this.dialogRef.close();
    }

}
