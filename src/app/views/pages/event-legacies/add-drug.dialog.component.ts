// Angular
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventLegaciesService } from './event-legacies.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-legacies-add-drug',
    templateUrl: './add-drug.dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddDrugDialogComponent implements OnInit {
    drugForm1: JSON;
    drugFormData1: JSON;
    drugForm2: JSON;
    drugFormData2: JSON;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<AddDrugDialogComponent>,
        public dialog: MatDialog,
        private eventLegaciesService: EventLegaciesService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.eventLegaciesService.getDrugForm().subscribe(res => {
            this.eventLegaciesService.getDrugGroups().subscribe(res1 => {
                this.eventLegaciesService.getPharmacies().subscribe(res2 => {
                    this.drugForm1 = JSON.parse(res.detailsForm.form);
                    this.drugFormData1 = JSON.parse(`{"data":` + res.detailsForm.form_data + `}`);
                    this.drugForm2 = JSON.parse(res.preferenceForm.form);
                    this.drugFormData2 = JSON.parse(`{"data":` + res.preferenceForm.form_data + `}`);
                    this.drugFormData1[`data`].drugGroupData = res1;
                    this.drugFormData2[`data`].pharmacyData = res2;
                    this.cdr.detectChanges();
                });
                this.cdr.detectChanges();
            });
            this.cdr.detectChanges();
        });
    }

    onSubmit() {
        const submissionModel = {};
        submissionModel[`medido_id`] = 19;
        submissionModel[`drug`] = this.drugFormData1[`data`].drug;
        submissionModel[`ndc`] = this.drugFormData1[`data`].ndc;// HERE name of key for each component from formio api
        submissionModel[`drug_group_id`] = this.drugFormData1[`data`].drug_group_id.value;
        submissionModel[`brand_generic_flag`] = this.drugFormData1[`data`].brand_generic_flag;
        submissionModel[`brand`] = this.drugFormData1[`data`].brand;
        submissionModel[`cfi`] = this.drugFormData1[`data`].cfi;
        submissionModel[`hicl_seqno`] = this.drugFormData1[`data`].hicl_seqno;
        submissionModel[`strength_unit`] = this.drugFormData1[`data`].strength_unit;
        submissionModel[`strength_uom`] = this.drugFormData1[`data`].strength_uom;
        submissionModel[`jcode`] = this.drugFormData1[`data`].jcode;
        submissionModel[`drug_schedule`] = this.drugFormData1[`data`].drug_schedule;
        submissionModel[`otc_flag`] = this.drugFormData1[`data`].otc_flag;// HERE name of key for each component from formio api
        submissionModel[`generic`] = this.drugFormData1[`data`].generic;
        submissionModel[`gpi`] = this.drugFormData1[`data`].gpi;
        submissionModel[`therapeutic`] = this.drugFormData1[`data`].therapeutic;
        submissionModel[`note`] = this.drugFormData1[`data`].note;
        submissionModel[`is_active`] = this.drugFormData1[`data`].is_active;
        submissionModel[`primaryPharmacy`] = this.drugFormData2[`data`].primaryPharmacy.value;
        submissionModel[`secondaryPharmacy`] = this.drugFormData2[`data`].secondaryPharmacy.value;
        submissionModel[`tertiaryPharmacy`] = this.drugFormData2[`data`].tertiaryPharmacy.value;
        if (this.drugFormData1[`data`].is_active === true) {
            submissionModel[`is_active`] = '1'
        } else {
            submissionModel[`is_active`] = '0'
        }
        if (this.drugFormData1[`data`].brand_generic_flag === true) {
            submissionModel[`brand_generic_flag`] = '1'
        } else {
            submissionModel[`brand_generic_flag`] = '0'
        }
        if (this.drugFormData1[`data`].otc_flag === true) {
            submissionModel[`otc_flag`] = 'Y'
        } else {
            submissionModel[`otc_flag`] = 'N'
        }
        submissionModel[`last_modified_user`] = `admin`;
        this.drugFormData1[`data`].medido_id = 19;
        this.eventLegaciesService.insertDrug(submissionModel).subscribe(res => {
            if(res.insertId > 0) {
                this.dialogRef.close({insertId: res.insertId, drugFormData1: this.drugFormData1});
            } else {
                this.close();
            }
        })
    }

    close() {
        this.dialogRef.close();
    }

}
