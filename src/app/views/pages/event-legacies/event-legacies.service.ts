// Angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../../environments/environment';

const BASE_URL = environment.LEGACIES;

@Injectable()
export class EventLegaciesService {

  constructor(private http: HttpClient) {}

  getEventData() {
    const url = `${BASE_URL}/myeventsgeteventdatapoes`;
    return this.http.get<any>(url);
  }

  saveAndNext(submissionModel) {
    const url = `${BASE_URL}/saveandnextneworderpostpoes`;
    return this.http.post<any>(url, submissionModel);
  }

  getEventsAllStageData(event_root_id) {
    const url = `${BASE_URL}/myeventsgeteventallstagedatapoes`;
    return this.http.post<any>(url, {event_root_id});
  }

  searchEvents(data) {
    const url = `${BASE_URL}/myeventssearchpostpoes`;
    return this.http.post<any>(url, data);
  }

  sendEmail(submissionModel) {
    const url = `${BASE_URL}/sendemail`;
    return this.http.post<any>(url, submissionModel);
  }

  getInboxEntity(entity_type) {
    const url = `${BASE_URL}/inboxentitygetpoes`;
    return this.http.post<any>(url, { entity_type, medido_id: 19 });
  }

  sendFax(submissionModelFax) {
    const url = `${BASE_URL}/mfaxsendfax`;
    return this.http.post<any>(url, submissionModelFax);
  }

  addNewPatientInsurance(collectedData, insuranceFormData) {
    const url = `${BASE_URL}/patientmemberinsuranceinsertpoes`;
    const body =
    {
        medido_id: 19,
        patient_id: collectedData[`data`].patient_id,
        insurance_id: insuranceFormData[`data`].insuranceId,
        plan_id: insuranceFormData[`data`].plan_id,
        groupNo: insuranceFormData[`data`].groupNumber,
        issueDate: insuranceFormData[`data`].issueDate,
        expiryDate: insuranceFormData[`data`].expiryDate,
        personCode: insuranceFormData[`data`].personCode,
        priority: insuranceFormData[`data`].priority,
        is_active: (insuranceFormData[`data`].isActive) ? 1 : 0
    }
    return this.http.post<any>(url, body);
  }

  updatePatientInsurance(collectedData, insuranceFormData) {
    const url = `${BASE_URL}/patientmemberinsuranceupdatepoes`;
    const body =
    {
        medido_id: 19,
        patient_id: collectedData[`data`].patient_id,
        sr_id: insuranceFormData[`data`].sr_id,
        insurance_id: insuranceFormData[`data`].insuranceId,
        plan_id: insuranceFormData[`data`].plan_id,
        groupNo: insuranceFormData[`data`].groupNumber,
        issueDate: insuranceFormData[`data`].issueDate,
        expiryDate: insuranceFormData[`data`].expiryDate,
        personCode: insuranceFormData[`data`].personCode,
        priority: insuranceFormData[`data`].priority,
        is_active: (insuranceFormData[`data`].isActive) ? 1 : 0
    }
    return this.http.put<any>(url, body);
  }

  getInsuranceList(
      patient_id,
      name,
      groupNo,
      relation,
      priority,
      is_active
      ) {
    const url = `${BASE_URL}/patientmemberinsurancegetpoes`;
    const body =
    {
        patient_id,
        medido_id: 19,
        name,
        groupNo,
        relation,
        priority,
        is_active,
    }
    return this.http.post<any>(url, body);
  }

  getPlans() {
    const url = `${BASE_URL}/plansgetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  sendDataForClient(data) {
    const url = `${BASE_URL}/senddataforclient`;
    return this.http.post<any>(url, data);
  }

  updateClinical(data) {
    const url = `${BASE_URL}/patientclinicupdatepoes`;
    return this.http.put<any>(url, data);
  }

  getPatientForm() {
    const url = `${BASE_URL}/patientformsgetpoes`;
    return this.http.get<any>(url);
  }

  insertPatient(data) {
    const url = `${BASE_URL}/patientinsertpoes2`;
    return this.http.post<any>(url, data);
  }

  getPatients() {
    const url = `${BASE_URL}/patientprescriptiondatagetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getDoctors() {
    const url = `${BASE_URL}/doctorprescriptiondatagetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getDrugs() {
    const url = `${BASE_URL}/drugprescriptiondatagetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getDoctorForm() {
    const url = `${BASE_URL}/doctorinsertformgetpoes`;
    return this.http.get<any>(url);
  }

  getDrugForm() {
    const url = `${BASE_URL}/drugformgetpoes`;
    return this.http.get<any>(url);
  }

  insertDoctor(data) {
    const url = `${BASE_URL}/doctorinsertpoes2`;
    return this.http.post<any>(url, data);
  }

  insertDrug(data) {
    const url = `${BASE_URL}/druginsertpoes`;
    return this.http.post<any>(url, data);
  }

  getDrugGroups() {
    const url = `${BASE_URL}/druggroupslistgetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getPharmacies() {
    const url = `${BASE_URL}/pharmaciesgetpoes2`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getOrderEntryData(id) {
    const url = `${BASE_URL}/orderentrydatagetpoes`;
    return this.http.post<any>(url, { id, medido_id: 19 });
  }

  loadAttachments(id, email_type) {
    const url = `${BASE_URL}/downloads`;
    return this.http.post<any>(url, { id, email_type, medido_id: 19 });
  }

  getClients() {
    const url = `${BASE_URL}/clientsdropdowngetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getContacts() {
    const url = `${BASE_URL}/contactsdropdowngetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getPharmaciesDropdown() {
    const url = `${BASE_URL}/pharmaciesdropdowngetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  getPharmaciesLocation(pharmacy_id) {
    const url = `${BASE_URL}/pharmacylocationdatapoes`;
    return this.http.post<any>(url, { pharmacy_id, medido_id: 19 });
  }

  getMedications(patient_id) {
    const url = `${BASE_URL}/medicationsdropdowngetpoes`;
    return this.http.post<any>(url, { patient_id, medido_id: 19 });
  }

  getPrescriptionForm() {
    const url = `${BASE_URL}/prescriptionformiogetpoes2`;
    return this.http.get<any>(url);
  }

  insertPrescription(data) {
    const url = `${BASE_URL}/prescriptioninsertpoes`;
    return this.http.post<any>(url, data);
  }

  updateOrder(submissionModel) {
    const url = `${BASE_URL}/saveneworderpoes`;
    return this.http.put<any>(url, submissionModel);
  }

  getAssignedTo() {
    const url = `${BASE_URL}/assignedtoinboxgetpoes`;
    return this.http.post<any>(url, { medido_id: 19 });
  }

  postData(formData) {
    const url = 'https://api.documo.com/v1/faxes';
    const headers = new HttpHeaders({
        Authorization: 'Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2ZTQyYmQ4MS00MTRmLTQwMjktYWYxZi03NWFkYzgyYjYyNTkiLCJpYXQiOjE1ODczNjgxMjd9.PhVIT8DmHnqUrMQoMaKej6WjZjDHB281WkW3Phbpv7s'
    });
    const options = { headers };
    return this.http.post<any>(url, formData, options);
  }


}
