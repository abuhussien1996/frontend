import { Component, OnInit, ChangeDetectorRef, Injectable,ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment'
import {
    SelectItem,
    FormIOSelectData
} from './models';
import { EventLegaciesService } from './event-legacies.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { EventDialogComponent } from './event.dialog.component';

@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
    readonly DT_FORMAT
        = 'MM-DD-YY';
    parse(value: string): NgbDateStruct {
        if (value) {
            value = value.trim();
            const mdt = moment(value, this.DT_FORMAT)
        }
        return null;
    }
    format(date: NgbDateStruct): string {
        if (!date) return '';
        const mdt = moment([date.year, date.month - 1, date.day]);
        if (!mdt.isValid()) return '';
        return mdt.format(this.DT_FORMAT);
    }
}
@Component({
    selector: 'kt-event-legacies',
    templateUrl: './event-legacies.component.html',
    providers: [
        { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter }
    ]
})
export class EventLegaciesComponent implements OnInit {
    myEventsData: any[];
    event: SelectItem[];
    eventAllStage2: SelectItem[];
    event2 = new Array<FormIOSelectData>();
    eventAllStage = new Array<FormIOSelectData>();
    status: SelectItem[];
    selectedStatus: string;
    from: any;
    to: any;
    entitydata: SelectItem[];
    assignedto: SelectItem[];
    assignedto2 = new Array<FormIOSelectData>();
    entitydata2 = new Array<FormIOSelectData>();
    to2: any;
    from2: any;
    displayedColumns: string[] = ['date_time', 'stage', 'patient', 'rx', 'option', 'status', 'last_update', 'user_id', 'actions'];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    constructor(
        private cdr: ChangeDetectorRef,
        public datepipe: DatePipe,
        private eventLegaciesService: EventLegaciesService,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        this.status = [{ label: 'All', value: 'all' }, { label: 'Pending', value: 'pending' }, { label: 'Confirmed', value: 'confirmed' }]
        const today = new Date();
        const fromDate = new Date(today.getTime() + (-21 * 24 * 60 * 60 * 1000));
        const dd = String(today.getDate()).padStart(2, '0');
        const dd2 = String(fromDate.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
        const yyyy = today.getFullYear().toString().substring(2, -1);
        const mm2 = String(fromDate.getMonth() + 1).padStart(2, '0'); // January is 0!
        const yyyy2 = fromDate.getFullYear().toString().substring(2, -1);
        this.to2 = mm + '-' + dd + '-' + yyyy;
        this.from2 = mm2 + '-' + dd2 + '-' + yyyy2;
        this.filter();
        this.eventLegaciesService.getEventData().subscribe(res2 => {
            this.event2 = new Array<FormIOSelectData>();
            for (const atd of res2) {
                const u: FormIOSelectData = { label: atd.Description, value: atd.event_id };
                this.event2.push(u);
            }
            this.event = this.event2;
            this.cdr.detectChanges();
        });
    }

    callbackInvok(event) {
        this.eventLegaciesService.getEventsAllStageData(event.value).subscribe(res => {
            this.eventAllStage = new Array<FormIOSelectData>();
            for (const atd of res) {
                const u: FormIOSelectData = { label: atd.Description, value: atd.sequence };
                this.eventAllStage.push(u);
            }
            this.eventAllStage2 = this.eventAllStage;
            this.cdr.detectChanges();
        });
    }

    callbackInvokEntity(event) {
        this.eventLegaciesService.getInboxEntity(event.value).subscribe(res => {
            this.entitydata2 = new Array<FormIOSelectData>();
            this.entitydata2.push({ label: 'All', value: 'all' });
            for (const atd of res) {
                const u: FormIOSelectData = { label: atd.name, value: atd.id };
                this.entitydata2.push(u);
            }
            this.entitydata = this.entitydata2;
            this.cdr.detectChanges();
        });
    }

    filter() {
        const data = {
            from: this.from,
            to: this.to,
            status: this.selectedStatus,
            medido_id: 19
        };
        this.eventLegaciesService.searchEvents(data).subscribe(res => {
            for (const obj of res) {
                const tempDate = new Date(obj.date_time);
                const tempDate5 = new Date(obj.last_update)
                obj.date_time = this.datepipe.transform(tempDate, 'MM-dd-yy hh:mm a');
                obj.last_update = this.datepipe.transform(tempDate5, 'MM-dd-yy hh:mm a');
            }
            this.myEventsData = res;
            this.dataSource = new MatTableDataSource(this.myEventsData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.paginator.firstPage();
            this.cdr.detectChanges();
        });
    }

    onRowSelect(event) {
        this.dialog.open(
            EventDialogComponent,
            {data: {
                transaction_id: event.transaction_id,
                tran_id: event.tran_id,
                event_id: event.event_id,
                status: event.status,
                date_time: event.date_time
            },
            width: '90vw'
        }).afterClosed().subscribe(() => {
            this.filter();
        })
    }

}
