import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventLegaciesComponent } from './event-legacies.component';


const routes: Routes = [
  {
    path: '',
    component: EventLegaciesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventLegaciesRoutingModule { }
