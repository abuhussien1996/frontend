
export class Patient {
    constructor(
        public id: number,
        public name: string,
        public dob: string,
        public sex: string,
        public address: string
    ) {}
}

export class Client {
    constructor(
        public id: number,
        public name: string,
        public phone: string,
        public address: string
    ) {}
}

export class Contact {
    constructor(
        public id: number,
        public name: string,
        public phone: string,
        public address: string
    ) {}
}

export class Pharmacy {
    constructor(
        public id: number,
        public name: string,
        public phone: string,
        public address: string
    ) {}
}
export class PharmacyLocation {
    constructor(
        public pharmacy_location_id: number,
        public name: string,
        public npi: string,
        public address: string
    ) {}
}
export class Medication {
    constructor(
        public id: string,
        public doctor: string,
        public doctorData: string,
        public drug: string,
        public drug_id: string,
        public prescribe_date: string,
        public doctor_id: string,
        public refills: string,
        public sig: string,
        public qty: string,
        public ndc: string,
        public diagnosis: string,
        public jcode: string,
        public drug_note: string,
        public pharmacy: string,
        public pharmacyData: string,
        public cfi: string,
        public daysSupply: string
    ) {}
}

export class Drug {
    constructor(
        public id: number,
        public name: string,
        public drug_group: string,
        public ndc: string,
        public jcode: string,
        public cfi: string,
        public note: string,
        public copay_assistance: string,
        public copay_assistance_url: string
    ) {}
}

export class Doctor {
    constructor(
        public id: number,
        public name: string,
        public phone: string
    ) {}
}

export class Facility {
    constructor(
        public id: number,
        public name: string,
        public phone: string
    ) {}
}


export class Insurance {
    constructor(
        public id: number,
        public name: string,
        public bin: string,
        public pcn: string
    ) {}
}

export interface SelectItem {
    label?: string;
    value: any;
    styleClass?: string;
    icon?: string;
    title?: string;
    disabled?: boolean;
}

export interface FormIOSelectData {
    label: string; // description
    value: string; // id
}