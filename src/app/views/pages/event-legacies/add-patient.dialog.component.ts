// Angular
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
// Material
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { DomSanitizer } from '@angular/platform-browser';
import { Formio } from 'angular-formio';
import Webform from 'formiojs/Webform';
import { LayoutUtilsService, MessageType } from 'src/app/core/_base/crud';
import { EntitySearchDialogComponent } from './entity-search.dialog.component';
import { EventLegaciesService } from './event-legacies.service';
import { Insurance } from './models';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'kt-legacies-add-patient',
    templateUrl: './add-patient.dialog.component.html',
    styleUrls: ['./event-legacies.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddPatientDialogComponent implements OnInit {
    tabIndex: number;
    k: number;
    patientForm: JSON;
    patientFormData: JSON;
    clinicalForm: JSON;
    clinicalFormData: any;
    insuranceForm: JSON;
    insuranceFormData: JSON;
    pdfSizeMega: boolean;
    pdfSizelessMega: boolean;
    pdfPatient: any;
    pdfDisplay: boolean;
    pdfDisplayPatient: boolean;
    patientWebform: Webform;
    clinicalDisabled = true;
    detailsDisabled = false;
    insuranceDisabled = true;
    insurances: Insurance[] = [];
    add_flag: boolean;
    insuranceWebform: Webform;
    insuranceName: any;
    insuranceGroupNo: any;
    insuranceRelation: any;
    insurancePriority: any;
    insuranceIsActive: any;
    insuranceList = [];
    imgDisplay: boolean;
    id: any;
    dataSource: MatTableDataSource<any>;
    displayedColumns: string[] = ['insurance_id', 'name', 'group_no', 'person_code', 'priority', 'is_active'];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<AddPatientDialogComponent>,
        public dialog: MatDialog,
        private eventLegaciesService: EventLegaciesService,
        private cdr: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private layoutUtilsService: LayoutUtilsService
    ) { }

    ngOnInit() {
        this.tabIndex = 0;
        this.k = 0;
        this.eventLegaciesService.getPatientForm().subscribe(res => {
            this.patientForm = JSON.parse(res.details_form);
            this.patientFormData = JSON.parse(`{"data":` + res.details_data + `}`);
            this.clinicalForm = JSON.parse(res.clinical_form);
            this.clinicalFormData = JSON.parse(`{"data":` + res.clinical_data + `}`);
            this.insuranceForm = JSON.parse(res.insurance_form);
            this.insuranceFormData = JSON.parse(`{"data":` + res.insurance_data + `}`);
            if (this.data.extraData[`attachments`].length > 0) {
                this.eventLegaciesService.loadAttachments(this.data.extraData[`attachments`][0], this.data.emailType).subscribe((res2) => {
                    if (res2[`type`] === 'pdf' || res2[`type`] === 'PDF') {
                        this.pdfPatient = this.sanitize('data:application/pdf;base64,' + btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data))));
                        this.pdfDisplay = true;
                        this.pdfDisplay = false;
                        this.pdfDisplayPatient = true;
                    }
                    this.cdr.detectChanges();
                });
            }
            this.createPatientForm();
            this.cdr.detectChanges();
        });
    }

    // tslint:disable-next-line:no-shadowed-variable
    toArrayBuffer(buffer) {
        const ab = new ArrayBuffer(buffer.length);
        const view = new Uint8Array(buffer.length);
        for (let i = 0; i < buffer.length; ++i) {
            view[i] = buffer[i];
        }
        return view;
    }

    Uint8ToString(u8a) {
        const CHUNK_SZ = 0x8000;
        const c = [];
        for (let i = 0; i < u8a.length; i += CHUNK_SZ) {
            c.push(String.fromCharCode.apply(null, u8a.subarray(i, i + CHUNK_SZ)));
        }
        return c.join('');
    }

    createPatientForm() {
        Formio.createForm(document.getElementById('patientForm'), this.patientForm, {}).then((form) => {
            form.submission = this.patientFormData;
            form.url = '';
            this.patientWebform = form;
            this.cdr.detectChanges();
        })
    }

    // ************************************************************************/

    savePatient() {
        this.data.collectedData[`data`].patient = this.patientFormData[`data`].first + `  `
            + this.patientFormData[`data`].last + `  ` + this.patientFormData[`data`].sex
            + `  ` + this.patientFormData[`data`].address1;
        this.patientFormData[`data`].medido_id = 19;
        this.eventLegaciesService.insertPatient(this.patientFormData[`data`]).subscribe(res => {
            this.data.collectedData[`data`].patient_id = res.insertId;
            this.data.extraData[`patient_id`] = res.insertId;
            this.layoutUtilsService.showActionNotification(
                'Successfully Added!',
                MessageType.Read,
                3000,
                false,
                false
            );
            this.tabIndex = 1;
            this.clinicalFormData[`data`].first = this.patientFormData[`data`].first;
            this.clinicalFormData[`data`].middle = this.patientFormData[`data`].middle;
            this.clinicalFormData[`data`].last = this.patientFormData[`data`].last;
            this.clinicalFormData[`data`].dob = this.patientFormData[`data`].dob;
            this.clinicalFormData[`data`].sex = this.patientFormData[`data`].sex;
            this.clinicalFormData[`data`].last2 = this.patientFormData[`data`].last2;
            this.clinicalFormData[`data`].member = this.patientFormData[`data`].member;
            this.clinicalFormData[`data`].facility = this.patientFormData[`data`].facility;
            this.clinicalFormData = { data: this.clinicalFormData.data };
            this.clinicalDisabled = false;
            this.detailsDisabled = true;
            // this.webform.redraw();
            this.cdr.detectChanges();
        })
    }

    saveClinical() {
        this.clinicalFormData[`data`].patient_id = this.data.collectedData[`data`].patient_id;
        this.clinicalFormData[`data`].medido_id = 19;
        this.eventLegaciesService.updateClinical(this.clinicalFormData[`data`]).subscribe(res => {
            this.layoutUtilsService.showActionNotification(
                'Successfully Updated!',
                MessageType.Read,
                3000,
                false,
                false
            );
            this.add();
            this.eventLegaciesService.getPlans().subscribe((insurances) => {
                this.insurances = insurances.map(insurance => new Insurance(insurance.id, insurance.name, insurance.bin, insurance.pcn));
                this.tabIndex = 2;
                this.clinicalDisabled = true;
                this.insuranceDisabled = false;
                this.reloadInsuranceList();
                this.cdr.detectChanges();
            });
            this.cdr.detectChanges();
        })

    }

    fillInsuranceForm(insurance: any) {
        this.insuranceFormData[`data`].isActive = (insurance.is_active === '1' || insurance.is_active === true) ? true : false;
        this.insuranceFormData[`data`].priority = insurance.priority;
        this.insuranceFormData[`data`].issueDate = insurance.issue_date;
        this.insuranceFormData[`data`].expiryDate = insurance.expire_date;
        this.insuranceFormData[`data`].personCode = insurance.person_code;
        this.insuranceFormData[`data`].groupNumber = insurance.group_no;
        this.insuranceFormData[`data`].insuranceId = insurance.insurance_no;
        this.insuranceFormData[`data`].sr_id = insurance.sr_id;
        this.insuranceFormData[`data`].plan = insurance.name;
        this.insuranceFormData[`data`].bin = insurance.bin;
        this.insuranceFormData[`data`].pcn = insurance.pcn;
        this.insuranceFormData[`data`].plan_id = insurance.insurance_id;
        this.insuranceFormData[`data`].planName = insurance.name;
        this.insuranceFormData = JSON.parse('{"data" : ' + JSON.stringify(this.insuranceFormData[`data`]) + '}');
        this.createInsuranceForm();
        this.add_flag = false;
    }

    createInsuranceForm() {
        Formio.createForm(document.getElementById('insuranceForm'), this.insuranceForm, {}).then((form) => {
            form.submission = this.insuranceFormData;
            form.url = '';
            form.on('searchPlan', () => {
                this.searchInsurance();
                this.cdr.detectChanges();
            })
            this.insuranceWebform = form;
            this.cdr.detectChanges();
        })
    }

    saveInsurance() {
        if (this.add_flag) {
            this.eventLegaciesService.addNewPatientInsurance(
                this.data.collectedData,
                this.insuranceFormData
                ).subscribe(() => {
                        this.layoutUtilsService.showActionNotification(
                            'Successfully Added!',
                            MessageType.Read,
                            3000,
                            false,
                            false
                        );
                        this.add_flag = false;
                        this.add();

                        this.reloadInsuranceList();

                        this.cdr.detectChanges();
                    },
                    (error) =>
                        this.layoutUtilsService.showActionNotification(
                            'Something went wrong',
                            MessageType.Read,
                            3000,
                            false,
                            false
                        )
                )
        } else {
            this.eventLegaciesService.updatePatientInsurance(
                this.data.collectedData,
                this.insuranceFormData
                ).subscribe((res) => {
                        this.layoutUtilsService.showActionNotification(
                            'Successfully Updated!',
                            MessageType.Read,
                            3000,
                            false,
                            false
                        );
                        this.add();
                        this.reloadInsuranceList();
                        this.cdr.detectChanges();
                    },
                    (error) =>
                        this.layoutUtilsService.showActionNotification(
                            'Something went wrong',
                            MessageType.Read,
                            3000,
                            false,
                            false
                        )
                );
        }
    }

    clear() {
        this.add_flag = false;
        this.insuranceFormData[`data`].isActive = false;
        this.insuranceFormData[`data`].priority = '';
        this.insuranceFormData[`data`].issueDate = '';
        this.insuranceFormData[`data`].expiryDate = '';
        this.insuranceFormData[`data`].personCode = '';
        this.insuranceFormData[`data`].groupNumber = '';
        this.insuranceFormData[`data`].bin = '';
        this.insuranceFormData[`data`].pcn = '';
        this.insuranceFormData[`data`].insuranceId = 0;
        this.insuranceFormData[`data`].plan_id = 0;
        this.insuranceFormData[`data`].sr_id = 0;
        this.insuranceFormData[`data`].planName = '';
        this.insuranceFormData = JSON.parse('{"data" : ' + JSON.stringify(this.insuranceFormData[`data`]) + '}');
        if (this.insuranceWebform) {
            this.insuranceWebform.redraw();
        }
    }

    async add() {
        await this.clear();
        this.createInsuranceForm();
        this.add_flag = true;
    }

    reloadInsuranceList() {
        this.eventLegaciesService.getInsuranceList(
            this.data.collectedData[`data`].patient_id,
            this.insuranceName !== undefined ? this.insuranceName : '',
            this.insuranceGroupNo !== undefined ? this.insuranceGroupNo : '',
            this.insuranceRelation !== undefined ? this.insuranceRelation : '',
            this.insurancePriority !== undefined ? this.insurancePriority : '',
            this.insuranceIsActive !== undefined ? this.insuranceIsActive : '',
        ).subscribe((res) => {
                for (const obj of res) {
                    obj.is_active = (obj.is_active === '1') ? true : false;
                    obj.priority = (obj.priority === 'p') ? 'Primary' : (obj.priority === 's') ? 'Secondary' :
                        (obj.priority === 't') ? 'Tertiary' : (obj.priority === 'o') ? 'Other' : '';
                }
                this.insuranceList = res;
                this.dataSource = new MatTableDataSource(this.insuranceList);
                this.cdr.detectChanges();
            });
    }

    sanitize(url: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    close() {
        this.dialogRef.close({collectedData: this.data.collectedData, extraData: this.data.extraData});
    }

    searchInsurance() {
        const dataList = this.insurances;
        this.dialog.open(
            EntitySearchDialogComponent,
            {data : {dataList, type: 'insurance'}, width: '30vw'}
            ).afterClosed().subscribe((res) => {
            if(res) {
                this.handleInsurance(res);
            }
        })
    }

    handleInsurance(input) {
        if (input.name !== undefined) {
            this.insuranceFormData[`data`].planName = input.name;
            this.insuranceFormData[`data`].bin = input.bin;
            this.insuranceFormData[`data`].pcn = input.pcn;
            this.insuranceFormData[`data`].plan_id = input.id;
            this.insuranceWebform.redraw();
        }
    }

    Previous() {
        if (this.k > 0) {
            this.k = --this.k;
            this.id = this.data.extraData[`attachments`][this.k];
            this.downloads();
        }
    }

    Next() {
        if (this.k < this.data.extraData[`attachments`].length - 1) {
            this.k = ++this.k;
            this.id = this.data.extraData[`attachments`][this.k];
            this.downloads();
        }
    }


    downloads() {
        this.imgDisplay = false;
        this.pdfDisplay = false;
        this.pdfSizeMega = false;
        this.pdfSizelessMega = false;
        this.eventLegaciesService.loadAttachments(this.id, this.data.emailType).subscribe((res2) => {
            if (res2[`type`] === 'pdf' || res2[`type`] === 'PDF') {
                this.pdfPatient = this.sanitize('data:application/pdf;base64,'
                + btoa(this.Uint8ToString(this.toArrayBuffer(res2[`body`].data))));
                this.pdfDisplay = false;
                this.pdfDisplayPatient = true;
            }
            this.cdr.detectChanges();
        });
        this.cdr.detectChanges();
    }

}
