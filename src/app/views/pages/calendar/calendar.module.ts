import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarViewComponent } from './calendar-view/calendar-view.component';

import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import {CalendarService} from './_services/calendar.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {CalendarInterceptor} from './_services/calendar-interceptor.interceptor'; // a plugin
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TaskEffects, TaskService, tasksReducer } from 'src/app/core/timeline';
import { CreateTaskComponent } from './create-task/create-task.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TranslateModule } from '@ngx-translate/core';
import { PartialsModule } from '../../partials/partials.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ChooseTemplateDialogComponent } from './choose-template/choose-template.dialog.component';
import {TaskTimelineComponent} from './task-timeline/task-timeline.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {InlineSVGModule} from 'ng-inline-svg';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [
    CalendarViewComponent,
    CreateTaskComponent,
    ChooseTemplateDialogComponent,
    TaskTimelineComponent
  ],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    FullCalendarModule,
    StoreModule.forFeature('tasks', tasksReducer),
    EffectsModule.forFeature([TaskEffects]),
    PerfectScrollbarModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatIconModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatRadioModule,
    TranslateModule.forChild(),
    PartialsModule,
    MatSelectModule,
    MatCheckboxModule,
    NgxPermissionsModule.forChild(),
    MatButtonToggleModule,
    InlineSVGModule
  ],
  providers: [
    CalendarService,
    TaskService,
    { provide: HTTP_INTERCEPTORS, useClass: CalendarInterceptor, multi: true }
  ],
  entryComponents: [
    CreateTaskComponent
  ]
})
export class CalendarModule { }
