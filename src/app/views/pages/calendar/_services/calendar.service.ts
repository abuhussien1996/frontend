import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpUtilsService} from "../../../../core/_base/crud";
import {select, Store} from "@ngrx/store";
import {AppState} from "../../../../core/reducers";
import {currentAuthToken, currentUser} from "../../../../core/auth";
import {Observable} from "rxjs";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId =  res.companyID );
  }


  // Server should return sorted/filtered relationships and merge with items from state
  findCalendarTasks(start, end): Observable<any> {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/tasks?from=${start}&to=${end}`;
    // Note: Add headers if needed (tokens/bearer)
    console.log(url)
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  getCalendarTasksURL(): string {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/tasks';
    return url;
  }

  getAuthToken() {
    return this.authToken;
  }
}
