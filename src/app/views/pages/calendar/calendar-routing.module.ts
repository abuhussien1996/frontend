import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CalendarViewComponent} from './calendar-view/calendar-view.component';
import {CalendarService} from './_services/calendar.service';


const routes: Routes = [
  {
    path: '',
    component: CalendarViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalendarRoutingModule {
}
