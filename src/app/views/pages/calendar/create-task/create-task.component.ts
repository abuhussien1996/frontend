// Angular
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
// Material
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
// RxJS
import {BehaviorSubject, forkJoin, Observable, of, Subscription} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../core/reducers';
import {ContactsService, Task, TaskService} from 'src/app/core/timeline';
// Services and Models
import {currentUser, User} from 'src/app/core/auth';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from 'src/app/core/auth/_services';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud';
import {CategoryService} from 'src/app/core/lists';
import {GeneralAutocompleteComponent} from '../../../partials/layout/general-autocomplete/general-autocomplete.component';
import {CommunicationsService} from 'src/app/core/timeline/_services';
import {TagService} from 'src/app/core/services/tag-service.service';
import {CommentsService} from '../../../../core/services/comments.service';
import * as moment from 'formiojs/node_modules/moment';
import {GlobalSettingsService} from 'src/app/core/services/global-settings.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-create-task',
  templateUrl: './create-task.component.html',
  styleUrls:['./create-task.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateTaskComponent implements OnInit, OnDestroy {
  selectedUsers: Array<number>;
  currentUser: User;
  selfAssign = true;
  task: Task;
  loadingSubject = new BehaviorSubject<boolean>(true);
  loading$: Observable<boolean>;
  taskForm: FormGroup;
  hasFormErrors: boolean;
  clicked: boolean;
  createNoteTask: boolean;
  category: any;
  entity: any;
  assignee: any;
  note: any;
  start_time: any;
  lastCreatedTaskId: number;
  subscriptions: Subscription[] = [];
  entityAutocompleteCall;
  assigneeAutocompleteCall;
  categoryAutocompleteCall = this.categoryService.searchCategories.bind(this.categoryService);
  @ViewChild('followerAutocomplete') followerAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('entityAutocomplete') entityAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('assigneeAutocomplete') assigneeAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('fileInput2') fileInput2: ElementRef;
  @ViewChild('body') body: ElementRef;
  @ViewChild('comment') comment: ElementRef;
  comments = [];
  files = [];
  updatedFiles = [];
  attachments = [];
  updatedAttachments = [];
  showFileLoader = new BehaviorSubject(false);
  totalFileSize = 1;
  uploadRequests: any[];
  callback;
  subtasks = [];
  subFiles = [];
  progressOptions = ['PLANNED', 'WAITING', 'ON_HOLD', 'IN_PROGRESS', 'DONE', 'CANCELED'];
  priorityOptions = ['HIGH', 'MEDIUM', 'LOW'];
  deletedAttachments = []

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<CreateTaskComponent>,
    private userService: UserService,
    private store: Store<AppState>,
    private taskFB: FormBuilder,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    public datepipe: DatePipe,
    private layoutUtilsService: LayoutUtilsService,
    private taskService: TaskService,
    private globalSettingsService: GlobalSettingsService,
    private categoryService: CategoryService,
    private communicationsService: CommunicationsService,
    private tagService: TagService,
    private commentsService: CommentsService,
    private contactsService: ContactsService,
  ) {
    let categoryId = null
    if (this.data.mode !== 'create' && this.data.task.task_template) {
      categoryId = this.data.task.task_template.category_id
    }else if (this.data.mode === 'create' && this.data.template){
      categoryId = this.data.template.category_id
    }
    this.assigneeAutocompleteCall = this.userService.searchUser.bind(this.userService, categoryId);
  }

  ngOnInit() {
    if (!this.data.task) {
      this.task = new Task();
      this.task.clear();
      if (this.data.template) {
        const template = JSON.parse(this.data.template.template);
        this.task.title = template.task.title;
        this.task.description = template.task.description;
        this.task.priority = template.priority;
        this.task.task_followers = template.task_followers;
        this.task.task_responsible_users = template.task_responsible_users;

        template.subTasks.forEach((subTask) => {
          const _task = new Task();
          _task.clear();
          _task.title = subTask.title;
          _task.description = subTask.description;
          _task.attachments = [];
          _task.task_template = this.data.template
          _task.priority = subTask.priority ? subTask.priority : 'LOW';
          _task.task_followers = subTask.task_followers ? subTask.task_followers : [];
          _task.task_responsible_users = subTask.task_responsible_users ? subTask.task_responsible_users : [];
          this.subtasks.push(_task);
        });
      } else if (this.data.note) {
        this.task.description = this.data.note.description;
        this.task.due_date = this.data.note.due_date;
      }
    } else {
      this.task = this.data.task;
    }
    if (this.data.mode === 'update') {
      if (!this.data.nonDB) {
        this.getComments();
        if (this.data.task.task_responsible_users.length > 0) {
          this.taskService.getUsersbyId(this.data.task.task_responsible_users.map((user) => user.responsible_id)).subscribe((res) => {
            if (res.pageOfItems) {
              this.taskForm.get('task_responsible_users').setValue(res.pageOfItems.map(user => {
                return {
                  responsible_id: user.id,
                  responsible_type: 'USER',
                  fullName: user.fullName,
                  name: user.userName
                }
              }));
              this.cdr.detectChanges();
            }
          });
        }
        if (this.data.task.task_followers.length > 0) {
          this.taskService.getUsersbyId(this.data.task.task_followers.map((user) => user.follower_id)).subscribe((res) => {
            if (res.pageOfItems) {
              this.taskForm.get('task_followers').setValue(res.pageOfItems.map(user => {
                return {
                  follower_id: user.id,
                  type: 'USER',
                  fullName: user.fullName,
                  name: user.userName
                }
              }));
              this.cdr.detectChanges();
            }
          });
        }
        if (this.data.task.task_entities.length > 0) {
          this.contactsService.getContactsByIds(this.data.task.task_entities.map((entity) => entity.entity_id)).subscribe((res) => {
            if (res.data) {
              this.taskForm.get('task_entities').setValue(res.data.map(item => {
                if (item && item.id) {
                  const entity = {
                    entity_id: item.id,
                    dob: item.dob,
                    first_name: item.first_name,
                    last_name: item.last_name
                  };
                  return entity
                }
                return item
              }));
              this.cdr.detectChanges();
            }
          });
        }

        this.attachments = this.data.task.task_attachments;
        if (!this.data.isSubtask) {
          this.getSubTasks();
        }
      } else {
        this.attachments = this.data.task.attachments;
      }
    }
    this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe((res) => {
      if (res) {
        this.currentUser = res;
        const follower = {
          follower_id: res.id,
          fullName: res.fullName,
          name: res.fullName,
          type: 'USER',
        }
        if (!this.task.task_followers) {
          this.task.task_followers = [(follower)];
        } else {
          const ff = this.task.task_followers.find(f => {
            return f.follower_id === res.id;
          })
          if (!ff) {
            this.task.task_followers.push(follower);
          }
        }
      }
    }));
    this.createForm();
    this.loading$ = this.loadingSubject.asObservable();
    this.loadingSubject.next(true);
    this.loadingSubject.next(false);
    this.onChanges()
  }

  getTitle() {
    return this.data.isSubtask ?
      this.data.mode === 'create' ? this.translate.instant('TASK.CREATE_SUB_TASK') : this.data.task.title
      : this.data.mode === 'create' ? this.translate.instant('TASK.CREATE_TASK') : this.data.task.title;
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this.cdr.detach();
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Create form
   */
  createForm() {
    this.taskForm = this.taskFB.group({
      title: [this.task.title, Validators.required],
      description: [this.task.description],
      entityType: ['DOCTOR'],
      task_entities: [this.task.task_entities ? this.task.task_entities : []],
      task_responsible_users: [this.task.task_responsible_users ? this.task.task_responsible_users : []],
      task_followers: [this.task.task_followers ? this.task.task_followers : []],
      due_date: [new Date(this.task.due_date ? new Date(this.task.due_date + ' UTC') : new Date())],
      is_recurring: [this.task.is_recurring],
      priority: [this.task.priority ? this.task.priority : 'LOW'],
      progress: [this.task.progress, Validators.required],
    });
    if (this.data.mode === 'create') {
      this.taskForm.get('progress').disable();
    }
    this.entityAutocompleteCall = this.contactsService.searchContacts.bind(this.contactsService);
  }

  typeChanged(event) {
    this.entityAutocompleteCall = this.contactsService.searchContacts.bind(this.contactsService);
    setTimeout(() => {
      this.entityAutocomplete.reload.next(true);
    }, 1000)
  }

  /**
   * Save data
   */
  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.taskForm.controls;
    /** check form */
    if (this.taskForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      return;
    }
    this.followerAutocomplete.updateValueAndValidity();
    this.entityAutocomplete.updateValueAndValidity();
    this.assigneeAutocomplete.updateValueAndValidity();
    this.clicked = true;
    this.data.isSubtask ?
      this.data.nonDB ? this.dialogRef.close({task: this.prepareTask(), files: this.files}) :
        this.updateTask(this.prepareTask())
      : this.data.mode === 'create' ? this.createTask(this.prepareTask()) : this.updateTask(this.prepareTask());
  }

  /**
   * Returns object for saving
   */
  prepareTask(): Task {
    const controls = this.taskForm.controls;
    const _task = new Task();
    _task.id = this.task.id;
    _task.completed = this.task.completed;
    _task.parent_id = this.task.parent_id;
    _task.title = controls.title.value;
    _task.description = controls.description.value;
    _task.progress = controls.progress.value;
    _task.priority = controls.priority.value;
    _task.due_date = this.datepipe.transform(controls.due_date.value, 'yyyy-MM-dd HH:mm', 'utc');
    _task.is_recurring = controls.is_recurring.value;
    _task.task_entities = controls.task_entities.value;
    _task.task_responsible_users = controls.task_responsible_users.value;
    _task.task_followers = controls.task_followers.value;
    _task.attachments = this.data.mode === 'create' || this.data.nonDB ? this.attachments : null;
    _task.task_template = this.data.task ? this.data.task.task_template : this.data.template
    return _task;
  }

  /**
   * Create task
   *
   * @param _task: Task
   */
  createTask(_task: Task) {
    const taskArray = [_task];
    this.subtasks.forEach(val => taskArray.push(Object.assign({}, val)));
    let taskBody
    taskBody = {tasks: taskArray};
    if(this.data.fromWorkflow){
      taskBody = {tasks:taskArray,
        action_queue_id : this.data && this.data.action_queue_id ? this.data.action_queue_id:null,
        workflow_queue_id : this.data && this.data.workflow_queue_id ? this.data.workflow_queue_id:null,
        project_id : this.data && this.data.project_id ? this.data.project_id:null,
      }
    }
    this.taskService.createTask2(taskBody).subscribe((res) => {
      if (res.data) {
        this.globalSettingsService.loadTasks();
        if (this.files.length === 0) {
          const successMsg = this.translate.instant('TASK.SUCCESS_CREATE');
          this.layoutUtilsService.showActionNotification(successMsg, MessageType.Create, 3000, false, false);
          this.dialogRef.close({success: true,task:res.data});
        }
        const obs = [];
        const obs2 = [];
        let pre_signed_urls = [];
        for (const obj of res.data) {
          pre_signed_urls = pre_signed_urls.concat(obj.pre_signed_url.map(x => x.url));
          obs2.push(this.taskService.sendFiles(obj.id, obj.pre_signed_url.map(x => x.uuid)));
        }
        const files = this.files.concat(this.subFiles);
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < files.length; i++) {
          obs.push(this.communicationsService.uploadFiles(pre_signed_urls[i], files[i]));
          this.totalFileSize += files[i].size;
        }
        this.uploadRequests = obs;
        this.callback = this.sendFiles.bind(this, obs2);
        this.showFileLoader.next(true);
      } else {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Update,
          3000,
          false,
          false
        );
      }
    });
  }

  /**
   * Update task
   *
   * @param _task: Task
   */

  updateTask(_task: Task) {
    this.taskService.updateTask2(_task).subscribe((res) => {
      if (res && this.deletedAttachments.length === 0 && this.updatedAttachments.length === 0) {
        this.dialogRef.close({success: true,task:res});
      }
    });
    if (this.deletedAttachments.length > 0) {
      this.taskService.deleteAttachment(this.deletedAttachments, _task.id).subscribe(result => {
        if (this.updatedAttachments.length === 0) {
          this.dialogRef.close({success: true});
        }
      });
    }
    if (this.updatedAttachments.length > 0) {
      const body = {attachments: this.updatedAttachments}
      this.taskService.updateTaskAttachment(body, this.task.id).subscribe(res => {
        if (res.data) {
          const obs = [];
          const obs2 = [];
          let pre_signed_urls = [];
          let uuids = []
          const files = this.updatedFiles;
          for (const obj of res.data) {
            pre_signed_urls = pre_signed_urls.concat(obj.url);
            uuids = uuids.concat(obj.uuid);
          }
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < files.length; i++) {
            obs.push(this.communicationsService.uploadFiles(pre_signed_urls[i], files[i]).subscribe());
          }
          this.taskService.sendFiles(this.task.id, uuids).subscribe(response => {
            this.dialogRef.close({success: true});
          });
        }
      })
    }
  }

  /**
   * Checking control validation
   *
   * @param controlName: string => Equals to formControlName
   * @param validationType: string => Equals to validators name
   */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.taskForm.controls[controlName];
    if (!control) {
      return false;
    }
    return control.hasError(validationType) && (control.dirty || control.touched);
  }

  filter(arr) {
    const index = arr.indexOf(this.taskForm.get('assignee').value);
    if (index >= 0) {
      return of(arr.splice(index, 1));
    }
    return of(arr);
  }

  getTimePlaceholder(option) {
    switch (option) {
      case 'date':
        return this.translate.instant('TASK.DUE_DATE');
      case 'time':
        return this.translate.instant('TASK.DUE_TIME');
    }
  }

  close() {
    this.dialogRef.close();
  }

  filterEntityFn(entity: any): string {
    let res = ''
    if (!entity) {
      return res;
    }
    if (entity.first_name) {
      res = res + entity.first_name
    }
    if (entity.last_name) {
      res = res + ' ' + entity.last_name
    }
    if (entity && entity.dob) {
      res += '  ' + `<small class="text-muted">` + moment(entity.dob).format('l') + `</small>`
    }
    return res
  }

  filterAssigneeFn(assignee: any): string {
    let res = ''
    if (assignee) {
      res += assignee.fullName
    }
    if (assignee && assignee.name) {
      res += '  ' + `<small class="text-muted">` + assignee.name + `</small>`
    }
    return res
  }

  formatNewEntityFn(entity: any) {
    return entity;

  }

  formatNewAssigneeFn(assignee: any) {
    return assignee;
  }

  formatNewFollowerFn(follower: any) {
    return follower
  }

  onEntityChange(val) {
    const entities = val.map(item => {
      if (item && item.id) {
        const entity = {
          entity_id: item.id,
          dob: item.dob,
          first_name: item.first_name,
          last_name: item.last_name
        };
        return entity
      }
      return item
    })
    this.taskForm.controls.task_entities.setValue(entities);
    this.taskForm.controls.task_entities.updateValueAndValidity();
  }

  onAssigneeChange(val) {
    const users = val.map(item => {
      if (item && !item.responsible_id) {
        const user = {
          responsible_id: item.id,
          responsible_type: 'USER',
          fullName: item.fullName,
          name: item.name
        };
        return user
      }
      return item
    })
    this.taskForm.controls.task_responsible_users.setValue(users);
    this.taskForm.controls.task_responsible_users.updateValueAndValidity();
  }

  onFollowerChange(val) {
    const users = val.map(item => {
      if (item && !item.follower_id) {
        const user = {
          follower_id: item.id,
          type: 'USER',
          fullName: item.fullName,
          name: item.name
        };
        return user
      }
      return item
    })
    this.taskForm.controls.task_followers.setValue(users);
    this.taskForm.controls.task_followers.updateValueAndValidity();
  }

  onFileChange(event) {
    for (const file of event.target.files) {
      this.attachments.push(file.name);
      this.files.push(file);
    }
  }

  onFileUpdated(event) {
    for (const file of event.target.files) {
      this.updatedAttachments.push(file.name);
      this.updatedFiles.push(file);
    }

  }

  resetFileInput() {
    this.attachments = [];
    this.files = [];
    this.fileInput.nativeElement.value = [];
  }

  getAttachmentName(attachment) {
    return attachment.path.substring(attachment.path.lastIndexOf('/') + 1);
  }

  getSvgPath(attachment, type?: string) {
    const name = (this.data.mode === 'update' && !this.data.nonDB && type !== 'update') ?
      this.getAttachmentName(attachment) : attachment;

    switch (name.substring(name.lastIndexOf('.') + 1).toLowerCase()) {
      case 'pdf':
        return 'assets/media/svg/files/pdf.svg';
      case 'png':
        return 'assets/media/svg/files/png.svg';
      case 'jpg':
        return 'assets/media/svg/files/jpg.svg';
      case 'jpeg':
        return 'assets/media/svg/files/jpg.svg';
      case 'csv':
        return 'assets/media/svg/files/csv.svg';
      case 'txt':
        return 'assets/media/svg/files/txt.svg';
      case 'doc':
        return 'assets/media/svg/files/doc.svg';
      case 'docx':
        return 'assets/media/svg/files/doc.svg';
      case 'ppt':
        return 'assets/media/svg/files/ppt.svg';
      case 'html':
        return 'assets/media/svg/files/html.svg';
      case 'xls':
        return 'assets/media/svg/files/svg.svg';
      default:
        return 'assets/media/svg/files/document.svg';
    }
  }

  sendFiles(observables: Array<Observable<any>>) {
    forkJoin(observables).subscribe(() => {
      this.layoutUtilsService.showActionNotification(
        this.translate.instant('TASK.SUCCESS_CREATE'),
        MessageType.Update,
        3000,
        false,
        false
      );
      this.dialogRef.close();
    });
  }

  callbackOnFileLoad() {
    this.callback();
  }

  createSubTask() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = {mode: 'create', isSubtask: true, nonDB: this.data.mode === 'create'}
    this.dialog.open(CreateTaskComponent, dialogConfig).afterClosed().subscribe(res => {
      if (res && res.task) {
        this.subtasks.push(res.task);
        this.subFiles = this.subFiles.concat(res.files);
        this.cdr.detectChanges();
      }
    })
  }

  updateSubTask(task, index) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = {task, mode: 'update', isSubtask: true, nonDB: this.data.mode === 'create'}
    this.dialog.open(CreateTaskComponent, dialogConfig).afterClosed().subscribe(res => {
      if (res) {
        this.data.mode !== 'create' ? this.getSubTasks() : this.subtasks[index] = res.task;
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('TASK.SUCCESS_EDIT'),
          MessageType.Update,
          3000,
          false,
          false
        );
        this.cdr.detectChanges();
      }
    })
  }

  getSubTasks() {
    this.subtasks = [];
    this.taskService.getSubTasks(this.task.id).subscribe((res) => {
      if (res.data) {
        res.data.forEach((subtask) => this.subtasks.push(subtask));
      }
      this.cdr.detectChanges();
    });
  }

  getComments() {
    let users = []
    let resultOfCOmment = []

    this.commentsService.getComments('tasks_v2', this.task.id).subscribe((res) => {
      if (res && res.data) {
        resultOfCOmment = res.data;
        for (const c of resultOfCOmment) {
          c.editMode = false;
          users.push(c.last_modified_user)
        }
      }
      if (users.length > 0)
        this.userService.getUserByUserName(users).subscribe(result => {
          if (result.code === 200) {
            users = result.pageOfItems
            for (const c of resultOfCOmment) {
              c.userName = c.last_modified_user
              for (const x of users) {
                if (x.userName === c.last_modified_user) {
                  c.profileUrl = x.profileUrl;
                }
              }
            }
            this.comments = resultOfCOmment
            this.comments.sort((a: any, b: any) => {
              // @ts-ignore
              return new Date(b.updated) - new Date(a.updated);

            })
          }
        });
      this.cdr.detectChanges();
    });
  }

  getRecurringString(): string {
    switch (this.taskForm.get('is_recurring').value) {
      case true:
        return 'Recurring';
      case false:
        return 'Only Once';
    }
    return '';
  }

  getItemCssClassByStatus(status): string {
    switch (status) {
      case 'PLANNED':
        return 'warning';
      case 'IN_PROGRESS':
        return 'success';
      case 'DONE':
        return 'primary';
      case 'WAITING':
        return 'info';
      case 'ON_HOLD':
        return 'danger';
    }
  }

  getItemTextByStatus(status): string {
    switch (status) {
      case 'PLANNED':
        return this.translate.instant('TASK.PLANNED');
      case 'IN_PROGRESS':
        return this.translate.instant('TASK.IN_PROGRESS');
      case 'DONE':
        return this.translate.instant('TASK.DONE');
      case 'WAITING':
        return this.translate.instant('TASK.WAITING');
      case 'ON_HOLD':
        return this.translate.instant('TASK.ON_HOLD');
      case 'CANCELED':
        return this.translate.instant('TASK.CANCELED');
    }
  }

  getPriority(priority) {
    if (priority === 'HIGH')
      return 'svg-icon svg-icon-danger svg-icon-2x'
    if (priority === 'MEDIUM')
      return 'svg-icon svg-icon-warning svg-icon-2x'
    return 'svg-icon svg-icon-2x'
  }

  getPriorityText(priority) {
    switch (priority) {
      case 'HIGH':
        return this.translate.instant('TASK.HIGH');
      case 'MEDIUM':
        return this.translate.instant('TASK.MEDIUM');
      case 'LOW':
        return this.translate.instant('TASK.LOW');
      default:
        return '';
    }
  }

  assignSelf() {
    if (this.taskForm.get('task_responsible_users').value.findIndex((o) => o.responsible_id === this.currentUser.id) === -1) {
      const user = {
        responsible_id: this.currentUser.id,
        responsible_type: 'USER',
        fullName: this.currentUser.fullName,
        name: this.currentUser.name
      };
      this.taskForm.get('task_responsible_users').value.push(user);
    }
  }

  isSelfAssigned() {
    return this.taskForm.get('task_responsible_users').value.findIndex((o) => o.responsible_id === this.currentUser.id) !== -1
  }

  deleteAttachment(attachment, array) {
    if (array === 'latest') {
      this.deletedAttachments.push(attachment.id)
      this.attachments = this.attachments.filter(att => att.id !== attachment.id);
      return;
    }
    this.updatedAttachments = this.updatedAttachments.filter(att => att !== attachment);
  }

  onChanges(): void {
    this.taskForm.get('progress').valueChanges.subscribe(val => {
      if (this.data.mode === 'update' && val === 'DONE') {
        const _title = 'Update task'
        const _description = 'Are you sure you want to set the main task to Done ?'
        const _waitDesciption = 'Updating...';
        const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
        dialogRef.afterClosed().subscribe(res => {
          if (!res) {
            return;
          }
        });
      }
    });
  }

}
