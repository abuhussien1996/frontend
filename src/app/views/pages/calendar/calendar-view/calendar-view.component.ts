import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {CalendarOptions} from '@fullcalendar/angular';
import {TranslateService} from '@ngx-translate/core';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {ContactsService, TaskService} from 'src/app/core/timeline';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {CreateTaskComponent} from '../create-task/create-task.component';
import {Observable, Subscription} from 'rxjs';
import {FormControl} from '@angular/forms';
import {currentUser, isLoggedOut, User} from 'src/app/core/auth';
import {UserService} from 'src/app/core/auth/_services';
import {MatRadioChange} from '@angular/material/radio';
import {CategoryService} from 'src/app/core/lists';
import * as moment from 'formiojs/node_modules/moment';
import {QueryParamsModel} from '../../../../core/_base/crud';
import {TemplateService} from '../../../../core/lists/_services/template.service';
import {MatButton} from '@angular/material/button';
import {GeneralAutocompleteComponent} from '../../../partials/layout/general-autocomplete/general-autocomplete.component';

@Component({
  selector: 'kt-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss']
})
export class CalendarViewComponent implements OnInit {

  tasksSub: Subscription;
  userCtrl = new FormControl();
  entity = new FormControl();
  template = new FormControl();
  filteredUsers: Observable<any[]>;
  selectedUser;
  currentUser: User;
  eventClicked: boolean;
  private subscriptions: Subscription[] = [];
  searchUsers = this.userService.searchUsers.bind(this.userService);
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    themeSystem: 'bootstrap',
    headerToolbar: {center: 'dayGridMonth,dayGridWeek'}, // buttons for switching between views
    eventClick: (info) => this.handleEventClick(info.event),
    views: {
      dayGrid: {
        // options apply to dayGridMonth, dayGridWeek, and dayGridDay views
      },
      week: {
        // options apply to dayGridWeek and timeGridWeek views
      },
      day: {
        // options apply to dayGridDay and timeGridDay views
      }
    },
  };
  entityAutocompleteCall = this.contactsService.searchContacts.bind(this.contactsService);
  selectedEntity
  categoryId: null;
  categoryList = []
  private templateIds: any;
  private isFollowing = false;
  isMyTasks = true;
  @ViewChild('userAutocomplete') userAutocomplete: GeneralAutocompleteComponent;

  constructor(
    private templateService: TemplateService,
    private translate: TranslateService,
    private userService: UserService,
    private store: Store<AppState>,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CalendarViewComponent>,
    private categoryService: CategoryService,
    private taskService: TaskService,
    private contactsService: ContactsService,
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe((res) => {
      if (res) {
        this.currentUser = res;
        this.selectedUser = this.currentUser;
      }
    }));
    this.subscriptions.push(this.store.pipe(select(isLoggedOut)).subscribe((res) => {
      if (res) {
        this.dialogRef.close();
      }
    }));
    const categorySubscription = this.categoryService.getCategories().subscribe(res => {
      if (res)
        this.categoryList = res.data
    })
    this.subscriptions.push(categorySubscription)

    this.loadTasks();
  }

  loadTemplatesList(term) {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(term),
    );
    return this.templateService.findTemplates(queryParams);
  }

  filterConfiguration(term): any {
    const filter: any = {};
    filter.searchData = term;
    return filter;
  }

  displayTemplate(template) {
    let res = ''
    if (template) {
      res += template.name;
    }
    return res
  }

  onTemplateChange(val) {
    this.templateIds = val.id
    this.loadTasks();
  }

  displayTemplateSelected(template) {
    let res = ''
    if (template) {
      res += template.name;
    }
    return res;
  }


  loadTasks() {
    const _entityId = this.selectedEntity ? this.selectedEntity.id : ''
    this.taskService.getTasksV2('', false, '', '', '', _entityId,
      this.selectedUser?this.selectedUser.id:'', this.categoryId, this.templateIds, '', '', '', '', '',this.isFollowing).subscribe((res) => {
      if (res.data) {
        const calendarOptions = Object.assign({}, this.calendarOptions);
        calendarOptions.events = res.data.map(e => {
          const hours = Math.floor((new Date().getTime() - new Date(e.due_date).getTime()) / 36e5);
          return {
            eventId: e.id,
            title: e.title,
            date: e.due_date,
            start: e.due_date,
            end: e.due_date,
            task: e,
            backgroundColor: e.status !== 'DONE' && hours >= 72 ? 'red' : e.status === 'DONE' ? '#33cc33' : '#3788d8'
          }
        });
        this.calendarOptions = calendarOptions;
      }
      this.cdr.detectChanges();
    });
  }

  handleEventClick(event) {
    if (!this.eventClicked) {
      this.eventClicked = true;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.closeOnNavigation = true;
      dialogConfig.autoFocus = true;
      dialogConfig.width = '40vw';
      dialogConfig.data = {mode: 'update', task: event._def.extendedProps.task}
      const dialogRef = this.dialog.open(CreateTaskComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(() => {
        this.eventClicked = false;
        this.loadTasks();
      });
    }
  }

  userSelected(value): void {
    this.selectedUser = value;
    this.loadTasks();
  }

  fetchMyTasks() {
    this.isMyTasks = !this.isMyTasks;
    if (this.isMyTasks) {
      this.selectedUser = '' + this.currentUser.id;
      this.userAutocomplete.setValue(this.currentUser)
    } else {
      this.selectedUser = '';
      this.userAutocomplete.setValue(null)
    }    this.loadTasks();
  }

  fetchFollowingTasks() {
    this.isFollowing = !this.isFollowing;
    this.loadTasks()
  }

  radioGroupChange(event: MatRadioChange) {
    if (event.value === 'true') {
      this.selectedUser = this.currentUser;
      this.userCtrl.disable();
      this.loadTasks();
    } else {
      this.userCtrl.enable();
    }
    this.userCtrl.reset();
  }

  displayFn(val: User): string {
    if (!val)
      return '';
    let res = ''
    if (val.fullName) {
      res = val.fullName;
    }
    if (val.name) {
      res += ` <small class="text-muted">${val.name}</small>`
    }
    return res
  }

  displayFnSelected(val: User): string {
    if (!val)
      return '';
    let res = ''
    if (val.fullName) {
      res = val.fullName;
    }
    return res
  }

  close() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.dialogRef.close();
  }

  displayEntity(entity: any) {
    let res = ''
    if (!entity) {
      return res;
    }
    if (entity.first_name) {
      res = res + entity.first_name
    }
    if (entity.last_name) {
      res = res + ' ' + entity.last_name
    }
    if (entity && entity.dob) {
      res += '  ' + `<small class="text-muted">` + moment(entity.dob).format('l') + `</small>`
    }
    return res
  }

  displayEntitySelected(entity) {
    let res = ''
    if (!entity) {
      return res;
    }
    if (entity.first_name) {
      res = res + entity.first_name
    }
    if (entity.last_name) {
      res = res + ' ' + entity.last_name
    }
    return res
  }

  entitySelected(value): void {
    this.selectedEntity = value;
    this.loadTasks();
  }
}
