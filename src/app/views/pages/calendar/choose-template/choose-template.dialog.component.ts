import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import { CategoryService } from 'src/app/core/lists';
import { TemplateService } from 'src/app/core/lists/_services/template.service';
import { GeneralAutocompleteComponent } from 'src/app/views/partials/layout/general-autocomplete/general-autocomplete.component';
import { CreateTaskComponent } from '../create-task/create-task.component';

@Component({
  selector: 'kt-choose-template',
  templateUrl: 'choose-template.dialog.component.html',
})
export class ChooseTemplateDialogComponent implements OnInit {
  isFromTemplate = false;
  categoryInput: FormControl;
  templateInput: FormControl;
  categoryAutocompleteCall;
  templateAutocompleteCall;
  @ViewChild('categoryAutocomplete') categoryAutocomplete: GeneralAutocompleteComponent;
  @ViewChild('templateAutocomplete') templateAutocomplete: GeneralAutocompleteComponent;

  ngOnInit() {
    this.categoryInput = new FormControl();
    this.templateInput = new FormControl();
    this.categoryAutocompleteCall = this.categoryService.searchCategoriesWithTemplate.bind(this.categoryService);
    this.templateAutocompleteCall = this.templateService.searchTemplates.bind(
        this.templateService,
        this.categoryInput.value ? this.categoryInput.value.id : -1
    );
  }

  constructor(
    public dialogRef: MatDialogRef<ChooseTemplateDialogComponent>,
    private categoryService: CategoryService,
    private templateService: TemplateService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  filterCategoryFn(category: any): string {
    return category ? category.name : '';
  }

  onCategoryChange(val) {
    this.categoryInput.setValue(val);
    this.categoryInput.updateValueAndValidity();
    this.templateAutocompleteCall = this.templateService.searchTemplates.bind(
        this.templateService,
        this.categoryInput.value ? this.categoryInput.value.id : -1
    );
  }

  filterTemplateFn(template: any): string {
    return template ? template.name : '';
  }

  onTemplateChange(val) {
    this.templateInput.setValue(val);
    this.templateInput.updateValueAndValidity();
  }

  submit() {
    const action_queue_id = this.data && this.data.action_queue_id ? this.data.action_queue_id: null
    const workflow_queue_id = this.data && this.data.workflow_queue_id ? this.data.workflow_queue_id: null
    const project_id = this.data && this.data.project_id ? this.data.project_id: null
    const fromWorkflow = this.data && this.data.fromWorkflow ? this.data.fromWorkflow: null
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = this.isFromTemplate ? { mode: 'create', template: this.templateInput.value
      ,fromWorkflow ,action_queue_id,workflow_queue_id,project_id }
      : { mode: 'create', fromWorkflow,action_queue_id,workflow_queue_id,project_id  }
    const taskRef = this.dialog.open(CreateTaskComponent, dialogConfig);
    taskRef.afterClosed().subscribe(res => {
      this.dialogRef.close(res);
    })

  }

}
