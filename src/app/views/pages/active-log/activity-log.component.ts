import {Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime, distinctUntilChanged, tap, skip, take, delay} from 'rxjs/operators';
import {fromEvent, merge, of, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from './../../../core/reducers';
import {QueryParamsModel} from './../../../core/_base/crud';
import {Activity} from 'src/app/core/auth/_models/activity.model';
import {ActivityDataSource} from 'src/app/core/auth/_data-sources/activity.datasource';
import {ActivityPageRequested} from 'src/app/core/auth/_actions/activity.actions';
import {CompanyModel} from 'src/app/core/management/_models/company.model';
import {CompaniesService} from 'src/app/core/management/_services/companies.service';


@Component({
  selector: 'kt-activity-log',
  templateUrl: './activity-log.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivityLogComponent implements OnInit, OnDestroy {
  dataSource: ActivityDataSource;
  displayedColumns = ['log_name', 'causer', 'log_desc', 'log_at', 'subject_name'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild('searchInput', {static: true}) searchInput: ElementRef;
  lastQuery: QueryParamsModel;
  selection = new SelectionModel<Activity>(true, []);
  activityResult: Activity[] = [];
  allActivity: Activity[] = [];
  filterCompany = '';
  companies: CompanyModel[]
  private subscriptions: Subscription[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private company: CompaniesService,
  ) {
  }

  ngOnInit() {


    this.company.getAllCompanies().subscribe(res => {
      this.companies = res
    })
    // If the Activity changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
    this.subscriptions.push(sortSubscription);


    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => {
        this.loadActivityList();
      })
    )
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);


    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      // tslint:disable-next-line:max-line-length
      debounceTime(150), // The activity can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
      distinctUntilChanged(), // This operator will eliminate duplicate values
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadActivityList();
      })
    )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new ActivityDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(res => {
      this.activityResult = res;
    });
    this.subscriptions.push(entitiesSubscription);

    of(undefined).pipe(take(1), delay(1000)).subscribe(() => { // Remove this line, just loading imitation
      this.loadActivityList();
    });


  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(el => el.unsubscribe());
  }

  /**
   * Load activity list
   */
  loadActivityList() {

    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );

    this.store.dispatch(new ActivityPageRequested({page: queryParams}));
    this.selection.clear();
  }

  /** FILTRATION */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;
    filter.name = searchText;
    filter.searchData = searchText
    filter.email = searchText;
    filter.companyID = this.filterCompany
    return filter;
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.activityResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle selection
   */
  masterToggle() {
    if (this.selection.selected.length === this.activityResult.length) {
      this.selection.clear();
    } else {
      this.activityResult.forEach(row => this.selection.select(row));
    }
  }
}
