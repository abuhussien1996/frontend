import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../core/auth/_services';
import jwt_decode from 'jwt-decode';

@Component({
  selector: 'kt-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {

  extractToken: any;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private userService: UserService) {
    const activeSubscription = this.activatedRoute.queryParams.subscribe(params => {
      if (params.code) {
        const state = JSON.parse(params.state)
        this.userService.authenticateToNylas(params.code, state.provider, state.categoryId).subscribe((res) => {
          this.x(state);
        }, () => {
          this.x(state);
        })
        setTimeout(() => {
          if (activeSubscription && activeSubscription.unsubscribe) {
            activeSubscription.unsubscribe();
          }
        })

      }
    });

  }

  ngOnInit(): void {
  }

  private x(state) {
    if (state.listFlag === '/lists/category') {
      this.router.navigate(['/lists/category']);
      return;
    }
    if (state.listFlag === '/lists/team') {
      this.router.navigate(['/lists/team']);
      return;
    } else {
      this.extractToken = this.getDecodedAccessToken(localStorage.getItem('accessToken'));
      this.router.navigate(['user-management/users/edit/' + this.extractToken.user_id]);
    }
  }

  getDecodedAccessToken(token: any) {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
}
