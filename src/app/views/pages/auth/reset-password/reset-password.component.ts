// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';
// RxJS
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
import { AuthService } from 'src/app/core/auth/_services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LayoutUtilsService } from 'src/app/core/_base/crud';
import { AuthNoticeService } from 'src/app/core/auth';

export function ConfirmedValidator(controlName: string, matchingControlName: string){
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
            return;
        }
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ confirmedValidator: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
@Component({
	selector: 'kt-reset-password',
	templateUrl: './reset-password.component.html',
	encapsulation: ViewEncapsulation.None
})
export class ResetPasswordComponent implements OnInit {
	// Public properties

	hasFormErrors = false;
	resetPasswordForm: FormGroup;
	hide
	hide3
	hello :boolean
	loading
	/**
	 * Component constructor
	 *
	 * @param fb: FormBuilder
	 * @param auth: AuthService
	 * @param store: Store<AppState>
	 * @param layoutUtilsService: LayoutUtilsService
	 */
	constructor(private fb: FormBuilder,private translate: TranslateService,private router: Router,	private auth: AuthService,private snackBar:MatSnackBar,public authNoticeService: AuthNoticeService,
		private store: Store<AppState>,
		// tslint:disable-next-line:align
		private layoutUtilsService: LayoutUtilsService) {
	}

	
	ngOnInit() {
		this.resetPasswordForm = this.fb.group({
		
			password:  new FormControl('', Validators.compose([
				Validators.required,
				Validators.minLength(8),
				Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&#^])[A-Za-z\d$@$!%*?&#^].{8,}'),
				Validators.maxLength(100)
			])),
			confirmPassword:  new FormControl('', Validators.compose([
				Validators.required,
				
			])),
	
		
		
	}, { 
		validator: ConfirmedValidator('password', 'confirmPassword')
	  });	}

	

	

	
	createForm() {

		  
	}

	get f(){
		return this.resetPasswordForm.controls;
	  }

	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.resetPasswordForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
	/**
	 * Reset
	 */
	reset() {
		this.hasFormErrors = false;
		this.resetPasswordForm.markAsPristine();
		this.resetPasswordForm.markAsUntouched();
		this.resetPasswordForm.updateValueAndValidity();
	}




	submit() {
		this.hasFormErrors = false;
		const controls = this.resetPasswordForm.controls;
		/** check form */
		if (this.resetPasswordForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			this.hasFormErrors = true;

			return;
		}

		
		this.auth.resetPassword(controls.password.value).subscribe(res => {
			if(res){
				this.authNoticeService.setNotice(this.translate.instant('AUTH.FORGOT.SUCCESS'), 'success');
				console.log('reset successful', res);
				setTimeout(() => this.router.navigateByUrl('/auth/login'), 5000);

			}
 
			
		})
		
	
		

		this.reset();
	}
	/**
	 * Close alert
	 *
	 * @param _$event: Event
	 */
	onAlertClose(_$event) {
		this.hasFormErrors = false;
	}
	
}

