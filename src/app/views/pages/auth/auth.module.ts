// Angular
import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
// Material
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
// Translate
import {TranslateModule} from '@ngx-translate/core';
// NGRX
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
// CRUD
// Module components
import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {AuthNoticeComponent} from './auth-notice/auth-notice.component';
// Auth
import {AuthEffects, AuthGuard, authReducer, AuthService, UserEffects, usersReducer} from '../../../core/auth';
import {MatIconModule} from '@angular/material/icon';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {ErrorInterceptor} from 'src/app/core/auth/_helpers/error.interceptor';
import {AuthInterceptor} from './auth.interceptor';


const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {returnUrl: window.location.pathname, animation: 'login'}
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent,
        data: {animation: 'forget'}

      },
      {
        path: 'reset-password',
        component: ResetPasswordComponent,
      }
    ]
  }
];


@NgModule({
  imports: [
    CommonModule,
    // BrowserAnimationsModule,

    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    MatInputModule,
    MatIconModule,
    MatFormFieldModule,
    MatCheckboxModule,
    TranslateModule.forChild(),
    StoreModule.forFeature('auth', authReducer),
    EffectsModule.forFeature([AuthEffects]),
    StoreModule.forFeature('users', usersReducer),
    EffectsModule.forFeature([UserEffects]),
  ],
  providers: [
    // InterceptService,
    // { provide: HTTP_INTERCEPTORS, useClass: InterceptService, multi: true },
    // { provide: APP_INITIALIZER, useFactory: appInitializer, multi: true, deps: [AuthService] },
    // { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}


  ],
  exports: [AuthComponent],
  declarations: [
    AuthComponent,
    LoginComponent,
    ForgotPasswordComponent,
    AuthNoticeComponent,
    ResetPasswordComponent
  ]
})

export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        AuthGuard

      ]
    };
  }
}
