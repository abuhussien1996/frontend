// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';
// RxJS
import {Observable, Subject, Subscription} from 'rxjs';
import {finalize, takeUntil, tap} from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
// Auth
import {AuthNoticeService, AuthService} from '../../../../core/auth';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * ! Just example => Should be removed in development
 */


@Component({
	selector: 'kt-login',
	templateUrl: './login.component.html',
	encapsulation: ViewEncapsulation.None
})

export class LoginComponent implements OnInit, OnDestroy {
	submitted = false;
	// Public params
	notExistUser = false
	existUser = false
	loginForm: FormGroup;
	loading = false;
	isLoggedIn$: Observable<boolean>;
	errors: any = [];
	hide: any
	validEmail = false;
	validUsername = false;
	inputType: string;
	private unsubscribe: Subject<any>;

	returnUrl: any;

	// Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param router: Router
	 * @param auth: AuthService
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr: ChangeDetectorRef
	 * @param route: ActivatedRoute
	 */
	constructor(
		private router: Router,
		private auth: AuthService,
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef,
		private route: ActivatedRoute,
		private matIconRegistry: MatIconRegistry,
		private domSanitizer: DomSanitizer

	) {
		this.unsubscribe = new Subject();
		// redirect to home if already logged in
		if (this.auth.currentUserValue) {
			this.router.navigate(['/']);
		}

		this.matIconRegistry.addSvgIcon(
			`existuser`,
			this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/media/svg/icons/exist.svg')
		);
		this.matIconRegistry.addSvgIcon(
			`notexistuser`,
			this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/media/svg/icons/notexist.svg')
		);

		// LocalStorage.

	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.initLoginForm();
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params.returnUrl || '/';
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		this.authNoticeService.setNotice(null);
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	/**
	 * Form initalization
	 * Default params, validators
	 */
	initLoginForm() {

		this.loginForm = this.fb.group({
			email: new FormControl('', Validators.compose([
				Validators.required,

				Validators.minLength(4),
				Validators.maxLength(320)
			])),

			password: new FormControl('',Validators.required)

		}
		);
	}
	/**
	 * Form Submit
	 */


	submit() {
		const controls = this.loginForm.controls;
		/** check form */
		if (this.loginForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}

		this.loading = true;

		const authData = {
			email: controls.email.value,
			password: controls.password.value
		};
		this.auth
			.login(authData.email, authData.password)
			.pipe(
				tap(user => {
          if (user) {
            this.router.navigateByUrl(this.returnUrl);
          } else {
            this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
          }
				}),
				takeUntil(this.unsubscribe),
				finalize(() => {
					this.loading = false;
					this.cdr.markForCheck();
				})
			)
			.subscribe();

	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.loginForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	checkInputType(input: string) {

		if (input.includes('@')) {
			return 'email'
		} else {
			return 'username'
		}

	}

	checkUsernameOrEmail() {
		const controls = this.loginForm.controls;
		if ((controls.email.value).includes('@')) {
			this.inputType = 'email'
			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(controls.email.value)) {
				this.validEmail = false
			}
			else {
				this.validEmail = true
			}
		}
		else {
			this.inputType = 'username'
			if (/^[A-Za-z]+(?:[A-Za-z0-9]+)*$/.test(controls.email.value)) {
				this.validUsername = false
			}
			else {
				this.validUsername = true
			}
		}




	}

	checkExist() {
		const inputUser = this.loginForm.controls.email.value

		if (inputUser.length > 3) {
			this.auth.checkExist(inputUser).subscribe(res => {
				if (res[`code`] === 200) {
					this.existUser = true
					this.notExistUser = false
				}
				else {
					this.existUser = false
					this.notExistUser = true
				}
			})
		}
	}



}

export const validateEmailAndUsername: ValidatorFn = (loginForm: FormGroup): ValidationErrors | null => {
	const name = loginForm.get('email').value;

	let typeinput: string

	if (name.includes('@')) {
		typeinput = 'email'
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(name)) {
			return { validEmail: false }
		}
		else {
			return { validEmail: true }
		}
	}


};

