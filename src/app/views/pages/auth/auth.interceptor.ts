import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

import * as _ from 'lodash';
import {HttpUtilsService} from '../../../core/_base/crud';
import {AppState} from '../../../core/reducers';
import {currentAuthToken} from '../../../core/auth';
import {select, Store} from '@ngrx/store';
import {environment} from '../../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private authToken: any;

  constructor(private httpUtils: HttpUtilsService,
              private store: Store<AppState>) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res ? res : this.authToken);
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let req = request;
    if (request.url.indexOf('https://browser.ihtsdotools.org/snowstorm/snomed-ct/browser/MAIN/2020-07-31/descriptions') > -1 ||
      request.url.indexOf('X-Amz-Algorithm') > -1) {
      return next.handle(req);
    }
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    let headers = request.headers;
    (httpHeaders.keys() || []).forEach(key=>{
      headers = headers.set(key, httpHeaders.get(key));
    })
    request = request.clone({
      headers
    })
    return next.handle(request);
  }
}
