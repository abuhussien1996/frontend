import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'kt-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit, OnDestroy {

  // error type specified in navigation route 403, 404, 500
  errorType: string
	private sub: Subscription;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    /*this.errorType = this.route.snapshot.paramMap.get('type');
		this.sub = this.route.data.subscribe(param => {
			if (param.type) {
				this.errorType = param.type;
			}
      console.log(this.errorType);
    });
    console.log(this.errorType);*/
  }

	/**
	 * On destroy
	 */
	ngOnDestroy(): void {
		// this.sub.unsubscribe();
	}

}
