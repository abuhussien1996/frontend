import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '../../partials/partials.module';
import { CoreModule } from '../../../core/core.module';
import { RouterModule } from '@angular/router';
import { ErrorComponent } from './error.component';
import { Error403Component } from './error-403/error-403.component';
import { Error404Component } from './error-404/error-404.component';
import { Error500Component } from './error-500/error-500.component';

@NgModule({
  declarations: [
    ErrorComponent,
    Error403Component,
    Error404Component,
    Error500Component,
  ],
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    RouterModule.forChild([
      {
        path: '',
        component: ErrorComponent,
        children: [
          {
            path: '',
            redirectTo: '404',
            pathMatch: 'full'
          },
          {
            path: '403',
            component: Error403Component,
          },
          {
            path: '404',
            component: Error404Component,
          },
          {
            path: '500',
            component: Error500Component,
          },
          {
            path: '**',
            redirectTo: '404',
            pathMatch: 'full'
          },
        ],
      },
    ]),
  ],
})
export class ErrorModule {}
