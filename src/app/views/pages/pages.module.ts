import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PartialsModule } from '../partials/partials.module';
import { CoreModule } from '../../core/core.module';
import { CalendarModule } from './calendar/calendar.module';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {MatFormioModule} from 'angular-material-formio';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [],
  exports: [],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CoreModule,
    PartialsModule,
    CalendarModule,
    PerfectScrollbarModule,
    MatFormioModule,
    MatDialogModule,


  ],
  providers: []
})
export class PagesModule {
}
