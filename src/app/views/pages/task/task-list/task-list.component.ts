import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {MatAccordion} from '@angular/material/expansion';
import {TaskService} from '../../../../core/timeline/_services';
import {CreateTaskComponent} from '../../calendar/create-task/create-task.component';
import {LayoutUtilsService, QueryParamsModel} from '../../../../core/_base/crud';
import {CategoryService, ContactsService} from '../../../../core/lists/_services';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AuthService, UserService} from '../../../../core/auth/_services';
import {currentUser, User} from '../../../../core/auth';
import * as moment from 'formiojs/node_modules/moment';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../core/reducers';
import {GeneralAutocompleteComponent} from '../../../partials/layout/general-autocomplete/general-autocomplete.component';
import {take} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {CommentsDialogComponent} from '../comments-dialog/comments-dialog.component';
import {GlobalSettingsService} from '../../../../core/services/global-settings.service';
import {MatButton} from '@angular/material/button';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {TemplateService} from '../../../../core/lists/_services/template.service';
import {MatSort} from '@angular/material/sort';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {StringUtilsService} from '../../../../core/services/string-utils.service';
import {WorkflowService} from '../../workflow/_services/workflow.service';

@Component({
  selector: 'kt-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  @ViewChild('myTaskBtn') myTaskBtn: MatButton;
  @ViewChild('sort1', {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  searchForm: FormGroup;
  taskList = []
  subtaskList = []
  openTaskLength = 0
  completedTaskLength = 0
  loading = false
  subtaskLoading = false
  totalNumber = 0
  categoryId: null;
  priority: null;
  sortTask = 'created';
  sortDirection = 'desc';
  userIds = '';
  entityIds = '';
  parentId = '';
  size = 10;
  page = 0;
  isCompleted: any = '';
  entitiesIds = []
  usersIds = []
  entitiesObj = []
  entitiesGroupObj: {} = []
  usersObj = []
  categoryList = []
  progress = []
  done = []
  cancelled = []
  entityAutocompleteCall = this.contactService.searchContacts.bind(this.contactService);
  userAutocompleteCall = this.userService.searchUsers.bind(this.userService);
  loadProjectsList = this.projectService.searchProjects.bind(this.projectService);
  private subscriptions: Subscription[] = [];
  private user: any;
  @ViewChild('userAutocomplete') userAutocomplete: GeneralAutocompleteComponent;
  @Input() EntitiesFilteredBy
  @Output() totalCount = new EventEmitter<any>();
  @Input() inputTasks
  @Input() term
  cancelledTaskLength = 0;
  isCanceled: any;
  private usersChangedFirstTime = true;
  currentUser: User;
  private templateIds: any;
  private projectId: any;
  startDateCreated: FormControl;
  toDateCreated: FormControl;
  startDateDue: FormControl;
  toDateDue: FormControl;
  maxDateCreated: any;
  minDateCreated: any;
  maxDate: any;
  minDate: any;
  isFollowing = false;
  isMyTasks = true;

  ngOnChanges() {
  }

  constructor(private projectService: WorkflowService,
              private template: TemplateService,
              private fb: FormBuilder,
              private auth: AuthService,
              private store: Store<AppState>,
              private taskService: TaskService,
              private contactService: ContactsService,
              private userService: UserService,
              private globalSettingsService: GlobalSettingsService,
              private cdr: ChangeDetectorRef,
              public dialog: MatDialog,
              private layoutUtilsService: LayoutUtilsService,
              public stringUtils: StringUtilsService,
              private categoryService: CategoryService) {
    this.startDateCreated = new FormControl(null);
    this.toDateCreated = new FormControl(null);
    this.maxDateCreated = new Date();

    this.startDateDue = new FormControl(null);
    this.toDateDue = new FormControl(null);
    this.maxDate = new Date();

  }

  loadTasks(isCompleted, isCanceled, ignoreLoadCounts = false) {
    if (!ignoreLoadCounts) {
      this.loadTasksCounts();
    }
    this.loading = true
    this.taskList = []
    this.isCanceled = isCanceled;

    // tslint:disable-next-line:max-line-length
    const taskSubscription = this.taskService.getTasksV2(isCompleted, this.isCanceled, this.page, this.size, this.parentId,
      this.entityIds, this.userIds, this.categoryId, this.templateIds, this.priority, this.maxDateCreated,
      this.minDateCreated, this.maxDate, this.minDate, this.isFollowing, this.sortTask, this.sortDirection,this.projectId,this.term).subscribe(
      res => {
        this.taskList = res.data
        this.getAllEntitiesAndUsers(res.data)
        this.totalNumber = res.count
        this.loading = false
        this.isCompleted = isCompleted
        this.cdr.detectChanges()
      }
    )
    this.subscriptions.push(taskSubscription)
  }

  getAllEntitiesAndUsers(data) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      data[i].task_responsible_users.forEach(element => this.usersIds.push(element.responsible_id));
      data[i].task_entities.forEach(element => this.entitiesIds.push(element.entity_id));
    }
    if (this.entitiesIds.length > 0) {
      const entitiesSubscription = this.contactService.getContactsByIds(this.entitiesIds).subscribe(res => {
        this.entitiesObj = []
        this.entitiesGroupObj = []
        this.entitiesObj = res.data
        this.entitiesGroupObj = {CONTACT: this.entitiesObj};
        this.cdr.detectChanges()
      })
      this.subscriptions.push(entitiesSubscription)
    }
    if (this.usersIds.length > 0) {
      const userSubscription = this.taskService.getUsersbyId(this.usersIds).subscribe(res => {
          this.usersObj = []
          this.usersObj = res.pageOfItems
          this.cdr.detectChanges()
        }
      )
      this.subscriptions.push(userSubscription)
    }
  }

  loadSubtask(id) {
    this.subtaskLoading = true
    this.subtaskList = []
    const tasksSubscription = this.taskService.getTasksV2('', this.isCanceled, '', ''
      , id, '', '', '', '', '', '', '', '', 'id', false, 'created', 'asc').subscribe(
      res => {
        this.getAllEntitiesAndUsers(res.data)
        this.subtaskList = res.data
        this.subtaskLoading = false
        this.cdr.detectChanges()
      }
    )
    this.subscriptions.push(tasksSubscription)
  }

  ngAfterViewInit(): void {
    if (this.user && this.userAutocomplete) {
      this.userAutocomplete.setValue(this.user)
      this.usersChangedFirstTime = false;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(el => el.unsubscribe());
  }

  ngOnInit(): void {
    const userSubscription = this.store.pipe(select(currentUser), take(1)).subscribe(res => {
      this.user = res
      if (res.id && res.companyID) {
        this.userIds = res.id
        if(this.inputTasks){
          this.isMyTasks = false
          this.userIds = ''
        }
        this.loadTasks(this.isCompleted, this.isCanceled);
      }
    });
    this.subscriptions.push(userSubscription);

    this.globalSettingsService.$loadTaskList.subscribe(res => {
      if (res) {
        this.loadTasks(this.isCompleted, this.isCanceled)
      }
    })

    this.searchForm = this.fb.group({
      entity: [null],
      user: [null],
      template: [null],
      project: [null]
    });
    const categorySubscription = this.categoryService.getCategories().subscribe(res => {
      if (res)
        this.categoryList = res.data
    })
    this.subscriptions.push(categorySubscription)
  }

  loadTemplatesList(term) {
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(term),
    );
    return this.template.findTemplates(queryParams);
  }

  filterConfiguration(term): any {
    const filter: any = {};
    filter.searchData = term;
    return filter;
  }

  getPriority(priority) {
    if (priority === 'HIGH')
      return 'svg-icon svg-icon-danger svg-icon-xs'
    if (priority === 'MEDIUM')
      return 'svg-icon svg-icon-warning svg-icon-xs'
    return 'svg-icon svg-icon-xs'
  }

  getItemCssClassByStatus(status): string {
    switch (status) {
      case 'PLANNED':
        return 'warning';
      case 'IN_PROGRESS':
        return 'success';
      case 'DONE':
        return 'primary';
      case 'WAITING':
        return 'info';
      case 'ON_HOLD':
        return 'danger';
      case 'CANCELED':
        return 'Canceled';
    }
  }

  getItemTextByStatus(status): string {
    switch (status) {
      case 'PLANNED':
        return 'Planned';
      case 'IN_PROGRESS':
        return 'In Progress ';
      case 'DONE':
        return 'Done';
      case 'WAITING':
        return 'Waiting';
      case 'ON_HOLD':
        return 'On Hold';
      case 'CANCELED':
        return 'Canceled';
    }
  }

  updateCompleted(task) {
    if (task.progress !== 'DONE') {
      const _title = 'Update task'
      const _description = 'Are you sure you want to set the main task to Done ?'
      const _waitDesciption = 'Updating...';

      const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
      dialogRef.afterClosed().subscribe(res => {
        if (!res) {
          return;
        }
        task.progress = 'DONE'
        this.taskService.updateTask2(task).subscribe(result => {
          this.loadTasks(this.isCompleted, this.isCanceled)
        })
        return
      });
      return
    }
    const dialog = this.dialog.open(UpdateProgressComponent, {width: '250px'});
    dialog.afterClosed().subscribe(res => {
      if (!res) {
        return
      }
      task.progress = res
      const tasksSubscription = this.taskService.updateTask2(task).subscribe(result => {
        this.loadTasks(this.isCompleted, this.isCanceled);
      })
      this.subscriptions.push(tasksSubscription)
    })
  }

  checkDate(firstDate) {
    const today = new Date()
    firstDate = new Date(firstDate)
    if (firstDate.setHours(0, 0, 0, 0) - today.setHours(0, 0, 0, 0) >= 0) {
      return 'date'
    }
    return 'date bg-danger'
  }

  hasItem(items) {
    const length = items.length;
    if (!length || length === 0) {
      return {color: '#F7F7FA '}
    }
  }

  updateTask(task) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '40vw';
    dialogConfig.data = {mode: 'update', task}
    const dialogRef = this.dialog.open(CreateTaskComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      this.loadTasks(this.isCompleted, this.isCanceled);
    })
  }

  loadPage($event: PageEvent) {
    // if (this.size > this.totalNumber) {
    //   return
    // }
    this.page = $event.pageIndex;
    this.size = $event.pageSize;
    this.loadTasks(this.isCompleted, this.isCanceled, true);
  }

  getFullNameList(user) {
    const res = '';
    if (!user) {
      return res;
    } else {
      const u = this.usersObj.find(e => e.id === user.responsible_id)
      if (u) {
        return '' + u.userName
      }
      return res;
    }
  }

  mapArray(entities, type) {
    const res = [];
    if (!entities) {
      return res;
    } else {
      entities.forEach(item => {
        if (!this.entitiesGroupObj.hasOwnProperty(type))
          return
        const entity = this.entitiesGroupObj[type].find(e => e.id === item.entity_id)
        if (entity)
          res.push(entity)
      })
      return res;
    }
  }

  getTip(last, first) {
    let res = ''
    if (last) {
      res = res + last
    }
    if (first) {
      res = res + ' ' + first
    }
    return res
  }

  getNameList(users) {
    if (!users) {
      return '';
    } else {
      let res = '';
      users = users.slice(1)
      users.forEach((value) => {
        const u = this.usersObj.find(e => e.id === value.responsible_id)
        if (u) {
          res = res + '' + u.userName + '\n';
        }
      });
      return res;
    }
  }

  onEntityChange(val) {
    this.entityIds = val.id
    if (!val.id) {
      this.entityIds = ''
    }
    this.resetPagination();
    this.loadTasks(this.isCompleted, this.isCanceled);
  }

  displayUsers(user) {
    let res = ''
    if (user) {
      res += user.fullName;
    }
    if (user && user.name) {
      res += '  ' + `<small class="text-muted">` + user.name + `</small>`
    }
    return res
  }

  displayTemplate(template) {
    let res = ''
    if (template) {
      res += template.name;
    }
    return res
  }

  displayProject(template) {
    let res = ''
    if (template) {
      res += template.name;
    }
    return res
  }

  displayTemplateSelected(template) {
    let res = ''
    if (template) {
      res += template.name;
    }
    return res;
  }

  displayUsersSelected(user) {
    let res = ''
    if (user) {
      res += user.fullName;
    }
    return res;
  }

  onTemplateChange(val) {
    this.resetPagination();

    this.templateIds = val.id
    this.loadTasks(this.isCompleted, this.isCanceled);
  }

  onProjectChange(val) {
    this.resetPagination();
    this.projectId = val.id
    this.loadTasks(this.isCompleted, this.isCanceled);
  }

  onUsersChange(val) {
    this.resetPagination();

    if (this.usersChangedFirstTime) {
      this.usersChangedFirstTime = false;
      return;
    }
    if (!val || !val.id) {
      this.userIds = ''
      this.loadTasks(this.isCompleted, this.isCanceled);
      return;
    }
    if (val && val.id) {
      this.userIds = val.id
      this.loadTasks(this.isCompleted, this.isCanceled);
      return;
    }

  }

  getUser(user) {
    const u = this.usersObj.find(e => e.id === user.responsible_id)
    if (u) {
      return u
    }
    return new User();
  }

  getStyleNoTask() {
    return {height: '400px', 'padding-top': '12%'}
  }

  getStyleComponent() {
    if (this.inputTasks) {
      return {'box-shadow': 'unset', 'border-radius': '0px'}
    }
    return ''
  }

  showComments(comments: any, taskId) {

    comments.sort((a: any, b: any) => {
      // @ts-ignore
      return new Date(b.updated) - new Date(a.updated);

    })
    const dialog = this.dialog.open(CommentsDialogComponent, {
      data: {
        comments,
        taskId
      },

      width: '600px'
    });

  }

  private loadTasksCounts() {
    this.taskService.getTasksCounts(this.size, this.parentId, this.entityIds, this.userIds, this.categoryId
      ,  this.EntitiesFilteredBy, this.templateIds, this.priority, this.maxDateCreated,
      this.minDateCreated, this.maxDate, this.minDate, this.isFollowing,this.term).subscribe(res => {
      this.openTaskLength = res.data.open;
      this.completedTaskLength = res.data.completed;
      this.cancelledTaskLength = res.data.cancelled;
      this.totalCount.emit(this.openTaskLength + this.completedTaskLength + this.cancelledTaskLength)

    });
  }

  fetchMyTasks() {
    this.isMyTasks = !this.isMyTasks;
    if (this.isMyTasks) {
      this.userIds = '' + this.user.id;
      if(!this.inputTasks)
      this.userAutocomplete.setValue(this.user)
    } else {
      this.userIds = '';
      if(!this.inputTasks)
      this.userAutocomplete.setValue(null)
    }
    this.loadTasks(this.isCompleted, this.isCanceled)
  }

  fetchFollowingTasks() {
    this.isFollowing = !this.isFollowing;
    this.loadTasks(this.isCompleted, this.isCanceled)
  }

  clearDateFilter() {
    if (!this.maxDate && !this.minDate) {
      return
    }
    this.startDateDue.setValue(null);
    this.toDateDue.setValue(null);
    this.maxDate = null;
    this.minDate = null;
    this.resetPagination();
    this.loadTasks(this.isCompleted, this.isCanceled);
  }

  clearDateFilterCreated() {
    if (!this.maxDateCreated && !this.minDateCreated) {
      return
    }
    this.startDateCreated.setValue(null);
    this.toDateCreated.setValue(null);
    this.maxDateCreated = null;
    this.minDateCreated = null;
    this.resetPagination();
    this.loadTasks(this.isCompleted, this.isCanceled);
  }

  setMaxDateCreated($event: MatDatepickerInputEvent<any>) {
    this.maxDateCreated = $event.value;
    if (this.maxDateCreated && this.minDateCreated) {
      this.resetPagination();
      this.loadTasks(this.isCompleted, this.isCanceled);
    }
  }

  setMinDateCreated($event: MatDatepickerInputEvent<any>) {
    this.minDateCreated = $event.value;
    if (this.maxDateCreated && this.minDateCreated) {
      this.resetPagination();
      this.loadTasks(this.isCompleted, this.isCanceled);
    }
  }

  setMaxDate($event: MatDatepickerInputEvent<any>) {
    this.maxDate = $event.value;
    if (this.maxDate && this.minDate) {
      this.resetPagination();
      this.loadTasks(this.isCompleted, this.isCanceled);
    }
  }

  setMinDate($event: MatDatepickerInputEvent<any>) {
    this.minDate = $event.value;
    if (this.maxDate && this.minDate) {
      this.resetPagination();
      this.loadTasks(this.isCompleted, this.isCanceled);
    }
  }

  resetPagination() {
    this.page = 0;
    this.size = 10;
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
  }

  sortTasks(type, direction = 'desc') {
    this.sortTask = type;
    this.sortDirection = direction ? direction : 'asc'
    this.loadTasks(this.isCompleted, this.isCanceled)
  }

  displayEntitySelected(entity) {
    let res = ''
    if (!entity) {
      return res;
    }
    if (entity.first_name) {
      res = res + entity.first_name
    }
    if (entity.last_name) {
      res = res + ' ' + entity.last_name
    }
    return res
  }

  displayEntity(entity: any) {
    let res = ''
    if (!entity) {
      return res;
    }
    if (entity.first_name) {
      res = res + entity.first_name
    }
    if (entity.last_name) {
      res = res + ' ' + entity.last_name
    }
    if (entity && entity.dob) {
      res += '  ' + `<small class="text-muted">` + moment(entity.dob).format('l') + `</small>`
    }
    return res
  }

}

@Component({
  selector: 'kt-update-progress-dialog',
  templateUrl: 'update-progress-dialog.html',
})
export class UpdateProgressComponent {
  constructor(public dialogRef: MatDialogRef<UpdateProgressComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  updateStatus(val: string) {
    this.dialogRef.close(val);
  }
}
