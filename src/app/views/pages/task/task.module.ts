import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskListComponent, UpdateProgressComponent} from './task-list/task-list.component';
import {ModuleGuard} from '../../../core/auth';
import {RouterModule, Routes} from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {PartialsModule} from '../../partials/partials.module';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {NgxPermissionsModule} from 'ngx-permissions';
import {MatSelectModule} from "@angular/material/select";
import {CommentsDialogComponent} from './comments-dialog/comments-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatPaginatorModule} from "@angular/material/paginator";
import {ReactiveFormsModule} from "@angular/forms";


const routes: Routes = [
  {
    path: 'list',
    component: TaskListComponent,
    canActivate: [ModuleGuard],
    data: {permissionName: 'accessToTaskModule'},
  }
];

@NgModule({
    declarations: [TaskListComponent, UpdateProgressComponent, CommentsDialogComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatDatepickerModule,
        MatButtonModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        PartialsModule,
        MatButtonToggleModule,
        NgxPermissionsModule.forChild(),
        MatSelectModule,
        TranslateModule,
        MatDialogModule,
        MatPaginatorModule,
        ReactiveFormsModule,
    ],
    providers: [ModuleGuard],
    exports: [
        TaskListComponent
    ],
    entryComponents: [UpdateProgressComponent,CommentsDialogComponent]
})
export class TaskModule {
}
