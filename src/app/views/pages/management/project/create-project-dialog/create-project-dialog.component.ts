import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {JsonEditorComponent, JsonEditorOptions} from 'ang-jsoneditor';
import {MatDialogRef} from '@angular/material/dialog';
import {ProjectService} from '../../services/project.service';

@Component({
  selector: 'kt-create-project-dialog',
  templateUrl: './create-project-dialog.component.html',
  styleUrls: ['./create-project-dialog.component.scss']
})
export class CreateProjectDialogComponent implements OnInit {

  private isCreate = true;
  projectForm: FormGroup;
  viewLoading = false;
  public editorOptions: JsonEditorOptions;
  public data: any;
  @ViewChild(JsonEditorComponent, {static: false}) editor: JsonEditorComponent;

  constructor(private fb: FormBuilder,private projectService: ProjectService,
              private dialogRef: MatDialogRef<CreateProjectDialogComponent>) {
    this.editorOptions = new JsonEditorOptions()
    this.editorOptions.mode = 'code'; // set all allowed modes
    this.editorOptions.statusBar = false; // set all allowed modes
    this.projectForm = this.fb.group({
      name: ['', Validators.required],
      schema: [this.data,[Validators.required]]
    });
  }

  ngOnInit(): void {

  }

  getTitle() {
    return this.isCreate ? 'Create Project' : 'Update Project';
  }

  onSubmit() {
    if (!this.projectForm.valid || !this.editor.isValidJson()) {
      this.projectForm.markAsTouched();
      this.projectForm.markAllAsTouched();
      return;
    }

    if (this.isCreate) {
      const body = this.getEntityBody();
      this.projectService.createProjectSchema(body).subscribe(res => {
        this.dialogRef.close({reload: true});
      });
    }
  }

  private getEntityBody() {
    const body = this.projectForm.value;
    body.schema = JSON.stringify(body.schema);
    return body;
  }
}
