import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BaseDataSource, QueryParamsModel} from '../../../../../core/_base/crud';
import {MatDialog} from '@angular/material/dialog';
import {of} from 'rxjs/internal/observable/of';
import {CreateProjectDialogComponent} from '../create-project-dialog/create-project-dialog.component';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {merge, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ProjectService} from '../../services/project.service';

@Component({
  selector: 'kt-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  dataSource = new BaseDataSource();
  displayedColumns = ['id', 'name'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort', {static: true}) sort: MatSort;
  private subscriptions: Subscription[] = [];

  constructor(private dialog: MatDialog,
              private projectService: ProjectService) {
  }

  ngOnInit(): void {
    this.loadProjects();
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => {
        this.loadProjects();
      })
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);
  }

  addProject() {
    this.dialog.open(CreateProjectDialogComponent, {}).afterClosed().subscribe(res => {
      if (res.reload) {
        this.loadProjects();
      }
    })

  }

  private loadProjects() {
    this.dataSource.loading$ = of(true);
    this.projectService.getProjects(new QueryParamsModel({}, undefined, undefined,
      this.paginator.pageIndex, this.paginator.pageSize)).subscribe(res => {
      this.dataSource.loading$ = of(false);
      this.dataSource.isPreloadTextViewed$ = of(false);
      this.dataSource.entitySubject.next(res.data);
      this.dataSource.paginatorTotalSubject.next(res.count);
      this.dataSource.hasItems = res.count > 0;
    })

  }


  ngOnDestroy(): void {
    this.subscriptions.forEach(el => el.unsubscribe());
  }
}
