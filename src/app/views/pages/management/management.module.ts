// Angular
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
// Fake API Angular-in-memory
// Translate Module
import {TranslateModule} from '@ngx-translate/core';
// NGRX
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
// UI
import {PartialsModule} from '../../partials/partials.module';
// Auth
import {ModuleGuard} from '../../../core/auth';
// Core => Services
import {
  companiesReducer,
  CompaniesService,
  CompanyEffects,
  OfficeEffects,
  officesReducer,
  OfficesService,
} from '../../../core/management';
// Core => Utils
import {HttpUtilsService, InterceptService, LayoutUtilsService, TypesUtilsService} from '../../../core/_base/crud';
// Shared
import {
  ActionNotificationComponent,
  ConfirmActionDialogComponent,
  DeleteEntityDialogComponent,
  FetchEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../../partials/content/crud';
// Components
import {ManagementComponent} from './management.component';
// Offices
import {OfficesListComponent} from './offices/offices-list/offices-list.component';
import {OfficeEditDialogComponent} from './offices/office-edit/office-edit.dialog.component';
import {OfficeViewDialogComponent} from './offices/office-view/office-view.dialog.component';
// Companies
import {CompaniesListComponent} from './companies/companies-list/companies-list.component';
import {CompanyEditDialogComponent} from './companies/company-edit/company-edit.dialog.component';
import {CompanyViewDialogComponent} from './companies/company-view/company-view.dialog.component';
// Material
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatSelectModule} from '@angular/material/select';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPermissionsModule} from 'ngx-permissions';
import {SharedModule} from 'src/app/shared.module';

import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {InlineSVGModule} from 'ng-inline-svg';
import { EntityListComponent } from './entity/entity-list/entity-list.component';
import { CreateEntityDialogComponent } from './entity/create-entity-dialog/create-entity-dialog.component';
import {NgJsonEditorModule} from "ang-jsoneditor";
import { ProjectListComponent } from './project/project-list/project-list.component';
import { CreateProjectDialogComponent } from './project/create-project-dialog/create-project-dialog.component';

// tslint:disable-next-line:class-name
const routes: Routes = [
	{
		path: '',
		component: ManagementComponent,
		children: [
			{
				path: '',
				redirectTo: 'offices',
				pathMatch: 'full'
			},
			{
				path: 'offices',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToOfficeModule'},
				component: OfficesListComponent
			},
			{
				path: 'companies',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToCompanyModule'},
				component: CompaniesListComponent
			},
			{
				path: 'entity',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToCompanyModule'},
				component: EntityListComponent
			},
			{
				path: 'project',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToCompanyModule'},
				component: ProjectListComponent
			}
		]
	}
];

@NgModule({
  imports: [
    MatDialogModule,
    CommonModule,
    HttpClientModule,
    PartialsModule,
    NgxPermissionsModule.forChild(),
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    MatButtonModule,
    MatMenuModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    NgbProgressbarModule,
    SharedModule,
    PerfectScrollbarModule,
    StoreModule.forFeature('offices', officesReducer),
    EffectsModule.forFeature([OfficeEffects]),
    StoreModule.forFeature('companies', companiesReducer),
    EffectsModule.forFeature([CompanyEffects]),
    InlineSVGModule,
    NgJsonEditorModule
  ],
	providers: [
		ModuleGuard,
		InterceptService,
      	{
        	provide: HTTP_INTERCEPTORS,
       	 	useClass: InterceptService,
        	multi: true
      	},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'mat-dialog-container-wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		TypesUtilsService,
		LayoutUtilsService,
		HttpUtilsService,
		OfficesService,
		CompaniesService,
		TypesUtilsService,
		LayoutUtilsService,
		DatePipe,
	],
	entryComponents: [
		ActionNotificationComponent,
		OfficeEditDialogComponent,
		OfficeViewDialogComponent,
		CompanyEditDialogComponent,
		CompanyViewDialogComponent,
		ConfirmActionDialogComponent,
		DeleteEntityDialogComponent,
		FetchEntityDialogComponent,
		UpdateStatusDialogComponent,
		CreateEntityDialogComponent,
		CreateProjectDialogComponent,
	],
	declarations: [
		ManagementComponent,
		// Offices
		OfficesListComponent,
		OfficeEditDialogComponent,
		OfficeViewDialogComponent,
		// Companies
		CompaniesListComponent,
		CompanyEditDialogComponent,
		CompanyViewDialogComponent,
		EntityListComponent,
		CreateEntityDialogComponent,
		ProjectListComponent,
		CreateProjectDialogComponent,
    // Links
	]
})
export class ManagementModule { }
