import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpUtilsService, QueryParamsModel} from '../../../../core/_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../core/reducers';
import {currentAuthToken, currentUser, User} from '../../../../core/auth';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public currentUser: Observable<User>;
  private authToken;
  private companyId: any;
  private baseUrl = environment.USER_BASE;

  constructor(
    private httpClient: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);

  }

  createProjectSchema(body: any): Observable<any> {
    return this.httpClient.post(`${this.baseUrl}/project_schemas`, body);
  }

  createProject(body: any): Observable<any> {
    return this.httpClient.post(environment.CORE_BASE + '/companies/' + this.companyId + '/projects', body);
  }

  updateProject(body: any): Observable<any> {
    return this.httpClient.put(environment.CORE_BASE + '/companies/' + this.companyId + '/projects/'+body.id, body);
  }

  getProjects(queryParams: QueryParamsModel | any): Observable<any> {
    const params = this.getFindHTTPParams(queryParams);
    return this.httpClient.get(`${this.baseUrl}/project_schemas`, {params});
  }

  getProjectforAdmin(queryParams: QueryParamsModel | any): Observable<any> {

    return this.httpClient.get(`${environment.CORE_BASE}/companies/${this.companyId}/projects?page=${queryParams.pageNumber}&size=${queryParams.pageSize}`, );
  }

  private getFindHTTPParams(queryParams: any) {
    if(queryParams){
      queryParams = {}
    }
    return new HttpParams()
      // .set('sort', queryParams.sortField+','+queryParams.sortOrder)
      // .set('field', queryParams.sortField)
      .set('page', (queryParams.pageNumber||'').toString())
      .set('size', (queryParams.pageSize||'').toString());
  }
  getProjectsByIds(projectIds: any[]) {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    let ids = ''
    projectIds = _.uniqWith(projectIds, _.isEqual);
    projectIds.forEach(element => ids = ids + '&ids=' + element)
    const params = new HttpParams({fromString: ids});
    return this.httpClient.get<any>(environment.CORE_BASE + '/companies/' + this.companyId + '/projects', {
      headers,
      params
    });
  }
  deleteProject(project){
    return this.httpClient.delete<any>(environment.CORE_BASE + '/companies/' + this.companyId + '/projects/' + project.id);
  }
}
