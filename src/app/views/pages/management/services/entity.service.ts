import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {currentAuthToken, currentUser, User} from '../../../../core/auth';
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpUtilsService, QueryParamsModel} from '../../../../core/_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../../../core/reducers';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntityService {
  public currentUser: Observable<User>;
  private authToken;
  private companyId: any;
  private baseUrl = environment.USER_BASE;

  constructor(
    private httpClient: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);

  }

  createEntity(body: any): Observable<any> {
    return this.httpClient.post(`${this.baseUrl}/entity_schemas`, body);
  }

  getEntities(queryParams: QueryParamsModel | any): Observable<any> {
    const params = this.getFindHTTPParams(queryParams);
    return this.httpClient.get(`${this.baseUrl}/entity_schemas`, {params});
  }

  private getFindHTTPParams(queryParams: any) {
    if(queryParams){
      queryParams = {}
    }
    return new HttpParams()
      // .set('sort', queryParams.sortField+','+queryParams.sortOrder)
      // .set('field', queryParams.sortField)
      .set('page', (queryParams.pageNumber||'').toString())
      .set('size', (queryParams.pageSize||'').toString());
  }

  getEntitiesBY(): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}/companies/${this.companyId}/company_settings`);
  }
}
