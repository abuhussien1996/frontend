// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, ViewEncapsulation, OnDestroy } from '@angular/core';
// Material
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Subscription } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
// Services and Models
import { CompanyModel,
	selectCompaniesActionLoading,
} from '../../../../../core/management';
import { TranslateService } from '@ngx-translate/core';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-company-view-dialog',
	templateUrl: './company-view.dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class CompanyViewDialogComponent implements OnInit, OnDestroy {
	// Public properties
	company: CompanyModel;
	admin: string;
	hasFormErrors = false;
	viewLoading = false;
	// Private properties
	private componentSubscriptions: Subscription;

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<CompanyEditDialogComponent>
	 * @param data: any
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 */
	constructor(public dialogRef: MatDialogRef<CompanyViewDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private store: Store<AppState>,
		private translate: TranslateService) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.store.pipe(select(selectCompaniesActionLoading)).subscribe(res => this.viewLoading = res);
		this.company = this.data.company;
		this.admin = this.data.admin
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}

	/**
	 * Retursn CSS Class Name by status
	 *
	 * @param isActive: boolean
	 */
	getItemCssClassByStatus(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
	}

	/**
	 * Returns Item Status in string
	 * @param isActive: boolean
	 */
	getItemStatusString(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return this.translate.instant('GLOBAL.INACTIVE');
			case true:
				return this.translate.instant('GLOBAL.ACTIVE');
		}
	}

	/**
	 * Returns Item Status in string
	 * @param measurementSystem: string
	 */
	getMeasurementSystemString(measurementSystem: string): string {
		switch (measurementSystem) {
			case 'i':
				return this.translate.instant('COMPANY.IMPERIAL');
			case 'm':
				return this.translate.instant('COMPANY.METRIC');
			default:
				return '';
		}
	}
}
