// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip } from 'rxjs/operators';
import { fromEvent, merge, Subscription } from 'rxjs';
// Translate Module
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../core/reducers';
// CRUD
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../../core/_base/crud';
// Services and Models
import {
  CompanyModel,
  CompaniesDataSource,
  CompaniesPageRequested,
  CompaniesService,
  OneCompanyOnServerDeleted,
  selectCompanyError,
  selectCompanyLatestSuccessfullAction,
  ManyCompaniesOnServerDeleted,
  CompanyStatusOnServerUpdated,
  CompaniesStatusOnServerUpdated, CompanyActionTypes
} from '../../../../../core/management';
// Components
import { CompanyEditDialogComponent } from '../company-edit/company-edit.dialog.component';
import { CompanyViewDialogComponent } from '../company-view/company-view.dialog.component';
import { AuthService } from 'src/app/core/auth';

@Component({
	selector: 'kt-companies-list',
	templateUrl: './companies-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,

})
export class CompaniesListComponent implements OnInit, OnDestroy {
	// Table fields
	dataSource: CompaniesDataSource;
	displayedColumns = ['select', 'name', 'admin', 'isActive', 'actions'];
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild('sort1', {static: true}) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	filterStatus = '';
	filterType = '';
	users :any[];
	// Selection
	selection = new SelectionModel<CompanyModel>(true, []);
	companiesResult: CompanyModel[] = [];
	// Subscriptions
	private subscriptions: Subscription[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialog: MatDialog
	 * @param snackBar: MatSnackBar
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param translate: TranslateService
	 * @param companiesService: CompaniesService
	 * @param store: Store<AppState>
	 * @param cdr: ChangeDetectorRef
	 * @param authService: AuthService
	 */
	constructor(
		public dialog: MatDialog,
		public snackBar: MatSnackBar,
		private layoutUtilsService: LayoutUtilsService,
		private companiesService: CompaniesService,
		private translate: TranslateService,
		private store: Store<AppState>,
		private cdr: ChangeDetectorRef,
		private authService: AuthService
	) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		// load Users list for create/edit dialog
		this.loadUsersList();
		// If the user changes the sort order, reset back to the first page.
    this.subscriptions.push(this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0)));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
    this.subscriptions.push(merge(this.sort.sortChange, this.paginator.page).pipe(
			tap(() => this.loadCompaniesList())
		).subscribe());

		// Filtration, bind to searchInput
    this.subscriptions.push(fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
			// tslint:disable-next-line:max-line-length
			debounceTime(50), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
			distinctUntilChanged(), // This operator will eliminate duplicate values
			tap(() => {
				this.paginator.pageIndex = 0;
				this.loadCompaniesList();
			})
		).subscribe());

		// Init DataSource
		this.dataSource = new CompaniesDataSource(this.store);
    this.subscriptions.push(this.dataSource.entitySubject.pipe(
			skip(1),
			distinctUntilChanged()
		).subscribe(res => {
			this.companiesResult = res;
		}));
		// First load
		this.loadCompaniesList();
    this.subscriptions.push(this.store.pipe(select(selectCompanyError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Update,
          3000,
          true,
          false
        );
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectCompanyLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch (res.action_type) {
          case CompanyActionTypes.CompanyCreated:
            this.loadCompaniesList();
            successMsg = this.translate.instant('COMPANY.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case CompanyActionTypes.CompanyUpdated:
            this.loadCompaniesList();
            successMsg = this.translate.instant('COMPANY.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case CompanyActionTypes.CompanyStatusUpdated:
            successMsg = this.translate.instant('COMPANY.UPDATE_STATUS_SIMPLE.MESSAGE');
            msgType = MessageType.Update;
            break;
          case CompanyActionTypes.CompaniesStatusUpdated:
            this.selection.clear();
            successMsg = this.translate.instant('COMPANY.UPDATE_STATUS_MULTI.MESSAGE');
            msgType = MessageType.Update;
            break;
          case CompanyActionTypes.ManyCompaniesDeleted:
            this.selection.clear();
            successMsg = this.translate.instant('COMPANY.DELETE_COMPANY_MULTI.MESSAGE');
            msgType = MessageType.Delete;
            break;
          case CompanyActionTypes.OneCompanyDeleted:
            successMsg = this.translate.instant('COMPANY.DELETE_COMPANY_SIMPLE.MESSAGE');
            msgType = MessageType.Delete;
            break;
        }
        if (successMsg) {
          this.layoutUtilsService.showActionNotification(
            successMsg,
            msgType,
            3000,
            true,
            false
          );
        }
      }
    }));
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	/**
	 * Load Companies List from service through data-source
	 */
	loadCompaniesList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		// Call request from server
		this.store.dispatch(new CompaniesPageRequested({ page: queryParams, id: 0 }));
		this.selection.clear();
	}

	loadUsersList() {
		this.authService.getAllUsers().subscribe(res => this.users = res);
	}

	/**
	 * Returns object for filter
	 */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter.status = this.filterStatus;
		filter.text = searchText ? searchText : '';

		if (!searchText) {
			return filter;
        }

		filter.name = searchText;
		filter.company = searchText;
		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete company
	 *
	 * @param _item: CompanyModel
	 */
	deleteCompany(_item: CompanyModel) {
		const _title: string = this.translate.instant('COMPANY.DELETE_COMPANY_SIMPLE.TITLE');
		const _description: string = this.translate.instant('COMPANY.DELETE_COMPANY_SIMPLE.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('COMPANY.DELETE_COMPANY_SIMPLE.WAIT_DESCRIPTION');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.store.dispatch(new OneCompanyOnServerDeleted({ id: _item.id }));
		});
	}

	/**
	 * Delete selected companies
	 */
	deleteCompanies() {
		const _title: string = this.translate.instant('COMPANY.DELETE_COMPANY_MULTI.TITLE');
		const _description: string = this.translate.instant('COMPANY.DELETE_COMPANY_MULTI.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('COMPANY.DELETE_COMPANY_MULTI.WAIT_DESCRIPTION');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			const idsForDeletion: number[] = [];
			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < this.selection.selected.length; i++) {
				idsForDeletion.push(this.selection.selected[i].id);
			}
			this.store.dispatch(new ManyCompaniesOnServerDeleted({ ids: idsForDeletion }));
		});
	}

	/**
	 * Fetch selected companies
	 */
	fetchCompanies() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.name}`,
				id: elem.id.toString(),
				status: elem.isActive
			});
		});
		this.layoutUtilsService.fetchElements(messages);
	}

	/**
	 * Update Status for single company
	 */
	updateStatusForCompany(company: CompanyModel) {
		const _title: string = this.translate.instant('COMPANY.UPDATE_STATUS_SIMPLE.TITLE');
		const _description: string = this.translate.instant('COMPANY.UPDATE_STATUS_SIMPLE.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('COMPANY.UPDATE_STATUS_SIMPLE.WAIT_DESCRIPTION');
		const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.store.dispatch(new CompanyStatusOnServerUpdated({
					id: company.id,
					company,
					isActive: !company.isActive
				}));
		});
	}

	/**
	 * Show UpdateStatuDialog for selected companies
	 */
	updateStatusForCompanies() {
		const _title = this.translate.instant('COMPANY.UPDATE_STATUS_MULTI.TITLE');
		const _updateMessage = this.translate.instant('COMPANY.UPDATE_STATUS_MULTI.MESSAGE');
		const _statuses = [{ value: false, text: this.translate.instant('GLOBAL.INACTIVE') }, { value: true, text: this.translate.instant('GLOBAL.ACTIVE') }];
		const _messages = [];

		this.selection.selected.forEach(elem => {
			_messages.push({
				text: `${elem.name}`,
				id: elem.id.toString(),
				status: elem.isActive,
				statusTitle: this.getItemStatusString(elem.isActive),
				statusCssClass: this.getItemCssClassByStatus(elem.isActive)
			});
		});

		const dialogRef = this.layoutUtilsService.updateStatusForEntities(_title, _statuses, _messages);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				this.selection.clear();
				return;
			}

			const ids = [];
			this.selection.selected.forEach(elem => {
				ids.push(elem.id);
			});

			this.store.dispatch(new CompaniesStatusOnServerUpdated({
				isActive:  res === 'true' ? true : false,
				ids,
				companies: this.selection.selected
			}));
			this.selection.clear();
		});
	}

	/**
	 * Show add company dialog
	 */
	addCompany() {
		const newCompany = new CompanyModel();
		newCompany.clear(); // Set all defaults fields
		this.updateCompany(newCompany);
	}

	editCompany(company: CompanyModel) {
		this.companiesService.getCompanyById(company.id).subscribe(res => this.updateCompany(res))
	}

	/**
	 * Show Edit company dialog and save after success close result
	 * @param company: CompanyModel
	 */
	updateCompany(company: CompanyModel) {
		this.dialog.open(CompanyEditDialogComponent, { data: { company } });
	}

	/**
	 * Show View company dialog
	 * @param company: CompanyModel
	 */
	viewCompany(company: CompanyModel) {
		this.companiesService.getCompanyById(company.id).subscribe(res => {
			this.dialog.open(CompanyViewDialogComponent, { data: { company: res, admin: this.getAdminName(company.adminID) } });
		});
	}

	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.companiesResult.length;
		return numSelected === numRows;
	}

	/**
	 * Toggle all selections
	 */
	masterToggle() {
		if (this.selection.selected.length === this.companiesResult.length) {
			this.selection.clear();
		} else {
			this.companiesResult.forEach(row => this.selection.select(row));
		}
	}

	/** UI */
	/**
	 * Retursn CSS Class Name by status
	 *
	 * @param isActive: boolean
	 */
	getItemCssClassByStatus(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
	}

	/**
	 * Returns Item Status in string
	 * @param isActive: boolean
	 */
	getItemStatusString(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return this.translate.instant('GLOBAL.INACTIVE');
			case true:
				return this.translate.instant('GLOBAL.ACTIVE');
		}
	}

	/**
	 * Returns Item Status in string
	 * @param adminID: number
	 */
	getAdminName(adminID: number = 0): string {
		if(!this.users) {
			return;
		}
		const admin = this.users.find(user => user.id === adminID);
		return admin !== null && admin !== undefined ? admin.name : this.translate.instant('GLOBAL.NOT_ASSIGNED');;
	}
}
