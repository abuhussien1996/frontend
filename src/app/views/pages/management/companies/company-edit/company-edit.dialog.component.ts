// Angular
import {
  Component,
  OnInit,
  Inject,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  OnDestroy,
  ChangeDetectorRef, ViewChild, AfterViewInit
} from '@angular/core';
import {FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl} from '@angular/forms';
// Material
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
// RxJS
import {Subscription} from 'rxjs';
// NGRX
import {Update} from '@ngrx/entity';
import {Store, select} from '@ngrx/store';
// State
import {AppState} from '../../../../../core/reducers';
// Services and Models
import {
  CompaniesService,
  CompanyActionTypes, CompanyModel,
  CompanyOnServerCreated,
  CompanyOnServerUpdated,
  selectCompaniesActionLoading,
  selectCompanyError,
  selectCompanyLatestSuccessfullAction
} from '../../../../../core/management';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {skip} from 'rxjs/operators';
import {ProjectService} from '../../services/project.service';
import {EntityService} from '../../services/entity.service';
import {GeneralAutocompleteComponent} from "../../../../partials/layout/general-autocomplete/general-autocomplete.component";

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'kt-company-edit-dialog',
  templateUrl: './company-edit.dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class CompanyEditDialogComponent implements OnInit, OnDestroy, AfterViewInit {

  // Public properties
  company: CompanyModel;
  companyForm: FormGroup;
  hasFormErrors = false;
  hasSubmissionErrors = false;
  submissionErrorObj: any;
  viewLoading = false;
  // Private properties
  private subscriptions: Subscription[] = [];
  loadEntityFunction;
  loadProjectFunction;

  @ViewChild('projectAutocomplete') projectAutocomplete: GeneralAutocompleteComponent
  @ViewChild('entityAutocomplete') entityAutocomplete: GeneralAutocompleteComponent;
  entityFormatter = (val) => {
    return val.name
  };

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<CompanyEditDialogComponent>
   * @param data: any
   * @param fb: FormBuilder
   * @param datePipe: DatePipe
   * @param store: Store<AppState>
   * @param translate: TranslateService
   * @param cdr: ChangeDetectorRef
   */
  constructor(
    public dialogRef: MatDialogRef<CompanyEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private store: Store<AppState>,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private projectService: ProjectService,
    private entityService: EntityService,
    private companiesService: CompaniesService,
  ) {
    this.loadProjectFunction = this.projectService.getProjects.bind(this.projectService);
    this.loadEntityFunction = this.entityService.getEntities.bind(this.entityService);
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    this.subscriptions.push(this.store.pipe(select(selectCompaniesActionLoading)).subscribe(res => this.viewLoading = res));
    this.company = this.data.company.id !== 0 ? this.data.company : new CompanyModel();
    this.createForm();
    this.subscriptions.push(this.store.pipe(select(selectCompanyError), skip(1)).subscribe((error) => {
      if (error) {
        if (error.error.code) {
          this.submissionErrorObj = error.error.errors;
        } else {
          this.submissionErrorObj = [{message: 'GLOBAL.UNSPECIFIED_ERROR'}]
        }
        this.hasSubmissionErrors = true;
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectCompanyLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        if (res.action_type === CompanyActionTypes.CompanyCreated || res.action_type === CompanyActionTypes.CompanyUpdated) {
          this.submissionErrorObj = null;
          this.hasSubmissionErrors = false;
          this.dialogRef.close();
        }
      }
    }));
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  createForm() {
    this.companyForm = this.fb.group({
      name: [this.company.name, Validators.required],
      limitNumberOffices: [this.company.limitNumberOffices, Validators.compose([
        Validators.required,
        this.limitValidator('offices')
      ])
      ],
      limitNumberUsers: [this.company.limitNumberUsers, Validators.compose([
        Validators.required,
        this.limitValidator('users')
      ])
      ],
      createdAt: [{value: this.datePipe.transform(this.company.createdAt, 'long'), disabled: true}],
      updatedAt: [{value: this.datePipe.transform(this.company.updatedAt, 'long'), disabled: true}],
      isActive: [this.company.isActive],
      measurementSystem: [this.company.measurementSystem],
      entity: ['', Validators.required],
      project: ['', Validators.required],
    });
  }

  /**
   * Returns page title
   */
  getTitle(): string {
    if (this.company.id > 0) {
      return 'COMPANY.EDIT_COMPANY';
    }

    return 'COMPANY.NEW_COMPANY';
  }

  /**
   * Check control is invalid
   * @param controlName: string
   */
  isControlInvalid(controlName: string): boolean {
    const control = this.companyForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  /** ACTIONS */

  /**
   * Returns prepared company
   */
  prepareCompany(): CompanyModel {
    const controls: any = this.companyForm.controls;
    const _company = new CompanyModel();
    _company.id = this.company.id;
    _company.name = controls.name.value;
    _company.limitNumberOffices = controls.limitNumberOffices.value;
    _company.limitNumberUsers = controls.limitNumberUsers.value;
    _company.isActive = controls.isActive.value;
    _company.measurementSystem = controls.measurementSystem.value;
    _company.project_schema = controls.project.value;
    _company.entity_schema = controls.entity.value;
    return _company;
  }

  /**
   * On Submit
   */
  onSubmit() {
    this.hasFormErrors = false;
    const controls = this.companyForm.controls;
    /** check form */
    if (this.companyForm.invalid) {
      this.entityAutocomplete.updateValueAndValidity();
      this.projectAutocomplete.updateValueAndValidity();
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );

      this.hasFormErrors = true;
      return;
    }

    const editedCompany = this.prepareCompany();
    console.log("saved")
    if (editedCompany.id > 0) {
      this.updateCompany(editedCompany);
    } else {
      this.createCompany(editedCompany);
    }
  }

  /**
   * Update company
   *
   * @param _company: CompanyModel
   */
  updateCompany(_company: CompanyModel) {
    const updateCompany: Update<CompanyModel> = {
      id: _company.id,
      changes: _company
    };
    this.store.dispatch(new CompanyOnServerUpdated({
      partialCompany: updateCompany,
      company: _company
    }));
  }

  /**
   * Create company
   *
   * @param _company: CompanyModel
   */
  createCompany(_company: CompanyModel) {
    this.store.dispatch(new CompanyOnServerCreated({company: _company}));
  }

  /** Alect Close event */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }

  /**
   * Rerurns status string
   */
  getStatusString(): string {
    switch (this.companyForm.get('isActive').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }

  limitValidator(parameter: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      switch (parameter) {
        case 'offices' :
          return control.value < this.company.numberOffices ? {officeLimitError: true} : null;
        case 'users' :
          return control.value < this.company.numberUsers ? {userLimitError: true} : null;
      }
      return null;
    };
  }

  onEntityChange(val) {

    this.companyForm.controls.entity.setValue(val);
    this.companyForm.controls.entity.updateValueAndValidity();
    this.companyForm.updateValueAndValidity();
  }

  onProjectChange(val) {

    this.companyForm.controls.project.setValue(val);
    this.companyForm.controls.project.updateValueAndValidity();
    this.companyForm.updateValueAndValidity();
  }

  ngAfterViewInit(): void {
    if (this.company.id > 0) {
      this.companiesService.getCompanySetting(this.company.id).subscribe((res:any) => {
        console.log(res)
        this.projectAutocomplete.setValue(res.project_schema)
        this.entityAutocomplete.setValue(res.entity_schema)
      })
    }
  }
}
