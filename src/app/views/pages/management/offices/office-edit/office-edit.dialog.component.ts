// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, ViewEncapsulation, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Material
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Subscription } from 'rxjs';
// NGRX
import { Update } from '@ngrx/entity';
import { Store, select } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
// Services and Models
import {
  OfficeModel,
  OfficeOnServerCreated,
  selectOfficesActionLoading,
  selectOfficeError,
  selectOfficeLatestSuccessfullAction,
  OfficeOnServerUpdated,
  OfficeActionTypes
} from '../../../../../core/management';
import { User, currentUser } from 'src/app/core/auth';
import { TranslateService } from '@ngx-translate/core';
import {skip} from 'rxjs/operators';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-offices-edit-dialog',
	templateUrl: './office-edit.dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class OfficeEditDialogComponent implements OnInit, OnDestroy {
	// Public properties
	office: OfficeModel;
	officeForm: FormGroup;
	hasFormErrors = false;
	hasSubmissionErrors = false;
	submissionErrorObj: any;
	viewLoading = false;
	currentUser: User;
	// Private properties
  private subscriptions: Subscription[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<AssignCompanyProgramEditComponent>
	 * @param data: any
	 * @param fb: FormBuilder
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 * @param cdr: ChangeDetectorRef
	 */
	constructor(public dialogRef: MatDialogRef<OfficeEditDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private fb: FormBuilder,
		private store: Store<AppState>,
		private translate: TranslateService,
		private cdr: ChangeDetectorRef) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
    this.subscriptions.push(this.store.pipe(select(selectOfficesActionLoading)).subscribe(res => this.viewLoading = res));
    this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe(res => this.currentUser = res));
		this.office = this.data.office;
		this.createForm();
    this.subscriptions.push(this.store.pipe(select(selectOfficeError), skip(1)).subscribe((error) => {
      if (error) {
        if(error.error.code) {
          this.submissionErrorObj = error.error.errors;
        } else {
          this.submissionErrorObj = [{message:'GLOBAL.UNSPECIFIED_ERROR'}]
        }
        this.hasSubmissionErrors = true;
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectOfficeLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        if (res.action_type === OfficeActionTypes.OfficeCreated || res.action_type === OfficeActionTypes.OfficeUpdated) {
          this.submissionErrorObj = null;
          this.hasSubmissionErrors = false;
          this.dialogRef.close();
        }
      }
    }));
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
	}

	createForm() {
		this.officeForm = this.fb.group({
			name: [this.office.name, Validators.required],
			address: [this.office.address, Validators.compose([Validators.required])],
			phone: [
				this.office.phone && this.office.phone !== '' ?
				'(' + this.office.phone.substring(0, 3) + ') '
				+ this.office.phone.substring(3, 6)
				+ '-' + this.office.phone.substring(6, 10) : '',
				[Validators.required, Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)]],
				fax: [
					this.office.fax && this.office.fax !== '' ?
					'(' + this.office.fax.substring(0, 3) + ') '
					+ this.office.fax.substring(3, 6)
					+ '-' + this.office.fax.substring(6, 10) : '',
					[Validators.required, Validators.pattern(/^\(([0-9]{3})\)[ ]([0-9]{3})[-]([0-9]{4})$/)]],
			isActive: [this.office.isActive],
			prefix: [this.office.prefix],
			suffix: [this.office.suffix]
		});
	}

	/**
	 * Returns page title
	 */
	getTitle(): string {
		if (this.office.id > 0) {
			return 'OFFICE.EDIT_OFFICE';
		}

		return 'OFFICE.NEW_OFFICE';
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.officeForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}

	/** ACTIONS */

	/**
	 * Returns prepared office
	 */
	prepareOffice(): OfficeModel {
		const controls = this.officeForm.controls;
		const _office = new OfficeModel();
		_office.id = this.office.id;
		_office.name = controls.name.value;
		_office.companyID = this.currentUser.companyID;
		_office.address = controls.address.value;
		_office.phone = controls.phone.value !== null ? controls.phone.value.replace(/\D/g, '') : controls.phone.value;
		_office.fax = controls.fax.value !== null ? controls.fax.value.replace(/\D/g, '') : controls.fax.value;
		_office.isActive = controls.isActive.value;
		_office.prefix = controls.prefix.value !== undefined && controls.prefix.value !== null ? controls.prefix.value : '';
		_office.suffix = controls.suffix.value !== undefined && controls.suffix.value !== null ? controls.suffix.value : '';
		return _office;
	}

	/**
	 * On Submit
	 */
	onSubmit() {
		this.hasFormErrors = false;
		this.hasSubmissionErrors = false;
		const controls = this.officeForm.controls;
		/** check form */
		if (this.officeForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);

			this.hasFormErrors = true;
			return;
		}

		const editedOffice = this.prepareOffice();
		if (editedOffice.id > 0) {
			this.updateOffice(editedOffice);
		} else {
			this.createOffice(editedOffice);
		}
	}

	/**
	 * Update office
	 *
	 * @param _office: OfficeModel
	 */
	updateOffice(_office: OfficeModel) {
		_office.isActive = _office.isActive.toString() === 'true' ? true : false;
		const updateOffice: Update<OfficeModel> = {
			id: _office.id,
			changes: _office
		};
		this.store.dispatch(new OfficeOnServerUpdated({
			partialOffice: updateOffice,
			office: _office
		}));
	}

	/**
	 * Create office
	 *
	 * @param _office: OfficeModel
	 */
	createOffice(_office: OfficeModel) {
		this.store.dispatch(new OfficeOnServerCreated({ office: _office }));
	}

	/** Alect Close event */
	onAlertClose($event) {
		this.hasFormErrors = false;
	}

	/**
	 * Rerurns status string
	 */
	getStatusString(): string {
		switch (this.officeForm.get('isActive').value) {
			case true:
				return this.translate.instant('GLOBAL.ACTIVE');
			case false:
				return this.translate.instant('GLOBAL.INACTIVE');
		}
		return '';
	}
}
