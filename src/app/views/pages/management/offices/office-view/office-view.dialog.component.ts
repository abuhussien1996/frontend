// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, ViewEncapsulation, OnDestroy } from '@angular/core';
// Material
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Subscription, of } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
// Services and Models
import { OfficeModel,
	selectOfficesActionLoading,
} from '../../../../../core/management';
import { TranslateService } from '@ngx-translate/core';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-office-view-dialog',
	templateUrl: './office-view.dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class OfficeViewDialogComponent implements OnInit, OnDestroy {
	// Public properties
	office: OfficeModel;
	viewLoading = false;
	// Private properties
	private componentSubscriptions: Subscription;

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<AssignCompanyProgramEditComponent>
	 * @param data: any
	 * @param store: Store<AppState>
	 * @param translate: TranslateService
	 */
	constructor(public dialogRef: MatDialogRef<OfficeViewDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private store: Store<AppState>,
		private translate: TranslateService) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		this.store.pipe(select(selectOfficesActionLoading)).subscribe(res => this.viewLoading = res);
		this.office = this.data.office;
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}

	/**
	 * Retursn CSS Class Name by status
	 *
	 * @param isActive: boolean
	 */
	getItemCssClassByStatus(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
	}

	/**
	 * Returns Item Status in string
	 * @param isActive: boolean
	 */
	getItemStatusString(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return this.translate.instant('GLOBAL.INACTIVE');
			case true:
				return this.translate.instant('GLOBAL.ACTIVE');
		}
	}

	/**
	 * Returns phone/fax in the correct format
	 */
	formatPhone(phone) {
	  if ((phone) != null && (phone) !== '0' && (phone) !== '') {
		return '(' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + '-' + phone.substring(6, 10);
	  } else {
		return '';
	  }
	}
}
