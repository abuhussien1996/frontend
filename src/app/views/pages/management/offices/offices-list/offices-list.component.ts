// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip } from 'rxjs/operators';
import { fromEvent, merge, Subscription } from 'rxjs';
// Translate Module
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../core/reducers';
// CRUD
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../../core/_base/crud';
// Services and Models
import {
  OfficeModel,
  OfficesDataSource,
  OfficesPageRequested,
  OfficesService,
  OneOfficeOnServerDeleted,
  ManyOfficesOnServerDeleted,
  OfficeStatusOnServerUpdated,
  OfficesStatusOnServerUpdated,
  selectOfficesActionLoading,
  selectOfficeError,
  selectOfficeLatestSuccessfullAction,
  OfficeActionTypes
} from '../../../../../core/management';
// Components
import { OfficeEditDialogComponent } from '../office-edit/office-edit.dialog.component';
import { OfficeViewDialogComponent } from '../office-view/office-view.dialog.component';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'kt-offices-list',
	templateUrl: './offices-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,

})
export class OfficesListComponent implements OnInit, OnDestroy {
	// Table fields
	dataSource: OfficesDataSource;
	displayedColumns = ['select', 'name', 'phone', 'fax', 'isActive', 'actions'];
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChild('sort1', {static: true}) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', {static: true}) searchInput: ElementRef;
	filterStatus = '';
	filterType = '';
	// Selection
	selection = new SelectionModel<OfficeModel>(true, []);
	officesResult: OfficeModel[] = [];
	// Subscriptions
	private subscriptions: Subscription[] = [];

	/**
	 * Component constructor
	 *
	 * @param dialog: MatDialog
	 * @param snackBar: MatSnackBar
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param translate: TranslateService
	 * @param officesService: OfficesService
	 * @param store: Store<AppState>
	 * @param cdr: ChangeDetectorRef
	 */
	constructor(
		public dialog: MatDialog,
		public snackBar: MatSnackBar,
		private layoutUtilsService: LayoutUtilsService,
		private translate: TranslateService,
		private officesService: OfficesService,
		private store: Store<AppState>,
		private cdr: ChangeDetectorRef
	) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {

		// If the user changes the sort order, reset back to the first page.
    this.subscriptions.push(this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0)));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
    this.subscriptions.push(merge(this.sort.sortChange, this.paginator.page).pipe(
			tap(() => this.loadOfficesList())
		).subscribe());

		// Filtration, bind to searchInput
    this.subscriptions.push(fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
			// tslint:disable-next-line:max-line-length
			debounceTime(50), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
			distinctUntilChanged(), // This operator will eliminate duplicate values
			tap(() => {
				this.paginator.pageIndex = 0;
				this.loadOfficesList();
			})
		).subscribe());

		// Init DataSource
		this.dataSource = new OfficesDataSource(this.store);
    this.subscriptions.push(this.dataSource.entitySubject.pipe(
			skip(1),
			distinctUntilChanged()
		).subscribe(res => {
			this.officesResult = res;
		}));
		this.loadOfficesList();
    this.subscriptions.push(this.store.pipe(select(selectOfficeError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Update,
          3000,
          true,
          false
        );
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectOfficeLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch (res.action_type) {
          case OfficeActionTypes.OfficeCreated:
            this.loadOfficesList();
            successMsg = this.translate.instant('OFFICE.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case OfficeActionTypes.OfficeUpdated:
            this.loadOfficesList();
            successMsg = this.translate.instant('OFFICE.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case OfficeActionTypes.OfficeStatusUpdated:
            successMsg = this.translate.instant('OFFICE.UPDATE_STATUS_SIMPLE.MESSAGE');
            msgType = MessageType.Update;
            break;
          case OfficeActionTypes.OfficesStatusUpdated:
            this.selection.clear();
            successMsg = this.translate.instant('OFFICE.UPDATE_STATUS_MULTI.MESSAGE');
            msgType = MessageType.Update;
            break;
          case OfficeActionTypes.ManyOfficesDeleted:
            this.selection.clear();
            successMsg = this.translate.instant('OFFICE.DELETE_OFFICE_MULTI.MESSAGE');
            msgType = MessageType.Delete;
            break;
          case OfficeActionTypes.OneOfficeDeleted:
            successMsg = this.translate.instant('OFFICE.DELETE_OFFICE_SIMPLE.MESSAGE');
            msgType = MessageType.Delete;
            break;
        }
        if (successMsg) {
          this.layoutUtilsService.showActionNotification(
            successMsg,
            msgType,
            3000,
            true,
            false
          );
        }
      }
    }));
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	/**
	 * Load Offices List from service through data-source
	 */
	loadOfficesList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		// Call request from server
		this.store.dispatch(new OfficesPageRequested({ page: queryParams, id: 0 }));
		this.selection.clear();
	}

	/**
	 * Returns object for filter
	 */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter.status = this.filterStatus;
		filter.text = searchText ? searchText : '';

		if (!searchText) {
			return filter;
        }

		filter.name = searchText;
		filter.company = searchText;
		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete office
	 *
	 * @param _item: OfficeModel
	 */
	deleteOffice(_item: OfficeModel) {
		const _title: string = this.translate.instant('OFFICE.DELETE_OFFICE_SIMPLE.TITLE');
		const _description: string = this.translate.instant('OFFICE.DELETE_OFFICE_SIMPLE.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('OFFICE.DELETE_OFFICE_SIMPLE.WAIT_DESCRIPTION');

		const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.store.dispatch(new OneOfficeOnServerDeleted({ id: _item.id, companyID: _item.companyID }));
		});
	}

	/**
	 * Delete selected offices
	 */
	deleteOffices() {
		const _title: string = this.translate.instant('OFFICE.DELETE_OFFICE_MULTI.TITLE');
		const _description: string = this.translate.instant('OFFICE.DELETE_OFFICE_MULTI.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('OFFICE.DELETE_OFFICE_MULTI.WAIT_DESCRIPTION');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			const officesForDeletion: any[] = [];
			const ids: number[] = [];
			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < this.selection.selected.length; i++) {
				officesForDeletion.push({
					id: this.selection.selected[i].id,
					companyID: this.selection.selected[i].companyID
				});
				ids.push(this.selection.selected[i].id);
			}
			this.store.dispatch(new ManyOfficesOnServerDeleted({ offices: officesForDeletion, ids }));
		});
	}

	/**
	 * Fetch selected offices
	 */
	fetchOffices() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.name}`,
				id: elem.id.toString(),
				status: elem.isActive
			});
		});
		this.layoutUtilsService.fetchElements(messages);
	}

	/**
	 * Update Status for single office
	 */
	updateStatusForOffice(office: OfficeModel) {
		const _title: string = this.translate.instant('OFFICE.UPDATE_STATUS_SIMPLE.TITLE');
		const _description: string = this.translate.instant('OFFICE.UPDATE_STATUS_SIMPLE.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('OFFICE.UPDATE_STATUS_SIMPLE.WAIT_DESCRIPTION');

		const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.store.dispatch(new OfficeStatusOnServerUpdated({
					id: office.id,
					office,
					isActive: !office.isActive
				}));
		});
	}

	/**
	 * Show UpdateStatuDialog for selected offices
	 */
	updateStatusForOffices() {
		const _title = this.translate.instant('OFFICE.UPDATE_STATUS_MULTI.TITLE');
		const _updateMessage = this.translate.instant('OFFICE.UPDATE_STATUS_MULTI.MESSAGE');
		const _statuses = [{ value: false, text: this.translate.instant('GLOBAL.INACTIVE') }, { value: true, text: this.translate.instant('GLOBAL.ACTIVE') }];
		const _messages = [];

		this.selection.selected.forEach(elem => {
			_messages.push({
				text: `${elem.name}`,
				id: elem.id.toString(),
				status: elem.isActive,
				statusTitle: this.getItemStatusString(elem.isActive),
				statusCssClass: this.getItemCssClassByStatus(elem.isActive)
			});
		});

		const dialogRef = this.layoutUtilsService.updateStatusForEntities(_title, _statuses, _messages);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				this.selection.clear();
				return;
			}

			const ids = [];
			this.selection.selected.forEach(elem => {
				ids.push(elem.id);
			});
			this.store.dispatch(new OfficesStatusOnServerUpdated({
				ids,
				offices: this.selection.selected,
				isActive: res === 'true' ? true : false
			}));
		});
	}

	/**
	 * Show add office dialog
	 */
	addOffice() {
		const newOffice = new OfficeModel();
		newOffice.clear(); // Set all defaults fields
		this.updateOffice(newOffice);
	}

	editOffice(office: OfficeModel) {
		this.officesService.getOfficeById(office.id).subscribe(res => this.updateOffice(res))
	}

	/**
	 * Show Edit office dialog and save after success close result
	 * @param office: OfficeModel
	 */
	updateOffice(office: OfficeModel) {
		this.dialog.open(OfficeEditDialogComponent, { data: { office },width:'400px' });
	}

	/**
	 * Show View office dialog
	 * @param office: OfficeModel
	 */
	viewOffice(office: OfficeModel) {
		this.officesService.getOfficeById(office.id).subscribe(res => {
			this.dialog.open(OfficeViewDialogComponent, { data: { office: res } });
		});
	}

	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.officesResult.length;
		return numSelected === numRows;
	}

	/**
	 * Toggle all selections
	 */
	masterToggle() {
		if (this.selection.selected.length === this.officesResult.length) {
			this.selection.clear();
		} else {
			this.officesResult.forEach(row => this.selection.select(row));
		}
	}

	/** UI */
	/**
	 * Retursn CSS Class Name by status
	 *
	 * @param isActive: boolean
	 */
	getItemCssClassByStatus(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
	}

	/**
	 * Returns Item Status in string
	 * @param isActive: boolean
	 */
	getItemStatusString(isActive: boolean = false): string {
		switch (isActive) {
			case false:
				return this.translate.instant('GLOBAL.INACTIVE');
			case true:
				return this.translate.instant('GLOBAL.ACTIVE');
		}
	}

	/**
	 * Returns phone/fax in the correct format
	 */
	formatPhone(phone) {
	  if ((phone) != null && (phone) !== '0' && (phone) !== '') {
		return '(' + phone.substring(0, 3) + ') ' + phone.substring(3, 6) + '-' + phone.substring(6, 10);
	  } else {
		return '';
	  }
	}
}
