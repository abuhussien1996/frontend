import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, merge, Subscription} from 'rxjs';
import {BaseDataSource, QueryParamsModel} from '../../../../../core/_base/crud';
import {of} from 'rxjs/internal/observable/of';
import {MatDialog} from '@angular/material/dialog';
import {CreateEntityDialogComponent} from '../create-entity-dialog/create-entity-dialog.component';
import {EntityService} from "../../services/entity.service";
import {MatPaginator} from "@angular/material/paginator";
import {tap} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'kt-entity-list',
  templateUrl: './entity-list.component.html',
  styleUrls: ['./entity-list.component.scss']
})
export class EntityListComponent implements OnInit, OnDestroy {

  dataSource = new BaseDataSource();
  displayedColumns = ['id', 'name'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('sort', {static: true}) sort: MatSort;
  private subscriptions: Subscription[] = [];

  constructor(private dialog: MatDialog, private entityService: EntityService) {
    this.dataSource.isPreloadTextViewed$ = of(true);

  }

  ngOnInit(): void {
    this.loadEntities();
    const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => {
        this.loadEntities();
      })
    ).subscribe();
    this.subscriptions.push(paginatorSubscriptions);
  }

  addEntity() {
    this.dialog.open(CreateEntityDialogComponent, {}).afterClosed().subscribe(res => {
      if (res.reload) {
        this.loadEntities();
      }
    })

  }

  private loadEntities() {
    this.dataSource.loading$ = of(true);
    this.entityService.getEntities(new QueryParamsModel({}, undefined, undefined,
      this.paginator.pageIndex, this.paginator.pageSize)).subscribe(res => {
      this.dataSource.loading$ = of(false);
      this.dataSource.isPreloadTextViewed$ = of(false);
      this.dataSource.entitySubject.next(res.data);
      this.dataSource.paginatorTotalSubject.next(res.count);
      this.dataSource.hasItems = res.count > 0;
    })

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(el => el.unsubscribe());
  }
}
