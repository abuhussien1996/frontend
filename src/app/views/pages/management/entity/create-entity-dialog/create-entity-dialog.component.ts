import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {JsonEditorComponent, JsonEditorOptions} from 'ang-jsoneditor';
import {EntityService} from "../../services/entity.service";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'kt-create-entity-dialog',
  templateUrl: './create-entity-dialog.component.html',
  styleUrls: ['./create-entity-dialog.component.scss']
})
export class CreateEntityDialogComponent implements OnInit {
  private isCreate = true;
  entityForm: FormGroup;
  viewLoading = false;
  public editorOptions: JsonEditorOptions;
  public data: any;
  @ViewChild(JsonEditorComponent, {static: false}) editor: JsonEditorComponent;

  constructor(private fb: FormBuilder, private entityService: EntityService,
              private dialogRef: MatDialogRef<CreateEntityDialogComponent>) {
    this.editorOptions = new JsonEditorOptions()
    this.editorOptions.mode = 'code'; // set all allowed modes
    this.editorOptions.statusBar = false; // set all allowed modes
    this.entityForm = this.fb.group({
      name: ['', Validators.required],
      schema: [this.data, [Validators.required]]
    });
  }

  ngOnInit(): void {

  }

  getTitle() {
    return this.isCreate ? 'Create Entity' : 'Update Entity';
  }

  onSubmit() {
    if (!this.entityForm.valid || !this.editor.isValidJson()) {
      this.entityForm.markAsTouched();
      this.entityForm.markAllAsTouched();
      return;
    }
    if (this.isCreate) {
      const body = this.getEntityBody();
      this.entityService.createEntity(body).subscribe(res => {
        this.dialogRef.close({reload: true});
      });
    }
  }

  private getEntityBody() {
    const body = this.entityForm.value;
    body.schema = JSON.stringify(body.schema);
    return body;
  }


}
