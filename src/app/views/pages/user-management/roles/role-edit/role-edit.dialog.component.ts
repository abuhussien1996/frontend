// Angular
import {Component, OnInit, Inject, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
// RxJS
import {Observable, of, Subscription} from 'rxjs';
// Lodash
import {each, find, some} from 'lodash';
// NGRX
import {Update} from '@ngrx/entity';
import {Store, select} from '@ngrx/store';
// State
import {AppState} from '../../../../../core/reducers';
// Services and Models
import {
  Role,
  Permission,
  selectRoleById,
  RoleOnServerCreated,
  RoleOnServerUpdated,
  selectRoleError,
  currentUserRoleIds,
  selectAllNonSuperAdminPermissions,
  currentUserPermissions,
  currentUser,
  AllRolesRequested, selectRoleLatestSuccessfullAction, RoleActionTypes
} from '../../../../../core/auth';
import {CompanyModel} from 'src/app/core/management';
import * as _ from 'lodash';
import {skip} from 'rxjs/operators';
import {CopyRolePermissionDialogComponent} from "../copy-role-permission-dialog/copy-role-permission-dialog.component";

@Component({
  selector: 'kt-role-edit-dialog',
  templateUrl: './role-edit.dialog.component.html',
  styleUrls: ['./role-edit.dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class RoleEditDialogComponent implements OnInit, OnDestroy {
  // Public properties
  role: Role;
  role$: Observable<Role>;
  hasFormErrors = false;
  hasSubmissionErrors = false;
  submissionErrorObj: any;
  viewLoading = false;
  loadingAfterSubmit = false;
  isSuperAdmin: boolean;
  allPermissions$: Observable<Permission[]>;
  rolePermissions: Permission[] = [];
  companiesList: CompanyModel[];
  // Private properties
  private subscriptions: Subscription[] = [];
  showCopyPermissions = true;

  /**
   * Component constructor
   *
   * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
   * @param data: any
   * @param store: Store<AppState>
   * @param cdr: ChangeDetectorRef
   */
  constructor(public dialogRef: MatDialogRef<RoleEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private store: Store<AppState>,
              private cdr: ChangeDetectorRef, private dialog: MatDialog) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    this.subscriptions.push(this.store.pipe(select(currentUserRoleIds)).subscribe(res => this.isSuperAdmin = res && res.includes(1)));
    this.companiesList = this.data.companies;
    if (this.data.roleId) {
      this.role$ = this.store.pipe(select(selectRoleById(this.data.roleId)));
      this.showCopyPermissions = false;
    } else {
      const newRole = new Role();
      newRole.clear();
      this.role$ = of(newRole);
    }

    this.role$.subscribe(res => {
      if (!res) {
        return;
      }
      this.role = new Role();
      this.role.id = res.id;
      this.role.title = res.title;
      this.role.permissions = res.permissions;
      this.role.type = res.type;
      this.role.companyID = res.companyID;

      if (this.isSuperAdmin) {
        this.allPermissions$ = this.store.pipe(select(selectAllNonSuperAdminPermissions));
      } else {
        this.allPermissions$ = this.store.pipe(select(currentUserPermissions));
        this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe(result => this.role.companyID = result.companyID));
      }
      this.loadPermissions();
    });
    this.subscriptions.push(this.store.pipe(select(selectRoleError), skip(1)).subscribe((error) => {
      if (error) {
        if (error.error.code) {
          this.submissionErrorObj = error.error.errors;
        } else {
          this.submissionErrorObj = [{message: 'GLOBAL.UNSPECIFIED_ERROR'}]
        }
        this.hasSubmissionErrors = true;
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectRoleLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        if (res.action_type === RoleActionTypes.RoleCreated || res.action_type === RoleActionTypes.RoleUpdated) {
          this.submissionErrorObj = null;
          this.hasSubmissionErrors = false;
          this.store.dispatch(new AllRolesRequested());
          this.dialogRef.close();
        }
      }
    }));
  }

  /**
   * On destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * Load permissions
   */
  loadPermissions() {
    this.subscriptions.push(this.allPermissions$.subscribe(_allPermissions => {
      if (!_allPermissions) {
        return;
      }

      const mainPermissions = _allPermissions.filter(el => !el.parentId);
      mainPermissions.forEach((element: Permission) => {
        const hasUserPermission = this.role.permissions.some(t => t === element.id);
        const rootPermission = new Permission();
        rootPermission.clear();
        rootPermission.isSelected = hasUserPermission;
        rootPermission._children = [];
        rootPermission.id = element.id;
        rootPermission.level = element.level;
        rootPermission.parentId = element.parentId;
        rootPermission.title = element.title;
        const children = _allPermissions.filter(el => el.parentId && el.parentId === element.id);
        children.forEach(child => {
          const hasUserChildPermission = this.role.permissions.some(t => t === child.id);
          const childPermission = new Permission();
          childPermission.clear();
          childPermission.isSelected = hasUserChildPermission;
          childPermission._children = [];
          childPermission.id = child.id;
          childPermission.level = child.level;
          childPermission.parentId = child.parentId;
          childPermission.title = child.title;
          rootPermission._children.push(childPermission);
        });
        this.rolePermissions.push(rootPermission);
        this.rolePermissions = _.orderBy(this.rolePermissions, ['id']).reverse()

      });
    }));
  }

  /** ACTIONS */
  /**
   * Returns permissions ids
   */
  preparePermissionIds(): number[] {
    const result = [];
    each(this.rolePermissions, (_root: Permission) => {
      if (_root.isSelected) {
        result.push(_root.id);
        each(_root._children, (_child: Permission) => {
          if (_child.isSelected) {
            result.push(_child.id);
          }
        });
      }
    });
    return result;
  }

  /**
   * Returns role for save
   */
  prepareRole(): Role {
    const _role = new Role();
    _role.id = this.role.id;
    _role.permissions = this.preparePermissionIds();
    _role.title = this.role.title;
    _role.type = this.role.type;
    _role.type = this.isSuperAdmin ? 2 : 3;
    _role.companyID = this.role.companyID;
    return _role;
  }

  /**
   * Save data
   */
  onSubmit() {
    this.hasFormErrors = false;
    this.loadingAfterSubmit = false;
    /** check form */
    if (!this.isTitleValid()) {
      this.hasFormErrors = true;
      return;
    }

    const editedRole = this.prepareRole();
    if (editedRole.id > 0) {
      this.updateRole(editedRole);
    } else {
      this.createRole(editedRole);
    }
  }

  /**
   * Update role
   *
   * @param _role: Role
   */
  updateRole(_role: Role) {
    this.loadingAfterSubmit = true;
    this.viewLoading = true;
    /* Server loading imitation. Remove this on real code */
    const updateRole: Update<Role> = {
      id: this.role.id,
      changes: _role
    };
    this.store.dispatch(new RoleOnServerUpdated({
      partialrole: updateRole,
      role: _role
    }));
  }

  /**
   * Create role
   *
   * @param _role: Role
   */
  createRole(_role: Role) {
    this.loadingAfterSubmit = true;
    this.viewLoading = true;
    this.store.dispatch(new RoleOnServerCreated({role: _role}));
  }

  /**
   * Close alert
   *
   * @param $event: Event
   */
  onAlertClose($event) {
    this.hasFormErrors = false;
  }

  /**
   * Check is selected changes
   *
   * @param $event: Event
   * @param permission: Permission
   */
  isSelectedChanged($event, permission: Permission) {
    if (permission._children.length === 0 && permission.isSelected) {
      const _root = find(this.rolePermissions, (item: Permission) => item.id === permission.parentId);
      if (_root && !_root.isSelected) {
        _root.isSelected = true;
      }
      return;
    }

    if (permission._children.length === 0 && !permission.isSelected) {
      const _root = find(this.rolePermissions, (item: Permission) => item.id === permission.parentId);
      if (_root && _root.isSelected) {
        if (!some(_root._children, (item: Permission) => item.isSelected === true)) {
          _root.isSelected = false;
        }
      }
      return;
    }

    if (permission._children.length > 0 && permission.isSelected) {
      each(permission._children, (item: Permission) => item.isSelected = true);
      return;
    }

    if (permission._children.length > 0 && !permission.isSelected) {
      each(permission._children, (item: Permission) => {
        item.isSelected = false;
      });
      return;
    }
  }

  /** UI */
  /**
   * Returns component title
   */
  getTitle(): string {
    if (this.role && this.role.id) {
      // tslint:disable-next-line:no-string-throw
      return 'ROLES.EDIT_ROLE';
    }

    // tslint:disable-next-line:no-string-throw
    return 'ROLES.NEW_ROLE';
  }

  /**
   * Returns is title valid
   */
  isTitleValid(): boolean {
    return (this.role && this.role.title && this.role.title.length > 0);
  }

  getChildrenSorted(permissions) {
    return _.orderBy(permissions, ['title']).reverse()
  }

  onCopyPermissionClicked() {
    const dialogRef = this.dialog.open(CopyRolePermissionDialogComponent, {
      data: {},
      width: '450px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(role => {
      if(role) {
        this.rolePermissions.map(e => {
          e.isSelected = role.indexOf(e.id) > -1
          e._children.map(e1 => {
            e1.isSelected = role.indexOf(e.id) > -1
            return e1;
          })
          return e;
        })
      }
    })
  }
}
