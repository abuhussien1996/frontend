import {Component, OnInit, ViewChild} from '@angular/core';
import {CategoryService} from '../../../../../core/lists';
import {FormControl} from '@angular/forms';
import {AuthService} from '../../../../../core/auth';
import {GeneralAutocompleteComponent} from '../../../../partials/layout/general-autocomplete/general-autocomplete.component';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'kt-copy-role-permission-dialog',
  templateUrl: './copy-role-permission-dialog.component.html',
  styleUrls: ['./copy-role-permission-dialog.component.scss']
})
export class CopyRolePermissionDialogComponent implements OnInit {

  categoryAutocompleteCall = this.categoryService.searchCategories.bind(this.categoryService);
  roleCtrl = new FormControl();
  searchRolesFn = this.authService.searchRoles.bind(this.authService);
  formatNewRoleFn = ((value) => {
    return value;
  }).bind(this)

  @ViewChild('roleAutocomplete') roleAutocomplete: GeneralAutocompleteComponent;
  private selectedRole: any;
  disableBtns = false;

  constructor(private categoryService: CategoryService, private authService: AuthService,
              public dialogRef: MatDialogRef<CopyRolePermissionDialogComponent>) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.roleAutocomplete.controlObject.invalid) {
      this.roleAutocomplete.updateValueAndValidity();
      return
    }
    this.disableBtns = true;
    this.authService.findRoles({
      filter: {title: this.selectedRole.name, searchData: this.selectedRole.name},
      pageNumber: 0,
      pageSize: 10,
      sortField: 'id',
      sortOrder: 'asc',
    }).subscribe(res=>{
      this.dialogRef.close(res && res.pageOfItems && res.pageOfItems[0] && res.pageOfItems[0].permissions &&
      res.pageOfItems[0].permissions.length ? res.pageOfItems[0].permissions : []);
    })
  }

  onCategorChange(val) {

  }


  filterCategoryFn(event: any): string {
    return event ? event.name : '';
  }

  filterRoleFn(event: any): string {
    return event ? event.name : '';
  }

  selected(val): void {
    this.selectedRole = val;
  }
}
