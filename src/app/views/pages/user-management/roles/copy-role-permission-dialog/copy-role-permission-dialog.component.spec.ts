import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyRolePermissionDialogComponent } from './copy-role-permission-dialog.component';

describe('CopyRolePermissionDialogComponent', () => {
  let component: CopyRolePermissionDialogComponent;
  let fixture: ComponentFixture<CopyRolePermissionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyRolePermissionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyRolePermissionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
