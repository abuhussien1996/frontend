// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { fromEvent, merge, of, Subscription } from 'rxjs';
// Translate Module
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store, select } from '@ngrx/store';
// Services
import { LayoutUtilsService, MessageType } from '../../../../../core/_base/crud';
// Models
import {
  Role,
  RolesDataSource,
  RolesPageRequested,
  selectRoleError,
  selectRoleLatestSuccessfullAction,
  RoleOnServerDeleted,
  RolesOnServerDeleted,
  currentUserRoleIds, RoleActionTypes
} from '../../../../../core/auth';
import { AppState } from '../../../../../core/reducers';
import { QueryParamsModel } from '../../../../../core/_base/crud';
// Components
import { RoleEditDialogComponent } from '../role-edit/role-edit.dialog.component';
// Management components
import { CompanyModel, CompaniesService } from 'src/app/core/management';

@Component({
	selector: 'kt-roles-list',
	templateUrl: './roles-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RolesListComponent implements OnInit, OnDestroy {
	// Table fields
	dataSource: RolesDataSource;
	displayedColumns = [];
	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
	@ViewChild('sort1', { static: true }) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', { static: true }) searchInput: ElementRef;
	// Selection
	selection = new SelectionModel<Role>(true, []);
	rolesResult: Role[] = [];
	companies: CompanyModel[] = [];
	companiesNoAdminRole: CompanyModel[] = [];
	isSuperAdmin: boolean;

	// Subscriptions
	private subscriptions: Subscription[] = [];

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 * @param dialog: MatDialog
	 * @param snackBar: MatSnackBar
	 * @param layoutUtilsService: LayoutUtilsService
	 * @param companiesService: CompaniesService
	 * @param cdr: ChangeDetectorRef
	 * @param translate: TranslateService
	 */
	constructor(
		private store: Store<AppState>,
		public dialog: MatDialog,
		public snackBar: MatSnackBar,
		private layoutUtilsService: LayoutUtilsService,
		private companiesService: CompaniesService,
		private cdr: ChangeDetectorRef,
		private translate: TranslateService
		) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
    this.subscriptions.push(this.store.pipe(select(currentUserRoleIds)).subscribe(res => {
			this.isSuperAdmin = res ? res.includes(1): null;
			this.displayedColumns = this.isSuperAdmin ? ['select', 'title', 'company', 'actions'] : ['select', 'title', 'actions'];
			if(this.isSuperAdmin) {
				this.loadCompaniesList();
			}
		}));
		// If the user changes the sort order, reset back to the first page.
    this.subscriptions.push(this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0)));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
    this.subscriptions.push(merge(this.sort.sortChange, this.paginator.page).pipe(
			tap(() => {
				this.loadRolesList();
			})
		).subscribe());

		// Filtration, bind to searchInput
    this.subscriptions.push(fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
			// tslint:disable-next-line:max-line-length
			debounceTime(150), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
			distinctUntilChanged(), // This operator will eliminate duplicate values
			tap(() => {
				this.paginator.pageIndex = 0;
				this.loadRolesList();
			})
		).subscribe());

		// Init DataSource
		this.dataSource = new RolesDataSource(this.store);
    this.subscriptions.push(this.dataSource.entitySubject.pipe(
			skip(1),
			distinctUntilChanged()
		).subscribe(res => {
			this.rolesResult = res;
		}));

		// First load
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => { // Remove this line, just loading imitation
			this.loadRolesList();
		});
    this.subscriptions.push(this.store.pipe(select(selectRoleError), skip(1)).subscribe((res) => {
      if (res) {
        this.layoutUtilsService.showActionNotification(
          this.translate.instant('GLOBAL.UNSPECIFIED_ERROR'),
          MessageType.Update,
          3000,
          true,
          false
        );
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectRoleLatestSuccessfullAction), skip(1)).subscribe((res) => {
      if (res && res.action_type) {
        let successMsg;
        let msgType;
        switch (res.action_type) {
          case RoleActionTypes.RoleCreated:
            this.loadRolesList();
            successMsg = this.translate.instant('ROLES.EDIT.ADD_MESSAGE');
            msgType = MessageType.Create;
            break;
          case RoleActionTypes.RoleUpdated:
            this.loadRolesList();
            successMsg = this.translate.instant('ROLES.EDIT.UPDATE_MESSAGE');
            msgType = MessageType.Update;
            break;
          case RoleActionTypes.RolesDeleted:
            this.loadRolesList();
            this.selection.clear();
            successMsg = this.translate.instant('ROLES.DELETE_ROLE_MULTI.MESSAGE');
            msgType = MessageType.Delete;
            break;
          case RoleActionTypes.RoleDeleted:
            this.loadRolesList();
            successMsg = this.translate.instant('ROLES.DELETE_ROLE_SIMPLE.MESSAGE');
            msgType = MessageType.Delete;
            break;
        }
        if (successMsg) {
          this.layoutUtilsService.showActionNotification(
            successMsg,
            msgType,
            3000,
            true,
            false
          );
        }
      }
    }));
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(sub => sub.unsubscribe());
	}

	/**
	 * Load Roles List
	 */
	loadRolesList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);
		// Call request from server
		this.store.dispatch(new RolesPageRequested({ page: queryParams }));
		this.selection.clear();
	}

	loadCompaniesList() {
		this.companiesService.getAllCompanies().subscribe(res => this.companies = res);
		this.companiesService.getCompaniesNoAdminRole().subscribe(res => this.companiesNoAdminRole = res);
	}

	/**
	 * Returns object for filter
	 */
	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;
		filter.title = searchText;
		filter.searchData = searchText;
		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete role
	 *
	 * @param _item: Role
	 */
	deleteRole(_item: Role) {
		const _title: string = this.translate.instant('ROLES.DELETE_ROLE_SIMPLE.TITLE');
		const _description: string = this.translate.instant('ROLES.DELETE_ROLE_SIMPLE.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('ROLES.DELETE_ROLE_SIMPLE.WAIT_DESCRIPTION');

		const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.store.dispatch(new RoleOnServerDeleted({ id: _item.id }));
		});
	}

	/**
	 * Delete roles
	 *
	 * @param _item: Roles
	 */
	deleteRoles() {
		const _title: string = this.translate.instant('ROLES.DELETE_ROLE_MULTI.TITLE');
		const _description: string = this.translate.instant('ROLES.DELETE_ROLE_MULTI.DESCRIPTION');
		const _waitDesciption: string = this.translate.instant('ROLES.DELETE_ROLE_MULTI.WAIT_DESCRIPTION');
		const _deleteMessage = this.translate.instant('ROLES.DELETE_ROLE_MULTI.MESSAGE');

		const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			const ids: number[] = [];
			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < this.selection.selected.length; i++) {
				ids.push(this.selection.selected[i].id);
			}
			this.store.dispatch(new RolesOnServerDeleted({ ids }));
		});

	}

	/** Fetch */
	/**
	 * Fetch selected rows
	 */
	fetchRoles() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.title}`,
				id: elem.id.toString(),
				// status: elem.username
			});
		});
		this.layoutUtilsService.fetchElements(messages);
	}

	/**
	 * Add role
	 */
	addRole() {
		const newRole = new Role();
		newRole.clear(); // Set all defaults fields
		this.editRole(newRole);
	}

	/**
	 * Edit role
	 *
	 * @param role: Role
	 */
	editRole(role: Role) {
		const dialogConfig = new MatDialogConfig();
		dialogConfig.disableClose = false;
		dialogConfig.closeOnNavigation = true;
		dialogConfig.maxHeight = '100vh';
		dialogConfig.maxWidth = '100vw';
		dialogConfig.height = '100vh';
		dialogConfig.width = '100vw';
		this.dialog.open(RoleEditDialogComponent, { data: { roleId: role.id, companies: this.companiesNoAdminRole } });
	}

	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.rolesResult.length;
		return numSelected === numRows;
	}

	/**
	 * Toggle selection
	 */
	masterToggle() {
		if (this.selection.selected.length === this.rolesResult.length) {
			this.selection.clear();
		} else {
			this.rolesResult.forEach(row => this.selection.select(row));
		}
	}

	getCompanyName(companyID) {
		// tslint:disable-next-line:max-line-length
		return this.companies.find(company => company.id === companyID) !== undefined ? this.companies.find(company => company.id === companyID).name : '';
	}
}
