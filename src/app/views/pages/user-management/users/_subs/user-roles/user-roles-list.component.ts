// Angular
import {Component, OnInit, Input, ChangeDetectorRef} from '@angular/core';
// RxJS
import { BehaviorSubject, Observable } from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
// Lodash
import { each, find, remove } from 'lodash';
// State
import { AppState } from '../../../../../../core/reducers';
// Auth
import { Role, selectAllCompanyRoles, AuthService  } from '../../../../../../core/auth';
import { UserService } from 'src/app/core/auth/_services/user.service';

@Component({
	selector: 'kt-user-roles-list',
	templateUrl: './user-roles-list.component.html'
})
export class UserRolesListComponent implements OnInit {

	@Input() loadingSubject = new BehaviorSubject<boolean>(false);
	@Input() rolesSubject: BehaviorSubject<number[]>;

	// Roles
	allUserRoles$: Observable<Role[]>;
	allRoles: Role[] = [];
	unassignedRoles: Role[] = [];
	assignedRoles: Role[] = [];
	roleIdForAdding: number;
	Roles:Role[]
  rolenotloaded = true

	constructor(private store: Store<AppState>,
		private auth: AuthService,
			private user:UserService,
              private cdr: ChangeDetectorRef

  ) {}

	ngOnInit() {

    console.log(this.rolenotloaded)
		this.allUserRoles$ = this.store.pipe(select(selectAllCompanyRoles));
		this.auth.getAllUserRoles().subscribe((res: Role[]) => {
			each(res, (_role: Role) => {
				this.allRoles.push(_role);
				this.unassignedRoles.push(_role);
        this.cdr.detectChanges();
      });

			each(this.rolesSubject.value, (roleId: number) => {
				const role = find(this.allRoles, (_role: Role) => {
					return _role.id === roleId;
				});

				if (role) {
					this.assignedRoles.push(role);
					remove(this.unassignedRoles, (el) => el.id === role.id);
				}
			});
		});
		if(this.unassignedRoles.length > 0 || this.assignedRoles.length > 0){
		  this.rolenotloaded = false;
      console.log(this.rolenotloaded)
    }



  }

	assignRole() {

		if (this.roleIdForAdding === 0) {
			return;
		}

		const role = find(this.allRoles, (_role: Role) => {
			return _role.id === (+this.roleIdForAdding);
		});

		if (role) {
			this.assignedRoles.push(role);
			remove(this.unassignedRoles, (el) => el.id === role.id);
			this.roleIdForAdding = 0;
			this.updateRoles();
		}
	}

	unassingRole(role: Role) {
		this.roleIdForAdding = 0;
		this.unassignedRoles.push(role);
		remove(this.assignedRoles, el => el.id === role.id);
		this.updateRoles();
	}

	updateRoles() {
		const _roles = [];
		each(this.assignedRoles, elem => _roles.push(elem.id));
		this.rolesSubject.next(_roles);
	}
}
