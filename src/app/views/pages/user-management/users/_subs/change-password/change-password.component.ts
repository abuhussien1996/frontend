// Angular
import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
// RxJS
import {BehaviorSubject} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
// Auth
import {AuthService, currentUser, User} from '../../../../../../core/auth/';
// State
import {AppState} from '../../../../../../core/reducers';
// Layout
import {LayoutUtilsService} from '../../../../../../core/_base/crud';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UserService} from '../../../../../../core/auth/_services';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';

export function ConfirmedValidator(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({confirmedValidator: true});
    } else {
      matchingControl.setErrors(null);
    }
  }
}

@Component({
  selector: 'kt-change-password',
  templateUrl: './change-password.component.html',
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangePasswordComponent implements OnInit {
  // Public properties
  @Input() userId: number;
  @Input() loadingSubject = new BehaviorSubject<boolean>(false);
  hasFormErrors = false;
  user: User;
  changePasswordForm: FormGroup;
  hide1
  hide2
  hide3
  passnotloaded = true
  viewCurrentPassword = false

  /**
   * Component constructor
   *
   * @param fb: FormBuilder
   * @param auth: AuthService
   * @param store: Store<AppState>
   * @param layoutUtilsService: LayoutUtilsService
   */
  constructor(private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private auth: AuthService,
              private snackBar: MatSnackBar,
              private store: Store<AppState>,
              private layoutUtilsService: LayoutUtilsService,
              private  userService: UserService, private translate: TranslateService
  ) {
    const roleType = this.auth.currentUserValue.typeRole
    let loggedInUserId = null
    this.store.pipe(select(currentUser)).subscribe((res) => {
      loggedInUserId = res.id.toString();
    });
    let pathUserId = null
    this.activatedRoute.params.subscribe(params => {
      pathUserId = params.id.toString();
    });
    if (((roleType === 'Super Admin' || roleType === 'Admin') && loggedInUserId === pathUserId) || roleType === 'User') {
      this.viewCurrentPassword = true
    }
  }

  /**
   * On init
   */
  ngOnInit() {
    this.loadData()
    this.createForm();
  }

  /**
   * Load data
   */
  loadData() {
    this.auth.getUserById(this.userId).subscribe(res => {
      this.user = res;

    });
  }

  createForm() {
    this.passnotloaded = false

    this.changePasswordForm = this.fb.group({
      oldPassword: new FormControl(''),

      password: new FormControl('', Validators.compose([
        Validators.required,
        // Validators.minLength(8),
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&#^])[A-Za-z\d$@$!%*?&#^].{8,}'),
        // Validators.maxLength(100)
      ])),
      confirmPassword: new FormControl('', Validators.compose([
        Validators.required,

      ])),


    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    });
    if (this.viewCurrentPassword) {
      this.changePasswordForm.get('oldPassword').setValidators([Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&#^])[A-Za-z\d$@$!%*?&#^].{8,}')]);
      this.changePasswordForm.get('oldPassword').updateValueAndValidity({emitEvent: false, onlySelf: true});
    }
  }

  get f() {
    return this.changePasswordForm.controls;
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.changePasswordForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  /**
   * Reset
   */
  reset() {
    this.hasFormErrors = false;
    this.loadingSubject.next(false);
    this.changePasswordForm.markAsPristine();
    this.changePasswordForm.markAsUntouched();
    this.changePasswordForm.updateValueAndValidity();
  }

  /**
   * Save data
   */
  onSubmit() {
    this.loadingSubject.next(true);
    this.hasFormErrors = false;
    const controls = this.changePasswordForm.controls;
    /** check form */
    if (this.changePasswordForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.hasFormErrors = true;
      this.loadingSubject.next(false);

      return;
    }

    this.userService.changePassword(this.user.userName, controls.oldPassword.value, controls.password.value).subscribe(res => {
      console.log(res)
      const x = this.translate.instant(res['message'])
      if (res['code'] === 200) {
        this.snackBar.open(x, 'action', {duration: 4000,});
      }
      if (res['code'] === 404) {
        this.snackBar.open(x, 'action', {duration: 4000,});
      }
    })


    this.loadData();
    this.loadingSubject.next(false);
    this.reset();
  }

  /**
   * Close alert
   *
   * @param _$event: Event
   */
  onAlertClose(_$event) {
    this.hasFormErrors = false;
  }

}

