// Angular
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
// RXJS
import { debounceTime, distinctUntilChanged, tap, skip, take, delay } from 'rxjs/operators';
import { fromEvent, merge, Observable, of, Subscription } from 'rxjs';
// LODASH
import { each, find } from 'lodash';
// NGRX
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../../../core/reducers';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

// Services
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../../../core/_base/crud';
// Models
import {
	User,
	Role,
	UsersDataSource,
	UsersPageRequested,
	selectAllCompanyRoles,
	selectUsersActionLoading,
	AuthService,
} from '../../../../../core/auth';
import { UserViewDialogComponent } from '../user-view/user-view-dialog.component';
import {
	UserOnServerDeleted,
	UserStatusOnServerUpdated,
	ManyUsersOnServerDeleted,
	UsersStatusOnServerUpdated
} from 'src/app/core/auth/_actions/user.actions';
import { selectErrorState } from 'src/app/core/auth/_selectors/user.selectors';

// Table with EDIT item in MODAL
// ARTICLE for table with sort/filter/paginator
// https://blog.angular-university.io/angular-material-data-table/
// https://v5.material.angular.io/components/table/overview
// https://v5.material.angular.io/components/sort/overview
// https://v5.material.angular.io/components/table/overview#sorting
// https://www.youtube.com/watch?v=NSt9CI3BXv4
@Component({
	selector: 'kt-users-list',
	templateUrl: './users-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent implements OnInit, OnDestroy {
	// Table fields
	dataSource: UsersDataSource;
	displayedColumns = ['select', 'userName', 'email', 'company', 'status', 'actions'];
	@ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
	@ViewChild('sort1', { static: true }) sort: MatSort;
	// Filter fields
	@ViewChild('searchInput', { static: true }) searchInput: ElementRef;
	lastQuery: QueryParamsModel;
	// Selection
	selection = new SelectionModel<User>(true, []);
	usersResult: User[] = [];
	allRoles: Role[] = [];
	allUsers: User[] = [];
	paginatorTotal;
	userNoResult = false;
	hasSubmissionErrors = false;
	submissionErrorObj: any;
	// Subscriptions
	private subscriptions: Subscription[] = [];
	// translate: any;
	filterStatus = '';
	newStatus: any;
	currentUserRoleType
	currentCompany

	/**
	 *
	 * @param activatedRoute: ActivatedRoute
	 * @param store: Store<AppState>
	 * @param router: Router
	 * @param translate: TranslateService
	 * @param layoutUtilsService: LayoutUtilsService
	 */
	constructor(
		private activatedRoute: ActivatedRoute,
		private store: Store<AppState>,
		private router: Router,
		private auth: AuthService,
		private translate: TranslateService,
		private dialog: MatDialog,
		private layoutUtilsService: LayoutUtilsService,
		private cdr: ChangeDetectorRef) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {


		// load roles list
		const rolesSubscription = this.store.pipe(select(selectAllCompanyRoles)).subscribe(res => this.allRoles = res);
		this.subscriptions.push(rolesSubscription);

		// If the user changes the sort order, reset back to the first page.
		const sortSubscription = this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));
		this.subscriptions.push(sortSubscription);

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		const paginatorSubscriptions = merge(this.sort.sortChange, this.paginator.page).pipe(
			tap(() => {
				this.loadUsersList();
			})
		)
			.subscribe();
		this.subscriptions.push(paginatorSubscriptions);


		// Filtration, bind to searchInput
		const searchSubscription = fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
			// tslint:disable-next-line:max-line-length
			debounceTime(150), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
			distinctUntilChanged(), // This operator will eliminate duplicate values
			tap(() => {
				this.paginator.pageIndex = 0;
				this.loadUsersList();
			})
		)
			.subscribe();
		this.subscriptions.push(searchSubscription);
		// // Init DataSource
		this.dataSource = new UsersDataSource(this.store);
		const entitiesSubscription = this.dataSource.entitySubject.pipe(
			skip(1),
			distinctUntilChanged()
		).subscribe(res => {
			this.usersResult = res;
		});
		this.subscriptions.push(entitiesSubscription);

		// // First Load
		of(undefined).pipe(take(1), delay(1000)).subscribe(() => { // Remove this line, just loading imitation
			this.loadUsersList();
		});

		this.currentUserRoleType = this.auth.currentUserValue.typeRole
		this.currentCompany = JSON.parse(localStorage.getItem('userInfo')).companyName
		console.log(this.currentCompany);


	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.subscriptions.forEach(el => el.unsubscribe());
	}

	/**
	 * Load users list
	 */
	loadUsersList() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(),
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex,
			this.paginator.pageSize
		);

		this.store.dispatch(new UsersPageRequested({ page: queryParams }));
		this.selection.clear();




	}

	filterConfiguration(): any {
		const filter: any = {};
		const searchText: string = this.searchInput.nativeElement.value;

		console.log(this.filterStatus)

		filter.isActive = this.filterStatus;
		filter.searchData = searchText
		filter.userName = searchText;
		filter.email = searchText;
		filter.company = searchText;

		console.log(filter);

		return filter;
	}

	/** ACTIONS */
	/**
	 * Delete user
	 *
	 * @param _item: User
	 */
	deleteUser(_item: User) {
		const _title = this.translate.instant('DIALOG.DELETE_USER_SINGLE');
		const _description = this.translate.instant('DIALOG.ARE_YOU_SURE_DELETE_USER');
		const _waitDesciption = this.translate.instant('DIALOG.DELETING_USER');
		const _deleteMessage = this.translate.instant('DIALOG.USER_HAVE_BEEN_DELETED');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.store.dispatch(new UserOnServerDeleted({ id: _item.id, companyID: _item.companyID }));
			const sub = this.store.pipe(select(selectUsersActionLoading)
			).subscribe(response => {
				if (response !== true) {
					this.store.pipe(
						select(selectErrorState)
					).subscribe(error => {
						if (error) {
							if (error.error.code) {
								this.submissionErrorObj = error.error.errors;
							} else {
								this.submissionErrorObj = [{ message: 'GLOBAL.UNSPECIFIED_ERROR' }]
							}
							this.hasSubmissionErrors = true;
						} else {
							this.submissionErrorObj = null;
							this.hasSubmissionErrors = false;
						}
						this.cdr.detectChanges();
					})
					console.log(this.submissionErrorObj);
					if (this.submissionErrorObj !== null) {
						const errArray = [];
						this.submissionErrorObj.forEach(errObj => {
							const message = this.translate.instant(errObj.message);
							errArray.push(message);
						});
						const errMsg = errArray.join('\n');
						this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
					} else {
						this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete, 3000, false, false);
						this.loadUsersList();
					}
					sub.unsubscribe();
				}
				this.cdr.detectChanges();
			});
		});
	}


	/**
	 * Fetch selected rows
	 */
	fetchUsers() {
		const messages = [];
		this.selection.selected.forEach(elem => {
			messages.push({
				text: `${elem.userName}, ${elem.email}`,
				id: elem.id.toString(),
				status: elem.userName
			});
		});
		this.layoutUtilsService.fetchElements(messages);
	}

	/**
	 * Check all rows are selected
	 */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.usersResult.length;
		return numSelected === numRows;
	}

	/**
	 * Toggle selection
	 */
	masterToggle() {
		if (this.selection.selected.length === this.usersResult.length) {
			this.selection.clear();
		} else {
			this.usersResult.forEach(row => this.selection.select(row));
		}
	}

	/* UI */
	/**
	 * Returns user roles string
	 *
	 * @param user: User
	 */
	getUserRolesStr(user: User): string {
		const titles: string[] = [];
		each(user.roles, (roleId: number) => {
			const _role = find(this.allRoles, (role: Role) => role.id === roleId);
			if (_role) {
				titles.push(_role.title);
			}
		});
		return titles.join(', ');
	}

	/**
	 * Redirect to edit page
	 *
	 * @param id: number
	 */
	editUser(id) {

		this.router.navigate(['../users/edit', id], { relativeTo: this.activatedRoute });
	}

	/** UI */
	/**
	 * Retursn CSS Class Name by status
	 *
	 * @param status: boolean
	 */
	getItemCssClassByStatus(status: boolean): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';

		}
		return '';
	}

	/**
	 * Returns Item Status in string
	 * @param status: boolean
	 */
	getItemStatusString(status: boolean): string {
		switch (status) {
			case false:
				return 'inActive';
			case true:
				return 'Active';

		}
		return '';
	}
	viewUser(user: User) {
		const _saveMessage = this.translate.instant('DIALOG.SAVE');
		const _messageType = user.id ? MessageType.Update : MessageType.Create;
		const dialogRef = this.dialog.open(UserViewDialogComponent, { data: { userId: user.id } });
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification(_saveMessage, _messageType, 5000, false, false);
			this.loadUsersList();
		});
	}

	updateSingleStatus(_item: User) {
		const _title: string = this.translate.instant('DIALOG.UPDATE_SINGLE_STATUS');
		const _description: string = this.translate.instant('DIALOG.ARE_YOU_SURE_TO_UPDATE_USER');
		const _waitDesciption: string = this.translate.instant('DIALOG.UPDATING_USER');
		const _updateMessage = this.translate.instant('DIALOG.STATUS_HAVE_BEEN_UPDATED');

		const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.store.dispatch(new UserStatusOnServerUpdated({
				id: _item.id,
				isActive: !_item.isActive
			}));
			const sub = this.store.pipe(select(selectUsersActionLoading)
			).subscribe(response => {
				if (response !== true) {
					this.store.pipe(
						select(selectErrorState)
					).subscribe(error => {
						if (error) {
							if (error.error.code) {
								this.submissionErrorObj = error.error.errors;
							} else {
								this.submissionErrorObj = [{ message: 'GLOBAL.UNSPECIFIED_ERROR' }]
							}
							this.hasSubmissionErrors = true;
						} else {
							this.submissionErrorObj = null;
							this.hasSubmissionErrors = false;
						}
					})
					if (this.submissionErrorObj !== null) {
						const errArray = [];
						this.submissionErrorObj.forEach(errObj => {
							const message = this.translate.instant(errObj.message);
							errArray.push(message);
						});
						const errMsg = errArray.join('\n');
						this.layoutUtilsService.showActionNotification(errMsg, MessageType.Update, 3000, false, false);
					} else {
						this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update, 3000, false, false);
					}
					sub.unsubscribe();
				}
			});
		});
	}



	deleteUsers() {
		const _title: string = this.translate.instant('DIALOG.DELETE_USER_MULTY');
		const _description: string = this.translate.instant('DIALOG.ARE_YOU_SURE_TO_DELETE_SELECTED_USER');
		const _waitDesciption: string = this.translate.instant('DIALOG.DELETING');
		const _deleteMessage = this.translate.instant('DIALOG.SELECTED_USERS_HAVE_BEEN_DELETED');

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			const idsForDeletion: any[] = [];
			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < this.selection.selected.length; i++) {
				idsForDeletion.push({ companyID: this.selection.selected[i].companyID, id: this.selection.selected[i].id });
			}
			this.store.dispatch(new ManyUsersOnServerDeleted({ ids: idsForDeletion }));
			const sub = this.store.pipe(select(selectUsersActionLoading)
			).subscribe(response => {
				if (response !== true) {
					this.store.pipe(
						select(selectErrorState)
					).subscribe(error => {
						if (error) {
							if (error.error.code) {
								this.submissionErrorObj = error.error.errors;
							} else {
								this.submissionErrorObj = [{ message: 'GLOBAL.UNSPECIFIED_ERROR' }]
							}
							this.hasSubmissionErrors = true;
						} else {
							this.submissionErrorObj = null;
							this.hasSubmissionErrors = false;
						}
						this.cdr.detectChanges();
					})
					if (this.submissionErrorObj !== null) {
						const errArray = [];
						this.submissionErrorObj.forEach(errObj => {
							const message = this.translate.instant(errObj.message);
							errArray.push(message);
						});
						const errMsg = errArray.join('\n');
						this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
					} else {
						this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete, 3000, false, false);
						this.loadUsersList();
					}
					sub.unsubscribe();
					this.selection.clear();
				}
				this.cdr.detectChanges();
			});
		});
	}

	updateStatusForUsers() {
		const _title = this.translate.instant('DIALOG.UPDATE_STATUS_MULTY');
		const _updateMessage = this.translate.instant('DIALOG.STATUS_HAVE_BEEN_UPDATED');
		const _statuses = [{ value: 0, text: 'Inactive' }, { value: 1, text: 'Active' }];
		const _messages = [];

		this.selection.selected.forEach(elem => {
			_messages.push({
				text: `${elem.userName}`,
				id: elem.id.toString(),
				status: elem.isActive,
				statusTitle: this.getItemStatusString(elem.isActive),
				statusCssClass: this.getItemCssClassByStatus(elem.isActive)
			});
		});

		const dialogRef = this.layoutUtilsService.updateStatusForEntities(_title, _statuses, _messages);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				this.selection.clear();
				return;
			}

			this.store.dispatch(new UsersStatusOnServerUpdated({
				isActive: +res,
				users: this.selection.selected
			}));
			const sub = this.store.pipe(select(selectUsersActionLoading)
			).subscribe(response => {
				if (response !== true) {
					this.store.pipe(
						select(selectErrorState)
					).subscribe(error => {
						if (error) {
							if (error.error.code) {
								this.submissionErrorObj = error.error.errors;
							} else {
								this.submissionErrorObj = [{ message: 'GLOBAL.UNSPECIFIED_ERROR' }]
							}
							this.hasSubmissionErrors = true;
						} else {
							this.submissionErrorObj = null;
							this.hasSubmissionErrors = false;
						}
					})
					if (this.submissionErrorObj !== null) {
						const errArray = [];
						this.submissionErrorObj.forEach(errObj => {
							const message = this.translate.instant(errObj.message);
							errArray.push(message);
						});
						const errMsg = errArray.join('\n');
						this.layoutUtilsService.showActionNotification(errMsg, MessageType.Update, 3000, false, false);
					} else {
						this.layoutUtilsService.showActionNotification(_updateMessage, MessageType.Update, 3000, false, false);
					}
					sub.unsubscribe();
					this.selection.clear();
				}
			});
			this.selection.clear();
		});
	}

}
