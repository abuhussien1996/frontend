// Angular
import {ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
// RxJS
import {BehaviorSubject, Observable, of, Subscription} from 'rxjs';
// NGRX
import {select, Store} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {AppState} from '../../../../../core/reducers';
// Layout
import {LayoutUtilsService, MessageType} from '../../../../../core/_base/crud';
import {TranslateService} from '@ngx-translate/core';

// Services and Models
import {AuthService, currentUser, selectUsersActionLoading, User, UserOnServerCreated} from '../../../../../core/auth';
import {CompanyModel} from 'src/app/core/management/_models/company.model';
import {UserService} from 'src/app/core/auth/_services/user.service';
import {OfficeModel} from 'src/app/core/management/_models/office.model';
import {selectErrorState} from 'src/app/core/auth/_selectors/user.selectors';
import {UserOnServerUpdated} from 'src/app/core/auth/_actions/user.actions';
import {SubheaderService} from '../../../../../core/_base/layout';
import {AuthServer} from '../../../../../core/enums/enums';


export function ConfirmedValidator(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({confirmedValidator: true});
    } else {
      matchingControl.setErrors(null);
    }
  }
}


@Component({
  selector: 'kt-user-edit',
  templateUrl: './user-edit.component.html',
})
export class UserEditComponent implements OnInit, OnDestroy {
  // Public properties
  user: User;
  AuthServer = AuthServer;
  userId$: Observable<number>;
  oldUser: User;
  selectedTab = 0;
  loading$: Observable<boolean>;
  rolesSubject = new BehaviorSubject<number[]>([]);
  newUser: any = {}
  userForm: FormGroup;
  hasFormErrors = false;
  hasSubmissionErrors = false;
  submissionErrorObj: any;
  hide: any
  private subscriptions: Subscription[] = [];
  offices: OfficeModel[]
  passwordinput: boolean;
  filteredOptions: Observable<string[]>;
  companies: CompanyModel[]
  currentUserRoleType
  currentUserCompany
  formnotloaded = true;
  formcreated = false
  withPhoto = false
  file = []
  attachments: string[] = [];
  files = [];
  @ViewChild('fileInput') fileInput: ElementRef;
  pathUserId: any;
  loggedInUser: any;
  currentUserId;
  disableBtn: boolean;

  /**
   * Component constructor
   *
   * @param activatedRoute: ActivatedRoute
   * @param router: Router
   * @param userFB: FormBuilder
   * @param layoutUtilsService: LayoutUtilsService
   * @param store: Store<AppState>
   * @param layoutConfigService: LayoutConfigService
   * @param cdr: ChangeDetectorRef
   */
  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private userFB: FormBuilder,
              private auth: AuthService,
              private userService: UserService,
              private translate: TranslateService,
              private layoutUtilsService: LayoutUtilsService,
              private store: Store<AppState>,
              private cdr: ChangeDetectorRef,
              public subheaderService: SubheaderService) {
  }

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */


  ngOnInit() {
    this.formcreated = false
    this.newUser.userName = 'New'
    this.newUser.profileUrl = ''
    this.store.pipe(select(currentUser)).subscribe((res) => {
      if (!res) {
        this.router.navigateByUrl('/');
      }
      this.loggedInUser = res !== undefined ? res : {};
    });
    this.userService.getAllCompanies().subscribe(res => {
      this.companies = res;
    })
    if (this.auth.currentUserValue.typeRole !== 'Super Admin' && this.auth.currentUserValue.typeRole !== 'User') {
      this.userService.getAllOffices().subscribe(res => {
        this.offices = res;
      })
    }
    this.loading$ = this.store.pipe(select(selectUsersActionLoading));
    const routeSubscription = this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      this.formnotloaded = true;
      this.pathUserId = id;
      if (id && id > 0) {
        this.userService.getUserById(id).subscribe(res => {
          if (res === undefined) {
            this.goBackWithId();
          } else if (res) {
            this.user = res;
            this.rolesSubject.next(this.user.roles);
            this.oldUser = Object.assign({}, this.user);

            this.initUser(this.user.id);
          }

          this.cdr.detectChanges();
        });
      } else {
        this.loading$ = of(false);
        this.user = new User();
        this.user.clear();
        this.rolesSubject.next(this.user.roles);
        this.oldUser = Object.assign({}, this.user);

        this.initUser(this.user.id);
      }
    });
    this.subscriptions.push(routeSubscription);
    this.currentUserRoleType = this.auth.currentUserValue.typeRole
    this.currentUserId = this.auth.currentUserId
    this.currentUserCompany = this.auth.currentUserValue.companyID

    // tslint:disable-next-line:triple-equals
    if (this.currentUserRoleType == 'User') {
      this.subheaderService.setBreadcrumbs([{
        title: 'My Profile',
        page: '/user-management/users/edit/' + this.pathUserId
      }])
    }

  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  /**
   * Init user
   */
  initUser(id) {
    this.createForm();
  }

  /**
   * Create form
   */
  createForm() {
    this.formnotloaded = false;
    const initialCompanyId = this.companies && this.companies.length > 0 ? this.companies[0].id : 0
    let companyId = this.auth.currentUserValue.typeRole !== 'Super Admin' && this.user.id !== 0 ? this.user.companyID : initialCompanyId
    if (this.auth.currentUserValue.typeRole !== 'SuperAdmin') {
      companyId = this.auth.currentUserValue.companyID
    } else if (this.user.id !== 0) {
      companyId = this.user.companyID
    } else {
      companyId = this.companies[0].id
    }

    this.userForm = this.userFB.group({

      userName: [this.user.userName, Validators.required],
      firstName: [this.user.firstName, Validators.required],
      lastName: [this.user.lastName, Validators.required],
      email: [this.user.email, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: [this.user.password, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*]).{8,}')],
      company: [companyId],
      primaryOffice: [this.user[`primaryOfficeiD`]],
      allowedOffices: [this.user.allowedOffices],
      isActive: [this.user.isActive, Validators.compose([
        Validators.required,
      ])],
      confirmPassword: [this.user.confirmPassword]


    }, {
      validator: ConfirmedValidator('password', 'confirmPassword')
    });
    if (this.auth.currentUserValue.typeRole !== 'Super Admin') {
      this.userForm.get('primaryOffice').setValidators([Validators.required]);
    }
    if (this.user.id === 0 || this.user.id === undefined) {
      if (this.auth.currentUserValue.typeRole === 'Super Admin') {
        this.userForm.get('company').setValidators([Validators.required]);
      }

    }
    this.formcreated = true
  }

  get f() {
    return this.userForm.controls;
  }

  /**
   * Redirect to list
   *
   */
  goBackWithId() {
    const url = `/user-management/users`;
    this.router.navigateByUrl(url, {relativeTo: this.activatedRoute});
  }

  /**
   * Refresh user
   *
   * @param isNew: boolean
   * @param id: number
   */
  refreshUser(isNew: boolean = false, id = 0) {
    let url = this.router.url;
    if (!isNew) {
      this.router.navigate([url], {relativeTo: this.activatedRoute});
      return;
    }

    url = `/user-management/users/edit/${id}`;
    this.router.navigateByUrl(url, {relativeTo: this.activatedRoute});
  }


  reset() {
    const _title = this.translate.instant('DIALOG.RESET');
    const _description = this.translate.instant('DIALOG.ARE_YOU_SURE');
    const _waitDesciption = this.translate.instant('DIALOG.RESETING');

    const dialogRef = this.layoutUtilsService.confirmAction(_title, _description, _waitDesciption);
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }


      this.user = Object.assign({}, this.oldUser);
      this.createForm();
      this.hasFormErrors = false;
      this.userForm.markAsPristine();
      this.userForm.markAsUntouched();
      this.userForm.updateValueAndValidity();
    });

  }

  onSumbit(withBack: boolean = false) {
    this.hasFormErrors = false;
    const controls = this.userForm.controls;
    /** check form */
    if (this.userForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );

      this.hasFormErrors = true;
      this.selectedTab = 0;
      return;
    }

    const editedUser = this.prepareUser();
    if (editedUser.id > 0) {
      this.updateUser(editedUser, withBack);
      return;
    }

    this.addUser(editedUser, withBack);
  }

  prepareUser(): User {
    const controls = this.userForm.controls;
    const _user = new User();
    _user.clear();
    _user.roles = this.rolesSubject.value;
    _user.id = this.user.id;
    _user.userName = controls.userName.value;
    _user.firstName = controls.firstName.value;
    _user.lastName = controls.lastName.value;
    _user.email = controls.email.value;
    _user.isActive = controls.isActive.value;
    _user.companyID = controls.company.value;
    _user.primaryOffice = controls.primaryOffice.value;
    // tslint:disable-next-line:max-line-length
    _user.allowedOffices = controls.allowedOffices.value === null || controls.allowedOffices.value === undefined ? [] : controls.allowedOffices.value;
    _user.password = controls.password.value;
    _user.confirmPassword = controls.confirmPassword.value;
    if (this.withPhoto) {
      _user.addProfile = true
    }
    return _user;
  }

  /**
   * Add User
   *
   * @param _user: User
   * @param withBack: boolean
   */
  addUser(_user: User, withBack: boolean = false) {
    if (_user.addProfile) {
      this.store.dispatch(new UserOnServerCreated({user: _user, file: this.file}));
    } else {
      this.store.dispatch(new UserOnServerCreated({user: _user}));
    }
    const addSubscription = this.store.pipe(
      select(selectUsersActionLoading)
    ).subscribe(res => {
      if (res !== true) {
        this.store.pipe(
          select(selectErrorState)
        ).subscribe(error => {
          if (error) {
            if (error.error.errors) {
              this.submissionErrorObj = error.error.errors;
            } else {
              this.submissionErrorObj = [{message: 'GLOBAL.UNSPECIFIED_ERROR'}]
            }
            this.hasSubmissionErrors = true;
          } else {
            const message = this.translate.instant('USER.ADDED');
            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, false, false);
            this.goBackWithId();
          }
          this.cdr.detectChanges();
        })
      }
      this.cdr.detectChanges();
    });
    this.subscriptions.push(addSubscription);
  }


  /**
   * Update user
   *
   * @param _user: User
   * @param withBack: boolean
   */
  updateUser(_user: User, withBack: boolean = false) {
    // Update User
    if (this.currentUserRoleType === 'User') {
      _user.id = null
    }
    const updatedUser: Update<User> = {
      id: _user.id,
      changes: _user
    };
    this.store.dispatch(new UserOnServerUpdated({
      partialUser: updatedUser,
      user: _user
    }));
    const updateSubscription = this.store.pipe(
      select(selectUsersActionLoading)
    ).subscribe(res => {
      if (res !== true) {
        this.store.pipe(
          select(selectErrorState)
        ).subscribe(error => {
          if (error) {
            if (error.error.errors) {
              this.submissionErrorObj = error.error.errors;
            } else {
              this.submissionErrorObj = [{message: 'GLOBAL.UNSPECIFIED_ERROR'}]
            }
            this.hasSubmissionErrors = true;
          } else {
            const message = this.translate.instant('USER.UPDATED');
            this.layoutUtilsService.showActionNotification(message, MessageType.Update, 5000, false, false);
          }
        })
      }
      this.cdr.detectChanges();
    });
    this.subscriptions.push(updateSubscription);
  }

  /**
   * Returns component title
   */
  getComponentTitle() {
    let result = 'Create ';
    if (!this.user || !this.user.id) {
      return result;
    }

    result = `Edit - ${this.user.userName}`;
    return result;
  }

  /**
   * Close Alert
   *
   * @param $event: Event
   */
  onAlertClose() {
    this.hasFormErrors = false;
  }

  /**
   * Checking control validation
   *
   * @param controlName: string => Equals to formControlName
   * @param validationType: string => Equals to valitors name
   */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.userForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  getStatusString(): string {
    switch (this.userForm.get('isActive').value) {
      case true:
        return this.translate.instant('GLOBAL.ACTIVE');
      case false:
        return this.translate.instant('GLOBAL.INACTIVE');
    }
    return '';
  }

  profileSelectedSuccess(value: any) {
    this.withPhoto = true
    this.file = value
  }

  onFileChange(event) {
    this.attachments = [];
    this.file = [];
    for (const file of event.target.files) {
      this.attachments.push(file.name);
      this.file.push(file);
      this.withPhoto = true
    }

  }

  resetFileInput() {
    this.attachments = [];
    this.file = [];
    this.fileInput.nativeElement.value = [];
    this.withPhoto = false
  }

  authAccount(hostServer) {
    this.disableBtn = true;
    this.userService.getOAuthURL(hostServer).subscribe(res => {
      this.user.inboxActivated = !this.user.inboxActivated
      window.location.href = res.auth_uri;
      this.disableBtn = false;
    });
  }

  disconnectAccount() {
    this.disableBtn = true;
    this.userService.disconnectFromAuthServer().subscribe(res => {
      this.disableBtn = false;
      this.user.inboxActivated = !this.user.inboxActivated;
      this.cdr.detectChanges();
    })
  }

  getText(textType: boolean, hostServer: AuthServer) {
    const text = {
      [AuthServer.GOOGLE]: 'Google',
      [AuthServer.MICROSOFT]: 'Microsoft'
    }
    if (textType === false) {
      return 'Connect with ' + text[hostServer]
    }
    return 'Disconnect Account ';// + text[hostServer]
  }

  isConnected() {
    if (this.user.inboxActivated) {
      return false;
    }
    return true;
  }

  connectionFunction(connection: boolean, hostServer: AuthServer) {
    connection ? this.authAccount(hostServer) : this.disconnectAccount()
  }
}

