// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// RxJS
import { Observable, of, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
// Services and Models
import {
	User,
	Permission,
	selectUserById,
	AuthService
} from '../../../../../core/auth';

@Component({
	selector: 'kt-user-edit-dialog',
	templateUrl: './user-view-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.Default,
})
export class UserViewDialogComponent implements OnInit, OnDestroy {
	// Public properties
	user: User;
	user$: Observable<User>;
	hasFormErrors = false;
	viewLoading = false;
	loadingAfterSubmit = false;
	allPermissions$: Observable<Permission[]>;
	userPermissions: Permission[] = [];
	// Private properties
	private componentSubscriptions: Subscription;

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<AdminProgramViewComponent>
	 * @param data: any
	 * @param store: Store<AppState>
	 */
	constructor(public dialogRef: MatDialogRef<UserViewDialogComponent>,
		private auth: AuthService,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private store: Store<AppState>) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
		if (this.data.userId) {
			this.user$ = this.store.pipe(select(selectUserById(this.data.userId)));
		} else {
			const newUser = new User();
			newUser.clear();
			this.user$ = of(newUser);
		}
		this.user$.subscribe(res => {
			if (!res) {
				return;
			}
			this.user = new User();
			this.user.id = res.id;
			this.user.firstName = res.firstName;
			this.user.lastName = res.lastName;
			this.user.userName = res.userName;
			this.user.email = res.email;
			this.user.isActive = res.isActive;
			this.user.companyID = res.companyID;
			this.user.company = res.company;
			// this.allPermissions$ = this.store.pipe(select(selectAllPermissions));
			// this.loadPermissions();
		});
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}

	prepareUser(): User {
		const _user = new User();
		_user.id = this.user.id;
		// _user.permissions = this.preparePermissionIds();
		// each(this.assignedUsers, (_user: User) => _user.users.push(_user.id));
		_user.firstName = this.user.firstName;
		_user.lastName = this.user.lastName;
		_user.userName = this.user.userName;
		_user.isActive = this.user.isActive;
		_user.companyID = this.user.companyID;
		_user.company = this.user.company;
		_user.email = this.user.email;
		return _user;
	}


	/** UI */
	/**
	 * Retursn CSS Class Name by status
	 *
	 * @param status: boolean
	 */
	getItemCssClassByStatus(status: boolean): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';

		}
		return '';
	}

	/**
	 * Returns Item Status in string
	 * @param status: boolean
	 */
	getItemStatusString(status: boolean): string {
		switch (status) {
			case false:
				return 'inActive';
			case true:
				return 'Active';
			default:
				return '';

		}
	}
}
