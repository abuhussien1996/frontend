// Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
// NGRX
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
// Translate
import {TranslateModule} from '@ngx-translate/core';
import {PartialsModule} from '../../partials/partials.module';
// Services
import {HttpUtilsService, TypesUtilsService, InterceptService, LayoutUtilsService} from '../../../core/_base/crud';
// Shared
import {
  ActionNotificationComponent,
  ConfirmActionDialogComponent,
  DeleteEntityDialogComponent,
  FetchEntityDialogComponent,
  UpdateStatusDialogComponent
} from '../../partials/content/crud';
// Components
import {UserManagementComponent} from './user-management.component';
import {UsersListComponent} from './users/users-list/users-list.component';
import {UserEditComponent} from './users/user-edit/user-edit.component';
import {UserViewDialogComponent} from './users/user-view/user-view-dialog.component';
import {RolesListComponent} from './roles/roles-list/roles-list.component';
import {RoleEditDialogComponent} from './roles/role-edit/role-edit.dialog.component';
import {UserRolesListComponent} from './users/_subs/user-roles/user-roles-list.component';
import {ChangePasswordComponent} from './users/_subs/change-password/change-password.component';
import {AddressComponent} from './users/_subs/address/address.component';

// Material
import {
  usersReducer,
  UserEffects,
  rolesReducer,
  RoleEffects, ModuleGuard
} from '../../../core/auth';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatRadioModule} from '@angular/material/radio';
import {MatIconModule} from '@angular/material/icon';
import {MatNativeDateModule} from '@angular/material/core';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {CompaniesService} from 'src/app/core/management/_services';
import { NgxPermissionsModule } from 'ngx-permissions';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {InlineSVGModule} from 'ng-inline-svg';
import { CopyRolePermissionDialogComponent } from './roles/copy-role-permission-dialog/copy-role-permission-dialog.component';
import {AuthInterceptor} from '../auth/auth.interceptor';

const routes: Routes = [
  {
    path: '',
    component: UserManagementComponent,
    children: [
      {
        path: '',
        redirectTo: 'roles',
        pathMatch: 'full'
      },
      {
        path: 'roles',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToRoleModule'},
        component: RolesListComponent
      },
      {
        path: 'users',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToUserManagementModule'},
        component: UsersListComponent
      },
      {
        path: 'users:id',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToUserManagementModule'},
        component: UsersListComponent
      },
      {
        path: 'users/add',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canAddUser'},
        component: UserEditComponent
      },
      {
        path: 'users/add:id',
        canActivate: [ModuleGuard],
        data: {permissionName: 'canAddUser'},
        component: UserEditComponent
      },
      {
        path: 'users/edit',
        component: UserEditComponent
      },
      {
        path: 'users/edit/:id',
        component: UserEditComponent
      },
    ]
  }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        PartialsModule,
        RouterModule.forChild(routes),
        StoreModule.forFeature('users', usersReducer),
        EffectsModule.forFeature([UserEffects]),
        StoreModule.forFeature('roles', rolesReducer),
        EffectsModule.forFeature([RoleEffects]),
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        MatButtonModule,
        MatMenuModule,
        MatSelectModule,
        MatInputModule,
        MatTableModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatIconModule,
        MatNativeDateModule,
        MatProgressBarModule,
        MatDatepickerModule,
        MatCardModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatExpansionModule,
        MatTabsModule,
        MatTooltipModule,
        MatDialogModule,
        MatSlideToggleModule,
        NgxPermissionsModule.forChild(),
        InlineSVGModule
    ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        hasBackdrop: true,
        panelClass: 'kt-mat-dialog-container__wrapper',
        height: 'auto',
        width: '900px'
      }
    },
    HttpUtilsService,
    TypesUtilsService,
    LayoutUtilsService,
    CompaniesService
  ],
  entryComponents: [
    ActionNotificationComponent,
    ConfirmActionDialogComponent,
    RoleEditDialogComponent,
    UserViewDialogComponent,
    DeleteEntityDialogComponent,
    UpdateStatusDialogComponent,
    FetchEntityDialogComponent,
    CopyRolePermissionDialogComponent
  ],
  declarations: [
    UserManagementComponent,
    UsersListComponent,
    UserEditComponent,
    RolesListComponent,
    RoleEditDialogComponent,
    UserRolesListComponent,
    ChangePasswordComponent,
    AddressComponent,
    UserViewDialogComponent,
    CopyRolePermissionDialogComponent
  ]
})
export class UserManagementModule {
}
