import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WorkflowListComponent} from './workflow-list/workflow-list.component';


const routes: Routes = [
  {
    path: '',
    component: WorkflowListComponent
  },
  {
    path: ':id',
    component: WorkflowListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkflowRoutingModule { }
