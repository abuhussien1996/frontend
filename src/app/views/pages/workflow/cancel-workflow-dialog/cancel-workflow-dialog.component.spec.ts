import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelWorkflowDialogComponent } from './cancel-workflow-dialog.component';

describe('CancelWorkflowDialogComponent', () => {
  let component: CancelWorkflowDialogComponent;
  let fixture: ComponentFixture<CancelWorkflowDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelWorkflowDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelWorkflowDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
