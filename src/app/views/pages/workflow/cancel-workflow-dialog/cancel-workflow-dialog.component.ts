import {Component, Inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {forkJoin} from 'rxjs';
import {WorkflowService} from '../_services/workflow.service';

@Component({
  selector: 'kt-cancel-workflow-dialog',
  templateUrl: './cancel-workflow-dialog.component.html',
  styleUrls: ['./cancel-workflow-dialog.component.scss']
})
export class CancelWorkflowDialogComponent implements OnInit {
  viewLoading: boolean;
  formGroup: FormGroup;
  hasFormErrors: boolean;

  constructor(
    public dialogRef: MatDialogRef<CancelWorkflowDialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private workflowService: WorkflowService,
    private formBuilder: FormBuilder
    ) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
      this.formGroup = this.formBuilder.group({
        reason: ['', Validators.required],
        confirmation: ['yes']
      });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
      const control = this.formGroup.controls[controlName];
      if (!control) {
          return false;
      }
      const result = control.hasError(validationType) && (control.dirty || control.touched);
      return result;
  }

  onSubmit() {
      this.hasFormErrors = false;
      const controls = this.formGroup.controls;
      /** check form */
      if (this.formGroup.invalid) {
          Object.keys(controls).forEach(controlName =>
              controls[controlName].markAsTouched()
          );
          this.hasFormErrors = true;
          return;
      }
      this.viewLoading = true;
      this.proceed(this.formGroup.controls.reason.value);
  }

  onNoClick() {
    console.log(this.formGroup.controls);
    this.close(false);
  }

  proceed(reason) {
    if(!this.data.recreate) {
      this.workflowService.cancelWorkflow(this.data.workflowQueueId, reason).subscribe(e=>{
        this.workflowService.getCancelAPIs(this.data.workflowId).subscribe(res => {
          const apis = res.data;
          if(apis.length > 0) {
            const observables = [];
            for (const api of apis) {
              observables.push(this.workflowService.executeApi(
                api.http_method,
                api.url,
                api.payload,
                this.data.inputData,
                this.data.workflowQueueId,
                0
              ));
            }
            forkJoin(observables).subscribe(() => {
              this.close(true);
            });
          } else {
            this.close(true);
          }
        });
      });
    } else {
      this.dialogRef.close({cancelled: true, reason});
    }
  }

  close(val){
    this.dialogRef.close({
      cancelled: val,
    });
  }
}
