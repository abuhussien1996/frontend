import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {WorkflowService, WorkflowStatuses} from '../_services/workflow.service';
import {CreateWorkflowDialogComponent} from '../create-workflow-dialog/create-workflow-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {LayoutUtilsService, MessageType} from '../../../../core/_base/crud';
import {ConfirmActionDialogComponent} from '../../../partials/content/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {currentUser, User} from 'src/app/core/auth';
import {Subscription} from 'rxjs';
import {UserService} from '../../../../core/auth/_services';

@Component({
  selector: 'kt-workflow-accordion',
  templateUrl: './workflow-accordion.component.html',
  styleUrls: ['./workflow-accordion.component.scss']
})
export class WorkflowAccordionComponent implements OnInit, OnDestroy , AfterViewInit {

  @Input() statusesList;
  @Input() workflow;
  @Input() header;
  @Input() users;
  @Input() entities;
  @Input() drugs;
  @Input() contacts;
  @Input() userSymbol;
  @Input() checkValues;
  @Output() refreshWorkflowList = new EventEmitter<any>();
  hasDescription = true
  locked = false;
  lockedBy = '';
  currentUser;
  subscriptions: Subscription[] = [];
  userExist = false
  userProfile
  workflowEntities = []
  workflowDrugs = []
  workflowDoctors = []
  workflowPatients = []
  workflowClients = []
  workflowFacilities = []
  workflowPharmacies = []
  workflowPharmacyGroups = []
  workflowContacts = []

  constructor(
    public dialog: MatDialog,
    private workflowService: WorkflowService,
    private layoutUtilsService: LayoutUtilsService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
  ) {
  }
  ngAfterViewInit(): void {
  }
  ngOnInit(): void {
    if(!this.header) {
      this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe(res => this.currentUser = res.userName));
      this.locked = this.workflow.workflow_queue.lock
      && this.workflow.workflow_queue.locked_by.trim() !== this.currentUser.trim() ? true : false;
      if (this.locked) {
        this.lockedBy = this.workflow.workflow_queue.locked_by
      }
      if (
        this.checkValues
        && this.checkValues.workflow_queue_id === this.workflow.workflow_queue.id
        && this.checkValues.workflow_action_id === this.workflow.workflow_action.id
      ) {
        this.openEditFlowModal();
      }
      if (this.users && this.users.length > 0) {
        const u = this.users.find(e => e.userName === this.workflow.last_modified_user)
        if (u) {
          this.userProfile = u
          this.userExist = true
          this.cdr.detectChanges()
        }
        this.workflowContacts = this.getArrayForPrint(this.workflowContacts,'CONTACT')
        this.workflowDrugs = this.getArrayForPrint(this.workflowDrugs,'DRUG')
        this.workflowClients = this.getArrayForPrint(this.workflowClients,'CLIENT')
        this.workflowDoctors = this.getArrayForPrint(this.workflowDoctors,'DOCTOR')
        this.workflowPatients = this.getArrayForPrint(this.workflowPatients,'PATIENT')
        this.workflowPharmacies = this.getArrayForPrint(this.workflowPharmacies,'PHARMACY')
        this.workflowPharmacyGroups = this.getArrayForPrint(this.workflowPharmacyGroups,'PHARMACY_GROUP')
        this.workflowFacilities = this.getArrayForPrint(this.workflowFacilities,'FACILITY')
      }
    }
  }
  getArrayForPrint(arr,arrName){
    const current = this.workflow.workflow_queue.workflow_queue_entities
    current.forEach(element => {
      if(element && element.type === arrName)
        arr = arr.concat(element.ids)
      arr = this.arrayUnique(arr)
    })
    return arr
  }
  arrayUnique(array) {
    const a = array.concat();
    for(let i=0; i<a.length; ++i) {
      for(let j=i+1; j<a.length; ++j) {
        if(a[i] === a[j])
          a.splice(j--, 1);
      }
    }
    return a;
  }

  getEntity(entity) {
    const res = '';
    if (!entity) {
      return res;
    } else {
      const u = this.entities.find(e => e.id === entity)
      if (u) {
        return '' + this.getTip(u.first_name, u.last_name)
      }
      return res;
    }
  }
  getContact(entity) {
    const res = '';
    if (!entity) {
      return res;
    } else {
      const u = this.contacts.find(e => e.id === entity)
      if (u) {
        return '' + this.getTip(u.first_name, u.last_name)
      }
      return res;
    }
  }
  getDrug(entity) {
    const res = '';
    if (!entity) {
      return res;
    } else {
      const u = this.drugs.find(e => e.id === entity)
      if (u) {
        return '' + this.getTip(u.drug,'')
      }
      return res;
    }
  }
  getTip(last, first) {
    let res = ''
    if (last) {
      res = res + last
    }
    if (first) {
      res = res + ' ' + first
    }
    return res
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getAllAtt(obj) {
    let str = ' '
    // tslint:disable-next-line:forin
    for (const item in obj) {
      str += obj[item] + ' ';
    }
    return str;
  }

  getClassBasedOnStatus() {
    let classSet = 'border-left rounded-left border-warning';
    switch (this.workflow.workflow_queue.status) {
      case WorkflowStatuses.IN_PROGRESS:
        classSet += 'border-warning'
        break;
      case WorkflowStatuses.PENDING:
        classSet += 'border-secondary'
        break;
      case WorkflowStatuses.READY:
        classSet += 'border-primar'
        break;
      case WorkflowStatuses.REVIEW:
        classSet += 'border-success'
        break;
    }
    return classSet
  }

  getHeader() {
    let current = ''
    const jsonObject = JSON.parse(this.workflow.workflow_queue.input_data);
    // tslint:disable-next-line:forin
    for (const item in jsonObject) {
      current += this.getAllAtt(jsonObject[item].attributes)
    }
    return current
  }

  openEditFlowModal() {
    const dialogRef = this.dialog.open(CreateWorkflowDialogComponent, {
      data: {
        workflow: this.workflow,
        isNew: false,
        header: this.getHeader(),
        statuses:this.statusesList
      },
      width: '100vw',
      maxWidth: 'unset !important',
      disableClose: true,
      hasBackdrop: true,
      closeOnNavigation: true,
      autoFocus: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && (result.reloadWorkflow || result.cancelledWorkflowId)) {
        this.refreshWorkflowList.emit({
          workflow_action_id: result.workflow_action_id,
          workflow_queue_id: result.workflow_queue_id
        });
      }
    });
  }

  unlockCard() {
    const dialogRef = this.dialog.open(ConfirmActionDialogComponent, {
      data: {
        title: 'Unlock Card',
        description: 'Are you sure to force unlock the card?',
        waitDesciption: 'Unlocking ...'
      },
      width: '440px'
    });
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.workflowService.lockWorkflow(this.workflow.workflow_queue.id, false).subscribe(() => {
        this.locked = false;
        this.workflow.workflow_queue.lock = true;
        this.layoutUtilsService.showActionNotification('Workflow successfully Unlocked', MessageType.Read, 3000, false, false);
        setTimeout(() => {
          this.openEditFlowModal();
        }, 500);
        this.cdr.detectChanges();
      });
    });
  }

  clickPin(workflowQueue) {
    this.workflowService.updateExpedite(workflowQueue.id, !workflowQueue.expedite_pin).subscribe(res => {
      this.refreshWorkflowList.emit({});
    })
  }

  getPinCSS(workflowQueue: any) {
    if (!workflowQueue.expedite_pin) {
      return 'btn svg-icon svg-icon-sd p-0  pr-3'
    }
    return 'btn svg-icon svg-icon-warning svg-icon-sd p-0  pr-3'
  }
  getCategoryName(category){
    if(category){
      return category.category_name
    }
  }
}
