import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {WorkflowService} from '../../_services/workflow.service';
import {map} from 'rxjs/operators';
import {of} from 'rxjs';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {UserService} from 'src/app/core/auth/_services';
import {ActivatedRoute, Router} from '@angular/router';
import {WorkflowFilterComponent} from './workflow-filter.component';

@Component({
  selector: 'kt-dynamic-filters',
  templateUrl: './dynamic-filters.component.html',
  styleUrls: ['./workflow-filter.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class DynamicFiltersComponent implements OnInit {
  searchForm: FormGroup;
  filterByFilter;
  filteredFilter
  @ViewChild('filterInput') filterInput: ElementRef<HTMLInputElement>;
  filterList: any = [];
  @Input() filters;
  filt
  @Output() getMyFilters = new EventEmitter<any>();
  selected
  constructor(
    private userService: UserService,
    private workflowService: WorkflowService,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.filt = this.filters
    this.searchForm = this.fb.group({
      filterControl: [],

    })

  }


  selectedFilter(event: MatAutocompleteSelectedEvent, display,type): void {
    console.log(display)
    this.filterList.push(event.option.value);
    if (this.filterInput && (display[0] || display[1])){
      const x = display[0] ? event.option.value[display[0]] : '';
      const y = display[1] ? event.option.value[display[1]] : ''
      const z = x+' '+y
      this.filterInput.nativeElement.value = z
      this.selected = event.option.value
    }
    const obj = { name: type ,value: this.selected? this.selected.id:''}
    if(obj && obj.name && obj.value){
      this.getMyFilters.emit(obj)
    }
    this.selected = null
  }

  getName(value, display) {
    if (display[0] || display[1]){
      const x = display[0] ? value[display[0]] : '';
      const y = display[1] ? value[display[1]] : ''
      const z = x+' '+y
      return z;
    }
  }

  filterInputChanged(value, url, term,type) {
    console.log(value, url)
    if (value  && value !== ''  && url && term) {
      this.filteredFilter = this.workflowService.search(url, value, term).pipe(map(res => res.data.filter((r) => {
        return !this.filterList.find((e: any) => e.id === r.id);
      })));
    }

  }

  displayFnFilter(event: any): string {
    return event ? event.name : '';
  }

  remove(type): void {
    if(this.filterInput && this.filterInput.nativeElement ){
      if(this.filterInput.nativeElement.value === ''){
        return
      }
      this.filterInput.nativeElement.value = null;
      this.filteredFilter = null;
      const obj = { name: type ,value: null}
      if(obj && obj.name ){
        this.getMyFilters.emit(obj)
      }
    }
  }
}
