import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {WorkflowService} from '../../_services/workflow.service';
import {Observable} from 'rxjs';
import {MatOption} from '@angular/material/core';
import {UserService} from 'src/app/core/auth/_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'kt-workflow-filter',
  templateUrl: './workflow-filter.component.html',
  styleUrls: ['./workflow-filter.component.scss']
})
export class WorkflowFilterComponent implements OnInit {
  status = new FormControl();
  @Input() statusList;
  @Input() categoryList;
  @Input() filters;
  @Input() inputs;
  @Input() icon = 'flaticon2-search-1';
  @Input() useSVG: boolean;
  contactName: any;
  userField: any;
  filteredEntities: Observable<[]>;
  filteredUsers: Observable<[]>;
  fromDate = new FormControl();
  toDate = new FormControl();
  category = new FormControl();
  contactField = new FormControl();
  private contact: MatOption;
  private user: MatOption;
  parent = this;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onFilterChange = new EventEmitter<any>();
  private workflowId: any;
  workflow: any
  schema: any
  filtersArray = []
  private myfilters: any[] = [];
show
  constructor(
    private userService?: UserService,
    private workflowService?: WorkflowService,
    private cdr?: ChangeDetectorRef,
    private activatedRoute?: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
this.show = false
    const routeSubscription = this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.workflowId = params.id;
        this.workflowService.loadAllWorkflows().subscribe(res => {
          this.workflow = res.data.find(x => x.id.toString() === this.workflowId)
          this.schema = this.workflow ? this.workflow.schema : null
          const objectSchema = JSON.parse(this.schema)
          if (objectSchema !== null && objectSchema !== undefined){
            this.filtersArray = []
            const indexs = objectSchema ? Object.keys(objectSchema):null
            for (let i=0;i<indexs.length;i++){
              this.filtersArray.push(objectSchema[indexs[i]])
            }
          }
        })
      }
    });

  }



  choosedDate($event: any) {
    this.dispatchEvent()
  }



  filterChanged() {
    this.dispatchEvent();
  }

  dispatchEvent() {
    const filter = [];
    filter[0]={name: 'from_date', value : this.fromDate.value}
    filter[1]={name: 'to_date',  value : this.toDate.value}
    filter[2]={name: 'status' ,  value :this.status.value}
    filter[3]={name: 'category_id' ,  value :this.category.value}
    if (this.category.value === 'true'){
      filter[3]={name: 'without_category' ,  value :this.category.value}
    }
    if(this.myfilters){
      for(let i=0; i<this.myfilters.length ; i++){
          filter[i+4]= { name: this.myfilters[i].name, value : this.myfilters[i].value}
      }
    }
    this.onFilterChange.emit(filter)
    console.log(filter)
  }

  filterChanges(value: any) {
    for(let i=0; i<this.myfilters.length ; i++){
      if(this.myfilters &&this.myfilters[i] && this.myfilters[i].name === value.name){
        this.myfilters[i].value = value.value
        this.dispatchEvent();
        return;
      }
    }
    this.myfilters.push(value);
    console.log(this.myfilters)
    this.dispatchEvent();
  }

  startFilter(d: Date | null): boolean {
    const date = (d || new Date());
    // Prevent Saturday and Sunday from being selected.
    if (this.toDate && this.toDate.value) {
      return date.getTime() < this.toDate.value.getTime();
    }
    return true;
  }

  toFilter(d: Date | null): boolean {
    const date = (d || new Date());
    // Prevent Saturday and Sunday from being selected.
    if (this.fromDate && this.fromDate.value) {
      return date.getTime() > this.fromDate.value.getTime();
    }
    return true;
  }


}
