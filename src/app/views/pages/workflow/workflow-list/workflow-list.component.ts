import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {CreateWorkflowDialogComponent} from '../create-workflow-dialog/create-workflow-dialog.component';
import {WorkflowService} from '../_services/workflow.service';
import {ActivatedRoute} from '@angular/router';
import {pipe, Subject, Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {selectLatestCardValues} from 'src/app/core/events';
import {UpdateCardValues} from 'src/app/core/events/_actions/inbox.actions';
import {UserService} from '../../../../core/auth/_services';
import {TaskService} from '../../../../core/timeline/_services';
import {CategoryService, ContactsService} from '../../../../core/lists/_services';
import {ProjectService} from '../../management/services/project.service';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'kt-workflow-list',
  templateUrl: './workflow-list.component.html',
  styleUrls: ['./workflow-list.component.scss']
})
export class WorkflowListComponent implements OnInit, OnDestroy, OnChanges {
  form = new FormGroup({});
  model: any = {};
  readOnly = false;
  listOfWorkflows = [];
  private ktDialog: any;
  isLoading = true;
  noDataAvailable: any;
  private filters: any = {};
  private workflowId: any;
  statusList = new Subject<any>();
  statusesList = []
  categoryList = [];
  workflowStatusesList: any;
  checkValues = null;
  private subscriptions: Subscription[] = [];
  showList = false
  showKampain = true
  workflowGrid
  userProfile
  usersObj
  entitiesObj = []
  contactsObj = []
  projectsObj = []
  drugsObj = []
  contactIds = []
  drugIds = []
  projectIds =[]
  entitiesIds = []
  initialEntity = []
  listOfLastModified = []
  entitiesLoading = true
  firstLoaded = true
  @Output() totalCount = new EventEmitter<any>();
  private loadWorkflowList$: Subscription;

  ngOnChanges() {
  }

  constructor(public dialog: MatDialog,
              private workflowService: WorkflowService,
              private cdr: ChangeDetectorRef,
              private activatedRoute: ActivatedRoute,
              private store: Store<AppState>,
              private userService: UserService,
              private taskService: TaskService,
              private contactsService: ContactsService,
              private categoriesService: CategoryService,
              private projectsService: ProjectService
  ) {
    this.ktDialog = new KTDialog({type: 'loader', placement: 'top center', message: 'Loading ...'});
  }

  ngOnInit(): void {
    this.getCategories()
    this.subscriptions.push(this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.workflowId = params.id;
      }
      if (this.workflowId) {
        this.loadWorkflows();
        this.prepareStatusesList();
      }
    }));
    this.subscriptions.push(this.store.pipe(select(selectLatestCardValues)).subscribe((res) => {
      if (res && res.workflow_queue_id !== null) {
        this.checkValues = {
          workflow_action_id: res.workflow_action_id,
          workflow_queue_id: res.workflow_queue_id
        }
        this.store.dispatch(new UpdateCardValues({
          new_values: {
            group_id: null,
            workflow_queue_id: null,
            workflow_action_id: null
          }
        }))
      }
    }));
  }

  openAddFlowModal() {
    const dialogRef = this.dialog.open(CreateWorkflowDialogComponent, {
      data: {
        data: null,
        isNew: true,
        workflowId: this.workflowId
      },
      width: '600px',
      maxWidth: 'unset !important',

    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.reloadWorkflow) {
        this.checkValues = {
          workflow_action_id: result.workflow_action_id,
          workflow_queue_id: result.workflow_queue_id
        }
        this.loadWorkflows();
        this.cdr.detectChanges();
      }
    });
  }
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  loadWorkflows() {
    this.isLoading = true;
    this.listOfWorkflows = [];

    if (this.loadWorkflowList$ && this.loadWorkflowList$.unsubscribe) {
      this.loadWorkflowList$.unsubscribe();
      this.loadWorkflowList$ = undefined;
    }
    this.loadWorkflowList$ = this.workflowService.getWorkflowsList(this.workflowId, this.filters).subscribe(response => {
      if (response.data.length === 0) {
        this.noDataAvailable = true;
      } else {
        this.noDataAvailable = false;
        this.workflowGrid = response.data
        this.workflowGrid.forEach((element) => {
          this.listOfLastModified.push(element.last_modified_user);
        });
        this.workflowGrid.forEach((element) => {
          this.initialEntity.push(element.workflow_queue);
        });
        this.getEntities(this.initialEntity)
        this.userService.getUserByUserName(this.listOfLastModified).subscribe(res => {
            this.usersObj = []
            this.usersObj = res.pageOfItems
          }
        )

        response.data = response.data.sort((x, y) => {
          const d1 = new Date(x.workflow_queue.created);
          const d2 = new Date(y.workflow_queue.created);
          return x.workflow_queue.expedite_pin === y.workflow_queue.expedite_pin
            ? d1 > d2 ? 1 : -1 : x.workflow_queue.expedite_pin ? -1 : 1;
        });
        response.data.forEach(workflow => {
          if (!this.listOfWorkflows[workflow.workflow_queue.status]) {
            this.listOfWorkflows[workflow.workflow_queue.status] = [];
          }
          this.listOfWorkflows[workflow.workflow_queue.status].push(workflow)
        })
        this.firstLoaded = false
      }
      this.isLoading = false;
      this.cdr.detectChanges();
      const totalLength = (response && response.data ? response.data.length : 0) - (this.listOfWorkflows['DONE'] ? this.listOfWorkflows['DONE'].length : 0)
      this.totalCount.emit(totalLength > 0 ? totalLength : 0)

    });

  }

  refreshWorkflowList(checkValues?) {
    this.checkValues = checkValues;
    this.loadWorkflows();
    this.cdr.detectChanges();
  }

  filterChanges(value: any) {
    this.filters = null
    this.filters = value;
    this.refreshWorkflowList();
    // setTimeout(() => this.refreshWorkflowList(), 1500);
  }

  private prepareStatusesList() {
    this.workflowService.getWorkflowStatusesList(this.workflowId).subscribe(response => {
      const statuses = [];
      this.workflowStatusesList = [];
      response.data.map((status) => {
        this.workflowStatusesList.push(status.status);
        this.statusesList.push(status)
        statuses.push(
          {
            label: status.status,
            value: status.number
          }
        )
      });
      this.statusList.next(statuses);
    })
  }

  showKampainOnClick() {
    this.showKampain = true
    this.showList = false
    this.prepareStatusesList()
  }

  showListOnClick() {
    if (this.entitiesLoading || this.firstLoaded) {
      return
    }
    this.showList = true;
    this.showKampain = false
  }

  getEntities(data) {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      const obj = data[i].workflow_queue_entities
      obj.forEach(element => {
        if (element && element.type === 'PROJECT')
          this.projectIds = this.arrayUnique(this.projectIds.concat(element.ids))
        if (element && element.type === 'CONTACT')
          this.contactIds = this.arrayUnique(this.contactIds.concat(element.ids))
        if (element && element.type === 'DRUG')
          this.drugIds = this.arrayUnique(this.drugIds.concat(element.ids))
        if (element && (element.type !== 'DRUG' && element.type !== 'CONTACT'))
          this.entitiesIds = this.arrayUnique(this.entitiesIds.concat(element.ids))
      })
    }
    if (this.entitiesIds.length > 0) {
      this.taskService.getEntities(this.entitiesIds).subscribe(res => {
        this.entitiesObj = []
        this.entitiesObj = res.data
        this.cdr.detectChanges()
      })
    }
    if (this.contactIds.length > 0) {
      this.contactsService.getContactsByIds(this.contactIds).subscribe(res => {
        this.contactsObj = []
        this.contactsObj = res.data
        this.cdr.detectChanges()
      })
    }
    if (this.projectIds.length > 0) {
      this.projectsService.getProjectsByIds(this.projectIds).subscribe(res => {
        this.projectsObj = []
        this.projectsObj = res.data
        this.cdr.detectChanges()
      })
    }
    setTimeout(() => this.entitiesLoading = false, 1000);
  }

  arrayUnique(array) {
    const a = array.concat();
    for (let i = 0; i < a.length; ++i) {
      for (let j = i + 1; j < a.length; ++j) {
        if (a[i] === a[j])
          a.splice(j--, 1);
      }
    }
    return a;
  }

  getCategories() {
    this.categoriesService.getCategories().subscribe(res => {
      if (res)
        this.categoryList = res.data
    })
  }
}
