import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {WorkflowService, WorkflowStatuses} from '../_services/workflow.service';
import {CreateWorkflowDialogComponent} from '../create-workflow-dialog/create-workflow-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {LayoutUtilsService, MessageType} from '../../../../core/_base/crud';
import {ConfirmActionDialogComponent} from '../../../partials/content/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {currentUser} from 'src/app/core/auth';
import {Subscription} from 'rxjs';

@Component({
  selector: 'kt-workflow-card',
  templateUrl: './workflow-card.component.html',
  styleUrls: ['./workflow-card.component.scss']
})
export class WorkflowCardComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input() statusesList;
  @Input() workflow;
  @Input() checkValues;
  @Output() refreshWorkflowList = new EventEmitter<any>();
  hasDescription = true
  locked = false;
  lockedBy = '';
  currentUser;
  subscriptions: Subscription[] = [];

  constructor(
    public dialog: MatDialog,
    private workflowService: WorkflowService,
    private layoutUtilsService: LayoutUtilsService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe(res => this.currentUser = res.userName));
    this.locked = this.workflow.workflow_queue.lock
    && this.workflow.workflow_queue.locked_by.trim() !== this.currentUser.trim() ? true : false;
    if (this.locked) {
      this.lockedBy = this.workflow.workflow_queue.locked_by
    }
    if (
      this.checkValues
      && this.checkValues.workflow_queue_id === this.workflow.workflow_queue.id
      && this.checkValues.workflow_action_id === this.workflow.workflow_action.id
    ) {
      this.openEditFlowModal();
    }
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  getNextStateString() {
    return 'Move To Next';
  }


  getDescription() {
    let result = ''
    if (this.workflow.workflow_queue.input_data) {
      try {
        const jsonObject = JSON.parse(this.workflow.workflow_queue.input_data);
        // tslint:disable-next-line:forin
        for (const item in jsonObject) {
          result += '<p class="h7 text-capitalize mb-1 "  style="font-weight: 400;">' +
            '' + this.getAllAtt(jsonObject[item].attributes) + '</p>';
        }
        return result
      } catch (objError) {
        this.hasDescription = false
        return '<p>No data</p>'
      }
    }
    this.hasDescription = false
    return '<p>No data </p>'
  }

  getAllAtt(obj, withItem = false) {
    let str = ' '
    // tslint:disable-next-line:forin
    for (const item in obj) {
      if (withItem) {
        // str += '<span class="text-muted mr-3 font-weight-bold font-size-sm">'
        str += '<span class="text-primary text-dark-50 ">' + ' ' + obj[item] + '</span>'
        // str += item[0].toUpperCase() + item.substr(1).toLowerCase().replace('_',' ') + ': <span class="text-primary font-weight-bolder">'+' '+obj[item]+'</span></span>
        continue
      }
      str += obj[item] + '  ';
    }
    return str;
  }

  getClassBasedOnStatus() {
    let classSet = 'border-left rounded-left border-warning';
    switch (this.workflow.workflow_queue.status) {
      case WorkflowStatuses.IN_PROGRESS:
        classSet += 'border-warning'
        break;
      case WorkflowStatuses.PENDING:
        classSet += 'border-secondary'
        break;
      case WorkflowStatuses.READY:
        classSet += 'border-primar'
        break;
      case WorkflowStatuses.REVIEW:
        classSet += 'border-success'
        break;
    }
    return classSet
  }

  getHeader() {
    let current = '<div class="d-flex align-items-center flex-wrap">'
    const jsonObject = JSON.parse(this.workflow.workflow_queue.input_data);
    // tslint:disable-next-line:forin
    for (const item in jsonObject) {
      // current += '<div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">'
      current += '<span class="text-muted mr-3 font-weight-bold font-size-sm">' +
        item[0].toUpperCase() + item.substr(1).toLowerCase() + ': '
      // current += '<div class="d-flex align-items-center flex-lg-fill mr-5">'
      current += this.getAllAtt(jsonObject[item].attributes, true)
      current += ' </span>'
    }
    return current + ' </div>';
  }

  openEditFlowModal() {
    const dialogRef = this.dialog.open(CreateWorkflowDialogComponent, {
      data: {
        workflow: this.workflow,
        isNew: false,
        header: this.getHeader(),
        statuses: this.statusesList
      },
      width: '100vw',
      maxWidth: 'unset !important',
      disableClose: true,
      hasBackdrop: true,
      closeOnNavigation: true,
      autoFocus: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && (result.reloadWorkflow || result.cancelledWorkflowId)) {
        this.refreshWorkflowList.emit({
          workflow_action_id: result.workflow_action_id,
          workflow_queue_id: result.workflow_queue_id
        });
      }
    });
  }

  unlockCard() {
    const dialogRef = this.dialog.open(ConfirmActionDialogComponent, {
      data: {
        title: 'Unlock Card',
        description: 'Are you sure to force unlock the card?',
        waitDesciption: 'Unlocking ...'
      },
      width: '440px'
    });
    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        return;
      }
      this.workflowService.lockWorkflow(this.workflow.workflow_queue.id, false).subscribe(() => {
        this.locked = false;
        this.workflow.workflow_queue.lock = true;
        this.layoutUtilsService.showActionNotification('Workflow successfully Unlocked', MessageType.Read, 3000, false, false);
        setTimeout(() => {
          this.openEditFlowModal();
        }, 500);
        this.cdr.detectChanges();
      });
    });
  }

  clickPin(workflowQueue) {
    this.workflowService.updateExpedite(workflowQueue.id, !workflowQueue.expedite_pin).subscribe(res => {
      this.refreshWorkflowList.emit({});
    })
  }

  getPinCSS(workflowQueue: any) {
    if (!workflowQueue.expedite_pin) {
      return 'btn svg-icon svg-icon-sd p-0 pl-3'
    }
    return 'btn svg-icon svg-icon-warning svg-icon-sd p-0 pl-3'
  }

  getHeaderStyle(old_workflow_queue_id: any) {
    if (old_workflow_queue_id) {
      return "ribbon-target bg-success";
    }
    return "ribbon-target bg-primary";
  }
}
