/* tslint:disable:no-string-literal */
import {ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup} from '@angular/forms';
import {WorkflowService} from '../_services/workflow.service';
import {map} from 'rxjs/operators';
import {forkJoin, Observable, Subscription} from 'rxjs';
import {LayoutUtilsService, MessageType} from 'src/app/core/_base/crud';
import {FormioComponent} from 'angular-formio';
import {select, Store} from '@ngrx/store';
import {AppState} from 'src/app/core/reducers';
import {selectLastCreatedContact} from 'src/app/core/lists';
import {CancelWorkflowDialogComponent} from '../cancel-workflow-dialog/cancel-workflow-dialog.component';
import {AuthService, currentUser} from 'src/app/core/auth';
import * as moment from 'moment';
import {CommentsService} from 'src/app/core/services/comments.service';
import {CommunicationsService, TaskService} from 'src/app/core/timeline/_services';
import {UserService} from '../../../../core/auth/_services';
import * as _ from 'lodash';
import {CreateTaskComponent} from '../../calendar/create-task/create-task.component';
import {ChooseTemplateDialogComponent} from '../../calendar/choose-template/choose-template.dialog.component';
import {TaskTimelineComponent} from '../../calendar/task-timeline/task-timeline.component';

@Component({
  selector: 'kt-create-workflow-dialog',
  templateUrl: './create-workflow-dialog.component.html',
  styleUrls: ['./create-workflow-dialog.component.scss']
})
export class CreateWorkflowDialogComponent implements OnInit {
  currentId: number;
  parentId: number;
  rightPanelId: number;
  viewLoading = true;
  loadingAfterSubmit = false;
  form = new FormGroup({});
  model: any = {};
  options = {};
  modelMappedData = {data: {}};
  fields = {};
  isCreate = false;
  formValue = {};
  private workflowResponse: any;
  private isNewWorkflow: any;
  showReadOnlyData: boolean;
  isDone: boolean;
  readOnlyField: any;
  readOnlyValue: any;
  isLoading: any;
  isSubmitting = false;
  selectingWorkflow = true;
  workflow: any;
  project: any;
  workflowsList: Observable<any>;
  private workflowId: any;
  private projectId: any;
  loadingWorkflowData = false;
  attachments = [];
  index = 0;
  viewAttachments = true;
  newPatient;
  newContact;
  keepOpen = true;
  subscriptions: Subscription[] = [];
  comments = [];
  editCommentValue;
  currentUser;
  commentCount: number;
  history = [];
  isSubmittingComment: boolean;
  @ViewChild('formio', {static: false}) formRight: FormioComponent;
  currentStatusNumber: any;
  maxStatusNumber: any;
  projectAutocompleteCall = this.projectService.searchProjects.bind(this.projectService);
  isFirst = false
  task = {}
  readOnlyProject = true
  isFormReadOnly = true
  leftFormIdForSave
  leftFormInputDataForSave

  constructor(
    public dialogRef: MatDialogRef<CreateWorkflowDialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private workflowService: WorkflowService,
    private cdr: ChangeDetectorRef,
    private layoutUtilsService: LayoutUtilsService,
    private commentsService: CommentsService,
    private communicationsService: CommunicationsService,
    private projectService: WorkflowService,
    private userService: UserService,
    private auth: AuthService,
    private taskService: TaskService,
    private store: Store<AppState>) {
    this.workflow = new FormControl();
    this.project = new FormControl();
  }

  ngOnInit(): void {
    if (!this.data.isNew) {
      this.getComments();
      this.getHistory();
    }
    const contactSub = this.store.pipe(select(selectLastCreatedContact)).subscribe(res => this.newContact = res);
    this.subscriptions.push(contactSub);
    this.subscriptions.push(this.store.pipe(select(currentUser)).subscribe(res => this.currentUser = res.userName));
    if (this.data.workflowId) {
      this.workflowId = this.data.workflowId;
    }
    if (this.data.workflow) {
      this.currentId = this.data.workflow.workflow_action.id;
      this.parentId = this.data.workflow.workflow_action.parent_id;
      this.attachments = this.data.workflow.workflow_queue.attachments;
    }

    this.isNewWorkflow = this.data.isNew;
    this.isLoading = true;
    if (!this.data.isNew) {
      this.currentStatusNumber = this.data.statuses.find(e => e.status === this.getTitle())
      this.maxStatusNumber = _.maxBy(this.data.statuses, 'number')
      if (Number(this.maxStatusNumber.number) === Number(this.currentStatusNumber.number)) {
        this.isDone = true
      }
    }
    if (this.isNewWorkflow) {
      this.isCreate = true;
      this.workflowsList = this.workflowService.getAvailableWorkflowInGroup(this.workflowId).pipe(map(res => res.data));
      this.workflow.valueChanges.subscribe((id: string | null) => {
        if (id) {
          this.readOnlyProject = false
          this.workflowId = id;
        }
      });
      this.project.valueChanges.subscribe((id: string | null) => {
        if (id !== '') {
          this.initCreateWorkflow();
          this.prepareReadOnlyDataForCreateWorkflowStep(this.project.value.data)
          this.projectId = id;
          return;
        }
        this.fields = {}
        this.modelMappedData = {data: {}}
      })
    } else {
      this.workflowId = this.data.workflow.workflow_action.workflow_id
      this.projectId = this.data.workflow.workflow_queue.workflow_queue_entities[0].ids[0]
      this.initUpdateWorkflow();
    }
    // this.leftFormIdForSave = this.data.workflow.workflow_action.id
    // this.leftFormInputDataForSave = this.data.workflow.input_data
  }

  saveTasksInForm(formSide) {
    const body = {
      input_data: JSON.stringify(this.model),
      move_to_next: false,
      entities: [{
        id: this.projectId && this.projectId.id? this.projectId.id : this.projectId,
        type: 'PROJECT'
      }],
      workflow_action_id: this.workflowResponse.data.id
    }
    if (!this.isCreate) {
      body['workflow_queue_id'] = this.data.workflow.workflow_queue.id
    }
    if(formSide === 'left'){
      body['workflow_action_id'] = this.currentId
      body['input_data'] = JSON.stringify(this.leftFormInputDataForSave)
    }
    this.workflowService.saveStepData(body, this.workflowId).subscribe(() => {
    })
  }
  onSubmit(moveToNext) {
    this.isFirst = true
    this.isSubmitting = true;
    const body = {
      input_data: JSON.stringify(this.model),
      move_to_next: moveToNext,
      entities: [{
        id: this.projectId && this.projectId.id? this.projectId.id : this.projectId,
        type: 'PROJECT'
      }],
      workflow_action_id: this.workflowResponse.data.id
    }

    if (!this.isCreate) {
      body['workflow_queue_id'] = this.data.workflow.workflow_queue.id
    }
    if (this.isCreate) {
      const card_data = {}
      // tslint:disable-next-line:forin
      for (const item in this.model) {
        card_data[item] = {}
        card_data[item][item] = this.model[item]
      }
      body['card_data'] = JSON.stringify(card_data)
    }

    if ((moveToNext && this.formRight.formio.checkValidity()) || !moveToNext) {
      this.workflowService.saveStepData(body, this.workflowId).subscribe((res) => {
        if (this.workflowResponse.data.apis !== null && this.workflowResponse.data.apis.length > 0) {
          const observables = [];
          const startWorkflowApis = this.workflowResponse.data.apis.filter(api => api.execution_type === 'startWorkflow');
          if (startWorkflowApis.length > 0) {
            this.executeStartWorkflowApis(startWorkflowApis);
          }
          const apis = moveToNext ?
            this.workflowResponse.data.apis.filter(api => api.execution_type === 'SaveAndNext' || api.execution_type === 'SaveANDSaveAndNext')
            : this.workflowResponse.data.apis.filter(api => api.execution_type === 'SAVE' || api.execution_type === 'SaveANDSaveAndNext');
          if (apis.length > 0) {
            for (const api of apis) {
              if (
                api.condition === undefined ||
                api.condition === null ||
                this.canExecuteAPI(this.model, api.condition)
              ) {
                if (this.workflowResponse.data.status === 'NOTIFY CLIENT' && api.url.includes('timeline/companies/13/communication?communication_type=data.communicationType')) {
                  this.sendToPharmacyEmail(api, res);
                } else {
                  observables.push(this.workflowService.executeApi(
                    api.http_method,
                    api.url,
                    api.payload,
                    this.model,
                    this.data.workflow.workflow_queue.id,
                    this.data.workflow.workflow_queue.received_id
                  ));
                }
              }
            }
            if (observables.length > 0) {
              forkJoin(observables).subscribe(() => {
                this.close({
                  reloadWorkflow: true,
                  workflow_action_id: this.keepOpen ? res.data.workflow_action_id : null,
                  workflow_queue_id: this.keepOpen ? res.data.workflow_queue_id : null
                });
              });
            } else {
              this.close({
                reloadWorkflow: true,
                workflow_action_id: this.keepOpen ? res.data.workflow_action_id : null,
                workflow_queue_id: this.keepOpen ? res.data.workflow_queue_id : null
              });
            }
          } else {
            this.close({
              reloadWorkflow: true,
              workflow_action_id: this.keepOpen ? res.data.workflow_action_id : null,
              workflow_queue_id: this.keepOpen ? res.data.workflow_queue_id : null
            });
          }
        } else {
          this.close({
            reloadWorkflow: moveToNext,
            workflow_action_id: this.keepOpen ? res.data.workflow_action_id : null,
            workflow_queue_id: this.keepOpen ? res.data.workflow_queue_id : null
          });
        }
      }, err => {
        this.layoutUtilsService.showActionNotification(
          'someone else opening the card will reload the view',
          MessageType.Read,
          3000,
          false,
          false
        );
        setTimeout(() => {
          this.close({
            reloadWorkflow: true,
          }, false);
        }, 1000);
      });
    } else {
      this.isSubmitting = false;
      this.isFirst = false
      const errMsg = 'Invalid Form Submission. Please check before submitting again';
      this.layoutUtilsService.showActionNotification(errMsg, MessageType.Delete, 3000, false, false);
    }
  }

  private initCreateWorkflow() {
    this.isCreate = true;
    this.loadingWorkflowData = true;
    this.workflowService.getWorkflowsStepData(0, 0, this.workflow.value).subscribe(response => {
      this.workflowResponse = response;
      this.fields = JSON.parse(response.data.form.schema);
      this.isLoading = false;
      this.loadingWorkflowData = false;
    })
  }

  private initUpdateWorkflow() {
    this.loadingWorkflowData = true;
    if (!this.isDone) {
      this.workflowService.getWorkflowsStepData(this.data.workflow.workflow_action.id,
        this.data.workflow.workflow_queue.id, this.workflowId).subscribe(response => {
        this.showReadOnlyData = true;
        this.isLoading = false;
        this.loadingWorkflowData = false;
        this.rightPanelId = response.data.parent_id;
        this.prepareReadOnlyData();
        this.workflowResponse = response;
        this.fields = JSON.parse(response.data.form.schema);
        this.prepareOldData();
        this.onLockWorkflowClick();

      }, error => {
        this.layoutUtilsService.showActionNotification('someone else opening the card will reload the view',
          MessageType.Read, 3000, false, false);
        setTimeout(() => {
          this.close({
            reloadWorkflow: true,
            workflow_action_id: null,
            workflow_queue_id: null
          }, false);
        }, 1000);
      })
    } else {
      this.rightPanelId = this.currentId;
      this.showReadOnlyData = true;
      this.isLoading = false;
      this.loadingWorkflowData = false;
      this.prepareReadOnlyData();
    }
  }

  private prepareReadOnlyData() {
    const jsonObject = JSON.parse(this.data.workflow.form_schema);
    const jsonValue = JSON.parse(this.data.workflow.input_data);
    this.readOnlyField = jsonObject;
    this.readOnlyValue = {data: jsonValue};
    for (const attr in jsonValue) {
      if (attr === 'task') {
        this.isFormReadOnly = false
      }
    }
    this.cdr.detectChanges()
  }

  private prepareReadOnlyDataForCreateWorkflowStep(input_data) {
    const jsonValue = JSON.parse(input_data);
    this.modelMappedData = {data: jsonValue};
    this.isFirst = true
    this.cdr.detectChanges();
  }

  changeReadOnlyForm(direction) {
    const action_id = direction === 'PREV' ? this.parentId : this.currentId;
    this.workflowService.getWorkflowsStepData(
      action_id,
      this.data.workflow.workflow_queue.id,
      this.workflowId,
      direction
    ).subscribe((res) => {
      this.readOnlyField = JSON.parse(res.data.form.schema);
      this.readOnlyValue = {data: JSON.parse(res.data.input_data)};
      this.isFormReadOnly = true
      for (const attr in JSON.parse(res.data.input_data)) {
        if (attr === 'task') {
          this.isFormReadOnly = false
        }
      }
      this.currentId = res.data.id;
      this.parentId = res.data.parent_id;
      this.cdr.detectChanges();
    })
  }

  onChange(event, form) {
    if (event.data)
      this.model = event.data
  }

  onChangeLeftSide(event) {
    if (event.data)
      this.leftFormInputDataForSave = event.data
  }

  private prepareOldData() {
    if (!this.workflowResponse.data.input_data && this.workflowResponse.data.mapping_template) {
      const mapping = JSON.parse(this.workflowResponse.data.mapping_template.template)
      mapping.forEach((item) => {
        this.modelMappedData.data[item.destination] = this.readOnlyValue.data[item.source];
        if (item.destination === 'task') {
          this.modelMappedData.data[item.destination] = this.modelMappedData.data[item.destination].filter(a => a.id)
        }
      })
    }
    const jsonData = JSON.parse(this.workflowResponse.data.input_data);
    // tslint:disable-next-line:forin
    for (const attr in jsonData) {
      this.modelMappedData.data[attr] = jsonData[attr];
      if (attr === 'task') {
        this.modelMappedData.data[attr] = this.modelMappedData.data[attr].filter(a => a.id)
      }
    }
  }

  onCancelClick(): void {
    this.close({
      reloadWorkflow: true
    });
  }

  getFullName(obj) {
    return obj.first_name + ' ' + obj.last_name
  }

  displayContact(entity: any) {
    if (entity) {
      return entity.first_name + ' ' + entity.last_name;
    }
  }

  Next() {
    if (this.index < this.attachments.length) {
      this.index++;
    }
  }

  Previous() {
    if (this.index > 0) {
      this.index--;
    }
  }

  getTitle() {
    return this.data.workflow.workflow_action.status;
  }

  openTimeLine(id) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    dialogConfig.data = {
      task_id: id
    }
    this.dialog.open(TaskTimelineComponent, dialogConfig);
  }

  handleFormEvent(event) {
    switch (event.type) {
      case 'openTimeLine':
        this.openTimeLine(event.data.id);
        break;
      case 'editTask':
        this.editTask(event.data.id,'right');
        break;
      default:
        break;
    }
  }

  handleFormEventOnLeft(event) {
    switch (event.type) {
      case 'openTimeLine':
        this.openTimeLine(event.data.id);
        break;
      case 'editTask':
        this.editTask(event.data.id,'left');
        break;
      default:
        break;
    }
  }

  onCancelWorkflowClick() {
    const dialogRef = this.dialog.open(CancelWorkflowDialogComponent, {
      data: {
        workflowQueueId: this.data.workflow.workflow_queue.id,
        inputData: this.model,
        workflowId: this.workflowId
      },
      width: '600px',
      maxWidth: 'unset !important'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cancelled) {
        this.close({
          cancelledWorkflowId: this.workflowId,
        });
      }
    });
  }

  onLockWorkflowClick() {
    this.workflowService.lockWorkflow(this.data.workflow.workflow_queue.id, true).subscribe(() => {
      this.data.workflow.workflow_queue.lock = true;
    });

  }

  close(obj, callLockUnlock = true) {
    if (callLockUnlock && !this.isCreate) {
      this.onUnLockWorkflowClick();
    }
    this.dialogRef.close(obj);
  }

  onUnLockWorkflowClick() {
    this.workflowService.lockWorkflow(this.data.workflow.workflow_queue.id, false).subscribe(() => {
      this.data.workflow.workflow_queue.lock = false;
    });
  }

  cancelAndRecreate() {
    const dialogRef = this.dialog.open(CancelWorkflowDialogComponent, {
      data: {
        workflowQueueId: this.data.workflow.workflow_queue.id,
        recreate: true,
        inputData: this.model,
        workflowId: this.workflowId
      },
      width: '600px',
      maxWidth: 'unset !important'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cancelled) {
        this.workflowService.getFirstAction(this.workflowId, this.data.workflow.workflow_queue.id).subscribe((res1) => {
          const firstAction = res1.data;
          const body = {
            form_schema: firstAction.form.schema,
            input_data: firstAction.input_data,
            move_to_next: true,
            workflow_action_id: firstAction.id,
            old_workflow_queue_id: this.data.workflow.workflow_queue.id,
            received_id: this.data.workflow.workflow_queue.received_id
          }
          this.workflowService.saveStepData(body, this.workflowId).subscribe((res2) => {
            const newItem = {
              id: this.data.workflow.workflow_queue.received_id,
              workflow_id: this.workflowId,
              workflow_queue_id: res2.data.workflow_queue_id
            }
            const observables = [];
            if (this.data.workflow.workflow_queue.received_id) {
              observables.push(this.workflowService.updateItem(newItem));
            }
            observables.push(this.workflowService.postAttachments(
              newItem.workflow_queue_id,
              this.data.workflow.workflow_queue.attachments)
            );
            if (firstAction.apis !== null && firstAction.apis.length > 0) {
              for (const api of firstAction.apis) {
                if (
                  api.condition === undefined ||
                  api.condition === null ||
                  this.canExecuteAPI(JSON.parse(firstAction.input_data), api.condition)
                ) {
                  observables.push(this.workflowService.executeApi(
                    api.http_method,
                    api.url,
                    api.payload,
                    JSON.parse(firstAction.input_data),
                    res2.data.workflow_queue_id,
                    0
                  ));
                }
              }
            }
            forkJoin(observables).subscribe(() => {
              this.workflowService.cancelWorkflow(this.data.workflow.workflow_queue.id, result.reason).subscribe(() => {
                this.close({
                  reloadWorkflow: true,
                  workflow_action_id: firstAction.id,
                  workflow_queue_id: newItem.workflow_queue_id
                });
              });
            });
          });
        });
      }
    });
  }

  loadOldWorkflowData() {
    this.workflowService.getWorkflowsStepData(
      this.workflowResponse.data.id,
      this.data.workflow.workflow_queue.old_workflow_queue_id,
      this.workflowId,
      'CURRENT'
    ).subscribe((res) => {
      if (res.data && res.data.input_data !== null) {
        this.modelMappedData = {data: JSON.parse(res.data.input_data)};
      }
    });
  }

  executeStartWorkflowApis(apis) {
    for (const api of apis) {
      if (
        api.condition === undefined ||
        api.condition === null ||
        this.canExecuteAPI(this.model, api.condition)
      ) {
        const workflowId = parseInt(api.url.substring(api.url.lastIndexOf('/') + 1), 10);
        this.workflowService.getFirstAction(workflowId, null).subscribe((res1) => {
          const firstAction = res1.data;
          this.workflowService.executeApi(
            api.http_method,
            api.url,
            api.payload,
            this.model,
            0,
            0
          ).subscribe((res2) => {
            if (firstAction.apis !== null && firstAction.apis.length > 0) {
              const observables = [];
              for (const firstActionApi of firstAction.apis) {
                if (
                  firstActionApi.condition === undefined ||
                  firstActionApi.condition === null ||
                  this.canExecuteAPI(this.model, firstActionApi.condition)
                ) {
                  observables.push(this.workflowService.executeApi(
                    firstActionApi.http_method,
                    firstActionApi.url,
                    firstActionApi.payload,
                    this.model,
                    res2.data.workflow_queue_id,
                    0
                  ));
                }
              }
              forkJoin(observables).subscribe(() => {
              });
            }
          });
        });
      }
    }
  }

  getMomentDate(date) {
    return moment(new Date(date.concat(' GMT'))).fromNow();
  }

  getHistory() {
    let users = []
    let resultOfHistory = []

    this.workflowService.getHistory(this.data.workflow.workflow_queue.id).subscribe((res) => {
      if (res && res.data) {
        resultOfHistory = res.data;
        for (const c of resultOfHistory) {
          users.push(c.userName)
        }
        if (users.length > 0)
          this.userService.getUserByUserName(users).subscribe(result => {
            if (result.code === 200) {
              users = result.pageOfItems
              for (const c of resultOfHistory) {
                for (const x of users) {
                  if (x.userName === c.userName) {
                    c.profileUrl = x.profileUrl;
                  }
                }
              }
              this.history = resultOfHistory
            }
          });
      }
    });
  }

  getComments() {
    let users = []
    let resultOfComment = []

    this.commentsService.getComments('workflow_queues', this.data.workflow.workflow_queue.id).subscribe((res) => {
      if (res && res.data) {
        resultOfComment = res.data;
        for (const c of resultOfComment) {
          c.editMode = false;
          users.push(c.last_modified_user)
        }
        this.commentCount = res.count;
      }
      if (users.length > 0)
        this.userService.getUserByUserName(users).subscribe(result => {
          if (result.code === 200) {
            users = result.pageOfItems
            for (const c of resultOfComment) {
              c.userName = c.last_modified_user
              for (const x of users) {
                if (x.userName === c.last_modified_user) {
                  c.profileUrl = x.profileUrl;
                }
              }
            }
            this.comments = resultOfComment
            this.comments.sort((a: any, b: any) => {
              // @ts-ignore
              return new Date(b.updated) - new Date(a.updated);

            })
          }
        });
    });
  }

  sendToPharmacyEmail(api, res) {
    const attachments = this.attachments.filter((a) => a.uuid.includes('.pdf'));
    const arr = attachments.map((a) => a.uuid.substring(a.uuid.lastIndexOf('/') + 1));
    this.workflowService.executeApi(
      api.http_method,
      api.url,
      api.payload,
      this.model,
      this.data.workflow.workflow_queue.id,
      this.data.workflow.workflow_queue.received_id,
      arr
    ).subscribe(async (result) => {
      if (result.data) {
        const obs = [];
        for (const [i, url] of result.data.pre_signed_urls.entries()) {
          const file = await fetch(attachments[i].url).then(r => r.blob()).then(blobFile => new File([blobFile], arr[i], {type: 'application/pdf'}));
          obs.push(this.communicationsService.uploadFiles(url.url, file));
        }
        forkJoin(obs).subscribe(() => {
          const uuids = result.data.pre_signed_urls.map((a) => a.uuid);
          this.communicationsService.sendFiles(result.data.id, uuids).subscribe(() => {
            this.close({
              reloadWorkflow: true,
              workflow_action_id: this.keepOpen ? res.data.workflow_action_id : null,
              workflow_queue_id: this.keepOpen ? res.data.workflow_queue_id : null
            });
          })
        });
      }
    })
  }

  canExecuteAPI(data: any, conditionStr: string) {
    try {
      const stack = [];
      const array = conditionStr.split(';');
      array.forEach((item) => {
        item = item.trim();
        if (item.startsWith('{')) {
          stack.push(this.evaluateCondition(data, JSON.parse(item)));
        } else {
          const a = stack.pop();
          const b = stack.pop();
          let c;
          switch (item) {
            case '&&' :
              c = a && b;
              break;
            case '||' :
              c = a || b;
              break;
          }
          stack.push(c)
        }
      });
      const res = stack.pop();
      return typeof res === 'boolean' ? res : false;
    } catch (e) {
      return false;
    }
  }

  getDescendantProp(obj, desc) {
    const arr = desc.split('.');
    while (arr.length) {
      obj = obj[arr.shift()];
    }
    return obj;
  }

  evaluateCondition(data, obj) {
    const a = this.getDescendantProp(data, obj[`field`]);
    const b = obj[`value`];
    switch (obj[`op`]) {
      case '=' :
        return a === b;
      case '>' :
        return a > b;
      case '<' :
        return a < b;
      case '>=' :
        return a >= b;
      case '<=' :
        return a <= b;
      case '!=' :
        return a !== b;
    }
  }

  getWorkflowName() {
    return this.data.workflow.workflow_queue.workflow.name;
  }

  filterProjectFn(project: any): string {
    let res = ''
    if (project) {
      res += project.name
    }
    return res
  }

  onProjectChange(val) {
    this.project.setValue(val);
    this.project.updateValueAndValidity();
  }

  createTask(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '30vw';
    dialogConfig.data = {
      action_queue_id: data.workflow.id,
      workflow_queue_id: data.workflow.workflow_queue.id,
      project_id: data.workflow.workflow_queue.workflow_queue_entities[0] ?
        data.workflow.workflow_queue.workflow_queue_entities[0].ids[0] : null,
      fromWorkflow: true
    }
    const taskRef = this.dialog.open(ChooseTemplateDialogComponent, dialogConfig);
    taskRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      const newTask = result.task.map(value => {
        return {
          id: value.id,
          description: value.description,
          title: value.title,
          priority: value.priority
        }
      });
      this.modelMappedData.data['task'].push(newTask[0]);
      this.modelMappedData.data['task'] = this.modelMappedData.data['task'].filter(a => a.id)
      this.modelMappedData = {data: this.modelMappedData.data};
      this.cdr.detectChanges()
      this.saveTasksInForm('right')
      this.ngOnInit()
    })
  }

  editTask(id,formSide) {
    this.taskService.getTaskById(id).subscribe(result => {
      if (result.data && result.data.id) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.closeOnNavigation = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '40vw';
        dialogConfig.data = {mode: 'update', task: result.data}
        const dialogRef = this.dialog.open(CreateTaskComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(res => {
          if(res) {
            if (formSide === 'right') {
              this.modelMappedData = {data: this.modelMappedData.data};
              this.modelMappedData = {data: this.updateJson(id, res.task.data, formSide)}
              this.saveTasksInForm(formSide)
              this.cdr.detectChanges()
              return;
            }
            this.readOnlyValue = {data: this.readOnlyValue.data};
            this.readOnlyValue = {data: this.updateJson(id, res.task.data, formSide)}
            this.saveTasksInForm(formSide)
            this.cdr.detectChanges()
          }
        })
      }
    })
  }

  updateJson(id, newTask,formSide) {
    let jsonObj = this.modelMappedData.data['task']
    if(formSide === 'left'){
      jsonObj = this.readOnlyValue.data['task']
    }
    const task = {
      task: []
    }
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < jsonObj.length; i++) {
      if (jsonObj[i].id === id) {
        jsonObj[i].title = newTask.title;
        jsonObj[i].description = newTask.description;
        jsonObj[i].priority = newTask.priority;
        task.task = jsonObj;
        return task
      }
    }
  }
}
