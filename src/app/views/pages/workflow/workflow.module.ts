import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatGridListModule} from '@angular/material/grid-list';
import {InlineSVGModule} from 'ng-inline-svg';

import {WorkflowRoutingModule} from './workflow-routing.module';
import {WorkflowListComponent} from './workflow-list/workflow-list.component';
import {WorkflowAccordionComponent} from './workflow-accordion/workflow-accordion.component';
import {WorkflowCardComponent} from './workflow-card/workflow-card.component';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CreateWorkflowDialogComponent} from './create-workflow-dialog/create-workflow-dialog.component';
import {WorkflowService} from './_services/workflow.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatFormioModule} from 'angular-material-formio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {WorkflowFilterComponent} from './workflow-list/workflow-filter/workflow-filter.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';

import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormioModule} from 'angular-formio';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import {DynamicFiltersComponent} from './workflow-list/workflow-filter/dynamic-filters.component';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import { CreateContactDialogComponent } from '../../partials/content/general/contact-add-dialog/contact-add.dialog.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  ContactEffects,
  contactsReducer,
} from 'src/app/core/lists';
import { OfficesService } from 'src/app/core/management';
import {CancelWorkflowDialogComponent} from './cancel-workflow-dialog/cancel-workflow-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {ConfirmActionDialogComponent} from '../../partials/content/crud';
import {NgxPermissionsModule} from 'ngx-permissions';
import { TranslateModule } from '@ngx-translate/core';
import { inboxReducer } from 'src/app/core/events';
import {ErrorInterceptor} from '../../../core/auth/_helpers/error.interceptor';
import {AuthInterceptor} from '../auth/auth.interceptor';
import { CoreModule } from 'src/app/core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { CommentsService } from 'src/app/core/services/comments.service';
import {MatExpansionModule} from '@angular/material/expansion';
import {TaskModule} from '../task/task.module';

@NgModule({
  entryComponents: [
    ConfirmActionDialogComponent,
    CreateWorkflowDialogComponent,
    CreateContactDialogComponent,
    CancelWorkflowDialogComponent
  ],
  declarations: [
    WorkflowListComponent,
    WorkflowCardComponent,
    WorkflowAccordionComponent,
    CreateWorkflowDialogComponent,
    WorkflowFilterComponent,
    CancelWorkflowDialogComponent,
    DynamicFiltersComponent
  ],
    imports: [
        MatInputModule,
        InlineSVGModule,
        MatButtonModule,
        MatGridListModule,
        CommonModule,
        WorkflowRoutingModule,
        MatCardModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatFormioModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatRippleModule,
        MatNativeDateModule,
        PerfectScrollbarModule,
        FormsModule,
        NgxDaterangepickerMd.forRoot(),
        HttpClientModule,
        FormioModule,
        NgxPermissionsModule.forChild(),
        NgxExtendedPdfViewerModule,
        NgbDropdownModule,
        DragDropModule,
        TranslateModule,
        CoreModule,
        StoreModule.forFeature('contacts', contactsReducer),
        EffectsModule.forFeature([ContactEffects]),
        StoreModule.forFeature('inbox', inboxReducer),
        PartialsModule,
        MatExpansionModule,
        TaskModule
    ],
  providers: [
    WorkflowService,
    OfficesService,
    CommentsService,
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  exports: [CreateWorkflowDialogComponent, WorkflowListComponent]

})
export class WorkflowModule {
}
