import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import {HttpUtilsService, QueryResultsModel} from '../../../../core/_base/crud';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
import { currentAuthToken, currentUser, User } from '../../../../core/auth';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';

export enum WorkflowStatuses {
  ALL = 'ALL',
  PENDING = 'PENDING',
  REVIEW = 'REVIEW',
  IN_PROGRESS = 'INPROGRESS',
  READY = 'READY'
}

const WORKFLOW_ID = 52

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }


  getWorkflowsList(workflowId, filters): Observable<any> {
    let url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_groups/${workflowId}/workflow_queues`;
    const obj = {};
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < filters.length; i++) {
      if ((filters[i].name === 'to_date' || filters[i].name === 'from_date') && filters[i].value) {
        obj[filters[i].name] = filters[i].value ? filters[i].value.toISOString() : ''
        continue;
      }
      if (filters[i].name === 'status' && filters[i].value) {
        if (filters[i].name !== WorkflowStatuses.ALL) {
          obj[filters[i].name] = filters[i].value ? filters[i].value : ''
        }
        continue;
      }
      if (filters[i].value) {
        obj[filters[i].name] = filters[i].value === '' ? '' : filters[i].value
      }
    }
    const params = new HttpParams({
      fromObject: obj
    });
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return  this.http.get<any>(url, { params });
  }

  getWorkflowsStepData(actionId, queueId, workflowId, direction?): Observable<any> {
    let params = new HttpParams();
    if (actionId > 0 && queueId > 0) {
      params = new HttpParams({ fromString: 'workflow_action_id=' + actionId + '&workflow_queue_id=' + queueId });
    }
    if (direction) {
      params = new HttpParams({ fromString: `workflow_action_id=${actionId}&workflow_queue_id=${queueId}&direction=${direction}` });
    }
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${workflowId}`;
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, { params });
  }

  getWorkflowById(workflowId): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${workflowId}`;
    return this.http.get<any>(url);
  }

  saveStepData(body, workflowId): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${workflowId}/v2`;
    return this.http.post<User>(url, body, {})
  }

  loadAllWorkflows(): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);

    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_groups/`;
    return this.http.get<any>(url, { headers: httpHeaders });
  }

  getWorkflowStatusesList(workflowId: any): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_groups/${workflowId}/statuses`;
    return this.http.get<any>(url);
  }

  getAvailableWorkflowInGroup(workflowId: any): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_groups/${workflowId}/workflows`;
    return this.http.get<any>(url);
  }

  executeApi(method, url: string, body, data, workflow_queue_id, received_id, attachments?) {
    if(url.includes('environment.')) {
      const startPoint = url.indexOf('environment.');
      const endPoint = url.indexOf('/', startPoint);
      const x = url.substring(startPoint + 12, endPoint);
      const replacement = environment[x] ? environment[x] : '';
      url = `${url.substring(0, startPoint)}${replacement}${url.substring(endPoint)}`;
    }
    url = url.replace('{workflow_queue_id}', workflow_queue_id);
    url = url.replace('{received_id}', received_id);
    let start = url.indexOf('data.');
    while (start !== -1) {
      const end = url.indexOf('/', start) !== -1 ? url.indexOf('/', start) : url.length;
      const keyArr = url.substring(start + 5, end).split('.');
      let oldValue = data;
      for (const attr of keyArr) {
        if(oldValue !== undefined && oldValue !== null) {
          oldValue = oldValue[attr];
        } else {
          oldValue = '';
          break;
        }
      }
      url = url.substring(0, start) + oldValue + url.substring(end);
      start = url.indexOf('data.');
    }
    body = body.replace('${data.attachment_names}', JSON.stringify(attachments));
    let begin = body.indexOf('${');
    while (begin !== -1) {
      let end = body.indexOf('}', begin);
      const keyArr = body.substring(begin + 7, end).split('.');
      let oldValue = data;
      for (const attr of keyArr) {
        if(oldValue !== undefined && oldValue !== null && oldValue[attr] !== undefined) {
          oldValue = oldValue[attr];
        } else {
          oldValue = '';
          break;
        }
      }
      const replacement = typeof (oldValue) === 'string' || typeof (oldValue) === 'number' ? oldValue : JSON.stringify(oldValue);
      if (typeof (oldValue) === 'number' && body.charAt(begin - 1) === '"' && body.charAt(end + 1) === '"') {
        begin--;
        end++;
      }
      body = `${body.substring(0, begin)}${replacement}${body.substring(end + 1)}`;
      begin = body.indexOf('${');
    }
    body = body.replace(new RegExp('\n', 'g'), '<br />');
    body = body.replace(new RegExp('\t', 'g'), '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    while(body.indexOf('"[') !== -1) {
      body = body.replace('"[', '[');
      body = body.replace(']"', ']');
    }
    const payload = JSON.parse(body);
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return method === 'POST' ? this.http.post<any>(url, payload, { headers }) : this.http.put(url, payload, { headers });
  }

  search(url, value, term): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const userToken = localStorage.getItem(environment.authTokenKey);
    headers = headers.set('Authorization', 'Bearer ' + userToken);
    const urlReplaced = url.replace('{company_id}', this.companyId)
    const api = urlReplaced + '?' + term + '=' + value;
    return this.http.get<any>(api, { headers });
  }
  cancelWorkflow(workflowQueueId: any, cancel_reason: string): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/cancel_status/${workflowQueueId}`;
    const body = {
      status: 'cancel',
      cancel_reason
    };
    return this.http.put<User>(url, body, {})
  }

  lockWorkflow(workflowQueueId: any, lock): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflowQueueId}/lock_unlock`;
    const body = { lock };
    return this.http.put<User>(url, body, {})
  }

  getFirstAction(workflow_id, workflow_queue_id) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${workflow_id}?workflow_queue_id=${workflow_queue_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, { headers });
  }

  updateItem(item): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/received_communication/' + item.id;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, item, { headers });
  }

  postAttachments(workflow_queue_id, attachments) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/attachments`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, { attachments }, { headers });
  }

  updateExpedite(workflowId,isPin){
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflowId}/expedite_pin`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url,{expedite_pin:isPin}, { headers });
  }

  getCancelAPIs(workflow_id) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${workflow_id}/cancel_apis`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, { headers });
  }

  getComments(workflow_queue_id: number) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/comments`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, { headers });
  }

  postComment(workflow_queue_id: number, comment: string) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/comments`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, {comment}, { headers });
  }

  updateComment(workflow_queue_id: number, comment_id: number, comment: string) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/comments/${comment_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, {comment}, { headers });
  }

  deleteComment(workflow_queue_id: number, comment_id: number) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/comments/${comment_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url, { headers });
  }

  getHistory(workflow_queue_id: number) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/history`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, { headers });
  }
  searchProjects(term){
    const url = `${environment.CORE_BASE}/companies/${this.companyId}/projects/autocomplete`;
    let params = new HttpParams();
    params = params.append('term', term);
    return this.http.get<QueryResultsModel>(url , {params});
  }
}
