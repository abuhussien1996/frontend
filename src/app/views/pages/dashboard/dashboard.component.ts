// Angular
import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AuthService} from 'src/app/core/auth';
import {AllCompaniesRequested} from 'src/app/core/management/_actions/company.actions';
import {AppState} from 'src/app/core/reducers';
import {SubheaderService} from '../../../core/_base/layout';
import {Subscription} from 'rxjs';

@Component({
  selector: 'kt-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  showDashboard = false;

  constructor(
    private store: Store<AppState>,
    private auth: AuthService,
    private cdr: ChangeDetectorRef,
    public subheaderService: SubheaderService
  ) { }

  ngOnInit(): void {
    this.subscriptions.push(this.auth.currentUser.subscribe(res => {
      if (res && res.typeRole === 'Super Admin') {
        this.store.dispatch(new AllCompaniesRequested());
      }
    }));
    this.subheaderService.setBreadcrumbs([{title:'Data Analysis',page:'/data-analysis/analysis-view'}])
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.cdr.detach();
  }
}
