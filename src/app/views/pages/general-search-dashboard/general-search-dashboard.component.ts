import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AuthService, currentUser} from 'src/app/core/auth';
import {AppState} from 'src/app/core/reducers';
import {SubheaderService} from '../../../core/_base/layout';
import {Subscription} from 'rxjs';
import {GeneralSearchService} from '../../../core/services/general-search.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AttachmentsRequested, InboxItem, InboxService, ItemUpdated} from '../../../core/events';
import {Update} from '@ngrx/entity';
import {MailViewDialogComponent} from '../apps/events/inbox/mail-view-dialog/mail-view-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'kt-dashboard',
  templateUrl: './general-search-dashboard.component.html',
  styleUrls: ['general-search-dashboard.component.scss'],
})
export class GeneralSearchDashboardComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  result
  emails
  projects
  inboxes
  tasks
  displayedEmailsColumns = ['subject', 'sender', 'sender_email', 'created_date'];
  displayedProjectsColumns = ['name'];
  displayedInboxColumns = ['created', 'type', 'sender_name', 'status', 'actions'];
  currentItem: InboxItem;
  user: any;
  displayEmailsTable = false
  displayProjectsTable = false
  displayInboxTable = false
  displayTasksTable = false
  term = ''
  constructor(
    public dialog: MatDialog,
    private store: Store<AppState>,
    private auth: AuthService,
    private cdr: ChangeDetectorRef,
    public subheaderService: SubheaderService,
    private generalSearchService: GeneralSearchService,
    private router: Router,
    private translate: TranslateService,
    private inboxService: InboxService,
  ) {
    this.store.pipe(select(currentUser)).subscribe((res) => {this.user = res});
  }

  ngOnInit(): void {
    this.subheaderService.setBreadcrumbs([{title: 'Search Results', page: '/search/result-dashboard'}])
    this.getResult()
    this.getTerm()
  }

  getResult(){
    this.generalSearchService.getCurrent().subscribe(res => {
      this.result = res
      if (!this.result) {
        this.router.navigate(['/dashboard'], {});
        return
      }
      this.reset()
      this.emails = this.result.emails
      this.projects = this.result.projects
      this.inboxes = this.result.inboxes
      this.tasks = this.result.tasks
      this.cdr.detectChanges()
    })
  }

  getTerm(){
    this.generalSearchService.term$.subscribe(ter =>{
      this.term = ter;
    })
  }
  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.cdr.detach();
  }

  reset(){
    this.displayEmailsTable = false
    this.displayProjectsTable = false
    this.displayInboxTable = false
    this.displayTasksTable = false
  }

  getItemCssClassByStatus(isActive): string {
    switch (isActive) {
      case 0:
        return 'danger';
      case 1:
        return 'success';
    }
  }

  getItemStatusString(isActive): string {
    switch (isActive) {
      case 0:
        return this.translate.instant('GLOBAL.INACTIVE');
      case 1:
        return this.translate.instant('GLOBAL.ACTIVE');
    }
  }

  viewMail(inboxItem: InboxItem) {
    this.currentItem = inboxItem;
    this.inboxService.getLatestInboxUpdate(inboxItem).subscribe( res => {
      // tslint:disable-next-line:triple-equals
      inboxItem = res.data;
      if( inboxItem.locked && this.user.userName !== inboxItem.locked_by){
        const partialItem: Update<InboxItem> = {
          id: inboxItem.id,
          changes: {locked: true, locked_by: inboxItem.locked_by}
        };
        this.store.dispatch(
          new ItemUpdated({
            item: this.currentItem,
            partialItem
          })
        );
        return;
      }
      // tslint:disable-next-line:triple-equals
      if (this.user.userName != inboxItem.locked_by)
        this.inboxService.lockInbox(inboxItem, true).subscribe();
      this.requestAttachments();
    })

    this.openMailViewDialog()
  }

  requestAttachments() {
    this.store.dispatch(new AttachmentsRequested({
        itemId: this.currentItem.id,
        contactId: this.currentItem.sender_id
      })
    );
  }

  openMailViewDialog() {
    const dialogRef = this.dialog.open(MailViewDialogComponent, {
      data: {inboxItem: this.currentItem},
      width: '60vw'
    });

    dialogRef.afterClosed().subscribe(res => {
      this.inboxService.lockInbox(this.currentItem, false).subscribe();
      if (res && res.reloadInbox) {
        // this.loadInbox();
      }
    })
  }

  getItemTextByStatusInbox(status): string {
    switch (status) {
      case 'PENDING':
        return 'Pending';
      case 'IN_PROGRESS':
        return 'In Progress ';
      case 'DONE':
        return 'Done';
    }
  }

  getItemCssClassByStatusInbox(status): string {
    switch (status) {
      case 'PENDING':
        return 'warning';
      case 'IN_PROGRESS':
        return 'success';
      case 'DONE':
        return 'primary';
    }
  }

  ToggleTable(tableName) {
    switch (tableName) {
      case  'Emails':
        this.displayEmailsTable = !this.displayEmailsTable
        break;
        case  'Projects':
        this.displayProjectsTable = !this.displayProjectsTable
        break;
      case 'Inbox':
        this.displayInboxTable = !this.displayInboxTable
        break;
      case 'Tasks':
        this.displayTasksTable = !this.displayTasksTable
        break;
    }
    this.cdr.detectChanges();
  }
}
