import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { GeneralSearchDashboardComponent } from './general-search-dashboard.component';
import {NgxPermissionsModule} from 'ngx-permissions';
import {ListsModule} from '../apps/lists/lists.module';
import {MatTableModule} from '@angular/material/table';
import {InlineSVGModule} from 'ng-inline-svg';
import {MatTooltipModule} from '@angular/material/tooltip';
import {InboxService} from '../../../core/events/_services';
import {MailViewDialogComponent} from '../apps/events/inbox/mail-view-dialog/mail-view-dialog.component';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {ReplyDialogComponent} from '../apps/events/inbox/reply-dialog/reply-dialog.component';
import {SelectWorkflowDialogComponent} from '../apps/events/inbox/select-workflow-dialog/select-workflow-dialog.component';
import {WorkflowDialogComponent} from '../apps/events/inbox/workflow-dialog/workflow-dialog.component';
import {FormioModule} from 'angular-formio';
import {GeneralSearchService} from '../../../core/services/general-search.service';
import {TaskModule} from '../task/task.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormField, MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';

const routes: Routes = [
  {
    path: 'result-dashboard',
    component: GeneralSearchDashboardComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    NgbDropdownModule,
    NgbTabsetModule,
    NgbTooltipModule,
    NgxPermissionsModule.forChild(),
    ListsModule,
    RouterModule.forChild(routes),
    MatTableModule,
    InlineSVGModule,
    MatTooltipModule,
    FormioModule,
    TaskModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,

  ],
  providers: [InboxService],
  declarations: [
    GeneralSearchDashboardComponent
  ],
  entryComponents: [MailViewDialogComponent,ReplyDialogComponent,SelectWorkflowDialogComponent,WorkflowDialogComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GeneralSearchDashboardModule {
}
