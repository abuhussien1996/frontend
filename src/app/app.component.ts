import {fromEvent, Observable, Subscription} from 'rxjs';
// Angular
import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
// Layout
import {LayoutConfigService, SplashScreenService, TranslationService} from './core/_base/layout';
// language list
import {locale as enLang} from './core/_config/i18n/en';
import {locale as chLang} from './core/_config/i18n/ch';
import {locale as esLang} from './core/_config/i18n/es';
import {locale as jpLang} from './core/_config/i18n/jp';
import {locale as deLang} from './core/_config/i18n/de';
import {locale as frLang} from './core/_config/i18n/fr';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {OfflineDialogComponent} from './views/partials/content/general/offline-modal/offline-dialog.component';
import {filter} from 'rxjs/operators';
import {currentUser, Logout} from './core/auth';
import {select, Store} from '@ngrx/store';
import {AppState} from './core/reducers';
import {environment} from "../environments/environment";

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'body[kt-root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
  // Public properties
  title = 'Metronic';
  loader: boolean;
  private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  /**
   * Component constructor
   *
   * @param translationService: TranslationService
   * @param router: Router
   * @param layoutConfigService: LayoutConfigService
   * @param splashScreenService: SplashScreenService
   */
  constructor(
    private store: Store<AppState>,
    private translationService: TranslationService,
    private router: Router,
    private route: ActivatedRoute,
    private layoutConfigService: LayoutConfigService,
    private splashScreenService: SplashScreenService,
    private dialog: MatDialog,
    private offlineDialogRef: MatDialogRef<OfflineDialogComponent>
  ) {

    // register translations
    this.translationService.loadTranslations(enLang, chLang, esLang, jpLang, deLang, frLang);
  }

  onlineEvent: Observable<Event>;
  offlineEvent: Observable<Event>;

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit(): void {

    this.onlineEvent = fromEvent(window, 'online');
    this.offlineEvent = fromEvent(window, 'offline');

    const connectionOnlineSubscription = this.onlineEvent.subscribe(e => {
      this.closeDialog();
    });

    const connectionOfflineSubscription = this.offlineEvent.subscribe(e => {
      this.openDialog();
    });
    // enable/disable loader
    this.loader = this.layoutConfigService.getConfig('page-loader.type');

    const routerSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        // hide splash screen
        this.splashScreenService.hide();

        // scroll to top on every route change
        window.scrollTo(0, 0);

        // @ts-ignore
        window.environment = environment
        // to display back the body content
        setTimeout(() => {
          document.body.classList.add('page-loaded');
        }, 500);
      }
    });
    // const routerRefrishSubscription = this.router.events
    //   .pipe(filter((rs): rs is NavigationEnd => rs instanceof NavigationEnd))
    //   .subscribe(event => {
    //     if (
    //       event.id === 1 &&
    //       event.url === event.urlAfterRedirects
    //     ) {
    //       this.router.navigateByUrl('dashboard');
    //     }
    //   })

    // this.unsubscribe.push(routerRefrishSubscription);
    this.unsubscribe.push(routerSubscription)// window.addEventListener('storage', (event) => {
    //   if (event.storageArea === localStorage) {
    //     const currentUser = localStorage.getItem('currentUser');
    //     const userInfo = localStorage.getItem('userInfo');
    //     if (!currentUser || !userInfo) {
    //       this.store.dispatch(new Logout());
    //     }
    //   }
    // }, false);

    // window.addEventListener('beforeunload', (event) => {
    //   if (event) {
    //     localStorage.clear()
    //     this.store.dispatch(new Logout());
    //   }
    // });
  }

  openDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.closeOnNavigation = false;
    dialogConfig.maxHeight = '100vh';
    dialogConfig.maxWidth = '100vw';
    dialogConfig.height = '100vh';
    dialogConfig.width = '100vw';

    this.offlineDialogRef = this.dialog.open(OfflineDialogComponent, dialogConfig);
  }

  closeDialog() {
    this.offlineDialogRef.close();
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.unsubscribe.forEach(sb => sb.unsubscribe());
  }
}
