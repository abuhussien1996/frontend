import { PhoneMaskDirective } from './views/pages/apps/lists/phone-mask.directive';
import { NgModule } from '@angular/core';

@NgModule({
    exports: [PhoneMaskDirective],
    declarations: [PhoneMaskDirective]
  })
  export class SharedModule {}
