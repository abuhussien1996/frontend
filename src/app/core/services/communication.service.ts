import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {HttpUtilsService} from "../_base/crud";
import {select, Store} from "@ngrx/store";
import {AppState} from "../reducers";
import {currentAuthToken, currentUser} from "../auth";

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  loadAllInboxes(): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/inboxes`;
    return this.http.get<any>(url, {headers: httpHeaders});
  }
  loadAllInboxesWithFax(): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/inboxes?has_fax=true`;
    return this.http.get<any>(url, {headers: httpHeaders});
  }
}
