import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../auth/_services';
import {select, Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {HttpUtilsService} from '../_base/crud';
import {currentAuthToken, currentUser} from '../auth';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../environments/environment';


export enum entityTypes {
  CONTACT = 'CONTACT',
  PATIENT = 'PATIENT',
  DOCTOR = 'DOCTOR',
  FACILITY = 'FACILITY',
  COMMUNICATION = 'COMMUNICATION',
  EMAIL = 'EMAIL',
  FAX = 'FAX',
  NOTIFICATION = 'NOTIFICATION',
  MEETING = 'MEETING',

}

@Injectable({
  providedIn: 'root'
})
export class TagService {
  private authToken: any;
  private companyId: any;
  private userInfo: any;

  constructor(private httpClient: HttpClient,
              private snackBar: MatSnackBar, private auth: AuthService,
              private store: Store<AppState>, private httpUtils: HttpUtilsService
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => {
      if (res) {
        this.companyId = res.companyID;
        this.userInfo = res;
      }
    });

  }

  loadRelatedTagItems(term: string, entityType): Observable<any> {
    const user = JSON.parse(localStorage.getItem('userInfo'))
    // tslint:disable-next-line:max-line-length
    let category = '';
    let baseURL = environment.CORE_BASE
    // const params = new HttpParams();
    // params.set('tag',term);
    let ss = '';
    switch (entityType) {
      case entityTypes.CONTACT:
        category = 'contacts';
        break;
      case entityTypes.DOCTOR:
      case entityTypes.PATIENT:
      case entityTypes.FACILITY:
        category = 'entities';
        // params.set('entity_type',entityType);
        ss = '&entity_type=' + entityType
        break;
      case entityTypes.COMMUNICATION:
      case entityTypes.EMAIL:
      case entityTypes.MEETING:
      case entityTypes.FAX:
        category = 'communication';
        // params.set('entity_type',entityType);
        ss = '&communication_type=' + entityTypes.EMAIL;
        baseURL = environment.TIMELINE_BASE;
        break;

    }
    const params = new HttpParams({fromString: 'tag=' + term + ss});

    const url = `${baseURL}/companies/${this.companyId}/${category}`;
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.get<any>(url, {headers: httpHeaders, params});
  }

  searchTags(
    category,
    autocompleteEntity: 'DOCTOR' | 'PATIENT' | 'FACILITY' | 'CONTACT' | 'COMMUNICATION' | 'DOCUMENT',
    term: string
    ): Observable<any> {
    let baseUrl = '';
    switch (category) {
      case 'Entity':
        baseUrl = environment.CORE_BASE;
        break;
      case 'Communication':
        baseUrl = environment.TIMELINE_BASE;
        break;

    }
    // tslint:disable-next-line:max-line-length
    const url = `${baseUrl}/companies/${this.companyId}/tags/autocomplete?term=${term}&type=${autocompleteEntity}`;
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.get<any>(url, {headers: httpHeaders});
  }
}
