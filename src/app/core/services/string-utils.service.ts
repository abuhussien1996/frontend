import {Injectable} from '@angular/core';
import * as moment from 'moment'
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class StringUtilsService {

  constructor(private datePipe: DatePipe) {
  }

  displayFullName(patient: any): string {
    let res = ''
    if (patient && patient.name) {
      res += patient.name
    }
    if (patient && patient.first_name) {
      res += patient.first_name + ' '
    }
    if (patient && patient.last_name) {
      res += patient.last_name + ' '
    }
    if (patient && patient.last_name2) {
      res += patient.last_name2 + ' '
    }
    if (patient && patient.dob) {
      res += '  ' + `<small class="text-muted">` + moment(patient.dob).format('l') + `</small>`
    }
    return res;
  }

  displayDevice(device: any) {
    if (device && typeof device === 'object') {
      return device.name;
    }
  }

  displayName(obj: any) {
    let res = ''
    if (obj && obj.name) {
      res += obj.name
    }
    if (obj && obj.first_name) {
      res += obj.first_name + ' '
    }
    if (obj && obj.last_name) {
      res += obj.last_name + ' '
    }
    if (obj && obj.last_name2) {
      res += obj.last_name2
    }
    return res;
  }

  displayProgram(event: any): string {
    return event && event.program ? event.program.name : '';
  }

  formatDate(date, format = 'MM/dd/yyyy'){
    console.log(date)
    console.log(new Date(date).toUTCString())
    try {
      return this.datePipe.transform(new Date(date).toUTCString(),format);
    }catch (e){
      return 'Invalid Date';
    }
  }
  formatDateDiff(from: any = new Date(), to: any = new Date()){
    to = moment(to);
    from = moment(from);
    const tempTime = moment.duration(to.diff(from));
    return tempTime.humanize(false)
  }
}
