import {Injectable} from '@angular/core';
import {FormControl} from '@angular/forms';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {HttpUtilsService} from '../_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {currentAuthToken, currentUser} from '../auth';

export const VALIDATION_MESSAGES = {
  required: 'this field required',
  min: 'this value should be more than {min}',
  max: 'this value should not exceed {max}',
}

@Injectable({
  providedIn: 'root'
})
export class GeneralValidationMessagesService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  getMessage(control: FormControl) {
    let msg = '';
    if (control && control.errors) {
      for (const [key, value] of Object.entries(control.errors)) {
        if (control.errors[key] && VALIDATION_MESSAGES[key]) {
          msg = this.handleMessageParams(VALIDATION_MESSAGES[key], value);
        }
      }
    }
    return msg;
  }

  private handleMessageParams(validationMessage: string, params: any) {
    for (const [key, value] of Object.entries(params)) {
      const valToReplace = '{'.concat(key).concat('}');
      if (validationMessage.indexOf(valToReplace) > -1) {
        const regex = new RegExp(valToReplace, 'gi');
        validationMessage = validationMessage.replace(regex, value.toString());
      }
    }
    return validationMessage;
  }

  getEntitiesById(type, id) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = environment.CORE_BASE + `/companies/${this.companyId}/${type}/` + id;
    return this.http.get<any>(url, {headers: httpHeaders});
  }
}
