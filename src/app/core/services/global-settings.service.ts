import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root',

})
export class GlobalSettingsService {
  private loadTaskList = new BehaviorSubject<boolean>(false);
  $loadTaskList = this.loadTaskList.asObservable();

  constructor(public dialog: MatDialog) {
  }

  loadTasks() {
    this.loadTaskList.next(true);
  }
}
