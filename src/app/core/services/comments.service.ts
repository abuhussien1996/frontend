// Angular
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// CRUD
import {HttpUtilsService} from '../_base/crud';
// Ngrx
import {select, Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {currentAuthToken, currentUser} from '../auth/_selectors/auth.selectors';
import {environment} from '../../../environments/environment';


@Injectable()
export class CommentsService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  getComments(type: string, id: number) {
    const url = `${type === 'tasks_v2' ? environment.TIMELINE_BASE : environment.EVENT_BASE}/companies/${this.companyId}/${type}/${id}/comments`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, { headers });
  }

  postComment(type: string, id: number, comment: string) {
    const url = `${type === 'tasks_v2' ? environment.TIMELINE_BASE : environment.EVENT_BASE}/companies/${this.companyId}/${type}/${id}/comments`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, {comment}, { headers });
  }

  updateComment(type: string, id: number, comment_id: number, comment: string) {
    const url = `${type === 'tasks_v2' ? environment.TIMELINE_BASE : environment.EVENT_BASE}/companies/${this.companyId}/${type}/${id}/comments/${comment_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, {comment}, { headers });
  }

  deleteComment(type: string, id: number, comment_id: number) {
    const url = `${type === 'tasks_v2' ? environment.TIMELINE_BASE : environment.EVENT_BASE}/companies/${this.companyId}/${type}/${id}/comments/${comment_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url, { headers });
  }
}
