import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../auth/_services';
import {select, Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {HttpUtilsService, QueryResultsModel} from '../_base/crud';
import {currentAuthToken, currentUser, User} from '../auth';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import * as _ from 'lodash';
import * as moment from 'moment';


export enum entityTypes {
  CONTACT = 'CONTACT',
  PATIENT = 'PATIENT',
  DOCTOR = 'DOCTOR',
  FACILITY = 'FACILITY',
  COMMUNICATION = 'COMMUNICATION',
  EMAIL = 'EMAIL',
  FAX = 'FAX',
  NOTIFICATION = 'NOTIFICATION',
  MEETING = 'MEETING',

}

@Injectable({
  providedIn: 'root'
})
export class GeneralSearchService{
  private authToken: any;
  private companyId: any;
  private userInfo: any;
  private generalSearchResult$ = new BehaviorSubject(null);
  public term$ = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient,
              private snackBar: MatSnackBar, private auth: AuthService,
              private store: Store<AppState>, private httpUtils: HttpUtilsService
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => {
      if (res) {
        this.companyId = res.companyID;
        this.userInfo = res;
      }
    });

  }

  generalSearch(term: string,filter?,fromDate?, toDate?) {
    let filterVar = ''
    if(filter){
      filter.forEach(element => filterVar = filterVar + '&type=' + element)
    }
    if(fromDate && moment.isDate(fromDate)){
      filterVar+='&from='+moment(fromDate).format('YYYY-MM-DD')
    }
    if(fromDate && moment.isDate(toDate)){
      filterVar+='&to='+moment(toDate).format('YYYY-MM-DD')
    }
    const url = environment.CORE_BASE + `/companies/${this.companyId}/search?term=${term}` + filterVar;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.get<any>(url, {headers: httpHeaders});
  }
  searchTasks(term: string) {
    const url = environment.TIMELINE_BASE
      + '/companies/' + this.companyId + '/tasks_v2?&search_term=' + term
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.get<any>(url, {headers: httpHeaders});
  }
  countTasks(term: string) {
    const url = environment.TIMELINE_BASE
      + '/companies/' + this.companyId + '/tasks_v2/count?&search_term=' + term
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.get<any>(url, {headers: httpHeaders});
  }
  searchProjects(term){
    const url = `${environment.CORE_BASE}/companies/${this.companyId}/projects/autocomplete`;
    let params = new HttpParams();
    params = params.append('term', term);
    return this.httpClient.get<any>(url , {params});
  }
  searchCommunications(term){
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/communication`;
    let params = new HttpParams();
    params = params.append('search_term', term);
    return this.httpClient.get<any>(url , {params});
  }
  searchInboxes(term){
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/received_communication`;
    let params = new HttpParams();
    params = params.append('search_term', term);
    return this.httpClient.get<any>(url , {params});
  }
  public setCurrent(res) {
    return this.generalSearchResult$.next(res);
  }
  public getCurrent(): Observable<any> {
    return this.generalSearchResult$.asObservable();
  }
}
