export class QueryResultsModel {
  // fields
  items: any[];
  totalCount: number;
  error: any;
  pager: any;
  pageOfItems : any[];

  constructor(items: any[] = [], totalCount: number = 0) {
    this.error = null;
    this.items = items;
    this.totalCount = totalCount;
    this.pager = totalCount;
    this.pageOfItems = items
  }
}
