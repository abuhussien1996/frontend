// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {AuthService} from '../../../auth/_services';
import {Router} from '@angular/router';

/**
 * More information there => https://medium.com/@MetonymyQT/angular-http-interceptors-what-are-they-and-how-to-use-them-52e060321088
 */
@Injectable()
export class InterceptService implements HttpInterceptor {
  constructor(private authenticationService: AuthService,private router: Router) {
  }
  // intercept request and add token
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(catchError(err => {
      if ([401].includes(err.status)) {
        // auto logout if 401 or 403 response returned from api
        this.authenticationService.logout();
      }
      if ([403].includes(err.status)) {
        this.router.navigateByUrl('/error/403');
      }
      if ([404].includes(err.status)) {
        this.router.navigateByUrl('/error/404');
      }
      const error = err;
      return throwError(error);
    }))
  }
}
