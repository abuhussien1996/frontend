import { BaseModel } from '../../_base/crud';

export class CompanyModel  extends BaseModel {
  id: number;
  name: string;
  admin: string;
  adminID: number
  limitNumberOffices: number;
  limitNumberUsers: number;
  numberOffices: number;
  numberUsers: number;
  isActive: boolean;
  createdAt: Date;
  updatedAt: Date;
  measurementSystem: string;
  project_schema: { name: any; id: any };
  entity_schema: { name: any; id: any };

  clear() {
    this.id = 0;
    this.name = '';
    this.adminID = 0;
    this.limitNumberOffices = 0;
    this.limitNumberUsers = 0;
    this.numberOffices = 0;
    this.numberUsers = 0;
    this.createdAt = new Date();
    this.updatedAt = new Date();
    this.isActive = false;
    this.measurementSystem = 'm';
  }
}
