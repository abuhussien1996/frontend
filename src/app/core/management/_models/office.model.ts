import { BaseModel } from '../../_base/crud';

export class OfficeModel  extends BaseModel {
  id: number;
  name: string;
  company: string;
  companyID: number;
  address: string;
  isActive: boolean;
  prefix: string;
  suffix: string;
  phone: string;
  fax: string;

  clear() {
    this.id = 0;
    this.name = '';
    this.company = '';
    this.companyID = 0;
    this.address = '';
    this.isActive = false;
    this.phone = '';
    this.fax = '';
    this.prefix = '';
    this.suffix = '';
  }
}
