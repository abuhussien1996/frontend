// Angular
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
// CRUD
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
// Models
import {OfficeModel} from '../_models/office.model';

// Ngrx
import {select, Store} from '@ngrx/store';
import {currentAuthToken} from 'src/app/core/auth/_selectors/auth.selectors';
import {AppState} from '../../reducers';
import {environment} from '../../../../environments/environment';

const API_URL = environment.USER_BASE;

@Injectable()
export class OfficesService {
  private authToken;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  // CREATE =>  POST: add a new office to the server
  createOffice(office: OfficeModel): Observable<OfficeModel> {
    // Note: Add headers if needed (tokens/bearer)
    const url = API_URL + '/c-office';
    const body = {
      office,
      name: office.name
    };
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<OfficeModel>(url, body, {headers: httpHeaders});
  }

  // READ
  getAllOffices(): Observable<OfficeModel[]> {
    return this.http.get<OfficeModel[]>(API_URL);
  }

  getOfficeById(officeId: number): Observable<OfficeModel> {
    const body = {
      id: officeId,
    }
    const url = API_URL + '/offices';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<OfficeModel>(url, body, {headers: httpHeaders});
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  // Server should return filtered/sorted result
  findOffices(queryParams: QueryParamsModel, officeId: number): Observable<QueryResultsModel> {
    // Note: Add headers if needed (tokens/bearer)
    const body = {
      searchData: queryParams.filter.text,
      id: officeId,
      isActive: queryParams.filter.status,
      pageIndex: queryParams.pageNumber,
      pageSize: queryParams.pageSize
    }
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const httpParams = this.httpUtils.getFindHTTPParams(queryParams);

    const url = API_URL + `/offices`;
    return this.http.post<QueryResultsModel>(url, body, {headers: httpHeaders});
  }

  // UPDATE => PUT: update the office on the server
  updateOffice(office: OfficeModel): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = API_URL + `/office`;
    const body = {
      office,
      name: office.name
    };
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // UPDATE Status
  updateStatusForOffice(id: number, isActive: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id,
      isActive
    };
    const url = API_URL + '/officestatusupdatesingle';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // UPDATE Status
  updateStatusForOffices(ids: number[], isActive: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids,
      isActive
    };
    const url = API_URL + '/officestatusupdate';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // DELETE => delete the office from the server
  deleteOffice(officeId: number, companyID: number): Observable<any> {
    const url = API_URL + '/single-soft-delete-office';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id: officeId,
      companyID
    };
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  deleteOffices(officeIds: any[] = []): Observable<any> {
    const url = API_URL + '/multiple-soft-delete-office';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids: officeIds
    };
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  searchOffices(term: string): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = API_URL + '/all-office?term=' + term;
    return this.http.get<any>(url, {headers});
  }
}
