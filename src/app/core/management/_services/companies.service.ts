// Angular
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
// CRUD
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
// Models
import {CompanyModel} from '../_models/company.model';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {Login} from '../../auth';
import {AllCompaniesRequested} from '..';

const API_URL = environment.USER_BASE;

@Injectable()
export class CompaniesService {
  private authToken;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  // CREATE =>  POST: add a new company to the server
  createCompany(company: CompanyModel): Observable<CompanyModel> {
    // Note: Add headers if needed (tokens/bearer)
    const url = API_URL + '/c-company';
    const body = {
      company,
      name: company.name,
      project_schema: company.project_schema,
      entity_schema: company.entity_schema,
    };
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<CompanyModel>(url, body, {headers: httpHeaders});
  }


  // READ
  getAllCompanies(): Observable<CompanyModel[]> {
    const url = API_URL + '/all-company';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<CompanyModel[]>(url, {headers: httpHeaders});
  }

  // READ
  getCompaniesNoAdminRole(): Observable<CompanyModel[]> {
    const url = API_URL + '/company-no-admin-role';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<CompanyModel[]>(url, {headers: httpHeaders});
  }

  getCompanyById(companyId: number): Observable<CompanyModel> {
    const body = {
      id: companyId,
    }
    const url = API_URL + '/companies';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<CompanyModel>(url, body, {headers: httpHeaders});
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  // Server should return filtered/sorted result
  findCompanies(queryParams: QueryParamsModel, companyId: number): Observable<QueryResultsModel> {
    const body = {
      searchData: queryParams.filter.text === undefined ? '' : queryParams.filter.text,
      id: companyId,
      isActive: queryParams.filter.status === undefined ? '' : queryParams.filter.status,
      pageIndex: queryParams.pageNumber,
      pageSize: queryParams.pageSize
    }
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);

    const url = API_URL + `/companies`;

    return this.http.post<QueryResultsModel>(url, body, {headers: httpHeaders});
  }

  // UPDATE => PUT: update the company on the server
  updateCompany(company: CompanyModel): Observable<any> {
    const httpHeader = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = API_URL + `/company`;
    const body = {
      company,
      name: company.name
    };
    return this.http.put(url, body, {headers: httpHeader});
  }

  // UPDATE Status
  updateStatusForCompany(id: number, isActive: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id,
      isActive
    };
    const url = API_URL + '/companystatusupdatesingle';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // UPDATE Status
  updateStatusForCompanies(ids: number[], isActive: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids,
      isActive
    };
    const url = API_URL + '/companystatusupdate';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // DELETE => delete the company from the server
  deleteCompany(companyId: number): Observable<any> {
    const url = API_URL + '/single-soft-delete-company';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id: companyId
    };
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  deleteCompanies(companiesIds: number[] = []): Observable<any> {
    const url = API_URL + '/multiple-soft-delete-company';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids: companiesIds
    };
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  searchCompanies(search_term: string){
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = API_URL + `/companies`;
    const body = {
      searchData: search_term,
      id: 0,
      isActive: true,
      pageIndex:0,
      pageSize: 10
    }
    return this.http.post<any>(url, body,{headers: httpHeaders}).pipe(map(res => {
      if (res.code === 200) {
        return res.pageOfItems;
      }
      return [];
    }));
  }
  // READ
  getCompanySetting(companyId): Observable<CompanyModel[]> {
    const url = API_URL + `/companies/${companyId}/company_settings`;
    // const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<CompanyModel[]>(url);
  }
}
