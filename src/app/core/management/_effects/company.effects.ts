import {forkJoin, of} from 'rxjs';
// Angular
import {Injectable} from '@angular/core';
// RxJS
import {catchError, map, mergeMap} from 'rxjs/operators';
// NGRX
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
// CRUD
import {QueryParamsModel} from '../../_base/crud';
// Services
import {CompaniesService} from '../_services/companies.service';
// State
import {AppState} from '../../reducers';
// Actions
import {
  AllCompaniesRequested,
  FireCompanySuccess,
  CompaniesPageCancelled,
  CompaniesPageLoaded,
  CompaniesPageRequested,
  CompaniesPageToggleLoading,
  CompaniesStatusOnServerUpdated,
  CompaniesStatusUpdated,
  CompanyActionToggleLoading,
  CompanyActionTypes,
  CompanyCreated,
  CompanyOnServerCreated,
  CompanyOnServerUpdated,
  CompanyStatusOnServerUpdated,
  CompanyStatusUpdated,
  CompanyUpdated,
  ManyCompaniesDeleted,
  ManyCompaniesOnServerDeleted,
  OneCompanyDeleted,
  OneCompanyOnServerDeleted,
  ThrowCompanyError
} from '../_actions/company.actions';
import {Router} from '@angular/router';
import {AuthService} from '../../auth/_services/auth.service';

@Injectable()
export class CompanyEffects {
  showPageLoadingDistpatcher = new CompaniesPageToggleLoading({isLoading: true});
  showActionLoadingDistpatcher = new CompanyActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new CompanyActionToggleLoading({isLoading: false});

  @Effect()
  loadAllCompanies$ = this.actions$
    .pipe(
      ofType<AllCompaniesRequested>(CompanyActionTypes.AllCompaniesRequested),
      mergeMap(() => {


        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.companiesService.findCompanies(new QueryParamsModel('', '', '', 0, 10), 0);
        const lastQuery = of(new QueryParamsModel('', 'asc', '', 0, 10));
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 401) {
                return new CompaniesPageCancelled();
              } else if (response[0].status === 403) {
                this.store.dispatch(new ThrowCompanyError({error: response[0].error}));

                return new CompaniesPageCancelled();
              } else {
                this.store.dispatch(new ThrowCompanyError({error: response[0].error}));
                return new CompaniesPageCancelled();
              }
            } else {
              const result = response[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery: QueryParamsModel = response[1];
              return new CompaniesPageLoaded({
                companies: result.pageOfItems,
                totalCount: result.pager.totalCount,
                page: lastQuery
              });
            }
          })
        );
      })
    );

  @Effect()
  loadCompaniesPage$ = this.actions$.pipe(
    ofType<CompaniesPageRequested>(CompanyActionTypes.CompaniesPageRequested),
    mergeMap(({payload}) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.companiesService.findCompanies(payload.page, payload.id);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery).pipe(
        catchError(error => {
          return of([error]);
        }),
        map(response => {
          if (response[0].error) {
            if (response[0].status === 401) {
              this.authService.logout();
              return new CompaniesPageCancelled();
            } else if (response[0].status === 403) {
              this.router.navigateByUrl('/error/403');
              return new CompaniesPageCancelled();
            } else {
              this.store.dispatch(new ThrowCompanyError({error: response[0].error}));
              return new CompaniesPageCancelled();
            }
          } else {
            const result = response[0];
            // tslint:disable-next-line:no-shadowed-variable
            const lastQuery: QueryParamsModel = response[1];
            return new CompaniesPageLoaded({
              companies: result.pageOfItems,
              totalCount: result.pager.totalCount,
              page: lastQuery
            });
          }
        })
      );
    })
  );

  @Effect()
  deleteCompany$ = this.actions$
    .pipe(
      ofType<OneCompanyOnServerDeleted>(CompanyActionTypes.OneCompanyOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.companiesService.deleteCompany(payload.id).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if (res.error) {
                return new ThrowCompanyError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new FireCompanySuccess({action_type: CompanyActionTypes.OneCompanyDeleted}));
                return new OneCompanyDeleted(payload);
              }
            })
          );
        }
      ),
    );

  @Effect()
  deleteCompanies$ = this.actions$
    .pipe(
      ofType<ManyCompaniesOnServerDeleted>(CompanyActionTypes.ManyCompaniesOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.companiesService.deleteCompanies(payload.ids).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if (res.error) {
                return new ThrowCompanyError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new FireCompanySuccess({action_type: CompanyActionTypes.ManyCompaniesDeleted}));
                return new ManyCompaniesDeleted(payload);
              }
            })
          );
        }
      ),
    );

  @Effect()
  updateCompany$ = this.actions$
    .pipe(
      ofType<CompanyOnServerUpdated>(CompanyActionTypes.CompanyOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.companiesService.updateCompany(payload.company).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowCompanyError({error: res.error});
            } else {
              console.log(payload.company)
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireCompanySuccess({action_type: CompanyActionTypes.CompanyUpdated}));
              return new CompanyUpdated(payload);
            }
          })
        );
      }),
    );

  @Effect()
  updateCompanyStatus$ = this.actions$
    .pipe(
      ofType<CompanyStatusOnServerUpdated>(CompanyActionTypes.CompanyStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.companiesService.updateStatusForCompany(payload.id, payload.isActive).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowCompanyError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireCompanySuccess({action_type: CompanyActionTypes.CompanyStatusUpdated}));
              return new CompanyStatusUpdated(payload);
            }
          })
        );
      }),
    );

  @Effect()
  updateCompaniesStatus$ = this.actions$
    .pipe(
      ofType<CompaniesStatusOnServerUpdated>(CompanyActionTypes.CompaniesStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.companiesService.updateStatusForCompanies(payload.ids, payload.isActive).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowCompanyError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireCompanySuccess({action_type: CompanyActionTypes.CompaniesStatusUpdated}));
              return new CompaniesStatusUpdated(payload);
            }
          })
        );
      }),
    );

  @Effect()
  createCompany$ = this.actions$
    .pipe(
      ofType<CompanyOnServerCreated>(CompanyActionTypes.CompanyOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.companiesService.createCompany(payload.company).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if (!res.id) {
              return new ThrowCompanyError({error: res});
            } else {
              console.log(res)
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireCompanySuccess({action_type: CompanyActionTypes.CompanyCreated}));
              return new CompanyCreated({company: res});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private companiesService: CompaniesService,
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
  ) {

  }
}
