import {forkJoin, of} from 'rxjs';
// Angular
import {Injectable} from '@angular/core';
// RxJS
import {map, mergeMap, catchError} from 'rxjs/operators';
// NGRX
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
// CRUD
import {QueryParamsModel} from '../../_base/crud';
// Services
import {OfficesService} from '../_services/offices.service';
// State
import {AppState} from '../../reducers';
// Actions
import {
  OfficeActionToggleLoading,
  OfficeActionTypes,
  OfficeCreated,
  OfficeOnServerCreated,
  OfficesPageLoaded,
  OfficesPageRequested,
  OfficesPageToggleLoading,
  OfficeStatusUpdated,
  OfficesStatusUpdated,
  OfficeUpdated,
  ManyOfficesDeleted,
  OneOfficeDeleted,
  OfficesPageCancelled,
  ThrowOfficeError,
  FireOfficeSuccess,
  OneOfficeOnServerDeleted,
  ManyOfficesOnServerDeleted,
  OfficeOnServerUpdated,
  OfficeStatusOnServerUpdated,
  OfficesStatusOnServerUpdated
} from '../_actions/office.actions';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/_services/auth.service';

@Injectable()
export class OfficeEffects {
  showPageLoadingDistpatcher = new OfficesPageToggleLoading({isLoading: true});
  showActionLoadingDistpatcher = new OfficeActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new OfficeActionToggleLoading({isLoading: false});

  @Effect()
  loadOfficesPage$ = this.actions$.pipe(
    ofType<OfficesPageRequested>(OfficeActionTypes.OfficesPageRequested),
    mergeMap(({payload}) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.officesService.findOffices(payload.page, payload.id);
      const lastQuery = of(payload.page);
      return forkJoin([requestToServer, lastQuery]).pipe(
        catchError(error => {
          return of([error]);
        }),
        map(response => {
          if(response[0].error) {
            if(response[0].status === 401) {
              this.authService.logout();
              return new OfficesPageCancelled();
            }
            else if(response[0].status === 403) {
              this.router.navigateByUrl('/error/403');
              return new OfficesPageCancelled();
            } else {
              this.store.dispatch(new ThrowOfficeError({error: response[0].error}));
              return new OfficesPageCancelled();
            }
          } else {
            const result = response[0];
            // tslint:disable-next-line:no-shadowed-variable
            const lastQuery: QueryParamsModel = response[1];
            return new OfficesPageLoaded({
              offices: result.pageOfItems,
              totalCount: result.pager.totalCount,
              page: lastQuery
            });
          }
        })
      );
    })
  );

  @Effect()
  deleteOffice$ = this.actions$
    .pipe(
      ofType<OneOfficeOnServerDeleted>(OfficeActionTypes.OneOfficeOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.officesService.deleteOffice(payload.id, payload.companyID).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if(res.error) {
                return new ThrowOfficeError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new FireOfficeSuccess({action_type: OfficeActionTypes.OneOfficeDeleted}));
                return new OneOfficeDeleted(payload);
              }
            })
          );
        })
      );

  @Effect()
  deleteOffices$ = this.actions$
    .pipe(
      ofType<ManyOfficesOnServerDeleted>(OfficeActionTypes.ManyOfficesOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.officesService.deleteOffices(payload.offices).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if(res.error) {
                return new ThrowOfficeError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new FireOfficeSuccess({action_type: OfficeActionTypes.ManyOfficesDeleted}));
                return new ManyOfficesDeleted(payload);
              }
            })
          );
        })
      );

  @Effect()
  updateOffice$ = this.actions$
    .pipe(
      ofType<OfficeOnServerUpdated>(OfficeActionTypes.OfficeOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.officesService.updateOffice(payload.office).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowOfficeError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireOfficeSuccess({action_type: OfficeActionTypes.OfficeUpdated}));
              return new OfficeUpdated(payload);
            }
          })
        );
      })
    );

  @Effect()
  updateOfficeStatus$ = this.actions$
    .pipe(
      ofType<OfficeStatusOnServerUpdated>(OfficeActionTypes.OfficeStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.officesService.updateStatusForOffice(payload.id, payload.isActive).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowOfficeError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireOfficeSuccess({action_type: OfficeActionTypes.OfficeStatusUpdated}));
              return new OfficeStatusUpdated(payload);
            }
          })
        );
      })
    );

  @Effect()
  updateOfficesStatus$ = this.actions$
    .pipe(
      ofType<OfficesStatusOnServerUpdated>(OfficeActionTypes.OfficesStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.officesService.updateStatusForOffices(payload.ids, payload.isActive).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowOfficeError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireOfficeSuccess({action_type: OfficeActionTypes.OfficesStatusUpdated}));
              return new OfficesStatusUpdated(payload);
            }
          })
        );
      })
    );

  @Effect()
  createOffice$ = this.actions$
    .pipe(
      ofType<OfficeOnServerCreated>(OfficeActionTypes.OfficeOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.officesService.createOffice(payload.office).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(!res.id) {
              return new ThrowOfficeError({error: res});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireOfficeSuccess({action_type: OfficeActionTypes.OfficeCreated}));
              return new OfficeCreated({office: res});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private officesService: OfficesService,
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
    ) {
  }
}
