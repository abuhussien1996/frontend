// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { CompanyActions, CompanyActionTypes } from '../_actions/company.actions';
// Models
import { CompanyModel } from '../_models/company.model';
import { QueryParamsModel } from '../../_base/crud';

export interface CompaniesState extends EntityState<CompanyModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedCompanyId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
  error: any|null
  _isAllCompaniesLoaded: boolean;
  latestSuccessfullAction: any;
}

export const adapter: EntityAdapter<CompanyModel> = createEntityAdapter<CompanyModel>();

export const initialCompaniesState: CompaniesState = adapter.getInitialState({
  companyForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedCompanyId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
  error: null,
  _isAllCompaniesLoaded: false,
  latestSuccessfullAction: null
});

export function companiesReducer(state = initialCompaniesState, action: CompanyActions): CompaniesState {
  switch (action.type) {
    case CompanyActionTypes.AllCompaniesRequested:
      return {
        ...state,
        _isAllCompaniesLoaded: false
      };
    case CompanyActionTypes.AllCompaniesLoaded:
      return adapter.addAll(action.payload.companies, {
        ...state,
        _isAllCompaniesLoaded: true
      });

    case CompanyActionTypes.CompaniesPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedCompanyId: undefined
      };
    }
    case CompanyActionTypes.CompanyActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case CompanyActionTypes.CompanyOnServerCreated:
      return {
        ...state
      };
    case CompanyActionTypes.CompanyCreated:
      return adapter.addOne(action.payload.company, {
        ...state, lastCreatedCompanyId: action.payload.company.id
      });
    case CompanyActionTypes.CompanyUpdated:
      return adapter.updateOne(action.payload.partialCompany, state);
    case CompanyActionTypes.CompanyStatusUpdated: {
      // tslint:disable-next-line
      const _partialCompany: Update<CompanyModel> = {
        id: action.payload.company.id,
        changes: {
          isActive: action.payload.isActive
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialCompany, state);
    }
    case CompanyActionTypes.CompaniesStatusUpdated: {
      // tslint:disable-next-line
      const _partialCompanies: Update<CompanyModel>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.companies.length; i++) {
        _partialCompanies.push({
          id: action.payload.companies[i].id,
          changes: {
            isActive: action.payload.isActive
          }
        });
      }
      return adapter.updateMany(_partialCompanies, state);
    }
    case CompanyActionTypes.OneCompanyDeleted:
      return adapter.removeOne(action.payload.id, state);
    case CompanyActionTypes.ManyCompaniesDeleted:
      return adapter.removeMany(action.payload.ids, state);
    case CompanyActionTypes.CompaniesPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case CompanyActionTypes.CompaniesPageLoaded: {
      return adapter.addMany(action.payload.companies, {
        ...initialCompaniesState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
        latestSuccessfullAction: state.latestSuccessfullAction,
        error: null
      });
    }
    case CompanyActionTypes.ThrowCompanyError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case CompanyActionTypes.FireCompanySuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getCompanyState = createFeatureSelector<CompanyModel>('companies');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
