// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { OfficeActions, OfficeActionTypes } from '../_actions/office.actions';
// Models
import { OfficeModel } from '../_models/office.model';
import { QueryParamsModel } from '../../_base/crud';

export interface OfficesState extends EntityState<OfficeModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedOfficeId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
  error: any|null;
  latestSuccessfullAction: any;
}

export const adapter: EntityAdapter<OfficeModel> = createEntityAdapter<OfficeModel>();

export const initialOfficesState: OfficesState = adapter.getInitialState({
  officeForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedOfficeId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
  error: null,
  latestSuccessfullAction: null
});

export function officesReducer(state = initialOfficesState, action: OfficeActions): OfficesState {
  switch (action.type) {
    case OfficeActionTypes.OfficesPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedOfficeId: undefined
      };
    }
    case OfficeActionTypes.OfficeActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case OfficeActionTypes.OfficeOnServerCreated:
      return {
        ...state
      };
    case OfficeActionTypes.OfficeOnServerUpdated:
      return {
        ...state
      };
    case OfficeActionTypes.OfficeStatusOnServerUpdated:
      return {
        ...state
      };
    case OfficeActionTypes.OfficesStatusOnServerUpdated:
      return {
        ...state
      };
    case OfficeActionTypes.OneOfficeOnServerDeleted:
      return {
        ...state
      };
    case OfficeActionTypes.ManyOfficesOnServerDeleted:
      return {
        ...state
      };
    case OfficeActionTypes.OfficeCreated:
      return adapter.addOne(action.payload.office, {
        ...state, lastCreatedOfficeId: action.payload.office.id
      });
    case OfficeActionTypes.OfficeUpdated:
      return adapter.updateOne(action.payload.partialOffice, state);
    case OfficeActionTypes.OfficeStatusUpdated: {
      // tslint:disable-next-line
      const _partialOffice: Update<OfficeModel> = {
        id: action.payload.office.id,
        changes: {
          isActive: action.payload.isActive
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialOffice, state);
    }
    case OfficeActionTypes.OfficesStatusUpdated: {
      // tslint:disable-next-line
      const _partialOffices: Update<OfficeModel>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.offices.length; i++) {
        _partialOffices.push({
          id: action.payload.offices[i].id,
          changes: {
            isActive: action.payload.isActive
          }
        });
      }
      return adapter.updateMany(_partialOffices, state);
    }
    case OfficeActionTypes.OneOfficeDeleted:
      return adapter.removeOne(action.payload.id, state);
    case OfficeActionTypes.ManyOfficesDeleted:
      return adapter.removeMany(action.payload.ids, state);
    case OfficeActionTypes.OfficesPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({}), showInitWaitingMessage: false
      };
    }
    case OfficeActionTypes.OfficesPageLoaded: {
      return adapter.addMany(action.payload.offices, {
        ...initialOfficesState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
        latestSuccessfullAction: state.latestSuccessfullAction,
        error: null
      });
    }
    case OfficeActionTypes.ThrowOfficeError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case OfficeActionTypes.FireOfficeSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getOfficeState = createFeatureSelector<OfficeModel>('offices');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
