// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { OfficeModel } from '../_models/office.model';

export enum OfficeActionTypes {
  OfficeOnServerCreated = '[Edit Office Dialog] Office On Server Created',
  OfficeCreated = '[Edit Office Dialog] Office Created',
  OfficeOnServerUpdated = '[Edit Office Dialog] Office On Server Updated',
  OfficeUpdated = '[Edit Office Dialog] Office Updated',
  OfficesStatusOnServerUpdated = '[Edit Office Dialog] Offices Status On Server Updated',
  OfficesStatusUpdated = '[Office List Page] Offices Status Updated',
  OfficeStatusOnServerUpdated = '[Office List Page] Office Status On Server Updated',
  OfficeStatusUpdated = '[Office List Page] Office Status Updated',
  OneOfficeOnServerDeleted = '[Offices List Page] One Office On Server Deleted',
  OneOfficeDeleted = '[Offices List Page] One Office Deleted',
  ManyOfficesOnServerDeleted = '[Offices List Page] Many Offices On Server Deleted',
  ManyOfficesDeleted = '[Offices List Page] Many Office Deleted',
  OfficesPageRequested = '[Offices List Page] Offices Page Requested',
  OfficesPageLoaded = '[Offices API] Offices Page Loaded',
  OfficesPageCancelled = '[Offices API] Offices Page Cancelled',
  OfficesPageToggleLoading = '[Offices] Offices Page Toggle Loading',
  OfficeActionToggleLoading = '[Offices] Offices Action Toggle Loading',
  ThrowOfficeError = '[Offices] Throw Office Error',
  FireOfficeSuccess = '[Offices] Fire Office Success'
}

export class OfficeOnServerCreated implements Action {
  readonly type = OfficeActionTypes.OfficeOnServerCreated;
  constructor(public payload: { office: OfficeModel }) {
  }
}

export class OfficeCreated implements Action {
  readonly type = OfficeActionTypes.OfficeCreated;

  constructor(public payload: { office: OfficeModel }) {
  }
}


export class OfficeOnServerUpdated implements Action {
  readonly type = OfficeActionTypes.OfficeOnServerUpdated;

  constructor(public payload: {
    partialOffice: Update<OfficeModel>, // For State update
    office: OfficeModel, // For Server update (through service)
  }) {
  }
}
export class OfficeUpdated implements Action {
  readonly type = OfficeActionTypes.OfficeUpdated;

  constructor(public payload: {
    partialOffice: Update<OfficeModel>, // For State update
    office: OfficeModel, // For Server update (through service)
  }) {
  }
}

export class OfficeStatusOnServerUpdated implements Action {
  readonly type = OfficeActionTypes.OfficeStatusOnServerUpdated;

  constructor(public payload: {
    id: number,
    office: OfficeModel,
    isActive: boolean
  }) {
  }
}

export class OfficeStatusUpdated implements Action {
  readonly type = OfficeActionTypes.OfficeStatusUpdated;

  constructor(public payload: {
    id: number,
    office: OfficeModel,
    isActive: boolean
  }) {
  }
}

export class OfficesStatusOnServerUpdated implements Action {
  readonly type = OfficeActionTypes.OfficesStatusOnServerUpdated;

  constructor(public payload: {
    ids: number[],
    offices: OfficeModel[],
    isActive: boolean
  }) {
  }
}

export class OfficesStatusUpdated implements Action {
  readonly type = OfficeActionTypes.OfficesStatusUpdated;

  constructor(public payload: {
    ids: number[],
    offices: OfficeModel[],
    isActive: boolean
  }) {
  }
}

export class OneOfficeOnServerDeleted implements Action {
  readonly type = OfficeActionTypes.OneOfficeOnServerDeleted;

  constructor(public payload: { id: number, companyID: number }) {
  }
}

export class OneOfficeDeleted implements Action {
  readonly type = OfficeActionTypes.OneOfficeDeleted;

  constructor(public payload: { id: number, companyID: number }) {
  }
}

export class ManyOfficesOnServerDeleted implements Action {
  readonly type = OfficeActionTypes.ManyOfficesOnServerDeleted;

  constructor(public payload: { offices: any[], ids: number[] }) {
  }
}


export class ManyOfficesDeleted implements Action {
  readonly type = OfficeActionTypes.ManyOfficesDeleted;

  constructor(public payload: { offices: any[], ids: number[] }) {
  }
}

export class OfficesPageRequested implements Action {
  readonly type = OfficeActionTypes.OfficesPageRequested;

  constructor(public payload: { page: QueryParamsModel, id: number }) {
  }
}

export class OfficesPageLoaded implements Action {
  readonly type = OfficeActionTypes.OfficesPageLoaded;

  constructor(public payload: { offices: OfficeModel[], totalCount: number, page: QueryParamsModel }) {
  }
}

export class OfficesPageCancelled implements Action {
  readonly type = OfficeActionTypes.OfficesPageCancelled;
}

export class OfficesPageToggleLoading implements Action {
  readonly type = OfficeActionTypes.OfficesPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class OfficeActionToggleLoading implements Action {
  readonly type = OfficeActionTypes.OfficeActionToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class ThrowOfficeError implements Action {
  readonly type = OfficeActionTypes.ThrowOfficeError;
  constructor(public payload: { error: any }) {
  }
}

export class FireOfficeSuccess implements Action {
  readonly type = OfficeActionTypes.FireOfficeSuccess;
  constructor(public payload: {action_type: string}) {}
}

export type OfficeActions = OfficeOnServerCreated
| OfficeCreated
| OfficeOnServerUpdated
| OfficeUpdated
| OfficeStatusOnServerUpdated
| OfficeStatusUpdated
| OfficesStatusOnServerUpdated
| OfficesStatusUpdated
| OneOfficeOnServerDeleted
| OneOfficeDeleted
| ManyOfficesOnServerDeleted
| ManyOfficesDeleted
| OfficesPageRequested
| OfficesPageLoaded
| OfficesPageCancelled
| OfficesPageToggleLoading
| OfficeActionToggleLoading
| ThrowOfficeError
| FireOfficeSuccess;
