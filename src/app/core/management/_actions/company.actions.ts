// NGRX
import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
// CRUD
import {QueryParamsModel, QueryResultsModel} from '../../_base/crud';
// Models
import {CompanyModel} from '../_models/company.model';
import {Permission, PermissionActionTypes} from '../../auth';

export enum CompanyActionTypes {
  CompanyOnServerCreated = '[Edit Company Dialog] Company On Server Created',
  CompanyCreated = '[Edit Company Dialog] Company Created',
  CompanyOnServerUpdated = '[Edit Company Dialog] Company On Server Updated',
  CompanyUpdated = '[Edit Company Dialog] Company Updated',
  CompanyStatusOnServerUpdated = '[Company List Page] Company Status On Server Updated',
  CompanyStatusUpdated = '[Company List Page] Company Status Updated',
  CompaniesStatusOnServerUpdated = '[Company List Page] Companies Status On Server Updated',
  CompaniesStatusUpdated = '[Company List Page] Companies Status Updated',
  OneCompanyOnServerDeleted = '[Companies List Page] One Company On Server Deleted',
  OneCompanyDeleted = '[Companies List Page] One Company Deleted',
  ManyCompaniesOnServerDeleted = '[Companies List Page] Many Companies On Server Deleted',
  ManyCompaniesDeleted = '[Companies List Page] Many Companies Deleted',
  CompaniesPageRequested = '[Companies List Page] Companies Page Requested',
  CompaniesPageLoaded = '[Companies API] Companies Page Loaded',
  CompaniesPageCancelled = '[Companies API] Companies Page Cancelled',
  CompaniesPageToggleLoading = '[Companies] Companies Page Toggle Loading',
  CompanyActionToggleLoading = '[Companies] Companies Action Toggle Loading',
  ThrowCompanyError = '[Companies] Throw Company Error',
  FireCompanySuccess = '[Companies] Fire Success',
  AllCompaniesRequested = '[Companies] All Companies Requested',
  AllCompaniesLoaded = '[Companies] All Companies Loaded'
}

export class CompanyOnServerCreated implements Action {
  readonly type = CompanyActionTypes.CompanyOnServerCreated;
  constructor(public payload: { company: CompanyModel }) {
  }
}

export class CompanyCreated implements Action {
  readonly type = CompanyActionTypes.CompanyCreated;

  constructor(public payload: { company: CompanyModel }) {
  }
}

export class CompanyOnServerUpdated implements Action {
  readonly type = CompanyActionTypes.CompanyOnServerUpdated;

  constructor(public payload: {
    partialCompany: Update<CompanyModel>, // For State update
    company: CompanyModel // For Server update (through service)
  }) {
  }
}

export class CompanyUpdated implements Action {
  readonly type = CompanyActionTypes.CompanyUpdated;

  constructor(public payload: {
    partialCompany: Update<CompanyModel>, // For State update
    company: CompanyModel // For Server update (through service)
  }) {
  }
}

export class CompanyStatusOnServerUpdated implements Action {
  readonly type = CompanyActionTypes.CompanyStatusOnServerUpdated;

  constructor(public payload: {
    company: CompanyModel,
    id: number,
    isActive: boolean
  }) {
  }
}

export class CompanyStatusUpdated implements Action {
  readonly type = CompanyActionTypes.CompanyStatusUpdated;

  constructor(public payload: {
    company: CompanyModel,
    id: number,
    isActive: boolean
  }) {
  }
}

export class CompaniesStatusOnServerUpdated implements Action {
  readonly type = CompanyActionTypes.CompaniesStatusOnServerUpdated;

  constructor(public payload: {
    companies: CompanyModel[],
    ids: number[],
    isActive: boolean
  }) {
  }
}

export class CompaniesStatusUpdated implements Action {
  readonly type = CompanyActionTypes.CompaniesStatusUpdated;

  constructor(public payload: {
    companies: CompanyModel[],
    ids: number[],
    isActive: boolean
  }) {
  }
}

export class OneCompanyOnServerDeleted implements Action {
  readonly type = CompanyActionTypes.OneCompanyOnServerDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class OneCompanyDeleted implements Action {
  readonly type = CompanyActionTypes.OneCompanyDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class ManyCompaniesOnServerDeleted implements Action {
  readonly type = CompanyActionTypes.ManyCompaniesOnServerDeleted;

  constructor(public payload: { ids: number[] }) {
  }
}

export class ManyCompaniesDeleted implements Action {
  readonly type = CompanyActionTypes.ManyCompaniesDeleted;

  constructor(public payload: { ids: number[] }) {
  }
}

export class CompaniesPageRequested implements Action {
  readonly type = CompanyActionTypes.CompaniesPageRequested;

  constructor(public payload: { page: QueryParamsModel, id: number }) {
  }
}

export class CompaniesPageLoaded implements Action {
  readonly type = CompanyActionTypes.CompaniesPageLoaded;

  constructor(public payload: { companies: CompanyModel[], totalCount: number, page: QueryParamsModel }) {
  }
}

export class CompaniesPageCancelled implements Action {
  readonly type = CompanyActionTypes.CompaniesPageCancelled;
}

export class CompaniesPageToggleLoading implements Action {
  readonly type = CompanyActionTypes.CompaniesPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class CompanyActionToggleLoading implements Action {
  readonly type = CompanyActionTypes.CompanyActionToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class ThrowCompanyError implements Action {
  readonly type = CompanyActionTypes.ThrowCompanyError;

  constructor(public payload: { error: any }) {
  }
}

export class FireCompanySuccess implements Action {
  readonly type = CompanyActionTypes.FireCompanySuccess;
  constructor(public payload: {action_type: string}) {}
}

export class AllCompaniesRequested implements Action {
  readonly type = CompanyActionTypes.AllCompaniesRequested;
}

export class AllCompaniesLoaded implements Action {
  readonly type = CompanyActionTypes.AllCompaniesLoaded;
  constructor(public payload: { companies: CompanyModel[] }) {
   }
}
export type CompanyActions = CompanyOnServerCreated
  | CompanyCreated
  | CompanyOnServerUpdated
  | CompanyUpdated
  | CompanyStatusOnServerUpdated
  | CompanyStatusUpdated
  | CompaniesStatusOnServerUpdated
  | CompaniesStatusUpdated
  | OneCompanyOnServerDeleted
  | OneCompanyDeleted
  | ManyCompaniesOnServerDeleted
  | ManyCompaniesDeleted
  | CompaniesPageRequested
  | CompaniesPageLoaded
  | CompaniesPageCancelled
  | CompaniesPageToggleLoading
  | CompanyActionToggleLoading
  | AllCompaniesRequested
  | AllCompaniesLoaded
  | ThrowCompanyError
  | FireCompanySuccess;
