// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { OfficesState } from '../_reducers/office.reducers';
import { OfficeModel } from '../_models/office.model';

export const selectOfficesState = createFeatureSelector<OfficesState>('offices');

export const selectOfficeById = (officeId: number) => createSelector(
    selectOfficesState,
    officesState => officesState.entities[officeId]
);

export const selectOfficesPageLoading = createSelector(
    selectOfficesState,
    officesState => officesState.listLoading
);

export const selectOfficesActionLoading = createSelector(
    selectOfficesState,
    officesState => officesState.actionsloading
);

export const selectLastCreatedOfficeId = createSelector(
    selectOfficesState,
    officesState => officesState.lastCreatedOfficeId
);

export const selectOfficeError = createSelector(
    selectOfficesState,
    officesState => officesState.error
);

export const selectOfficeLatestSuccessfullAction = createSelector(
  selectOfficesState,
  officesState => officesState.latestSuccessfullAction
);

export const selectOfficesShowInitWaitingMessage = createSelector(
    selectOfficesState,
    officesState => officesState.showInitWaitingMessage
);

export const selectOfficesInStore = createSelector(
    selectOfficesState,
    officesState => {
      const items: OfficeModel[] = [];
      each(officesState.entities, element => {
        items.push(element);
      });
      const httpExtension = new HttpExtenstionsModel();
      const result: OfficeModel[] =
        httpExtension.sortArray(items, officesState.lastQuery.sortField, officesState.lastQuery.sortOrder);
      return new QueryResultsModel(result, officesState.totalCount);
    }
);
