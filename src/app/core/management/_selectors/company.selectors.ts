// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { CompaniesState } from '../_reducers/company.reducers';
import { CompanyModel } from '../_models/company.model';
import * as fromCompanies from '../_reducers/company.reducers';

export const selectCompaniesState = createFeatureSelector<CompaniesState>('companies');

export const selectCompanyById = (companyId: number) => createSelector(
    selectCompaniesState,
    companiesState => companiesState.entities[companyId]
);

export const selectAllCompanies = createSelector(
    selectCompaniesState,
    fromCompanies.selectAll
);

export const selectAllCompaniesIds = createSelector(
    selectCompaniesState,
    fromCompanies.selectIds
);

export const allCompaniesLoaded = createSelector(
    selectCompaniesState,
    ps  => ps._isAllCompaniesLoaded
);

export const selectCompaniesPageLoading = createSelector(
    selectCompaniesState,
    companiesState => companiesState.listLoading
);

export const selectCompaniesActionLoading = createSelector(
    selectCompaniesState,
    companiesState => companiesState.actionsloading
);

export const selectLastCreatedCompanyId = createSelector(
    selectCompaniesState,
    companiesState => companiesState.lastCreatedCompanyId
);

export const selectCompanyError = createSelector(
    selectCompaniesState,
    companiesState => companiesState.error
);

export const selectCompanyLatestSuccessfullAction = createSelector(
  selectCompaniesState,
  companiesState => companiesState.latestSuccessfullAction
);

export const selectCompaniesShowInitWaitingMessage = createSelector(
    selectCompaniesState,
    companiesState => companiesState.showInitWaitingMessage
);

export const selectCompaniesInStore = createSelector(
    selectCompaniesState,
    companiesState => {
      const items: CompanyModel[] = [];
      each(companiesState.entities, element => {
        items.push(element);
      });
      const httpExtension = new HttpExtenstionsModel();
      const result: CompanyModel[] =
        httpExtension.sortArray(items, companiesState.lastQuery.sortField, companiesState.lastQuery.sortOrder);
      return new QueryResultsModel(result, companiesState.totalCount);
    }
);
