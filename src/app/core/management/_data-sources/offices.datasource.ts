import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import { delay, distinctUntilChanged, skip, filter, take, map } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import {
  selectOfficesInStore,
  selectOfficesPageLoading,
  selectOfficesShowInitWaitingMessage
} from '../_selectors/office.selectors';

export class OfficesDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectOfficesPageLoading),
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectOfficesShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectOfficesInStore),
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
