// Models and Consts
export { OfficeModel } from './_models/office.model';
export { CompanyModel } from './_models/company.model';

// DataSources
export { OfficesDataSource } from './_data-sources/offices.datasource';
export { CompaniesDataSource } from './_data-sources/companies.datasource';

// Actions
// Office Actions =>
export {
    OfficeActionTypes,
    OfficeActions,
    OfficeOnServerCreated,
    OfficeCreated,
    OfficeOnServerUpdated,
    OfficeUpdated,
    OfficeStatusOnServerUpdated,
    OfficeStatusUpdated,
    OfficesStatusOnServerUpdated,
    OfficesStatusUpdated,
    OneOfficeOnServerDeleted,
    OneOfficeDeleted,
    ManyOfficesOnServerDeleted,
    ManyOfficesDeleted,
    OfficesPageRequested,
    OfficesPageLoaded,
    OfficesPageCancelled,
    OfficesPageToggleLoading,
    ThrowOfficeError,
    FireOfficeSuccess
} from './_actions/office.actions';

// Company Actions =>
export {
    CompanyActionTypes,
    CompanyActions,
    CompanyOnServerCreated,
    CompanyCreated,
    CompanyOnServerUpdated,
    CompanyUpdated,
    CompanyStatusOnServerUpdated,
    CompanyStatusUpdated,
    CompaniesStatusOnServerUpdated,
    CompaniesStatusUpdated,
    OneCompanyOnServerDeleted,
    OneCompanyDeleted,
    ManyCompaniesOnServerDeleted,
    ManyCompaniesDeleted,
    CompaniesPageRequested,
    CompaniesPageLoaded,
    CompaniesPageCancelled,
    CompaniesPageToggleLoading,
    ThrowCompanyError,
    FireCompanySuccess,
    AllCompaniesLoaded,
    AllCompaniesRequested
} from './_actions/company.actions';

// Effects
export { OfficeEffects } from './_effects/office.effects';
export { CompanyEffects } from './_effects/company.effects';

// Reducers
export { officesReducer } from './_reducers/office.reducers';
export { companiesReducer } from './_reducers/company.reducers';

// Selectors
// Office selectors =>
export {
    selectOfficeById,
    selectOfficesInStore,
    selectOfficesPageLoading,
    selectLastCreatedOfficeId,
    selectOfficesActionLoading,
    selectOfficesShowInitWaitingMessage,
    selectOfficeError,
    selectOfficeLatestSuccessfullAction
} from './_selectors/office.selectors';

// Company selectors =>
export {
    selectCompanyById,
    selectCompaniesInStore,
    selectCompaniesPageLoading,
    selectLastCreatedCompanyId,
    selectCompaniesActionLoading,
    selectCompaniesShowInitWaitingMessage,
    selectCompanyError,
    selectCompanyLatestSuccessfullAction,
    selectAllCompanies,
    selectAllCompaniesIds,
    selectCompaniesState,
    allCompaniesLoaded
} from './_selectors/company.selectors';


// Services
export { OfficesService, CompaniesService } from './_services/';
