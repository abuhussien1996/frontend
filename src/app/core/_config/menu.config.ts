export class MenuConfig {
  public workflowMenu = {
    title: 'Workflow',
    permission: 'accessToWorkflowMenuModule',
    translate: 'WORKFLOW.WORKFLOWS',
    root: true,
    bullet: 'dot',
    icon: ' fab fa-trello',
    submenu: [],
  };
  public inboxMenu = {
    title: 'Communication',
    // permission: 'accessToWorkflowMenuModule',
    // translate: 'WORKFLOW.WORKFLOWS',
    root: true,
    bullet: 'dot',
    icon: 'flaticon2-mail',
    submenu: [],
  };

  public defaults: any = {
    aside: {
      items: [
        {
          title: 'Dashboard',
          icon: 'flaticon2-architecture-and-city',
          page: '/dashboard',
          translate: 'MENU.DASHBOARD',
          bullet: 'dot',
        },
        {section: 'Apps'},
        this.workflowMenu,
        {
          title: 'Legacy Workflows',
          permission: 'accessToEventLegacies',
          icon: 'flaticon-reply',
          page: '/event-legacies',
          translate: 'MENU.LEGACY_WORKFLOWS',
          bullet: 'dot',
        },
        {
          title: 'Task',
          permission: 'accessToTaskModule',
          page: '/task/list',
          icon: ' fa fa-tasks',
          bullet: 'dot',
          root: true,
        },
        {
          title: 'Inbox',
          permission: 'accessToInboxModule',
          icon: 'flaticon2-email',
          page: '/events/inbox',
          translate: 'MENU.INBOX',
          bullet: 'dot',
        },
        {section: 'Applications'},
        this.inboxMenu,
        {
          title: 'Fax',
          // permission: 'accessToInboxModule',
          icon: 'flaticon2-fax',
          page: '/events/fax',
          // translate: 'MENU.INBOX',
          bullet: 'dot',
        },
        {section: 'Lists'},
        {
          title: 'Contact',
          page: '/lists/contacts',
          icon: 'flaticon2-user',
          bullet: 'dot',
        },
        {
          title: 'Entity',
          page: '/lists/entity',
          icon: 'flaticon-users-1',
          bullet: 'dot',
        },
        {
          title: 'Project',
          page: '/lists/project',
          icon: 'flaticon2-gear',
          bullet: 'dot',
        },
        {section: 'Settings'},
        {
          title: 'Events',
          permission: 'accessToEventsModule',
          translate: 'MENU.EVENTS',
          bullet: 'dot',
          icon: 'flaticon2-checking',
          root: true,
          submenu: [
            {
              title: 'Action',
              permission: 'accessToActionModule',
              translate: 'MENU.ACTION',
              page: '/events/event-actions'
            },
            {
              title: 'Event',
              permission: 'accessToEventModule',
              translate: 'MENU.EVENT',
              page: '/events/event-events'
            },
            {
              title: 'Workflow Group',
              permission: 'accessToWorkflowGroupModule',
              translate: 'MENU.WORKFLOW_GROUP',
              page: '/events/workflow-group'
            },
            {
              title: 'Workflow',
              permission: 'accessToWorkflowModule',
              translate: 'MENU.WORKFLOW',
              page: '/events/event-workflows'
            },
            {
              title: 'API',
              permission: 'accessToApiModule',
              translate: 'MENU.API',
              page: '/events/apis'
            }
          ]
        },
        {
          title: 'Management',
          permission: 'accessToManagementModule',
          translate: 'MENU.MANAGEMENT',
          bullet: 'dot',
          icon: 'flaticon2-user-outline-symbol',
          root: true,
          submenu: [
            {
              title: 'User Management',
              permission: 'accessToUserManagementMenu',
              translate: 'MENU.USER_MANAGEMENT',
              bullet: 'dot',
              root: true,
              submenu: [{
                title: 'User',
                translate: 'MENU.USER',
                permission: 'accessToUserManagementModule',
                page: '/user-management/users'
              },
                {
                  title: 'Role',
                  translate: 'MENU.ROLE',
                  permission: 'accessToRoleModule',
                  page: '/user-management/roles'
                },
                {
                  title: 'Team',
                  page: '/lists/team',
                  permission: 'accessToTeamModule',
                }]
            },
            {
              title: 'Task Template',
              permission: 'accessToTemplateModule',
              translate: 'MENU.TEMPLATE',
              page: '/lists/template'
            },
            {
              title: 'Activity Log',
              page: '/activity-log',
              permission: 'accessToActivityLogModule',
              translate: 'MENU.ACTIVITY_LOG',
              bullet: 'dot',
            },
            {
              title: 'Company',
              permission: 'accessToCompanyModule',
              translate: 'MENU.COMPANY',
              page: '/management/companies'
            },
            {
              title: 'Entity',
              permission: 'accessToCompanyModule',
              translate: 'MENU.ENTITY',
              page: '/management/entity'
            },
            {
              title: 'Projects',
              permission: 'accessToCompanyModule',
              translate: 'MENU.PROJECTS',
              page: '/management/project'
            },
            {
              title: 'Office',
              permission: 'accessToOfficeModule',
              translate: 'MENU.OFFICE',
              page: '/management/offices'
            },
            {
              title: 'Category',
              permission: 'accessToCategoryModule',
              page: '/lists/category'
            },
            {section: ' '}
          ]
        },
      ]
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
