export class PageConfig {
  public defaults: any = {/*
    dashboard: {
      page: {
        title: 'Dashboard',
        desc: 'Latest updates and statistic charts'
      },
    },
    'activity-log': {
      page: {
        title: 'Activity Log',
        desc: ''
      },
    },
    management: {
      companies: {
        page: {title: 'Company', desc: ''}
      },
      offices: {
        page: {title: 'Office', desc: ''}
      }
    },
    'user-management': {
      users: {
        page: {title: 'Users', desc: ''}
      },
      roles: {
        page: {title: 'Roles', desc: ''}
      }
    },
    profile: {
      page: {title: 'User Profile', desc: ''}
    },
    error: {
      404: {
        page: {title: '404 Not Found', desc: '', subheader: false}
      },
      403: {
        page: {title: '403 Access Forbidden', desc: '', subheader: false}
      }
    }
  */};

  public get configs(): any {
    return this.defaults;
  }
}
