import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../_models/user.model';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {CompanyModel} from '../../management/_models/company.model';
import {environment} from 'src/environments/environment';
import {OfficeModel} from '../../management/_models/office.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';
import * as _ from 'lodash';
import {of} from 'rxjs/internal/observable/of';
import {AuthServer} from '../../enums/enums';


@Injectable({providedIn: 'root'})
export class UserService {
  public currentUser: Observable<User>;
  private authToken;

  constructor(
    private httpClient: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  createUser(user: User) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    const userName = user.userName
    const isActive = user.isActive
    const companyID = user.companyID
    const email = user.email
    const password = user.password
    const firstName = user.firstName
    const lastName = user.lastName
    const roles = user.roles
    const primaryOfficeId = user.primaryOffice
    const otherOfficeIds = user.allowedOffices
    const addProfile = user.addProfile ? user.addProfile : false
    return this.httpClient.post<User>(environment.USER_BASE + '/c-user', {
      userName, firstName, lastName, isActive, email, password,
      companyID, primaryOfficeId, otherOfficeIds, roles, addProfile
    }, {headers: httpHeaders})
  }


  getUserById(userId?: number): Observable<User> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    httpHeaders.set('access-control-allow-methods', 'POST');
    return this.httpClient.post<User>(environment.USER_BASE + '/user-by-id', {id: userId}, {headers: httpHeaders});
  }

  getUserByUserName(userName?: any[]): Observable<any> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    httpHeaders.set('access-control-allow-methods', 'POST');

    let param = ''
    userName = _.uniqWith(userName, _.isEqual);
    userName.forEach(element => param = param + '&name=' + element)
    const params = new HttpParams({fromString: param});

    return this.httpClient.get<any>(environment.USER_BASE + '/users-by-ids', {headers: httpHeaders, params});
  }

  updatStatusSingle(id?: number, isActive?: boolean) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.put<any>(environment.USER_BASE + '/userstatusupdatesingle', {
      id,
      isActive
    }, {headers: httpHeaders});
  }


  deleteUser(id?: number, companyID?: number) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<any>(environment.USER_BASE + '/single-soft-delete-user', {
      id,
      companyID
    }, {headers: httpHeaders});
  }

  deleteUsers(ids: any[]) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<any>(environment.USER_BASE + '/multiple-soft-delete-user', {ids}, {headers: httpHeaders});
  }

  updateStatusForUsers(ids?: User[], isActive?: number) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    const idsForUpdate: any[] = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < ids.length; i++) {
      idsForUpdate.push(ids[i].id);
    }
    const body = {
      ids: idsForUpdate,
      isActive
    };
    return this.httpClient.put<any>(environment.USER_BASE + '/userstatusupdate', body, {headers: httpHeaders});

  }


  updateUser(_user: User): Observable<any> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    const userName = _user.userName
    const email = _user.email
    const firstName = _user.firstName
    const lastName = _user.lastName
    const roles = _user.roles
    const primaryOfficeId = _user.primaryOffice
    const otherOfficeIds = _user.allowedOffices
    const isActive = _user.isActive
    const id = _user.id
    return this.httpClient.put(
      environment.USER_BASE + '/user',
      {userName, lastName, firstName, email, isActive, id, roles, primaryOfficeId, otherOfficeIds},
      {headers: httpHeaders}
    );
  }


  findUsers(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const body = {
      pageSize: queryParams.pageSize,
      pageIndex: queryParams.pageNumber,
      searchData: queryParams.filter.searchData,
      isActive: queryParams.filter.isActive

    }
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<QueryResultsModel>(environment.USER_BASE + '/all-users', body, {headers: httpHeaders});
  }

  searchUser(categoryId, searchTerm) {
    return this.searchUsers(searchTerm, categoryId)
  }

  searchUsers(searchTerm: string, categoryId = null): Observable<any> {
    let httpHeaders = new HttpHeaders();
    const category = categoryId ? 'category_id=' + categoryId : ''
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);

    return this.httpClient.get<any>(environment.USER_BASE + '/autocomplete-user?' + category + '&term=' + searchTerm, {headers: httpHeaders});
  }

  changePassword(userName?: string, oldPassword?: any, newPassword?: any) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    let body = {}
    body = {userName, newPassword}
    if (oldPassword !== '')
      body = {userName, newPassword, oldPassword}
    return this.httpClient.post(
      environment.USER_BASE + '/change-password', body
      ,
      {headers: httpHeaders}
    );
  }

  getAllCompanies(): Observable<CompanyModel[]> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.get<CompanyModel[]>(environment.USER_BASE + '/company-no-admin', {headers: httpHeaders});
  }


  getAllOffices(): Observable<OfficeModel[]> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.get<OfficeModel[]>(environment.USER_BASE + '/all-office', {headers: httpHeaders});
  }

  changeProfilePic(_user: any): Observable<any> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.put(
      environment.USER_BASE + '/user',
      _user,
      {headers: httpHeaders}
    );
  }

  getOAuthURL(authServer:AuthServer,list?, categoryId?): Observable<any> {
    let params = new HttpParams();
    const urlString = {
      [AuthServer.GOOGLE]:'google',
      [AuthServer.MICROSOFT]:'microsoft',
    }

    if (categoryId) {
      params = params.set('categoryId', categoryId);
      params = params.set('list', list);
    }
    return this.httpClient.get(environment.USER_BASE + `/${urlString[authServer]}_oauth_url`, {params});
  }

  disconnectFromAuthServer(categoryId?): Observable<any> {
    const body = {
      category_id: categoryId
    }
    return this.httpClient.put(environment.USER_BASE + '/deactivate-inbox', body);
  }
  connectFax(categoryId): Observable<any>{
    const body = {
      category_id: categoryId
    }
    return this.httpClient.post(environment.USER_BASE + '/create_documo_user' , body );
  }

  disconnectFax(categoryId): Observable<any>{
    const body = {
      category_id:categoryId
    }
    return this.httpClient.put(environment.USER_BASE + '/delete_documo_user' , body);
  }
  authenticateToNylas(code, provider, categoryId?): Observable<any> {
    const body = {
      google_code: code,
      microsoft_code: code,
      category_id: categoryId
    }
    // tslint:disable-next-line:triple-equals
    provider == 'MICROSOFT'? delete body.google_code : delete body.microsoft_code
    return this.httpClient.post(environment.USER_BASE + '/authenticate_to_nylas', body);
  }
}
