import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {User} from '../_models/user.model';
import {Permission} from '../_models/permission.model';
import {Role} from '../_models/role.model';
import {filter, map} from 'rxjs/operators';
import {HttpUtilsService, QueryParamsModel} from '../../_base/crud';
import {environment} from '../../../../environments/environment';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../_selectors/auth.selectors';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Login, Logout, UserLoaded} from '../_actions/auth.actions';
import {AllCompaniesRequested} from '../../management';

const API_URL = environment.USER_BASE;

@Injectable({providedIn: 'root'})
export class AuthService {
  companyId: number;
  redirectUrl: string;
  currentUserId;
  private user$ = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
    private store: Store<AppState>,
    private httpUtils: HttpUtilsService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.currentUserSubject = new BehaviorSubject<User>(null);
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.store.pipe(select(currentUser),filter(res => !!res.id)).subscribe(res => {
          this.companyId = res.companyID
          this.user$.next(res);
          this.currentUserId = res.id;
    });

    // if (this.currentUserValue) {
    //   // this.startRefreshTokenTimer();
    //   this.startaccessTokenTimer();
    // }

  }

  public getUser(): Observable<User> {
    return this.user$.asObservable();
  }

  public setUser(user) {
    return this.user$.next(user);
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  private authToken;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;


  // helper methods

  private accessTokenTimeout;

  // Authentication/Authorization

  login(userName: string, password: string): Observable<User> {
    return this.http.post<any>(API_URL + '/login', {userName, password})
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        if (user.code === 200) {
          user.loginTime = new Date();
          this.store.dispatch(new Login({authToken: user.accessToken}));
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('measurementSystem', user.measurementSystem)
          this.currentUserSubject.next(user);
          // this.startaccessTokenTimer();
          if (user && user.typeRole === 'Super Admin') {
            this.store.dispatch(new AllCompaniesRequested());
          }
          return user;
        }
        return;
      }));
  }

  autoLogin() {
    const user = localStorage.getItem('userInfo');
    try {
      if (user) {
        this.store.dispatch(new UserLoaded({user: JSON.parse(user)}));
      }
    } catch (e) {
    }
  }

  logout(saveURL = true) {
    this.store.dispatch(new Logout(saveURL));
    // this.endAccessTokenTimer();
    this.currentUserSubject.next(null);
  }

  getUserByToken(): Observable<User> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<User>(API_URL + '/user-by-token', {headers: httpHeaders}).pipe(map(user => {
      localStorage.setItem('userInfo', JSON.stringify(user));
      return user

    }))
  }

  checkExist(username?: string): Observable<User> {
    const userToken = localStorage.getItem(environment.authTokenKey);
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const userName = username
    return this.http.post<User>(API_URL + '/exist-username', {userName}, {headers: httpHeaders})

  }


  public requestPassword(email: string): Observable<any> {
    return this.http.post(API_URL + '/sendPasswordResetEmail', {email})
  }


  getAllUsers(): Observable<any[]> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any[]>(API_URL + '/all-user', {headers: httpHeaders});
  }


  getUserById(userId?: number): Observable<User> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<User>(API_URL + '/user-by-id', {id: userId}, {headers: httpHeaders});
  }


  // private startaccessTokenTimer() {
  //   if (this.currentUserValue.accessToken != null) {
  //     const jwtToken = JSON.parse(atob(this.currentUserValue.accessToken.split('.')[1]));
  //     const expires = new Date(jwtToken.exp * 1000);
  //     const timeoutaccess = expires.getTime() - Date.now() - (1 * 1000);
  //     this.accessTokenTimeout = setTimeout(() => this.logout(), timeoutaccess);
  //   }
  // }


  // private endAccessTokenTimer() {
  //   clearTimeout(this.accessTokenTimeout);
  // }

  resetPassword(newPassword?: any) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const userTokenForget = this.route.snapshot.queryParams.param1;

    return this.http.post(API_URL + '/receiveNewPassword', {newPassword, token: userTokenForget}, {headers: httpHeaders});
  }

  // Permission
  getAllPermissions(): Observable<Permission[]> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<Permission[]>(API_URL + '/permissions', {headers: httpHeaders});
  }

  // Roles
  getAllRoles(): Observable<Role[]> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<Role[]>(API_URL + '/roles', {headers: httpHeaders});
  }


  getAllUserRoles(): Observable<Role[]> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<Role[]>(API_URL + '/user-roles', {headers: httpHeaders});
  }

  // CREATE =>  POST: add a new role to the server
  createRole(role: Role): Observable<Role> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<Role>(API_URL + '/c-role', role, {headers: httpHeaders});
  }

  // UPDATE => PUT: update the role on the server
  updateRole(role: Role): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put(API_URL + '/u-role', role, {headers: httpHeaders});
  }

  // DELETE => delete the role from the server
  deleteRole(roleId: number): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id: roleId
    }
    return this.http.put<Role>(API_URL + '/d-role', body, {headers: httpHeaders});
  }

  // DELETE => delete the roles from the server
  deleteRoles(roleIds: number[]): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids: roleIds
    }
    return this.http.put<Role>(API_URL + '/d-roles', body, {headers: httpHeaders});
  }

  findRoles(queryParams: QueryParamsModel): Observable<any> {
    // This code imitates server calls
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      pageIndex: queryParams.pageNumber,
      pageSize: queryParams.pageSize,
      searchData: queryParams.filter.searchData
    }
    return this.http.post<any>(API_URL + '/find-roles', body, {headers: httpHeaders});
  }

  private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }
  searchRoles(term: string): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = environment.USER_BASE + '/roles/autocomplete?term=' + term;
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  setRedirectUrl(s: string) {
    this.redirectUrl = s;
    localStorage.setItem('redirectUrl', s);
  }
}
