import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {environment} from '../../../../environments/environment';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';


@Injectable({providedIn: 'root'})

export class ActivityService {
  private authToken;

  constructor(
    private httpClient: HttpClient,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  getAllActivity(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const body = {
      pageSize: queryParams.pageSize,
      pageIndex: queryParams.pageNumber,
      searchData: queryParams.filter.searchData,
      companyID: queryParams.filter.companyID
    }
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST')
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<QueryResultsModel>(environment.USER_BASE + '/activity-log', body, {headers: httpHeaders})
  }
}
