// Angular
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
// RxJS
import {Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
// NGRX
import {select, Store} from '@ngrx/store';
// Module reducers and selectors
import {AppState} from '../../../core/reducers/';
import {currentUserPermissions} from '../_selectors/auth.selectors';
import {Permission} from '../_models/permission.model';
import {find} from 'lodash';
import {NgxPermissionsService} from 'ngx-permissions';

@Injectable()
export class ModuleGuard implements CanActivate {
  readonly permission

  constructor(private store: Store<AppState>,
              private permissionsService: NgxPermissionsService,
              private router: Router
  ) {
    this.permission = JSON.parse(localStorage.getItem('currentUserPermissions'));
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    const permissionName = route.data.permissionName as string;
    if (!permissionName) {
      return of(false);
    }

    return this.store
      .pipe(
        select(currentUserPermissions),
        map((permissions: Permission[]) => {
          const currentPermissions = permissions.length > 0 ? permissions : this.permission;
          const perm = find(currentPermissions, (elem: Permission) => {
            return elem.name.toLocaleLowerCase() === permissionName.toLocaleLowerCase();
          });
          return perm || (this.permissionsService.getPermissions())[permissionName] ? true : false;
        }),
        tap(hasAccess => {
          if (!hasAccess) {
            this.router.navigateByUrl('/error/403');
          }
        })
      );
  }
}
