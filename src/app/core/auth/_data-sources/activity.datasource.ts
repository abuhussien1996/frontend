// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../../core/reducers';
import { selectActivityInStore, selectActivityPageLoading, selectActivityShowInitWaitingMessage } from '../_selectors/activity.selectors';


export class ActivityDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectActivityPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectActivityShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectActivityInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
