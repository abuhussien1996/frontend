// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { User } from '../_models/user.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum UserActionTypes {
    AllUsersRequested = '[Users Module] All Users Requested',
    AllUsersLoaded = '[Users API] All Users Loaded',
    UserOnServerCreated = '[Edit User Component] User On Server Created',
    UserCreated = '[Edit User Dialog] User Created',
    UserOnServerUpdated = '[Edit User Dialog] User On Server Updated',
    UserUpdated = '[Edit User Dialog] User Updated',
    UserStatusOnServerUpdated = '[Users List Page] User Status On Server Updated Updated',
    UserStatusUpdated = '[Users List Page] User Status Updated Updated',
    UsersStatusOnServerUpdated = '[Users List Page] Users Status On Server Updated Updated',
    UsersStatusUpdated = '[Users List Page] Users Status Updated Updated',
    UserOnServerDeleted = '[Users List Page] User On Server Deleted',
    UserDeleted = '[Users List Page] User Deleted',
    ManyUsersOnServerDeleted = '[Users List Page] Users On Server Deleted',
    ManyUsersDeleted = '[Users List Page] Users Deleted',
    UsersPageRequested = '[Users List Page] Users Page Requested',
    UsersPageLoaded = '[Users API] Users Page Loaded',
    UsersPageCancelled = '[Users API] Users Page Cancelled',
    UsersPageToggleLoading = '[Users] Users Page Toggle Loading',
    UsersActionToggleLoading = '[Users] Users Action Toggle Loading',
    ChangePassword = '[Users] Password changed',
    ActivityPageRequested = '[Users] ActivityPageRequested',
    ThrowError = '[Users] ThrowError',
    ClearErrors = '[Users] ClearErrors'
}

export class UserOnServerCreated implements Action {
    readonly type = UserActionTypes.UserOnServerCreated;
    constructor(public payload: { user: User ,file?:any }) { }
}

export class UserCreated implements Action {
    readonly type = UserActionTypes.UserCreated;
    constructor(public payload: { user: User }) { }
}

export class ThrowError implements Action {
    readonly type = UserActionTypes.ThrowError;

    constructor(public payload: { error: any }) {
    }
}

export class ClearErrors implements Action {
    readonly type = UserActionTypes.ClearErrors;
}
export class UserOnServerUpdated implements Action {
    readonly type = UserActionTypes.UserOnServerUpdated;
    constructor(public payload: {
        partialUser: Update<User>,
        user: User
    }) { }
}
export class UserUpdated implements Action {
    readonly type = UserActionTypes.UserUpdated;
    constructor(public payload: {
        partialUser: Update<User>,
        user: User
    }) { }
}


export class UserStatusOnServerUpdated implements Action {
    readonly type = UserActionTypes.UserStatusOnServerUpdated;
    constructor(public payload: { id: number, isActive: boolean }) { }
}


export class UserStatusUpdated implements Action {
    readonly type = UserActionTypes.UserStatusUpdated;
    constructor(public payload: { id: number, isActive: boolean }) { }
}


export class UsersStatusOnServerUpdated implements Action {
    readonly type = UserActionTypes.UsersStatusOnServerUpdated;
    constructor(public payload: {
        users: User[],
        isActive: number
    }) { }
}


export class UsersStatusUpdated implements Action {
    readonly type = UserActionTypes.UsersStatusUpdated;
    constructor(public payload: {
        users: User[],
        isActive: number
    }) { }
}

export class UserOnServerDeleted implements Action {
    readonly type = UserActionTypes.UserOnServerDeleted;
    constructor(public payload: { id: number, companyID: number }) { }
}

export class UserDeleted implements Action {
    readonly type = UserActionTypes.UserDeleted;
    constructor(public payload: { id: number, companyID: number }) { }
}

export class ManyUsersOnServerDeleted implements Action {
    readonly type = UserActionTypes.ManyUsersOnServerDeleted;
    constructor(public payload: { ids: any[] }) { }
}

export class ManyUsersDeleted implements Action {
    readonly type = UserActionTypes.ManyUsersDeleted;
    constructor(public payload: { ids: any[] }) { }
}



export class UsersPageRequested implements Action {
    readonly type = UserActionTypes.UsersPageRequested;
    constructor(public payload: { page: QueryParamsModel }) {
    }
}

export class UsersPageLoaded implements Action {
    readonly type = UserActionTypes.UsersPageLoaded;
    constructor(public payload: { users: User[], totalCount: number, page: QueryParamsModel }) { }
}


export class UsersPageCancelled implements Action {
    readonly type = UserActionTypes.UsersPageCancelled;
}

export class UsersPageToggleLoading implements Action {
    readonly type = UserActionTypes.UsersPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class UsersActionToggleLoading implements Action {
    readonly type = UserActionTypes.UsersActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class ChangePassword implements Action {
    readonly type = UserActionTypes.ChangePassword;
    constructor(public payload: { userName: string, oldPassword: string, newPassword: string }) { }
}



export type UserActions = UserCreated
    | UserUpdated
    | UserDeleted
    | ManyUsersDeleted
    | UserOnServerCreated
    | UserOnServerUpdated
    | UserOnServerDeleted
    | ManyUsersOnServerDeleted
    | UserStatusOnServerUpdated
    | UsersStatusOnServerUpdated
    | UsersPageLoaded
    | UsersPageCancelled
    | UsersPageToggleLoading
    | UsersPageRequested
    | ChangePassword
    | UserStatusUpdated
    | UsersStatusUpdated
    | ThrowError
    | ClearErrors
    | UsersActionToggleLoading;
