// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Activity } from '../_models/activity.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum ActivityActionTypes {
    AllActivityRequested = '[Activity Module] All Activity Requested',
    AllActivityLoaded = '[Activity API] All Activity Loaded',
    ActivityOnServerCreated = '[Edit Activity Component] Activity On Server Created',
    ActivityCreated = '[Edit Activity Dialog] Activity Created',
    ActivityUpdated = '[Edit Activity Dialog] Activity Updated',
    ActivityDeleted = '[Activity List Page] Activity Deleted',
    ActivityPageRequested = '[Activity List Page] Activity Page Requested',
    ActivityPageLoaded = '[Activity API] Activity Page Loaded',
    ActivityPageCancelled = '[Activity API] Activity Page Cancelled',
    ActivityPageToggleLoading = '[Activity] Activity Page Toggle Loading',
    ActivityActionToggleLoading = '[Activity] Activity Action Toggle Loading'
}

export class ActivityOnServerCreated implements Action {
    readonly type = ActivityActionTypes.ActivityOnServerCreated;
    constructor(public payload: { activity: Activity }) { }
}

export class ActivityCreated implements Action {
    readonly type = ActivityActionTypes.ActivityCreated;
    constructor(public payload: { activity: Activity }) { }
}


export class ActivityUpdated implements Action {
    readonly type = ActivityActionTypes.ActivityUpdated;
    constructor(public payload: {
        partialActivity: Update<Activity>,
        activity: Activity
    }) { }
}

export class ActivityDeleted implements Action {
    readonly type = ActivityActionTypes.ActivityDeleted;
    constructor(public payload: { id: number }) {}
}

export class ActivityPageRequested implements Action {
    readonly type = ActivityActionTypes.ActivityPageRequested;
    constructor(public payload: { page: QueryParamsModel }) { }
}

export class ActivityPageLoaded implements Action {
    readonly type = ActivityActionTypes.ActivityPageLoaded;
    constructor(public payload: { activities: Activity[], totalCount: number, page: QueryParamsModel  }) { }
}


export class ActivityPageCancelled implements Action {
    readonly type = ActivityActionTypes.ActivityPageCancelled;
}

export class ActivityPageToggleLoading implements Action {
    readonly type = ActivityActionTypes.ActivityPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class ActivityActionToggleLoading implements Action {
    readonly type = ActivityActionTypes.ActivityActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}


export type ActivityActions = ActivityCreated
| ActivityUpdated
| ActivityDeleted
| ActivityOnServerCreated
| ActivityPageLoaded
| ActivityPageCancelled
| ActivityPageToggleLoading
| ActivityPageRequested
| ActivityActionToggleLoading;
