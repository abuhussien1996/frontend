import { Action } from '@ngrx/store';
import { User } from '../_models/user.model';

export enum AuthActionTypes {
    Login = '[Auth] Login',
    Logout = '[Auth] Logout',
    Register = '[Auth] Register',
    UserRequested = '[Auth] Request User',
    UserLoaded = '[Auth] Load User',
    ResetPassword = '[Auth] Password changed',
    RefreshAccessToken = '[Auth] Refresh Access Token',
    RequestPassword = '[Auth] RequestPassword',
    AuthActionToggleLoading = '[Auth] Action Toggle Loading',
    ThrowError = '[Auth] ThrowError',
    ClearErrors = '[Auth] ClearErrors'
}

export class Login implements Action {
    readonly type = AuthActionTypes.Login;
    constructor(public payload: { authToken: string }) { }
}

export class Logout implements Action {
    readonly type = AuthActionTypes.Logout;
    constructor(private saveURL = true) {
    }
}

export class Register implements Action {
    readonly type = AuthActionTypes.Register;
    constructor(public payload: { authToken: string }) { }
}


export class UserRequested implements Action {
    readonly type = AuthActionTypes.UserRequested;
}

export class UserLoaded implements Action {
    readonly type = AuthActionTypes.UserLoaded;
    constructor(public payload: { user: User }) { }
}

export class RefreshAccessToken implements Action {
    readonly type = AuthActionTypes.RefreshAccessToken;
    constructor(public payload: { accessToken: string }) { }
}

export class RequestPassword implements Action {
    readonly type = AuthActionTypes.RequestPassword;
    constructor(public payload: { email: string }) { }
}

export class AuthPageToggleLoading implements Action {
    readonly type = AuthActionTypes.AuthActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class AuthActionToggleLoading implements Action {
    readonly type = AuthActionTypes.AuthActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}


export class ThrowError implements Action {
    readonly type = AuthActionTypes.ThrowError;

    constructor(public payload: { error: any }) {
    }
}

export class ClearErrors implements Action {
    readonly type = AuthActionTypes.ClearErrors;
}
export type AuthActions =
Login
| Logout
| Register
| UserRequested
| UserLoaded
| RefreshAccessToken
| RequestPassword
| AuthPageToggleLoading
| AuthActionToggleLoading
|ClearErrors
|ThrowError;
