// NGRX
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { RoleActions, RoleActionTypes } from '../_actions/role.actions';
// Models
import { Role } from '../_models/role.model';
import { QueryParamsModel } from '../../_base/crud';

export interface RolesState extends EntityState<Role> {
    isAllRolesLoaded: boolean;
    queryRowsCount: number;
    queryResult: Role[];
    lastCreatedRoleId: number;
    listLoading: boolean;
    actionsloading: boolean;
    lastQuery: QueryParamsModel;
    showInitWaitingMessage: boolean;
    allRoles: Role[];
    error: any|null;
    latestSuccessfullAction: any;
}

export const adapter: EntityAdapter<Role> = createEntityAdapter<Role>();

export const initialRolesState: RolesState = adapter.getInitialState({
  isAllRolesLoaded: false,
  queryRowsCount: 0,
  queryResult: [],
  lastCreatedRoleId: undefined,
  listLoading: false,
  actionsloading: false,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
  allRoles: [],
  error: null,
  latestSuccessfullAction: null
});

export function rolesReducer(state = initialRolesState, action: RoleActions): RolesState {
  switch (action.type) {
    case RoleActionTypes.RolesPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedRoleId: undefined
      };
    case RoleActionTypes.RolesActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case RoleActionTypes.RoleOnServerCreated:
      return {
        ...state
      };
    case RoleActionTypes.RoleOnServerUpdated:
      return {
        ...state
      };
    case RoleActionTypes.RoleOnServerDeleted:
      return {
        ...state
      };
    case RoleActionTypes.RolesOnServerDeleted:
      return {
        ...state
      };
    case RoleActionTypes.RoleCreated:
      return adapter.addOne(action.payload.role, {
        ...state, lastCreatedRoleId: action.payload.role.id
      });
    case RoleActionTypes.RoleUpdated:
      return adapter.updateOne(action.payload.partialrole, state);
    case RoleActionTypes.RoleDeleted:
      return adapter.removeOne(action.payload.id, state);
      case RoleActionTypes.RolesDeleted:
        return adapter.removeMany(action.payload.ids, state);
    case RoleActionTypes.AllRolesLoaded:
      return {
        ...state, allRoles: action.payload.roles
      };
    case RoleActionTypes.RolesPageCancelled:
      return {
        ...state, listLoading: false, showInitWaitingMessage: false, queryRowsCount: 0, queryResult: [], lastQuery: new QueryParamsModel({})
      };
    case RoleActionTypes.RolesPageLoaded:
      return adapter.addMany(action.payload.roles, {
        ...state,
        listLoading: false,
        queryRowsCount: action.payload.totalCount,
        queryResult: action.payload.roles,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
        latestSuccessfullAction: state.latestSuccessfullAction,
        error: null
      });
    case RoleActionTypes.ThrowRoleError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case RoleActionTypes.FireRoleSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
