// Actions
import {AuthActions, AuthActionTypes} from '../_actions/auth.actions';
// Models
import {User} from '../_models/user.model';

export interface AuthState {
  loggedIn: boolean;
  authToken: string;
  user: any;
  isUserLoaded: boolean;
}

export const initialAuthState: AuthState = {
  loggedIn: false,
  authToken: undefined,
  user: {},
  isUserLoaded: false
};

export function authReducer(state = initialAuthState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.Login: {
      const token: string = action.payload.authToken;
      return {
        ...state,
        loggedIn: true,
        authToken: token,
        isUserLoaded: false
      };
    }
    // Do nothing
    case AuthActionTypes.Register: {
      const token: string = action.payload.authToken;
      return {
        ...state,
        loggedIn: true,
        authToken: token,
        isUserLoaded: false
      };
    }

    case AuthActionTypes.Logout:
      return initialAuthState;

    case AuthActionTypes.UserLoaded: {
      const user: User = action.payload.user;
      return {
        ...state,
        user,
        isUserLoaded: true
      };
    }

    // Do nothing
    case AuthActionTypes.RefreshAccessToken: {
      const accessToken: string = action.payload.accessToken;
      return {
        ...state,
        authToken: accessToken
      };
    }

    default:
      return state;
  }
}
