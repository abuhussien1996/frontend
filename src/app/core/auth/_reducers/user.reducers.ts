// NGRX
import {createFeatureSelector} from '@ngrx/store';
import {EntityState, EntityAdapter, createEntityAdapter, Update} from '@ngrx/entity';
// Actions
import {UserActions, UserActionTypes} from '../_actions/user.actions';
// CRUD
import {QueryParamsModel} from '../../_base/crud';
// Models
import {User} from '../_models/user.model';

// tslint:disable-next-line:no-empty-interface
export interface UsersState extends EntityState<User> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedUserId: number;
  lastQuery: QueryParamsModel;
  error: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<User> = createEntityAdapter<User>();

export const initialUsersState: UsersState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedUserId: undefined,
  showInitWaitingMessage: true
});

export function usersReducer(state = initialUsersState, action: UserActions): UsersState {
  switch (action.type) {
    case UserActionTypes.UsersPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedUserId: undefined
      };
    case UserActionTypes.UsersActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case UserActionTypes.UserOnServerCreated:
      return {
        ...state
      };
    case UserActionTypes.UserOnServerUpdated:
      return {
        ...state
      };
    case UserActionTypes.UserStatusOnServerUpdated:
      return {
        ...state
      };
    case UserActionTypes.UsersStatusOnServerUpdated:
      return {
        ...state
      };
    case UserActionTypes.UserOnServerDeleted:
      return {
        ...state
      };
    case UserActionTypes.ManyUsersOnServerDeleted:
      return {
        ...state
      };
    case UserActionTypes.UserCreated:
      return adapter.addOne(action.payload.user, {
        ...state, lastCreatedUserId: action.payload.user.id
      });
    case UserActionTypes.UserUpdated:
      return adapter.updateOne(action.payload.partialUser, state);
    case UserActionTypes.UserStatusUpdated: {
      // tslint:disable-next-line
      const _partialUser: Update<User> = {
        id: action.payload.id,
        changes: {
          isActive: action.payload.isActive
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialUser, state);
    }
    case UserActionTypes.UsersStatusUpdated: {
      // tslint:disable-next-line
      const _partialUsers: Update<User>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.users.length; i++) {
        _partialUsers.push({
          id: action.payload.users[i].id,
          changes: {
            isActive: action.payload.isActive === 1 ? true : false
          }
        });
      }
      return adapter.updateMany(_partialUsers, state);
    }
    case UserActionTypes.UserDeleted:
      return adapter.removeOne(action.payload.id, state);
    case UserActionTypes.ManyUsersDeleted:
      const _ids = []
      action.payload.ids.forEach((obj) => _ids.push(obj.id));
      return adapter.removeMany(_ids, state);
    case UserActionTypes.UsersPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case UserActionTypes.UsersPageLoaded: {
      return adapter.addMany(action.payload.users, {
        ...initialUsersState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false
      });
    }
    case UserActionTypes.ThrowError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case UserActionTypes.ClearErrors: {
      return {
        ...state, error: null
      };
    }
    default:
      return state;
  }
}

export const getUserState = createFeatureSelector<UsersState>('users');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
