import { BaseModel } from '../../_base/crud';

export class Role extends BaseModel {
  id: number;
  companyID: number;
  title: string;
  permissions: number[];
  type: number;

  clear(): void {
    this.id = 0;
    this.companyID = 0;
    this.title = '';
    this.permissions = [];
    this.type = 0;
  }
}
