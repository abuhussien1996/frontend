import { BaseModel } from '../../_base/crud';


export class Activity extends BaseModel {
  id: number;
  log_name:string;
  causer: string;
  log_desc: string;
  log_at: Date;
  subject_name: string;
  subject_id: number;
 

  clear(): void {
    this.id = undefined;
    this.log_name = '';
    this.causer = '';
    this.log_desc = '';
    this.log_at = undefined;
    this.subject_name= '';
    this.subject_id = undefined;
    
 
  }
}
