import { BaseModel } from '../../_base/crud';

export class User extends BaseModel {
  id: number;
  userName:string;
  firstName:string;
  lastName:string;
  password: string;
  confirmPassword:string;
  email: string;
  accessToken: string;
  refreshToken: string;
  roles: number[];
  fullName: string;
  companyID: number;
  company: string;
  primaryOffice:number;
  allowedOffices:number[];
  isActive: boolean;
  typeRole:string
	name: string;
  inboxActivated:boolean;
  addProfile
  profileUrl
  companyName
  clear(): void {
    this.id = 0;
    this.userName = '';
    this.firstName = '';
    this.lastName = '';
    this.password = '';
    this.confirmPassword = '';
    this.email = '';
    this.companyName = '';
    this.roles = [];
    this.typeRole = ''
    this.fullName = '';
    this.accessToken = 'access-token-' + Math.random();
    this.refreshToken = 'access-token-' + Math.random();
    this.companyID = undefined;
    this.company = '';
    this.primaryOffice = undefined;
    this.allowedOffices = [];
    this.isActive = false;
  }
}
