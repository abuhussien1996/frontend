import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';
import { AuthService } from 'src/app/core/auth/_services/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        throw new Error('Method not implemented.');
    }

    // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     // add auth header with jwt if user is logged in and request is to the api url
    //     const user = this.authenticationService.currentUserValue;
    //     const isLoggedIn = user && user.accessToken;
    //     const isApiUrl = request.url.startsWith(environment.apiUrl);
    //     // if (isLoggedIn && isApiUrl) {
    //     //     request = request.clone({
    //     //         setHeaders: { Authorization: `Bearer ${user.accessToken}` }
    //     //     });
    //     // }

    //     return next.handle(request);
    // }
}