import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {AuthService} from 'src/app/core/auth/_services/auth.service';
import {Router} from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   throw new Error('Method not implemented.');
  // }

  constructor(private authenticationService: AuthService,
              private router: Router,
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if ([401].includes(err.status)) {
        // auto logout if 401 response returned from api
        this.authenticationService.logout();
      }
      if ([403].includes(err.status)) {
        this.router.navigateByUrl('/error/403');
      }
      if ([404].includes(err.status) && request.url.indexOf('/workflow') !== -1) {
        this.router.navigateByUrl('/error/404');
      }
      const error = err;
      return throwError(error);
    }))
  }
}
