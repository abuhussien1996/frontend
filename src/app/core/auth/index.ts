// SERVICES
export { AuthService } from './_services';
export { AuthNoticeService } from './auth-notice/auth-notice.service';

// DATA SOURCERS
export { RolesDataSource } from './_data-sources/roles.datasource';
export { UsersDataSource } from './_data-sources/users.datasource';

// ACTIONS
export {
    Login,
    Logout,
    Register,
    UserRequested,
    UserLoaded,
    AuthActionTypes,
    AuthActions,
    RefreshAccessToken,
    RequestPassword, AuthPageToggleLoading, AuthActionToggleLoading,ThrowError,ClearErrors
} from './_actions/auth.actions';
export {
    ActivityOnServerCreated,
    ActivityCreated,
    ActivityUpdated,
    ActivityDeleted,
    ActivityPageRequested,
    ActivityPageLoaded,
    ActivityPageCancelled,
    ActivityActionTypes,
    ActivityActions,
	ActivityPageToggleLoading,
	ActivityActionToggleLoading
} from './_actions/activity.actions';
export {
    AllPermissionsRequested,
    AllPermissionsLoaded,
    PermissionActionTypes,
    PermissionActions
} from './_actions/permission.actions';
export {
    RoleOnServerCreated,
    RoleCreated,
    RoleOnServerUpdated,
    RoleUpdated,
    RoleOnServerDeleted,
    RoleDeleted,
    RolesOnServerDeleted,
    RolesDeleted,
    RolesPageRequested,
    RolesPageLoaded,
    RolesPageCancelled,
    AllRolesLoaded,
    AllRolesRequested,
    ThrowRoleError,
    FireRoleSuccess,
    RoleActionTypes,
    RoleActions
} from './_actions/role.actions';
export {
    UserCreated,
    UserUpdated,
    UserDeleted,
    UserOnServerCreated,
    UsersPageLoaded,
    UsersPageCancelled,
    UsersPageToggleLoading,
    UsersPageRequested,
    UsersActionToggleLoading
} from './_actions/user.actions';




// EFFECTS
export { AuthEffects } from './_effects/auth.effects';
export { PermissionEffects } from './_effects/permission.effects';
export { RoleEffects } from './_effects/role.effects';
export { UserEffects } from './_effects/user.effects';
export { ActivityEffects } from './_effects/activity.effects';

// REDUCERS
export { authReducer } from './_reducers/auth.reducers';
export { permissionsReducer } from './_reducers/permission.reducers';
export { rolesReducer } from './_reducers/role.reducers';
export { usersReducer } from './_reducers/user.reducers';
export {ActivityReducer } from './_reducers/activity.reducers';

// SELECTORS
export {
    isLoggedIn,
    isLoggedOut,
    isUserLoaded,
    currentAuthToken,
    currentUser,
    currentUserRoleIds,
    currentUserPermissionsIds,
    currentUserPermissions,
    checkHasUserPermission,
} from './_selectors/auth.selectors';
export {
    selectPermissionById,
    selectAllPermissions,
    selectAllPermissionsIds,
    allPermissionsLoaded,
    selectAllNonSuperAdminPermissions
} from './_selectors/permission.selectors';
export {
    selectRoleById,
    selectAllRoles,
    selectAllCompanyRoles,
    selectAllRolesIds,
    allRolesLoaded,
    selectLastCreatedRoleId,
    selectRolesPageLoading,
    selectQueryResult,
    selectRolesActionLoading,
    selectRolesShowInitWaitingMessage,
    selectRoleError,
    selectRoleLatestSuccessfullAction
} from './_selectors/role.selectors';
export {
    selectActivityById,
    selectActivityState,
    selectActivityPageLoading,
    selectActivityActionLoading,
    selectLastCreatedActivityId,
    selectActivityPageLastQuery,
    selectActivityInStore,
    selectActivityShowInitWaitingMessage,
    selectHasActivityInStore
} from './_selectors/activity.selectors';
export {
    selectUserById,
    selectUsersPageLoading,
    selectLastCreatedUserId,
    selectUsersInStore,
    selectHasUsersInStore,
    selectUsersPageLastQuery,
    selectUsersActionLoading,
    selectUsersShowInitWaitingMessage
} from './_selectors/user.selectors';



// GUARDS
export { AuthGuard } from './_guards/auth.guard';
export { ModuleGuard } from './_guards/module.guard';

// MODELS
export { User } from './_models/user.model';
export { Permission } from './_models/permission.model';
export { Role } from './_models/role.model';
export { Address } from './_models/address.model';
export { AuthNotice } from './auth-notice/auth-notice.interface';

