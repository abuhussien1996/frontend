// Angular
import { Injectable } from '@angular/core';
// RxJS
import { of, Observable, defer, forkJoin } from 'rxjs';
import { mergeMap, map, withLatestFrom, filter, catchError } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// Services
import { AuthService } from '../_services';
// State
import { AppState } from '../../../core/reducers';
// Selectors
import { allRolesLoaded } from '../_selectors/role.selectors';
// Actions
import {
    AllRolesLoaded,
    AllRolesRequested,
    RoleActionTypes,
    RolesPageRequested,
    RolesPageLoaded,
    RoleUpdated,
    RolesPageToggleLoading,
    RoleDeleted,
    RolesDeleted,
    RoleOnServerCreated,
    RoleCreated,
    RolesActionToggleLoading,
    RolesPageCancelled,
    ThrowRoleError,
    FireRoleSuccess,
    RoleOnServerDeleted,
    RolesOnServerDeleted,
    RoleOnServerUpdated
} from '../_actions/role.actions';

@Injectable()
export class RoleEffects {
  showPageLoadingDistpatcher = new RolesPageToggleLoading({isLoading: true});
  hidePageLoadingDistpatcher = new RolesPageToggleLoading({isLoading: false});

  showActionLoadingDistpatcher = new RolesActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new RolesActionToggleLoading({isLoading: false});

  @Effect()
  loadAllRoles$ = this.actions$
    .pipe(
      ofType<AllRolesRequested>(RoleActionTypes.AllRolesRequested),
      withLatestFrom(this.store.pipe(select(allRolesLoaded))),
      filter(([action, isAllRolesLoaded]) => !isAllRolesLoaded),
      mergeMap(() => this.auth.getAllRoles()),
      map(roles => {
        localStorage.setItem('allRole', JSON.stringify(roles));
        return new AllRolesLoaded({roles});
      })
    );

  @Effect()
  loadRolesPage$ = this.actions$
    .pipe(
      ofType<RolesPageRequested>(RoleActionTypes.RolesPageRequested),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.auth.findRoles(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of(error);
          }),
          map(res => {
            if(res.length === 2) {
              const result = res[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery = res[1];
              return new RolesPageLoaded({
                roles: result.pageOfItems,
                totalCount: result.pager.totalCount,
                page: lastQuery
              });
            } else {
              this.store.dispatch(new ThrowRoleError({error: res}));
              return new RolesPageCancelled();
            }
          })
        );
      })
    );

  @Effect()
  deleteRole$ = this.actions$
    .pipe(
      ofType<RoleOnServerDeleted>(RoleActionTypes.RoleOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.auth.deleteRole(payload.id).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if(res.error) {
                return new ThrowRoleError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new FireRoleSuccess({action_type: RoleActionTypes.RoleDeleted}));
                return new RoleDeleted(payload)
              }
            })
          );
        }
      ),
    );

    @Effect()
    deleteRoles$ = this.actions$
      .pipe(
        ofType<RolesOnServerDeleted>(RoleActionTypes.RolesOnServerDeleted),
        mergeMap(({payload}) => {
            this.store.dispatch(this.showActionLoadingDistpatcher);
            return this.auth.deleteRoles(payload.ids).pipe(
              catchError(error => {
                return of({error});
              }),
              map((res) => {
                if(res.error) {
                  return new ThrowRoleError({error: res.error});
                } else {
                  this.store.dispatch(this.hideActionLoadingDistpatcher);
                  this.store.dispatch(new FireRoleSuccess({action_type: RoleActionTypes.RolesDeleted}));
                  return new RolesDeleted(payload)
                }
              })
            );
          }
        ),
      );

  @Effect()
  updateRole$ = this.actions$
    .pipe(
      ofType<RoleOnServerUpdated>(RoleActionTypes.RoleOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.auth.updateRole(payload.role).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowRoleError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireRoleSuccess({action_type: RoleActionTypes.RoleUpdated}));
              return new RoleUpdated(payload)
            }
          })
        );
      }),
    );


  @Effect()
  createRole$ = this.actions$
    .pipe(
      ofType<RoleOnServerCreated>(RoleActionTypes.RoleOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.auth.createRole(payload.role).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(!res.id) {
              return new ThrowRoleError({error: res});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireRoleSuccess({action_type: RoleActionTypes.RoleCreated}));
              return new RoleCreated({role: res})
            }
          })
        );
      }),
    );

  @Effect()
  init$: Observable<Action> = defer(() => {
    const  allRole = localStorage.getItem('allRole');
    if (allRole){
      return of(new AllRolesLoaded({
        roles: JSON.parse(allRole)
      }))
    }
    return of(new AllRolesRequested());
  });

  constructor(private actions$: Actions, private auth: AuthService, private store: Store<AppState>) {
  }
}
