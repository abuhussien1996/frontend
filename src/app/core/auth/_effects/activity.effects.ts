// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap } from 'rxjs/operators';
import { Observable, defer, of, forkJoin, from } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { AuthService } from '../../../core/auth/_services';
import { ActivityService } from '../../../core/auth/_services/activity.service'
import { UserService } from '../../../core/auth/_services/user.service'
// State
import { AppState } from '../../../core/reducers';
import {
  ActivityPageRequested, ActivityPageToggleLoading, ActivityActionToggleLoading, ActivityPageLoaded
} from '../_actions/activity.actions';
import { ActivityActionTypes } from '../_actions/activity.actions';

@Injectable()
export class ActivityEffects {
  showPageLoadingDistpatcher = new ActivityPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new ActivityPageToggleLoading({ isLoading: false });

  showActionLoadingDistpatcher = new ActivityActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new ActivityActionToggleLoading({ isLoading: false });


  // done
  @Effect()
  loadActivitysPage$ = this.actions$
    .pipe(
      ofType<ActivityPageRequested>(ActivityActionTypes.ActivityPageRequested),
      mergeMap(({ payload }) => {

        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.activity.getAllActivity(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery);
      }),
      map(response => {

        const result: QueryResultsModel = response[0];
        const lastQuery: QueryParamsModel = response[1];
        return new ActivityPageLoaded({
          activities: result.pageOfItems,
          totalCount: result.pager.totalCount,
          page: lastQuery
        });
      }),
    );



  constructor(private actions$: Actions, private activity: ActivityService, private user: UserService, private store: Store<AppState>) {
  }
}
