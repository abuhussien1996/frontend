// Angular
import {Injectable} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
// RxJS
import {filter, map, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {defer, Observable, of} from 'rxjs';
// NGRX
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, select, Store} from '@ngrx/store';
// Auth actions
import {
  AuthActionToggleLoading,
  AuthActionTypes,
  AuthPageToggleLoading,
  Login,
  Logout,
  Register,
  RequestPassword,
  UserLoaded,
  UserRequested
} from '../_actions/auth.actions';
import {AuthService} from '../_services/index';
import {AppState} from '../../reducers';
import {environment} from '../../../../environments/environment';
import {isUserLoaded} from '../_selectors/auth.selectors';
import {AuthNoticeService} from '../auth-notice/auth-notice.service';
import {TranslateService} from '@ngx-translate/core';
import {AllRolesRequested} from '../_actions/role.actions';
import {AllPermissionsRequested} from '../_actions/permission.actions';
import {GlobalSettingsService} from '../../services/global-settings.service';

// new edit on function login
@Injectable()
export class AuthEffects {
  showPageLoadingDistpatcher = new AuthPageToggleLoading({isLoading: true});
  hidePageLoadingDistpatcher = new AuthPageToggleLoading({isLoading: false});

  showActionLoadingDistpatcher = new AuthActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new AuthActionToggleLoading({isLoading: false});

  @Effect({dispatch: false})
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.Login),
    tap(action => {
      localStorage.setItem(environment.authTokenKey, action.payload.authToken);
      this.store.dispatch(new UserRequested());
    }),
  );


  @Effect({dispatch: false})
  logout$ = this.actions$.pipe(
    ofType<Logout>(AuthActionTypes.Logout),
    tap((payload: any) => {
      localStorage.clear();
      this.authService.setRedirectUrl(payload.saveURL ? this.router.url : '');
      this.authService.setUser(null);
      this.router.navigate(['/auth/login']);
      this.store.dispatch(new AllRolesRequested());
      this.store.dispatch(new AllPermissionsRequested());
    })
  );

  // Do nothing
  @Effect({dispatch: false})
  register$ = this.actions$.pipe(
    ofType<Register>(AuthActionTypes.Register),
    tap(action => {
      localStorage.setItem(environment.authTokenKey, action.payload.authToken);
    })
  );

  @Effect({dispatch: false})
  loadUser$ = this.actions$
    .pipe(
      ofType<UserRequested>(AuthActionTypes.UserRequested),
      withLatestFrom(this.store.pipe(select(isUserLoaded))),
      filter(([action, _isUserLoaded]) => !_isUserLoaded),
      mergeMap(([action, _isUserLoaded]) => this.auth.getUserByToken()),
      tap(_user => {
        if (_user) {
          this.store.dispatch(new UserLoaded({user: _user}));
        } else {
          this.store.dispatch(new Logout());
        }
      })
    );

  @Effect()
  init$: Observable<Action> = defer(() => {
    const userToken = localStorage.getItem(environment.authTokenKey);
    let observableResult = of({type: 'NO_ACTION'});
    if (userToken) {
      observableResult = of(new Login({authToken: userToken}));
    }
    return observableResult;
  });


  @Effect()
  requestpassword$ = this.actions$
    .pipe(
      ofType<RequestPassword>(AuthActionTypes.RequestPassword),
      mergeMap(({payload}) => {
        console.log(payload);

        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.auth.requestPassword(payload.email).pipe(
          map((res) => {
            console.log(res);
            if (res[`code`] === 404) {
              this.authNoticeService.setNotice(this.translate.instant('AUTH.NOT_FOUND'), 'danger');
            } else {
              this.authNoticeService.setNotice('please check your email', 'success');
              return this.hideActionLoadingDistpatcher;
              // this.router.navigate(['/auth/login']);
            }

            return this.hideActionLoadingDistpatcher;
          }),
        )
      }));
  private returnUrl: string;

  constructor(private actions$: Actions,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService,
              private auth: AuthService,
              private store: Store<AppState>,
              private authNoticeService: AuthNoticeService,
              private translate: TranslateService,
              private globalSettings: GlobalSettingsService,
  ) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.returnUrl = event.url;
      }
    });
  }
}
