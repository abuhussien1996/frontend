// Angular
import {Injectable} from '@angular/core';
// RxJS
import {mergeMap, map, tap, catchError} from 'rxjs/operators';
import {Observable, defer, of, forkJoin} from 'rxjs';
// NGRX
import {Effect, Actions, ofType} from '@ngrx/effects';
import {Store, select, Action} from '@ngrx/store';
// CRUD
import {QueryResultsModel, QueryParamsModel} from '../../_base/crud';
// Services
import {AuthService} from '../../../core/auth/_services';
import {UserService} from '../../../core/auth/_services';
// State
import {AppState} from '../../../core/reducers';
import {
  UserActionTypes,
  UsersPageRequested,
  UsersPageLoaded,
  UserCreated,
  UserDeleted,
  ManyUsersDeleted,
  UserUpdated,
  UserOnServerCreated,
  UsersActionToggleLoading,
  UsersPageToggleLoading,
  UsersStatusUpdated,
  ChangePassword,
  ThrowError,
  ClearErrors,
  UserOnServerUpdated,
  UsersPageCancelled,
  UserOnServerDeleted,
  ManyUsersOnServerDeleted,
  UserStatusOnServerUpdated,
  UserStatusUpdated,
  UsersStatusOnServerUpdated,
  // UserStatusUpdated
} from '../_actions/user.actions';
import {Router} from '@angular/router';
import {CommunicationsService} from '../../timeline/_services';
import { UserLoaded} from '../_actions/auth.actions';

@Injectable()
export class UserEffects {
  showPageLoadingDistpatcher = new UsersPageToggleLoading({isLoading: true});
  hidePageLoadingDistpatcher = new UsersPageToggleLoading({isLoading: false});

  showActionLoadingDistpatcher = new UsersActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new UsersActionToggleLoading({isLoading: false});


  // done
  @Effect()
  loadUsersPage$ = this.actions$
    .pipe(
      ofType<UsersPageRequested>(UserActionTypes.UsersPageRequested),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.user.findUsers(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 403 || response[0].status === 401) {
                this.router.navigateByUrl('/error/403');
                return new UsersPageCancelled();
              } else {
                this.store.dispatch(new ThrowError({error: response[0].error}));
                return new UsersPageCancelled();
              }
            } else {
              const result = response[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery: QueryParamsModel = response[1];
              this.store.dispatch(new ClearErrors());
              return new UsersPageLoaded({
                users: result.pageOfItems,
                totalCount: result.pager.totalCount,
                page: lastQuery
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteUser$ = this.actions$
    .pipe(
      ofType<UserOnServerDeleted>(UserActionTypes.UserOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.user.deleteUser(payload.id, payload.companyID).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if (res.error) {
                return new ThrowError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new ClearErrors());
                return new UserDeleted(payload)
              }
            })
          );
        }
      ),
    );

  @Effect()
  deleteUsers$ = this.actions$
    .pipe(
      ofType<ManyUsersOnServerDeleted>(UserActionTypes.ManyUsersOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.user.deleteUsers(payload.ids).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res) => {
              if (res.error) {
                return new ThrowError({error: res.error});
              } else {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new ClearErrors());
                return new ManyUsersDeleted(payload)
              }
            })
          );
        }
      ),
    );

  // done

  @Effect()
  updateUser$ = this.actions$
    .pipe(
      ofType<UserOnServerUpdated>(UserActionTypes.UserOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.user.updateUser(payload.user).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new ClearErrors());
              return new UserUpdated(payload)
            }
          })
        );
      }),
    );


  @Effect()
  updateUserStatus$ = this.actions$
    .pipe(
      ofType<UserStatusOnServerUpdated>(UserActionTypes.UserStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.user.updatStatusSingle(payload.id, payload.isActive).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new ClearErrors());
              return new UserStatusUpdated(payload)
            }
          })
        );
      }),
    );


  @Effect()
  createUser$ = this.actions$
    .pipe(
      ofType<UserOnServerCreated>(UserActionTypes.UserOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.user.createUser(payload.user).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            console.log(res);
            if(res.profileUrl){
              this.communicationsService.uploadProfilePic(res.profileUrl, payload.file[0]).subscribe();
              const request = {
                id: res.userID,
                updateProfile: false,
                profilePath: res.uuid,
                email: payload.user.email,
                userName: payload.user.userName,
                firstName: payload.user.firstName,
                lastName: payload.user.lastName
              }
              this.user.changeProfilePic(request).subscribe()
                }
            if (!res.userID) {
              return new ThrowError({error: res});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new ClearErrors());
              return new UserCreated({user: res})
            }
          })
        )
      }),
    );

  @Effect()
  UpdatedManyUsersStatus$ = this.actions$
    .pipe(
      ofType<UsersStatusOnServerUpdated>(UserActionTypes.UsersStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.user.updateStatusForUsers(payload.users, payload.isActive).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new ClearErrors());
              return new UsersStatusUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  init$: Observable<Action> = defer(() => {
    const user = localStorage.getItem('currentUser');
    let observableResult = of({ type: 'NO_ACTION' });
    if (user) {
      observableResult = of(new UserLoaded({ user: JSON.parse(user) }));
    }
    return observableResult;
  });


  constructor(
    private actions$: Actions,
    private auth: AuthService,
    private user: UserService,
    private store: Store<AppState>,
    private router: Router,
    private communicationsService: CommunicationsService,

  ) {
  }
}
