// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { ActivityState } from '../_reducers/activity.reducers';
import { each } from 'lodash';
import { Activity } from '../_models/activity.model';
import { ActivityService } from '../../../core/auth/_services/activity.service'
export const selectActivityState = createFeatureSelector<ActivityState>('activities');

export const selectActivityById = (activityId: number) => createSelector(
    selectActivityState,
    activityState => activityState.entities[activityId]
);

export const selectActivityPageLoading = createSelector(
    selectActivityState,
    activityState => {
        return activityState.listLoading;
    }
);

export const selectActivityActionLoading = createSelector(
    selectActivityState,
    activityState => activityState.actionsloading
);

export const selectLastCreatedActivityId = createSelector(
    selectActivityState,
    activityState => activityState.lastCreatedActivityId
);

export const selectActivityPageLastQuery = createSelector(
    selectActivityState,
    activityState => activityState.lastQuery
);

export const selectActivityInStore = createSelector(
    selectActivityState,
    activityState => {
        const items: Activity[] = [];
        each(activityState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Activity[] = httpExtension.sortArray(items, activityState.lastQuery.sortField, activityState.lastQuery.sortOrder);
        return new QueryResultsModel(result, activityState.totalCount);
    }
);

export const selectActivityShowInitWaitingMessage = createSelector(
    selectActivityState,
    activityState => activityState.showInitWaitingMessage
);

export const selectHasActivityInStore = createSelector(
    selectActivityState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
