import { BaseModel } from '../../_base/crud';

export class AttachmentModel extends BaseModel {
  id: number;
  url: string;
  uuid: string;

  clear() {
    this.id = 0;
    this.url = '';
    this.uuid = '';
  }
}
