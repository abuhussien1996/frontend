import { BaseModel } from '../../_base/crud';

export class Category extends BaseModel {
  id: number;
  company_id: number;
  name: string;
  roles:any;
  inbox_activated:boolean;
  fax_activated:boolean;
}
