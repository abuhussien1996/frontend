import {BaseModel} from '../../_base/crud';

export class ContactModel extends BaseModel {
  id: number;
  contact_id: number;
  company_id: number;
  entity_type: string;
  entity_id: any; // null
  pms_contact_id: number;
  first_name: string;
  last_name: string;
  address_1: string;
  address_2: string; // null
  city: string;
  state: string;
  zip: string;
  location_id: number;
  home_phone: string; // null
  cell_phone: string;
  fax: string;
  email: string;
  note: string; // null
  processed: boolean;
  form: any; // null
  form_data: any; // null
  is_active: boolean;
  link
  last_name2: any;
  urbanization: any
  dob:string;
  pin_number:number;
  entity_types:string;
  clear() {
    this.urbanization = null;
    this.id = 0;
    this.contact_id = 0;
    this.company_id = 0;
    this.entity_type = '';
    this.entity_id = null;
    this.pms_contact_id = 0;
    this.first_name = '';
    this.last_name = '';
    this.address_1 = '';
    this.address_2 = null
    this.city = '';
    this.state = '';
    this.zip = '';
    this.location_id = 0;
    this.home_phone = null;
    this.cell_phone = null;
    this.fax = '';
    this.dob = '';
    this.email = '';
    this.note = null;
    this.processed = false;
    this.form = null;
    this.form_data = null;
    this.is_active = true;
    this.pin_number = null;
    this.entity_types = null;
  }
  changeActiveStatus() {
    this.is_active = !this.is_active;
  }
}
