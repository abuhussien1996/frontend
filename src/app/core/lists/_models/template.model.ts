import { BaseModel } from '../../_base/crud';

export class Template extends BaseModel {
  id: number;
  name: string;
  category_id:any;
  template:any
  all_roles: boolean;
}
