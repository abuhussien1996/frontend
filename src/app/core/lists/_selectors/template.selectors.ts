// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { TemplatesState } from '../_reducers/template.reducers';
import { each } from 'lodash';
import { Template } from '../_models/template.model';

export const selectTemplatesState = createFeatureSelector<TemplatesState>('templates');

export const selectTemplateById = (templateId: number) => createSelector(
  selectTemplatesState,
  templatesState => templatesState.entities[templateId]
);

export const selectTemplatesPageLoading = createSelector(
  selectTemplatesState,
  templatesState => {
    return templatesState.listLoading;
  }
);

export const selectTemplatesActionLoading = createSelector(
  selectTemplatesState,
  templatesState => templatesState.actionsloading
);

export const selectLastCreatedTemplateId = createSelector(
  selectTemplatesState,
  templatesState => templatesState.lastCreatedTemplateId
);

export const selectLastCreatedTemplate = createSelector(
  selectTemplatesState,
  templatesState => templatesState.lastCreatedTemplate
);

export const selectTemplatesPageLastQuery = createSelector(
  selectTemplatesState,
  templatesState => templatesState.lastQuery
);
export const selectTemplateError = createSelector(
  selectTemplatesState,
  templatesState =>  templatesState.error
);
export const selectTemplateLatestSuccessfullAction = createSelector(
  selectTemplatesState,
  templatesState =>  templatesState.latestSuccessfullAction
);
export const selectTemplatesInStore = createSelector(
  selectTemplatesState,
  templatesState => {
    const items: Template[] = [];
    each(templatesState.ids, ele => {
      const index :number = +ele
      items.push(templatesState.entities[index]);
    });
    return new QueryResultsModel(items, templatesState.totalCount);
  }
);

export const selectTemplatesShowInitWaitingMessage = createSelector(
  selectTemplatesState,
  templatesState => templatesState.showInitWaitingMessage
);

export const selectHasTemplatesInStore = createSelector(
  selectTemplatesState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }

    return true;
  }
);
