// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { ContactsState } from '../_reducers/contact.reducers';
import { ContactModel } from '../_models/contact.model';

export const selectContactsState = createFeatureSelector<ContactsState>('contacts');

export const selectContactById = (contactId: number) => createSelector(
    selectContactsState,
    contactsState => contactsState.entities[contactId]
);

export const selectContactsPageLoading = createSelector(
    selectContactsState,
    contactsState => contactsState.listLoading
);

export const selectContactsActionLoading = createSelector(
    selectContactsState,
    contactsState => contactsState.actionsloading
);

export const selectLastCreatedContactId = createSelector(
    selectContactsState,
    contactsState => contactsState.lastCreatedContactId
);

export const selectLastCreatedContact = createSelector(
    selectContactsState,
    contactsState => contactsState.lastCreatedContact
);

export const selectContactErrorState = createSelector(
    selectContactsState,
    contactsState => contactsState.error && contactsState.error.error ? contactsState.error.error : contactsState.error
);

export const selectContactLatestSuccessfullAction = createSelector(
    selectContactsState,
    contactsState =>  contactsState.latestSuccessfullAction
);

export const selectContactsShowInitWaitingMessage = createSelector(
    selectContactsState,
    contactsState => contactsState.showInitWaitingMessage
);

export const selectContactsPageLastQuery = createSelector(
    selectContactsState,
    contactsState => contactsState.lastQuery
);

export const selectContactsInStore = createSelector(
    selectContactsState,
    contactsState => {
      const items: ContactModel[] = [];
      each(contactsState.ids, ele => {
        const index :number = +ele
        items.push(contactsState.entities[index]);
      });
      return new QueryResultsModel(items, contactsState.totalCount);
    }
);
