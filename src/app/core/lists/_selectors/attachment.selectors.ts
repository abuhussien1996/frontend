// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { AttachmentsState } from '../_reducers/attachment.reducers';
import { AttachmentModel } from '../_models/attachment.model';

export const selectAttachmentsState = createFeatureSelector<AttachmentsState>('attachments');

export const selectAttachmentById = (attachmentId: number) => createSelector(
    selectAttachmentsState,
    attachmentsState => attachmentsState.entities[attachmentId]
);

export const selectAttachmentsPageLoading = createSelector(
    selectAttachmentsState,
    attachmentsState => attachmentsState.listLoading
);

export const selectAttachmentsActionLoading = createSelector(
    selectAttachmentsState,
    attachmentsState => attachmentsState.actionsloading
);

export const selectLastCreatedAttachmentId = createSelector(
    selectAttachmentsState,
    attachmentsState => attachmentsState.lastCreatedAttachmentId
);

export const selectAttachmentErrorState = createSelector(
    selectAttachmentsState,
    attachmentsState => attachmentsState.error
);

export const selectAttachmentsShowInitWaitingMessage = createSelector(
    selectAttachmentsState,
    attachmentsState => attachmentsState.showInitWaitingMessage
);

export const selectAttachmentsInStore = createSelector(
    selectAttachmentsState,
    attachmentsState => {
      const items: AttachmentModel[] = [];
      each(attachmentsState.entities, element => {
        items.push(element);
      });
      const httpExtension = new HttpExtenstionsModel();
      const result: AttachmentModel[] = items;
      return new QueryResultsModel(result, attachmentsState.totalCount);
    }
);
