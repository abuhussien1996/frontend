// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { CategoriesState } from '../_reducers/category.reducers';
import { each } from 'lodash';
import { Category } from '../_models/category.model';

export const selectCategoriesState = createFeatureSelector<CategoriesState>('categories');

export const selectCategoryById = (categoryId: number) => createSelector(
  selectCategoriesState,
  categoriesState => categoriesState.entities[categoryId]
);

export const selectCategoriesPageLoading = createSelector(
  selectCategoriesState,
  categoriesState => {
    return categoriesState.listLoading;
  }
);

export const selectCategoriesShowInitWaitingMessage = createSelector(
  selectCategoriesState,
  categoriesState => categoriesState.showInitWaitingMessage
);

export const selectCategoryError = createSelector(
  selectCategoriesState,
  categoriesState =>  categoriesState.error
);
export const selectCategoryLatestSuccessfullAction = createSelector(
  selectCategoriesState,
  categoriesState =>  categoriesState.latestSuccessfullAction
);
export const selectCategoriesInStore = createSelector(
  selectCategoriesState,
  categoriesState => {
    const items: Category[] = [];
    each(categoriesState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: Category[] = httpExtension.sortArray(items, categoriesState.lastQuery.sortField, categoriesState.lastQuery.sortOrder);
    return new QueryResultsModel(result, categoriesState.totalCount);
  }
);

export const selectHasCategoriesInStore = createSelector(
  selectCategoriesState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }

    return true;
  }
);
