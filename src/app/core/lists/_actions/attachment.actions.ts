// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { AttachmentModel } from '../_models/attachment.model';

export enum AttachmentActionTypes {
  AttachmentOnServerCreated = '[Edit Attachment Dialog] Attachment On Server Created',
  AttachmentCreated = '[Edit Attachment Dialog] Attachment Created',
  AttachmentOnServerUpdated = '[Edit Attachment Dialog] Attachment On Server Updated',
  AttachmentUpdated = '[Edit Attachment Dialog] Attachment Updated',
  AttachmentStatusOnServerUpdated = '[Attachment List Page] Attachment Status On Server Updated',
  AttachmentStatusUpdated = '[Attachment List Page] Attachment Status Updated',
  AttachmentsStatusOnServerUpdated = '[Attachment List Page] Attachments Status On Server Updated',
  AttachmentsStatusUpdated = '[Attachment List Page] Attachments Status Updated',
  OneAttachmentOnServerDeleted = '[Attachments List Page] One Attachment On Server Deleted',
  OneAttachmentDeleted = '[Attachments List Page] One Attachment Deleted',
  ManyAttachmentsOnServerDeleted = '[Attachments List Page] Many Attachments On Server Deleted',
  ManyAttachmentsDeleted = '[Attachments List Page] Many Attachments Deleted',
  AttachmentsPageRequested = '[Attachments List Page] Attachments Page Requested',
  AttachmentsPageLoaded = '[Attachments API] Attachments Page Loaded',
  AttachmentsPageCancelled = '[Attachments API] Attachments Page Cancelled',
  AttachmentsPageToggleLoading = '[Attachments] Attachments Page Toggle Loading',
  AttachmentActionToggleLoading = '[Attachments] Attachments Action Toggle Loading',
  ThrowAttachmentError = '[Attachments] Throw Attachment Error',
  ClearAttachmentErrors = '[Attachments] Clear Attachment Errors',
  ClearAttachment = '[Attachments] Clear Attachment '
}

export class AttachmentOnServerCreated implements Action {
  readonly type = AttachmentActionTypes.AttachmentOnServerCreated;
  constructor(public payload: { attachment: AttachmentModel }) {
  }
}

export class AttachmentCreated implements Action {
  readonly type = AttachmentActionTypes.AttachmentCreated;

  constructor(public payload: { attachment: AttachmentModel }) {
  }
}

export class AttachmentOnServerUpdated implements Action {
  readonly type = AttachmentActionTypes.AttachmentOnServerUpdated;

  constructor(public payload: {
    partialAttachment: Update<AttachmentModel>, // For State update
    attachment: AttachmentModel // For Server update (through service)
  }) {
  }
}

export class AttachmentUpdated implements Action {
  readonly type = AttachmentActionTypes.AttachmentUpdated;

  constructor(public payload: {
    partialAttachment: Update<AttachmentModel>, // For State update
    attachment: AttachmentModel // For Server update (through service)
  }) {
  }
}

export class AttachmentStatusOnServerUpdated implements Action {
  readonly type = AttachmentActionTypes.AttachmentStatusOnServerUpdated;

  constructor(public payload: {
    attachment: AttachmentModel,
    id: number,
    is_active: boolean
  }) {
  }
}

export class AttachmentStatusUpdated implements Action {
  readonly type = AttachmentActionTypes.AttachmentStatusUpdated;

  constructor(public payload: {
    attachment: AttachmentModel,
    id: number,
    is_active: boolean
  }) {
  }
}

export class AttachmentsStatusOnServerUpdated implements Action {
  readonly type = AttachmentActionTypes.AttachmentsStatusOnServerUpdated;

  constructor(public payload: {
    attachments: AttachmentModel[],
    ids: number[],
    is_active: boolean
  }) {
  }
}

export class AttachmentsStatusUpdated implements Action {
  readonly type = AttachmentActionTypes.AttachmentsStatusUpdated;

  constructor(public payload: {
    attachments: AttachmentModel[],
    ids: number[],
    is_active: boolean
  }) {
  }
}

export class OneAttachmentOnServerDeleted implements Action {
  readonly type = AttachmentActionTypes.OneAttachmentOnServerDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class OneAttachmentDeleted implements Action {
  readonly type = AttachmentActionTypes.OneAttachmentDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class ManyAttachmentsOnServerDeleted implements Action {
  readonly type = AttachmentActionTypes.ManyAttachmentsOnServerDeleted;

  constructor(public payload: { ids: number[] }) {
  }
}

export class ManyAttachmentsDeleted implements Action {
  readonly type = AttachmentActionTypes.ManyAttachmentsDeleted;

  constructor(public payload: { ids: number[] }) {
  }
}

export class AttachmentsPageRequested implements Action {
  readonly type = AttachmentActionTypes.AttachmentsPageRequested;

  constructor(public payload: { contact_id: number, company_id: number, communication_id: number, received_id: number, type: string }) {
  }
}

export class AttachmentsPageLoaded implements Action {
  readonly type = AttachmentActionTypes.AttachmentsPageLoaded;

  constructor(public payload: { attachments: AttachmentModel[], totalCount: number, page: QueryParamsModel }) {
  }
}

export class AttachmentsPageCancelled implements Action {
  readonly type = AttachmentActionTypes.AttachmentsPageCancelled;
}

export class AttachmentsPageToggleLoading implements Action {
  readonly type = AttachmentActionTypes.AttachmentsPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class AttachmentActionToggleLoading implements Action {
  readonly type = AttachmentActionTypes.AttachmentActionToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class ThrowAttachmentError implements Action {
  readonly type = AttachmentActionTypes.ThrowAttachmentError;

  constructor(public payload: { error: any }) {
  }
}

export class ClearAttachmentErrors implements Action {
  readonly type = AttachmentActionTypes.ClearAttachmentErrors;
}

export class ClearAttachment implements Action {
  readonly type = AttachmentActionTypes.ClearAttachment;
}

export type AttachmentActions = AttachmentOnServerCreated
| AttachmentCreated
| AttachmentOnServerUpdated
| AttachmentUpdated
| AttachmentStatusOnServerUpdated
| AttachmentStatusUpdated
| AttachmentsStatusOnServerUpdated
| AttachmentsStatusUpdated
| OneAttachmentOnServerDeleted
| OneAttachmentDeleted
| ManyAttachmentsOnServerDeleted
| ManyAttachmentsDeleted
| AttachmentsPageRequested
| AttachmentsPageLoaded
| AttachmentsPageCancelled
| AttachmentsPageToggleLoading
| AttachmentActionToggleLoading
| ThrowAttachmentError
| ClearAttachmentErrors
| ClearAttachment;
