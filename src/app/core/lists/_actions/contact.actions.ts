// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { ContactModel } from '../_models/contact.model';

export enum ContactActionTypes {
  ContactOnServerCreated = '[Edit Contact Dialog] Contact On Server Created',
  ContactCreated = '[Edit Contact Dialog] Contact Created',
  ContactOnServerUpdated = '[Edit Contact Dialog] Contact On Server Updated',
  ContactUpdated = '[Edit Contact Dialog] Contact Updated',
  ContactStatusOnServerUpdated = '[Contact List Page] Contact Status On Server Updated',
  ContactStatusUpdated = '[Contact List Page] Contact Status Updated',
  ContactsStatusOnServerUpdated = '[Contact List Page] Contacts Status On Server Updated',
  ContactsStatusUpdated = '[Contact List Page] Contacts Status Updated',
  OneContactOnServerDeleted = '[Contacts List Page] One Contact On Server Deleted',
  OneContactDeleted = '[Contacts List Page] One Contact Deleted',
  ManyContactsOnServerDeleted = '[Contacts List Page] Many Contacts On Server Deleted',
  ManyContactsDeleted = '[Contacts List Page] Many Contacts Deleted',
  ContactsPageRequested = '[Contacts List Page] Contacts Page Requested',
  ContactsPageLoaded = '[Contacts API] Contacts Page Loaded',
  ContactsPageCancelled = '[Contacts API] Contacts Page Cancelled',
  ContactsPageToggleLoading = '[Contacts] Contacts Page Toggle Loading',
  ContactActionToggleLoading = '[Contacts] Contacts Action Toggle Loading',
  ThrowContactError = '[Contacts] Throw Contact Error',
  ClearContactErrors = '[Contacts] Clear Contact Errors',
  FireContactSuccess = '[Contacts] Fire Success'
}

export class ContactOnServerCreated implements Action {
  readonly type = ContactActionTypes.ContactOnServerCreated;
  constructor(public payload: { contact: ContactModel }) {
  }
}

export class ContactCreated implements Action {
  readonly type = ContactActionTypes.ContactCreated;

  constructor(public payload: { contact: ContactModel }) {
  }
}

export class ContactOnServerUpdated implements Action {
  readonly type = ContactActionTypes.ContactOnServerUpdated;

  constructor(public payload: {
    partialContact: Update<ContactModel>, // For State update
    contact: ContactModel // For Server update (through service)
  }) {
  }
}

export class ContactUpdated implements Action {
  readonly type = ContactActionTypes.ContactUpdated;

  constructor(public payload: {
    partialContact: Update<ContactModel>, // For State update
    contact: ContactModel // For Server update (through service)
  }) {
  }
}

export class ContactStatusOnServerUpdated implements Action {
  readonly type = ContactActionTypes.ContactStatusOnServerUpdated;

  constructor(public payload: {
    contact: ContactModel,
    id: number,
    is_active: boolean
  }) {
  }
}

export class ContactStatusUpdated implements Action {
  readonly type = ContactActionTypes.ContactStatusUpdated;

  constructor(public payload: {
    contact: ContactModel,
    id: number,
    is_active: boolean
  }) {
  }
}

export class ContactsStatusOnServerUpdated implements Action {
  readonly type = ContactActionTypes.ContactsStatusOnServerUpdated;

  constructor(public payload: {
    contacts: ContactModel[],
    ids: number[],
    is_active: boolean
  }) {
  }
}

export class ContactsStatusUpdated implements Action {
  readonly type = ContactActionTypes.ContactsStatusUpdated;

  constructor(public payload: {
    contacts: ContactModel[],
    ids: number[],
    is_active: boolean
  }) {
  }
}

export class OneContactOnServerDeleted implements Action {
  readonly type = ContactActionTypes.OneContactOnServerDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class OneContactDeleted implements Action {
  readonly type = ContactActionTypes.OneContactDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class ManyContactsOnServerDeleted implements Action {
  readonly type = ContactActionTypes.ManyContactsOnServerDeleted;

  constructor(public payload: { ids: number[] }) {
  }
}

export class ManyContactsDeleted implements Action {
  readonly type = ContactActionTypes.ManyContactsDeleted;

  constructor(public payload: { ids: number[] }) {
  }
}

export class ContactsPageRequested implements Action {
  readonly type = ContactActionTypes.ContactsPageRequested;

  constructor(public payload: { page: QueryParamsModel }) {
  }
}

export class ContactsPageLoaded implements Action {
  readonly type = ContactActionTypes.ContactsPageLoaded;

  constructor(public payload: { contacts: ContactModel[], totalCount: number, page: QueryParamsModel }) {
  }
}

export class ContactsPageCancelled implements Action {
  readonly type = ContactActionTypes.ContactsPageCancelled;
}

export class ContactsPageToggleLoading implements Action {
  readonly type = ContactActionTypes.ContactsPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class ContactActionToggleLoading implements Action {
  readonly type = ContactActionTypes.ContactActionToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class ThrowContactError implements Action {
  readonly type = ContactActionTypes.ThrowContactError;
  constructor(public payload: { error: any }) {
  }
}

export class ClearContactErrors implements Action {
  readonly type = ContactActionTypes.ClearContactErrors;
}

export class FireContactSuccess implements Action {
  readonly type = ContactActionTypes.FireContactSuccess;
  constructor(public payload: {action_type: string}) {}
}

export type ContactActions = ContactOnServerCreated
| ContactCreated
| ContactOnServerUpdated
| ContactUpdated
| ContactStatusOnServerUpdated
| ContactStatusUpdated
| ContactsStatusOnServerUpdated
| ContactsStatusUpdated
| OneContactOnServerDeleted
| OneContactDeleted
| ManyContactsOnServerDeleted
| ManyContactsDeleted
| ContactsPageRequested
| ContactsPageLoaded
| ContactsPageCancelled
| ContactsPageToggleLoading
| ContactActionToggleLoading
| ThrowContactError
| ClearContactErrors
| FireContactSuccess;
