import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Template } from '../_models/template.model';
import { QueryParamsModel } from '../../_base/crud';

export enum TemplateActionTypes {
  AllTemplatesRequested = '[Templates] All Templates Requested',
  AllTemplatesLoaded = '[Templates] All Templates Loaded',
  TemplateOnServerCreated = '[Templates] Template On Server Created',
  TemplateCreated = '[Templates] Template Created',
  TemplateOnServerUpdated = '[Templates] Template On Server Updated',
  TemplateUpdated = '[Templates] Template Updated',
  TemplateOnServerDeleted = '[Templates] Template On Server Deleted',
  TemplateDeleted = '[Templates] Template Deleted',
  TemplatesPageRequested = '[Templates] Templates Page Requested',
  TemplatesPageLoaded = '[Templates] Templates Page Loaded',
  TemplatesPageCancelled = '[Templates] Templates Page Cancelled',
  TemplatesPageToggleLoading = '[Templates] Templates Page Toggle Loading',
  TemplatesActionToggleLoading = '[Templates] Templates Action Toggle Loading',
  ThrowTemplateError = '[Templates] Throw Error',
  FireTemplateSuccess = '[Templates] Fire Success',
  ClearTemplateState = '[Templates] Clear Template Insurance State'
}

export class TemplateOnServerCreated implements Action {
  readonly type = TemplateActionTypes.TemplateOnServerCreated;
  constructor(public payload: { template: Template }) { }
}

export class TemplateCreated implements Action {
  readonly type = TemplateActionTypes.TemplateCreated;
  constructor(public payload: { template: Template }) { }
}

export class ThrowTemplateError implements Action {
  readonly type = TemplateActionTypes.ThrowTemplateError;
  constructor(public payload: { error: any }) { }
}

export class FireTemplateSuccess implements Action {
  readonly type = TemplateActionTypes.FireTemplateSuccess;
  constructor(public payload: {action_type: string}) {}
}

export class TemplateOnServerUpdated implements Action {
  readonly type = TemplateActionTypes.TemplateOnServerUpdated;
  constructor(public payload: {
    partialTemplate: Update<Template>,
    template: Template
  }) { }
}
export class TemplateUpdated implements Action {
  readonly type = TemplateActionTypes.TemplateUpdated;
  constructor(public payload: {
    partialTemplate: Update<Template>,
    template: Template
  }) { }
}

export class TemplateOnServerDeleted implements Action {
  readonly type = TemplateActionTypes.TemplateOnServerDeleted;
  constructor(public payload: { id: number }) { }
}

export class TemplateDeleted implements Action {
  readonly type = TemplateActionTypes.TemplateDeleted;
  constructor(public payload: { id: number }) { }
}

export class TemplatesPageRequested implements Action {
  readonly type = TemplateActionTypes.TemplatesPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {
  }
}

export class TemplatesPageLoaded implements Action {
  readonly type = TemplateActionTypes.TemplatesPageLoaded;
  constructor(public payload: { templates: Template[], totalCount: number, page: QueryParamsModel }) { }
}

export class TemplatesPageCancelled implements Action {
  readonly type = TemplateActionTypes.TemplatesPageCancelled;
}

export class TemplatesPageToggleLoading implements Action {
  readonly type = TemplateActionTypes.TemplatesPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) { }
}

export class TemplatesActionToggleLoading implements Action {
  readonly type = TemplateActionTypes.TemplatesActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) { }
}

export class ClearTemplateState implements Action {
  readonly type = TemplateActionTypes.ClearTemplateState
}

export type TemplateActions = TemplateCreated
  | TemplateUpdated
  | TemplateDeleted
  | TemplateOnServerCreated
  | TemplateOnServerUpdated
  | TemplateOnServerDeleted
  | TemplatesPageLoaded
  | TemplatesPageCancelled
  | TemplatesPageToggleLoading
  | TemplatesPageRequested
  | ThrowTemplateError
  | FireTemplateSuccess
  | TemplatesActionToggleLoading
  | ClearTemplateState;
