// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Category } from '../_models/category.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum CategoryActionTypes {
  CategoriesPageRequested = '[Categories] Categories Page Requested',
  CategoriesPageLoaded = '[Categories] Categories Page Loaded',
  CategoriesPageCancelled = '[Categories] Categories Page Cancelled',
  CategoryOnServerCreated = '[Categories] Category On Server Created',
  CategoryCreated = '[Categories] Category Created',
  CategoryOnServerUpdated = '[Categories] Category On Server Updated',
  CategoryUpdated = '[Categories] Category Updated',
  CategoryOnServerDeleted = '[Categories] Category On Server Deleted',
  CategoryDeleted = '[Categories] Category Deleted',
  ThrowCategoryError = '[Categories] Throw Error',
  FireCategorySuccess = '[Categories] Fire Success',
  CategoriesPageToggleLoading = '[Categories] Categories Page Toggle Loading'
}

export class CategoryOnServerCreated implements Action {
  readonly type = CategoryActionTypes.CategoryOnServerCreated;
  constructor(public payload: { body: any }) { }
}

export class ThrowCategoryError implements Action {
  readonly type = CategoryActionTypes.ThrowCategoryError;
  constructor(public payload: { error: any }) { }
}

export class FireCategorySuccess implements Action {
  readonly type = CategoryActionTypes.FireCategorySuccess;
  constructor(public payload: {action_type: string}) {}
}

export class CategoryOnServerUpdated implements Action {
  readonly type = CategoryActionTypes.CategoryOnServerUpdated;
  constructor(public payload: {
    partialCategory: Update<Category>,
    category: Category
  }) { }
}

export class CategoryOnServerDeleted implements Action {
  readonly type = CategoryActionTypes.CategoryOnServerDeleted;
  constructor(public payload: { id: number }) { }
}

export class CategoriesPageRequested implements Action {
  readonly type = CategoryActionTypes.CategoriesPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {
  }
}

export class CategoriesPageLoaded implements Action {
  readonly type = CategoryActionTypes.CategoriesPageLoaded;
  constructor(public payload: { categories: Category[], totalCount: number , page: QueryParamsModel }) { }
}

export class CategoriesPageCancelled implements Action {
  readonly type = CategoryActionTypes.CategoriesPageCancelled;
}

export class CategoriesPageToggleLoading implements Action {
  readonly type = CategoryActionTypes.CategoriesPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) { }
}

export class CategoryUpdated implements Action {
  readonly type = CategoryActionTypes.CategoryUpdated;
  constructor(public payload: Update<Category>) { }
}

export type CategoryActions = CategoryOnServerCreated
  | CategoryOnServerUpdated
  | CategoryOnServerDeleted
  | CategoriesPageRequested
  | CategoriesPageLoaded
  | CategoriesPageCancelled
  | CategoriesPageToggleLoading
  | ThrowCategoryError
  | FireCategorySuccess
  |CategoryUpdated;
