export {ContactModel} from './_models/contact.model';
export {AttachmentModel} from './_models/attachment.model';
export {Category} from './_models/category.model';


export {ContactsDataSource} from './_data-sources/contacts.datasource';
export {AttachmentsDataSource} from './_data-sources/attachments.datasource';
export {TemplatesDataSource} from './_data-sources/templates.datasource';
export {CategoriesDataSource} from './_data-sources/categories.datasource';




export {
  ContactActionTypes,
  ContactActions,
  ContactOnServerCreated,
  ContactCreated,
  ContactOnServerUpdated,
  ContactUpdated,
  ContactStatusOnServerUpdated,
  ContactStatusUpdated,
  ContactsStatusOnServerUpdated,
  ContactsStatusUpdated,
  OneContactOnServerDeleted,
  OneContactDeleted,
  ManyContactsOnServerDeleted,
  ManyContactsDeleted,
  ContactsPageRequested,
  ContactsPageLoaded,
  ContactsPageCancelled,
  ContactsPageToggleLoading,
  ThrowContactError,
  ClearContactErrors,
  FireContactSuccess
} from './_actions/contact.actions';


export {
  AttachmentActionTypes,
  AttachmentActions,
  AttachmentOnServerCreated,
  AttachmentCreated,
  AttachmentOnServerUpdated,
  AttachmentUpdated,
  AttachmentStatusOnServerUpdated,
  AttachmentStatusUpdated,
  AttachmentsStatusOnServerUpdated,
  AttachmentsStatusUpdated,
  OneAttachmentOnServerDeleted,
  OneAttachmentDeleted,
  ManyAttachmentsOnServerDeleted,
  ManyAttachmentsDeleted,
  AttachmentsPageRequested,
  AttachmentsPageLoaded,
  AttachmentsPageCancelled,
  AttachmentsPageToggleLoading,
  ThrowAttachmentError,
  ClearAttachmentErrors
} from './_actions/attachment.actions';


// Client Actions =>
export {
  CategoryOnServerCreated,
  CategoryOnServerUpdated,
  CategoryOnServerDeleted,
  CategoriesPageRequested,
  CategoryActionTypes
} from './_actions/category.actions';


// Template Actions =>
export {
  TemplateCreated,
  TemplateUpdated,
  TemplateDeleted,
  TemplateOnServerCreated,
  TemplateOnServerUpdated,
  TemplateOnServerDeleted,
  TemplatesPageLoaded,
  TemplatesPageCancelled,
  TemplatesPageToggleLoading,
  TemplatesPageRequested,
  TemplatesActionToggleLoading,
  ThrowTemplateError,
  FireTemplateSuccess,
  TemplateActionTypes,
  ClearTemplateState
} from './_actions/template.actions';



export {ContactEffects} from './_effects/contact.effects';
export {AttachmentEffects} from './_effects/attachment.effects';
export {TemplateEffects} from './_effects/template.effects';
export {CategoryEffects} from './_effects/category.effects';



// Reducers

export {contactsReducer} from './_reducers/contact.reducers';
export {attachmentsReducer} from './_reducers/attachment.reducers';
export {templatesReducer} from './_reducers/template.reducers';
export {categoriesReducer} from './_reducers/category.reducers';


// Selectors

export {
  selectContactById,
  selectContactsInStore,
  selectContactsPageLoading,
  selectLastCreatedContact,
  selectLastCreatedContactId,
  selectContactsActionLoading,
  selectContactsShowInitWaitingMessage,
  selectContactErrorState,
  selectContactsPageLastQuery,
  selectContactLatestSuccessfullAction
} from './_selectors/contact.selectors';



export {
  selectAttachmentById,
  selectAttachmentsInStore,
  selectAttachmentsPageLoading,
  selectLastCreatedAttachmentId,
  selectAttachmentsActionLoading,
  selectAttachmentsShowInitWaitingMessage,
  selectAttachmentErrorState
} from './_selectors/attachment.selectors';



// Template selectors =>
export {
  selectTemplateById,
  selectTemplatesPageLoading,
  selectLastCreatedTemplate,
  selectLastCreatedTemplateId,
  selectTemplatesInStore,
  selectHasTemplatesInStore,
  selectTemplatesPageLastQuery,
  selectTemplatesActionLoading,
  selectTemplatesShowInitWaitingMessage,
  selectTemplateError,
  selectTemplateLatestSuccessfullAction
} from './_selectors/template.selectors';


// Client selectors =>
export {
  selectCategoryById,
  selectCategoriesInStore,
  selectHasCategoriesInStore,
  selectCategoryError,
  selectCategoryLatestSuccessfullAction
} from './_selectors/category.selectors';

// Services
export {
  ContactsService,
  AttachmentsService,
  SharedHttpRequestService,
  CategoryService
} from './_services/';
