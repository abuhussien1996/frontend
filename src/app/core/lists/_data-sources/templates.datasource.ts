// NGRX
import {Store, select} from '@ngrx/store';
// CRUD
import {BaseDataSource, QueryResultsModel} from '../../_base/crud';
// State
import {AppState} from '../../reducers';
import {selectTemplatesInStore, selectTemplatesPageLoading, selectTemplatesShowInitWaitingMessage} from '../_selectors/template.selectors';


export class TemplatesDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectTemplatesPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectTemplatesShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectTemplatesInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
