import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import { delay, distinctUntilChanged, skip, filter, take, map } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import {
  selectAttachmentsInStore,
  selectAttachmentsPageLoading,
  selectAttachmentsShowInitWaitingMessage
} from '../_selectors/attachment.selectors';

export class AttachmentsDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectAttachmentsPageLoading),
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectAttachmentsShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectAttachmentsInStore),
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
