// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectCategoriesInStore, selectCategoriesPageLoading, selectCategoriesShowInitWaitingMessage } from '../_selectors/category.selectors';


export class CategoriesDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectCategoriesPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectCategoriesShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectCategoriesInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
