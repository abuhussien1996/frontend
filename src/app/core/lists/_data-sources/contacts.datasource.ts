import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import { delay, distinctUntilChanged, skip, filter, take, map } from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import {
  selectContactsInStore,
  selectContactsPageLoading,
  selectContactsShowInitWaitingMessage
} from '../_selectors/contact.selectors';

export class ContactsDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectContactsPageLoading),
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectContactsShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectContactsInStore),
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
