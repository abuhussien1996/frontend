import {BaseDataSource} from '../../_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {
  selectWorkflowGroupInStore,
  selectWorkflowGroupPageLoading,
  selectWorkflowGroupShowInitWaitingMessage
} from '../../events/_selectors/workflow-group.selectors';

export class WorkflowGroupDatasource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectWorkflowGroupPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectWorkflowGroupShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectWorkflowGroupInStore)
    ).subscribe((response) => {
      this.entitySubject.next(response.items);
    });

  }
}
