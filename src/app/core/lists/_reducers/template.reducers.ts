// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { TemplateActions, TemplateActionTypes } from '../_actions/template.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Template } from '../_models/template.model';

// tslint:disable-next-line:no-empty-interface
export interface TemplatesState extends EntityState<Template> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedTemplateId: number;
  lastCreatedTemplate: Template;
  lastQuery: QueryParamsModel;
  error:any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Template> = createEntityAdapter<Template>();

export const initialTemplatesState: TemplatesState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedTemplateId: undefined,
  lastCreatedTemplate: undefined,
  showInitWaitingMessage: true
});

export function templatesReducer(state = initialTemplatesState, action: TemplateActions): TemplatesState {
  switch (action.type) {
    case TemplateActionTypes.TemplatesPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedTemplateId: undefined
      };
    case TemplateActionTypes.TemplatesActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case TemplateActionTypes.TemplateCreated:
      return adapter.addOne(action.payload.template, {
        ...state, lastCreatedTemplateId: action.payload.template.id, lastCreatedTemplate: action.payload.template
      });
    case TemplateActionTypes.TemplateUpdated:
      return adapter.updateOne(action.payload.partialTemplate, state);
    case TemplateActionTypes.TemplateDeleted:
      return adapter.removeOne(action.payload.id, state);
    case TemplateActionTypes.TemplatesPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case TemplateActionTypes.TemplatesPageLoaded: {
      return adapter.addMany(action.payload.templates, {
        ...initialTemplatesState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
        latestSuccessfullAction: state.latestSuccessfullAction,
        error: null
      });
    }
    case TemplateActionTypes.ThrowTemplateError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case TemplateActionTypes.FireTemplateSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    case TemplateActionTypes.ClearTemplateState: {
      return initialTemplatesState;
    }
    default:
      return state;
  }
}

export const getTemplateState = createFeatureSelector<TemplatesState>('templates');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
