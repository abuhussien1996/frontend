// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { AttachmentActions, AttachmentActionTypes } from '../_actions/attachment.actions';
// Models
import { AttachmentModel } from '../_models/attachment.model';
import { QueryParamsModel } from '../../_base/crud';

export interface AttachmentsState extends EntityState<AttachmentModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedAttachmentId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
  error: any|null
}

export const adapter: EntityAdapter<AttachmentModel> = createEntityAdapter<AttachmentModel>();

export const initialAttachmentsState: AttachmentsState = adapter.getInitialState({
  attachmentForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedAttachmentId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
  error: null
});

export function attachmentsReducer(state = initialAttachmentsState, action: AttachmentActions): AttachmentsState {
  switch (action.type) {
    case AttachmentActionTypes.AttachmentsPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedAttachmentId: undefined
      };
    }
    case AttachmentActionTypes.AttachmentActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case AttachmentActionTypes.AttachmentOnServerCreated:
      return {
        ...state
      };
    case AttachmentActionTypes.AttachmentCreated:
      return adapter.addOne(action.payload.attachment, {
        ...state, lastCreatedAttachmentId: action.payload.attachment.id
      });
    case AttachmentActionTypes.AttachmentUpdated:
      return adapter.updateOne(action.payload.partialAttachment, state);
    case AttachmentActionTypes.OneAttachmentDeleted:
      return adapter.removeOne(action.payload.id, state);
    case AttachmentActionTypes.ManyAttachmentsDeleted:
      return adapter.removeMany(action.payload.ids, state);
    case AttachmentActionTypes.AttachmentsPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case AttachmentActionTypes.AttachmentsPageLoaded: {
      return adapter.addMany(action.payload.attachments, {
        ...initialAttachmentsState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    case AttachmentActionTypes.ThrowAttachmentError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case AttachmentActionTypes.ClearAttachmentErrors: {
      return {
        ...state, error: null
      };
    }
    case AttachmentActionTypes.ClearAttachment: {
      return initialAttachmentsState
    }
    default:
      return state;
  }
}

export const getAttachmentState = createFeatureSelector<AttachmentModel>('attachments');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
