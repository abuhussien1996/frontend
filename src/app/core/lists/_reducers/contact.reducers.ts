// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { ContactActions, ContactActionTypes } from '../_actions/contact.actions';
// Models
import { ContactModel } from '../_models/contact.model';
import { QueryParamsModel } from '../../_base/crud';

export interface ContactsState extends EntityState<ContactModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedContactId: number;
  lastCreatedContact: ContactModel;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
  latestSuccessfullAction: any;
  error: any|null
}

export const adapter: EntityAdapter<ContactModel> = createEntityAdapter<ContactModel>();

export const initialContactsState: ContactsState = adapter.getInitialState({
  contactForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedContactId: undefined,
  lastCreatedContact: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
  error: null,
  latestSuccessfullAction: null
});

export function contactsReducer(state = initialContactsState, action: ContactActions): ContactsState {
  switch (action.type) {
    case ContactActionTypes.ContactsPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedContactId: undefined
      };
    }
    case ContactActionTypes.ContactActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case ContactActionTypes.ContactOnServerCreated:
      return {
        ...state
      };
    case ContactActionTypes.ContactCreated:
      return adapter.addOne(action.payload.contact, {
        ...state, lastCreatedContactId: action.payload.contact.id, lastCreatedContact: action.payload.contact
      });
    case ContactActionTypes.ContactUpdated:
      return adapter.updateOne(action.payload.partialContact, state);
    case ContactActionTypes.ContactStatusUpdated: {
      // tslint:disable-next-line
      const _partialContact: Update<ContactModel> = {
        id: action.payload.contact.id,
        changes: {
          is_active: action.payload.is_active
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialContact, state);
    }
    case ContactActionTypes.ContactsStatusUpdated: {
      // tslint:disable-next-line
      const _partialContacts: Update<ContactModel>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.contacts.length; i++) {
        _partialContacts.push({
          id: action.payload.contacts[i].id,
          changes: {
            is_active: action.payload.is_active
          }
        });
      }
      return adapter.updateMany(_partialContacts, state);
    }
    case ContactActionTypes.OneContactDeleted:
      return adapter.removeOne(action.payload.id, state);
    case ContactActionTypes.ManyContactsDeleted:
      return adapter.removeMany(action.payload.ids, state);
    case ContactActionTypes.ContactsPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case ContactActionTypes.ContactsPageLoaded: {
      return adapter.addMany(action.payload.contacts, {
        ...initialContactsState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    case ContactActionTypes.ThrowContactError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case ContactActionTypes.ClearContactErrors: {
      return {
        ...state, error: null
      };
    }
    case ContactActionTypes.FireContactSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getContactState = createFeatureSelector<ContactModel>('contacts');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
