// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { CategoryActions, CategoryActionTypes } from '../_actions/category.actions';
// Models
import { Category } from '../_models/category.model';
import {QueryParamsModel} from '../../_base/crud';

// tslint:disable-next-line:no-empty-interface
export interface CategoriesState extends EntityState<Category> {
  totalCount: number;
  error:any;
  latestSuccessfullAction: any;
  listLoading: boolean;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Category> = createEntityAdapter<Category>();

export const initialCategoriesState: CategoriesState = adapter.getInitialState({
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  listLoading: false,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function categoriesReducer(state = initialCategoriesState, action: CategoryActions): CategoriesState {
  switch (action.type) {
    case CategoryActionTypes.CategoriesPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading
      };
    case CategoryActionTypes.CategoriesPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case CategoryActionTypes.CategoriesPageLoaded: {
      return adapter.addMany(action.payload.categories, {
        ...initialCategoriesState,
        totalCount: action.payload.totalCount,
        showInitWaitingMessage: false,
        lastQuery: action.payload.page,
        latestSuccessfullAction: state.latestSuccessfullAction,
        error: null
      });
    }
    case CategoryActionTypes.ThrowCategoryError: {
      return {
        ...state, error: action.payload.error
      };
    }
    case CategoryActionTypes.FireCategorySuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    case CategoryActionTypes.CategoryUpdated: {
      return adapter.updateOne(action.payload,state)
    }
    default:
      return state;
  }
}

export const getCategoryState = createFeatureSelector<CategoriesState>('categories');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
