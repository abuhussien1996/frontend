import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpUtilsService} from '../../_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth';

@Injectable({
  providedIn: 'root'
})
export class SharedHttpRequestService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

}
