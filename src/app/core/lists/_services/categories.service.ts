import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {Category} from '../_models/category.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from 'src/environments/environment';

@Injectable()
export class CategoryService {
  private authToken;
  private company_id;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.company_id = res.companyID);
  }

  createCategory(body: any) {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<Category>(url , body, {headers});
  }

  findCategories(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category`;
    const pageSize = queryParams.pageSize;
    const pageIndex = queryParams.pageNumber;
    const searchData = queryParams.filter.searchData;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    const params = new HttpParams({fromString: `page=${pageIndex}&size=${pageSize}&search_term=${searchData}`+'&sort='+`${queryParams.sortField},${queryParams.sortOrder}`});
    return this.http.get<QueryResultsModel>(url , {headers, params});
  }

  getCategories() {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: `size=500`})
    return this.http.get<any>(url , {headers,params});
  }

  updateCategory(_category: Category): Observable<any> {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category/${_category.id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      name: _category.name,
      roles: _category.roles
    }
    return this.http.put<Category>(url , body, {headers});
  }

  deleteCategory(categoryId: number): Observable<any> {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category/${categoryId}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url , {headers});
  }

  searchCategories(term: string,template): Observable<any> {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category/autocomplete`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    let params = new HttpParams();
    params = params.append('term', term);
    if (template){
      params = params.append('has_template', 'true');
    }
    return this.http.get<QueryResultsModel>(url , {headers, params});
  }

  getAllCategories(term: string,): Observable<any> {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category/autocomplete`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    let params = new HttpParams();
    params = params.append('term', term);
    params = params.append('all_categories', 'true');
    return this.http.get<QueryResultsModel>(url , {headers, params});
  }

  searchCategoriesWithTemplate(term: string): Observable<any> {
    return this.searchCategories(term,true);
  }

  getCategoriesByWorkflow(company,term: string): Observable<any> {
    const url = `${environment.USER_BASE}/companies/${company}/category/autocomplete`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'term=' + term});
    return this.http.get<QueryResultsModel>(url , {headers, params});
  }

  getCategoryById(id) {
    const url = `${environment.USER_BASE}/companies/${this.company_id}/category/${id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url , {headers});
  }
  getCategoryForWorkflowById(id,company) {
    const url = `${environment.USER_BASE}/companies/${company}/category/${id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url , {headers});
  }
}
