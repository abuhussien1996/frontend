// Angular
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
// CRUD
import {HttpUtilsService, QueryParamsModel} from '../../_base/crud';
// Models
import {ContactModel} from '../_models/contact.model';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from '../../../../environments/environment';
import * as _ from 'lodash';

@Injectable()
export class ContactsService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  // CREATE =>  POST: add a new contact to the server
  createContact(contact: ContactModel): Observable<ContactModel> {
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = _.cloneDeep(contact);
    delete body.id;
    delete body.location_id;
    return this.http.post<ContactModel>(url, body, {headers: httpHeaders});

  }

  getAllContacts(): Observable<ContactModel[]> {
    const url = environment.CORE_BASE + '/all-contact';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<ContactModel[]>(url, {headers: httpHeaders});
  }

  // READ
  getContactsNoAdminRole(): Observable<ContactModel[]> {
    const url = environment.CORE_BASE + '/contact-no-admin-role';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<ContactModel[]>(url, {headers: httpHeaders});
  }

  getContactById(contactId: number): Observable<ContactModel> {
    const body = {
      id: contactId,
    }
    const url = environment.CORE_BASE + '/contacts';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<ContactModel>(url, body, {headers: httpHeaders});
  }

  findContacts(queryParams: QueryParamsModel): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const sort = queryParams.sortField.replace(/([-_]\w)/g, g => g[1].toUpperCase())
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts?page=' + queryParams.pageNumber + '&size=' + queryParams.pageSize + '&is_active=' + queryParams.filter.status + '&search_term=' + queryParams.filter.text + '&sort=' + sort + `,${queryParams.sortOrder}` ;
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  getContactsByIds(contactIds: any[]) {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    let ids = ''
    // tslint:disable-next-line:no-shadowed-variable
    contactIds = _.uniqWith(contactIds, _.isEqual);
    contactIds.forEach(element => ids = ids + '&id=' + element)
    const params = new HttpParams({fromString: ids});
    return this.http.get<any>(environment.CORE_BASE + '/companies/' + this.companyId + '/contacts', {
      headers,
      params
    });
  }

  getContactByEntityId(id) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts?entity_id=' + id;
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  searchContacts(search_term: string): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts?search_term=' + search_term;
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  // UPDATE => PUT: update the contact on the server
  updateContact(contact: ContactModel): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts/' + contact.id;
    return this.http.put(url, contact, {headers: httpHeaders});
  }

  // UPDATE Status
  updateStatusForContact(id: number, is_active: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id,
      is_active
    };
    const url = environment.CORE_BASE + '/contactstatusupdatesingle';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // UPDATE Status
  updateStatusForContacts(ids: number[], is_active: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids,
      is_active
    };
    const url = environment.CORE_BASE + '/contactstatusupdate';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // DELETE => delete the contact from the server
  deleteContact(contactId: number): Observable<any> {
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts/' + contactId;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url, {headers: httpHeaders});
  }

  deleteContacts(contactsIds: number[] = []): Observable<any> {
    const url = environment.CORE_BASE + '/multiple-soft-delete-contact';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids: contactsIds
    };
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  searchRelatedPerson(search_term: string, entity_id): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Authorization', 'Bearer ' + this.authToken);
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/entities/' + entity_id + '/relations?term=' + search_term;
    return this.http.get<any>(url, {headers});
  }

  putContactMerge(contacts, contactId) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      other_contacts: contacts
    };
    const url = environment.CORE_BASE + '/companies/' + this.companyId + '/contacts/' + contactId + '/merge';
    return this.http.put(url, body, {headers: httpHeaders, observe: 'response'});
  }

}
