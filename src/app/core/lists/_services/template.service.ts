import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {Template} from '../_models/template.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from 'src/environments/environment';

@Injectable()
export class TemplateService {
  private authToken;
  private company_id;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.company_id = res.companyID );
  }

  createTemplate(_template: Template) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.company_id}/task_templates`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<Template>(url , _template, {headers});
  }

  findTemplates(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const pageSize = queryParams.pageSize;
    const pageIndex = queryParams.pageNumber;
    const searchData = queryParams.filter.searchData;
    const categoryId = queryParams.filter.categoryId;
    const url = `${environment.TIMELINE_BASE}/companies/${this.company_id}/task_templates`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    let params = new HttpParams()
      .append('page',String(pageIndex))
      .append('size',String(pageSize))
      .append('search_term',searchData)
    if (categoryId)
      params = params.append('category_id',categoryId)
    return this.http.get<QueryResultsModel>(url , {headers, params});
  }

  updateTemplate(_template: Template): Observable<any> {
    const url = `${environment.TIMELINE_BASE}/companies/${this.company_id}/task_templates/${_template.id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<Template>(url , _template, {headers});
  }

  deleteTemplate(templateId: number): Observable<any> {
    const url = `${environment.TIMELINE_BASE}/companies/${this.company_id}/task_templates/${templateId}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url , {headers});
  }

  searchTemplates(category_id, search_term) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.company_id}/task_templates`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams()
      .append('category_id',category_id)
      .append('search_term',search_term)
      .append('size','100')
    return this.http.get<any>(url , {headers, params});
  }
}
