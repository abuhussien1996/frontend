// Angular
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
// CRUD
import {HttpUtilsService, QueryResultsModel} from '../../_base/crud';
// Models
import {AttachmentModel} from '../_models/attachment.model';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';

import {environment} from 'src/environments/environment';


@Injectable()
export class AttachmentsService {
  private authToken;
  companyId: any;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID );
  }

  // CREATE =>  POST: add a new attachment to the server
  createAttachment(attachment: AttachmentModel): Observable<AttachmentModel> {
    const url = environment.TIMELINE_BASE + `/companies/${this.companyId}/attachments`;
    const body = attachment;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<AttachmentModel>(url, body, {headers: httpHeaders});
  }

  // READ
  // getAllAttachments(): Observable<AttachmentModel[]> {
  //   const url = environment.TIMELINE_BASE + '/all-attachment';
  //   const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
  //   return this.http.get<AttachmentModel[]>(url, {headers: httpHeaders});
  // }

  // getAttachmentById(attachmentId: number): Observable<AttachmentModel> {
  //   const body = {
  //     id: attachmentId,
  //   }
  //   const url = environment.TIMELINE_BASE + '/companies/{company_id}/attachments?page=0&size=10';
  //   const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
  //   return this.http.post<AttachmentModel>(url, body, {headers: httpHeaders});
  // }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  // Server should return filtered/sorted result
  findAttachments(contact_id, communication_id, received_id, type): Observable<QueryResultsModel> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    let url = '';
    if (type === 'RECEIVED' || type === 'REPLY')
      url = environment.TIMELINE_BASE + `/companies/${this.companyId}/contacts/${contact_id}/received_communication/${received_id}/attachments?type=${type}`;
    else
      url = environment.TIMELINE_BASE + `/companies/${this.companyId}/communication/${communication_id}/attachments`;
    return this.http.get<QueryResultsModel>(url, {headers: httpHeaders});
  }


  // UPDATE Status
  updateStatusForAttachment(id: number, isActive: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      id,
      isActive
    };
    const url = environment.TIMELINE_BASE + '/attachmentstatusupdatesingle';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // UPDATE Status
  updateStatusForAttachments(ids: number[], isActive: boolean): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids,
      isActive
    };
    const url = environment.TIMELINE_BASE + '/attachmentstatusupdate';
    return this.http.put(url, body, {headers: httpHeaders});
  }

  // DELETE => delete the attachment from the server
  deleteAttachment(attachmentId: number): Observable<any> {
    const url = environment.TIMELINE_BASE + `/companies/${this.companyId}/attachments/${attachmentId}`;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url, {headers: httpHeaders});
  }

  deleteAttachments(attachmentsIds: number[] = []): Observable<any> {
    const url = environment.TIMELINE_BASE + '/multiple-soft-delete-attachment';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      ids: attachmentsIds
    };
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

}
