import {forkJoin, of} from 'rxjs';
// Angular
import {Injectable} from '@angular/core';
// RxJS
import {map, mergeMap, catchError} from 'rxjs/operators';
// NGRX
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
// CRUD
import {QueryParamsModel, QueryResultsModel} from '../../_base/crud';
// Services
import {ContactsService} from '../_services/contacts.service';
// State
import {AppState} from '../../reducers';
// Actions
import {
  ContactActionToggleLoading,
  ContactActionTypes,
  ContactCreated,
  ContactOnServerCreated,
  ContactsPageLoaded,
  ContactsPageRequested,
  ContactsPageToggleLoading,
  ContactStatusUpdated,
  ContactsStatusUpdated,
  ContactUpdated,
  ManyContactsDeleted,
  OneContactDeleted,
  ThrowContactError,
  ClearContactErrors,
  ContactsStatusOnServerUpdated,
  ContactStatusOnServerUpdated,
  ContactOnServerUpdated,
  ManyContactsOnServerDeleted,
  OneContactOnServerDeleted,
  ContactsPageCancelled,
  FireContactSuccess
} from '../_actions/contact.actions';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/_services/auth.service';

@Injectable()
export class ContactEffects {
  showPageLoadingDistpatcher = new ContactsPageToggleLoading({isLoading: true});
  showActionLoadingDistpatcher = new ContactActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new ContactActionToggleLoading({isLoading: false});

  @Effect()
  loadContactsPage$ = this.actions$.pipe(
    ofType<ContactsPageRequested>(ContactActionTypes.ContactsPageRequested),
    mergeMap(({payload}) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.contactsService.findContacts(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin([requestToServer, lastQuery]).pipe(
        catchError(error => {
          return of([error]);
        }),
        map(response => {
          if(response[0].error) {
            if(response[0].status === 401) {
              this.authService.logout();
              return new ContactsPageCancelled();
            }
            else if(response[0].status === 403) {
              this.router.navigateByUrl('/error/403');
              return new ContactsPageCancelled();
            } else {
              this.store.dispatch(new ThrowContactError({error: response[0]}));
              return new ContactsPageCancelled();
            }
          } else {
            const result = response[0];
            // tslint:disable-next-line:no-shadowed-variable
            const lastQuery: QueryParamsModel = response[1];
            this.store.dispatch(new ClearContactErrors());
            return new ContactsPageLoaded({
              contacts: result.data,
              totalCount: result.count,
              page: lastQuery
            });
          }
        })
      );
    })
  );

  @Effect()
  deleteContact$ = this.actions$
    .pipe(
      ofType<OneContactOnServerDeleted>(ContactActionTypes.OneContactOnServerDeleted),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.contactsService.deleteContact(payload.id).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowContactError({error: res});
            } else {
              this.store.dispatch(new ClearContactErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new OneContactDeleted(payload)
            }
          })
        );
        }
      ),
    );

  @Effect()
  deleteContacts$ = this.actions$
    .pipe(
      ofType<ManyContactsOnServerDeleted>(ContactActionTypes.ManyContactsOnServerDeleted),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.contactsService.deleteContacts(payload.ids).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowContactError({error: res});
            } else {
              this.store.dispatch(new ClearContactErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ManyContactsDeleted(payload)
            }
          })
        );
        }
      ),
    );

  @Effect()
  updateContact$ = this.actions$
    .pipe(
      ofType<ContactOnServerUpdated>(ContactActionTypes.ContactOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.contactsService.updateContact(payload.contact).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowContactError({error: res});
            } else {
              this.store.dispatch(new ClearContactErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ContactUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  updateContactStatus$ = this.actions$
    .pipe(
      ofType<ContactStatusOnServerUpdated>(ContactActionTypes.ContactStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.contactsService.updateStatusForContact(payload.id, payload.is_active).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res[0]) {
              return new ThrowContactError({error: res[0].details});
            } else {
              this.store.dispatch(new ClearContactErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ContactStatusUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  updateContactsStatus$ = this.actions$
    .pipe(
      ofType<ContactsStatusOnServerUpdated>(ContactActionTypes.ContactsStatusOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.contactsService.updateStatusForContacts(payload.ids, payload.is_active).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowContactError({error: res});
            } else {
              this.store.dispatch(new ClearContactErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ContactsStatusUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createContact$ = this.actions$
    .pipe(
      ofType<ContactOnServerCreated>(ContactActionTypes.ContactOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.contactsService.createContact(payload.contact).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowContactError({error: res});
            } else {
              this.store.dispatch(new ClearContactErrors());
              this.store.dispatch(new FireContactSuccess({action_type: ContactActionTypes.ContactCreated}));
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ContactCreated({contact: res.data})
            }
          })
        )
      }),
    );


  constructor(
    private actions$: Actions,
    private contactsService: ContactsService,
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router
    ) {
  }
}
