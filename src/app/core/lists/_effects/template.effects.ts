import { Injectable } from '@angular/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import {
  TemplateActionTypes,
  TemplatesPageRequested,
  TemplatesPageLoaded,
  TemplateCreated,
  TemplateDeleted,
  TemplateUpdated,
  TemplateOnServerCreated,
  TemplatesActionToggleLoading,
  TemplatesPageToggleLoading,
  ThrowTemplateError,
  FireTemplateSuccess,
  TemplateOnServerUpdated,
  TemplatesPageCancelled,
  TemplateOnServerDeleted
} from '../_actions/template.actions';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/_services';
import {TemplateService} from '../_services/template.service';

@Injectable()
export class TemplateEffects {
  showPageLoadingDistpatcher = new TemplatesPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new TemplatesPageToggleLoading({ isLoading: false });
  showActionLoadingDistpatcher = new TemplatesActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new TemplatesActionToggleLoading({ isLoading: false });

  @Effect()
  loadTemplatesPage$ = this.actions$
    .pipe(
      ofType<TemplatesPageRequested>(TemplateActionTypes.TemplatesPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.template.findTemplates(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin([requestToServer, lastQuery]).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if(response[0] && response[0].error) {
              if(response[0].status === 401) {
                this.authService.logout();
                return new TemplatesPageCancelled();
              } else if(response[0].status === 403) {
                this.router.navigateByUrl('/error/403');
                return new TemplatesPageCancelled();
              } else {
                this.store.dispatch(new ThrowTemplateError({error: response[0].error.error}));
                return new TemplatesPageCancelled();
              }
            } else {
              const result = response[0];
              return new TemplatesPageLoaded({
                templates: result.data,
                totalCount: result.count,
                page: response[1]
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteTemplate$ = this.actions$
    .pipe(
      ofType<TemplateOnServerDeleted>(TemplateActionTypes.TemplateOnServerDeleted),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.template.deleteTemplate(payload.id).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowTemplateError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireTemplateSuccess({action_type: TemplateActionTypes.TemplateDeleted}));
              return new TemplateDeleted(payload);
            }
          })
        );
        }
      ),
    );

  @Effect()
  updateTemplate$ = this.actions$
    .pipe(
      ofType<TemplateOnServerUpdated>(TemplateActionTypes.TemplateOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.template.updateTemplate(payload.template).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowTemplateError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireTemplateSuccess({action_type: TemplateActionTypes.TemplateUpdated}));
              return new TemplateUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createTemplate$ = this.actions$
    .pipe(
      ofType<TemplateOnServerCreated>(TemplateActionTypes.TemplateOnServerCreated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.template.createTemplate(payload.template).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowTemplateError({error: res.error});
            } else {
              this.store.dispatch(new FireTemplateSuccess({action_type: TemplateActionTypes.TemplateCreated}));
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new TemplateCreated({template: res.data});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private template: TemplateService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
  ) {
  }
}
