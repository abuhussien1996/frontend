import { forkJoin, of } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { map, mergeMap, catchError } from 'rxjs/operators';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Services
import { AttachmentsService } from '../_services/attachments.service';
// State
import { AppState } from '../../reducers';
// Actions
import {
  AttachmentActionToggleLoading,
  AttachmentActionTypes,
  AttachmentCreated,
  AttachmentOnServerCreated,
  AttachmentsPageLoaded,
  AttachmentsPageRequested,
  AttachmentsPageToggleLoading,
  AttachmentStatusUpdated,
  AttachmentsStatusUpdated,
  AttachmentUpdated,
  ManyAttachmentsDeleted,
  OneAttachmentDeleted,
  ThrowAttachmentError,
  ClearAttachmentErrors,
  AttachmentsStatusOnServerUpdated,
  AttachmentStatusOnServerUpdated,
  AttachmentOnServerUpdated,
  ManyAttachmentsOnServerDeleted,
  OneAttachmentOnServerDeleted,
  AttachmentsPageCancelled
} from '../_actions/attachment.actions';
import { Router } from '@angular/router';

@Injectable()
export class AttachmentEffects {
  showPageLoadingDistpatcher = new AttachmentsPageToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new AttachmentActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new AttachmentActionToggleLoading({ isLoading: false });

  @Effect()
  loadAttachmentsPage$ = this.actions$.pipe(
    ofType<AttachmentsPageRequested>(AttachmentActionTypes.AttachmentsPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.attachmentsService.findAttachments(payload.contact_id, payload.communication_id, payload.received_id, payload.type);
      return forkJoin(requestToServer).pipe(
        catchError(error => {
          return of([error]);
        }),
        map(response => {
          if (response[0].error) {
            if (response[0].status === 403 || response[0].status === 401) {
              this.router.navigateByUrl('/error/403');
              return new AttachmentsPageCancelled();
            } else {
              this.store.dispatch(new ThrowAttachmentError({ error: response[0].error }));
              return new AttachmentsPageCancelled();
            }
          } else {
            const result = response[0];
            // tslint:disable-next-line:no-shadowed-variable
            const lastQuery: QueryParamsModel = response[1];
            this.store.dispatch(new ClearAttachmentErrors());
            const mappedResult = [...Array(result.data.length).keys()].map(i => Object.assign(result.data[i], { id: (i + 1) }));
            console.log(mappedResult)
            return new AttachmentsPageLoaded({
              attachments: mappedResult,
              totalCount: result.count,
              page: lastQuery
            });
          }
        })
      );
    })
  );

  @Effect()
  deleteAttachment$ = this.actions$
    .pipe(
      ofType<OneAttachmentOnServerDeleted>(AttachmentActionTypes.OneAttachmentOnServerDeleted),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.attachmentsService.deleteAttachment(payload.id).pipe(
          catchError(error => {
            return of({ error });
          }),
          map((res) => {
            if (res && res.error) {
              return new ThrowAttachmentError({ error: res.error });
            } else {
              this.store.dispatch(new ClearAttachmentErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new OneAttachmentDeleted(payload)
            }
          })
        );
      }
      ),
  );

  @Effect()
  deleteAttachments$ = this.actions$
    .pipe(
      ofType<ManyAttachmentsOnServerDeleted>(AttachmentActionTypes.ManyAttachmentsOnServerDeleted),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.attachmentsService.deleteAttachments(payload.ids).pipe(
          catchError(error => {
            return of({ error });
          }),
          map((res) => {
            if (res.error) {
              return new ThrowAttachmentError({ error: res.error });
            } else {
              this.store.dispatch(new ClearAttachmentErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ManyAttachmentsDeleted(payload)
            }
          })
        );
      }
      ),
  );

  // @Effect()
  // updateAttachment$ = this.actions$
  //   .pipe(
  //     ofType<AttachmentOnServerUpdated>(AttachmentActionTypes.AttachmentOnServerUpdated),
  //     mergeMap(({ payload }) => {
  //       this.store.dispatch(this.showActionLoadingDistpatcher);
  //       return this.attachmentsService.updateAttachment(payload.attachment).pipe(
  //         catchError(error => {
  //           return of({ error });
  //         }),
  //         map((res) => {
  //           if (res && res.error) {
  //             return new ThrowAttachmentError({ error: res.error });
  //           } else {
  //             this.store.dispatch(new ClearAttachmentErrors());
  //             this.store.dispatch(this.hideActionLoadingDistpatcher);
  //             return new AttachmentUpdated(payload)
  //           }
  //         })
  //       );
  //     }),
  // );

  @Effect()
  updateAttachmentStatus$ = this.actions$
    .pipe(
      ofType<AttachmentStatusOnServerUpdated>(AttachmentActionTypes.AttachmentStatusOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.attachmentsService.updateStatusForAttachment(payload.id, payload.is_active).pipe(
          catchError(error => {
            return of({ error });
          }),
          map((res) => {
            if (res.error) {
              return new ThrowAttachmentError({ error: res.error });
            } else {
              this.store.dispatch(new ClearAttachmentErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new AttachmentStatusUpdated(payload)
            }
          })
        );
      }),
  );

  @Effect()
  updateAttachmentsStatus$ = this.actions$
    .pipe(
      ofType<AttachmentsStatusOnServerUpdated>(AttachmentActionTypes.AttachmentsStatusOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.attachmentsService.updateStatusForAttachments(payload.ids, payload.is_active).pipe(
          catchError(error => {
            return of({ error });
          }),
          map((res) => {
            if (res.error) {
              return new ThrowAttachmentError({ error: res.error });
            } else {
              this.store.dispatch(new ClearAttachmentErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new AttachmentsStatusUpdated(payload)
            }
          })
        );
      }),
  );

  @Effect()
  createAttachment$ = this.actions$
    .pipe(
      ofType<AttachmentOnServerCreated>(AttachmentActionTypes.AttachmentOnServerCreated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.attachmentsService.createAttachment(payload.attachment).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if (res && res.error) {
              return new ThrowAttachmentError({ error: res });
            } else {
              this.store.dispatch(new ClearAttachmentErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new AttachmentCreated({ attachment: res.data })
            }
          })
        )
      }),
  );

  constructor(
    private actions$: Actions,
    private attachmentsService: AttachmentsService,
    private store: Store<AppState>,
    private router: Router
  ) {
  }
}
