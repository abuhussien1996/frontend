import { Injectable } from '@angular/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import {forkJoin, of} from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import {
  CategoryActionTypes,
  CategoryOnServerCreated,
  ThrowCategoryError,
  FireCategorySuccess,
  CategoryOnServerUpdated,
  CategoryOnServerDeleted,
  CategoriesPageRequested,
  CategoriesPageToggleLoading,
  CategoriesPageCancelled,
  CategoriesPageLoaded
} from '../_actions/category.actions';
import { Router } from '@angular/router';
import { CategoryService } from '../_services';
import { AuthService } from '../../auth/_services';
import {QueryParamsModel} from '../../_base/crud';

@Injectable()
export class CategoryEffects {

  @Effect()
  loadCategoriesPage$ = this.actions$
    .pipe(
      ofType<CategoriesPageRequested>(CategoryActionTypes.CategoriesPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(new CategoriesPageToggleLoading({ isLoading: true }));
        const requestToServer = this.category.findCategories(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin([requestToServer, lastQuery]).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if(response[0].error) {
              if(response[0].status === 403 || response[0].status === 401) {
                this.router.navigateByUrl('/error/403');
                return new CategoriesPageCancelled();
              } else {
                this.store.dispatch(new ThrowCategoryError({error: response[0].error}));
                return new CategoriesPageCancelled();
              }
            } else {
              const result = response[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery: QueryParamsModel = response[1];
              return new CategoriesPageLoaded({
                categories: result.data,
                totalCount: result.count,
                page: lastQuery
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteCategory$ = this.actions$
    .pipe(
      ofType<CategoryOnServerDeleted>(CategoryActionTypes.CategoryOnServerDeleted),
      mergeMap(({ payload }) => {
        return this.category.deleteCategory(payload.id).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowCategoryError({error: res.error});
            } else {
              return new FireCategorySuccess({action_type: CategoryActionTypes.CategoryDeleted});
            }
          })
        );
        }
      ),
    );

  @Effect()
  updateCategory$ = this.actions$
    .pipe(
      ofType<CategoryOnServerUpdated>(CategoryActionTypes.CategoryOnServerUpdated),
      mergeMap(({ payload }) => {
        return this.category.updateCategory(payload.category).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowCategoryError({error: res.error});
            } else {
              return new FireCategorySuccess({action_type: CategoryActionTypes.CategoryUpdated});
            }
          })
        );
      }),
    );

  @Effect()
  createCategory$ = this.actions$
    .pipe(
      ofType<CategoryOnServerCreated>(CategoryActionTypes.CategoryOnServerCreated),
      mergeMap(({ payload }) => {
        return this.category.createCategory(payload).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(res && res.error) {
              return new ThrowCategoryError({error: res.error});
            } else {
              return new FireCategorySuccess({action_type: CategoryActionTypes.CategoryCreated});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private category: CategoryService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
  ) {}
}
