// NGRX
import { routerReducer } from '@ngrx/router-store';
import {ActionReducer, ActionReducerMap, MetaReducer} from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '../../../environments/environment';
import {AuthActionTypes} from '../auth';

// that interface will be fill by all reducers (this is contain all reducers when add new reducer it will added here)
// tslint:disable-next-line:no-empty-interface
export interface AppState { }
// define type of reducers ,,,acording the method forRoot on app.module the type it is ActionReducerMap and it is
// generic type(interface)
export const reducers: ActionReducerMap<AppState> = { router: routerReducer };

export const metaReducers: Array<MetaReducer<AppState>> =  [clearState] ;

export function clearState(reducer: ActionReducer<any>): ActionReducer<any> {
  // tslint:disable-next-line:only-arrow-functions
  return function (state, action) {
    if (action.type === AuthActionTypes.Logout) {
      state = {};
    }
    return reducer(state, action);
  };
}
