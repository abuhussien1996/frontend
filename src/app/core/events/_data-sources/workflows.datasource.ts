// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import {
  selectWorkflowsInStore,
  selectWorkflowsPageLoading,
  selectWorkflowsShowInitWaitingMessage
} from '../_selectors/workflow.selectors';


export class WorkflowsDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectWorkflowsPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectWorkflowsShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectWorkflowsInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
