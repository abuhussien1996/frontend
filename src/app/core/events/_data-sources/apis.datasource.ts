import {select, Store} from '@ngrx/store';
import {BaseDataSource, QueryResultsModel} from '../../_base/crud';
import {AppState} from '../../../core/reducers';
import {selectApisInStore, selectApisPageLoading, selectApisShowInitWaitingMessage} from '../_selectors/api.selectors';


export class ApisDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectApisPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectApisShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectApisInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
