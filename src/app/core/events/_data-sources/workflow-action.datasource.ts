// NGRX
import {select, Store} from '@ngrx/store';
// CRUD
import {BaseDataSource, QueryResultsModel} from '../../_base/crud';
// State
import {AppState} from '../../reducers';
import {
  selectWorkflowActionsInStore,
  selectWorkflowActionsPageLoading,
  selectWorkflowActionsShowInitWaitingMessage
} from '../_selectors/workflow-actions.selectors';


export class WorkflowActionsDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectWorkflowActionsPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectWorkflowActionsShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectWorkflowActionsInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
