import { Store, select } from '@ngrx/store';
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
import { AppState } from '../../reducers';
import { selectEventActionsInStore, selectEventActionsPageLoading, selectEventActionsShowInitWaitingMessage } from '../_selectors/action.selectors';


export class EventActionsDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectEventActionsPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectEventActionsShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectEventActionsInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
