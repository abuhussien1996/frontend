import {select, Store} from '@ngrx/store';
import {BaseDataSource, QueryResultsModel} from '../../_base/crud';
import {AppState} from '../../../core/reducers';
import {selectInboxInStore, selectInboxPageLoading, selectInboxShowInitWaitingMessage} from '../_selectors/inbox.selectors';


export class InboxDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectInboxPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectInboxShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectInboxInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
