// Angular
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
// CRUD
import {HttpUtilsService, QueryParamsModel} from '../../_base/crud';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from '../../../../environments/environment';
import {InboxItem} from '..';


@Injectable()
export class InboxService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  loadInbox(queryParams: QueryParamsModel) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/received_communication`;
    let params = new HttpParams()
      .set('page', `${queryParams.pageNumber}`)
      .set('size', `${queryParams.pageSize}`)
      .set('sort', `${queryParams.sortField},${queryParams.sortOrder}`);
    if (queryParams.filter.type !== '') {
      params = params.set('type', queryParams.filter.type);
    }
    if (queryParams.filter.from) {
      params = params.set('from_date', queryParams.filter.from);
    }
    if (queryParams.filter.to) {
      params = params.set('to_date', queryParams.filter.to);
    }
    if (queryParams.filter.domain) {
      params = params.set('domain', queryParams.filter.domain);
    }
    if (queryParams.filter.inbox_id) {
      params = params.set('inbox_id', queryParams.filter.inbox_id);
    }
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers, params});
  }

  getWorkflow(workflow_id, workflow_queue_id?) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${workflow_id}`;
    const params = workflow_queue_id ? new HttpParams({
      fromString: `workflow_queue_id=${workflow_queue_id}`
    }) : null;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers, params});
  }

  getAttachments(contact_id, item_id?) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/contacts/${contact_id}/received_communication/${item_id}/attachments`;
    const params = new HttpParams({
      fromString: 'type=RECEIVED'
    });
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers, params});
  }

  startWorkflow(item, itemWorkflow, submissionData): Observable<any> {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow/${itemWorkflow.workflow_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      workflow_action_id: itemWorkflow.id,
      workflow_queue_id: null,
      input_data: submissionData,
      move_to_next: true,
      form_schema: itemWorkflow.form.schema,
      received_id: item.id
    };
    return this.http.post<any>(url, body, {headers})
  }

  updateItem(inbox): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/received_communication/' + inbox.id;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, inbox, {headers});
  }

  postAttachments(workflow_queue_id, attachments) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_queues/${workflow_queue_id}/attachments`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, {attachments}, {headers});
  }

  getWorkflowGroups() {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_groups`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers});
  }

  getWorkflows(workflow_group_id) {
    const url = `${environment.EVENT_BASE}/companies/${this.companyId}/workflow_groups/${workflow_group_id}/workflows`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers});
  }

  reply(item, msgBody, attachments, mode) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/contacts/${item.sender_id}/received_communication/${item.id}/replies?communication_type=${mode}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      sender_name: item.sender_name,
      subject: item.subject,
      body: msgBody,
      attachment_names: attachments
    }
    return this.http.post<any>(url, body, {headers});
  }

  postReplyAttachments(item, replyId, uuids, mode) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/contacts/${item.sender_id}/received_communication/${item.id}/reply/${replyId}/attachments?communication_type=${mode}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, {uuids}, {headers});
  }

  executeApi(method, url: string, body, data, workflow_queue_id) {
    if (url.includes('environment.')) {
      const startPoint = url.indexOf('environment.');
      const endPoint = url.indexOf('/', startPoint);
      const x = url.substring(startPoint + 12, endPoint);
      const replacement = environment[x] ? environment[x] : '';
      url = `${url.substring(0, startPoint)}${replacement}${url.substring(endPoint)}`;
    }
    url = url.replace('{workflow_queue_id}', workflow_queue_id);
    let start = url.indexOf('data.');
    while (start !== -1) {
      const end = url.indexOf('/', start) !== -1 ? url.indexOf('/', start) : url.length;
      const keyArr = url.substring(start + 5, end).split('.');
      let oldValue = data;
      for (const attr of keyArr) {
        if (oldValue !== undefined && oldValue !== null) {
          oldValue = oldValue[attr];
        } else {
          oldValue = '';
          break;
        }
      }
      url = url.substring(0, start) + oldValue + url.substring(end);
      start = url.indexOf('data.');
    }
    let begin = body.indexOf('${');
    while (begin !== -1) {
      let end = body.indexOf('}', begin);
      const keyArr = body.substring(begin + 7, end).split('.');
      let oldValue = data;
      for (const attr of keyArr) {
        if (oldValue !== undefined && oldValue !== null) {
          oldValue = oldValue[attr];
        } else {
          oldValue = '';
          break;
        }
      }
      const replacement = typeof (oldValue) === 'string' || typeof (oldValue) === 'number' ? oldValue : JSON.stringify(oldValue);
      if (typeof (oldValue) === 'number' && body.charAt(begin - 1) === '"' && body.charAt(end + 1) === '"') {
        begin--;
        end++;
      }
      body = `${body.substring(0, begin)}${replacement}${body.substring(end + 1)}`;
      begin = body.indexOf('${');
    }
    body = body.replace(new RegExp('\n', 'g'), '<br />');
    body = body.replace(new RegExp('\t', 'g'), '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    while (body.indexOf('"[') !== -1) {
      body = body.replace('"[', '[');
      body = body.replace(']"', ']');
    }
    const payload = JSON.parse(body);
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return method === 'POST' ? this.http.post<any>(url, payload, {headers}) : this.http.put(url, payload, {headers});
  }

  lockInbox(inboxItem: InboxItem, lock) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/received_communication/${inboxItem.id}/lock`;
    const body = {
      locked: lock
    }
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, body, {headers});
  }

  getLatestInboxUpdate(inboxItem: InboxItem) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/received_communication/${inboxItem.id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers});
  }

  updateInbox(inbox,taskId): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/received_communication/' + inbox.id + '/connect_to_task';
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      task:{
        id:taskId
      }
    }
    return this.http.put<any>(url, body, {headers});
  }
}
