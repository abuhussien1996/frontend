import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {Workflow} from '../_models/workflow.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';
import {environment} from 'src/environments/environment';
import {WorkflowAction} from '..';

@Injectable()
export class WorkflowService {
  private authToken;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  createWorkflow(_workflow: Workflow) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<Workflow>(environment.EVENT_BASE + '/workflow', _workflow, {
      headers: httpHeaders,
      observe: 'response'
    })
  }

  getAllWorkflows(): Observable<Workflow[]> {
    const url = environment.EVENT_BASE + '/workflowa';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<Workflow[]>(url, {headers: httpHeaders});
  }

  getWorkflowById(workflowId: number): Observable<Workflow> {
    const body = {
      id: workflowId,
    }
    const url = environment.EVENT_BASE + '/workflows';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<Workflow>(url, body, {headers: httpHeaders});
  }

  findWorkflows(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const searchData = queryParams.filter.searchData
    const company = queryParams.filter.company
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    const params = new HttpParams({fromString: 'page=' + pageIndex + '&size=' + pageSize + '&search_term=' + searchData + '&company_id=' + company});
    return this.http.get<QueryResultsModel>(environment.EVENT_BASE + '/workflow',
      {headers, params});
  }

  updateWorkflow(_workflow: Workflow): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const id = _workflow.id
    return this.http.put(environment.EVENT_BASE + '/workflow/' + id, _workflow, {headers: httpHeaders})
  }

  deleteWorkflow(workflowId: number): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(environment.EVENT_BASE + '/workflow/' + workflowId, {headers: httpHeaders});
  }

  // Workflow-actions
  findWorkflowActions(queryParams: QueryParamsModel, _workflow: number, _company: number): Observable<QueryResultsModel> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    const params = new HttpParams({fromString: 'company_id=' + _company});
    const workflowId = _workflow
    return this.http.get<QueryResultsModel>(environment.EVENT_BASE + '/workflow/' + workflowId + '/workflow_actions',
      {headers, params});
  }


  createWorkflowAction(_workflowAction: WorkflowAction, _workflow: number, _company: number) {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      description: _workflowAction.description,
      name: _workflowAction.name,
      action: _workflowAction.action,
      form: _workflowAction.form,
      apis: _workflowAction.apis,
      status: _workflowAction.status,
      parent_id: _workflowAction.parent_id
    }
    console.log(body);
    const workflowId = _workflow
    const params = new HttpParams({fromString: 'company_id=' + _company});
    return this.http.post<WorkflowAction>(environment.EVENT_BASE + '/workflow/' + workflowId + '/workflow_actions', body,
      {headers, params, observe: 'response'})
  }

  updateWorkflowAction(_workflowAction: WorkflowAction, _workflow: number, _company: number): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'company_id=' + _company});
    return this.http.put(
      environment.EVENT_BASE + '/workflow/' + _workflow + '/workflow_actions/' + _workflowAction.id,
      _workflowAction,
      {headers, params}
    );
  }

  searchForms(companyId,term: string): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'term=' + term + '&company_id=' + companyId});
    return this.http.get<any>(environment.EVENT_BASE + '/forms/autocomplete', {headers, params});
  }

  getAllWorkflowActions(_workflow: number, _company: number): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'company_id=' + _company});
    const workflowId = _workflow
    return this.http.get<any>(environment.EVENT_BASE + '/workflow/' + workflowId + '/workflow_actions', {headers, params});
  }

  updateWorkflowActionDeleted(_workflowAction: WorkflowAction[], _workflow: number, _company: number): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      workflow_actions: _workflowAction,
    }
    console.log(body);
    const params = new HttpParams({fromString: 'company_id=' + _company});
    return this.http.put(environment.EVENT_BASE + '/workflow/' + _workflow + '/workflow_actions', body, {
      headers,
      params,
      responseType: 'text',
      observe: 'response'
    })
  }

  mappingWorkflowAction(body, id, companyId) {
    const baseUrl = environment.EVENT_BASE + `/workflow/` + id + `/workflow_actions/` + body.id;
    const params = new HttpParams({fromString: 'company_id=' + companyId});
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(baseUrl, body, {headers, params});
  }


  getStatusByGroupANDCompany(workflowGroupId, companyId) {
    const baseUrl = environment.EVENT_BASE + `/companies/` + companyId + `/workflow_groups/` + workflowGroupId + '/statuses';
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(baseUrl, {headers});
  }

}
