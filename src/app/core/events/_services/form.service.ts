import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpUtilsService} from '../../_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';
import {environment} from 'src/environments/environment';

@Injectable()
export class FormService {
  private authToken;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  getForm(form_id: number) {
    const url = `${environment.EVENT_BASE}/forms/${form_id}`;
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers});
  }
}
