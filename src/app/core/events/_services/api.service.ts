import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Api} from '../_models/api.model';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {environment} from 'src/environments/environment';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';

@Injectable({providedIn: 'root'})
export class ApiService {
  private authToken;

  constructor(
    private httpClient: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  createApi(_api: Api) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.post<Api>(environment.EVENT_BASE + '/apis', _api, {
      headers: httpHeaders,
      observe: 'response'
    })
  }

  getAllApis(): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.get<any>(environment.EVENT_BASE + '/apis', {headers: httpHeaders});
  }


  getApiById(apiId?: number): Observable<Api> {
    return this.httpClient.post<Api>(environment.USER_BASE + '/api-by-id', {id: apiId});
  }

  deleteApi(id?: number) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.delete<any>(environment.EVENT_BASE + '/apis/' + id, {headers: httpHeaders});
  }

  updateApi(_api: Api): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const id = _api.id
    return this.httpClient.put(environment.EVENT_BASE + '/apis/' + id, _api, {headers: httpHeaders}
    );
  }

  findApis(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const searchData = queryParams.filter.searchData
    const company = queryParams.filter.company
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({
      fromString: 'page=' + pageIndex + '&size=' + pageSize + '&search_term=' + searchData + '&company_id=' + company
    });
    return this.httpClient.get<QueryResultsModel>(environment.EVENT_BASE + '/apis', {headers, params});
  }

  searchApis(companyId, term: string): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'term=' + term + '&company_id=' + companyId+ '&not_attached=true'});
    return this.httpClient.get<any>(environment.EVENT_BASE + '/apis/autocomplete', {headers, params});
  }
}
