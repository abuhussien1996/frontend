import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpUtilsService} from '../../_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';


@Injectable()
// tslint:disable-next-line:class-name
export class WorkflowGroupService {
  private authToken;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  getAllWorkflowGroup(): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(environment.EVENT_BASE + '/workflow_groups', {headers: httpHeaders});
  }

  createWorkflowGroup(WorkflowGroup): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(environment.EVENT_BASE + '/workflow_groups', WorkflowGroup, {headers: httpHeaders});
  }

  updateWorkflowGroup(WorkflowGroup): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(environment.EVENT_BASE + '/workflow_groups/' + WorkflowGroup.id, WorkflowGroup, {
      headers: httpHeaders,
      observe: 'response'
    });
  }

  deleteWorkflowGroup(workflowGroupId: any) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(environment.EVENT_BASE + '/workflow_groups/' + workflowGroupId, {
      headers: httpHeaders,
      observe: 'response'
    });
  }
}
