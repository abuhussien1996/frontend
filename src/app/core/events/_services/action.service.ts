import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {EventAction} from '../_models/action.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from 'src/environments/environment';

@Injectable()
export class EventActionService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  createEventAction(_eventAction: EventAction) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      description: _eventAction.description,
      action_name: _eventAction.action_name,
      is_active: _eventAction.is_active,
      company_id: _eventAction.company_id
    }
    return this.http.post<EventAction>(environment.EVENT_BASE + '/actions', body, {
      headers: httpHeaders,
      observe: 'response'
    })
  }

  findEventActions(queryParams: QueryParamsModel): Observable<any> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const searchData = queryParams.filter.searchData
    const company = queryParams.filter.company
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    const params = new HttpParams({fromString: 'page=' + pageIndex + '&size=' + pageSize + '&search_term=' + searchData + '&company_id=' + company});
    return this.http.get<QueryResultsModel>(environment.EVENT_BASE + '/actions',
      {headers, params});
  }

  updateEventAction(_eventAction: EventAction): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      description: _eventAction.description,
      action_name: _eventAction.action_name,
      is_active: _eventAction.is_active,
      company_id: _eventAction.company_id
    }
    const id = _eventAction.id
    return this.http.put(environment.EVENT_BASE + '/actions/' + id, body, {headers: httpHeaders})
  }

  searchEventActions(companyId, term: string): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'term=' + term + '&company_id=' + companyId});
    return this.http.get<any>(environment.EVENT_BASE + '/actions/autocomplete', {headers, params});
  }
}
