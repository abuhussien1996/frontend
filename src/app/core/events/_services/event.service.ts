import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {Event} from '../_models/event.model';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken} from '../../auth/_selectors/auth.selectors';
import {environment} from 'src/environments/environment';

@Injectable()
export class EventService {
  private authToken;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
  }

  createEvent(_event: Event) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      description: _event.description,
      event_name: _event.event_name,
      visible_from_app: _event.visible_from_app,
      company_id: _event.company_id,
      responsible: _event.responsible,
      due_date: _event.due_date
    }
    return this.http.post<Event>(environment.EVENT_BASE + '/events', body, {
      headers: httpHeaders,
      observe: 'response'
    })
  }

  getAllEvents() {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<QueryResultsModel>(environment.EVENT_BASE + '/events',
      {headers})
  }

  getEventById(eventId: number): Observable<Event> {
    const body = {
      id: eventId,
    }
    const url = environment.EVENT_BASE + '/events';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<Event>(url, body, {headers: httpHeaders});
  }

  findEvents(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const searchData = queryParams.filter.searchData
    const company = queryParams.filter.company
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    const params = new HttpParams({fromString: 'page=' + pageIndex + '&size=' + pageSize + '&search_term=' + searchData + '&company_id=' + company});
    return this.http.get<QueryResultsModel>(environment.EVENT_BASE + '/events',
      {headers, params});
  }

  updateEvent(_event: Event): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      description: _event.description,
      event_name: _event.event_name,
      visible_from_app: _event.visible_from_app,
      company_id: _event.company_id,
      responsible: _event.responsible,
      due_date: _event.due_date
    }
    const id = _event.id
    return this.http.put(environment.EVENT_BASE + '/events/' + id, body, {headers: httpHeaders})
  }

  deleteEvent(eventId: number): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(environment.EVENT_BASE + '/events/' + eventId, {headers: httpHeaders});
  }

  searchEvents(term: string, companyId): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'term=' + term + '&company_id=' + companyId});
    return this.http.get<any>(environment.EVENT_BASE + '/events/autocomplete', {headers, params});
  }
  searchEventsForWorkflow(companyId,term: string ): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'term=' + term + '&company_id=' + companyId});
    return this.http.get<any>(environment.EVENT_BASE + '/events/autocomplete', {headers, params});
  }
}
