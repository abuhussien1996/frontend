import {BaseModel} from '../../_base/crud';

export class InboxItem extends BaseModel {
  id: number;
  sender_id: number;
  sender_name: string;
  subject: string;
  sender_email: string;
  pre_signed_urls: null;
  type: 'EMAIL' | 'FAX';
  created: any;
  updated: any;
  status: 'PENDING' | 'IN_PROGRESS' | 'DONE';
  workflow_id: number;
  workflow_queue_id: number;
  fax: string;
  body: string;
  locked: boolean;
  locked_by: string;
}
