import {BaseModel} from '../../_base/crud';

export class EventAction extends BaseModel {
  id: number;
  action_name: any;
  company_id: number;
  description: string;
  is_active: boolean;
  last_modified_by: string;
  last_modified_date: string;

  clear(): void {
    this.id = 0;
    this.action_name = '';
    this.last_modified_by = '';
    this.company_id = null;
    this.description = '';
    this.is_active = false;
  }
}
