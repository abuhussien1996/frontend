import {BaseModel} from '../../_base/crud';

export class Api extends BaseModel {
  id: number;
  api_name: string;
  description: string;
  is_active: boolean;
  payload: any
  company_id: number
  url: string;
  http_method: string;
  execution_type:string;
  condition:string;
  clear(): void {
    this.id = 0;
    this.api_name = '';
    this.description = '';
    this.payload = null;
    this.company_id = null;
    this.url = '';
    this.http_method = '';
    this.is_active = false;
    this.execution_type = null;
    this.condition = null;
  }
}
