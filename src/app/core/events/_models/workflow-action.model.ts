import { BaseModel } from '../../_base/crud';
import { EventAction } from './action.model';
import { Api } from './api.model';
import { Form } from './form.model';

export class WorkflowAction extends BaseModel {
  id: number;
  name:any;
  description: string;
  action: EventAction;
  form: Form;
  apis;
  parent_id:number
  status:string
  deleted:boolean
  mapping_template:string

  clear(): void {
    this.id = 0;
    this.name = '';
    this.form = null;
    this.action = null;
    this.apis = null;
    this.description = '';
    this.parent_id = null;
    this.status = '';
    this.deleted = false;
    this.mapping_template = '';
  }
}
