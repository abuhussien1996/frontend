import {BaseModel} from '../../_base/crud';
import {Status} from './status.models';

export class EventWorkflowGroup extends BaseModel {
  id: number;
  name: any;
  // tslint:disable-next-line:variable-name
  company_id: number;
  description: string;
  isActive: boolean;
  status: Status [];
  schema: string;

  clear(): void {
    this.id = 0;
    this.name = '';
    this.status = [];
    this.company_id = null;
    this.description = '';
    this.schema = null;
    this.isActive = false;
  }
}
