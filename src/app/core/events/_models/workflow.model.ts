import {BaseModel} from '../../_base/crud';
import {Event} from './event.model'

export class Workflow extends BaseModel {
  id: number;
  name: any;
  company_id: number;
  workflow_group_id: number;
  description: string;
  is_active: boolean;
  last_modified_by: string;
  last_modified_date: string;
  event: Event;
  category
  category_id
  schema: string;

  clear(): void {
    this.workflow_group_id = null;
    this.id = 0;
    this.name = '';
    this.last_modified_by = '';
    this.company_id = null;
    this.description = '';
    this.is_active = false;
    this.event = null;
    this.schema = null;
  }
}
