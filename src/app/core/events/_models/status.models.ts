export class Status {
  number: string;
  status: string;
  workflow_group_id: number;
  hidden

  clear(): void {
    this.number = null;
    this.status = null;
    this.workflow_group_id = null;
    this.hidden = false
  }
}
