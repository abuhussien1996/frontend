import { BaseModel } from '../../_base/crud';

export class Event extends BaseModel {
  id: number;
  event_name:any;
  company_id:number;
  description: string;
  responsible:string;
  due_date:string;
  visible_from_app:boolean;



  clear(): void {
    this.id = 0;
    this.event_name = '';
    this.responsible = '';
    this.company_id = null;
    this.description = '';
    this.due_date = '';
    this.visible_from_app = false;

  }
}
