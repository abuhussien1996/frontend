import {BaseModel} from '../../_base/crud';

export class Form extends BaseModel {
  id: number;
  form_name: string;
  description: string;
  is_active: boolean;
  schema: any
  company_id: number

  clear(): void {
    this.id = 0;
    this.form_name = '';
    this.description = '';
    this.schema = null;
    this.company_id = null;
    this.is_active = false;
  }
}
