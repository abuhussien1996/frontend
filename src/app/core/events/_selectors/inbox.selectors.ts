import {createFeatureSelector, createSelector} from '@ngrx/store';
import {HttpExtenstionsModel, QueryResultsModel} from '../../_base/crud';
import {InboxState} from '../_reducers/inbox.reducers';
import {each} from 'lodash';
import {InboxItem} from '../_models/inbox.model';

export const selectInboxState = createFeatureSelector<InboxState>('inbox');

export const selectInboxById = (inboxId: number) => createSelector(
  selectInboxState,
  inboxState => inboxState.entities[inboxId]
);

export const selectInboxPageLoading = createSelector(
  selectInboxState,
  inboxState => {
    return inboxState.listLoading;
  }
);

export const selectInboxActionLoading = createSelector(
  selectInboxState,
  inboxState => inboxState.actionLoading
);

export const selectInboxWorkflow = createSelector(
  selectInboxState,
  inboxState => inboxState.inboxWorkflow
);

export const selectInboxAttachments = createSelector(
  selectInboxState,
  inboxState => inboxState.attachments
);

export const selectInboxPageLastQuery = createSelector(
  selectInboxState,
  inboxState => inboxState.lastQuery
);
export const selectInboxError = createSelector(
  selectInboxState,
  inboxState => inboxState.error
);
export const selectInboxLatestSuccessfullAction = createSelector(
  selectInboxState,
  inboxState =>  inboxState.latestSuccessfullAction
);
export const selectInboxInStore = createSelector(
  selectInboxState,
  inboxState => {
    const items: InboxItem[] = [];
    each(inboxState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: InboxItem[] = httpExtension.sortArray(
      items,
      inboxState.lastQuery.sortField,
      inboxState.lastQuery.sortOrder
      );
    return new QueryResultsModel(result, inboxState.totalCount);
  }
);

export const selectInboxShowInitWaitingMessage = createSelector(
  selectInboxState,
  inboxState => inboxState.showInitWaitingMessage
);

export const selectLatestCardValues = createSelector(
  selectInboxState,
  inboxState => inboxState.latestCardValues
);

export const selectHasInboxInStore = createSelector(
  selectInboxState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }
    return true;
  }
);
