import {createFeatureSelector, createSelector} from '@ngrx/store';
import {WorkflowGroupState} from '../_reducers/workflow-group.reducers';
import {each} from 'lodash';
import {HttpExtenstionsModel, QueryResultsModel} from '../../_base/crud';
import {EventWorkflowGroup} from '../_models/workflow-group.model';


export const selectWorkflowGroupState = createFeatureSelector<WorkflowGroupState>('workflowGroup');

export const selectWorkflowGroupPageLoading = createSelector(
  selectWorkflowGroupState,
  workflowGroupState => {
    return workflowGroupState.listLoading;
  }
);

export const selectWorkflowGroupError = createSelector(
  selectWorkflowGroupState,
  workflowGroupState => workflowGroupState.error
);

export const selectWorkflowGroupLatestSuccessfulAction = createSelector(
  selectWorkflowGroupState,
  workflowGroupState => workflowGroupState.latestSuccessfulAction
);

export const selectWorkflowGroupShowInitWaitingMessage = createSelector(
  selectWorkflowGroupState,
  workflowGroupState => workflowGroupState.showInitWaitingMessage
);

export const selectWorkflowGroupInStore = createSelector(
  selectWorkflowGroupState,
  workflowGroupState => {
    const items: EventWorkflowGroup[] = [];
    each(workflowGroupState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: EventWorkflowGroup[] = httpExtension.sortArray(
      items
    );
    return new QueryResultsModel(result);
  }
);
