// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { EventsState } from '../_reducers/event.reducers';
import { each } from 'lodash';
import { Event } from '../_models/event.model';

export const selectEventsState = createFeatureSelector<EventsState>('events');

export const selectEventById = (eventId: number) => createSelector(
  selectEventsState,
  eventsState => eventsState.entities[eventId]
);

export const selectEventsPageLoading = createSelector(
  selectEventsState,
  eventsState => {
    return eventsState.listLoading;
  }
);

export const selectEventsActionLoading = createSelector(
  selectEventsState,
  eventsState => eventsState.actionsloading
);

export const selectLastCreatedEventId = createSelector(
  selectEventsState,
  eventsState => eventsState.lastCreatedEventId
);

export const selectEventsPageLastQuery = createSelector(
  selectEventsState,
  eventsState => eventsState.lastQuery
);
export const selectEventError = createSelector(
  selectEventsState,
  eventsState =>  eventsState.error
);
export const selectEventLatestSuccessfullAction = createSelector(
  selectEventsState,
  eventsState =>  eventsState.latestSuccessfullAction
);
export const selectEventsInStore = createSelector(
  selectEventsState,
  eventsState => {
    const items: Event[] = [];
    each(eventsState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: Event[] = httpExtension.sortArray(items, eventsState.lastQuery.sortField, eventsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, eventsState.totalCount);
  }
);

export const selectEventsShowInitWaitingMessage = createSelector(
  selectEventsState,
  eventsState => eventsState.showInitWaitingMessage
);

export const selectHasEventsInStore = createSelector(
  selectEventsState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }

    return true;
  }
);
