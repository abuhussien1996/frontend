// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { WorkflowsState } from '../_reducers/workflow.reducers';
import { each } from 'lodash';
import { Workflow } from '../_models/workflow.model';

export const selectWorkflowsState = createFeatureSelector<WorkflowsState>('workflows');

export const selectWorkflowById = (workflowId: number) => createSelector(
    selectWorkflowsState,
    workflowsState => workflowsState.entities[workflowId]
);

export const selectWorkflowsPageLoading = createSelector(
    selectWorkflowsState,
    workflowsState => {
        return workflowsState.listLoading;
    }
);

export const selectWorkflowsActionLoading = createSelector(
    selectWorkflowsState,
    workflowsState => workflowsState.actionsloading
);

export const selectLastCreatedWorkflowId = createSelector(
    selectWorkflowsState,
    workflowsState => workflowsState.lastCreatedWorkflowId
);

export const selectWorkflowsPageLastQuery = createSelector(
    selectWorkflowsState,
    workflowsState => workflowsState.lastQuery
);

export const selectWorkflowError = createSelector(
    selectWorkflowsState,
    workflowsState =>  workflowsState.error
);

export const selectWorkflowLatestSuccessfullAction = createSelector(
    selectWorkflowsState,
    workflowsState =>  workflowsState.latestSuccessfullAction
);

export const selectWorkflowsInStore = createSelector(
    selectWorkflowsState,
    workflowsState => {
        const items: Workflow[] = [];
        each(workflowsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Workflow[] = httpExtension.sortArray(items, workflowsState.lastQuery.sortField, workflowsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, workflowsState.totalCount);
    }
);

export const selectWorkflowsShowInitWaitingMessage = createSelector(
    selectWorkflowsState,
    workflowsState => workflowsState.showInitWaitingMessage
);

export const selectHasWorkflowsInStore = createSelector(
    selectWorkflowsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }
        return true;
    }
);
