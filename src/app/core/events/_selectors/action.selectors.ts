// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { EventActionsState } from '../_reducers/action.reducers';
import { each } from 'lodash';
import { EventAction } from '../_models/action.model';

export const selectEventActionsState = createFeatureSelector<EventActionsState>('eventActions');

export const selectEventActionById = (eventActionId: number) => createSelector(
    selectEventActionsState,
    eventActionsState => eventActionsState.entities[eventActionId]
);

export const selectEventActionsPageLoading = createSelector(
    selectEventActionsState,
    eventActionsState => {
        return eventActionsState.listLoading;
    }
);

export const selectEventActionsActionLoading = createSelector(
    selectEventActionsState,
    eventActionsState => eventActionsState.actionsloading
);

export const selectLastCreatedEventActionId = createSelector(
    selectEventActionsState,
    eventActionsState => eventActionsState.lastCreatedEventActionId
);

export const selectEventActionsPageLastQuery = createSelector(
    selectEventActionsState,
    eventActionsState => eventActionsState.lastQuery
);
export const selectEventActionError = createSelector(
    selectEventActionsState,
    eventActionsState =>  eventActionsState.error
);
export const selectEventActionLatestSuccessfullAction = createSelector(
    selectEventActionsState,
    eventActionsState =>  eventActionsState.latestSuccessfullAction
);
export const selectEventActionsInStore = createSelector(
    selectEventActionsState,
    eventActionsState => {
        const items: EventAction[] = [];
        each(eventActionsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: EventAction[] = httpExtension.sortArray(
            items,
            eventActionsState.lastQuery.sortField,
            eventActionsState.lastQuery.sortOrder
            );
        return new QueryResultsModel(result, eventActionsState.totalCount);
    }
);

export const selectEventActionsShowInitWaitingMessage = createSelector(
    selectEventActionsState,
    eventActionsState => eventActionsState.showInitWaitingMessage
);

export const selectHasEventActionsInStore = createSelector(
    selectEventActionsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }
        return true;
    }
);
