import {createFeatureSelector, createSelector} from '@ngrx/store';
import {HttpExtenstionsModel, QueryResultsModel} from '../../_base/crud';
import {ApisState} from '../_reducers/api.reducers';
import {each} from 'lodash';
import {Api} from '../_models/api.model';

export const selectApisState = createFeatureSelector<ApisState>('apis');

export const selectApiById = (apiId: number) => createSelector(
  selectApisState,
  apisState => apisState.entities[apiId]
);

export const selectApisPageLoading = createSelector(
  selectApisState,
  apisState => {
    return apisState.listLoading;
  }
);

export const selectApisActionLoading = createSelector(
  selectApisState,
  apisState => apisState.actionsloading
);

export const selectLastCreatedApiId = createSelector(
  selectApisState,
  apisState => apisState.lastCreatedApiId
);

export const selectApisPageLastQuery = createSelector(
  selectApisState,
  apisState => apisState.lastQuery
);
export const selectApiError = createSelector(
  selectApisState,
  apisState => apisState.error
);
export const selectApiLatestSuccessfullAction = createSelector(
  selectApisState,
  apisState =>  apisState.latestSuccessfullAction
);
export const selectApisInStore = createSelector(
  selectApisState,
  apisState => {
    const items: Api[] = [];
    each(apisState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: Api[] = httpExtension.sortArray(items, apisState.lastQuery.sortField, apisState.lastQuery.sortOrder);
    return new QueryResultsModel(result, apisState.totalCount);
  }
);

export const selectApisShowInitWaitingMessage = createSelector(
  selectApisState,
  apisState => apisState.showInitWaitingMessage
);

export const selectHasApisInStore = createSelector(
  selectApisState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }

    return true;
  }
);
