import {createFeatureSelector, createSelector} from '@ngrx/store';
import {HttpExtenstionsModel, QueryResultsModel} from '../../_base/crud';
import {each} from 'lodash';
import {WorkflowActionsState} from '../_reducers/workflow-action.reducers';
import {WorkflowAction} from '../_models/workflow-action.model';

export const selectWorkflowActionsState = createFeatureSelector<WorkflowActionsState>('workflowActions');

export const selectWorkflowActionById = (workflowActionId: number) => createSelector(
  selectWorkflowActionsState,
  workflowActionsState => workflowActionsState.entities[workflowActionId]
);

export const selectWorkflowActionsPageLoading = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => {
    return workflowActionsState.listLoading;
  }
);

export const selectWorkflowActionsActionLoading = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => workflowActionsState.actionsloading
);

export const selectLastCreatedWorkflowActionId = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => workflowActionsState.lastCreatedWorkflowActionId
);

export const selectWorkflowActionsPageLastQuery = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => workflowActionsState.lastQuery
);
export const selectWorkflowActionError = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => workflowActionsState.error
);

export const selectWorkflowActionLatestSuccessfullAction = createSelector(
  selectWorkflowActionsState,
  workflowActionsState =>  workflowActionsState.latestSuccessfullAction
);

export const selectWorkflowActionsInStore = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => {
    const items: WorkflowAction[] = [];
    each(workflowActionsState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    // tslint:disable-next-line:max-line-length
    const result: WorkflowAction[] = httpExtension.sortArray(items, workflowActionsState.lastQuery.sortField, workflowActionsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, workflowActionsState.totalCount);
  }
);

export const selectWorkflowActionsShowInitWaitingMessage = createSelector(
  selectWorkflowActionsState,
  workflowActionsState => workflowActionsState.showInitWaitingMessage
);

export const selectHasWorkflowActionsInStore = createSelector(
  selectWorkflowActionsState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }
    return true;
  }
);
