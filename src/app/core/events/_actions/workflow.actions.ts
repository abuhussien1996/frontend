// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Workflow } from '../_models/workflow.model';
// Models
import { QueryParamsModel } from '../../_base/crud';
import { WorkflowActionsPageRequested } from './workflow-action.actions';

export enum WorkflowActionTypes {
    AllWorkflowsRequested = '[Workflows Module] All Workflows Requested',
    AllWorkflowsLoaded = '[Workflows API] All Workflows Loaded',
    WorkflowOnServerCreated = '[Edit Workflow Component] Workflow On Server Created',
    WorkflowCreated = '[Edit Workflow Dialog] Workflow Created',
    WorkflowOnServerUpdated = '[Edit Workflow Dialog] Workflow On Server Updated',
    WorkflowUpdated = '[Edit Workflow Dialog] Workflow Updated',
    WorkflowOnServerDeleted = '[Workflows List Page] Workflow On Server Deleted',
    WorkflowDeleted = '[Workflows List Page] Workflow Deleted',
    WorkflowsPageRequested = '[Workflows List Page] Workflows Page Requested',
    WorkflowsPageLoaded = '[Workflows API] Workflows Page Loaded',
    WorkflowsPageCancelled = '[Workflows API] Workflows Page Cancelled',
    WorkflowsPageToggleLoading = '[Workflows] Workflows Page Toggle Loading',
    WorkflowsActionToggleLoading = '[Workflows] Workflows Action Toggle Loading',
    ThrowWorkflowError = '[Workflows] Throw Error',
    FireWorkflowSuccess = '[Workflows] Fire Success'

}

export class WorkflowOnServerCreated implements Action {
    readonly type = WorkflowActionTypes.WorkflowOnServerCreated;
    constructor(public payload: { workflow: Workflow }) { }
}

export class WorkflowCreated implements Action {
    readonly type = WorkflowActionTypes.WorkflowCreated;
    constructor(public payload: { workflow: Workflow }) { }
}

export class ThrowWorkflowError implements Action {
    readonly type = WorkflowActionTypes.ThrowWorkflowError;

    constructor(public payload: { error: any }) {
    }
}

export class WorkflowOnServerUpdated implements Action {
    readonly type = WorkflowActionTypes.WorkflowOnServerUpdated;
    constructor(public payload: {
        partialWorkflow: Update<Workflow>,
        workflow: Workflow
    }) { }
}
export class WorkflowUpdated implements Action {
    readonly type = WorkflowActionTypes.WorkflowUpdated;
    constructor(public payload: {
        partialWorkflow: Update<Workflow>,
        workflow: Workflow
    }) { }
}

export class WorkflowOnServerDeleted implements Action {
    readonly type = WorkflowActionTypes.WorkflowOnServerDeleted;
    constructor(public payload: { id: number }) { }
}

export class WorkflowDeleted implements Action {
    readonly type = WorkflowActionTypes.WorkflowDeleted;
    constructor(public payload: { id: number }) { }
}

export class WorkflowsPageRequested implements Action {
    readonly type = WorkflowActionTypes.WorkflowsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) {
    }
}

export class WorkflowsPageLoaded implements Action {
    readonly type = WorkflowActionTypes.WorkflowsPageLoaded;
    constructor(public payload: { workflows: Workflow[], totalCount: number, page: QueryParamsModel }) { }
}


export class WorkflowsPageCancelled implements Action {
    readonly type = WorkflowActionTypes.WorkflowsPageCancelled;
}

export class WorkflowsPageToggleLoading implements Action {
    readonly type = WorkflowActionTypes.WorkflowsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class WorkflowsActionToggleLoading implements Action {
    readonly type = WorkflowActionTypes.WorkflowsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class FireWorkflowSuccess implements Action {
  readonly type = WorkflowActionTypes.FireWorkflowSuccess;
  constructor(public payload: {action_type: string}) {}
}


export type WorkflowActions = WorkflowCreated
    | WorkflowUpdated
    | WorkflowDeleted
    | WorkflowOnServerCreated
    | WorkflowOnServerUpdated
    | WorkflowOnServerDeleted
    | WorkflowsPageLoaded
    | WorkflowsPageCancelled
    | WorkflowsPageToggleLoading
    | WorkflowActionsPageRequested
    | ThrowWorkflowError
    | FireWorkflowSuccess
    | WorkflowsActionToggleLoading;
