// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Event } from '../_models/event.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum EventActionTypes {
  AllEventsRequested = '[Events Module] All Events Requested',
  AllEventsLoaded = '[Events API] All Events Loaded',
  EventOnServerCreated = '[Edit Event Component] Event On Server Created',
  EventCreated = '[Edit Event Dialog] Event Created',
  EventOnServerUpdated = '[Edit Event Dialog] Event On Server Updated',
  EventUpdated = '[Edit Event Dialog] Event Updated',
  EventOnServerDeleted = '[Events List Page] Event On Server Deleted',
  EventDeleted = '[Events List Page] Event Deleted',
  EventsPageRequested = '[Events List Page] Events Page Requested',
  EventsPageLoaded = '[Events API] Events Page Loaded',
  EventsPageCancelled = '[Events API] Events Page Cancelled',
  EventsPageToggleLoading = '[Events] Events Page Toggle Loading',
  EventsActionToggleLoading = '[Events] Events Action Toggle Loading',
  ThrowEventError = '[Events] Throw Error',
  FireEventSuccess = '[Events] Fire Success'
}

export class EventOnServerCreated implements Action {
  readonly type = EventActionTypes.EventOnServerCreated;
  constructor(public payload: { event: Event }) { }
}

export class EventCreated implements Action {
  readonly type = EventActionTypes.EventCreated;
  constructor(public payload: { event: Event }) { }
}

export class ThrowEventError implements Action {
  readonly type = EventActionTypes.ThrowEventError;
  constructor(public payload: { error: any }) { }
}

export class FireEventSuccess implements Action {
  readonly type = EventActionTypes.FireEventSuccess;
  constructor(public payload: {action_type: string}) {}
}

export class EventOnServerUpdated implements Action {
  readonly type = EventActionTypes.EventOnServerUpdated;
  constructor(public payload: {
    partialEvent: Update<Event>,
    event: Event
  }) { }
}
export class EventUpdated implements Action {
  readonly type = EventActionTypes.EventUpdated;
  constructor(public payload: {
    partialEvent: Update<Event>,
    event: Event
  }) { }
}

export class EventOnServerDeleted implements Action {
  readonly type = EventActionTypes.EventOnServerDeleted;
  constructor(public payload: { id: number }) { }
}

export class EventDeleted implements Action {
  readonly type = EventActionTypes.EventDeleted;
  constructor(public payload: { id: number }) { }
}

export class EventsPageRequested implements Action {
  readonly type = EventActionTypes.EventsPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {
  }
}

export class EventsPageLoaded implements Action {
  readonly type = EventActionTypes.EventsPageLoaded;
  constructor(public payload: { events: Event[], totalCount: number, page: QueryParamsModel }) { }
}

export class EventsPageCancelled implements Action {
  readonly type = EventActionTypes.EventsPageCancelled;
}

export class EventsPageToggleLoading implements Action {
  readonly type = EventActionTypes.EventsPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) { }
}

export class EventsActionToggleLoading implements Action {
  readonly type = EventActionTypes.EventsActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) { }
}

export type EventActions = EventCreated
  | EventUpdated
  | EventDeleted
  | EventOnServerCreated
  | EventOnServerUpdated
  | EventOnServerDeleted
  | EventsPageLoaded
  | EventsPageCancelled
  | EventsPageToggleLoading
  | EventsPageRequested
  | ThrowEventError
  | FireEventSuccess
  | EventsActionToggleLoading;
