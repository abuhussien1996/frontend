import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { WorkflowAction } from '../_models/workflow-action.model';
import { QueryParamsModel } from '../../_base/crud';

export enum WorkflowActionActionTypes {
  AllWorkflowActionsRequested = '[WorkflowActions Module] All WorkflowActions Requested',
  AllWorkflowActionsLoaded = '[WorkflowActions API] All WorkflowActions Loaded',
  WorkflowActionOnServerCreated = '[Edit WorkflowAction Component] WorkflowAction On Server Created',
  WorkflowActionCreated = '[Edit WorkflowAction Dialog] WorkflowAction Created',
  WorkflowActionOnServerUpdated = '[Edit WorkflowAction Dialog] WorkflowAction On Server Updated',
  WorkflowActionUpdated = '[Edit WorkflowAction Dialog] WorkflowAction Updated',
  WorkflowActionOnServerDeleted = '[WorkflowActions List Page] WorkflowAction On Server Deleted',
  WorkflowActionDeleted = '[WorkflowActions List Page] WorkflowAction Deleted',
  WorkflowActionsPageRequested = '[WorkflowActions List Page] WorkflowActions Page Requested',
  WorkflowActionsPageLoaded = '[WorkflowActions API] WorkflowActions Page Loaded',
  WorkflowActionsPageCancelled = '[WorkflowActions API] WorkflowActions Page Cancelled',
  WorkflowActionsPageToggleLoading = '[WorkflowActions] WorkflowActions Page Toggle Loading',
  WorkflowActionsActionToggleLoading = '[WorkflowActions] WorkflowActions Action Toggle Loading',
  ThrowWorkflowActionError = '[WorkflowActions] Throw Error',
  FireWorkflowActionSuccess = '[WorkflowActions] Fire Success',
  DeletedStatusUpdated = '[WorkflowActions] Workflow actions Status Updated',
  ParentUpdated = '[WorkflowActions] Workflow actions parent_id Updated',
  WorkflowActionMapUpdated = '[Edit WorkflowAction View Dialog] WorkflowAction Map Updated',
  WorkflowActionMapServerUpdated = '[Mapping API] WorkflowActions PUT Mapping Data'
}

export class WorkflowActionOnServerCreated implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionOnServerCreated;

  constructor(public payload: {
    workflowAction: WorkflowAction,
    workflow: number,
    company_id: number
  }) {
  }
}

export class WorkflowActionCreated implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionCreated;

  constructor(public payload: { workflowAction: WorkflowAction }) {
  }
}

export class ThrowWorkflowActionError implements Action {
  readonly type = WorkflowActionActionTypes.ThrowWorkflowActionError;

  constructor(public payload: { error: any }) {
  }
}

export class FireWorkflowActionSuccess implements Action {
  readonly type = WorkflowActionActionTypes.FireWorkflowActionSuccess;
  constructor(public payload: {action_type: string}) {}
}

export class WorkflowActionOnServerUpdated implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionOnServerUpdated;

  constructor(public payload: {
    partialWorkflowAction: Update<WorkflowAction>,
    workflowAction: WorkflowAction,
    workflow: number,
    company_id: number
  }) {
  }
}

export class WorkflowActionUpdated implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionUpdated;

  constructor(public payload: {
    partialWorkflowAction: Update<WorkflowAction>,
    workflowAction: WorkflowAction
  }) {
  }
}

export class WorkflowActionOnServerDeleted implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionOnServerDeleted;
  constructor(public payload: {
      id: number,
      workflowActions: WorkflowAction[],
      workflow_id: number,
      company_id: number
  }) { }
}

export class WorkflowActionDeleted implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionDeleted;

  constructor(public payload: { id: number }) {
  }
}

export class WorkflowActionsPageRequested implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionsPageRequested;

  constructor(public payload: {
    page: QueryParamsModel,
    workflow: number,
    company: number
  }) {
  }
}

export class WorkflowActionsPageLoaded implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionsPageLoaded;

  constructor(public payload: { workflowActions: WorkflowAction[], totalCount: number, page: QueryParamsModel }) {
  }
}


export class WorkflowActionsPageCancelled implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionsPageCancelled;
}

export class WorkflowActionsPageToggleLoading implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionsPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class WorkflowActionsActionToggleLoading implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionsActionToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}


export class DeletedStatusUpdated implements Action {
  readonly type = WorkflowActionActionTypes.DeletedStatusUpdated;

  constructor(public payload: { id: number, deleted: boolean }) {
  }
}


export class ParentUpdated implements Action {
  readonly type = WorkflowActionActionTypes.ParentUpdated;

  constructor(public payload: { id: number, parent_id: number }) {
  }
}

export class WorkflowActionMapServerUpdated implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionMapServerUpdated;

  constructor(public payload: { id: number, updatedObject: any, companyId: any }) {
  }
}

export class WorkflowActionMapUpdated implements Action {
  readonly type = WorkflowActionActionTypes.WorkflowActionMapUpdated;

  constructor(public payload: { id: number, updatedObject: any }) {
  }
}

export type WorkflowActionActions = WorkflowActionCreated
  | WorkflowActionMapServerUpdated
  | WorkflowActionMapUpdated
  | WorkflowActionUpdated
  | WorkflowActionDeleted
  | WorkflowActionOnServerCreated
  | WorkflowActionOnServerUpdated
  | WorkflowActionOnServerDeleted
  | WorkflowActionsPageLoaded
  | WorkflowActionsPageCancelled
  | WorkflowActionsPageToggleLoading
  | WorkflowActionsPageRequested
  | ThrowWorkflowActionError
  | FireWorkflowActionSuccess
  | WorkflowActionsActionToggleLoading
  | DeletedStatusUpdated
  | ParentUpdated;
