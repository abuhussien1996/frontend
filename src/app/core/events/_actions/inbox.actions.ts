// NGRX
import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { QueryParamsModel } from '../../_base/crud';
// CRUD
import { InboxItem } from '../_models/inbox.model';

export enum InboxActionTypes {
  InboxRequested = '[Inbox] Inbox Requested',
  InboxLoaded = '[Inbox] Inbox Loaded',
  InboxLoadFailed = '[Inbox] Inbox Load Failed',
  ItemWorkflowRequested = '[Inbox] Item Workflow Requested',
  ItemWorkflowLoaded = '[Inbox] Item Workflow Loaded',
  ItemOnServerUpdated = '[Inbox] Item On Server Updated',
  ItemUpdated = '[Inbox] Item Updated',
  AttachmentsRequested = '[Inbox] Attachments Requested',
  AttachmentsLoaded = '[Inbox] Attachments Loaded',
  StartWorkflow = '[Inbox] Start Workflow',
  UpdateCardValues = '[Inbox] Update Card Values',
  InboxListToggleLoading = '[Inbox] Inbox List Toggle Loading',
  InboxActionToggleLoading = '[Inbox] Inbox Action Toggle Loading',
  ThrowInboxError = '[Inbox] Throw Error',
  FireInboxSuccess = '[Inbox] Fire Success'
}

export class InboxRequested implements Action {
  readonly type = InboxActionTypes.InboxRequested;
  constructor(public payload: { page: QueryParamsModel }) { }
}

export class InboxLoaded implements Action {
  readonly type = InboxActionTypes.InboxLoaded;
  constructor(public payload: { entities: InboxItem[], totalCount: number, filterQuery: any }) { }
}

export class InboxLoadFailed implements Action {
  readonly type = InboxActionTypes.InboxLoadFailed;
}

export class ItemWorkflowRequested implements Action {
  readonly type = InboxActionTypes.ItemWorkflowRequested;
  constructor(public payload: { workflow_id: number, workflow_queue_id: number }) { }
}

export class ItemWorkflowLoaded implements Action {
  readonly type = InboxActionTypes.ItemWorkflowLoaded;
  constructor(public payload: { inboxWorkflow: any }) { }
}

export class ItemOnServerUpdated implements Action {
  readonly type = InboxActionTypes.ItemOnServerUpdated;
  constructor(public payload: {
    partialItem: Update<InboxItem>,
    item: InboxItem
  }) {}
}

export class ItemUpdated implements Action {
  readonly type = InboxActionTypes.ItemUpdated;
  constructor(public payload: {
    partialItem: Update<InboxItem>,
    item: InboxItem
  }) {}
}

export class AttachmentsRequested implements Action {
  readonly type = InboxActionTypes.AttachmentsRequested;
  constructor(public payload: { itemId: number, contactId: number }) { }
}

export class AttachmentsLoaded implements Action {
  readonly type = InboxActionTypes.AttachmentsLoaded;
  constructor(public payload: { attachments: any }) { }
}

export class StartWorkflow implements Action {
  readonly type = InboxActionTypes.StartWorkflow;
  constructor(public payload: {
    item: InboxItem,
    itemWorkflow: any,
    attachments: any[],
    submissionData: any,
    groupId: number,
    jump: boolean
  }) { }
}

export class UpdateCardValues implements Action {
  readonly type = InboxActionTypes.UpdateCardValues;
  constructor(public payload: {new_values: any}) {}
}

export class InboxListToggleLoading implements Action {
  readonly type = InboxActionTypes.InboxListToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class InboxActionToggleLoading implements Action {
  readonly type = InboxActionTypes.InboxActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ThrowInboxError implements Action {
  readonly type = InboxActionTypes.ThrowInboxError;
  constructor(public payload: { error: any }) { }
}

export class FireInboxSuccess implements Action {
  readonly type = InboxActionTypes.FireInboxSuccess;
  constructor(public payload: {action_type: string}) {}
}

export type InboxActions = InboxRequested
  | InboxLoaded
  | InboxLoadFailed
  | ItemWorkflowRequested
  | ItemWorkflowLoaded
  | ItemOnServerUpdated
  | ItemUpdated
  | AttachmentsRequested
  | AttachmentsLoaded
  | StartWorkflow
  | UpdateCardValues
  | InboxListToggleLoading
  | InboxActionToggleLoading
  | ThrowInboxError
  | FireInboxSuccess;
