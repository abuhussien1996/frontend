// NGRX
import {Action} from '@ngrx/store';
// CRUD
// Models
import {EventAction, EventActionActionTypes, WorkflowAction, WorkflowActionActionTypes, WorkflowActionCreated} from '..';
import {EventWorkflowGroup} from '../_models/workflow-group.model';
import {UpdateNum} from '@ngrx/entity/src/models';

export enum WorkflowGroupTypes {
  WorkflowGroupPageToggleLoading = '[workflowGroup] workflowGroup Page Toggle Loading',
  WorkflowGroupToggleLoading = '[workflowGroup] workflowGroup Action Toggle Loading',
  WorkflowGroupPageLoaded = '[workflowGroup] workflowGroup Page Loaded',
  WorkflowGroupPageRequested = '[workflowGroup] workflowGroup Page Requested',
  AllWorkflowGroupLoaded = '[workflowGroup API] All WorkflowActions Loaded',
  FireWorkflowGroupSuccess = '[workflowGroup] Fire Success',
  WorkflowGroupCreated = '[Create workflowGroup] workflowGroup Created',
  WorkflowGroupUpdate = '[Update workflowGroup] workflowGroup Update',
  WorkflowGroupUpdated = '[Updated workflowGroup edit] edit workflowGroup Updated',
  WorkflowGroupCreate = '[Edit workflowGroup create] workflowGroup Created',
  WorkflowGroupOnServerDeleted = '[workflowGroup List Page] workflowGroup Server Deleted',
  WorkflowGroupDeleted = '[workflowGroup List Page] workflowGroup Deleted',

  AllWorkflowActionsRequested = '[WorkflowActions Module] All WorkflowActions Requested',
  WorkflowActionUpdated = '[Edit WorkflowAction Dialog] WorkflowAction Updated',
  WorkflowActionsPageRequested = '[WorkflowActions List Page] WorkflowActions Page Requested',
  WorkflowActionsPageCancelled = '[WorkflowActions API] WorkflowActions Page Cancelled',
  ThrowWorkflowActionError = '[WorkflowActions] Throw Error',
  DeletedStatusUpdated = '[WorkflowActions] Workflow actions Status Updated',
  WorkflowActionMapUpdated = '[Edit WorkflowAction View Dialog] WorkflowAction Map Updated',
  WorkflowActionMapServerUpdated = '[Mapping API] WorkflowActions PUT Mapping Data'
}

export class WorkflowGroupPageToggleLoading implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class WorkflowGroupToggleLoading implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class WorkflowGroupPageLoaded implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupPageLoaded;

  constructor(public payload: { workflowGroup: EventWorkflowGroup[] }) {
  }
}

export class WorkflowGroupPageRequested implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupPageRequested;

  constructor() {
  }
}

export class WorkflowGroupCreated implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupCreated;

  constructor(public payload: { workflowGroup: EventWorkflowGroup }) {
  }
}

export class WorkflowGroupUpdate implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupUpdate;

  constructor(public payload: { partialEventAction: UpdateNum<EventWorkflowGroup>; workflowGroup: EventWorkflowGroup }) {
  }
}

export class WorkflowGroupCreate implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupCreate;
  constructor(public payload: { workflowGroup: EventWorkflowGroup }) { }
}

export class WorkflowGroupUpdated implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupUpdated;

  constructor(public payload: { partialEventAction: UpdateNum<EventWorkflowGroup>; workflowGroup: EventWorkflowGroup }) {
  }
}

export class FireWorkflowGroupSuccess implements Action {
  readonly type = WorkflowGroupTypes.FireWorkflowGroupSuccess;

  constructor(public payload: { action_type: string }) {
  }
}

export class WorkflowGroupOnServerDeleted implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupOnServerDeleted;
  constructor(public payload: {
    workflowGroupId: number,
  }) { }
}

export class WorkflowGroupDeleted implements Action {
  readonly type = WorkflowGroupTypes.WorkflowGroupDeleted;
  constructor(public payload: { id: number }) {
  }
}


export type WorkflowGroupActions = WorkflowActionCreated
  | WorkflowGroupPageToggleLoading
  | WorkflowGroupToggleLoading
  | WorkflowGroupPageLoaded
  | WorkflowGroupPageRequested
  | FireWorkflowGroupSuccess
  | WorkflowGroupUpdated
  | WorkflowGroupUpdate
  | WorkflowGroupCreated
  | WorkflowGroupDeleted
