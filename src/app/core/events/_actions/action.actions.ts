// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { EventAction } from '../_models/action.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum EventActionActionTypes {
    EventActionOnServerCreated = '[Edit Event Action Dialog] EventAction On Server Created',
    EventActionCreated = '[Edit Event Action Dialog] Event Action Created',
    EventActionOnServerUpdated = '[Edit Event Action Dialog] Event Action On Server Updated',
    EventActionUpdated = '[Edit Event Action Dialog] Event Action Updated',
    EventActionsPageRequested = '[Event Actions List Page] Event Actions Page Requested',
    EventActionsPageLoaded = '[Event Actions API] Event Actions Page Loaded',
    EventActionsPageCancelled = '[Event Actions API] Event Actions Page Cancelled',
    EventActionsPageToggleLoading = '[Event Actions] Event Actions Page Toggle Loading',
    EventActionsActionToggleLoading = '[Event Actions] Event Actions Action Toggle Loading',
    ThrowEventActionError = '[Event Actions] Throw Error',
    FireEventActionSuccess = '[Event Actions] Fire Success'

}

export class EventActionOnServerCreated implements Action {
    readonly type = EventActionActionTypes.EventActionOnServerCreated;
    constructor(public payload: { eventAction: EventAction }) { }
}

export class EventActionCreated implements Action {
    readonly type = EventActionActionTypes.EventActionCreated;
    constructor(public payload: { eventAction: EventAction }) { }
}

export class ThrowEventActionError implements Action {
    readonly type = EventActionActionTypes.ThrowEventActionError;

    constructor(public payload: { error: any }) { }
}

export class FireEventActionSuccess implements Action {
    readonly type = EventActionActionTypes.FireEventActionSuccess;
    constructor(public payload: {
        action_type: string
    }) { }
}
export class EventActionOnServerUpdated implements Action {
    readonly type = EventActionActionTypes.EventActionOnServerUpdated;
    constructor(public payload: {
        partialEventAction: Update<EventAction>,
        eventAction: EventAction
    }) { }
}
export class EventActionUpdated implements Action {
    readonly type = EventActionActionTypes.EventActionUpdated;
    constructor(public payload: {
        partialEventAction: Update<EventAction>,
        eventAction: EventAction
    }) { }
}

export class EventActionsPageRequested implements Action {
    readonly type = EventActionActionTypes.EventActionsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) {
    }
}

export class EventActionsPageLoaded implements Action {
    readonly type = EventActionActionTypes.EventActionsPageLoaded;
    constructor(public payload: { eventActions: EventAction[], totalCount: number, page: QueryParamsModel }) { }
}


export class EventActionsPageCancelled implements Action {
    readonly type = EventActionActionTypes.EventActionsPageCancelled;
}

export class EventActionsPageToggleLoading implements Action {
    readonly type = EventActionActionTypes.EventActionsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class EventActionsActionToggleLoading implements Action {
    readonly type = EventActionActionTypes.EventActionsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}


export type EventActionActions = EventActionCreated
    | EventActionUpdated
    | EventActionOnServerCreated
    | EventActionOnServerUpdated
    | EventActionsPageLoaded
    | EventActionsPageCancelled
    | EventActionsPageToggleLoading
    | EventActionsPageRequested
    | ThrowEventActionError
    | FireEventActionSuccess
    | EventActionsActionToggleLoading;
