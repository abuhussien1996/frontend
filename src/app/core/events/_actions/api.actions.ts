import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {Api} from '../_models/api.model';
import {QueryParamsModel} from '../../_base/crud';

export enum ApiActionTypes {
  AllApisRequested = '[Apis Module] All Apis Requested',
  AllApisLoaded = '[Apis API] All Apis Loaded',
  ApiOnServerCreated = '[Edit Api Component] Api On Server Created',
  ApiCreated = '[Edit Api Dialog] Api Created',
  ApiOnServerUpdated = '[Edit Api Dialog] Api On Server Updated',
  ApiUpdated = '[Edit Api Dialog] Api Updated',
  ApisPageRequested = '[Apis List Page] Apis Page Requested',
  ApisPageLoaded = '[Apis API] Apis Page Loaded',
  ApisPageCancelled = '[Apis API] Apis Page Cancelled',
  ApisPageToggleLoading = '[Apis] Apis Page Toggle Loading',
  ApisActionToggleLoading = '[Apis] Apis Action Toggle Loading',
  ThrowApiError = '[Apis] Throw Error',
  FireApiSuccess = '[Apis] Fire Success'
}

export class ApiOnServerCreated implements Action {
  readonly type = ApiActionTypes.ApiOnServerCreated;
  constructor(public payload: { api: Api }) {}
}

export class ApiCreated implements Action {
  readonly type = ApiActionTypes.ApiCreated;
  constructor(public payload: { api: Api }) {}
}

export class ThrowApiError implements Action {
  readonly type = ApiActionTypes.ThrowApiError;
  constructor(public payload: { error: any }) {}
}

export class FireApiSuccess implements Action {
  readonly type = ApiActionTypes.FireApiSuccess;
  constructor(public payload: {action_type: string}) {}
}

export class ApiOnServerUpdated implements Action {
  readonly type = ApiActionTypes.ApiOnServerUpdated;
  constructor(public payload: {
    partialApi: Update<Api>,
    api: Api
  }) {}
}

export class ApiUpdated implements Action {
  readonly type = ApiActionTypes.ApiUpdated;
  constructor(public payload: {
    partialApi: Update<Api>,
    api: Api
  }) {}
}

export class ApisPageRequested implements Action {
  readonly type = ApiActionTypes.ApisPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class ApisPageLoaded implements Action {
  readonly type = ApiActionTypes.ApisPageLoaded;
  constructor(public payload: { apis: Api[], totalCount: number, page: QueryParamsModel }) {}
}


export class ApisPageCancelled implements Action {
  readonly type = ApiActionTypes.ApisPageCancelled;
}

export class ApisPageToggleLoading implements Action {
  readonly type = ApiActionTypes.ApisPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ApisActionToggleLoading implements Action {
  readonly type = ApiActionTypes.ApisActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}


export type ApiActions =
  ApiCreated
  | ApiUpdated
  | ApiOnServerCreated
  | ApiOnServerUpdated
  | ApisPageLoaded
  | ApisPageCancelled
  | ApisPageToggleLoading
  | ApisPageRequested
  | ThrowApiError
  | FireApiSuccess
  | ApisActionToggleLoading;
