import { Injectable } from '@angular/core';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import {
  ApiActionTypes,
  ApiCreated,
  ApiOnServerCreated,
  ApiOnServerUpdated,
  ApisActionToggleLoading,
  ApisPageCancelled,
  ApisPageLoaded,
  ApisPageRequested,
  ApisPageToggleLoading,
  ApiUpdated,
  FireApiSuccess,
  ThrowApiError,
} from '../_actions/api.actions';
import { Router } from '@angular/router';
import { ApiService } from '../_services';
import { AuthService } from '../../auth/_services';

@Injectable()
export class ApiEffects {
  showPageLoadingDistpatcher = new ApisPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new ApisPageToggleLoading({ isLoading: false });
  showActionLoadingDistpatcher = new ApisActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new ApisActionToggleLoading({ isLoading: false });

  @Effect()
  loadApisPage$ = this.actions$
    .pipe(
      ofType<ApisPageRequested>(ApiActionTypes.ApisPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.api.findApis(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin([requestToServer, lastQuery]).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 401) {
                this.authService.logout();
                return new ApisPageCancelled();
              } else if (response[0].status === 403) {
                this.router.navigateByUrl('/error/403');
                return new ApisPageCancelled();
              } else {
                this.store.dispatch(new ThrowApiError({ error: response[0].error }));
                return new ApisPageCancelled();
              }
            } else {
              const result = response[0];
              return new ApisPageLoaded({
                apis: result.data,
                totalCount: result.count,
                page: response[1]
              });
            }
          })
        );
      })
    );

  @Effect()
  updateApi$ = this.actions$
    .pipe(
      ofType<ApiOnServerUpdated>(ApiActionTypes.ApiOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.api.updateApi(payload.api).pipe(
          catchError(error => {
            return of({ error });
          }),
          map((res) => {
            if (res.error) {
              return new ThrowApiError({ error: res.error });
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireApiSuccess({ action_type: ApiActionTypes.ApiUpdated }));
              return new ApiUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createApi$ = this.actions$
    .pipe(
      ofType<ApiOnServerCreated>(ApiActionTypes.ApiOnServerCreated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.api.createApi(payload.api).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if (res.status === 201) {
              this.store.dispatch(new FireApiSuccess({ action_type: ApiActionTypes.ApiCreated }));
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new ApiCreated({ api: res.body.data })
            } else {
              return new ThrowApiError({ error: res });
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private api: ApiService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
  ) {
  }
}
