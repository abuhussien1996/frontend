import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {ThrowApiError} from '..';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {WorkflowGroupService} from '../_services/workflow-group.service';
import {
  WorkflowGroupCreate,
  WorkflowGroupCreated,
  WorkflowGroupDeleted,
  WorkflowGroupOnServerDeleted,
  WorkflowGroupPageLoaded,
  WorkflowGroupPageRequested,
  WorkflowGroupPageToggleLoading,
  WorkflowGroupToggleLoading,
  WorkflowGroupTypes,
  WorkflowGroupUpdate,
  WorkflowGroupUpdated
} from '../_actions/workflow-group.action';

@Injectable()
// tslint:disable-next-line:class-name
export class workflowGroupEffects {

  showPageLoadingDispatcher = new WorkflowGroupPageToggleLoading({isLoading: true});
  hidePageLoadingDispatcher = new WorkflowGroupPageToggleLoading({isLoading: false});
  showActionLoadingDispatcher = new WorkflowGroupToggleLoading({isLoading: true});
  hideActionLoadingDispatcher = new WorkflowGroupToggleLoading({isLoading: false});

  @Effect()
  loadWorkflowGroup$ = this.actions$
    .pipe(
      ofType<WorkflowGroupPageRequested>(WorkflowGroupTypes.WorkflowGroupPageRequested),
      mergeMap(() => {
        this.store.dispatch(this.showPageLoadingDispatcher);
        return this.workflowGroup.getAllWorkflowGroup().pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowApiError({error: res.error});
            } else {
              const result = res;
              return new WorkflowGroupPageLoaded({
                workflowGroup: result.data
              });
            }
          })
        );
      })
    );

  @Effect()
  createWorkflowGroup$ = this.actions$
    .pipe(
      ofType<WorkflowGroupCreate>(WorkflowGroupTypes.WorkflowGroupCreate),
      mergeMap(({payload}) => {
        return this.workflowGroup.createWorkflowGroup(payload.workflowGroup).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowApiError({error: res.error});
            } else {
              return new WorkflowGroupCreated({workflowGroup: res.data})
            }
          })
        );
      })
    );

  @Effect()
  updateWorkflowGroup$ = this.actions$
    .pipe(
      ofType<WorkflowGroupUpdate>(WorkflowGroupTypes.WorkflowGroupUpdate),
      mergeMap(({payload}) => {
        return this.workflowGroup.updateWorkflowGroup(payload.workflowGroup).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowApiError({error: res.error});
            } else {
              return new WorkflowGroupUpdated({partialEventAction: payload.partialEventAction, workflowGroup: res.data})
            }
          })
        );
      })
    );

  @Effect()
  deleteWorkflowGroup$ = this.actions$
    .pipe(
      ofType<WorkflowGroupOnServerDeleted>(WorkflowGroupTypes.WorkflowGroupOnServerDeleted),
      mergeMap(({payload}) => {
          return this.workflowGroup.deleteWorkflowGroup(payload.workflowGroupId).pipe(
            catchError(error => {
              return of({error});
            }),
            map(() => {
              // @ts-ignore
              return new WorkflowGroupDeleted({id: payload.workflowGroupId})
            })
          );
        }
      ),
    );

  constructor(
    private actions$: Actions,
    private workflowGroup: WorkflowGroupService,
    private store: Store<AppState>,
  ) {
  }
}
