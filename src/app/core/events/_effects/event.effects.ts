import { Injectable } from '@angular/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import {
  EventActionTypes,
  EventsPageRequested,
  EventsPageLoaded,
  EventCreated,
  EventDeleted,
  EventUpdated,
  EventOnServerCreated,
  EventsActionToggleLoading,
  EventsPageToggleLoading,
  ThrowEventError,
  FireEventSuccess,
  EventOnServerUpdated,
  EventsPageCancelled,
  EventOnServerDeleted
} from '../_actions/event.actions';
import { Router } from '@angular/router';
import { EventService } from '../_services';
import { AuthService } from '../../auth/_services';

@Injectable()
export class EventEffects {
  showPageLoadingDistpatcher = new EventsPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new EventsPageToggleLoading({ isLoading: false });
  showActionLoadingDistpatcher = new EventsActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new EventsActionToggleLoading({ isLoading: false });

  @Effect()
  loadEventsPage$ = this.actions$
    .pipe(
      ofType<EventsPageRequested>(EventActionTypes.EventsPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.event.findEvents(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin([requestToServer, lastQuery]).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if(response[0].error) {
              if(response[0].status === 401) {
                this.authService.logout();
                return new EventsPageCancelled();
              } else if(response[0].status === 403) {
                this.router.navigateByUrl('/error/403');
                return new EventsPageCancelled();
              } else {
                this.store.dispatch(new ThrowEventError({error: response[0].error}));
                return new EventsPageCancelled();
              }
            } else {
              const result = response[0];
              return new EventsPageLoaded({
                events: result.data,
                totalCount: result.count,
                page: response[1]
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteEvent$ = this.actions$
    .pipe(
      ofType<EventOnServerDeleted>(EventActionTypes.EventOnServerDeleted),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.event.deleteEvent(payload.id).pipe(
          catchError(error => {
            return of({error});
          }),
          map(() => {
            this.store.dispatch(this.hideActionLoadingDistpatcher);
            this.store.dispatch(new FireEventSuccess({action_type: EventActionTypes.EventDeleted}));
            return new EventDeleted(payload)
          })
        );
        }
      ),
    );

  @Effect()
  updateEvent$ = this.actions$
    .pipe(
      ofType<EventOnServerUpdated>(EventActionTypes.EventOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.event.updateEvent(payload.event).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowEventError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireEventSuccess({action_type: EventActionTypes.EventUpdated}));
              return new EventUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createEvent$ = this.actions$
    .pipe(
      ofType<EventOnServerCreated>(EventActionTypes.EventOnServerCreated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.event.createEvent(payload.event).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(res.status === 201) {
              this.store.dispatch(new FireEventSuccess({action_type: EventActionTypes.EventCreated}));
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new EventCreated({event: res.body.data})
            } else {
              return new ThrowEventError({error: res});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private event: EventService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
  ) {
  }
}
