import {Injectable} from '@angular/core';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {forkJoin, of} from 'rxjs';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AppState} from '../../../core/reducers';
import {
  FireWorkflowActionSuccess,
  ThrowWorkflowActionError,
  WorkflowActionActionTypes,
  WorkflowActionCreated,
  WorkflowActionDeleted,
  WorkflowActionMapServerUpdated,
  WorkflowActionMapUpdated,
  WorkflowActionOnServerCreated,
  WorkflowActionOnServerDeleted,
  WorkflowActionOnServerUpdated,
  WorkflowActionsActionToggleLoading,
  WorkflowActionsPageCancelled,
  WorkflowActionsPageLoaded,
  WorkflowActionsPageRequested,
  WorkflowActionsPageToggleLoading,
  WorkflowActionUpdated,
} from '../_actions/workflow-action.actions';
import {Router} from '@angular/router';
import {WorkflowService} from '../_services/workflow.service';
import {AuthService} from '../../auth/_services';
import {WorkflowActionTypes} from '../_actions/workflow.actions';

@Injectable()
export class WorkflowActionEffects {
  showPageLoadingDistpatcher = new WorkflowActionsPageToggleLoading({isLoading: true});
  hidePageLoadingDistpatcher = new WorkflowActionsPageToggleLoading({isLoading: false});
  showActionLoadingDistpatcher = new WorkflowActionsActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new WorkflowActionsActionToggleLoading({isLoading: false});

  @Effect()
  loadWorkflowActionsPage$ = this.actions$
    .pipe(
      ofType<WorkflowActionsPageRequested>(WorkflowActionActionTypes.WorkflowActionsPageRequested),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.workflow.findWorkflowActions(payload.page, payload.workflow, payload.company);
        const lastQuery = of(payload.page);
        return forkJoin([requestToServer, lastQuery]).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 401) {
                this.authService.logout();
                return new WorkflowActionsPageCancelled();
              } else if (response[0].status === 403) {
                this.router.navigateByUrl('/error/403');
                return new WorkflowActionsPageCancelled();
              } else {
                this.store.dispatch(new ThrowWorkflowActionError({error: response[0].error}));
                return new WorkflowActionsPageCancelled();
              }
            } else {
              const result = response[0];
              return new WorkflowActionsPageLoaded({
                workflowActions: result.data,
                totalCount: result.count,
                page: response[1]
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteWorkflowAction$ = this.actions$
    .pipe(
      ofType<WorkflowActionOnServerDeleted>(WorkflowActionActionTypes.WorkflowActionOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.workflow.updateWorkflowActionDeleted(
            payload.workflowActions,
            payload.workflow_id,
            payload.company_id
          ).pipe(
            catchError(error => {
              return of({error});
            }),
            map((res: Response) => {
              if (res && res.status === 200) {
                this.store.dispatch(this.hideActionLoadingDistpatcher);
                this.store.dispatch(new FireWorkflowActionSuccess({action_type: WorkflowActionActionTypes.WorkflowActionDeleted}));
                return new WorkflowActionDeleted({id: payload.id})
              } else {
                return new ThrowWorkflowActionError({error: 'DELETE_ERROR'});
              }
            })
          );
        }
      ),
    );

  @Effect()
  updateWorkflowAction$ = this.actions$
    .pipe(
      ofType<WorkflowActionOnServerUpdated>(WorkflowActionActionTypes.WorkflowActionOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.workflow.updateWorkflowAction(payload.workflowAction, payload.workflow, payload.company_id).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowWorkflowActionError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireWorkflowActionSuccess({action_type: WorkflowActionTypes.WorkflowUpdated}));
              return new WorkflowActionUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createWorkflowAction$ = this.actions$
    .pipe(
      ofType<WorkflowActionOnServerCreated>(WorkflowActionActionTypes.WorkflowActionOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.workflow.createWorkflowAction(payload.workflowAction, payload.workflow, payload.company_id).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            console.log(res.status);
            if (res.status === 201) {
              this.store.dispatch(new FireWorkflowActionSuccess({action_type: WorkflowActionTypes.WorkflowCreated}));
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new WorkflowActionCreated({workflowAction: res.body.data})
            } else {
              return new ThrowWorkflowActionError({error: res});
            }
          })
        )
      }),
    );

  @Effect()
  updateMapWorkflow$ = this.actions$
    .pipe(
      ofType<WorkflowActionMapServerUpdated>(WorkflowActionActionTypes.WorkflowActionMapServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.workflow.mappingWorkflowAction(payload.updatedObject, payload.id, payload.companyId).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowWorkflowActionError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireWorkflowActionSuccess({action_type: WorkflowActionActionTypes.WorkflowActionMapUpdated}));
              return new WorkflowActionMapUpdated(payload)
            }
          })
        );
      }),
    );

  constructor(
    private actions$: Actions,
    private workflow: WorkflowService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
  ) {
  }
}
