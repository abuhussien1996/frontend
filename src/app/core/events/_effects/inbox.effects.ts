import { Injectable } from '@angular/core';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import {
    InboxLoaded,
    ItemWorkflowRequested,
    ItemWorkflowLoaded,
    ItemUpdated,
    AttachmentsRequested,
    AttachmentsLoaded,
    StartWorkflow,
    InboxListToggleLoading,
    InboxActionToggleLoading,
    ThrowInboxError,
    FireInboxSuccess,
    InboxActionTypes,
    InboxRequested,
    InboxLoadFailed,
    UpdateCardValues,
} from '../_actions/inbox.actions';
import { Router } from '@angular/router';
import { InboxService } from '../_services';
import { AuthService } from '../../auth/_services';
import { Update } from '@ngrx/entity';
import { InboxItem } from '../_models/inbox.model';

@Injectable()
export class InboxEffects {
    showPageLoadingDistpatcher = new InboxListToggleLoading({ isLoading: true });
    hidePageLoadingDistpatcher = new InboxListToggleLoading({ isLoading: false });
    showActionLoadingDistpatcher = new InboxActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new InboxActionToggleLoading({ isLoading: false });

    @Effect()
    loadInbox$ = this.actions$
        .pipe(
            ofType<InboxRequested>(InboxActionTypes.InboxRequested),
            mergeMap(({ payload }) => {
                this.store.dispatch(this.showPageLoadingDistpatcher);
                const requestToServer = this.inboxService.loadInbox(payload.page);
                const lastQuery = of(payload.page);
                return forkJoin([requestToServer, lastQuery]).pipe(
                    catchError(error => {
                        return of([error]);
                    }),
                    map(response => {
                        if (response[0].error) {
                            if (response[0].status === 401) {
                                this.authService.logout();
                                this.store.dispatch(this.hidePageLoadingDistpatcher);
                                return this.store.dispatch(new InboxLoadFailed());
                            } else if (response[0].status === 403) {
                                this.router.navigateByUrl('/error/403');
                                this.store.dispatch(this.hidePageLoadingDistpatcher);
                                return this.store.dispatch(new InboxLoadFailed());
                            } else {
                                this.store.dispatch(new ThrowInboxError({ error: response[0].error }));
                                this.store.dispatch(this.hidePageLoadingDistpatcher);
                                return new InboxLoadFailed();
                            }
                        } else {
                            const result = response[0];
                            return new InboxLoaded({
                                entities: result.data,
                                totalCount: result.count,
                                filterQuery: response[1]
                            });
                        }
                    })
                );
            })
        );

    @Effect()
    loadItemWorkflow$ = this.actions$
        .pipe(
            ofType<ItemWorkflowRequested>(InboxActionTypes.ItemWorkflowRequested),
            mergeMap(({ payload }) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.inboxService.getWorkflow(payload.workflow_id, payload.workflow_queue_id).pipe(
                    catchError(error => {
                        return of({ error });
                    }),
                    map((res) => {
                        if (res.error) {
                            return new ThrowInboxError({ error: res.error });
                        } else {
                            this.store.dispatch(this.hideActionLoadingDistpatcher);
                            this.store.dispatch(new FireInboxSuccess({ action_type: InboxActionTypes.ItemWorkflowLoaded }));
                            return new ItemWorkflowLoaded({inboxWorkflow: res.data})
                        }
                    })
                );
            }),
        );

    @Effect()
    loadAttachments$ = this.actions$
        .pipe(
            ofType<AttachmentsRequested>(InboxActionTypes.AttachmentsRequested),
            mergeMap(({ payload }) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.inboxService.getAttachments(payload.contactId, payload.itemId).pipe(
                    catchError(error => {
                        return of({ error });
                    }),
                    map((res) => {
                        if (res.error) {
                            return new ThrowInboxError({ error: res.error });
                        } else {
                            this.store.dispatch(this.hideActionLoadingDistpatcher);
                            this.store.dispatch(new FireInboxSuccess({ action_type: InboxActionTypes.AttachmentsLoaded }));
                            return new AttachmentsLoaded({attachments: res.data})
                        }
                    })
                );
            }),
        );

    @Effect()
    startWorkflow$ = this.actions$
        .pipe(
            ofType<StartWorkflow>(InboxActionTypes.StartWorkflow),
            mergeMap(({ payload }) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.inboxService.startWorkflow(payload.item, payload.itemWorkflow, payload.submissionData).pipe(
                    catchError(error => {
                        return of(error);
                    }),
                    mergeMap((res) => {
                    if (res.data) {
                        const observables = [];
                        const newItem = {
                            ...payload.item,
                            workflow_id: payload.itemWorkflow.workflow_id,
                            workflow_queue_id: res.data.workflow_queue_id
                        }
                        observables.push(this.inboxService.updateItem(newItem));
                        observables.push(this.inboxService.postAttachments(res.data.workflow_queue_id, payload.attachments));
                        for(const api of payload.itemWorkflow.apis) {
                          observables.push(this.inboxService.executeApi(
                            api.http_method,
                            api.url,
                            api.payload,
                            JSON.parse(payload.submissionData),
                            res.data.workflow_queue_id
                          ));
                        }
                        return forkJoin(observables).pipe(
                            catchError(error => {
                                return of([error]);
                            }),
                            map(response => {
                                const errorArray = [];
                                for(const r in response) {
                                    if (r[`error`]) {
                                       errorArray.push(r[`error`].errors);
                                    }
                                }
                                if (errorArray.length > 0) {
                                    this.store.dispatch(this.hidePageLoadingDistpatcher);
                                    return new ThrowInboxError({ error: errorArray });
                                } else {
                                    const updateItem: Update<InboxItem> = {
                                        id: newItem.id,
                                        changes: newItem
                                    };
                                    this.store.dispatch(this.hideActionLoadingDistpatcher);
                                    this.store.dispatch(new FireInboxSuccess({ action_type: InboxActionTypes.StartWorkflow }));
                                    this.store.dispatch(new ItemUpdated({
                                        item: newItem,
                                        partialItem: updateItem
                                    }));
                                    if(payload.jump) {
                                        this.store.dispatch( new UpdateCardValues({new_values: {
                                            group_id: payload.groupId,
                                            workflow_queue_id: res.data.workflow_queue_id,
                                            workflow_action_id: payload.itemWorkflow.id
                                        }}));
                                    }
                                    return this.hideActionLoadingDistpatcher
                                }
                            })
                        );
                    } else {
                        return of(new ThrowInboxError({ error: res.error.errors }))
                    }
                }))
            },
            ));

    constructor(
        private actions$: Actions,
        private inboxService: InboxService,
        private store: Store<AppState>,
        private router: Router,
        private authService: AuthService
    ) {}
}
