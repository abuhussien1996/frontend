import { Injectable } from '@angular/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { AppState } from '../../../core/reducers';
import {
  EventActionActionTypes,
  EventActionsPageRequested,
  EventActionsPageLoaded,
  EventActionCreated,
  EventActionUpdated,
  EventActionOnServerCreated,
  EventActionsActionToggleLoading,
  EventActionsPageToggleLoading,
  ThrowEventActionError,
  FireEventActionSuccess,
  EventActionOnServerUpdated,
  EventActionsPageCancelled
} from '../_actions/action.actions';
import { Router } from '@angular/router';
import { EventActionService } from '../_services';
import { AuthService } from '../../auth/_services';

@Injectable()
export class EventActionEffects {
  showPageLoadingDistpatcher = new EventActionsPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new EventActionsPageToggleLoading({ isLoading: false });
  showActionLoadingDistpatcher = new EventActionsActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new EventActionsActionToggleLoading({ isLoading: false });

  @Effect()
  loadEventActionsPage$ = this.actions$
    .pipe(
      ofType<EventActionsPageRequested>(EventActionActionTypes.EventActionsPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.eventAction.findEventActions(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin([requestToServer, lastQuery]).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if(response[0].error) {
              if(response[0].status === 401) {
                this.authService.logout();
                return new EventActionsPageCancelled();
              }
              else if(response[0].status === 403) {
                this.router.navigateByUrl('/error/403');
                return new EventActionsPageCancelled();
              } else {
                this.store.dispatch(new ThrowEventActionError({error: response[0].error}));
                return new EventActionsPageCancelled();
              }
            } else {
              const result = response[0];
              return new EventActionsPageLoaded({
                eventActions: result.data,
                totalCount: result.count,
                page: response[1]
              });
            }
          })
        );
      })
    );

  @Effect()
  updateEventAction$ = this.actions$
    .pipe(
      ofType<EventActionOnServerUpdated>(EventActionActionTypes.EventActionOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.eventAction.updateEventAction(payload.eventAction).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowEventActionError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireEventActionSuccess({action_type: EventActionActionTypes.EventActionUpdated}));
              return new EventActionUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createEventAction$ = this.actions$
    .pipe(
      ofType<EventActionOnServerCreated>(EventActionActionTypes.EventActionOnServerCreated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.eventAction.createEventAction(payload.eventAction).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if(res.status === 201) {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireEventActionSuccess({action_type: EventActionActionTypes.EventActionCreated}));
              return new EventActionCreated({eventAction: res.body.data});
            } else {
              return new ThrowEventActionError({error: res});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private eventAction: EventActionService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
    ) {
  }
}
