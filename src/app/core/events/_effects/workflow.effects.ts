import {Injectable} from '@angular/core';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {forkJoin, of} from 'rxjs';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AppState} from '../../../core/reducers';
import {
  FireWorkflowSuccess,
  ThrowWorkflowError,
  WorkflowActionTypes,
  WorkflowCreated,
  WorkflowDeleted,
  WorkflowOnServerCreated,
  WorkflowOnServerDeleted,
  WorkflowOnServerUpdated,
  WorkflowsActionToggleLoading,
  WorkflowsPageCancelled,
  WorkflowsPageLoaded,
  WorkflowsPageRequested,
  WorkflowsPageToggleLoading,
  WorkflowUpdated
} from '../_actions/workflow.actions';
import {Router} from '@angular/router';
import {WorkflowService} from '../_services/workflow.service';
import {AuthService} from '../../auth/_services';

@Injectable()
export class WorkflowEffects {
  showPageLoadingDistpatcher = new WorkflowsPageToggleLoading({isLoading: true});
  hidePageLoadingDistpatcher = new WorkflowsPageToggleLoading({isLoading: false});
  showActionLoadingDistpatcher = new WorkflowsActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new WorkflowsActionToggleLoading({isLoading: false});

  @Effect()
  loadWorkflowsPage$ = this.actions$
    .pipe(
      ofType<WorkflowsPageRequested>(WorkflowActionTypes.WorkflowsPageRequested),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.workflow.findWorkflows(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 401) {
                this.authService.logout();
                return new WorkflowsPageCancelled();
              } else if (response[0].status === 403) {
                this.router.navigateByUrl('/error/403');
                return new WorkflowsPageCancelled();
              } else {
                this.store.dispatch(new ThrowWorkflowError({error: response[0].error}));
                return new WorkflowsPageCancelled();
              }
            } else {
              const result = response[0];
              return new WorkflowsPageLoaded({
                workflows: result.data,
                totalCount: result.count,
                page: response[1]
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteWorkflow$ = this.actions$
    .pipe(
      ofType<WorkflowOnServerDeleted>(WorkflowActionTypes.WorkflowOnServerDeleted),
      mergeMap(({payload}) => {
          this.store.dispatch(this.showActionLoadingDistpatcher);
          return this.workflow.deleteWorkflow(payload.id).pipe(
            catchError(error => {
              return of({error});
            }),
            map(() => {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireWorkflowSuccess({action_type: WorkflowActionTypes.WorkflowDeleted}));
              return new WorkflowDeleted(payload)
            })
          );
        }
      ),
    );

  @Effect()
  updateWorkflow$ = this.actions$
    .pipe(
      ofType<WorkflowOnServerUpdated>(WorkflowActionTypes.WorkflowOnServerUpdated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.workflow.updateWorkflow(payload.workflow).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if (res.error) {
              return new ThrowWorkflowError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new FireWorkflowSuccess({action_type: WorkflowActionTypes.WorkflowUpdated}));
              return new WorkflowUpdated(payload)
            }
          })
        );
      }),
    );

  @Effect()
  createWorkflow$ = this.actions$
    .pipe(
      ofType<WorkflowOnServerCreated>(WorkflowActionTypes.WorkflowOnServerCreated),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.workflow.createWorkflow(payload.workflow).pipe(
          catchError(error => {
            return of(error);
          }),
          map((res) => {
            if (res.status === 201) {
              this.store.dispatch(new FireWorkflowSuccess({action_type: WorkflowActionTypes.WorkflowCreated}));
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new WorkflowCreated({workflow: res.body.data})
            } else {
              return new ThrowWorkflowError({error: res});
            }
          })
        )
      }),
    );

  constructor(
    private actions$: Actions,
    private workflow: WorkflowService,
    private store: Store<AppState>,
    private router: Router,
    private authService: AuthService
  ) {
  }
}
