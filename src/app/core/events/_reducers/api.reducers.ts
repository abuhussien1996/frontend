import {createFeatureSelector} from '@ngrx/store';
import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {ApiActions, ApiActionTypes} from '../_actions/api.actions';
import {QueryParamsModel} from '../../_base/crud';
import {Api} from '../_models/api.model';

export interface ApisState extends EntityState<Api> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedApiId: number;
  lastQuery: QueryParamsModel;
  error: any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Api> = createEntityAdapter<Api>();

export const initialApisState: ApisState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedApiId: undefined,
  showInitWaitingMessage: true
});

export function apisReducer(state = initialApisState, action: ApiActions): ApisState {
  switch (action.type) {
    case ApiActionTypes.ApisPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedApiId: undefined
      };
    case ApiActionTypes.ApisActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case ApiActionTypes.ApiCreated:
      return adapter.addOne(action.payload.api, {
        ...state, lastCreatedApiId: action.payload.api.id
      });
    case ApiActionTypes.ApiUpdated:
      return adapter.updateOne(action.payload.partialApi, state);
    case ApiActionTypes.ApisPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case ApiActionTypes.ApisPageLoaded: {
      return adapter.addMany(action.payload.apis, {
        ...initialApisState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case ApiActionTypes.ThrowApiError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case ApiActionTypes.FireApiSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getApiState = createFeatureSelector<ApisState>('apis');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
