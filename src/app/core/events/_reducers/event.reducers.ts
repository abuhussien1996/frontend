// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { EventActions, EventActionTypes } from '../_actions/event.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Event } from '../_models/event.model';

// tslint:disable-next-line:no-empty-interface
export interface EventsState extends EntityState<Event> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedEventId: number;
  lastQuery: QueryParamsModel;
  error:any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Event> = createEntityAdapter<Event>();

export const initialEventsState: EventsState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedEventId: undefined,
  showInitWaitingMessage: true
});

export function eventsReducer(state = initialEventsState, action: EventActions): EventsState {
  switch (action.type) {
    case EventActionTypes.EventsPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedEventId: undefined
      };
    case EventActionTypes.EventsActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case EventActionTypes.EventCreated:
      return adapter.addOne(action.payload.event, {
        ...state, lastCreatedEventId: action.payload.event.id
      });
    case EventActionTypes.EventUpdated:
      return adapter.updateOne(action.payload.partialEvent, state);
    case EventActionTypes.EventDeleted:
      return adapter.removeOne(action.payload.id, state);
    case EventActionTypes.EventsPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case EventActionTypes.EventsPageLoaded: {
      return adapter.addMany(action.payload.events, {
        ...initialEventsState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case EventActionTypes.ThrowEventError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case EventActionTypes.FireEventSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getEventState = createFeatureSelector<EventsState>('events');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
