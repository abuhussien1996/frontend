// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { WorkflowActions, WorkflowActionTypes } from '../_actions/workflow.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Workflow } from '../_models/workflow.model';

// tslint:disable-next-line:no-empty-interface
export interface WorkflowsState extends EntityState<Workflow> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedWorkflowId: number;
  lastQuery: QueryParamsModel;
  error:any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Workflow> = createEntityAdapter<Workflow>();

export const initialWorkflowsState: WorkflowsState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedWorkflowId: undefined,
  showInitWaitingMessage: true
});

export function workflowsReducer(state = initialWorkflowsState, action: WorkflowActions): WorkflowsState {
  switch (action.type) {
    case WorkflowActionTypes.WorkflowsPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedWorkflowId: undefined
      };
    case WorkflowActionTypes.WorkflowsActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case WorkflowActionTypes.WorkflowCreated:
      return adapter.addOne(action.payload.workflow, {
        ...state, lastCreatedWorkflowId: action.payload.workflow.id
      });
    case WorkflowActionTypes.WorkflowUpdated:
      return adapter.updateOne(action.payload.partialWorkflow, state);
    case WorkflowActionTypes.WorkflowDeleted:
      return adapter.removeOne(action.payload.id, state);
    case WorkflowActionTypes.WorkflowsPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case WorkflowActionTypes.WorkflowsPageLoaded: {
      return adapter.addMany(action.payload.workflows, {
        ...initialWorkflowsState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case WorkflowActionTypes.ThrowWorkflowError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case WorkflowActionTypes.FireWorkflowSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getWorkflowState = createFeatureSelector<WorkflowsState>('workflows');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
