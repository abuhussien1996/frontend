import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {QueryParamsModel} from '../../_base/crud';
import {EventWorkflowGroup} from '../_models/workflow-group.model';
import {createFeatureSelector} from '@ngrx/store';
import {WorkflowActionsState} from './workflow-action.reducers';
import {WorkflowGroupActions, WorkflowGroupTypes} from '../_actions/workflow-group.action';


// tslint:disable-next-line:class-name
export interface WorkflowGroupState extends EntityState<EventWorkflowGroup> {
  listLoading: boolean;
  actionsLoading: boolean;
  lastQuery: QueryParamsModel;
  error: any;
  latestSuccessfulAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<EventWorkflowGroup> = createEntityAdapter<EventWorkflowGroup>();

export const initialWorkflowGroupState: WorkflowGroupState = adapter.getInitialState({
  listLoading: false,
  actionsLoading: false,
  error: null,
  latestSuccessfulAction: null,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export const getWorkflowGroupState = createFeatureSelector<WorkflowActionsState>('workflowActions');


export function workflowGroupReducer(state = initialWorkflowGroupState, action: WorkflowGroupActions): WorkflowGroupState {
  switch (action.type) {
    case WorkflowGroupTypes.WorkflowGroupPageLoaded: {
      return adapter.addMany(action.payload.workflowGroup, {
        ...initialWorkflowGroupState,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case WorkflowGroupTypes.WorkflowGroupPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading
      };
    }
    case WorkflowGroupTypes.FireWorkflowGroupSuccess: {
      return {
        ...state, latestSuccessfulAction: {action_type: action.payload.action_type}, error: null
      };
    }
    case WorkflowGroupTypes.WorkflowGroupCreated:
      return adapter.addOne(action.payload.workflowGroup, state);
    case WorkflowGroupTypes.WorkflowGroupUpdated:
      return adapter.updateOne(action.payload.partialEventAction, state);
    case WorkflowGroupTypes.WorkflowGroupDeleted:
      return adapter.removeOne(action.payload.id, state);
    default:
      return state;
  }
}

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
