// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// Actions
import { InboxActions, InboxActionTypes } from '../_actions/inbox.actions';
// Models
import { InboxItem } from '../_models/inbox.model';
import { QueryParamsModel } from '../../_base/crud';

// tslint:disable-next-line:no-empty-interface
export interface InboxState extends EntityState<InboxItem> {
  inboxWorkflow: any;
  attachments: any;
  listLoading: boolean;
  actionLoading: boolean;
  totalCount: number;
  lastQuery: any;
  error:any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
  latestCardValues: any;
}

export const adapter: EntityAdapter<InboxItem> = createEntityAdapter<InboxItem>();

export const initialInboxState: InboxState = adapter.getInitialState({
  inboxWorkflow: null,
  attachments: [],
  listLoading: false,
  actionLoading: false,
  totalCount: 0,
  lastQuery: {},
  error: null,
  latestSuccessfullAction: null,
  showInitWaitingMessage: true,
  latestCardValues: null
});

export function inboxReducer(state = initialInboxState, action: InboxActions): InboxState {
  switch (action.type) {
    case InboxActionTypes.InboxListToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading
      };
    case InboxActionTypes.InboxActionToggleLoading:
      return {
        ...state, actionLoading: action.payload.isLoading
      };
    case InboxActionTypes.ItemWorkflowLoaded:
      return {
        ...state, inboxWorkflow: action.payload.inboxWorkflow
      };
    case InboxActionTypes.AttachmentsLoaded:
      return {
        ...state, attachments: action.payload.attachments
      };
    case InboxActionTypes.InboxLoaded: {
      return adapter.addMany(action.payload.entities, {
        ...initialInboxState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.filterQuery,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case InboxActionTypes.InboxLoadFailed:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case InboxActionTypes.ItemUpdated:
      return adapter.updateOne(action.payload.partialItem, state);
    case InboxActionTypes.UpdateCardValues:
      return {
        ...state, latestCardValues: action.payload.new_values
      };
    case InboxActionTypes.ThrowInboxError: {
      return {
        ...state, error: action.payload.error, listLoading: false
      };
    }
    case InboxActionTypes.FireInboxSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getInboxState = createFeatureSelector<InboxState>('inbox');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
