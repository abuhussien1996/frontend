import {createFeatureSelector} from '@ngrx/store';
import {createEntityAdapter, EntityAdapter, EntityState, Update} from '@ngrx/entity';
import {WorkflowActionActions, WorkflowActionActionTypes} from '../_actions/workflow-action.actions';
import {QueryParamsModel} from '../../_base/crud';
import {WorkflowAction} from '../_models/workflow-action.model';

// tslint:disable-next-line:no-empty-interface
export interface WorkflowActionsState extends EntityState<WorkflowAction> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedWorkflowActionId: number;
  lastQuery: QueryParamsModel;
  error:any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<WorkflowAction> = createEntityAdapter<WorkflowAction>();

export const initialWorkflowActionsState: WorkflowActionsState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedWorkflowActionId: undefined,
  showInitWaitingMessage: true
});

export function workflowActionsReducer(state = initialWorkflowActionsState, action: WorkflowActionActions): WorkflowActionsState {
  switch (action.type) {
    case WorkflowActionActionTypes.WorkflowActionsPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedWorkflowActionId: undefined
      };
    case WorkflowActionActionTypes.WorkflowActionsActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case WorkflowActionActionTypes.WorkflowActionCreated:
      return adapter.addOne(action.payload.workflowAction, {
        ...state, lastCreatedWorkflowActionId: action.payload.workflowAction.id
      });
    case WorkflowActionActionTypes.WorkflowActionUpdated:
      return adapter.updateOne(action.payload.partialWorkflowAction, state);
    case WorkflowActionActionTypes.WorkflowActionDeleted:
      return adapter.removeOne(action.payload.id, state);
    case WorkflowActionActionTypes.WorkflowActionsPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case WorkflowActionActionTypes.WorkflowActionsPageLoaded: {
      return adapter.addMany(action.payload.workflowActions, {
        ...initialWorkflowActionsState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case WorkflowActionActionTypes.ThrowWorkflowActionError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case WorkflowActionActionTypes.FireWorkflowActionSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    case WorkflowActionActionTypes.DeletedStatusUpdated: {
      const _partialWorkflowAction: Update<WorkflowAction> = {
        id: action.payload.id,
        changes: {
          deleted: action.payload.deleted
        }
      };
      return adapter.updateOne(_partialWorkflowAction, state);
    }
    case WorkflowActionActionTypes.ParentUpdated: {
      const _partialWorkflowAction: Update<WorkflowAction> = {
        id: action.payload.id,
        changes: {
          parent_id: action.payload.parent_id
        }
      };
      return adapter.updateOne(_partialWorkflowAction, state);
    }
    case WorkflowActionActionTypes.WorkflowActionMapUpdated: {
      const _partialWorkflowAction: Update<WorkflowAction> = {
        id: action.payload.updatedObject.id,
        changes: {
          mapping_template: action.payload.updatedObject.mapping_template
        }
      };
      return adapter.updateOne(_partialWorkflowAction, state);
    }
    default:
      return state;
  }
}

export const getWorkflowActionState = createFeatureSelector<WorkflowActionsState>('workflowActions');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
