// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { EventActionActions, EventActionActionTypes } from '../_actions/action.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { EventAction } from '../_models/action.model';

// tslint:disable-next-line:no-empty-interface
export interface EventActionsState extends EntityState<EventAction> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedEventActionId: number;
  lastQuery: QueryParamsModel;
  error:any;
  latestSuccessfullAction: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<EventAction> = createEntityAdapter<EventAction>();

export const initialEventActionsState: EventActionsState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedEventActionId: undefined,
  showInitWaitingMessage: true
});

export function eventActionsReducer(state = initialEventActionsState, action: EventActionActions): EventActionsState {
  switch (action.type) {
    case EventActionActionTypes.EventActionsPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedEventActionId: undefined
      };
    case EventActionActionTypes.EventActionsActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case EventActionActionTypes.EventActionCreated:
      return adapter.addOne(action.payload.eventAction, {
        ...state, lastCreatedEventActionId: action.payload.eventAction.id
      });
    case EventActionActionTypes.EventActionUpdated:
      return adapter.updateOne(action.payload.partialEventAction, state);
    case EventActionActionTypes.EventActionsPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case EventActionActionTypes.EventActionsPageLoaded: {
      return adapter.addMany(action.payload.eventActions, {
        ...initialEventActionsState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false,
        error: null
      });
    }
    case EventActionActionTypes.ThrowEventActionError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case EventActionActionTypes.FireEventActionSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getEventActionState = createFeatureSelector<EventActionsState>('eventActions');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
