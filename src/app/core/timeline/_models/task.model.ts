import {BaseModel} from '../../_base/crud';

export class Task extends BaseModel {
  id: number;
  title: string;
  description: string;
  progress: 'PLANNED' | 'ON_HOLD' | 'IN_PROGRESS' | 'WAITING' | 'DONE' = 'PLANNED';
  priority: 'HIGH' | 'MEDIUM' | 'LOW';
  due_date: any;
  is_recurring = false;
  parent_id: number;
  task_entities: any;
  task_responsible_users: any;
  task_followers: any;
  attachments: any;
  company_id: number;
  completed: boolean;
  task_template:any;
  user_id: number;
  topic: string;
  category_id: number;
  start_time: string;
  end_time: string;
  status: string;

  clear() {
    this.progress = 'PLANNED';
    this.completed = false;
  }
}
