import { BaseModel } from '../../_base/crud';
import { Timestamp } from 'rxjs/internal/operators/timestamp';

export class Meeting extends BaseModel {
  id: number;
  subject: string;
  body: string;
  from_time: string;
  to_time: string;
  with: any[];
  responsible: any[]
  files: any[];
  receivers: any[];
  is_completed: boolean
  is_important: boolean
  location: string;
  related_to_list: any[]
  data: any;
  drug
  clear() {

  }
}
