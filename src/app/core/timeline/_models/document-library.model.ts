import {BaseModel} from '../../_base/crud';

export class DocumentLibrary extends BaseModel {

  id: number;
  body: string;
  file: any[];
  receiver: any[];
  sender_email: string
  data: any
  is_completed: boolean;
  sender_name: string;

}
