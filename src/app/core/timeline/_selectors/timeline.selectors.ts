// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { TimelinesState } from '../_reducers/timeline.reducers';
import { each } from 'lodash';
import { Timeline } from '../_models/timeline.model';

export const selectTimelinesState = createFeatureSelector<TimelinesState>('timelines');

export const selectTimelineById = (timelineId: number) => createSelector(
    selectTimelinesState,
    timelinesState => timelinesState.entities[timelineId]
);

export const selectTimelinesPageLoading = createSelector(
    selectTimelinesState,
    timelinesState => {
        return timelinesState.listLoading;
    }
);

export const selectTimelinesActionLoading = createSelector(
    selectTimelinesState,
    timelinesState => timelinesState.actionsloading
);

export const selectLastCreatedTimelineId = createSelector(
    selectTimelinesState,
    timelinesState => timelinesState.lastCreatedTimelineId
);

export const selectTimelinesPageLastQuery = createSelector(
    selectTimelinesState,
    timelinesState => timelinesState.lastQuery
);
export const selectTimelineErrorState = createSelector(
    selectTimelinesState,
    timelinesState => timelinesState.error
);
export const selectTimelinesInStore = createSelector(
    selectTimelinesState,
    timelinesState => {
        const items: Timeline[] = [];
        each(timelinesState.entities, element => {
            items.push(element);
        });
        console.log(items);

        const httpExtension = new HttpExtenstionsModel();
        const result: Timeline[] = httpExtension.sortArray(items, timelinesState.lastQuery.sortField, timelinesState.lastQuery.sortOrder);
        return new QueryResultsModel(result, timelinesState.totalCount);
    }
);

export const selectTimelinesShowInitWaitingMessage = createSelector(
    selectTimelinesState,
    timelinesState => timelinesState.showInitWaitingMessage
);

export const selectHasTimelinesInStore = createSelector(
    selectTimelinesState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
