// NGRX
import {createFeatureSelector, createSelector} from '@ngrx/store';
// CRUD
import {HttpExtenstionsModel, QueryResultsModel} from '../../_base/crud';
// State
import {MessagesState} from '../_reducers/message.reducers';
import {each} from 'lodash';
import {Message} from '../_models/message.model';

export const selectMessagesState = createFeatureSelector<MessagesState>('messages');

export const selectMessageById = (messageId: number) => createSelector(
  selectMessagesState,
  messagesState => messagesState.entities[messageId]
);

export const selectMessagesPageLoading = createSelector(
  selectMessagesState,
  messagesState => {
    return messagesState.listLoading;
  }
);

export const selectMessagesActionLoading = createSelector(
  selectMessagesState,
  messagesState => messagesState.actionsloading
);

export const selectLastCreatedMessageId = createSelector(
  selectMessagesState,
  messagesState => messagesState.lastCreatedMessageId
);

export const selectMessagesPageLastQuery = createSelector(
  selectMessagesState,
  messagesState => messagesState.lastQuery
);
export const selectMessageErrorState = createSelector(
  selectMessagesState,
  messagesState => messagesState.error
);
export const selectMessagesInStore = createSelector(
  selectMessagesState,
  messagesState => {
    const items: Message[] = [];
    each(messagesState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: Message[] = httpExtension.sortArray(items, messagesState.lastQuery.sortField, messagesState.lastQuery.sortOrder);
    return new QueryResultsModel(result, messagesState.totalCount);
  }
);

export const selectMessagesShowInitWaitingMessage = createSelector(
  selectMessagesState,
  messagesState => messagesState.showInitWaitingMessage
);

export const selectHasMessagesInStore = createSelector(
  selectMessagesState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }
    return true;
  }
);
