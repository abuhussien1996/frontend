// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { TasksState } from '../_reducers/task.reducers';
import { each } from 'lodash';
import { Task } from '../_models/task.model';

export const selectTasksState = createFeatureSelector<TasksState>('tasks');

export const selectTaskById = (taskId: number) => createSelector(
    selectTasksState,
    tasksState => tasksState.entities[taskId]
);

export const selectTaskActionLoading = createSelector(
    selectTasksState,
    tasksState => tasksState.actionsloading
);

export const selectLastCreatedTaskId = createSelector(
    selectTasksState,
    tasksState => tasksState.lastCreatedTaskId
);

export const selectTaskErrors = createSelector(
    selectTasksState,
    tasksState => tasksState.error
);

export const selectTaskLatestSuccessfullAction = createSelector(
    selectTasksState,
    tasksState =>  tasksState.latestSuccessfullAction
);

export const taskSelectedUsers = createSelector(
    selectTasksState,
    tasksState => tasksState.selectedUsers
);

export const selectTasksInStore = createSelector(
    selectTasksState,
    tasksState => {
        const items: Task[] = [];
        each(tasksState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Task[] = httpExtension.sortArray(items, 'id', 'asc');
        return new QueryResultsModel(result, tasksState.totalCount);
    }
);

export const selectHasTasksInStore = createSelector(
    selectTasksState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }
        return true;
    }
);
