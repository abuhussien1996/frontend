// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { MeetingsState } from '../_reducers/meeting.reducers';
import { each } from 'lodash';
import { Meeting } from '../_models/meeting.model';

export const selectMeetingsState = createFeatureSelector<MeetingsState>('meetings');

export const selectMeetingById = (meetingId: number) => createSelector(
    selectMeetingsState,
    meetingsState => meetingsState.entities[meetingId]
);

export const selectMeetingsPageLoading = createSelector(
    selectMeetingsState,
    meetingsState => {
        return meetingsState.listLoading;
    }
);

export const selectMeetingsActionLoading = createSelector(
    selectMeetingsState,
    meetingsState => meetingsState.actionsloading
);

export const selectLastCreatedMeetingId = createSelector(
    selectMeetingsState,
    meetingsState => meetingsState.lastCreatedMeetingId
);

export const selectMeetingsPageLastQuery = createSelector(
    selectMeetingsState,
    meetingsState => meetingsState.lastQuery
);
export const selectErrorState = createSelector(
    selectMeetingsState,
    meetingsState => meetingsState.error
);
export const selectMeetingsInStore = createSelector(
    selectMeetingsState,
    meetingsState => {
        const items: Meeting[] = [];
        each(meetingsState.entities, element => {
            items.push(element);
        });
        const httpExtension = new HttpExtenstionsModel();
        const result: Meeting[] = httpExtension.sortArray(items, meetingsState.lastQuery.sortField, meetingsState.lastQuery.sortOrder);
        return new QueryResultsModel(result, meetingsState.totalCount);
    }
);

export const selectMeetingsShowInitWaitingMessage = createSelector(
    selectMeetingsState,
    meetingsState => meetingsState.showInitWaitingMessage
);

export const selectHasMeetingsInStore = createSelector(
    selectMeetingsState,
    queryResult => {
        if (!queryResult.totalCount) {
            return false;
        }

        return true;
    }
);
