import {createFeatureSelector, createSelector} from '@ngrx/store';
import {HttpExtenstionsModel, QueryResultsModel} from '../../_base/crud';
import {DocumentLibraryState} from '../_reducers/document-library.reducers';
import {each} from 'lodash';
import {DocumentLibrary} from '../_models/document-library.model';

export const selectDocumentLibraryState = createFeatureSelector<DocumentLibraryState>('documentLibrary');

export const selectDocumentLibraryById = (documentLibraryId: number) => createSelector(
  selectDocumentLibraryState,
  documentLibraryState => documentLibraryState.entities[documentLibraryId]
);

export const selectDocumentLibraryPageLoading = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => {
    return documentLibraryState.listLoading;
  }
);

export const selectDocumentLibraryActionLoading = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => documentLibraryState.actionsloading
);

export const selectLastCreatedDocumentLibraryId = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => documentLibraryState.lastCreatedDocumentLibraryId
);

export const selectDocumentLibraryPageLastQuery = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => documentLibraryState.lastQuery
);
export const selectDocumentLibraryErrorState = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => documentLibraryState.error
);
export const selectDocumentLibraryInStore = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => {
    const items: DocumentLibrary[] = [];
    each(documentLibraryState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: DocumentLibrary[] = httpExtension.sortArray(items, documentLibraryState.lastQuery.sortField,
      documentLibraryState.lastQuery.sortOrder);
    return new QueryResultsModel(result, documentLibraryState.totalCount);
  }
);

export const selectDocumentLibraryShowInitWaitingMessage = createSelector(
  selectDocumentLibraryState,
  documentLibraryState => documentLibraryState.showInitWaitingMessage
);

export const selectHasDocumentLibraryInStore = createSelector(
  selectDocumentLibraryState,
  queryResult => {
    if (!queryResult.totalCount) {
      return false;
    }
    return true;
  }
);
