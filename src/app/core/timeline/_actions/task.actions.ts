// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Task } from '../_models/task.model';

export enum TaskActionTypes {
    AllTasksRequested = '[Task Module] All Tasks Requested',
    AllTasksLoaded = '[Task API] All Tasks Loaded',
    TaskOnServerCreated = '[Edit Task Component] Task On Server Created',
    TaskCreated = '[Edit Task Dialog] Task Created',
    TaskOnServerUpdated = '[Edit Task Dialog] Task On Server Updated',
    TaskUpdated = '[Edit Task Dialog] Task Updated',
    TaskDeleted = '[Tasks] Task Deleted',
    TaskActionToggleLoading = '[Tasks] Task Action Toggle Loading',
    ThrowTaskError = '[Tasks] Throw Task Error',
    FireTaskSuccess = '[Tasks] Fire Success'
}

export class AllTasksRequested implements Action {
    readonly type = TaskActionTypes.AllTasksRequested;
    constructor(public payload: { start: string, end: string, user_ids?: number[] }) { }
}

export class AllTasksLoaded implements Action {
    readonly type = TaskActionTypes.AllTasksLoaded;
    constructor(public payload: { tasks: Task[], user_ids?: number[] }) { }
}

export class TaskOnServerCreated implements Action {
    readonly type = TaskActionTypes.TaskOnServerCreated;
    constructor(public payload: { task: Task }) { }
}

export class TaskCreated implements Action {
    readonly type = TaskActionTypes.TaskCreated;
    constructor(public payload: { task: Task }) { }
}

export class TaskOnServerUpdated implements Action {
    readonly type = TaskActionTypes.TaskOnServerUpdated;
    constructor(public payload: {
        partialTask: Update<Task>,
        task: Task
    }) { }
}

export class TaskUpdated implements Action {
    readonly type = TaskActionTypes.TaskUpdated;
    constructor(public payload: {
        partialTask: Update<Task>,
        task: Task
    }) { }
}

export class TaskDeleted implements Action {
    readonly type = TaskActionTypes.TaskDeleted;
    constructor(public payload: { id: number }) { }
}

export class TaskActionToggleLoading implements Action {
    readonly type = TaskActionTypes.TaskActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class ThrowTaskError implements Action {
    readonly type = TaskActionTypes.ThrowTaskError;

    constructor(public payload: { error: any }) {
    }
}

export class FireTaskSuccess implements Action {
  readonly type = TaskActionTypes.FireTaskSuccess;
  constructor(public payload: {action_type: string}) {}
}

export type TaskActions = AllTasksRequested
    | AllTasksLoaded
    | TaskCreated
    | TaskUpdated
    | TaskDeleted
    | TaskOnServerCreated
    | TaskOnServerUpdated
    | ThrowTaskError
    | FireTaskSuccess
    | TaskActionToggleLoading;
