// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Timeline } from '../_models/timeline.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum TimelineActionTypes {
    AllTimelinesRequested = '[Timelines Module] All Timelines Requested',
    AllTimelinesLoaded = '[Timelines API] All Timelines Loaded',
    TimelineOnServerCreated = '[Edit Timeline Component] Timeline On Server Created',
    TimelineCreated = '[Edit Timeline Dialog] Timeline Created',
    TimelineOnServerUpdated = '[Edit Timeline Dialog] Timeline On Server Updated',
    TimelineUpdated = '[Edit Timeline Dialog] Timeline Updated',
    TimelineStatusOnServerUpdated = '[Timelines List Page] Timeline Status On Server Updated Updated',
    TimelineStatusUpdated = '[Timelines List Page] Timeline Status Updated Updated',
    TimelinesStatusOnServerUpdated = '[Timelines List Page] Timelines Status On Server Updated Updated',
    TimelinesStatusUpdated = '[Timelines List Page] Timelines Status Updated Updated',
    TimelineOnServerDeleted = '[Timelines List Page] Timeline On Server Deleted',
    TimelineDeleted = '[Timelines List Page] Timeline Deleted',
    ManyTimelinesOnServerDeleted = '[Timelines List Page] Timelines On Server Deleted',
    ManyTimelinesDeleted = '[Timelines List Page] Timelines Deleted',
    TimelinesPageRequested = '[Timelines List Page] Timelines Page Requested',
    TimelinesPageLoaded = '[Timelines API] Timelines Page Loaded',
    TimelinesPageCancelled = '[Timelines API] Timelines Page Cancelled',
    TimelinesPageToggleLoading = '[Timelines] Timelines Page Toggle Loading',
    TimelinesActionToggleLoading = '[Timelines] Timelines Action Toggle Loading',
    ChangePassword = '[Timelines] Password changed',
    ActivityPageRequested = '[Timelines] ActivityPageRequested',
    ThrowTimelineError = '[Timelines] ThrowError',
    ClearTimelineErrors = '[Timelines] ClearErrors'
}

export class TimelineOnServerCreated implements Action {
    readonly type = TimelineActionTypes.TimelineOnServerCreated;
    constructor(public payload: { timeline: Timeline }) { }
}

export class TimelineCreated implements Action {
    readonly type = TimelineActionTypes.TimelineCreated;
    constructor(public payload: { timeline: Timeline }) { }
}

export class ThrowTimelineError implements Action {
    readonly type = TimelineActionTypes.ThrowTimelineError;

    constructor(public payload: { error: any }) {
    }
}

export class ClearTimelineErrors implements Action {
    readonly type = TimelineActionTypes.ClearTimelineErrors;
}
export class TimelineOnServerUpdated implements Action {
    readonly type = TimelineActionTypes.TimelineOnServerUpdated;
    constructor(public payload: {
        partialTimeline: Update<Timeline>,
        timeline: Timeline
    }) { }
}
export class TimelineUpdated implements Action {
    readonly type = TimelineActionTypes.TimelineUpdated;
    constructor(public payload: {
        partialTimeline: Update<Timeline>,
        timeline: Timeline
    }) { }
}


export class TimelineStatusOnServerUpdated implements Action {
    readonly type = TimelineActionTypes.TimelineStatusOnServerUpdated;
    constructor(public payload: { id: number, is_active: boolean }) { }
}


export class TimelineStatusUpdated implements Action {
    readonly type = TimelineActionTypes.TimelineStatusUpdated;
    constructor(public payload: { id: number, is_active: boolean }) { }
}


export class TimelinesStatusOnServerUpdated implements Action {
    readonly type = TimelineActionTypes.TimelinesStatusOnServerUpdated;
    constructor(public payload: {
        timelines: Timeline[],
        is_active: number
    }) { }
}


export class TimelinesStatusUpdated implements Action {
    readonly type = TimelineActionTypes.TimelinesStatusUpdated;
    constructor(public payload: {
        timelines: Timeline[],
        is_active: number
    }) { }
}

export class TimelineOnServerDeleted implements Action {
    readonly type = TimelineActionTypes.TimelineOnServerDeleted;
    constructor(public payload: { id: number }) { }
}

export class TimelineDeleted implements Action {
    readonly type = TimelineActionTypes.TimelineDeleted;
    constructor(public payload: { id: number }) { }
}

export class ManyTimelinesOnServerDeleted implements Action {
    readonly type = TimelineActionTypes.ManyTimelinesOnServerDeleted;
    constructor(public payload: { ids: any[] }) { }
}

export class ManyTimelinesDeleted implements Action {
    readonly type = TimelineActionTypes.ManyTimelinesDeleted;
    constructor(public payload: { ids: any[] }) { }
}



export class TimelinesPageRequested implements Action {
    readonly type = TimelineActionTypes.TimelinesPageRequested;
    constructor(public payload: { id : number,type:string }) {
    }
}

export class TimelinesPageLoaded implements Action {
    readonly type = TimelineActionTypes.TimelinesPageLoaded;
    constructor(public payload: { timelines: Timeline[], totalCount: number, page: QueryParamsModel }) { }
}


export class TimelinesPageCancelled implements Action {
    readonly type = TimelineActionTypes.TimelinesPageCancelled;
}

export class TimelinesPageToggleLoading implements Action {
    readonly type = TimelineActionTypes.TimelinesPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class TimelinesActionToggleLoading implements Action {
    readonly type = TimelineActionTypes.TimelinesActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class ChangePassword implements Action {
    readonly type = TimelineActionTypes.ChangePassword;
    constructor(public payload: { timelineName: string, oldPassword: string, newPassword: string }) { }
}



export type TimelineActions = TimelineCreated
    | TimelineUpdated
    | TimelineDeleted
    | ManyTimelinesDeleted
    | TimelineOnServerCreated
    | TimelineOnServerUpdated
    | TimelineOnServerDeleted
    | ManyTimelinesOnServerDeleted
    | TimelineStatusOnServerUpdated
    | TimelinesStatusOnServerUpdated
    | TimelinesPageLoaded
    | TimelinesPageCancelled
    | TimelinesPageToggleLoading
    | TimelinesPageRequested
    | ChangePassword
    | TimelineStatusUpdated
    | TimelinesStatusUpdated
    | ThrowTimelineError
    | ClearTimelineErrors
    | TimelinesActionToggleLoading;
