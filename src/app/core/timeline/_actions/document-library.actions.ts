import {Action} from '@ngrx/store';
import {DocumentLibrary} from '../_models/document-library.model';
import {QueryParamsModel} from '../../_base/crud';

export enum DocumentLibraryActionTypes {
  AllDocumentLibraryRequested = '[Document Library Module] All DocumentLibrary Requested',
  AllDocumentLibraryLoaded = '[Document Library API] All DocumentLibrary Loaded',
  DocumentLibraryPageRequested = '[Document Library List Page] DocumentLibrary Page Requested',
  DocumentLibraryPageLoaded = '[Document Library API] DocumentLibrary Page Loaded',
  DocumentLibraryPageCancelled = '[Document Library API] DocumentLibrary Page Cancelled',
  DocumentLibraryPageToggleLoading = '[Document Library] DocumentLibrary Page Toggle Loading',
  DocumentLibraryActionToggleLoading = '[Document Library] DocumentLibrary Action Toggle Loading',
  ActivityPageRequested = '[Document Library] Activity Page Requested',
  ThrowDocumentLibraryError = '[Document Library] Throw Error',
  ClearDocumentLibraryErrors = '[Document Library] Clear Errors',
}


export class ThrowDocumentLibraryError implements Action {
  readonly type = DocumentLibraryActionTypes.ThrowDocumentLibraryError;

  constructor(public payload: { error: any }) {
  }
}

export class ClearDocumentLibraryErrors implements Action {
  readonly type = DocumentLibraryActionTypes.ClearDocumentLibraryErrors;
}


export class DocumentLibraryPageRequested implements Action {
  readonly type = DocumentLibraryActionTypes.DocumentLibraryPageRequested;

  constructor(public payload: { page: QueryParamsModel }) {
  }
}

export class DocumentLibraryPageLoaded implements Action {
  readonly type = DocumentLibraryActionTypes.DocumentLibraryPageLoaded;

  constructor(public payload: { documentLibrary: DocumentLibrary[], totalCount: number, page: QueryParamsModel }) {
  }
}


export class DocumentLibraryPageCancelled implements Action {
  readonly type = DocumentLibraryActionTypes.DocumentLibraryPageCancelled;
}

export class DocumentLibraryPageToggleLoading implements Action {
  readonly type = DocumentLibraryActionTypes.DocumentLibraryPageToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}

export class DocumentLibraryActionToggleLoading implements Action {
  readonly type = DocumentLibraryActionTypes.DocumentLibraryActionToggleLoading;

  constructor(public payload: { isLoading: boolean }) {
  }
}


export type DocumentLibraryActions = DocumentLibraryPageLoaded
  | DocumentLibraryPageCancelled
  | DocumentLibraryPageToggleLoading
  | DocumentLibraryPageRequested
  | ThrowDocumentLibraryError
  | ClearDocumentLibraryErrors
  | DocumentLibraryActionToggleLoading;
