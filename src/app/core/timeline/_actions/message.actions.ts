// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Message } from '../_models/message.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum MessageActionTypes {
    AllMessagesRequested = '[Messages Module] All Messages Requested',
    AllMessagesLoaded = '[Messages API] All Messages Loaded',
    MessageOnServerCreated = '[Edit Message Component] Message On Server Created',
    MessageCreated = '[Edit Message Dialog] Message Created',
    MessageOnServerUpdated = '[Edit Message Dialog] Message On Server Updated',
    MessageUpdated = '[Edit Message Dialog] Message Updated',
    MessageStatusOnServerUpdated = '[Messages List Page] Message Status On Server Updated Updated',
    MessageStatusUpdated = '[Messages List Page] Message Status Updated Updated',
    MessagesStatusOnServerUpdated = '[Messages List Page] Messages Status On Server Updated Updated',
    MessagesStatusUpdated = '[Messages List Page] Messages Status Updated Updated',
    MessageOnServerDeleted = '[Messages List Page] Message On Server Deleted',
    MessageDeleted = '[Messages List Page] Message Deleted',
    ManyMessagesOnServerDeleted = '[Messages List Page] Messages On Server Deleted',
    ManyMessagesDeleted = '[Messages List Page] Messages Deleted',
    MessagesPageRequested = '[Messages List Page] Messages Page Requested',
    MessagesPageLoaded = '[Messages API] Messages Page Loaded',
    MessagesPageCancelled = '[Messages API] Messages Page Cancelled',
    MessagesPageToggleLoading = '[Messages] Messages Page Toggle Loading',
    MessagesActionToggleLoading = '[Messages] Messages Action Toggle Loading',
    ChangePassword = '[Messages] Password changed',
    ActivityPageRequested = '[Messages] Activity Page Requested',
    ThrowMessageError = '[Messages] Throw Error',
    ClearMessageErrors = '[Messages] Clear Errors',
    AddNewMessages = '[Messages] Add New Messages'
}

export class MessageOnServerCreated implements Action {
    readonly type = MessageActionTypes.MessageOnServerCreated;
    constructor(public payload: { request:string,message: Message }) { }
}

export class MessageCreated implements Action {
    readonly type = MessageActionTypes.MessageCreated;
    constructor(public payload: { message: Message }) { }
}

export class ThrowMessageError implements Action {
    readonly type = MessageActionTypes.ThrowMessageError;

    constructor(public payload: { error: any }) {
    }
}

export class ClearMessageErrors implements Action {
    readonly type = MessageActionTypes.ClearMessageErrors;
}
export class MessageOnServerUpdated implements Action {
    readonly type = MessageActionTypes.MessageOnServerUpdated;
    constructor(public payload: {
        partialMessage: Update<Message>,
        message: Message
    }) { }
}
export class MessageUpdated implements Action {
    readonly type = MessageActionTypes.MessageUpdated;
    constructor(public payload: {
        partialMessage: Update<Message>,
        message: Message
    }) { }
}


export class MessageStatusOnServerUpdated implements Action {
    readonly type = MessageActionTypes.MessageStatusOnServerUpdated;
    constructor(public payload: { id: number, is_active: boolean }) { }
}


export class MessageStatusUpdated implements Action {
    readonly type = MessageActionTypes.MessageStatusUpdated;
    constructor(public payload: { id: number, is_completed: boolean }) { }
}


export class MessagesStatusOnServerUpdated implements Action {
    readonly type = MessageActionTypes.MessagesStatusOnServerUpdated;
    constructor(public payload: {
        messages: Message[],
        is_active: number
    }) { }
}


export class MessagesStatusUpdated implements Action {
    readonly type = MessageActionTypes.MessagesStatusUpdated;
    constructor(public payload: {
        messages: Message[],
        is_active: number
    }) { }
}

export class MessageOnServerDeleted implements Action {
    readonly type = MessageActionTypes.MessageOnServerDeleted;
    constructor(public payload: { id: number }) { }
}

export class MessageDeleted implements Action {
    readonly type = MessageActionTypes.MessageDeleted;
    constructor(public payload: { id: number }) { }
}

export class ManyMessagesOnServerDeleted implements Action {
    readonly type = MessageActionTypes.ManyMessagesOnServerDeleted;
    constructor(public payload: { ids: any[] }) { }
}

export class ManyMessagesDeleted implements Action {
    readonly type = MessageActionTypes.ManyMessagesDeleted;
    constructor(public payload: { ids: any[] }) { }
}



export class MessagesPageRequested implements Action {
    readonly type = MessageActionTypes.MessagesPageRequested;
    constructor(public payload: { page: QueryParamsModel }) {
    }
}

export class MessagesPageLoaded implements Action {
    readonly type = MessageActionTypes.MessagesPageLoaded;
    constructor(public payload: { messages: Message[], totalCount: number, page: QueryParamsModel }) { }
}

export class AddNewMessages implements Action {
    readonly type = MessageActionTypes.AddNewMessages;
    constructor(public payload: { messages: Message[], addCount: number }) { }
}


export class MessagesPageCancelled implements Action {
    readonly type = MessageActionTypes.MessagesPageCancelled;
}

export class MessagesPageToggleLoading implements Action {
    readonly type = MessageActionTypes.MessagesPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class MessagesActionToggleLoading implements Action {
    readonly type = MessageActionTypes.MessagesActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class ChangePassword implements Action {
    readonly type = MessageActionTypes.ChangePassword;
    constructor(public payload: { messageName: string, oldPassword: string, newPassword: string }) { }
}



export type MessageActions = MessageCreated
    | MessageUpdated
    | MessageDeleted
    | ManyMessagesDeleted
    | MessageOnServerCreated
    | MessageOnServerUpdated
    | MessageOnServerDeleted
    | ManyMessagesOnServerDeleted
    | MessageStatusOnServerUpdated
    | MessagesStatusOnServerUpdated
    | MessagesPageLoaded
    | MessagesPageCancelled
    | MessagesPageToggleLoading
    | MessagesPageRequested
    | ChangePassword
    | MessageStatusUpdated
    | MessagesStatusUpdated
    | ThrowMessageError
    | ClearMessageErrors
    | MessagesActionToggleLoading
    | AddNewMessages;
