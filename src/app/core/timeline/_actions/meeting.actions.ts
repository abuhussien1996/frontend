// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { Meeting } from '../_models/meeting.model';
// Models
import { QueryParamsModel } from '../../_base/crud';

export enum MeetingActionTypes {
    AllMeetingsRequested = '[Meetings Module] All Meetings Requested',
    AllMeetingsLoaded = '[Meetings API] All Meetings Loaded',
    MeetingOnServerCreated = '[Edit Meeting Component] Meeting On Server Created',
    MeetingCreated = '[Edit Meeting Dialog] Meeting Created',
    MeetingOnServerUpdated = '[Edit Meeting Dialog] Meeting On Server Updated',
    MeetingUpdated = '[Edit Meeting Dialog] Meeting Updated',
    MeetingStatusOnServerUpdated = '[Meetings List Page] Meeting Status On Server Updated Updated',
    MeetingStatusUpdated = '[Meetings List Page] Meeting Status Updated Updated',
    MeetingsStatusOnServerUpdated = '[Meetings List Page] Meetings Status On Server Updated Updated',
    MeetingsStatusUpdated = '[Meetings List Page] Meetings Status Updated Updated',
    MeetingOnServerDeleted = '[Meetings List Page] Meeting On Server Deleted',
    MeetingDeleted = '[Meetings List Page] Meeting Deleted',
    ManyMeetingsOnServerDeleted = '[Meetings List Page] Meetings On Server Deleted',
    ManyMeetingsDeleted = '[Meetings List Page] Meetings Deleted',
    MeetingsPageRequested = '[Meetings List Page] Meetings Page Requested',
    MeetingsPageLoaded = '[Meetings API] Meetings Page Loaded',
    MeetingsPageCancelled = '[Meetings API] Meetings Page Cancelled',
    MeetingsPageToggleLoading = '[Meetings] Meetings Page Toggle Loading',
    MeetingsActionToggleLoading = '[Meetings] Meetings Action Toggle Loading',
    ChangePassword = '[Meetings] Password changed',
    ActivityPageRequested = '[Meetings] ActivityPageRequested',
    ThrowError = '[Meetings] ThrowError',
    ClearErrors = '[Meetings] ClearErrors'
}

export class MeetingOnServerCreated implements Action {
    readonly type = MeetingActionTypes.MeetingOnServerCreated;
    constructor(public payload: { request:string, meeting: Meeting }) { }
}

export class MeetingCreated implements Action {
    readonly type = MeetingActionTypes.MeetingCreated;
    constructor(public payload: { meeting: Meeting }) { }
}

export class ThrowError implements Action {
    readonly type = MeetingActionTypes.ThrowError;

    constructor(public payload: { error: any }) {
    }
}

export class ClearErrors implements Action {
    readonly type = MeetingActionTypes.ClearErrors;
}
export class MeetingOnServerUpdated implements Action {
    readonly type = MeetingActionTypes.MeetingOnServerUpdated;
    constructor(public payload: {
        partialMeeting: Update<Meeting>,
        meeting: Meeting
    }) { }
}
export class MeetingUpdated implements Action {
    readonly type = MeetingActionTypes.MeetingUpdated;
    constructor(public payload: {
        partialMeeting: Update<Meeting>,
        meeting: Meeting
    }) { }
}


export class MeetingStatusOnServerUpdated implements Action {
    readonly type = MeetingActionTypes.MeetingStatusOnServerUpdated;
    constructor(public payload: { id: number, is_active: boolean }) { }
}


export class MeetingStatusUpdated implements Action {
    readonly type = MeetingActionTypes.MeetingStatusUpdated;
    constructor(public payload: { id: number, is_active: boolean }) { }
}


export class MeetingsStatusOnServerUpdated implements Action {
    readonly type = MeetingActionTypes.MeetingsStatusOnServerUpdated;
    constructor(public payload: {
        meetings: Meeting[],
        is_active: number
    }) { }
}


export class MeetingsStatusUpdated implements Action {
    readonly type = MeetingActionTypes.MeetingsStatusUpdated;
    constructor(public payload: {
        meetings: Meeting[],
        is_active: number
    }) { }
}

export class MeetingOnServerDeleted implements Action {
    readonly type = MeetingActionTypes.MeetingOnServerDeleted;
    constructor(public payload: { id: number }) { }
}

export class MeetingDeleted implements Action {
    readonly type = MeetingActionTypes.MeetingDeleted;
    constructor(public payload: { id: number }) { }
}

export class ManyMeetingsOnServerDeleted implements Action {
    readonly type = MeetingActionTypes.ManyMeetingsOnServerDeleted;
    constructor(public payload: { ids: any[] }) { }
}

export class ManyMeetingsDeleted implements Action {
    readonly type = MeetingActionTypes.ManyMeetingsDeleted;
    constructor(public payload: { ids: any[] }) { }
}



export class MeetingsPageRequested implements Action {
    readonly type = MeetingActionTypes.MeetingsPageRequested;
    constructor(public payload: { page: QueryParamsModel }) {
    }
}

export class MeetingsPageLoaded implements Action {
    readonly type = MeetingActionTypes.MeetingsPageLoaded;
    constructor(public payload: { meetings: Meeting[], totalCount: number, page: QueryParamsModel }) { }
}


export class MeetingsPageCancelled implements Action {
    readonly type = MeetingActionTypes.MeetingsPageCancelled;
}

export class MeetingsPageToggleLoading implements Action {
    readonly type = MeetingActionTypes.MeetingsPageToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class MeetingsActionToggleLoading implements Action {
    readonly type = MeetingActionTypes.MeetingsActionToggleLoading;
    constructor(public payload: { isLoading: boolean }) { }
}

export class ChangePassword implements Action {
    readonly type = MeetingActionTypes.ChangePassword;
    constructor(public payload: { meetingName: string, oldPassword: string, newPassword: string }) { }
}



export type MeetingActions = MeetingCreated
    | MeetingUpdated
    | MeetingDeleted
    | ManyMeetingsDeleted
    | MeetingOnServerCreated
    | MeetingOnServerUpdated
    | MeetingOnServerDeleted
    | ManyMeetingsOnServerDeleted
    | MeetingStatusOnServerUpdated
    | MeetingsStatusOnServerUpdated
    | MeetingsPageLoaded
    | MeetingsPageCancelled
    | MeetingsPageToggleLoading
    | MeetingsPageRequested
    | ChangePassword
    | MeetingStatusUpdated
    | MeetingsStatusUpdated
    | ThrowError
    | ClearErrors
    | MeetingsActionToggleLoading;
