// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { MeetingActions, MeetingActionTypes } from '../_actions/meeting.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Meeting } from '../_models/meeting.model';

// tslint:disable-next-line:no-empty-interface
export interface MeetingsState extends EntityState<Meeting> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedMeetingId: number;
  lastQuery: QueryParamsModel;
  error:any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Meeting> = createEntityAdapter<Meeting>();

export const initialMeetingsState: MeetingsState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedMeetingId: undefined,
  showInitWaitingMessage: true
});

export function meetingsReducer(state = initialMeetingsState, action: MeetingActions): MeetingsState {
  switch (action.type) {
    case MeetingActionTypes.MeetingsPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedMeetingId: undefined
      };
    case MeetingActionTypes.MeetingsActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case MeetingActionTypes.MeetingOnServerCreated:
      return {
        ...state
      };
    case MeetingActionTypes.MeetingOnServerUpdated:
      return {
        ...state
      };
    case MeetingActionTypes.MeetingStatusOnServerUpdated:
      return {
        ...state
      };
    case MeetingActionTypes.MeetingsStatusOnServerUpdated:
      return {
        ...state
      };
    case MeetingActionTypes.MeetingOnServerDeleted:
      return {
        ...state
      };
    case MeetingActionTypes.ManyMeetingsOnServerDeleted:
      return {
        ...state
      };
    case MeetingActionTypes.MeetingCreated:
      return adapter.addOne(action.payload.meeting, {
        ...state, lastCreatedMeetingId: action.payload.meeting.id
      });
    case MeetingActionTypes.MeetingUpdated:
      return adapter.updateOne(action.payload.partialMeeting, state);
    case MeetingActionTypes.MeetingStatusUpdated: {
      // tslint:disable-next-line
      const _partialMeeting: Update<Meeting> = {
        id: action.payload.id,
        changes: {
        //   is_active: action.payload.is_active
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialMeeting, state);
      }
    case MeetingActionTypes.MeetingsStatusUpdated: {
      // tslint:disable-next-line
      const _partialMeetings: Update<Meeting>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.meetings.length; i++) {
        _partialMeetings.push({
          id: action.payload.meetings[i].id,
          changes: {
            // is_active: action.payload.is_active === 1 ? true : false
          }
        });
      }
      return adapter.updateMany(_partialMeetings, state);
    }
    case MeetingActionTypes.MeetingDeleted:
      return adapter.removeOne(action.payload.id, state);
    case MeetingActionTypes.ManyMeetingsDeleted:
      const _ids = []
      action.payload.ids.forEach((obj) => _ids.push(obj.id));
      return adapter.removeMany(_ids, state);
    case MeetingActionTypes.MeetingsPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case MeetingActionTypes.MeetingsPageLoaded: {
      return adapter.addMany(action.payload.meetings, {
        ...initialMeetingsState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false
      });
    }
    case MeetingActionTypes.ThrowError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case MeetingActionTypes.ClearErrors: {
      return {
        ...state, error: null
      };
    }
    default:
      return state;
  }
}

export const getMeetingState = createFeatureSelector<MeetingsState>('meetings');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
