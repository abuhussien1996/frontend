import {createFeatureSelector} from '@ngrx/store';
import {createEntityAdapter, EntityAdapter, EntityState, Update} from '@ngrx/entity';
import {DocumentLibraryActions, DocumentLibraryActionTypes} from '../_actions/document-library.actions';
import {QueryParamsModel} from '../../_base/crud';
import {DocumentLibrary} from '../_models/document-library.model';

export interface DocumentLibraryState extends EntityState<DocumentLibrary> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedDocumentLibraryId: number;
  lastQuery: QueryParamsModel;
  error: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<DocumentLibrary> = createEntityAdapter<DocumentLibrary>();

export const initialDocumentLibraryState: DocumentLibraryState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedDocumentLibraryId: undefined,
  showInitWaitingMessage: true
});

export function documentLibraryReducer(state = initialDocumentLibraryState, action: DocumentLibraryActions): DocumentLibraryState {
  switch (action.type) {
    case DocumentLibraryActionTypes.DocumentLibraryPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedDocumentLibraryId: undefined
      };
    case DocumentLibraryActionTypes.DocumentLibraryActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case DocumentLibraryActionTypes.DocumentLibraryPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case DocumentLibraryActionTypes.DocumentLibraryPageLoaded: {
      return adapter.addMany(action.payload.documentLibrary, {
        ...initialDocumentLibraryState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false
      });
    }

    case DocumentLibraryActionTypes.ThrowDocumentLibraryError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case DocumentLibraryActionTypes.ClearDocumentLibraryErrors: {
      return {
        ...state, error: null
      };
    }
    default:
      return state;
  }
}

export const getDocumentLibraryState = createFeatureSelector<DocumentLibraryState>('documentLibrary');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
