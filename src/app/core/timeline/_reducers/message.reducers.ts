// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { MessageActions, MessageActionTypes } from '../_actions/message.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Message } from '../_models/message.model';

// tslint:disable-next-line:no-empty-interface
export interface MessagesState extends EntityState<Message> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedMessageId: number;
  lastQuery: QueryParamsModel;
  error: any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Message> = createEntityAdapter<Message>();

export const initialMessagesState: MessagesState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedMessageId: undefined,
  showInitWaitingMessage: true
});

export function messagesReducer(state = initialMessagesState, action: MessageActions): MessagesState {
  switch (action.type) {
    case MessageActionTypes.MessagesPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedMessageId: undefined
      };
    case MessageActionTypes.MessagesActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case MessageActionTypes.MessageOnServerCreated:
      return {
        ...state
      };
    case MessageActionTypes.MessageOnServerUpdated:
      return {
        ...state
      };
    case MessageActionTypes.MessageStatusOnServerUpdated:
      return {
        ...state
      };
    case MessageActionTypes.MessagesStatusOnServerUpdated:
      return {
        ...state
      };
    case MessageActionTypes.MessageOnServerDeleted:
      return {
        ...state
      };
    case MessageActionTypes.ManyMessagesOnServerDeleted:
      return {
        ...state
      };
    case MessageActionTypes.MessageCreated:
      return adapter.addOne(action.payload.message, {
        ...state, lastCreatedMessageId: action.payload.message.id
      });
    case MessageActionTypes.MessageUpdated:
      return adapter.updateOne(action.payload.partialMessage, state);
    case MessageActionTypes.MessageStatusUpdated: {
      // tslint:disable-next-line
      const _partialMessage: Update<Message> = {
        id: action.payload.id,
        changes: {
          is_completed: action.payload.is_completed
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialMessage, state);
    }
    case MessageActionTypes.MessagesStatusUpdated: {
      // tslint:disable-next-line
      const _partialMessages: Update<Message>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.messages.length; i++) {
        _partialMessages.push({
          id: action.payload.messages[i].id,
          changes: {
            // is_active: action.payload.is_active === 1 ? true : false
          }
        });
      }
      return adapter.updateMany(_partialMessages, state);
    }
    case MessageActionTypes.MessageDeleted:
      return adapter.removeOne(action.payload.id, state);
    case MessageActionTypes.ManyMessagesDeleted:
      const _ids = []
      action.payload.ids.forEach((obj) => _ids.push(obj.id));
      return adapter.removeMany(_ids, state);
    case MessageActionTypes.MessagesPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case MessageActionTypes.MessagesPageLoaded: {
      return adapter.addMany(action.payload.messages, {
        ...initialMessagesState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false
      });
    }
    case MessageActionTypes.AddNewMessages: {
      return adapter.addMany(action.payload.messages, {
        ...state,
        totalCount: state.totalCount + action.payload.addCount,
        listLoading: false,
        showInitWaitingMessage: false
      });
    }
    case MessageActionTypes.ThrowMessageError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case MessageActionTypes.ClearMessageErrors: {
      return {
        ...state, error: null
      };
    }
    default:
      return state;
  }
}

export const getMessageState = createFeatureSelector<MessagesState>('messages');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
