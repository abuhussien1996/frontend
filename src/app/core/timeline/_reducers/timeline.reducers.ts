// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { TimelineActions, TimelineActionTypes } from '../_actions/timeline.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { Timeline } from '../_models/timeline.model';

// tslint:disable-next-line:no-empty-interface
export interface TimelinesState extends EntityState<Timeline> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedTimelineId: number;
  lastQuery: QueryParamsModel;
  error:any;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<Timeline> = createEntityAdapter<Timeline>();

export const initialTimelinesState: TimelinesState = adapter.getInitialState({
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  error: null,
  lastQuery: new QueryParamsModel({}),
  lastCreatedTimelineId: undefined,
  showInitWaitingMessage: true
});

export function timelinesReducer(state = initialTimelinesState, action: TimelineActions): TimelinesState {
  switch (action.type) {
    case TimelineActionTypes.TimelinesPageToggleLoading:
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedTimelineId: undefined
      };
    case TimelineActionTypes.TimelinesActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case TimelineActionTypes.TimelineOnServerCreated:
      return {
        ...state
      };
    case TimelineActionTypes.TimelineOnServerUpdated:
      return {
        ...state
      };
    case TimelineActionTypes.TimelineStatusOnServerUpdated:
      return {
        ...state
      };
    case TimelineActionTypes.TimelinesStatusOnServerUpdated:
      return {
        ...state
      };
    case TimelineActionTypes.TimelineOnServerDeleted:
      return {
        ...state
      };
    case TimelineActionTypes.ManyTimelinesOnServerDeleted:
      return {
        ...state
      };
    case TimelineActionTypes.TimelineCreated:
      return adapter.addOne(action.payload.timeline, {
        ...state, lastCreatedTimelineId: action.payload.timeline.id
      });
    case TimelineActionTypes.TimelineUpdated:
      return adapter.updateOne(action.payload.partialTimeline, state);
    case TimelineActionTypes.TimelineStatusUpdated: {
      // tslint:disable-next-line
      const _partialTimeline: Update<Timeline> = {
        id: action.payload.id,
        changes: {
        //   is_active: action.payload.is_active
        }
      };
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      return adapter.updateOne(_partialTimeline, state);
      }
    case TimelineActionTypes.TimelinesStatusUpdated: {
      // tslint:disable-next-line
      const _partialTimelines: Update<Timeline>[] = [];
      // tslint:disable-next-line:prefer-const
      // tslint:disable-next-line
      for (let i = 0; i < action.payload.timelines.length; i++) {
        _partialTimelines.push({
          id: action.payload.timelines[i].id,
          changes: {
            // is_active: action.payload.is_active === 1 ? true : false
          }
        });
      }
      return adapter.updateMany(_partialTimelines, state);
    }
    case TimelineActionTypes.TimelineDeleted:
      return adapter.removeOne(action.payload.id, state);
    case TimelineActionTypes.ManyTimelinesDeleted:
      const _ids = []
      action.payload.ids.forEach((obj) => _ids.push(obj.id));
      return adapter.removeMany(_ids, state);
    case TimelineActionTypes.TimelinesPageCancelled:
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    case TimelineActionTypes.TimelinesPageLoaded: {
      
      return adapter.addMany(action.payload.timelines, {
        ...initialTimelinesState,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        listLoading: false,
        showInitWaitingMessage: false
      });
    }
    case TimelineActionTypes.ThrowTimelineError: {
      return {
        ...state, error: action.payload.error, actionsloading: false, listLoading: false
      };
    }
    case TimelineActionTypes.ClearTimelineErrors: {
      return {
        ...state, error: null
      };
    }
    default:
      return state;
  }
}

export const getTimelineState = createFeatureSelector<TimelinesState>('timelines');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
