// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { TaskActions, TaskActionTypes } from '../_actions/task.actions';
// Models
import { Task } from '../_models/task.model';

// tslint:disable-next-line:no-empty-interface
export interface TasksState extends EntityState<Task> {
  actionsloading: boolean;
  selectedUsers: number[];
  totalCount: number;
  lastCreatedTaskId: number;
  error:any;
  latestSuccessfullAction: any;
}

export const adapter: EntityAdapter<Task> = createEntityAdapter<Task>();
export const initialTasksState: TasksState = adapter.getInitialState({
  actionsloading: false,
  selectedUsers: [0],
  totalCount: 0,
  error: null,
  latestSuccessfullAction: null,
  lastCreatedTaskId: undefined,
});

export function tasksReducer(state = initialTasksState, action: TaskActions): TasksState {
  switch (action.type) {
    case TaskActionTypes.TaskActionToggleLoading:
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    case TaskActionTypes.AllTasksLoaded: {
      return adapter.addMany(action.payload.tasks, {
        ...initialTasksState,
        totalCount: action.payload.tasks.length,
        selectedUsers: action.payload.user_ids ? action.payload.user_ids : initialTasksState.selectedUsers,
        error: null
      });
    }
    case TaskActionTypes.TaskCreated:
      return adapter.addOne(action.payload.task, {
        ...state, lastCreatedTaskId: action.payload.task.id
      });
    case TaskActionTypes.TaskUpdated:
      return adapter.updateOne(action.payload.partialTask, state);
    case TaskActionTypes.TaskDeleted:
      return adapter.removeOne(action.payload.id, state);
    case TaskActionTypes.ThrowTaskError: {
      return {
        ...state, error: action.payload.error, actionsloading: false
      };
    }
    case TaskActionTypes.FireTaskSuccess: {
      return {
        ...state, latestSuccessfullAction: {action_type: action.payload.action_type}, error: null
      };
    }
    default:
      return state;
  }
}

export const getTaskState = createFeatureSelector<TasksState>('tasks');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
