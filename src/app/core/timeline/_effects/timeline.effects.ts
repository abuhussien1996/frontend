// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, catchError } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { AuthService } from '../../auth/_services';
// State
import { AppState } from '../../reducers';
import {
  TimelineActionTypes,
  TimelinesPageRequested,
  TimelinesPageLoaded,
  TimelineCreated,
  TimelineDeleted,
  ManyTimelinesDeleted,
  TimelineUpdated,
  TimelineOnServerCreated,
  TimelinesActionToggleLoading,
  TimelinesPageToggleLoading,
  TimelinesStatusUpdated,
  ChangePassword,
  ThrowTimelineError,
  ClearTimelineErrors,
  TimelineOnServerUpdated,
  TimelinesPageCancelled,
  TimelineOnServerDeleted,
  ManyTimelinesOnServerDeleted,
  TimelineStatusOnServerUpdated,
  TimelineStatusUpdated,
  TimelinesStatusOnServerUpdated,
  // TimelineStatusUpdated
} from '../_actions/timeline.actions';
import { Router } from '@angular/router';
import { TimelineService } from '../_services/timeline.service';

@Injectable()
export class TimelineEffects {
  showPageLoadingDistpatcher = new TimelinesPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new TimelinesPageToggleLoading({ isLoading: false });

  showActionLoadingDistpatcher = new TimelinesActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new TimelinesActionToggleLoading({ isLoading: false });


  // done
  // @Effect()
  // loadTimelinesPage$ = this.actions$
  //   .pipe(
  //     ofType<TimelinesPageRequested>(TimelineActionTypes.TimelinesPageRequested),
  //     mergeMap(({ payload }) => {
  //       this.store.dispatch(this.showPageLoadingDistpatcher);
  //       const requestToServer = this.timeline.findTimelines(payload.type,payload.id);
  //       const lastQuery = of(payload.id);
  //       return forkJoin(requestToServer, lastQuery).pipe(
  //         catchError(error => {
  //           return of([error]);
  //         }),
  //         map(response => {
  //           if(response[0].error) {
  //             if(response[0].status === 403 || response[0].status === 401) {
  //               this.router.navigateByUrl('/error/403');
  //               return new TimelinesPageCancelled();
  //             } else {
  //               this.store.dispatch(new ThrowTimelineError({error: response[0].error}));
  //               return new TimelinesPageCancelled();
  //             }
  //           } else {
  //             const result = response[0];
  //             // tslint:disable-next-line:no-shadowed-variable
  //             const lastQuery: QueryParamsModel = response[1];
  //             this.store.dispatch(new ClearTimelineErrors());
  //             return new TimelinesPageLoaded({
  //               timelines: result.data,
  //               totalCount: result.count,
  //               page: lastQuery
  //             });
  //           }
  //         })
  //       );
  //     })
  //   );

//   @Effect()
//   deleteTimeline$ = this.actions$
//     .pipe(
//       ofType<TimelineOnServerDeleted>(TimelineActionTypes.TimelineOnServerDeleted),
//       mergeMap(({ payload }) => {
//         console.log(payload);

//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.deleteTimeline(payload.id).pipe(
//           catchError(error => {

//             return of({error});
//           }),
//           map((res) => {
//             this.store.dispatch(this.hideActionLoadingDistpatcher);
//             return new TimelineDeleted(payload)

//           })
//         );
//         }
//       ),
//     );

//   @Effect()
//   deleteTimelines$ = this.actions$
//     .pipe(
//       ofType<ManyTimelinesOnServerDeleted>(TimelineActionTypes.ManyTimelinesOnServerDeleted),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.deleteTimelines(payload.ids).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearErrors());
//               return new ManyTimelinesDeleted(payload)
//             }
//           })
//         );
//         }
//       ),
//     );

  // done

//   @Effect()
//   updateTimeline$ = this.actions$
//     .pipe(
//       ofType<TimelineOnServerUpdated>(TimelineActionTypes.TimelineOnServerUpdated),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.updateTimeline(payload.timeline).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowTimelineError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearTimelineErrors());
//               return new TimelineUpdated(payload)
//             }
//           })
//         );
//       }),
//     );


//   @Effect()
//   updateTimelineStatus$ = this.actions$
//     .pipe(
//       ofType<TimelineStatusOnServerUpdated>(TimelineActionTypes.TimelineStatusOnServerUpdated),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.updatStatusSingle(payload.id, payload.isActive).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearErrors());
//               return new TimelineStatusUpdated(payload)
//             }
//           })
//         );
//       }),
//     );


//   @Effect()
//   createTimeline$ = this.actions$
//     .pipe(
//       ofType<TimelineOnServerCreated>(TimelineActionTypes.TimelineOnServerCreated),
//       mergeMap(({ payload }) => {
//         console.log(payload);

//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.createTimeline(payload.timeline).pipe(
//           catchError(error => {
//             console.log(error);

//             return of(error);
//           }),
//           map((res) => {
//             console.log(res.status);
//             if(res.status == 201) {
//               this.store.dispatch(new ClearTimelineErrors());
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               return new TimelineCreated({timeline: res})

//             } else {
//               return new ThrowTimelineError({error: res});
//             }
//           })
//         )
//       }),
//     );



//   @Effect()
//   UpdatedManyTimelinesStatus$ = this.actions$
//     .pipe(
//       ofType<TimelinesStatusOnServerUpdated>(TimelineActionTypes.TimelinesStatusOnServerUpdated),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.updateStatusForTimelines(payload.timelines, payload.isActive).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearErrors());
//               return new TimelinesStatusUpdated(payload)
//             }
//           })
//         );
//       }),
//     );

//   @Effect()
//   changepassword$ = this.actions$
//     .pipe(
//       ofType<ChangePassword>(TimelineActionTypes.ChangePassword),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.timeline.changePassword(payload.timelineName, payload.oldPassword, payload.newPassword);
//       }),
//       map(() => {
//         return this.hideActionLoadingDistpatcher;
//       }),
//     );


  constructor(
    private actions$: Actions,
    private timeline: TimelineService,
    private store: Store<AppState>,
    private router: Router
    ) {
  }
}
