// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, catchError } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { AuthService } from '../../../core/auth/_services';
// State
import { AppState } from '../../../core/reducers';
import {
  MeetingActionTypes,
  MeetingsPageRequested,
  MeetingsPageLoaded,
  MeetingCreated,
  MeetingDeleted,
  ManyMeetingsDeleted,
  MeetingUpdated,
  MeetingOnServerCreated,
  MeetingsActionToggleLoading,
  MeetingsPageToggleLoading,
  MeetingsStatusUpdated,
  ChangePassword,
  ThrowError,
  ClearErrors,
  MeetingOnServerUpdated,
  MeetingsPageCancelled,
  MeetingOnServerDeleted,
  ManyMeetingsOnServerDeleted,
  MeetingStatusOnServerUpdated,
  MeetingStatusUpdated,
  MeetingsStatusOnServerUpdated,
  // MeetingStatusUpdated
} from '../_actions/meeting.actions';
import { Router } from '@angular/router';
import { MeetingService } from '../_services/meeting.service';

@Injectable()
export class MeetingEffects {
  showPageLoadingDistpatcher = new MeetingsPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new MeetingsPageToggleLoading({ isLoading: false });

  showActionLoadingDistpatcher = new MeetingsActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new MeetingsActionToggleLoading({ isLoading: false });


  // done
  @Effect()
  loadMeetingsPage$ = this.actions$
    .pipe(
      ofType<MeetingsPageRequested>(MeetingActionTypes.MeetingsPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.meeting.findMeetings(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 403 || response[0].status === 401) {
                this.router.navigateByUrl('/error/403');
                return new MeetingsPageCancelled();
              } else {
                this.store.dispatch(new ThrowError({ error: response[0].error }));
                return new MeetingsPageCancelled();
              }
            } else {
              const result = response[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery: QueryParamsModel = response[1];
              this.store.dispatch(new ClearErrors());
              return new MeetingsPageLoaded({
                meetings: result.data,
                totalCount: result.count,
                page: lastQuery
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteMeeting$ = this.actions$
    .pipe(
      ofType<MeetingOnServerDeleted>(MeetingActionTypes.MeetingOnServerDeleted),
      mergeMap(({ payload }) => {
        console.log(payload);

        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.meeting.deleteMeeting(payload.id).pipe(
          catchError(error => {

            return of({ error });
          }),
          map((res) => {
            this.store.dispatch(this.hideActionLoadingDistpatcher);
            return new MeetingDeleted(payload)

          })
        );
      }
      ),
    );

  //   @Effect()
  //   deleteMeetings$ = this.actions$
  //     .pipe(
  //       ofType<ManyMeetingsOnServerDeleted>(MeetingActionTypes.ManyMeetingsOnServerDeleted),
  //       mergeMap(({ payload }) => {
  //         this.store.dispatch(this.showActionLoadingDistpatcher);
  //         return this.meeting.deleteMeetings(payload.ids).pipe(
  //           catchError(error => {
  //             return of({error});
  //           }),
  //           map((res) => {
  //             if(res.error) {
  //               return new ThrowError({error: res.error});
  //             } else {
  //               this.store.dispatch(this.hideActionLoadingDistpatcher);
  //               this.store.dispatch(new ClearErrors());
  //               return new ManyMeetingsDeleted(payload)
  //             }
  //           })
  //         );
  //         }
  //       ),
  //     );

  // done

  @Effect()
  updateMeeting$ = this.actions$
    .pipe(
      ofType<MeetingOnServerUpdated>(MeetingActionTypes.MeetingOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.meeting.updateMeeting(payload.meeting).pipe(
          catchError(error => {
            return of({ error });
          }),
          map((res) => {
            if (res.error) {
              return new ThrowError({ error: res.error });
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new ClearErrors());
              return new MeetingUpdated(payload)
            }
          })
        );
      }),
    );


  //   @Effect()
  //   updateMeetingStatus$ = this.actions$
  //     .pipe(
  //       ofType<MeetingStatusOnServerUpdated>(MeetingActionTypes.MeetingStatusOnServerUpdated),
  //       mergeMap(({ payload }) => {
  //         this.store.dispatch(this.showActionLoadingDistpatcher);
  //         return this.meeting.updatStatusSingle(payload.id, payload.isActive).pipe(
  //           catchError(error => {
  //             return of({error});
  //           }),
  //           map((res) => {
  //             if(res.error) {
  //               return new ThrowError({error: res.error});
  //             } else {
  //               this.store.dispatch(this.hideActionLoadingDistpatcher);
  //               this.store.dispatch(new ClearErrors());
  //               return new MeetingStatusUpdated(payload)
  //             }
  //           })
  //         );
  //       }),
  //     );


  @Effect()
  createMeeting$ = this.actions$
    .pipe(
      ofType<MeetingOnServerCreated>(MeetingActionTypes.MeetingOnServerCreated),
      mergeMap(({ payload }) => {
        console.log(payload);

        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.meeting.createMeeting(payload.request,payload.meeting).pipe(
          catchError(error => {
            console.log(error);

            return of(error);
          }),
          map((res) => {

            console.log('uuid',res.body.data.pre_signed_urls[0].uuid);

            console.log(res.status);
            if (res.status == 201) {
              this.store.dispatch(new ClearErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new MeetingCreated({ meeting: res })

            } else {
              return new ThrowError({ error: res });
            }
          })
        )
      }),
    );



  //   @Effect()
  //   UpdatedManyMeetingsStatus$ = this.actions$
  //     .pipe(
  //       ofType<MeetingsStatusOnServerUpdated>(MeetingActionTypes.MeetingsStatusOnServerUpdated),
  //       mergeMap(({ payload }) => {
  //         this.store.dispatch(this.showActionLoadingDistpatcher);
  //         return this.meeting.updateStatusForMeetings(payload.meetings, payload.isActive).pipe(
  //           catchError(error => {
  //             return of({error});
  //           }),
  //           map((res) => {
  //             if(res.error) {
  //               return new ThrowError({error: res.error});
  //             } else {
  //               this.store.dispatch(this.hideActionLoadingDistpatcher);
  //               this.store.dispatch(new ClearErrors());
  //               return new MeetingsStatusUpdated(payload)
  //             }
  //           })
  //         );
  //       }),
  //     );

  //   @Effect()
  //   changepassword$ = this.actions$
  //     .pipe(
  //       ofType<ChangePassword>(MeetingActionTypes.ChangePassword),
  //       mergeMap(({ payload }) => {
  //         this.store.dispatch(this.showActionLoadingDistpatcher);
  //         return this.meeting.changePassword(payload.meetingName, payload.oldPassword, payload.newPassword);
  //       }),
  //       map(() => {
  //         return this.hideActionLoadingDistpatcher;
  //       }),
  //     );


  constructor(
    private actions$: Actions,
    private meeting: MeetingService,
    private store: Store<AppState>,
    private router: Router
  ) {
  }
}
