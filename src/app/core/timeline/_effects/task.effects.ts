import { of } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { map, mergeMap, catchError } from 'rxjs/operators';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// Services
import { TaskService } from '../_services/task.service';
// State
import { AppState } from '../../reducers';
// Actions
import {
    TaskActionToggleLoading,
    TaskActionTypes,
    TaskCreated,
    TaskOnServerCreated,
    TaskUpdated,
    TaskOnServerUpdated,
    AllTasksRequested,
    ThrowTaskError,
    FireTaskSuccess,
    AllTasksLoaded
} from '../_actions/task.actions';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/_services/auth.service';

@Injectable()
export class TaskEffects {
    showActionLoadingDistpatcher = new TaskActionToggleLoading({ isLoading: true });
    hideActionLoadingDistpatcher = new TaskActionToggleLoading({ isLoading: false });

    @Effect()
    loadTasks$ = this.actions$.pipe(
        ofType<AllTasksRequested>(TaskActionTypes.AllTasksRequested),
        mergeMap(({ payload }) => {
            this.store.dispatch(this.showActionLoadingDistpatcher);
            return this.taskService.getUserTasks(payload.start , payload.end, payload.user_ids? payload.user_ids : null).pipe(
                catchError(error => {
                    return of(error);
                }),
                map((res) => {
                    if (res.error) {
                        if(res.status === 401) {
                          this.authService.logout();
                          return this.hideActionLoadingDistpatcher;
                        }
                        else if(res.status === 403) {
                          this.router.navigateByUrl('/error/403');
                          return this.hideActionLoadingDistpatcher;
                        } else {
                            return new ThrowTaskError({ error: res });
                        }
                    } else {
                        this.store.dispatch(this.hideActionLoadingDistpatcher);
                        if(payload.user_ids) {
                            return new AllTasksLoaded({ tasks: res.data, user_ids: payload.user_ids });
                        } else {
                            return new AllTasksLoaded({ tasks: res.data });
                        }
                    }
                })
            )
        }),
    );

    @Effect()
    createTask$ = this.actions$
        .pipe(
            ofType<TaskOnServerCreated>(TaskActionTypes.TaskOnServerCreated),
            mergeMap(({ payload }) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.taskService.createTask2(payload.task).pipe(
                    catchError(error => {
                        return of(error);
                    }),
                    map((res) => {
                        if (res.error) {
                            if(res.status === 401) {
                              this.authService.logout();
                              return this.hideActionLoadingDistpatcher;
                            }
                            else if(res.status === 403) {
                              this.router.navigateByUrl('/error/403');
                              return this.hideActionLoadingDistpatcher;
                            } else {
                                return new ThrowTaskError({ error: res });
                            }
                        } else {
                            this.store.dispatch(this.hideActionLoadingDistpatcher);
                            this.store.dispatch(new TaskCreated({ task: res.data }));
                            return new FireTaskSuccess({ action_type: TaskActionTypes.TaskCreated })
                        }
                    })
                )
            }),
        );

    @Effect()
    updateTask$ = this.actions$
        .pipe(
            ofType<TaskOnServerUpdated>(TaskActionTypes.TaskOnServerUpdated),
            mergeMap(({ payload }) => {
                this.store.dispatch(this.showActionLoadingDistpatcher);
                return this.taskService.updateTask2(payload.task).pipe(
                    catchError(error => {
                        return of({ error });
                    }),
                    map((res) => {
                        if (res.error) {
                            if(res.status === 401) {
                              this.authService.logout();
                              return this.hideActionLoadingDistpatcher;
                            }
                            else if(res.status === 403) {
                              this.router.navigateByUrl('/error/403');
                              return this.hideActionLoadingDistpatcher;
                            } else {
                                return new ThrowTaskError({ error: res });
                            }
                        } else {
                            this.store.dispatch(this.hideActionLoadingDistpatcher);
                            this.store.dispatch(new FireTaskSuccess({ action_type: TaskActionTypes.TaskUpdated }));
                            return new TaskUpdated(payload)
                        }
                    })
                );
            }),
        );

    constructor(
        private actions$: Actions,
        private taskService: TaskService,
        private authService: AuthService,
        private store: Store<AppState>,
        private router: Router
    ) {}
}
