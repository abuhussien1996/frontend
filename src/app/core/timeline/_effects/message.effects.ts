// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, catchError } from 'rxjs/operators';
import { Observable, defer, of, forkJoin } from 'rxjs';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
// CRUD
import { QueryResultsModel, QueryParamsModel } from '../../_base/crud';
// Services
import { AuthService } from '../../auth/_services';
// State
import { AppState } from '../../reducers';
import {
  MessageActionTypes,
  MessagesPageRequested,
  MessagesPageLoaded,
  MessageCreated,
  MessageDeleted,
  ManyMessagesDeleted,
  MessageUpdated,
  MessageOnServerCreated,
  MessagesActionToggleLoading,
  MessagesPageToggleLoading,
  MessagesStatusUpdated,
  ChangePassword,
  ThrowMessageError,
  ClearMessageErrors,
  MessageOnServerUpdated,
  MessagesPageCancelled,
  MessageOnServerDeleted,
  ManyMessagesOnServerDeleted,
  MessageStatusOnServerUpdated,
  MessageStatusUpdated,
  MessagesStatusOnServerUpdated,
  // MessageStatusUpdated
} from '../_actions/message.actions';
import { Router } from '@angular/router';
import { MessageService } from '../_services/message.service';

@Injectable()
export class MessageEffects {
  showPageLoadingDistpatcher = new MessagesPageToggleLoading({ isLoading: true });
  hidePageLoadingDistpatcher = new MessagesPageToggleLoading({ isLoading: false });

  showActionLoadingDistpatcher = new MessagesActionToggleLoading({ isLoading: true });
  hideActionLoadingDistpatcher = new MessagesActionToggleLoading({ isLoading: false });


  // done
  @Effect()
  loadMessagesPage$ = this.actions$
    .pipe(
      ofType<MessagesPageRequested>(MessageActionTypes.MessagesPageRequested),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.message.findMessages(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if(response[0].error) {
              if(response[0].status === 403 || response[0].status === 401) {
                this.router.navigateByUrl('/error/403');
                return new MessagesPageCancelled();
              } else {
                this.store.dispatch(new ThrowMessageError({error: response[0].error}));
                return new MessagesPageCancelled();
              }
            } else {
              const result = response[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery: QueryParamsModel = response[1];
              this.store.dispatch(new ClearMessageErrors());
              return new MessagesPageLoaded({
                messages: result.data,
                totalCount: result.count,
                page: lastQuery
              });
            }
          })
        );
      })
    );

  @Effect()
  deleteMessage$ = this.actions$
    .pipe(
      ofType<MessageOnServerDeleted>(MessageActionTypes.MessageOnServerDeleted),
      mergeMap(({ payload }) => {
        console.log(payload);
        
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.message.deleteMessage(payload.id).pipe(
          catchError(error => {
            
            return of({error});
          }),
          map((res) => {
            this.store.dispatch(this.hideActionLoadingDistpatcher);
            return new MessageDeleted(payload)
            
          })
        );
        }
      ),
    );

//   @Effect()
//   deleteMessages$ = this.actions$
//     .pipe(
//       ofType<ManyMessagesOnServerDeleted>(MessageActionTypes.ManyMessagesOnServerDeleted),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.message.deleteMessages(payload.ids).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearErrors());
//               return new ManyMessagesDeleted(payload)
//             }
//           })
//         );
//         }
//       ),
//     );

  // done

  @Effect()
  updateMessage$ = this.actions$
    .pipe(
      ofType<MessageOnServerUpdated>(MessageActionTypes.MessageOnServerUpdated),
      mergeMap(({ payload }) => {
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.message.updateMessage(payload.message).pipe(
          catchError(error => {
            return of({error});
          }),
          map((res) => {
            if(res.error) {
              return new ThrowMessageError({error: res.error});
            } else {
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              this.store.dispatch(new ClearMessageErrors());
              return new MessageUpdated(payload)
            }
          })
        );
      }),
    );


//   @Effect()
//   updateMessageStatus$ = this.actions$
//     .pipe(
//       ofType<MessageStatusOnServerUpdated>(MessageActionTypes.MessageStatusOnServerUpdated),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.message.updatStatusSingle(payload.id, payload.isActive).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearErrors());
//               return new MessageStatusUpdated(payload)
//             }
//           })
//         );
//       }),
//     );


  @Effect()
  createMessage$ = this.actions$
    .pipe(
      ofType<MessageOnServerCreated>(MessageActionTypes.MessageOnServerCreated),
      mergeMap(({ payload }) => {
        console.log(payload);
        
        this.store.dispatch(this.showActionLoadingDistpatcher);
        return this.message.createMessage(payload.request,payload.message).pipe(
          catchError(error => {
            console.log(error);
            
            return of(error);
          }),
          map((res) => {
            console.log(res.status);
            if(res.status == 201) {
              this.store.dispatch(new ClearMessageErrors());
              this.store.dispatch(this.hideActionLoadingDistpatcher);
              return new MessageCreated({message: res})
             
            } else {
              return new ThrowMessageError({error: res});
            }
          })
        )
      }),
    );
  


//   @Effect()
//   UpdatedManyMessagesStatus$ = this.actions$
//     .pipe(
//       ofType<MessagesStatusOnServerUpdated>(MessageActionTypes.MessagesStatusOnServerUpdated),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.message.updateStatusForMessages(payload.messages, payload.isActive).pipe(
//           catchError(error => {
//             return of({error});
//           }),
//           map((res) => {
//             if(res.error) {
//               return new ThrowError({error: res.error});
//             } else {
//               this.store.dispatch(this.hideActionLoadingDistpatcher);
//               this.store.dispatch(new ClearErrors());
//               return new MessagesStatusUpdated(payload)
//             }
//           })
//         );
//       }),
//     );

//   @Effect()
//   changepassword$ = this.actions$
//     .pipe(
//       ofType<ChangePassword>(MessageActionTypes.ChangePassword),
//       mergeMap(({ payload }) => {
//         this.store.dispatch(this.showActionLoadingDistpatcher);
//         return this.message.changePassword(payload.messageName, payload.oldPassword, payload.newPassword);
//       }),
//       map(() => {
//         return this.hideActionLoadingDistpatcher;
//       }),
//     );


  constructor(
    private actions$: Actions,
    private message: MessageService,
    private store: Store<AppState>,
    private router: Router
    ) {
  }
}
