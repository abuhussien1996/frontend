import {Injectable} from '@angular/core';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {forkJoin, of} from 'rxjs';import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {QueryParamsModel} from '../../_base/crud';
import {AppState} from '../../reducers';
import {
  ClearDocumentLibraryErrors,
  DocumentLibraryActionToggleLoading,
  DocumentLibraryActionTypes,
  DocumentLibraryPageCancelled,
  DocumentLibraryPageLoaded,
  DocumentLibraryPageRequested,
  DocumentLibraryPageToggleLoading,
  ThrowDocumentLibraryError,
} from '../_actions/document-library.actions';
import {Router} from '@angular/router';
import {CommunicationsService} from '../_services';

@Injectable()
export class DocumentLibraryEffects {
  showPageLoadingDistpatcher = new DocumentLibraryPageToggleLoading({isLoading: true});
  hidePageLoadingDistpatcher = new DocumentLibraryPageToggleLoading({isLoading: false});
  showActionLoadingDistpatcher = new DocumentLibraryActionToggleLoading({isLoading: true});
  hideActionLoadingDistpatcher = new DocumentLibraryActionToggleLoading({isLoading: false});

  @Effect()
  loadDocumentLibraryPage$ = this.actions$
    .pipe(
      ofType<DocumentLibraryPageRequested>(DocumentLibraryActionTypes.DocumentLibraryPageRequested),
      mergeMap(({payload}) => {
        this.store.dispatch(this.showPageLoadingDistpatcher);
        const requestToServer = this.communicationsService.findDocumentLibrary(payload.page);
        const lastQuery = of(payload.page);
        return forkJoin(requestToServer, lastQuery).pipe(
          catchError(error => {
            return of([error]);
          }),
          map(response => {
            if (response[0].error) {
              if (response[0].status === 403 || response[0].status === 401) {
                this.router.navigateByUrl('/error/403');
                return new DocumentLibraryPageCancelled();
              } else {
                this.store.dispatch(new ThrowDocumentLibraryError({error: response[0].error}));
                return new DocumentLibraryPageCancelled();
              }
            } else {
              const result = response[0];
              // tslint:disable-next-line:no-shadowed-variable
              const lastQuery: QueryParamsModel = response[1];
              this.store.dispatch(new ClearDocumentLibraryErrors());
              return new DocumentLibraryPageLoaded({
                documentLibrary: result.data,
                totalCount: result.count,
                page: lastQuery
              });
            }
          })
        );
      })
    );

  constructor(
    private actions$: Actions,
    private communicationsService: CommunicationsService,
    private store: Store<AppState>,
    private router: Router
  ) {
  }
}
