// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectMessagesInStore, selectMessagesPageLoading, selectMessagesShowInitWaitingMessage } from '../_selectors/message.selectors';


export class MessagesDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectMessagesPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectMessagesShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectMessagesInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
