import {select, Store} from '@ngrx/store';
import {BaseDataSource, QueryResultsModel} from '../../_base/crud';
import {AppState} from '../../reducers';
import {
  selectDocumentLibraryInStore,
  selectDocumentLibraryPageLoading,
  selectDocumentLibraryShowInitWaitingMessage
} from '../_selectors/document-library.selectors';


export class DocumentLibraryDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectDocumentLibraryPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectDocumentLibraryShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectDocumentLibraryInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
