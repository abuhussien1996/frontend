// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { AppState } from '../../reducers';
import { selectTimelinesInStore, selectTimelinesPageLoading, selectTimelinesShowInitWaitingMessage } from '../_selectors/timeline.selectors';


export class TimelinesDataSource extends BaseDataSource {
  constructor(private store: Store<AppState>) {
    super();

    this.loading$ = this.store.pipe(
      select(selectTimelinesPageLoading)
    );

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectTimelinesShowInitWaitingMessage)
    );

    this.store.pipe(
      select(selectTimelinesInStore)
    ).subscribe((response: QueryResultsModel) => {
      this.paginatorTotalSubject.next(response.totalCount);
      this.entitySubject.next(response.items);
    });
  }
}
