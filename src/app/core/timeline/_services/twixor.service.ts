// Angular
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class TwixorService {

  constructor(private http: HttpClient) {}

  login(email: string, password: string): Observable<any> {
    const url = environment.USER_BASE + '/twixor-login';
    return this.http.post<any>(url, {email, password});
  }
}
