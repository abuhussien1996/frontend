import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Meeting} from '../_models/meeting.model';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {environment} from 'src/environments/environment';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';


@Injectable({providedIn: 'root'})
export class MeetingService {
  private authToken;
  private companyId;

  constructor(
    private httpClient: HttpClient,
    private store: Store<AppState>,
    private httpUtils: HttpUtilsService
    ) {
      this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
      this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
    }

  uploadFiles(url, file): Observable<any> {
    return this.httpClient.put(url, file, {
      observe: 'events',
      reportProgress: true,
    });
  }

  sendFiles(id, uuids): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication/' + id + '/attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      uuids
    }
    return this.httpClient.post<any>(url, body, {headers: httpHeaders});
  }

  createMeeting(request, _meeting: Meeting) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      sender_name: request.body.sender_name,
      subject: _meeting.subject,
      from_time: _meeting.from_time,
      to_time: _meeting.to_time,
      body: _meeting.body,
      location: _meeting.location,
      receivers: _meeting.receivers,
      attachment_names: request.body.attachment_names,
      is_completed: _meeting.is_completed,
      is_important: _meeting.is_important,
      related_to_list: _meeting.related_to_list,
      drug: _meeting.drug
    }
    const params = new HttpParams({fromString: 'communication_type=MEETING'});
    return this.httpClient.post<Meeting>(environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication', body, {
      headers: httpHeaders,
      observe: 'response',
      params
    })
  }


  getAllMeetings(pageIndex?: number, pageSize?: number, searchInput?: any, isActive?: boolean, id?: number): Observable<Meeting[]> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST')
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<Meeting[]>(
      environment.USER_BASE + '/all-meetings',
      {pageIndex, pageSize, searchData: searchInput, isActive, id},
      {headers: httpHeaders}
    );
  }


  getMeetingById(meetingId?: number): Observable<Meeting> {
    return this.httpClient.post<Meeting>(environment.USER_BASE + '/meeting-by-id', {id: meetingId});
  }

  updatStatusSingle(id?: number, isActive?: boolean) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.put<any>(environment.USER_BASE + '/meetingstatusupdatesingle', {id, isActive}, {headers: httpHeaders});
  }


  deleteMeeting(id?: number) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.delete<any>(environment.CORE_BASE + '/companies/' + this.companyId + '/meetings/' + id, {headers: httpHeaders});
  }

  deleteMeetings(ids: any[]) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<any>(environment.USER_BASE + '/multiple-soft-delete-meeting', {ids}, {headers: httpHeaders});
  }

  updateStatusForMeetings(ids?: Meeting[], isActive?: number) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    const idsForUpdate: any[] = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < ids.length; i++) {
      idsForUpdate.push(ids[i].id);
    }
    const body = {
      ids: idsForUpdate,
      isActive
    };
    return this.httpClient.put<any>(environment.USER_BASE + '/meetingstatusupdate', body, {headers: httpHeaders});

  }


  updateMeeting(_meeting: Meeting): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {}
    const id = _meeting.id
    return this.httpClient.put(environment.CORE_BASE + '/companies/' + this.companyId + '/meetings/' + id, body, {headers: httpHeaders}
    );
  }

  findMeetings(queryParams: QueryParamsModel): Observable<any> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const sort = queryParams.sortField.replace(/([-_]\w)/g, g => g[1].toUpperCase())
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({fromString: 'communication_type=MEETING&search_term='+queryParams.filter.searchData+'&sort='+sort+`,${queryParams.sortOrder}`+'&page=' + pageIndex + '&size=' + pageSize});
    return this.httpClient.get<QueryResultsModel>(environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication', {
      headers,
      params
    })
  }

}
