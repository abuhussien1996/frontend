import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import { HttpUtilsService } from '../../_base/crud';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';


@Injectable({providedIn: 'root'})
export class TimelineService {
  private authToken;
  private companyId;

  constructor(
    private httpClient: HttpClient,
    private store: Store<AppState>,
    private httpUtils: HttpUtilsService
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  // @ts-ignore
  findSentTimelines(term, type, id?: number, fromDate, toDate): Observable<any> {
    if (!term) {
      term = ''
    }
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({
      fromString: 'sort=updated,DESC' + '&communication_type=' + type + '&search_term='
        + term + '&from_date=' + toDate + '&to_date=' + fromDate
    });
    return this.httpClient.get<any>(environment.TIMELINE_BASE + '/companies/' + this.companyId +
      '/communication/sent_merge_received/contact/' + id, {
      headers,
      params
    })
  }
}
