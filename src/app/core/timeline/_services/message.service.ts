import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message} from '../_models/message.model';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {environment} from 'src/environments/environment';
import {AuthService} from '../../auth/_services';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';


@Injectable({providedIn: 'root'})
export class MessageService {
  private authToken;
  private companyId;

  constructor(
    private httpClient: HttpClient,
    private store: Store<AppState>,
    private httpUtils: HttpUtilsService
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }


  createMessage(request, _message: Message) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      body: _message.body,
      receivers: _message.receiver,
      attachment_names: request.body.attachment_names,
      sender_name: request.body.sender_name,
      subject: request.body.subject,
    }
    const params = new HttpParams({fromString: 'communication_type=NOTIFICATION'});
    return this.httpClient.post<Message>(environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication', body, {
      headers: httpHeaders,
      observe: 'response',
      params
    })
  }

  uploadFiles(url, file): Observable<any> {
    return this.httpClient.put(url, file, {
      observe: 'events',
      reportProgress: true,
    });
  }

  sendFiles(id, uuids): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication/' + id + '/attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      uuids
    }
    return this.httpClient.post<any>(url, body, {headers: httpHeaders});
  }

  getAllMessages(pageIndex?: number, pageSize?: number, searchInput?: any, isActive?: boolean, id?: number): Observable<Message[]> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST')
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<Message[]>(
      environment.USER_BASE + '/all-messages',
      {pageIndex, pageSize, searchData: searchInput, isActive, id},
      {headers: httpHeaders}
    );
  }


  getMessageById(messageId?: number): Observable<Message> {
    return this.httpClient.post<Message>(environment.USER_BASE + '/message-by-id', {id: messageId});
  }

  updatStatusSingle(id?: number, isActive?: boolean) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.put<any>(environment.USER_BASE + '/messagestatusupdatesingle', {id, isActive}, {headers: httpHeaders});
  }


  deleteMessage(id?: number) {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.httpClient.delete<any>(environment.CORE_BASE + '/companies/' + this.companyId + '/messages/' + id, {headers: httpHeaders});
  }

  deleteMessages(ids: any[]) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    return this.httpClient.post<any>(environment.USER_BASE + '/multiple-soft-delete-message', {ids}, {headers: httpHeaders});
  }

  updateStatusForMessages(ids?: Message[], isActive?: number) {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');
    httpHeaders.set('access-control-allow-methods', 'POST');
    httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + this.authToken);
    const idsForUpdate: any[] = [];
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < ids.length; i++) {
      idsForUpdate.push(ids[i].id);
    }
    const body = {
      ids: idsForUpdate,
      isActive
    };
    return this.httpClient.put<any>(environment.USER_BASE + '/messagestatusupdate', body, {headers: httpHeaders});
  }


  updateMessage(_message: Message): Observable<any> {
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {}
    const id = _message.id
    return this.httpClient.put(environment.CORE_BASE + '/companies/' + this.companyId + '/messages/' + id, body, {headers: httpHeaders}
    );
  }

  findMessages(queryParams: QueryParamsModel): Observable<any> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    const params = new HttpParams({fromString: 'communication_type=NOTIFICATION&sort=updated,DESC&page=' + pageIndex + '&size=' + pageSize});
    return this.httpClient.get<QueryResultsModel>(
      `${environment.TIMELINE_BASE}/companies/${this.companyId}/communication`,
      {headers, params}
      );
  }

  findNewMessages(FULLTIME): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    return this.httpClient.get<QueryResultsModel>(environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication/messages?after=' + FULLTIME, {headers})
  }

  unreadMessages(communication_id): Observable<any> {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    // tslint:disable-next-line:max-line-length
    return this.httpClient.put<QueryResultsModel>(environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication/' + communication_id, {is_completed: true}, {headers})
  }
}
