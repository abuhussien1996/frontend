import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HttpUtilsService, QueryParamsModel, QueryResultsModel} from '../../_base/crud';
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from '../../../../environments/environment';


@Injectable()
export class CommunicationsService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID );
  }

  // POST: send a new email or fax
  sendEmailFax(request): Observable<any> {
    // tslint:disable-next-line:max-line-length
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication?communication_type=' + request.communication_type;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, request.body, {headers: httpHeaders});
  }

  uploadManual(request): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/received_communication';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, request.body, {headers: httpHeaders});
  }

  // POST: send a new email or fax
  sendDocuments(request): Observable<any> {
    // tslint:disable-next-line:max-line-length
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/document/send';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, request.body, {headers: httpHeaders});
  }

  // PUT: upload files to the server
  uploadFiles(url, file): Observable<any> {
    return this.http.put(url, file, {
      observe: 'events',
      reportProgress: true,
    });
  }

  // PUT: upload files to the server
  uploadProfilePic(url, file): Observable<any> {
    return this.http.put(url, file);
  }

  // PUT: upload files to the server
  uploadDocs(request): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/document';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, request.body, {headers: httpHeaders});
  }

  updateDoc(id, uuid): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/document/' + id;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, {uuid}, {headers: httpHeaders});
  }

  getDocs(term): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/document?term=' + term;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  sendFiles(id, uuids): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/communication/' + id + '/attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      uuids
    }
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  received_communication(id, uuids): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/received_communication/' + id + '/attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      uuids
    }
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  findDocumentLibrary(queryParams: QueryParamsModel): Observable<any> {
    const pageSize = queryParams.pageSize
    const pageIndex = queryParams.pageNumber
    const term = queryParams.filter.searchData
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const userToken = localStorage.getItem(environment.authTokenKey);
    headers = headers.set('Authorization', 'Bearer ' + userToken);
    const user = JSON.parse(localStorage.getItem('userInfo'))
    const company = user.companyID
    const params = new HttpParams({fromString: 'term=' + term + '&page=' + pageIndex +'&size=' + pageSize+'&sort='+`${queryParams.sortField},${queryParams.sortOrder}`});
    return this.http.get<QueryResultsModel>(environment.TIMELINE_BASE + '/companies/' + company + '/document', {headers, params})

  }
  geturl(id): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    const userToken = localStorage.getItem(environment.authTokenKey);
    headers = headers.set('Authorization', 'Bearer ' + userToken);
    const user = JSON.parse(localStorage.getItem('userInfo'))
    const company = user.companyID
    return this.http.get<any>(environment.TIMELINE_BASE + '/companies/' + company + '/document/'+id, {headers})
  }
}
