// Angular
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
// RxJS
import {Observable} from 'rxjs';
// CRUD
import {HttpUtilsService} from '../../_base/crud';

// Ngrx
import {select, Store} from '@ngrx/store';
import {AppState} from '../../reducers';
import {currentAuthToken, currentUser} from '../../auth/_selectors/auth.selectors';
import {environment} from '../../../../environments/environment';
import * as _ from 'lodash';
import * as moment from 'moment';


@Injectable()
export class TaskService {
  private authToken;
  private companyId;

  constructor(
    private http: HttpClient,
    private httpUtils: HttpUtilsService,
    private store: Store<AppState>
  ) {
    this.store.pipe(select(currentAuthToken)).subscribe(res => this.authToken = res);
    this.store.pipe(select(currentUser)).subscribe(res => this.companyId = res.companyID);
  }

  // tslint:disable-next-line:max-line-length
  getTasksV2(isCompleted, isCanceled = false, page, size, parentId, entityId, userId,
             categoryId, templateId, priority, maxCreated, minCreated, maxDue, minDue, isFollowing = false
    , sort = 'created', direction = 'desc', projectId = null,term = ''): Observable<any> {
    let project = ''
    if (projectId) {
      project = '&project_id=' + projectId
    }
    let progress = ''
    if (isCompleted && isCompleted !== '') {
      progress = 'DONE'
    }
    if (isCanceled) {
      progress = 'CANCELED'
    }
    let category = ''
    if (categoryId !== undefined) {
      category = categoryId !== 'without' ? '&category_id=' + categoryId : '&without_category=true'
    }
    let template = ''
    if (templateId) {
      template = '&template_id=' + templateId;
    }
    let _priority = ''
    if (priority) {
      _priority = '&priority=' + priority;
    }
    let _term = ''
    if (term) {
      _term = '&search_term=' + term;
    }
    let responsible_user_id =''
    if (userId) {
      responsible_user_id = '&responsible_user_id=' + userId;
    }
    let dueDate = ''
    if (maxDue && minDue && moment.isDate(maxDue) && moment.isDate(minDue)) {
      dueDate = '&due_date_from=' + moment(minDue).format('YYYY-MM-DD') + '&due_date_to=' + moment(maxDue).format('YYYY-MM-DD');
    }
    let createdDate = ''
    if (maxCreated && minCreated && moment.isDate(maxCreated) && moment.isDate(minCreated)) {
      createdDate = '&created_date_from=' + moment(minCreated).format('YYYY-MM-DD') + '&created_date_to=' + moment(maxCreated).format('YYYY-MM-DD');
    }
    const url = environment.TIMELINE_BASE
      + '/companies/' + this.companyId + '/tasks_v2?&sort=' + sort + ',' + direction + '&page=' + page + '&size=' + size
      + '&progress=' + progress + '&parent_id=' + parentId + '&entity_id=' + entityId + '&user_following=' + isFollowing +
      project + _term + category + template + _priority + createdDate + dueDate + responsible_user_id;

    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  getSubTasks(parent_id) {
    const url = `${environment.TIMELINE_BASE}/companies/${this.companyId}/tasks_v2?parent_id=${parent_id}`;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  getUserTasks(start, end, user_ids) {
    const url = user_ids ?
      `${environment.TIMELINE_BASE}/companies/${this.companyId}/tasks?from=${start}&to=${end}&user_ids=${user_ids[0]}`
      : `${environment.TIMELINE_BASE}/companies/${this.companyId}/tasks?from=${start}&to=${end}`;
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }

  createTask2(body): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/tasks_v2';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  updateTaskAttachment(body, task_id): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/tasks_v2/' + task_id + '/upload_attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }

  updateTask2(task): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/tasks_v2/' + task.id;
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.put<any>(url, task, {headers: httpHeaders});
  }

  getEntities(entitiesIds: any[]) {
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    let ids = ''
    // tslint:disable-next-line:no-shadowed-variable
    entitiesIds = _.uniqWith(entitiesIds, _.isEqual);
    entitiesIds.forEach(element => ids = ids + '&id=' + element)
    const params = new HttpParams({fromString: ids});
    return this.http.get<any>(environment.CORE_BASE + '/companies/' + this.companyId + '/general_entities', {
      headers,
      params
    });
  }


  sendFiles(id, uuids): Observable<any> {
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/tasks_v2/' + id + '/attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    const body = {
      uuids
    }
    return this.http.post<any>(url, body, {headers: httpHeaders});
  }


  deleteAttachment(ids: any[], task_id): Observable<any> {
    let paramsArr = 'id='
    for (const id of ids) {
      paramsArr += id + '&id='
    }
    const params = new HttpParams({fromString: paramsArr});
    const url = environment.TIMELINE_BASE + '/companies/' + this.companyId + '/tasks_v2/' + task_id + '/attachments';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.delete<any>(url, {headers: httpHeaders, params});
  }


  getUsersbyId(ids) {
    let param = ''
    const url = environment.USER_BASE + '/users-by-ids';
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    ids = _.uniqWith(ids, _.isEqual);
    ids.forEach(element => param = param + '&id=' + element)
    const params = new HttpParams({fromString: param});
    return this.http.get<any>(url, {headers: httpHeaders, params});
  }

  getTasksCounts(size: number, parentId: string, entityId: string, userId: string, categoryId
    , entitiesId, templateId, priority, maxCreated, minCreated, maxDue, minDue, isFollowing,term?:string): Observable<any> {
    let category = ''
    if (categoryId !== undefined) {
      category = categoryId !== 'without' ? '&category_id=' + categoryId : '&without_category=true'
    }
    let template = ''
    if (templateId) {
      template = '&template_id=' + templateId;
    }
    let _term = ''
    if (term) {
      _term = '&search_term=' + term;
    }
    let _priority = ''
    if (priority) {
      _priority = '&priority=' + priority;
    }
    let dueDate = ''
    if (maxDue && minDue && moment.isDate(maxDue) && moment.isDate(minDue)) {
      dueDate = '&due_date_from=' + moment(minDue).format('YYYY-MM-DD') + '&due_date_to=' + moment(maxDue).format('YYYY-MM-DD');
    }
    let createdDate = ''
    if (maxCreated && minCreated && moment.isDate(maxCreated) && moment.isDate(minCreated)) {
      createdDate = '&created_date_from=' + moment(minCreated).format('YYYY-MM-DD') + '&created_date_to=' + moment(maxCreated).format('YYYY-MM-DD');
    }
    const url = environment.TIMELINE_BASE
      + '/companies/' + this.companyId + '/tasks_v2/count' + '?parent_id='
      + parentId + '&entity_id=' + entityId + '&responsible_user_id=' + userId + category + template + _priority + dueDate + createdDate +
      '&user_following=' + isFollowing + _term;


    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);

    return this.http.get<any>(url, {headers: httpHeaders});
  }

  autocomplateTask(term) {
    const url = environment.TIMELINE_BASE
      + '/companies/' + this.companyId + '/tasks_v2?&search_term=' + term
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }
  getTaskById(id): Observable<any> {
    const url = environment.TIMELINE_BASE + `/companies/${this.companyId}/tasks_v2/${id}`
    const httpHeaders = this.httpUtils.getHTTPHeaders(this.authToken);
    return this.http.get<any>(url, {headers: httpHeaders});
  }
  // @ts-ignore
  findSentTimelines(term, type, id?: number, fromDate, toDate): Observable<any> {
    if (!term) {
      term = ''
    }
    const headers = this.httpUtils.getHTTPHeaders(this.authToken);
    const params = new HttpParams({
      fromString: 'sort=updated,DESC' + '&communication_type=' + type + '&search_term='
        + term + '&from_date=' + toDate + '&to_date=' + fromDate
    });
    return this.http.get<any>(environment.TIMELINE_BASE + '/companies/' + this.companyId +
      '/tasks_v2/'+id+'/timeline', {
      headers,
      params
    })
  }
}
