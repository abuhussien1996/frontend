import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BaseComponent} from './views/theme/base/base.component';
import {AuthGuard, ModuleGuard} from './core/auth';

const routes: Routes = [
  {path: 'auth', loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule)},
  {path: 'error', loadChildren: () => import('./views/pages/error/error.module').then(m => m.ErrorModule)},
  {path: 'callback', loadChildren: () => import('./views/pages/callback/callback.module').then(m => m.CallbackModule)},
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'search',
        // canActivate: [ModuleGuard],
        loadChildren: () => import('./views/pages/general-search-dashboard/general-search-dashboard.module')
          .then(m => m.GeneralSearchDashboardModule),
      },
      {
        path: 'calendar',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToCalendarModule'},
        loadChildren: () => import('./views/pages/calendar/calendar.module').then(m => m.CalendarModule),
      },
      {
        path: 'task',
        loadChildren: () => import('./views/pages/task/task.module').then(m => m.TaskModule),
      },
      {
        path: 'management',
        loadChildren: () => import('./views/pages/management/management.module').then(m => m.ManagementModule),
      },
      {
        path: 'lists',
        loadChildren: () => import('./views/pages/apps/lists/lists.module').then(m => m.ListsModule),
      },
      {
        path: 'events',
        loadChildren: () => import('./views/pages/apps/events/events.module').then(m => m.EventsModule),
      },
      {
        path: 'timeline',
        loadChildren: () => import('./views/pages/apps/timeline/timeline.module').then(m => m.TimelineModule),
      },
      {
        path: 'user-management',
        loadChildren: () => import('./views/pages/user-management/user-management.module').then(m => m.UserManagementModule),
      },
      {
        path: 'activity-log',
        loadChildren: () => import('./views/pages/active-log/activity-log.module').then(m => m.ActivityLogModule),
      },
      {
        path: 'workflows',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToWorkflowMenuModule'},
        loadChildren: () => import('./views/pages/workflow/workflow.module').then(m => m.WorkflowModule),
      },
      {
        path: 'event-legacies',
        canActivate: [ModuleGuard],
        data: {permissionName: 'accessToEventLegacies'},
        loadChildren: () => import('./views/pages/event-legacies/event-legacies.module').then(m => m.EventLegaciesModule),
      },
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: '**', redirectTo: 'error', pathMatch: 'full'},
    ],
  },
  {path: '**', redirectTo: 'error', pathMatch: 'full'},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [ModuleGuard]
})
export class AppRoutingModule {
}
