export const environment = {
  production: false,
  authTokenKey: 'accessToken',
  TIMELINE_BASE: 'http://ec2-3-142-122-212.us-east-2.compute.amazonaws.com:8090/timeline',
  CORE_BASE: 'http://ec2-3-142-122-212.us-east-2.compute.amazonaws.com:8090/core',
  EVENT_BASE: 'http://ec2-3-142-122-212.us-east-2.compute.amazonaws.com:8090/event',
  USER_BASE: 'https://2rawtgd2z0.execute-api.us-east-2.amazonaws.com/dev',
  LEGACIES: 'https://4n9hptxz1a.execute-api.us-east-2.amazonaws.com/dev',
  measurementSystem: 'measurementSystem',
};

