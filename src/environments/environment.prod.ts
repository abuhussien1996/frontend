export const environment = {
  production: true,
  authTokenKey: 'accessToken',
  TIMELINE_BASE: 'https://api.knostos.com/timeline',
  CORE_BASE: 'https://api.knostos.com/core',
  EVENT_BASE: 'https://api.knostos.com/event',
  ME_BASE: 'https://api.knostos.com/me',
  USER_BASE: 'https://5mr1t3q4j0.execute-api.us-east-2.amazonaws.com/dev',
  LEGACIES: 'https://4n9hptxz1a.execute-api.us-east-2.amazonaws.com/dev',
  measurementSystem: 'measurementSystem',
};
