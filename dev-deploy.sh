echo "Start..................."
echo "************************"
echo "************************"
echo "************************"
echo "************************"
echo "************************"
node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng b --prod -c=dev
aws s3 rm s3://dev.wrkflow --recursive --exclude "assets/*"
aws s3 cp --recursive ./dist/ s3://dev.wrkflow --exclude "assets/*"
exec bash;
